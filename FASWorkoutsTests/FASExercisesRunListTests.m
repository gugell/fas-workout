//
//  FASExercisesRunListTests.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/8/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FASExercisesRunList.h"

@interface UBIRunListWhereList (FASExercisesRunListTests)
@property (nonatomic, strong) NSMutableArray* whereListItems;
@end

@interface FASExercisesRunListTests : XCTestCase
@end

@implementation FASExercisesRunListTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testInitHasCorrectFieldList {
    FASExercisesRunList* testObject = [[FASExercisesRunList alloc] init];
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListIDFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListNameFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListCodeFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListMuscleTypesFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListRestTimeFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListTimeSpanFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListLapsNumberFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListWorkoutIDFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListExerciseIDFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListIsStaticFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListModifyDateFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListSortIndexFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListPositionInSupersetFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListDescriptionFieldKey]);
}

- (void)testInitHasExpectedFieldListCount {
    FASExercisesRunList* testObject = [[FASExercisesRunList alloc] init];
    XCTAssertEqual(testObject.fieldList.count, 14);
}

- (void)testInitWithFieldList {
    // 1. create an object
    FASExercisesRunList* testObject = [[FASExercisesRunList alloc] initWithFieldList:@[kFASExercisesRunListExerciseIDFieldKey]];
    // 2. test field list
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListExerciseIDFieldKey]);
    // 3. test options
    XCTAssertEqualObjects(testObject.options.limit, @(100));
    XCTAssertEqualObjects(testObject.options.start, @(0));
    // 4. test method
    XCTAssertEqual(testObject.method, UBIRunListMethodSelect);
    // 5. test entity
    XCTAssertEqualObjects(testObject.entity, @"fas_workout_exercise");
}

- (void)testInitWithWorkoutID {
    NSNumber* testWorkoutID = @(123456);
    FASExercisesRunList* testObject = [[FASExercisesRunList alloc] initWithWorkoutID:testWorkoutID];
    // test fieldList
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListIDFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListNameFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListCodeFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListMuscleTypesFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListRestTimeFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListTimeSpanFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListLapsNumberFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListWorkoutIDFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListExerciseIDFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListIsStaticFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListModifyDateFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListSortIndexFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASExercisesRunListPositionInSupersetFieldKey]);
    // test whereList
    XCTAssertNotNil(testObject.whereList);
    XCTAssertEqual(testObject.whereList.whereListItems.count, 1);
    // TODO: currently UBIRunListWhereListItem internals are hidden and so we cannot test it
//    id condition = [testObject.whereList.whereListItems firstObject];
//    XCTAssertEqual([condition objectForKey:@"name"], @"");
//    XCTAssertEqual([condition objectForKey:@"condition"], @"");
//    XCTAssertEqual([condition objectForKey:@"value"], @"");
//    XCTAssertEqual([condition objectForKey:@"key"], @"");
    
}
@end
