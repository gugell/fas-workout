//
//  UIFont+CustomFontsTests.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 6/21/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit
import XCTest

class UIFont_CustomFontsTests: XCTestCase {

    var fontSize: CGFloat = 12.0
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testRegularBabasFontOfSize() {
        let testFont = UIFont.regularBabasFontOfSize(fontSize)
        XCTAssertNotNil(testFont, "must not be nil")
    }
    
    func testThinBabasFontOfSize() {
        let testFont = UIFont.thinBabasFontOfSize(fontSize)
        XCTAssertNotNil(testFont, "must not be nil")
    }
    
    func testBookBabasFontOfSize() {
        let testFont = UIFont.bookBabasFontOfSize(fontSize)
        XCTAssertNotNil(testFont, "must not be nil")
    }
    
    func testLightBabasFontOfSize() {
        let testFont = UIFont.lightBabasFontOfSize(fontSize)
        XCTAssertNotNil(testFont, "must not be nil")
    }
    
    func testBoldBabasFontOfSize() {
        let testFont = UIFont.boldBabasFontOfSize(fontSize)
        XCTAssertNotNil(testFont, "must not be nil")
    }
    
    // TODO: test preferredBabasFontForTextStyle
}
