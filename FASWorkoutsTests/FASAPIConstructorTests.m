//
//  FASAPIConstructorTests.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/28/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

/// This test case for constructor methods so each FASAPI object is
/// constructed with correct input

#import <XCTest/XCTest.h>
#import "FASAPI.h"
#import "UBIRunList.h"

// mock class
@interface UBIRunList_Mock : UBIRunList
@end

// hidden FASAPI methods
@interface FASAPI (FASAPIConstructorTests)
// properties:
@property (nonatomic) BOOL tokenRequired;
@property (nonatomic, copy) NSString* HTTPMethod;
@property (nonatomic, copy) NSString* url;
// methods:
+ (FASAPI*)APIWithRunList:(UBIRunList*)runList
                   method:(FASAPIMethod)method
                 delegate:(id <FASAPIDelegate>)delegate;
@end

// testcase interface
@interface FASAPIConstructorTests : XCTestCase <FASAPIDelegate> {
    UBIRunList_Mock* mockRunList;
}
@end

@implementation FASAPIConstructorTests
///////////////////////////////////////////////
#pragma mark - setup
///////////////////////////////////////////////
- (void)setUp {
    [super setUp];
    mockRunList = [[UBIRunList_Mock alloc] init];
}

- (void)tearDown {
    mockRunList = nil;
    [super tearDown];
}
///////////////////////////////////////////////
#pragma mark - RunList tests
///////////////////////////////////////////////
- (void)testCommonContructorCreatesAPIWithExpectedValues {
    // 1. create API object
    // Note: we don't implement delegate protocol - the delegation behaviour is not a point of this test-case
    FASAPI* testObject = [FASAPI APIWithRunList:mockRunList
                                         method:FASAPIMethodGetWorkoutsModifyDate
                                       delegate:self];
    // 2. test after init
    XCTAssertNotNil(testObject);
    XCTAssertEqualObjects(testObject.body, @"test runlist string");
    XCTAssertEqual(testObject.method, FASAPIMethodGetWorkoutsModifyDate);
    XCTAssertEqual(testObject.delegate, self);
    XCTAssertTrue(testObject.tokenRequired);
    XCTAssertEqualObjects(testObject.HTTPMethod, @"POST");
    // Note: we cannot test for exact string matching here, because base URL and the protocol is
    // taken from environment configuration, thus we test only for substring for now.
    // TODO: test it w/ UBIEnvironment mock
    XCTAssertTrue([testObject.url containsString:@"runList?"]);
}

// Note: tests below only make sure the class assign correct 'tag' (method).
// Other parts of initialization are tested in the method above.

- (void)testGetWorkoutsWithRunListDelegate {
    FASAPI* testObject = [FASAPI getWorkoutsModifyDateWithRunList:mockRunList delegate:self];
    XCTAssertNotNil(testObject);
    XCTAssertEqual(testObject.method, FASAPIMethodGetWorkoutsModifyDate);
}

- (void)testGetExercisesWithRunListDelegate {
    FASAPI* testObject = [FASAPI getExercisesWithRunList:mockRunList delegate:self];
    XCTAssertNotNil(testObject);
    XCTAssertEqual(testObject.method, FASAPIMethodGetExercisesWithOptions);
}

- (void)testGetAttachmentsWithRunList {
    FASAPI* testObject = [FASAPI getAttachmentsWithRunList:mockRunList delegate:self];
    XCTAssertNotNil(testObject);
    XCTAssertEqual(testObject.method, FASAPIMethodGetAttachmentsWithOptions);
}

- (void)testGetAttachmentsForExercisesWithRunList {
    FASAPI* testObject = [FASAPI getAttachmentsForExercisesWithRunList:mockRunList delegate:self];
    XCTAssertNotNil(testObject);
    XCTAssertEqual(testObject.method, FASAPIMethodGetAttachmentsForExercisesWithOptions);
}

@end

// Mock classes
@implementation UBIRunList_Mock
- (NSString *)stringValue {
    // Note: the string returned here has incorrect format,
    // but for these test cases we don't need it to be so
    return @"test runlist string";
}
@end
