//
//  FASTimerControllerTests.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/13/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FASTimerController.h"

// private properties
@interface FASTimerController(FASTimerControllerBasicTests)
@property (strong, readonly)            NSTimer *       timer;
@property (nonatomic, strong, readonly) NSDate *        startCountDate;
@property (nonatomic, strong, readonly) NSDate *        pausedTime;
@property (nonatomic, strong, readonly) NSDate *        date1970;
@property (nonatomic, strong, readonly) NSDate *        timeToCountOff;
@end

@interface FASTimerControllerBasicTests : XCTestCase

@end

@implementation FASTimerControllerBasicTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// Test that default initialization went w/ correct default values being set to variables

- (void)testInit {
    FASTimerController* timerCtrl = [[FASTimerController alloc] init];
    // just check the correct default parameters are being set
    XCTAssertEqual(timerCtrl.mode, FASTimerControllerModeDefault);
    XCTAssertEqual(timerCtrl.type, FASTimerControllerTypeCountdownTimer);
    XCTAssertEqual(timerCtrl.userTime, 1.0);
}

- (void)testInitWithStopwatch {
    FASTimerController* timerCtrl = [[FASTimerController alloc] initWithType:FASTimerControllerTypeStopwatch
                                                                    userTime:2.0
                                                                        mode:FASTimerControllerModeDefault];
    XCTAssertEqual(timerCtrl.type, FASTimerControllerTypeStopwatch);
    XCTAssertEqual(timerCtrl.mode, FASTimerControllerModeDefault);
    XCTAssertEqual(timerCtrl.userTime, 2.0);
    XCTAssertNotNil(timerCtrl.date1970);
    XCTAssertNotNil(timerCtrl.startCountDate, "Should not be nil for Stopwatch configuration");
    XCTAssertNotNil(timerCtrl.pausedTime, "Should not be nil for Stopwatch configuration");
    XCTAssertNotEqual(timerCtrl.pausedTime, timerCtrl.startCountDate, "start Count & Paused date must not be equal");
    NSTimeInterval diff = [timerCtrl.pausedTime timeIntervalSinceDate:timerCtrl.startCountDate];
    XCTAssertEqualWithAccuracy(timerCtrl.userTime, diff, 0.001);
    XCTAssertEqual(timerCtrl.counting, NO);
    XCTAssertNil(timerCtrl.timer, @"Should be nil until \"start\"");
    
    XCTAssertEqualWithAccuracy(timerCtrl.userTime, [timerCtrl getTimeCounted], 0.001);
    XCTAssertEqual([timerCtrl getTimeRemaining], 0);
}

- (void)testInitWithCountdownTimer {
    FASTimerController* timerCtrl = [[FASTimerController alloc] initWithType:FASTimerControllerTypeCountdownTimer
                                                                    userTime:2.0
                                                                        mode:FASTimerControllerModeDefault];
    XCTAssertEqual(timerCtrl.type, FASTimerControllerTypeCountdownTimer);
    XCTAssertEqual(timerCtrl.mode, FASTimerControllerModeDefault);
    XCTAssertEqual(timerCtrl.userTime, 2.0);
    XCTAssertNotNil(timerCtrl.date1970);
    XCTAssertNotNil(timerCtrl.timeToCountOff, "Should not be nil for Countdown Timer configuration");
    NSTimeInterval diff = [timerCtrl.timeToCountOff timeIntervalSinceDate:timerCtrl.date1970];
    XCTAssertEqualWithAccuracy(timerCtrl.userTime, diff, 0.001);
    XCTAssertEqual(timerCtrl.counting, NO);
    XCTAssertNil(timerCtrl.timer, @"Should be nil until \"start\"");
    
    XCTAssertEqual([timerCtrl getTimeCounted], 0);
    XCTAssertEqual([timerCtrl getTimeRemaining], timerCtrl.userTime);
}

- (void)testInitWithStopwatchNegativeTime {
    FASTimerController* timerCtrl = [[FASTimerController alloc] initWithType:FASTimerControllerTypeStopwatch
                                                                    userTime:-1.0
                                                                        mode:FASTimerControllerModeDefault];
    XCTAssertEqual(timerCtrl.userTime, 0.0);
}

- (void)testInitWithCountdownTimerNegativeTime {
    FASTimerController* timerCtrl = [[FASTimerController alloc] initWithType:FASTimerControllerTypeCountdownTimer
                                                                    userTime:-1.0
                                                                        mode:FASTimerControllerModeDefault];
    XCTAssertEqual(timerCtrl.userTime, 0.0);
}
@end
