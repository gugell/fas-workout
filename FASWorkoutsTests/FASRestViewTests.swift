//
//  FASRestViewTests.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/18/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit
import XCTest

class FASRestViewTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testXIBDoesExist() {
        let bundle = NSBundle(forClass: FASRestViewTests.self )
        XCTAssertNotNil(bundle)
        XCTAssertNotNil(bundle.pathForResource("FASRestView", ofType: "nib"))
    }
}
