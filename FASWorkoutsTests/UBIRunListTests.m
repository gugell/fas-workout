//
//  UBIRunListTests.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/27/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UBIRunList.h"

@interface UBIRunListTests : XCTestCase

@end

@implementation UBIRunListTests {
    @private
    UBIRunList *rl;
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark setup
#pragma mark -
///////////////////////////////////////////////
- (void)setUp {
    [super setUp];
    rl = [[UBIRunList alloc] init];
    XCTAssertNotNil(rl);
}

- (void)tearDown {
    rl = nil;
    [super tearDown];
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark simple tests
#pragma mark -
///////////////////////////////////////////////
- (void)testInit {
    XCTAssertEqual(rl.method, UBIRunListMethodNone);
    XCTAssertNotNil(rl.options);
    XCTAssertNotNil(rl.whereList);
    XCTAssertFalse(rl.nativeDatasetFormat);
    XCTAssertNil(rl.entity);
}

- (void)testInitReturnsNilStringValue {
    NSString* testValue = [rl stringValue];
    XCTAssertNil(testValue);
}

- (void)testRunListReturnsNilStringValue {
    // add first valid param
    rl.entity = @"test_entity";
    XCTAssertNil([rl stringValue]);
    // add one more valid
    rl.method = UBIRunListMethodSelect;
    XCTAssertNil([rl stringValue]);
    // last valid param needed
    rl.fieldList = @[@"ID"];
    XCTAssertNotNil([rl stringValue]);
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark correct string value tests
#pragma mark -
///////////////////////////////////////////////
- (void)testSelectForEntityWithOneField {
    rl.entity = @"test_entity";
    rl.method = UBIRunListMethodSelect;
    rl.fieldList = @[@"ID"];
    NSString* expectedValue = @"[{\"entity\":\"test_entity\",\"method\":\"select\",\"fieldList\":[\"ID\"],}]";
    // Note: although comma after fieldList might seem wrong, UB returns data set with runList formatted like this
    NSString* testValue = [rl stringValue];
    XCTAssertEqualObjects(expectedValue, testValue);
}

- (void)testSelectForEntityWithTwoFields {
    rl.entity = @"test_entity";
    rl.method = UBIRunListMethodSelect;
    rl.fieldList = @[@"ID",@"mi_modifyDate"];
    NSString* expectedValue = @"[{\"entity\":\"test_entity\",\"method\":\"select\",\"fieldList\":[\"ID\",\"mi_modifyDate\"],}]";
    NSString* testValue = [rl stringValue];
    XCTAssertEqualObjects(expectedValue, testValue);
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark correct string value tests (options)
#pragma mark -
///////////////////////////////////////////////
- (void)testStringValueWithSelectMethodForEntityFieldsAndOptions {
    rl.entity = @"test_entity";
    rl.method = UBIRunListMethodSelect;
    rl.fieldList = @[@"field01",@"field02",@"field03"];
    rl.options.limit = @(100);
    rl.options.start = @(0);
    
    NSString* expectedValue = @"[{\"entity\":\"test_entity\",\"method\":\"select\",\"fieldList\":[\"field01\",\"field02\",\"field03\"],\"options\":{\"limit\":100,\"start\":0}}]";
    NSString* testValue = [rl stringValue];
    
    XCTAssertEqualObjects(expectedValue, testValue);
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark correct string value tests (whereList)
#pragma mark -
///////////////////////////////////////////////
- (void)testStringValueWithSelectMethodForEntityFieldsAndWhereList {
    rl.entity = @"test_entity";
    rl.method = UBIRunListMethodSelect;
    rl.fieldList = @[@"field01",@"field02",@"field03"];
    [rl.whereList addItem:@"TestWhereListCondition" condition:UBIRunListWhereListConditionEqual value:@(20) key:@"test_key"];
    // tow commas is all right for UB
    NSString* expectedValue = @"[{\"entity\":\"test_entity\",\"method\":\"select\",\"fieldList\":[\"field01\",\"field02\",\"field03\"],,\"whereList\":{\"TestWhereListCondition\":{\"values\":{\"test_key\":\"20\"},\"expression\":\"[test_key]\",\"condition\":\"equal\"}}}}]";
    NSString* testValue = [rl stringValue];
    
    XCTAssertEqualObjects(expectedValue, testValue);
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark correct string value tests (whereList & options)
#pragma mark -
///////////////////////////////////////////////
- (void)testStringValueWithSelectMethodForEntityFieldsWhereListAndOptions {
    rl.entity = @"test_entity";
    rl.method = UBIRunListMethodSelect;
    rl.fieldList = @[@"field01",@"field02",@"field03"];
    [rl.whereList addItem:@"TestWhereListCondition" condition:UBIRunListWhereListConditionEqual value:@(20) key:@"test_key"];
    rl.options.limit = @(100);
    rl.options.start = @(0);
    // tow commas is all right for UB
    NSString* expectedValue = @"[{\"entity\":\"test_entity\",\"method\":\"select\",\"fieldList\":[\"field01\",\"field02\",\"field03\"],\"options\":{\"limit\":100,\"start\":0},\"whereList\":{\"TestWhereListCondition\":{\"values\":{\"test_key\":\"20\"},\"expression\":\"[test_key]\",\"condition\":\"equal\"}}}}]";
    NSString* testValue = [rl stringValue];
    
    XCTAssertEqualObjects(expectedValue, testValue);
}

// TODO: test addValuesFromDictionary:

@end
