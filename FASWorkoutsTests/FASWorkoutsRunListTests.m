//
//  FASWorkoutsRunListTests.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/28/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FASWorkoutsRunList.h"

@interface FASWorkoutsRunListTests : XCTestCase

@end

@implementation FASWorkoutsRunListTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testInitHasCorrectFieldList {
    FASWorkoutsRunList* testObject = [[FASWorkoutsRunList alloc] init];
    XCTAssertTrue([testObject.fieldList containsObject:kFASWorkoutsRunListNameFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASWorkoutsRunListCodeFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASWorkoutsRunListMuscleTypesNameFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASWorkoutsRunListRestTimeFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASWorkoutsRunListTimeSpanFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASWorkoutsRunListLapsNumberFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASWorkoutsRunListDifficultyFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASWorkoutsRunListMotivationTextFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASWorkoutsRunListWorkoutIDFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASWorkoutsRunListModifyDateFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASWorkoutsRunListCaloriesFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASWorkoutsRunListInAppProductIDFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASWorkoutsRunListOnSaleFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASWorkoutsRunListGenderFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASWorkoutsRunListPrimaryMuscleTypeFieldKey]);
    XCTAssertTrue([testObject.fieldList containsObject:kFASWorkoutsRunListSortIndexFieldKey]);    
}

- (void)testInitHasExpectedFieldListCount {
    FASWorkoutsRunList* testObject = [[FASWorkoutsRunList alloc] init];
    XCTAssertEqual(testObject.fieldList.count, 16);
}

- (void)testInitWithFieldList {
    // 1. create an object
    FASWorkoutsRunList* testObject = [[FASWorkoutsRunList alloc] initWithFieldList:@[kFASWorkoutsRunListWorkoutIDFieldKey]];
    // 2. test field list
    XCTAssertTrue([testObject.fieldList containsObject:kFASWorkoutsRunListWorkoutIDFieldKey]);
    // 3. test options
    XCTAssertEqualObjects(testObject.options.limit, @(100));
    XCTAssertEqualObjects(testObject.options.start, @(0));
    // 4. test method
    XCTAssertEqual(testObject.method, UBIRunListMethodSelect);
    // 5. test entity
    XCTAssertEqualObjects(testObject.entity, @"fas_workout");
}

- (void)testWorkoutsRunListDefaultSameAsInit {
    // 1. create objects using different methods
    FASWorkoutsRunList* testObject = [[FASWorkoutsRunList alloc] init];
    FASWorkoutsRunList* expectedObject = [FASWorkoutsRunList workoutsRunListDefault];
    // 2. test field list are equal
    [expectedObject.fieldList enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        BOOL containsObject = NO;
        for (id object in testObject.fieldList) {
            if ([object isEqual:obj]) {
                containsObject = YES;
                break;
            }
        }
        if (!containsObject) {
            XCTAssertTrue(NO, @"testObject does not have '%@' field", obj);
            *stop = YES;
        }
    }];
    // 3. test options are equal
    XCTAssertEqualObjects(testObject.options.limit, expectedObject.options.limit);
    XCTAssertEqualObjects(testObject.options.start, expectedObject.options.start);
    // 4. test method
    XCTAssertEqual(testObject.method, expectedObject.method);
    // 5. test entity
    XCTAssertEqualObjects(testObject.entity, expectedObject.entity);
    // TODO:
    // better approach would probably be
    // 1. override isEqual for Run List and other related classes
    // 2. use aspects to find outh whether the 'init' method is called inside 'workoutsRunListDefault'
}

- (void)testWorkoutsRunListExcludeIncomplete {
    // TODO: this is very silly testing, should be improved
    FASWorkoutsRunList* testObject = [FASWorkoutsRunList workoutsRunListExcludeIncomplete];
    XCTAssertNotNil(testObject);
    XCTAssertNotNil(testObject.whereList, @"expect not nil whereList");
}

@end
