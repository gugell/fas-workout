//
//  NSDate+MyExtensionsTests.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/13/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import XCTest

class NSDate_MyExtensionsFormatedStringTests: XCTestCase {

    var date: NSDate?
    
    override func setUp() {
        super.setUp()
        
        func createDate() -> NSDate {
            let dateComponents = NSDateComponents()
            dateComponents.hour = 18
            dateComponents.minute = 38
            dateComponents.second = 15
            dateComponents.nanosecond = 123000000
            let calendar = NSCalendar.currentCalendar()
            calendar.timeZone = NSTimeZone.systemTimeZone()//NSTimeZone(forSecondsFromGMT: 0)
            return calendar.dateFromComponents(dateComponents)!
        }
        
        date = createDate()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testFormatWithSeconds() {
        XCTAssertNotNil(date, "Test preconditions failed")
        
        XCTAssertEqual(date!.toStringTime(), "18:38:15", "String from date format with seconds failed")
    }
    
    func testFormatWithMiliseconds() {
        XCTAssertNotNil(date, "Test preconditions failed")
        
        XCTAssertEqual(date!.toStringTimeWithMiliseconds(), "18:38:15:12", "String from date format with miliseconds failed")
    }
    
    func testWrongFormatWithSeconds() {
        XCTAssertNotNil(date, "Test preconditions failed")
        
        XCTAssertNotEqual(date!.toStringTime(), "18:38:16", "Conversion failed for not equal date with seconds")
    }
    
    func testFormatWithMinutesAndSeconds() {
        XCTAssertNotNil(date, "Test preconditions failed")
        
        XCTAssertEqual(date!.toStringTimeMinutesAndSeconds(), "38:15", "String from date format with minutes and seconds failed")
    }
    
    func testWrongFormatWithMiliseconds() {
        XCTAssertNotNil(date, "Test preconditions failed")
        
        XCTAssertNotEqual(date!.toStringTimeWithMiliseconds(), "18:38:15:13", "Conversion failed for not equal date with miliseconds")
    }
    
    func testFormatWithHoursAndMinutes() {
        XCTAssertNotNil(date, "Test preconditions failed")
        
        XCTAssertEqual(date!.toStringTimeHoursAndMinutes(), "18:38", "Conversion failed for not equal date with miliseconds")
    }
}

class NSDate_MyExtensionsPeriodOfTimeTests: XCTestCase {
    
    // MARK: setup
    let calendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: tests
    
    func testYesterday() {
        let expectedDate: NSDate = NSDate(timeIntervalSinceNow: -86400)
        let testDate: NSDate = NSDate.yesterday()
        
        let comparisonResult = { (unit: NSCalendarUnit) -> NSComparisonResult in
            return self.calendar!.compareDate(expectedDate, toDate: testDate, toUnitGranularity: unit)
        }
        
        XCTAssertTrue(comparisonResult(NSCalendarUnit.Day) == .OrderedSame)
        XCTAssertTrue(comparisonResult(NSCalendarUnit.Hour) == .OrderedSame)
        XCTAssertTrue(comparisonResult(NSCalendarUnit.Minute) == .OrderedSame)
        XCTAssertTrue(comparisonResult(NSCalendarUnit.Second) == .OrderedSame)
    }
    
    func testTwoHoursAgo() {
        let expectedDate: NSDate = NSDate(timeIntervalSinceNow: -7200)
        let testDate: NSDate = NSDate.twoHoursAgo()
        
        let comparisonResult = { (unit: NSCalendarUnit) -> NSComparisonResult in
            return self.calendar!.compareDate(expectedDate, toDate: testDate, toUnitGranularity: unit)
        }
        
        XCTAssertTrue(comparisonResult(NSCalendarUnit.Day) == .OrderedSame)
        XCTAssertTrue(comparisonResult(NSCalendarUnit.Hour) == .OrderedSame)
        XCTAssertTrue(comparisonResult(NSCalendarUnit.Minute) == .OrderedSame)
        XCTAssertTrue(comparisonResult(NSCalendarUnit.Second) == .OrderedSame)
    }
    
    func testTodayAtNineAM() {
        // TODO: add a test
        let testDate: NSDate? = NSDate.todayAtNineAM()
        XCTAssertNotNil(testDate, "unexpectadly found nil")
    }
}
