//
//  UBIRunListWhereListTests.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/27/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UBIRunList.h"

// hidden methods/ properties
@interface UBIRunListWhereList (UBIRunListWhereListTests)
@property (nonatomic, strong) NSMutableArray* whereListItems;
@end

@interface UBIRunListWhereListTests : XCTestCase

@end

@implementation UBIRunListWhereListTests {
    UBIRunListWhereList* rlWhereList;
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark setup
#pragma mark -
///////////////////////////////////////////////
- (void)setUp {
    [super setUp];
    rlWhereList = [[UBIRunListWhereList alloc] init];
    XCTAssertNotNil(rlWhereList);
}

- (void)tearDown {
    rlWhereList = nil;
    [super tearDown];
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark test
#pragma mark -
///////////////////////////////////////////////
- (void)testInit {
    XCTAssertNotNil(rlWhereList.whereListItems);
}

- (void)testAddItemWithConditionEqual {
    [rlWhereList addItem:@"TestItemName" condition:UBIRunListWhereListConditionEqual value:@"test_value" key:@"test_key"];
    XCTAssertEqual(rlWhereList.whereListItems.count, 1);
    
    // test correct stringValue is returned
    NSString* expectedValue = @",\"whereList\":{\"TestItemName\":{\"values\":{\"test_key\":\"test_value\"},\"expression\":\"[test_key]\",\"condition\":\"equal\"}}}";
    NSString* testValue = [rlWhereList stringValue];
    XCTAssertEqualObjects(testValue, expectedValue);
}

- (void)testAddItemWithConditionIn {
    [rlWhereList addItem:@"TestItemName" condition:UBIRunListWhereListConditionIn value:@"test_value" key:@"test_key"];
    XCTAssertEqual(rlWhereList.whereListItems.count, 1);
    
    // test correct stringValue is returned
    NSString* expectedValue = @",\"whereList\":{\"TestItemName\":{\"values\":{\"test_key\":},\"expression\":\"[test_key]\",\"condition\":\"in\"}}";
    NSString* testValue = [rlWhereList stringValue];
    XCTAssertEqualObjects(testValue, expectedValue);
}

- (void)testUnsupportedWhereListElementCount {
    // where list does not currently support adding more than one conditions
    // thus should return an empty string value
    [rlWhereList addItem:@"TestItemName1" condition:UBIRunListWhereListConditionEqual value:@"test_value1" key:@"test_key"];
    XCTAssertEqual(rlWhereList.whereListItems.count, 1);
    [rlWhereList addItem:@"TestItemName2" condition:UBIRunListWhereListConditionIn value:@"test_value2" key:@"test_key"];
    XCTAssertEqual(rlWhereList.whereListItems.count, 2);
    
    XCTAssertEqualObjects(@"", [rlWhereList stringValue]);
}

@end