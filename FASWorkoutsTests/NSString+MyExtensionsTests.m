//
//  NSString+MyExtensionsTests.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/23/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "NSString+MyExtensions.h"

@interface NSString_MyExtensionsTests : XCTestCase

@end

@implementation NSString_MyExtensionsTests
///////////////////////////////////////////////
#pragma mark -
#pragma mark setup
#pragma mark -
///////////////////////////////////////////////
- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark stringByRemovingFirstOccurrenceOfString tests
#pragma mark -
///////////////////////////////////////////////
- (void)testStringByRemovingFirstOccurrenceOfString_SingleOccurence
{
    NSString* testString = @"Cmd #0: Attribute fas_workout_exercise.isSplit not exist";
    NSString* substring = @"Cmd #0: ";
    NSString* expectedString = @"Attribute fas_workout_exercise.isSplit not exist";
    NSString* result = [testString stringByRemovingFirstOccurrenceOfString:substring];
    XCTAssertTrue([expectedString isEqualToString:result]);
}

- (void)testStringByRemovingFirstOccurrenceOfString_MultipleOccurence
{
    NSString* testString = @"Cmd #0: Attribute fas_workout_exercise.isSplit not exist Cmd #0: ";
    NSString* substring = @"Cmd #0: ";
    NSString* expectedString = @"Attribute fas_workout_exercise.isSplit not exist Cmd #0: ";
    NSString* result = [testString stringByRemovingFirstOccurrenceOfString:substring];
    XCTAssertTrue([expectedString isEqualToString:result]);
}

- (void)testStringByRemovingFirstOccurrenceOfString_NoneOccurence
{
    NSString* testString = @"Cmd #0: Attribute fas_workout_exercise.isSplit not exist";
    NSString* substring = @"asdasdasdsa";
    NSString* expectedString = @"Cmd #0: Attribute fas_workout_exercise.isSplit not exist";
    NSString* result = [testString stringByRemovingFirstOccurrenceOfString:substring];
    XCTAssertTrue([expectedString isEqualToString:result]);
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark formattedTimeFromSeconds tests
#pragma mark -
///////////////////////////////////////////////

- (void)testFormattedTimeFromSecondsLessThanMinute {
    NSString* testValue = [NSString formattedTimeFromSeconds:@24];
    NSString* expectedValue = @"00:24";
    XCTAssertEqualObjects(testValue, expectedValue);
}

- (void)testFormattedTimeFromSecondsLessThanHour {
    NSString* testValue = [NSString formattedTimeFromSeconds:@400];
    NSString* expectedValue = @"06:40";
    XCTAssertEqualObjects(testValue, expectedValue);
}

- (void)testFormattedTimeFromSecondsMoreThanHour {
    NSString* testValue = [NSString formattedTimeFromSeconds:@3680];
    NSString* expectedValue = @"1:01:20";
    XCTAssertEqualObjects(testValue, expectedValue);
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark stringByTakingFirstPartOfEmailAddress tests
#pragma mark -
///////////////////////////////////////////////
- (void)testStringByTakingFirstPartOfEmailAddressValidEmail {
    NSString* testString = @"test@test.com";
    NSString* expectedString = @"test";
    NSString* result = [testString stringByTakingFirstPartOfEmailAddress];
    XCTAssertTrue([expectedString isEqualToString:result]);
}

- (void)testStringByTakingFirstPartOfEmailAddressNotAnEmail {
    NSString* testString = @"test";
    NSString* expectedString = @"test";
    NSString* result = [testString stringByTakingFirstPartOfEmailAddress];
    XCTAssertTrue([expectedString isEqualToString:result]);
}

@end
