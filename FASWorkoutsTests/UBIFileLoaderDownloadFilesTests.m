//
//  UBIFileLoaderDownloadFilesTests.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/22/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

@interface UBIFileLoaderDownloadFilesTests : XCTestCase

@end

@implementation UBIFileLoaderDownloadFilesTests {
    NSURL *file01URL, *file02URL, *file03URL;
}

- (void)setUp {
    [super setUp];
    
    file01URL = [NSURL URLWithString:@""];
    file02URL = [NSURL URLWithString:@""];
    file03URL = [NSURL URLWithString:@""];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

@end
