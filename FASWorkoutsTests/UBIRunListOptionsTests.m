//
//  UBIRunListOptionsTests.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/27/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "UBIRunList.h"
#import <XCTest/XCTest.h>

@interface UBIRunListOptionsTests : XCTestCase

@end

@implementation UBIRunListOptionsTests {
    UBIRunListOptions* rlOptions;
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark setup
#pragma mark -
///////////////////////////////////////////////
- (void)setUp {
    [super setUp];
    rlOptions = [[UBIRunListOptions alloc] init];
    XCTAssertNotNil(rlOptions);
}

- (void)tearDown {
    rlOptions = nil;
    [super tearDown];
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark setup
#pragma mark -
///////////////////////////////////////////////
- (void)testInit {
    XCTAssertNil(rlOptions.limit);
    XCTAssertNil(rlOptions.start);
}

- (void)testSetLimitAndStart {
    rlOptions.limit = [NSNumber numberWithFloat:20];
    rlOptions.start = [NSNumber numberWithFloat:0];
    XCTAssertNotNil(rlOptions.limit);
    XCTAssertNotNil(rlOptions.start);
}

- (void)testInitRetutnsEmptyStringValue {
    XCTAssertEqualObjects([rlOptions stringValue], @"");
}

- (void)testOptionsReturnsEmptyStringValueForIncompleteParams {
    rlOptions.limit = [NSNumber numberWithFloat:20];
    rlOptions.start = nil;
    XCTAssertEqualObjects([rlOptions stringValue], @"");
    
    rlOptions.limit = nil;
    rlOptions.start = [NSNumber numberWithFloat:0];
    XCTAssertEqualObjects([rlOptions stringValue], @"");
}

- (void)testOptionsStringValueReturnsCorrectValue {
    rlOptions.limit = [NSNumber numberWithFloat:20];
    rlOptions.start = [NSNumber numberWithFloat:0];
    XCTAssertEqualObjects([rlOptions stringValue], @"\"options\":{\"limit\":20,\"start\":0}");
}

// TODO: tets addValuesFromDictionary:

@end
