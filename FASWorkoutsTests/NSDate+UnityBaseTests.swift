//
//  NSDate+UnityBaseTests.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/12/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import XCTest

class NSDate_UnityBaseTests: XCTestCase {
    
    let correctDateString = "2014-11-23T18:38:15Z"
    let wrongDateString = "wrong format string"
    var expectedDate: NSDate {
        let dateComponents = NSDateComponents()
        dateComponents.year = 2014
        dateComponents.month = 11
        dateComponents.day = 23
        dateComponents.hour = 18
        dateComponents.minute = 38
        dateComponents.second = 15
        let calendar = NSCalendar.currentCalendar()
        calendar.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        return calendar.dateFromComponents(dateComponents)!
    }
    
    override func setUp() {
        super.setUp()
        // setup code
    }
    
    override func tearDown() {
        super.tearDown()
        // tear down code
    }
    
    func testCorrectFormarSting() {
        let testDate = NSDate(fromUnityBaseString: correctDateString)
        XCTAssertNotNil(testDate, "Converted date object must not be nil")
        XCTAssertEqual(testDate!, expectedDate, "Converted date not equal to test date")
    }
    
    func testWrongFormatSting() {
        let testDate = NSDate(fromUnityBaseString: wrongDateString)
        XCTAssertNil(testDate, "Converted date object must be nil")
    }
    
    func testClassMethod() {
        let testDate = NSDate.dateFromUnityBaseString(correctDateString)
        XCTAssertNotNil(testDate, "Converted date object must not be nil")
    }
    
    func testClassMethodCorrectFormarSting() {
        let testDate = NSDate.dateFromUnityBaseString(correctDateString)
        XCTAssertEqual(testDate!, expectedDate, "Converted date not equal to test date")
    }
    
    func testTwoSubsequentConvertationsReturnsSameDate() {
        let date1: NSDate = NSDate(fromUnityBaseString: correctDateString)!
        let date2: NSDate = NSDate(fromUnityBaseString: correctDateString)!
        
        XCTAssertEqual(date1, date2, "dates created from same string should be equal")
        XCTAssertTrue(date1.compare(date2) == .OrderedSame)
    }
}