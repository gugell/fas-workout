//
//  FASWorkoutSessionTests.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/30/15.
//  Copyright © 2015 FAS. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FASWorkoutSession.h"

@interface FASWorkoutSession (TestablePrivate)
@property (nonatomic, strong) NSDate* onPauseData;
@end

@interface FASWorkoutSessionTests : XCTestCase

@end

@implementation FASWorkoutSessionTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Pause
#pragma mark -
///////////////////////////////////////////////

- (void)testSetOnPause {
    // initial state
    FASWorkoutSession* testSession = [[FASWorkoutSession alloc] initWithWorkoutID:@3000000009703];
    XCTAssertNil(testSession.onPauseData, @" upon init is expected");
    XCTAssertNotNil(testSession.pauseDuration);
    XCTAssertEqual(testSession.pauseDuration, @0);
    
    // this should be ignored b/c it is not "on pause" currently
    [testSession setOnPause:NO];
    // just make sure nothing is changed
    XCTAssertNil(testSession.onPauseData);
    XCTAssertEqual(testSession.pauseDuration, @0);
    
    // set "on pause"
    [testSession setOnPause:YES];
    XCTAssertNotNil(testSession.onPauseData, @"onPauseData != nil after set on pause is expected");
    XCTAssertEqual(testSession.pauseDuration, @0);
    
    // subsequent set on pause should be ignored
    [testSession setOnPause:YES];
    // just make sure nothing is changed
    XCTAssertNotNil(testSession.onPauseData);
    XCTAssertEqual(testSession.pauseDuration, @0);
    
    // emulate "after some time"
    sleep(2);
    
    // unset "on pause"
    [testSession setOnPause:NO];
    XCTAssertNil(testSession.onPauseData);
    XCTAssertNotEqual(testSession.pauseDuration, @0);
    XCTAssertTrue([testSession.pauseDuration compare:@0] == NSOrderedDescending);
    
    // make sure we actually "add" pause duration
    NSNumber* previousPauseDuration  = testSession.pauseDuration;
    [testSession setOnPause:YES];
    sleep(2);
    [testSession setOnPause:NO];
    XCTAssertTrue([testSession.pauseDuration compare:previousPauseDuration] == NSOrderedDescending);
}

@end
