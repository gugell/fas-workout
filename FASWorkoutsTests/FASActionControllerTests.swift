//
//  FASActionControllerTests.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 6/29/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import XCTest

class MockShareableItem: FacebookContentShareable {
    // conforming to FacebookContentShareableStings
    func fbShareContentTitle() -> String {
        return ""
    }
    
    func fbShareContentDescription() -> String {
        return ""
    }
    
    // conforming to FacebookContentShareableURLs
    func fbShareContentURL() -> NSURL? {
        return NSURL()
    }
    
    func fbShareContentImageURL() -> NSURL? {
        return NSURL()
    }
}

class FASActionControllerTests: XCTestCase {

    var vc: UIViewController?
    
    override func setUp() {
        super.setUp()
        vc = UIViewController()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFacebookShareContentAllArgumentsAreNil() {
        let actionCtrl = FASActionController(viewController: vc)
        actionCtrl.fbShareContentWithItem(nil, completion: nil)
    }
    
    func testFacebookShareContentWithNoShareableItem() {
        let actionCtrl = FASActionController(viewController: vc)
        
        let expectation = expectationWithDescription("COMPLETED")
        
        actionCtrl.fbShareContentWithItem(nil, completion: { (error) -> Void in
            expectation.fulfill()
            XCTAssertNotNil(error, "error should not be nil")
        })
        
        waitForExpectationsWithTimeout(15) { error in
            if let error = error {
                printlnDebugError("Error was: \(error.localizedDescription)")
            }
        }
    }

    func testFacebookShareContentWithNilViewController() {
        let actionCtrl = FASActionController()
        let shareableItem = MockShareableItem()
        let expectation = expectationWithDescription("COMPLETED")
        
        actionCtrl.fbShareContentWithItem(shareableItem, completion: { (error) -> Void in
            expectation.fulfill()
            XCTAssertNotNil(error, "error should not be nil")
        })
        
        waitForExpectationsWithTimeout(15) { error in
            if let error = error {
                printlnDebugError("Error was: \(error.localizedDescription)")
            }
        }
    }
}
