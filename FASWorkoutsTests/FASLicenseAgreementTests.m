//
//  FASLicenseAgreementTests.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/10/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FASLicenseAgreement.h"

@interface FASLicenseAgreementTests : XCTestCase

@end

@implementation FASLicenseAgreementTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testLicenseAgreementFromFileNil {
    NSString* filename = nil;
    FASLicenseAgreement* la = [FASLicenseAgreement licenseAgreementFromFile:filename];
    XCTAssertNil(la);
}

- (void)testLicenseAgreementFromFileDoesNotExist {
    NSString* filename = @"invalid_filename";
    FASLicenseAgreement* la = [FASLicenseAgreement licenseAgreementFromFile:filename];
    XCTAssertNil(la);
}

- (void)testLicenseAgreementFromFileInMainBundle {
    NSString* filename = @"LicenseAgreementData";
    FASLicenseAgreement* la = [FASLicenseAgreement licenseAgreementFromFile:filename];
    XCTAssertNotNil(la);
}

- (void)testLicenseAgreementFromFileInMainBundleWithExtension {
    NSString* filename = @"LicenseAgreementData.plist";
    FASLicenseAgreement* la = [FASLicenseAgreement licenseAgreementFromFile:filename];
    XCTAssertNotNil(la);
}

- (void)testLicenseAgreementFromConstFilename {
    NSString* filename = kFASLicenseAgreementDataDefaultFilename;
    FASLicenseAgreement* la = [FASLicenseAgreement licenseAgreementFromFile:filename];
    XCTAssertNotNil(la);
}

- (void)testDefaultLicenseAgreement {
    FASLicenseAgreement* la = [FASLicenseAgreement defaultLicenseAgreement];
    XCTAssertNotNil(la);
}

- (void)testDefaultLicenseAgreementHasExpectedData {
    FASLicenseAgreement* la = [FASLicenseAgreement defaultLicenseAgreement];
    XCTAssertEqual(la.version, 1);
    XCTAssertTrue([la.filename isEqualToString:@"FAS_License_v1.rtf"]);
    XCTAssertNotNil([la loadLicenseText]);
    XCTAssertNotNil([la loadLicenseAttributedText]);
}

- (void)testLoadTextPerformance {
    FASLicenseAgreement* la = [FASLicenseAgreement defaultLicenseAgreement];
    [self measureBlock:^{
        NSString *string = [la loadLicenseText];
        XCTAssertNotNil(string);
    }];
}

- (void)testLoadAttributedTextPerformance {
    FASLicenseAgreement* la = [FASLicenseAgreement defaultLicenseAgreement];
    [self measureBlock:^{
        NSAttributedString *string = [la loadLicenseAttributedText];
        XCTAssertNotNil(string);
    }];
}
@end
