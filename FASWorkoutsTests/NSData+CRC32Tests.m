//
//  NSData+CRC32Tests.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/23/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "NSData+CRC32.h"
#import <XCTest/XCTest.h>

@interface NSData_CRC32Tests : XCTestCase

@end

@implementation NSData_CRC32Tests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testNSDataCRC32
{
    // # Test data from Ruby/Zlib:
    //
    // require 'zlib'
    // Zlib::crc32('')              # => 0
    // Zlib::crc32('abc')           # => 891568578
    // Zlib::crc32('Hello, World!') # => 3964322768
    
    NSString *str;
    NSData *data;
    NSUInteger checksum, expected;
    
    str = @"";
    data = [str dataUsingEncoding:NSUTF8StringEncoding];
    checksum = [data crc32];
    expected = 0;
    XCTAssertEqual(checksum, expected, @"did not compute correct checksum for %@", str);
    
    str = @"abc";
    data = [str dataUsingEncoding:NSUTF8StringEncoding];
    checksum = [data crc32];
    expected = 891568578;
    XCTAssertEqual(checksum, expected, @"did not compute correct checksum for %@", str);
    
    str = @"Hello, World!";
    data = [str dataUsingEncoding:NSUTF8StringEncoding];
    checksum = [data crc32];
    expected = 3964322768;
    XCTAssertEqual(checksum, expected, @"did not compute correct checksum for %@", str);
}

@end
