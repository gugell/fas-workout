//
//  UIImage+MyExtensionsTests.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/15/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import XCTest
import UIKit

class UIImage_MyExtensionsTests: XCTestCase {

    let testImageName = "BackgroundWelcome"
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    // MARK: Making a screenshot from a view tests
    
    func testImageWithScreenshotFromView() {
        let view = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 10.0, height: 10.0))
        view.backgroundColor = UIColor.redColor()
        let screenshot = UIImage.imageWithScreenshotFromView(view)
        XCTAssertNotNil(screenshot)
    }
    
    func testImageWithScreenshotFromViewWithZeroFrame() {
        let view = UIView(frame: CGRect.zero)
        let screenshot = UIImage.imageWithScreenshotFromView(view)
        XCTAssertNotNil(screenshot)
    }

    // MARK: Stretchable images
    
    // before we can use an image for test this is a helper to determine 
    // that the image itself does exist im the bundle
    func testImageWithNameForTestsExists() {
        let originalImage = UIImage(named: testImageName)
        XCTAssertNotNil(originalImage, "Image for tests with name was not found")
    }
    
    func testStretchableImageWithFilename () {
        let insets = UIEdgeInsetsMake(2, 2, 2, 2)
        let stretchableImage = UIImage.stretchableImage(named: testImageName, inset: insets)
        XCTAssertNotNil(stretchableImage)
    }
    
    func testStretchableImageWithZeroInsets () {
        let insets = UIEdgeInsetsZero
        let stretchableImage = UIImage.stretchableImage(named: testImageName, inset: insets)
        XCTAssertNotNil(stretchableImage)
    }
    
    func testStretchableImageWithNegativeInsets () {
        let insets = UIEdgeInsetsMake(-1, -1, -1, -1)
        let stretchableImage = UIImage.stretchableImage(named: testImageName, inset: insets)
        XCTAssertNotNil(stretchableImage)
    }
    
    func testStretchableImageWithNonExistedFilename() {
        // using arbitrary UUID so it is nearly imposible to have such an image somewhere
        let unexistedImageName = "20621066-1859-4DA3-A113-FFCB21DA499F"
        let insets = UIEdgeInsetsMake(2, 2, 2, 2)
        let stretchableImage = UIImage.stretchableImage(named: unexistedImageName, inset: insets)
        XCTAssertNil(stretchableImage, "Must be nil for non-existed image")
    }
    
    
}
