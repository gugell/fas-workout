//
//  FASTimerControllerCountdownTimerTests.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/13/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FASTimerController.h"

static int howManyTimesDidCountToDateDelegateIsCalled = 0;

@interface FASTimerControllerCountdownTimerTests : XCTestCase <FASTimerControllerDelegate>

@end

@implementation FASTimerControllerCountdownTimerTests {
    XCTestExpectation* _countdownTimerDidFinishCountdownWithTimeExpectation;
    
    XCTestExpectation* _countdownTimerDidCountToTimeExpectation01;
    XCTestExpectation* _countdownTimerDidCountToTimeExpectation02;
    XCTestExpectation* _countdownTimerDidCountToTimeExpectation03;
    
    XCTestExpectation* _countdownTimerDidCountToTimeHighUseExpectation01;
    XCTestExpectation* _countdownTimerDidCountToTimeHighUseExpectation02;
    XCTestExpectation* _countdownTimerDidCountToTimeHighUseExpectation03;
    
    XCTestExpectation* _countdownTimerDidCountToDateExpectation;
    
    XCTestExpectation* _countdownTimerDidCountToDateAndResetExpectation;
    
    int howManyTimesDidCountToTimeDelegateIsCalled;
}

- (void)setUp {
    [super setUp];

    howManyTimesDidCountToTimeDelegateIsCalled = 0;
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

///////////////////////////////////////////////
// DID FINISH COUNTDOWN tests
///////////////////////////////////////////////

// The test covers a bug when userTime to zero results in callback block to never be called
- (void)testCountdownTimerStartWithEndingBlockUserTimeEqualsToZero {
    // Note: userTime == FASTimerControllerModeDefault, because this ensures only one-time delegate/block call
    FASTimerController* timerCtrl = [[FASTimerController alloc] initWithType:FASTimerControllerTypeCountdownTimer
                                                                    userTime:0
                                                                        mode:FASTimerControllerModeDefault];
    XCTestExpectation *timerExpectation = [self expectationWithDescription:@"timer did count"];
    
    [timerCtrl startWithEndingBlock:^(NSTimeInterval countTime) {
        XCTAssertEqual(countTime, timerCtrl.userTime);
        [timerExpectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:5 handler:^(NSError *error) {
        if (error) {
            NSLog(@"timeout error:\n%@", error);
        }
        
        [timerCtrl stop];
    }];
}

// Test the delegate ending block callback
- (void)testCountdownTimerStartWithEndingBlock {
    // Note: userTime == FASTimerControllerModeDefault, because this ensures only one-time delegate/block call
    FASTimerController* timerCtrl = [[FASTimerController alloc] initWithType:FASTimerControllerTypeCountdownTimer
                                                                    userTime:0.1
                                                                        mode:FASTimerControllerModeDefault];
    XCTestExpectation *timerExpectation = [self expectationWithDescription:@"timer did count"];
    
    [timerCtrl startWithEndingBlock:^(NSTimeInterval countTime) {
        XCTAssertEqual(countTime, timerCtrl.userTime);
        [timerExpectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:5 handler:^(NSError *error) {
        if (error) {
            NSLog(@"timeout error:\n%@", error);
        }
        
        [timerCtrl stop];
    }];
}

// Test the did finish delegate for countdown timer
- (void)testCountdownTimerDidFinishCountdownWithTime {
    FASTimerController* timerCtrl = [[FASTimerController alloc] initWithType:FASTimerControllerTypeCountdownTimer
                                                                    userTime:4.0
                                                                        mode:FASTimerControllerModeDefault];
    _countdownTimerDidFinishCountdownWithTimeExpectation = [self expectationWithDescription:@"countdown did finish"];
    
    [timerCtrl setDelegate:self];
    [timerCtrl start];
    
    [self waitForExpectationsWithTimeout:5 handler:^(NSError *error) {
        if (error) {
            NSLog(@"timeout error:\n%@", error);
        }
        
        [timerCtrl stop];
    }];
}

///////////////////////////////////////////////
// DID COUNT TO TIME tests
///////////////////////////////////////////////

// Complex did finish delegate for countdown timer test w/ checking intermediate results
- (void)testCountdownTimerDidCountToTime {
    FASTimerController* timerCtrl = [[FASTimerController alloc] initWithType:FASTimerControllerTypeCountdownTimer
                                                                    userTime:3.0
                                                                        mode:FASTimerControllerModeDefault];
    _countdownTimerDidCountToTimeExpectation01 = [self expectationWithDescription:@"countdown did cout to time INITIAL"];
    _countdownTimerDidCountToTimeExpectation02 = [self expectationWithDescription:@"countdown did cout to time INTERMEDIATE"];
    _countdownTimerDidCountToTimeExpectation03 = [self expectationWithDescription:@"countdown did cout to time FINAL"];
    
    [timerCtrl setDelegate:self];
    [timerCtrl start];
    
    [self waitForExpectationsWithTimeout:5 handler:^(NSError *error) {
        if (error) {
            NSLog(@"timeout error:\n%@", error);
        }
        
        [timerCtrl stop];
    }];
}

// Test has the semantic similar to the above, but use high frequency (FASTimerControllerModeHighUse)
- (void)testCountdownTimerDidCountToTimeHighUse {
    FASTimerController* timerCtrl = [[FASTimerController alloc] initWithType:FASTimerControllerTypeCountdownTimer
                                                                    userTime:1.3
                                                                        mode:FASTimerControllerModeHighUse];
    _countdownTimerDidCountToTimeHighUseExpectation01 = [self expectationWithDescription:@"countdown did cout to time INITIAL"];
    _countdownTimerDidCountToTimeHighUseExpectation02 = [self expectationWithDescription:@"countdown did cout to time INTERMEDIATE"];
    _countdownTimerDidCountToTimeHighUseExpectation03 = [self expectationWithDescription:@"countdown did cout to time FINAL"];
    
    [timerCtrl setDelegate:self];
    [timerCtrl start];
    
    [self waitForExpectationsWithTimeout:5 handler:^(NSError *error) {
        if (error) {
            NSLog(@"timeout error:\n%@", error);
        }
        
        [timerCtrl stop];
    }];
}

///////////////////////////////////////////////
// DID COUNT TO DATE tests
///////////////////////////////////////////////
- (void)testCountdownTimerDidCountToDate {
    FASTimerController* timerCtrl = [[FASTimerController alloc] initWithType:FASTimerControllerTypeCountdownTimer
                                                                    userTime:7.0
                                                                        mode:FASTimerControllerModeDefault];
    
    _countdownTimerDidCountToDateExpectation = [self expectationWithDescription:@"countdown did cout to date"];
    
    [timerCtrl setDelegate:self];
    [timerCtrl start];
    
    [self waitForExpectationsWithTimeout:10 handler:^(NSError *error) {
        if (error) {
            NSLog(@"timeout error:\n%@", error);
        }
        
        [timerCtrl stop];
    }];

}

- (void)testCountdownTimerDidCountToDateAndReset {
    FASTimerController* timerCtrl = [[FASTimerController alloc] initWithType:FASTimerControllerTypeCountdownTimer
                                                                    userTime:7.0
                                                                        mode:FASTimerControllerModeDefault];
    
    _countdownTimerDidCountToDateAndResetExpectation = [self expectationWithDescription:@"countdown did cout to date and reset"];
    
    timerCtrl.resetTimerAfterFinish = YES;
    
    [timerCtrl setDelegate:self];
    [timerCtrl start];
    
    [self waitForExpectationsWithTimeout:10 handler:^(NSError *error) {
        if (error) {
            NSLog(@"timeout error:\n%@", error);
        }
        
        [timerCtrl stop];
    }];
}
///////////////////////////////////////////////
#pragma mark - FASTimerControllerDelegate
///////////////////////////////////////////////
- (void)timerController:(FASTimerController *)timerController didFinishCountdownWithTime:(NSTimeInterval)time
{
    if (_countdownTimerDidFinishCountdownWithTimeExpectation) {
        XCTAssertEqual(time, timerController.userTime);
        XCTAssertEqual([timerController getTimeCounted], 0);
        XCTAssertEqual([timerController getTimeRemaining], 4.0); // use const here just in case :)
        
        [_countdownTimerDidFinishCountdownWithTimeExpectation fulfill];
    }
}

- (void)timerController:(FASTimerController *)timerController didCountToTime:(NSTimeInterval)time
{
    // running 'testCountdownTimerDidCountToTime'
    if (_countdownTimerDidCountToTimeExpectation01 &&
        _countdownTimerDidCountToTimeExpectation02 &&
        _countdownTimerDidCountToTimeExpectation03) {
        
        if (howManyTimesDidCountToTimeDelegateIsCalled == 0) {
            XCTAssertEqualWithAccuracy(time, 3.0, 0.01);
            XCTAssertEqualWithAccuracy([timerController getTimeCounted], 0, 0.01);
            XCTAssertEqualWithAccuracy([timerController getTimeRemaining], 3.0, 0.01);
            
            [_countdownTimerDidCountToTimeExpectation01 fulfill];
        }
        else if (howManyTimesDidCountToTimeDelegateIsCalled == 14) {
            XCTAssertEqualWithAccuracy(time, 1.6, 0.01);
            XCTAssertEqualWithAccuracy([timerController getTimeCounted], 1.4, 0.01);
            XCTAssertEqualWithAccuracy([timerController getTimeRemaining], 1.6, 0.01);
            
            [_countdownTimerDidCountToTimeExpectation02 fulfill];
        }
        else if (howManyTimesDidCountToTimeDelegateIsCalled == 30) {
            XCTAssertEqualWithAccuracy(time, 0, 0.01);
            XCTAssertEqualWithAccuracy([timerController getTimeCounted], 3, 0.01);
            XCTAssertEqualWithAccuracy([timerController getTimeRemaining], 0, 0.01);
            
            [_countdownTimerDidCountToTimeExpectation03 fulfill];
        }
    }
    
    // running 'testCountdownTimerDidCountToTimeHighUse'
    else if (_countdownTimerDidCountToTimeHighUseExpectation01 &&
             _countdownTimerDidCountToTimeHighUseExpectation02 &&
             _countdownTimerDidCountToTimeHighUseExpectation03) {
        
        if (howManyTimesDidCountToTimeDelegateIsCalled == 0) {
            XCTAssertEqualWithAccuracy(time, 1.3, 0.001);
            XCTAssertEqualWithAccuracy([timerController getTimeCounted], 0, 0.001);
            XCTAssertEqualWithAccuracy([timerController getTimeRemaining], 1.3, 0.001);
            
            [_countdownTimerDidCountToTimeHighUseExpectation01 fulfill];
        }
        else if (howManyTimesDidCountToTimeDelegateIsCalled == 26) {
            XCTAssertEqualWithAccuracy(time, 0.78, 0.01);
            XCTAssertEqualWithAccuracy([timerController getTimeCounted], 0.52, 0.01);
            XCTAssertEqualWithAccuracy([timerController getTimeRemaining], 0.78, 0.01);
            
            [_countdownTimerDidCountToTimeHighUseExpectation02 fulfill];
        }
        else if (howManyTimesDidCountToTimeDelegateIsCalled == 65) {
            XCTAssertEqualWithAccuracy(time, 0, 0.01);
            XCTAssertEqualWithAccuracy([timerController getTimeCounted], 1.3, 0.01);
            XCTAssertEqualWithAccuracy([timerController getTimeRemaining], 0, 0.01);
            
            [_countdownTimerDidCountToTimeHighUseExpectation03 fulfill];
        }
    }
    
    ++howManyTimesDidCountToTimeDelegateIsCalled;
}

- (void)timerController:(FASTimerController *)timerController didCountToDate:(NSDate *)date
{
    if (_countdownTimerDidCountToDateExpectation) {
        
        // resetTimerAfterFinish == NO
        if (howManyTimesDidCountToDateDelegateIsCalled == 69) {
            
            [_countdownTimerDidCountToDateExpectation fulfill];
        }
        else if (howManyTimesDidCountToDateDelegateIsCalled == 45) {
            // calendar to be used for comparison
            NSCalendar* cal = [NSCalendar currentCalendar];
            [cal setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
            
            // create date to test w/
            NSDateComponents *comps = [[NSDateComponents alloc] init];
            [comps setHour:0];
            [comps setMinute:0];
            [comps setSecond:2];
//            [comps setNanosecond:500000000];
            NSDate* testTime = [cal dateFromComponents:comps];
            
            // extract the components from timer controller's date
            NSDateComponents* componentsToCompareBy = [cal components: NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond /*| NSCalendarUnitNanosecond*/ fromDate:date];
            NSDate* dateToCompare = [cal dateFromComponents:componentsToCompareBy];
            
            // TODO: compare w/ nanoseconds
            
            XCTAssertEqual([testTime compare:dateToCompare], NSOrderedSame);
        }
    }
    
    if (_countdownTimerDidCountToDateAndResetExpectation) {
        
        // resetTimerAfterFinish == YES
        if (howManyTimesDidCountToDateDelegateIsCalled == 70) {
            [_countdownTimerDidCountToDateAndResetExpectation fulfill];
        }
    }
    
    ++howManyTimesDidCountToDateDelegateIsCalled;
}

@end
