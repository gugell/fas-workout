//
//  FASAPITests.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/28/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FASAPI.h"

/// This test case incorporates core FASAPI functionality
/// which ensures the internal flow is correct and expected

@interface FASAPITests : XCTestCase

@end

@implementation FASAPITests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

@end
