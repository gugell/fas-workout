//
//  UBIFileLoaderTests.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/22/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <Foundation/Foundation.h>

#import "UBIFileLoader.h"
#import "XJSafeMutableDictionary.h"

@interface UBIFileLoader (UBIFileLoaderTests)
@property (nonatomic, readonly) XJSafeMutableDictionary * loaderInfo;
@property (nonatomic, readonly) NSURLSession * backgroundSession;
@property (nonatomic, copy, readonly) NSString* loaderInfoKeyPrefix;
@end

@interface UBIFileLoaderBasicTests : XCTestCase

@end

@implementation UBIFileLoaderBasicTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testInit {
    UBIFileLoader* fileLoader = [[UBIFileLoader alloc] init];
    XCTAssertEqual(fileLoader.allowsCellularAccess, NO);
    XCTAssertNil(fileLoader.authorizationToken);
    XCTAssertNotNil(fileLoader.backgroundSession);
    XCTAssertNotNil(fileLoader.loaderInfo);
    XCTAssertEqual(fileLoader.loaderInfoKeyPrefix, @"task_id");
}

- (void)testInitWithAuthorizationToken {
    UBIFileLoader* fileLoader = [[UBIFileLoader alloc] initWithAuthorizationToken:@"test token" allowsCellularAccess:NO];
    XCTAssertEqual(fileLoader.allowsCellularAccess, NO);
    XCTAssertNotNil(fileLoader.backgroundSession);
    XCTAssertNotNil(fileLoader.loaderInfo);
    XCTAssertEqual(fileLoader.loaderInfoKeyPrefix, @"task_id");
    XCTAssertEqual(fileLoader.authorizationToken, @"test token");
}

@end
