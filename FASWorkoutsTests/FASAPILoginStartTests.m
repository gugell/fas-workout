//
//  FASAPILoginNewTests.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 6/1/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FASAPI.h"

@interface FASAPILoginStartTests : XCTestCase <FASAPIDelegate> {
    @private
    XCTestExpectation* _didFinishExpectation;
    XCTestExpectation* _willInvokeExpectation;
    XCTestExpectation* _didInvokeExpectation;
}

@property (nonatomic, strong) FASAPI* theAPI;
@end

@interface FASAPILoginStartTests (UtilMethods)
- (BOOL)FASAPIhasExpectedReponse:(FASAPI*)api;
@end

@implementation FASAPILoginStartTests

- (void)setUp {
    [super setUp];
    
    _theAPI = [FASAPI loginStartWithUsername:@"admin" delegate:self];
}

- (void)tearDown {
    [_theAPI cancel];
    _theAPI = nil;
    
    [super tearDown];
}

- (void)testInit
{
    XCTAssertNotNil(self.theAPI);
}

- (void)testMethod
{
    XCTAssertEqual(self.theAPI.method, FASAPIMethodLoginStart);
}

- (void)testInvoke
{
    _willInvokeExpectation = [self expectationWithDescription:@"API will invoke"];
    _didInvokeExpectation = [self expectationWithDescription:@"API did invoke"];
    _didFinishExpectation = [self expectationWithDescription:@"API did finish"];
    
    [self.theAPI invoke];
    XCTAssertTrue(self.theAPI.invoked);
    
    [self waitForExpectationsWithTimeout:5 handler:^(NSError *error) {
        if (error) {
            NSLog(@"timeout error:\n%@", error);
        }
        
        XCTAssertTrue([self FASAPIhasExpectedReponse:self.theAPI]);
        
        [self.theAPI cancel];
    }];
}
@end
///////////////////////////////////////////////
#pragma mark - Util methods
///////////////////////////////////////////////
@implementation FASAPILoginStartTests (UtilMethods)
- (BOOL)FASAPIhasExpectedReponse:(FASAPI*)api
{
    XCTAssertNotNil(api.response, @"response should not be nil");
    XCTAssertTrue([api.response isKindOfClass:[NSDictionary class]], @"unexpected type of response");
    if ([api.response objectForKey:@"result"]) {
        return YES;
    }
    return NO;
}
@end
///////////////////////////////////////////////
#pragma mark - FASAPIDelegate methods
///////////////////////////////////////////////
@implementation FASAPILoginStartTests (FASAPIDelegate)

- (void)FASAPIwillInvoke:(FASAPI *)theFASAPI
{
    [_willInvokeExpectation fulfill];
}

- (void)FASAPIdidInvoke:(FASAPI *)theFASAPI
{
    [_didInvokeExpectation fulfill];
}

- (void)FASAPIdidFinish:(FASAPI *)theFASAPI
{
    [_didFinishExpectation fulfill];
}

- (void)FASAPIdidFail:(FASAPI *)theFASAPI
{
    XCTAssertFalse(YES, @"API did fail");
}

@end
