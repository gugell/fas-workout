FASWorkouts for iOS
===

Pre-requisites
---

* Xcode 7.0 (starting from September, 25)
* Swift 2.0 (starting from September, 25)
* git
* [CocoaPods](https://http://cocoapods.org) v0.38.2 (starting from September, 24). [Read more on Wiki](https://bitbucket.org/fasworkouts/fas-workouts-ios/wiki/CocoaPods).
 
Opening the project
---
* clone the repo
* make sure you have CocoaPods installed, and run `pod install`
* open FASWorkouts.xcworkspace

Branches
---

The current development tip is the `master` branch.

Typically, development branches / forks are not pushed to the repository but they can be if necessary. Once a development branch has been merged into master and/or discarded it should be deleted from the repository.