#import <UIKit/UIKit.h>

#import "PAPreferences.h"
#import "PAPropertyDescriptor.h"

FOUNDATION_EXPORT double PAPreferencesVersionNumber;
FOUNDATION_EXPORT const unsigned char PAPreferencesVersionString[];

