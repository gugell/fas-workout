#!/bin/bash

# README:
# The script aims to help bumping build version
# before creating an AdHoc or other builds
# The file should sit inside ../FASWorkouts-iOS/scripts dir
# in order to run properly

echo "Bump FAS Version - START"
BASEDIR=$(dirname $0)
cd $BASEDIR
cd ..
BASEDIR=$(pwd)
if [ ! -f $BASEDIR/FASWorkouts/FASWorkouts-Info.plist ]
then
	echo "[ERROR] script must be executed from ../FASWorkouts-iOS/scripts directory"
	echo $BASEDIR
	exit 1
else
	agvtool what-version

	read -p "Do you want to bump build version? [Yy] " -n 1 -r
	echo
	if [[ $REPLY =~ ^[Yy]$ ]]
	then
    	agvtool next-version -all
	fi
fi
echo "Bump FAS Version - FINISH"