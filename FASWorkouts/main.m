//
//  main.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/14/14.
//  Copyright (c) 2014 Dubinin Evgeniy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FASAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FASAppDelegate class]));
    }
}
