//
//  AppDelegate.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/14/14.
//  Copyright (c) 2014 Dubinin Evgeniy. All rights reserved.
//

#import <UIKit/UIKit.h>

// notifications
extern NSString * const FASAppDelegateDidRegisterUserNotificationSettingsNotification;

@interface FASAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (copy) void (^backgroundSessionCompletionHandler)();

@property (nonatomic, strong) NSDictionary *launchOptions;
@property (nonatomic) BOOL isNewInstall;
@property (nonatomic) BOOL isUpgradeInstall;

- (void)serverReportsAPNSTokens:(NSArray *)APNSTokens;

@property (nonatomic, readonly, copy) NSString *APNSToken;

// when UI is ready, it should call app delegate to remove splash
- (void)removeSplashView;

- (void)setupViewControllers;

@end

