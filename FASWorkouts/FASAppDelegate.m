//
//  AppDelegate.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/14/14.
//  Copyright (c) 2014 Dubinin Evgeniy. All rights reserved.
//

#import "FASAppDelegate.h"
#import "FASData.h"
#import "FASWorkouts-Swift.h"
#import "FASAPISession.h"
#import "UBIAttachFileCache.h"
// logging
#import "UBILogger.h"
// In-App
@import StoreKit;
#import "StoreObserver.h"

// Facebook SDK
#import <FBSDKCoreKit/FBSDKCoreKit.h>

// detailed NSError info
#import "NSError+MyExtensions.h"

// simulator or device define
#import <TargetConditionals.h>

// GAI
#import <Google/Analytics.h>

// Crash reporting
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

// supporting various data migration tools
#import "FASData+DataMigration.h"

// Apptentive SDK
#import "ATConnect.h"

// background time interval thresholds
#ifdef DEBUG
#define     kFASAppDelegateBackgroundIntervalToRefreshSession    5
#define     kFASAppDelegateBackgroundIntervalToTreatLikeLaunch   10
NSString * const FASAppDelegateBuildConfig = @"DEBUG";
#else
#define     kFASAppDelegateBackgroundIntervalToRefreshSession    300     // 5 min
#define     kFASAppDelegateBackgroundIntervalToTreatLikeLaunch   1800    // 30 minutes
NSString * const FASAppDelegateBuildConfig = @"RELEASE";
#endif

// notification keys
NSString * const FASAppDelegateDidRegisterUserNotificationSettingsNotification = @"FASAppDelegateDidRegisterUserNotificationSettings";

@interface FASAppDelegate ()
<UISplitViewControllerDelegate,
FASAPIDelegate>

// YES = the app was activated from launch
// NO = the app was activated from inactive or background state
@property (nonatomic) BOOL activatedFromLaunch;
@property (nonatomic, strong) NSURL *launchURL;
@property (nonatomic, strong) NSDictionary *notificationInfo;

// timestamp when the app enters the background
@property (nonatomic, strong) NSDate *backgroundDate;

@property (nonatomic, copy) NSString *APNSToken;
@property (nonatomic, strong) UIViewController *modalAlertController;
@property (nonatomic, strong) UIView* splashView;
@property (nonatomic, strong) UISplitViewController* splitViewController;
@end

@implementation FASAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Crashlytics setup
    // Note: see Crashlytics Run Script build phase to find out when to send crashes
    [Fabric with:@[[Crashlytics class]]];
    
    // Apptentive API key
    [ATConnect sharedConnection].apiKey = [FASEnvironment ApptentiveAPIKey];
    
#if TARGET_IPHONE_SIMULATOR
    // helper to find out the app's sandbox directory
    UBILogTrace(@"Documents Directory: %@", [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
#endif
    
    self.launchOptions = launchOptions;
    self.activatedFromLaunch = YES;
    
    self.isNewInstall = NO;
    self.isUpgradeInstall = NO;
    
    self.backgroundDate = nil;
    
    _APNSToken = nil;
    
    // set default formatters behavior
    [NSDateFormatter setDefaultFormatterBehavior:NSDateFormatterBehavior10_4];
    [NSNumberFormatter setDefaultFormatterBehavior:NSNumberFormatterBehavior10_4];
    
    [FASData.sharedData restore];
    
    // observe the payment queue
    [[SKPaymentQueue defaultQueue] addTransactionObserver:[StoreObserver sharedInstance]];
    
    [self checkForUpdateVersion];
    
    // register defaults
    NSUserDefaults *defaults = NSUserDefaults.standardUserDefaults;
    NSDictionary *defaultSettings = @{
                                      FASDataSkipWelcomeScreenPrefKey : @NO
                                      };
    [defaults registerDefaults:defaultSettings];
    
    [self logBootState];
    
    // GAI: Configure tracker from GoogleService-Info.plist.
    NSError *configureError;
    [[GGLContext sharedInstance] configureWithError:&configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    // GAI Configure default tracking ID
    GAI *gai = [GAI sharedInstance];
    id newTracker = [gai trackerWithTrackingId:[FASEnvironment GoogleAnalyticsPropertyID]];
    [GAI sharedInstance].defaultTracker = newTracker;
    // GAI Configure options.
    gai.trackUncaughtExceptions = NO;
   
    // uncomment when you need GAI logging during development
//#ifdef DEBUG
//    gai.logger.logLevel = kGAILogLevelVerbose;
//#endif
    
    // Track
    // TODO: this is a quickly made checking just for analytics tracking
    UILocalNotification *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (notification) {
        [FASAnalyticsEventHelper trackAppDidLaunchFromRemoteNotification];
    }
    else {
        [FASAnalyticsEventHelper trackAppDidLaunch];
    }
    
#ifdef DEBUG
    [FBSDKSettings setLoggingBehavior:[NSSet setWithObject:FBSDKLoggingBehaviorGraphAPIDebugInfo]];
#endif
    
    if (launchOptions[@"UIApplicationLaunchOptionsURLSessionKey"]) {
        NSString* backgroundSessionIdentifier = [launchOptions objectForKey:@"UIApplicationLaunchOptionsURLSessionKey"];
        UBILogTrace(@"Application was launched w/ background NSURLSession identifier:\n%@", backgroundSessionIdentifier);
    }
    
    [Attach fileCache];
    
    [self setupViewControllers];
    
    [self addSplashView];
    
    [FASAPISession.currentSession restore];
    
    // defer remaining initialization to next run loop
    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self loadData];
    });
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString* strUserID = [FASAPISession currentSession].userInfo.stringUserID;
        if (strUserID) {
            [[ATConnect sharedConnection] addCustomPersonData:strUserID withKey:@"userID"];
        }
        
        NSString* email = [FASAPISession currentSession].userInfo.email;
        if ( email ) {
            [[ATConnect sharedConnection] addCustomPersonData:email withKey:@"email"];
        }
    });
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    UBILogTrace(@"");
    [[Crashlytics sharedInstance] setObjectValue:@"inactive" forKey:@"app_state"];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    UBILogTrace(@"");
    [[Crashlytics sharedInstance] setObjectValue:@"background" forKey:@"app_state"];
    self.backgroundDate = [NSDate date];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    UBILogTrace(@"");
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    UBILogTrace(@"");
    [[Crashlytics sharedInstance] setObjectValue:@"active" forKey:@"app_state"];
    [FBSDKAppEvents activateApp];
    
    // register/re-register for current user-facing notification types  & remote notifications
#if !TARGET_IPHONE_SIMULATOR
    // IF user allowed any user-facing notification types,
    // THEN re-register for required types (All)
    // AND conditionally register for APNS
    if ([[UIApplication sharedApplication] isAnyUserFacingNotificationTypesAllowed]) {
        // if other than None, registering makes sense
        UIUserNotificationType requiredType = [FASData.sharedData requiredNotificationType];
        [[UIApplication sharedApplication] registerUserFacingNotificationTypes:requiredType];
        // Register for APNS
        [self registerForAPNSandForceUpdate:NO];
    }
#endif
    
    // TEST: mock APNS notification
//    self.notificationInfo = [self mockNotificationInfo];
    
    // IF activateFromLaunch, clear so it's not handled again and do nothing
    // Remaining logic is handled in 'continueLaunchingAfterResumeFromCrash:'
    if (self.activatedFromLaunch) {
        self.activatedFromLaunch = NO;
        return;
    }
    
    // ELSE IF backgroundDate != nil then the app became active after
    // returning to foreground
    else if (self.backgroundDate) {
        NSTimeInterval secondsElapsedInBackground = -[self.backgroundDate timeIntervalSinceNow];
        // clear the backgroundDate so we don't handle it again unexpectedly
        self.backgroundDate = nil;
        
        // IF app is in background > min interval
        // AND not activated from a launch
        // THEN update the session data
        if (secondsElapsedInBackground > kFASAppDelegateBackgroundIntervalToRefreshSession) {
            [FASData.sharedData updateSessionData];
        }
        
        // IF we have a launchURL
        // THEN handle it
        if (self.launchURL) {
            (void)[self handleURL:nil];
        }
        
        // ELSE IF we have a notification
        // OR app was in background > max interval
        // OR app badge is different then last known count of unseen notifications
        // THEN
        else if (self.notificationInfo != nil ||
                 secondsElapsedInBackground > kFASAppDelegateBackgroundIntervalToTreatLikeLaunch /*||
                 appBadgeIsDifferentThanUnseenNotifications*/)
        {
            [self loadData];
        }
    }
    
    // ELSE
    // note: this means the app was inactive but not in the background
    // this can happen if the users answers a phone call while using the app
    else {
        // IF have launchURL
        // THEN handle it
        if (self.launchURL) {
            (void)[self handleURL:nil];
        }
        
        // IF have notificationInfo
        // THEN handle the notification
        else if (self.notificationInfo) {
            [self handleRemoteNotification:self.notificationInfo loadImmediately:NO];
            self.notificationInfo = nil;
        }
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    UBILogTrace(@"");
    
    // Remove the observer
    [[SKPaymentQueue defaultQueue] removeTransactionObserver: [StoreObserver sharedInstance]];
}

- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler
{
    UBILogTrace(@"");
    
    /*
     Store the completion handler. The completion handler is invoked by the view controller's checkForAllDownloadsHavingCompleted method (if all the download tasks have been completed).
     */
    self.backgroundSessionCompletionHandler = completionHandler;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
//    return [FBAppCall handleOpenURL:url
//                  sourceApplication:sourceApplication
//                    fallbackHandler:^(FBAppCall *call) {
//                        if ([call.dialogData.method isEqualToString:@"appinvites"]) {
//                            // handle response
//                        }
//                    }];
    
    self.launchURL = url;
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];
    if (application) { [userInfo setObject:application forKey:@"application"]; }
    if (sourceApplication) { [userInfo setObject:sourceApplication forKey:@"sourceApplication"]; }
    if (annotation) { [userInfo setObject:annotation forKey:@"annotation"]; }
    
    return [self handleURL:[userInfo copy]];
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    UIUserNotificationType types = notificationSettings.types;
    NSMutableString* notificationPermissionsString  = [NSMutableString string];
    
    if (UIUserNotificationTypeNone != types) {
        [notificationPermissionsString appendString:@"\nALLOWED "];
        
        // determine types
        BOOL hasAtLeastOneType = NO;
        
        if (UIUserNotificationTypeBadge & types) {
            [notificationPermissionsString appendString:@"badge"];
            hasAtLeastOneType = YES;
        }
        
        if (UIUserNotificationTypeSound & types) {
            if (hasAtLeastOneType) {
                [notificationPermissionsString appendString:@","];
            }
            [notificationPermissionsString appendString:@"sound"];
            hasAtLeastOneType = YES;
        }
        
        if (UIUserNotificationTypeAlert & types) {
            if (hasAtLeastOneType) {
                [notificationPermissionsString appendString:@","];
            }
            [notificationPermissionsString appendString:@"alert"];
        }
    }
    else {
        [notificationPermissionsString appendString:@"\nDENIED"];
    }
    UBILogTrace(@"did register user notification: %@", notificationPermissionsString);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:FASAppDelegateDidRegisterUserNotificationSettingsNotification
                                                        object:self];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    UBILogTrace(@"devToken = %@", deviceToken);
    
    NSCharacterSet *charSet = [NSCharacterSet characterSetWithCharactersInString:@"<>"];
    NSString *token = [[deviceToken description]
                       stringByTrimmingCharactersInSet:charSet];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (![self.APNSToken isEqualToString:token]) {
        self.APNSToken = token;
        
        // TODO: ommit needless token updates with the server, if token has been already submitted
        [self submitCurrentAPNSToken];
    }
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSString *action = [NSString stringWithFormat:@"APNS Error: %@",
                        [error detailedInfo]];
    UBILogError(@"%@", action);
    // TODO: send it as a stat event, so we know the scale of occurrence
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    UBILogTrace(@"userInfo:\n\t%@", userInfo);
    
    // if the app is inactive then save the notification info for later
    if (UIApplication.sharedApplication.applicationState == UIApplicationStateInactive) {
        self.notificationInfo = userInfo;
    }
    // else handle the notification now
    else {
        [self handleRemoteNotification:userInfo loadImmediately:NO];
    }
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    if(application.applicationState == UIApplicationStateActive ) {
        // alert, because app is running in foreground
        UBILogTrace(@"notification:\n\t%@", notification);
    }
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Push Token Methods
#pragma mark -
///////////////////////////////////////////////

- (void)registerForAPNSandForceUpdate:(BOOL)force
{
    if (force) {
        self.APNSToken = nil;
    }
    
    if (self.APNSToken == nil) {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

- (void)serverReportsAPNSTokens:(NSArray *)APNSTokens
{
    UBILogTrace(@"reported tokens:\n%@", APNSTokens);
    
    // if the app doesn't have a token yet do nothing
    // this means either APNS is unauthorized or the app
    // is waiting for iOS to return a token
    if (!self.APNSToken || [self.APNSToken isBlank]) {
        return;
    }
    
    BOOL isReported = NO;
    for (NSString* APNSToken in APNSTokens) {
        NSString *serverAPNSToken =  [APNSToken
                                      stringByReplacingOccurrencesOfString:@" "
                                      withString:@""];
        if ([serverAPNSToken isEqualToString:self.APNSToken]) {
            isReported = YES;
            break;
        }
    }
    
    // if no APNStoken then submit current
    if (!isReported) {
        UBILogWarning(@"Server needs APNS token: %@", self.APNSToken);
        [self submitCurrentAPNSToken];
    }
}

- (void)submitCurrentAPNSToken
{
    if (!self.APNSToken) {
        return;
    }
    if ([[FASAPISession currentSession] isLoggedInAsUser]) {
        UBILogTrace(@"submit token to server");
        // send APNS token to the server
        FASAPI *api = [FASAPI registerDeviceToken:self.APNSToken
                                         delegate:self];
        [api invoke];
    }
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark UISplitViewControllerDelegate
#pragma mark -
///////////////////////////////////////////////
- (BOOL)splitViewController:(UISplitViewController *)splitViewController collapseSecondaryViewController:(UIViewController *)secondaryViewController ontoPrimaryViewController:(UIViewController *)primaryViewController {
    if ([secondaryViewController isKindOfClass:[UINavigationController class]] && [[(UINavigationController *)secondaryViewController topViewController] isKindOfClass:[WorkoutDetailViewController class]] && ([(WorkoutDetailViewController *)[(UINavigationController *)secondaryViewController topViewController] detailWorkout] == nil)) {
        // Return YES to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
        return YES;
    } else {
        return NO;
    }
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Helpers
#pragma mark -
///////////////////////////////////////////////
- (void)loadData
{
    UBILogTrace(@"");
    
//#if DEBUG
//    id locNotificationObject = [self.launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
//#endif
    
    // get notificationInfo from launchOptions
    // if we already have notificationInfo, it came from app:didReceiveRemoteNotification:
    if (!self.notificationInfo) {
        self.notificationInfo = [self.launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    }
    
    // get launchURL from launchOptions
    // if we already have launchURL, it came from application:openURL:sourceApplication:annotation:
    NSDictionary* launchURLUserInfo = nil;
    if (!self.launchURL && self.launchOptions) {
        self.launchURL = [self.launchOptions objectForKey:UIApplicationLaunchOptionsURLKey];
        NSString* sourceApplication = [self.launchOptions objectForKey:UIApplicationLaunchOptionsSourceApplicationKey];
        if (sourceApplication) {
            launchURLUserInfo = @{@"sourceApplication": sourceApplication};
        }
    }
    
    self.launchOptions = nil;
    
    if (self.notificationInfo) {
        [self handleRemoteNotification:self.notificationInfo loadImmediately:YES];
        self.notificationInfo = nil;
    }
    
    if (self.launchURL) {
        (void)[self handleURL:launchURLUserInfo];
    }
    
    else {
        // dismiss modal sighting view if necessary
        if (self.modalAlertController) {
            [self.modalAlertController dismissViewControllerAnimated:YES completion:nil];
            self.modalAlertController = nil;
        }
        
        // TODO: submit workout modify date request
    }
}

- (BOOL)handleURL:(NSDictionary *)userInfo
{
    UBILogTrace(@"URL = %@", self.launchURL);
    BOOL result = YES;
    if (self.launchURL) {
        // starsightings:// url
        if ([[self.launchURL scheme] isEqualToString:@"fasworkout"]) {
            // TODO: uncomment when ready to handle applink
            // TODO: #import <Bolts/Bolts.h>
//            BFURL *parsedUrl = [BFURL URLWithInboundURL:self.launchURL sourceApplication:userInfo[@"sourceApplication"]];
//            if ([parsedUrl appLinkData]) {
                // this is an applink url, handle it here
//                NSURL *targetUrl = [parsedUrl targetURL];
//                [[[UIAlertView alloc] initWithTitle:@"Received link:"
//                                            message:[targetUrl absoluteString]
//                                           delegate:nil
//                                  cancelButtonTitle:@"OK"
//                                  otherButtonTitles:nil] show];
//            }
//            [FASData.sharedData openURL:self.launchURL];
        }

        else if ([[self.launchURL scheme] isEqualToString:@"fb978386405535500"]) {
            if (userInfo) {
                result = [[FBSDKApplicationDelegate sharedInstance] application:userInfo[@"application"]
                                                                        openURL:self.launchURL
                                                              sourceApplication:userInfo[@"sourceApplication"]
                                                                     annotation:userInfo[@"annotation"]];
            }
        }
        
        self.launchURL = nil;
    }
    
    return result;
}

- (void)checkForUpdateVersion
{
    // update app version
    NSString *storedVersion = FASData.sharedData.appVersion;
    NSString *storedBuildVersion = FASData.sharedData.buildVersion;
    [FASData.sharedData setAppVersionFromBundle];
    NSString *bundleVersion = FASData.sharedData.appVersion;
    NSString *bundleBuildVersion = FASData.sharedData.buildVersion;
    
    UBILogTrace(@"old version %@, new version = %@", storedVersion, bundleVersion);
    
    // if no stored version, then first launch of any FAS version
    if (!storedVersion) {
        self.isNewInstall = YES;
    }
    
    // if upgrading, perform any upgrade tasks
    if (storedBuildVersion != nil &&
        ![storedBuildVersion isEqualToString:bundleBuildVersion])
    {
        self.isUpgradeInstall = YES;
        
        UBILogTrace(@"Upgrade Install!");
        
        NSUInteger previousBuildVersion = [storedBuildVersion integerValue];
        if (previousBuildVersion < 61) {
            [FASData.sharedData migrateAvailableProductIDsToPurchasedItems];
        }
    }
}

- (void)logBootState
{
    // log boot info
    UBILogTrace(
          @"\n============================================================\n"
          @"FASWorkouts iPhone/iPad version %@(%@) (%@)\n"
          @"\tAPI base path:      %@\n"
          @"\tiOS version:        %@\n"
          @"\tApplication state:  %ld\n"
          @"============================================================\n",
          FASData.sharedData.appVersion,
          FASData.sharedData.buildVersion,
          FASAppDelegateBuildConfig,
          FASEnvironment.APIBasePath,
          UIDevice.currentDevice.systemVersion,
          (long)UIApplication.sharedApplication.applicationState);
}

- (void)handleRemoteNotification:(NSDictionary *)userInfo
                 loadImmediately:(BOOL)loadImmediately
{
    NSDictionary *payload = [userInfo objectForKey:@"aps"];
    NSArray *messageIDs = [userInfo objectForKey:@"ids"];
    NSString *alertText = [payload objectForKey:@"alert"];
#pragma unused(alertText) // suppressing the warning, as we will get back so to ir
    
    UBILogTrace(@"notification:\n%@\npayload:\n%@\nmessageIDs:\n%@",
            userInfo, payload, messageIDs);
    
    if (loadImmediately) {
        void (^loadingBlock)() = ^{
            // if we have ids, load them as a query
            if (messageIDs && [messageIDs count] > 0) {
                // TODO: load the messages for the messageIDs and display them in a modal view controller
                NSUInteger messageID = [[messageIDs objectAtIndex:0] integerValue];
                if (messageID > 0) {
                    // TODO: present the vc
                }
            }
            
            // else update alerts query
            else {
                
            }
        };
        
        // make sure to dismiss the current modal VC if other than the modalScrollVC used for
        // loading messages from APNS notifications
        UIViewController *currentModalVC = nil; // TODO: retrieve currently presented view controller
        if (currentModalVC && !self.modalAlertController) {
            [currentModalVC dismissViewControllerAnimated:NO
                                               completion:^{
                                                   loadingBlock();
                                               }];
        }
        else {
            loadingBlock();
        }
    }
    
    // TODO: else add to some place where user can load it manually
    else {
        UBILogTrace(@"do not load immediately");
    }
}

- (void)presentMessageWithID:(NSUInteger)messageID
{
    UBILogTrace(@"");
    
    // TODO: stop any tasks tha might prevent the data to load
    
    // if not active, do nothing
    if (UIApplication.sharedApplication.applicationState != UIApplicationStateActive) {
        return;
    }
}

- (void)setupViewControllers
{
    if ([NSThread currentThread] != [NSThread mainThread]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setupViewControllers];
        });
        return;
    }
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UIViewController *vc =[storyboard instantiateInitialViewController];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = vc;
    
    self.splitViewController = (UISplitViewController *)self.window.rootViewController;
    // TODO: uncomment when you need to allow "expand detail view" controls
//    UINavigationController *navigationController = [self.splitViewController.viewControllers lastObject];
//    navigationController.topViewController.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
//    navigationController.topViewController.navigationItem.leftItemsSupplementBackButton = YES;
    self.splitViewController.delegate = self;
    self.splitViewController.preferredDisplayMode = UISplitViewControllerDisplayModeAllVisible;
    self.splitViewController.preferredPrimaryColumnWidthFraction = 0.5;
    [self.window makeKeyAndVisible];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark ---- Splash
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)addSplashView
{
    if (!self.splashView) {
        UINib* launchScreenNib = [UINib nibWithNibName:@"LaunchScreen" bundle:nil];
        self.splashView = (UIView*)[launchScreenNib instantiateWithOwner:nil options:nil].firstObject;
        self.splashView.frame = self.window.frame;
        self.splashView.contentMode = UIViewContentModeTop;
        [self.window addSubview:self.splashView];
        [self.window bringSubviewToFront:self.splashView];
    }
}

- (void)removeSplashView
{
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.splashView.alpha = 0.0;
                     }
                     completion:^(BOOL finished) {
                         [self.splashView removeFromSuperview];
                         self.splashView = nil;
                     }];
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark ---- MOCKS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

// mock APNS notification
- (NSDictionary *)mockNotificationInfo
{
    return @{
             @"aps" : @{
                     @"alert" : @"Hey",
                     @"badge" : @1,
                     @"sound" : @"default"
                     },
             @"ids" : @[@41295]
             };
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark ---- FASAPIDelegate
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)FASAPIdidFinish:(FASAPI *)theFASAPI
{
    UBILogTrace(@"%@", theFASAPI);
    
    if (theFASAPI.method == FASAPIMethodRegisterDeviceToken) {
       // successfully submitted token to the server
    }
}

- (void)FASAPIdidFail:(FASAPI *)theFASAPI
{
    UBILogTrace(@"%@", theFASAPI);
    
    if (theFASAPI.method == FASAPIMethodRegisterDeviceToken) {
        // failed to submit token to the server
    }
}

@end
