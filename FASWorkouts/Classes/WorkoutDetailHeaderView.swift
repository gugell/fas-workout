//
//  WorkoutDetailHeaderView.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 4/11/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit

class WorkoutDetailHeaderView: UIView {
    // MARK: Public Properties
    var shouldHaveSeparator: Bool = true {
        didSet {
            setNeedsDisplay()
        }
    }
    // it appears, that toolbar's button titles and images vertical
    // adjusments are hard to change - toolbar position them on its own
    // below is an ideal height, so everything looks centerd vertically
    var height: CGFloat {
        get {
            if shouldHaveSeparator {
                return 51.0
            }
            else {
                return 50.0
            }
        }
    }
    var calories: String? = nil {
        didSet {
            updateElements()
        }
    }
    var muscles: String? = nil {
        didSet {
            updateElements()
        }
    }
    // MARK: Private Properties
    private let toolbar: UIToolbar = UIToolbar()
    
    ///////////////////////////////////////////////
    // MARK: - Init -
    ///////////////////////////////////////////////
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(toolbar)
        toolbar.translucent = true
        toolbar.tintColor = UIColor.whiteColor()
        toolbar.barTintColor = UIColor.fasGrayColor41()
        toolbar.translatesAutoresizingMaskIntoConstraints = false
        // prevent interaction w/ elements
        toolbar.userInteractionEnabled = false
        
        updateElements()
        
        // TODO: add workout observers, to update view according to the model
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func drawRect(rect: CGRect) {
        // draw separator
        if shouldHaveSeparator == true, let context = UIGraphicsGetCurrentContext() {
            CGContextSetStrokeColorWithColor(context, UIColor.fasSeparatorColor().CGColor)
            CGContextSetLineWidth(context, 2.0)
            CGContextMoveToPoint(context, 0.0, rect.height)
            CGContextAddLineToPoint(context, rect.width, rect.height)
            CGContextStrokePath(context)
        }
    }
    
    ///////////////////////////////////////////////
    // MARK: - Layout -
    ///////////////////////////////////////////////
    
    override func updateConstraints() {
        
        var viewConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()
        let views = NSDictionary(object: toolbar, forKey: "toolbar")
        viewConstraints += NSLayoutConstraint.constraintsWithVisualFormat("H:|[toolbar]|",
            options: NSLayoutFormatOptions(rawValue: 0),
            metrics: nil,
            views: views as! [String : AnyObject])
        if shouldHaveSeparator {
            viewConstraints += NSLayoutConstraint.constraintsWithVisualFormat("V:|[toolbar(==51)]", // 1 for separator
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: nil,
                views: views as! [String : AnyObject])
        }
        else {
            viewConstraints += NSLayoutConstraint.constraintsWithVisualFormat("V:|[toolbar(==50)]",
                options: NSLayoutFormatOptions(rawValue: 0),
                metrics: nil,
                views: views as! [String : AnyObject])
        }
        self.removeConstraints(viewConstraints)
        self.addConstraints(viewConstraints)
        
        super.updateConstraints()
    }
    
    ///////////////////////////////////////////////
    // MARK: - Update -
    ///////////////////////////////////////////////
    
    private func updateElements() {
        // titles according to design
        let caloriesAttributes = [
            NSForegroundColorAttributeName : UIColor.whiteColor(),
            NSFontAttributeName : UIFont.boldBabasFontOfSize(25.0)
        ]
        
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil)
        let fixedSpace = UIBarButtonItem(barButtonSystemItem: .FixedSpace, target: nil, action: nil)
        fixedSpace.width = 44.0
        
        var buttonItems: [UIBarButtonItem] = []
        var hasLeftItem: Bool = false
        // leftmost
        buttonItems.append(flexibleSpace)
        
        if let _ = calories {
//            if hasLeftItem {
//                buttonItems.append(fixedSpace)
//            }
            let caloriesImage = UIBarButtonItem(image: UIImage(named: "IconFire"), style: .Plain, target: nil, action: nil)
            let caloriesTitle = UIBarButtonItem(title: calories, style: .Plain, target: nil, action: nil)
            caloriesTitle.setTitleTextAttributes(caloriesAttributes, forState: .Normal)
            caloriesTitle.setTitleTextAttributes(caloriesAttributes, forState: .Highlighted)
            buttonItems.append(caloriesImage)
            buttonItems.append(caloriesTitle)
            hasLeftItem = true
        }

        if let _ = muscles {
            if hasLeftItem {
                buttonItems.append(fixedSpace)
            }
            let musclesImage = UIBarButtonItem(image: UIImage(named: "IconGoal"), style: .Plain, target: nil, action: nil)
            let musclesTitle = UIBarButtonItem(title: muscles, style: .Plain, target: nil, action: nil)
            musclesTitle.setTitleTextAttributes(caloriesAttributes, forState: .Normal)
            musclesTitle.setTitleTextAttributes(caloriesAttributes, forState: .Highlighted)
            buttonItems.append(musclesImage)
            buttonItems.append(musclesTitle)
            hasLeftItem = true
        }
        // rightmost
        buttonItems.append(flexibleSpace)
        
        toolbar.setItems(buttonItems, animated: false)
        setNeedsUpdateConstraints()
    }
}
