//
//  FASWorkoutHistoryRunList.m
//  FASWorkouts
//
//  Created by Vladimir Stepanchenko on 4/18/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "FASWorkoutHistoryRunList.h"
#import "FASAPISession.h"
#import "FASUserInfo.h"

// entity
NSString* const kFASWorkoutHistoryRunListEntity = @"fas_user_workout_history";

// fields
NSString* const kFASWorkoutHistoryRunListWorkoutHistoryIdFieldKey                   = @"ID";
NSString* const kFASWorkoutHistoryRunListWorkoutHistoryUserIdFieldKey               = @"userID";
NSString* const kFASWorkoutHistoryRunListWorkoutHistoryWorkoutIDFieldKey            = @"workoutID";
NSString* const kFASWorkoutHistoryRunListWorkoutHistoryUserIDFieldKey               = @"userID";
NSString* const kFASWorkoutHistoryRunListWorkoutHistoryRateFieldKey                 = @"rate";
NSString* const kFASWorkoutHistoryRunListWorkoutHistoryStartDateFieldKey            = @"startDate";
NSString* const kFASWorkoutHistoryRunListWorkoutHistoryEndDateFieldKey              = @"endDate";
NSString* const kFASWorkoutHistoryRunListWorkoutHistoryPauseDurationFieldKey        = @"pauseDuration";

@implementation FASWorkoutHistoryRunList
///////////////////////////////////////////////
#pragma mark -
#pragma mark Initialization
#pragma mark -
///////////////////////////////////////////////
- (instancetype)init
{
    
    self = [self initWithFieldList:@[kFASWorkoutHistoryRunListWorkoutHistoryIdFieldKey,
                                     kFASWorkoutHistoryRunListWorkoutHistoryWorkoutIDFieldKey,
                                     kFASWorkoutHistoryRunListWorkoutHistoryUserIDFieldKey,
                                     kFASWorkoutHistoryRunListWorkoutHistoryRateFieldKey,
                                     kFASWorkoutHistoryRunListWorkoutHistoryStartDateFieldKey,
                                     kFASWorkoutHistoryRunListWorkoutHistoryEndDateFieldKey,
                                     kFASWorkoutHistoryRunListWorkoutHistoryPauseDurationFieldKey]];
    [self.whereList addItem:@"userID" condition:UBIRunListWhereListConditionEqual value:[[FASAPISession currentSession].userInfo stringUserID] key:kFASWorkoutHistoryRunListWorkoutHistoryUserIdFieldKey];
    return self;
}

- (instancetype)initWithFieldList:(NSArray *)fieldList
{
    return [super initWithFieldList:fieldList entity:kFASWorkoutHistoryRunListEntity];
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Workout Run List Constructors
#pragma mark -
///////////////////////////////////////////////
+ (instancetype)workoutHistoryRunListDefault
{
    return [[FASWorkoutHistoryRunList alloc] init];
}

@end