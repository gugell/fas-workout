//
//  FASSegmentedNavigationBar.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/4/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

class FASSegmentedNavigationBar: UIView {
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    override func willMoveToWindow(newWindow: UIWindow?) {
        // Use the layer shadow to draw a one pixel hairline under this view.
        layer.shadowOffset = CGSize(width: 0.0, height: (1.0/UIScreen.mainScreen().scale))
        layer.shadowRadius = 0.0
        // UINavigationBar's hairline is adaptive, its properties change with
        // the contents it overlies.  You may need to experiment with these
        // values to best match your content.
        layer.shadowColor = UIColor.blackColor().CGColor
        layer.shadowOpacity = 0.25
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        segmentedControl.apportionsSegmentWidthsByContent = true
    }
//    required init(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        
//        commonInit()
//    }
//    
//    private func commonInit() {
//        printlnDebugTrace("commonInit")
//    }
}
