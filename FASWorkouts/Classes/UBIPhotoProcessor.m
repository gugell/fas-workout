//
//  UBIPhotoProcessor.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 3/19/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "UBIPhotoProcessor.h"
#import "UBIObjectRetainer.h"
#import <UIKit/UIKit.h>
#import "UBIPhotoController.h"
#import "FASUserInfo.h"
#import "UIImage+Resize.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "FASData.h"
#import "UIImage+ImageEffects.h"
#import "UBILogger.h"

static dispatch_queue_t sUBIPhotoProcessorQueue;

@interface UBIPhotoProcessor ()
@property (nonatomic, strong) UBIObjectRetainer * retainer;
@end

@implementation UBIPhotoProcessor

+ (void)initialize
{
    if (!sUBIPhotoProcessorQueue) {
        sUBIPhotoProcessorQueue = dispatch_queue_create("com.fas-sport.FASWorkouts.photoImporterQueue",
                                                      DISPATCH_QUEUE_SERIAL);
    }
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _bgTask = UIBackgroundTaskInvalid;
    }
    return self;
}

+ (void)photoProcessorWithInfo:(NSDictionary *)info
                       options:(UBIPhotoProcessorOptions)options
                    completion:(photoProcessorCompletionHandler_t)completion
{
    UBIPhotoProcessor *importer = [self new];
    [importer processPostWithInfo:info options:options completion:completion];
}

// saves the supplied photo in multiple formats and processes it according to options
// blocks are submitted to the processing queue in order of largest memory footprint to smallest
// each block includes an @autorelease wrapper to memory is freed up as quickly as possible
// TODO: needs to execute when app is in background
- (void)processPostWithInfo:(NSDictionary *)info
                   options:(UBIPhotoProcessorOptions)options
                completion:(photoProcessorCompletionHandler_t)completion
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        // gather info from photo controller
        UIImage *image = [info objectForKey:UBIPhotoControllerPhotoInfoImageKey];
        UBIPhotoControllerPhotoSource photoSource = [[info objectForKey:UBIPhotoControllerPhotoInfoSourceKey] integerValue];
        
        // user info
        FASUserInfo* userInfo = info[@"userInfo"];
        
        __block NSMutableDictionary* outputDict = [NSMutableDictionary dictionary];
        
        UIApplication *app = UIApplication.sharedApplication;
        if (UIDevice.currentDevice.multitaskingSupported) {
            self.bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
                UBILogTrace(@"FASPhotoImporter(%@):importPhotoWithInfo EXPIRATION HANDLER", self);
                // handle error
                [app endBackgroundTask:self.bgTask];
                self.bgTask = UIBackgroundTaskInvalid;
            }];
        }
        
        if (image) {
            self.retainer = [UBIObjectRetainer new];
            self.retainer.object = self;
            
            __block UIImage *rotatedImage;
            
            if (options & UBIPhotoProcessorOptionsSaveToDisc) {
                
                // generate rotatedImage (image corrected for device orientation)
                dispatch_async(sUBIPhotoProcessorQueue, ^{
                    @autoreleasepool {
                        UBILogTrace(@"ROTATING ORIGINAL IMAGE");
                        if (photoSource == UBIPhotoControllerPhotoSourceCamera) {
                            // if the image is from the camera, rotate it based on device orientation
                            // TODO: get device orientation from UIImagePicker
                            rotatedImage = [image imageRotatedUpForDeviceOrientation:UIDeviceOrientationUnknown];
                        }
                        else {
                            // if the image is from the camera roll, rotate it based on imageOrientation
                            rotatedImage = [image exResizedImage:image.size zoomLevel:1.0 interpolationQuality:kCGInterpolationHigh];
                        }
                    }
                });
                
                // scale the rotatedImage for posting and save to disk
                NSString* originalPhotoPath = userInfo.originalPhotoPath;
                dispatch_async(sUBIPhotoProcessorQueue, ^{
                    @autoreleasepool {
                        UBILogTrace(@"PROCESSING ORIGINAL IMAGE");
                        
                        if (rotatedImage.size.width > 1536 || rotatedImage.size.width > 1536) {
                            CGSize maxSize = CGSizeMake(1536.0, 1152.0);
                            if (rotatedImage.size.height > rotatedImage.size.width) {
                                maxSize = CGSizeMake(1152.0, 1536.0);
                            }
                            rotatedImage = [rotatedImage resizedImageWithContentMode:UIViewContentModeScaleAspectFill
                                                                              bounds:maxSize
                                                                           zoomLevel:1.0
                                                                interpolationQuality:kCGInterpolationDefault];
                        }
                        
                        UBILogTrace(@"Original Image WxH = %f x %f", rotatedImage.size.width, rotatedImage.size.height);
                        [FASData.sharedData saveImage:rotatedImage toPath:originalPhotoPath];
                        UBILogTrace(@"ORIGINAL IMAGE SAVED");
                    }
                });
            }
            
            if (options & UBIPhotoProcessorOptionsSaveToLibrary) {
                // Save the new image (original or edited) to the Camera Roll
//                UIImageWriteToSavedPhotosAlbum(image, nil, nil , nil);
                
                // save rotatedImage to the saved photos album
                dispatch_async(sUBIPhotoProcessorQueue, ^{
                    @autoreleasepool {
                        NSDictionary *metadata = @{}; // add metadata (e.g. location) if needed
                        CGImageRef imageToSave = rotatedImage != nil ? [rotatedImage CGImage] : [image CGImage];
                        
                        ALAssetsLibrary *library = [ALAssetsLibrary new];
                        [library writeImageToSavedPhotosAlbum:imageToSave metadata:metadata completionBlock:^(NSURL *assetURL, NSError *error) {
                            if (error) {
                                UBILogTrace(@"ERROR while saving image to saved photo album %@", error);
                            }
                            else {
                                UBILogTrace(@"SAVED TO SAVED PHOTOS ALBUM");
                            }
                        }];
                    }
                });
            }
            
            if (options & UBIPhotoProcessorOptionsFxBlur) {
                // create image w/ blur fx
                dispatch_async(sUBIPhotoProcessorQueue, ^{
                    @autoreleasepool {
                        // TODO: add image processing
                    }
                });
            }
        }
        
        else {
            UBILogWarning(@"image was nil - processing is scipped");
        }
        
        dispatch_async(sUBIPhotoProcessorQueue, ^{
            // dispatch the completion block
            if (completion) {
                completion([outputDict copy]);
            }
            
            // release the retainer (break the retain cycle)
            self.retainer = nil;
            if (self.bgTask != UIBackgroundTaskInvalid) {
                [app endBackgroundTask:self.bgTask];
                self.bgTask = UIBackgroundTaskInvalid;
            }
        });
    });
}

@end
