//
//  FASSettingsDataSource.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 2/20/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "FASMenuDataSource.h"
#import "FASWorkouts-Swift.h"

@implementation FASMenuDataSource

- (instancetype)init
{
    self = [self initWithSections:[FASMenuDataSource tableSections]];
    if (self) {
        // custom initialization
    }
    return self;
}

// TODO: need logged out version
+ (NSArray *)tableSections
{
    return @[
             @{
                 kUBIDataSourceTitle : @"My account",
                 kUBIDataSourceItems : @[
                         @{kUBIDataSourceTitle : @"Profile",
                           kUBIDataSourceItemID : @(FASSettingsDataSourceItemIDProfile)},
                         
                         @{kUBIDataSourceTitle : @"Restore Purchases",
                           kUBIDataSourceCellIdentifier : @"noaccessory",
                           kUBIDataSourceItemID : @(FASSettingsDataSourceItemIDRestorePurchases)}
                         // uncomment when ready to implement
//                         @{kUBIDataSourceTitle : @"HealthKit",
//                           kUBIDataSourceItemID : @(FASSettingsDataSourceItemIDHealthKit)}
                         ]
                 },
             // uncomment when ready to implement
//             @{
//                 kUBIDataSourceTitle : @"Training",
//                 kUBIDataSourceItems : @[
//                         @{kUBIDataSourceTitle : @"Exercises Base",
//                           kUBIDataSourceItemID : @(FASSettingsDataSourceItemIDExercisesBase)}
//                         ]
//                 },
             @{
                 kUBIDataSourceTitle : @"General",
                 kUBIDataSourceItems : @[
                         // uncomment when ready to implement
//                         @{kUBIDataSourceTitle : @"Audio",
//                           kUBIDataSourceItemID : @(FASSettingsDataSourceItemIDAudio)},
                         
                         @{kUBIDataSourceTitle : @"Notifications",
                           kUBIDataSourceItemID : @(FASSettingsDataSourceItemIDNotificationsSettings)}
#ifdef DEBUG_SETTINGS
                         ,
                         @{kUBIDataSourceTitle : @"Debug",
                           kUBIDataSourceItemID : @(FASSettingsDataSourceItemIDDebug)}
#endif
                 ]
                 },
             @{
                 kUBIDataSourceTitle : @"FAS Workout",
                 kUBIDataSourceItems : @[
                         @{kUBIDataSourceTitle : @"FAQ",
                           kUBIDataSourceItemID : @(FASSettingsDataSourceItemIDFAQ)},
                         
                         @{kUBIDataSourceTitle : @"Rate Us",
                           kUBIDataSourceCellIdentifier : @"noaccessory",
                           kUBIDataSourceItemID : @(FASSettingsDataSourceItemIDRateApp)},
                         
                         @{kUBIDataSourceTitle : @"Feedback",
                           kUBIDataSourceCellIdentifier : @"noaccessory",
                           kUBIDataSourceItemID : @(FASSettingsDataSourceItemIDFeedback)},
                         
                         @{kUBIDataSourceTitle : @"Share Us",
                           kUBIDataSourceCellIdentifier : @"noaccessory",
                           kUBIDataSourceItemID : @(FASSettingsDataSourceItemIDShareApp)},
                         
                         @{kUBIDataSourceTitle : @"Invite Friends",
                           kUBIDataSourceCellIdentifier : @"noaccessory",
                           kUBIDataSourceItemID : @(FASSettingsDataSourceItemIDFacebookInviteFriends)}
                         ]
                 }
             ];
}

- (UITableViewCell *)newCellWithIdentifier:(NSString *)cellIdentifier
{
    UITableViewCell *cell;
    if ([cellIdentifier isEqualToString:@"noaccessory"]) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }

    cell.textLabel.font = [UIFont boldBabasFontOfSize:25.0f];
    cell.textLabel.textColor = UIColor.whiteColor;
    cell.backgroundColor = UIColor.clearColor;
    cell.contentView.backgroundColor = UIColor.clearColor;
    
    // customize selection
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    UIView *selectedBackgroundView = [[UIView alloc] init];
    selectedBackgroundView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.3f];
    [cell setSelectedBackgroundView:selectedBackgroundView];
    
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    // call super to set textLabel
    [super configureCell:cell atIndexPath:indexPath];
    
    NSDictionary *item = [self itemAtIndexPath:indexPath];
    FASSettingsDataSourceItemID itemId = [item[kUBIDataSourceItemID] integerValue];
    
    switch (itemId) {
        case FASSettingsDataSourceItemIDProfile: {
//            cell.imageView.image = <#image#>
        } break;
        
        case FASSettingsDataSourceItemIDExercisesBase: {
        } break;
            
        case FASSettingsDataSourceItemIDAudio: {
        } break;
            
        case FASSettingsDataSourceItemIDNotificationsSettings: {
        } break;
            
        case FASSettingsDataSourceItemIDFAQ: {
        } break;
            
        case FASSettingsDataSourceItemIDHealthKit: {
        } break;
            
        case FASSettingsDataSourceItemIDRateApp: {
            // TODO: use Apptentive
        } break;
            
        case FASSettingsDataSourceItemIDFeedback: {
        } break;
          
        case FASSettingsDataSourceItemIDFacebookInviteFriends: {            
        } break;
            
        default: break;
    }
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark UITableViewDataSource
#pragma mark -
///////////////////////////////////////////////

// implemented in superclass
@end
