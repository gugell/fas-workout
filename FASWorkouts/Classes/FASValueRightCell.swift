//
//  FASValueRightCell.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 8/18/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit

class FASValueRightCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
}