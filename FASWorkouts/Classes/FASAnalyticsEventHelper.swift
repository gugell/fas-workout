//
//  FASAnalyticsEventHelper.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/26/15.
//  Copyright © 2015 FAS. All rights reserved.
//

import Foundation

class FASAnalyticsEventHelper: NSObject {
    
    //////////////////////////////////////////////
    // Marketer events
    //////////////////////////////////////////////
    
    // 0. Запуск приложения
    class func trackAppDidLaunch() {
        FASData.sharedData().trackEvent("App", withAction: "Launched")
    }
    
    // 0.1. Запуск приложения через пуш
    class func trackAppDidLaunchFromRemoteNotification() {
        FASData.sharedData().trackEvent("App", withAction: "Launched From Push")
    }
    
    // 1. Просмотр воркаута (название воркаута)
    class func trackWorkoutViewWithName(workoutName: String) {
        FASData.sharedData().trackEvent("WorkoutView", withAction: workoutName)
    }
    
    // 2. Просмотр упражнения (название воркаута, название упражнения)
    class func trackExerciseViewWithName(exerciseName: String, inWorkout workoutName: String) {
        FASData.sharedData().trackEvent("ExerciseView", withAction: "\(workoutName)/\(exerciseName)")
    }
    
    // 3. Запуск воркаута (название воркута)
    class func trackWorkoutDidStartWithName(workoutName: String) {
        FASData.sharedData().trackEvent("WorkoutStart", withAction: workoutName)
    }
    
    // 4. Пропуск воркаута (тут в значении незавершенности) (название воркаута)
    class func trackWorkoutDidAbortWithName(workoutName: String) {
        FASData.sharedData().trackEvent("WorkoutAbort", withAction: workoutName)
    }
    
    // 5. Финиш воркаута (название воркаута, оценка)
    class func trackWorkoutDidFinishWithName(workoutName: String) {
        FASData.sharedData().trackEvent("WorkoutFinish", withAction: workoutName)
    }
    
    // 6. Покупка (название воркаута)
    class func trackWorkoutPurchasedWithName(workoutName: String) {
        FASData.sharedData().trackEvent("WorkoutPurchase", withAction: workoutName)
    }
    
    // 7. Покупка (название воркаута)
    class func trackWorkoutPackPurchasedWithName(workoutPackName: String) {
        FASData.sharedData().trackEvent("WorkoutPackPurchase", withAction: workoutPackName)
    }
    
    
    
    //////////////////////////////////////////////
    // Developer events
    //////////////////////////////////////////////
    
    // In-App product id that became invalid
    class func trackInvalidProductID(productID: String) {
        FASData.sharedData().trackEvent("InvalidProduct", withAction: productID)
    }
}