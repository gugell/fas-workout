//
//  FASLicenseAgreement.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/10/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>

extern  NSString * __nonnull const kFASLicenseAgreementDataDefaultFilename;

@interface FASLicenseAgreement : NSObject

@property (nonatomic) NSUInteger version; // 0 - invalid
@property (nonatomic, copy, nullable) NSString* filename;

// load from a file in main bundle
+ (nullable instancetype)licenseAgreementFromFile:(nonnull NSString *)filename;

+ (nullable instancetype)defaultLicenseAgreement;

- (nullable NSString *)loadLicenseText;
- (nullable NSAttributedString *)loadLicenseAttributedText;

@end
