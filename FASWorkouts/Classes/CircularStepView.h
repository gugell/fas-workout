//
//  CircularStepView.h
//  FAS Workouts
//
//  Created by Vladimir Stepanchenko on 12/15/14.
//  Copyright (c) 2014 Vladimir Stepanchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CircularStepDelegate <NSObject>
@optional
-(void)valueChanged:(NSUInteger)value;

@end

@interface CircularStepView : UIView

@property (assign, nonatomic) id<CircularStepDelegate> delegate;

@property(nonatomic, retain) UIColor *textColor;

@property(nonatomic) NSUInteger value;
@property(nonatomic) NSUInteger step;
@property(nonatomic) NSUInteger max;
@property(nonatomic) NSUInteger min;

-(void)setup;

@end
