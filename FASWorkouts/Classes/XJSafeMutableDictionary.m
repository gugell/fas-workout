//
//  XJSafeMutableDictionary.m
//  FASWorkout
//

#import "XJSafeMutableDictionary.h"

@interface XJSafeMutableDictionary()
@property (nonatomic, strong)   NSMutableDictionary *   backingDictionary;
@property (nonatomic)           dispatch_queue_t        lockQueue;

- (id)init NS_DESIGNATED_INITIALIZER;

@end

@implementation XJSafeMutableDictionary

+ (XJSafeMutableDictionary *)dictionary
{
    return [XJSafeMutableDictionary new];
}

+ (XJSafeMutableDictionary *)dictionaryWithDictionary:(NSDictionary *)dictionary
{
    return [[XJSafeMutableDictionary alloc] initWithDictionary:dictionary];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _lockQueue = dispatch_queue_create("XJSafeMutableDictionary",
                                           DISPATCH_QUEUE_CONCURRENT);
        _backingDictionary = [NSMutableDictionary dictionary];
    }
    return self;
}

- (instancetype)initWithCapacity:(NSUInteger)numItems
{
    self = [self init];
    if (self) {
        _backingDictionary = [[NSMutableDictionary alloc] initWithCapacity:numItems];
    }
    return self;
}

- (instancetype)initWithObjects:(NSArray *)objects forKeys:(NSArray *)keys
{
    self = [self init];
    if (self) {
        _backingDictionary = [[NSMutableDictionary alloc] initWithObjects:objects
                                                                  forKeys:keys];
    }
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary *)aDictionary
{
    self = [self init];
    if (self) {
        [_backingDictionary addEntriesFromDictionary:aDictionary];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [self init];
    if (self) {
        _backingDictionary = [[NSMutableDictionary alloc] initWithCoder:aDecoder];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    dispatch_sync(self.lockQueue, ^{
        [self.backingDictionary encodeWithCoder:aCoder];
    });
}

- (NSUInteger)count
{
    __block NSUInteger n;
    dispatch_sync(self.lockQueue, ^{
        n = self.backingDictionary.count;
    });
    return n;
}

- (id)objectForKey:(id)aKey
{
    __block id obj = nil;
    dispatch_sync(self.lockQueue, ^{
        obj = [_backingDictionary objectForKey:aKey];
    });
    return obj;
}

- (NSEnumerator *)keyEnumerator
{
    __block NSEnumerator *enumerator;
    dispatch_sync(self.lockQueue, ^{
        enumerator = [self.backingDictionary keyEnumerator];
    });
    return enumerator;
}

- (void)setObject:(id)anObject forKey:(id<NSCopying>)aKey
{
    dispatch_sync(self.lockQueue, ^{
        [self.backingDictionary setObject:anObject forKey:aKey];
    });
}

- (void)setValue:(id)value forKey:(NSString *)aKey
{
    dispatch_sync(self.lockQueue, ^{
        [self.backingDictionary setValue:value forKey:aKey];
    });
}

- (void)addEntriesFromDictionary:(NSDictionary *)otherDictionary
{
    dispatch_sync(self.lockQueue, ^{
        [self.backingDictionary addEntriesFromDictionary:otherDictionary];
    });
}

- (void)removeObjectForKey:(id)aKey
{
    dispatch_sync(self.lockQueue, ^{
        [self.backingDictionary removeObjectForKey:aKey];
    });
}

- (void)removeObjectsForKeys:(NSArray *)keyArray
{
    dispatch_sync(self.lockQueue, ^{
        [self.backingDictionary removeObjectsForKeys:keyArray];
    });
}

- (void)removeAllObjects
{
    dispatch_sync(self.lockQueue, ^{
        [self.backingDictionary removeAllObjects];
    });
}

@end
