//
//  FASAsynchronousActionTableViewCell.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/20/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

class FASAsynchronousActionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var busy: Bool = false {
        didSet {
            titleLabel.hidden = busy;
            if (busy) {
                activityIndicator.startAnimating()
            }
            else {
                activityIndicator.stopAnimating()
            }
        }
    }
}