//
//  UBIArrayDataSource.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 2/20/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "UBIArrayDataSource.h"

@implementation UBIArrayDataSource

- (instancetype)initWithSections:(NSArray *)sections
{
    self = [super init];
    if (self) {
        self.sections = sections;
    }
    return self;
}

- (NSDictionary *)sectionInfoAtIndexPath:(NSIndexPath *)indexPath
{
    return self.sections[indexPath.section];
}

- (NSArray *)itemsForSection:(NSUInteger)section
{
    NSArray *items;
    NSDictionary *sectionInfo = self.sections[section];
    if (sectionInfo) {
        items = sectionInfo[kUBIDataSourceItems];
    }
    return items;
}

- (NSDictionary *)itemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *itemInfo;
    NSArray *items = [self itemsForSection:indexPath.section];
    if (items && items.count > indexPath.row) {
        itemInfo = [items objectAtIndex:indexPath.row];
    }
    return itemInfo;
}

- (NSString *)cellIdentifierAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *sectionInfo = [self sectionInfoAtIndexPath:indexPath];
    NSDictionary *itemInfo = sectionInfo[kUBIDataSourceItems][indexPath.row];
    NSString *cellId = itemInfo[kUBIDataSourceCellIdentifier];
    if (!cellId) {
        cellId = sectionInfo[kUBIDataSourceCellIdentifier];
    }
    if (!cellId && self.cellIdentifierBlock) {
        cellId = self.cellIdentifierBlock();
    }
    if (!cellId) {
        cellId = @"UBIArrayDataSource";
    }
    return cellId;
}

- (UITableViewCell *)newCellWithIdentifier:(NSString *)cellIdentifier
{
    UITableViewCell *cell;
    if (self.createCellBlock) {
        cell = self.createCellBlock(cellIdentifier);
    }
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
    }
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *itemInfo = [self itemAtIndexPath:indexPath];
    
    if (self.configureCellBlock) {
        self.configureCellBlock(cell, itemInfo);
    }
    else {
        cell.textLabel.text = itemInfo[kUBIDataSourceTitle];
    }
}

// TODO: may need more flexible tableView reload options
- (void)didUpdateDataSource
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

- (NSUInteger)itemIDatIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *item = [self itemAtIndexPath:indexPath];
    NSNumber *itemIDNumber = item[kUBIDataSourceItemID];
    NSUInteger itemID = NSNotFound;
    if (itemIDNumber) {
        itemID = [itemIDNumber integerValue];
    }
    return itemID;
}

//////////////////////////////////////////////////////////////////////////////
#pragma mark ---- UITableViewDataSource
//////////////////////////////////////////////////////////////////////////////

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.sections.count;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    NSArray *items = [self itemsForSection:section];
    return items.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellId = [self cellIdentifierAtIndexPath:indexPath];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    
    if (!cell) {
        cell = [self newCellWithIdentifier:cellId];
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSDictionary *sectionInfo = self.sections[section];
    return sectionInfo[kUBIDataSourceTitle];
}

@end
