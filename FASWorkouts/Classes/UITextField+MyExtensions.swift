//
//  UITextField+MyExtensions.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/9/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

extension UITextField {
    // display semi-transparent textfield if it has no text
    func setTransparencyIfNeeded() {
        if let notNilText = self.text {
            if notNilText.isEmpty {
                self.alpha = 0.5
            } else {
                self.alpha = 1
            }
        } else {
            self.alpha = 0.5
        }
    }
}
