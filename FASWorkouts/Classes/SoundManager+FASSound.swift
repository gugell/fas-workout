//
//  SoundManager+FASSound.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/13/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import AVFoundation

enum FASWorkoutSound {
    case NextExercise
    case BeginWorkout
    
    var filename: String? {
        switch self {
        case .NextExercise:
            return "sound_next_exercise.wav"
        case .BeginWorkout:
            return "sound_begin_workout.wav"
        }
    }
}

extension Sound {
    
    class func fasSound(fasSound: FASWorkoutSound, playImmediately: Bool = false) -> Sound? {
        var sound: Sound?
        if let filename = fasSound.filename {
            sound = Sound(named: filename)
        }
        
        if let sound = sound where playImmediately==true{
            sound.play()
        }
        
        return sound
    }
}