//
//  UIImage+RoundedCorner.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/1/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <UIKit/UIKit.h>

// Extends the UIImage class to support making rounded corners
@interface UIImage (RoundedCorner)
- (UIImage *)roundedCornerImage:(NSInteger)cornerSize borderSize:(NSInteger)borderSize;
@end
