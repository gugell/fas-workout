//
//  FASCounterView.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/11/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit

class FASCounterView: UIView {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var counter: CircularStepView!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var getReady: UILabel!
    
    var exercise: Exercise? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        if let ex = exercise {
            
        }
        else {
            getReady.hidden = true
           timeLabel.hidden = true
        }
    }

    
    override func awakeFromNib() {
        titleLabel.attributedText = attributedTitleString()
    }
    
    private func attributedTitleString() -> NSAttributedString {
        let firstLine = "how many times\n"
        let secondLine = "have you done that exercise?"
        let attrFirstLine = NSAttributedString(string: firstLine, attributes:
            [
                NSFontAttributeName: UIFont.boldBabasFontOfSize(40.0),
                NSForegroundColorAttributeName: UIColor.whiteColor(),
                NSBackgroundColorAttributeName: UIColor.clearColor()
            ])
        let attrSecondLine = NSAttributedString(string: secondLine, attributes:
            [
                NSFontAttributeName:UIFont.boldBabasFontOfSize(25.0),
                NSForegroundColorAttributeName: UIColor.whiteColor(),
                NSBackgroundColorAttributeName: UIColor.clearColor()
            ])
        let attrLine = NSMutableAttributedString(attributedString: attrFirstLine)
        attrLine.appendAttributedString(attrSecondLine)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .Center
        attrLine.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSRange(location: 0, length: attrLine.length) )
        
        return attrLine as NSAttributedString
    }
}
