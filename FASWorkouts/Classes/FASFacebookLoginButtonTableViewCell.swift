//
//  FASFacebookLoginButtonTableViewCell.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 6/21/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit
// Facebook SDK
import FBSDKCoreKit
import FBSDKLoginKit

class FASFacebookLoginButtonTableViewCell: UITableViewCell {

    let fbLoginButton: FBSDKLoginButton = FBSDKLoginButton()
    let transparentView: UIView = UIView()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    private func commonInit() {
        transparentView.backgroundColor = UIColor.blackColor()
        transparentView.alpha = 0.5
        contentView.addSubview(transparentView)
        contentView.addSubview(fbLoginButton)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        fbLoginButton.center = contentView.center
        transparentView.frame = contentView.bounds
    }
}
