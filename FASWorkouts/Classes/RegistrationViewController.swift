//
//  RegistrationViewController.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/11/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

private var kLoggingIn = 0 // a global context variable used for KVO

class RegistrationViewController: FASAuthenticationViewController {
    // MARK: Properties
    
    var registerAPI: FASAPI?
    
    var emailTextField: FASTextField?
    var passwordTextField: FASTextField?
    var confirmPasswordTextField: FASTextField?
    
    var signUpButtonCellRow: Int {
        get {
            return fieldsCellCount
        }
    }
    
    var busy: Bool = false {
        didSet {
            // WORKAROUND: to fix <Issue #51: "sign up" and "sign in" form not fixed on screens>
            // instead of refreshing the entire tableView here (as it was done previously), we merely refresh a single row
            // doing so, we don't hide keyboard and let the iOS handle it. There are basically two circumstances when iOS hides it:
            // 1. when UIAlertView object is shown
            // 2. when VC is deallocated
            
            // refresh the last row only (supposing it is a SIGN UP button)
            let signUpIP  = NSIndexPath(forRow: fieldsCellCount, inSection: AuthenticationFormTableViewSection.Fields.rawValue)
            self.tableView.reloadRowsAtIndexPaths([signUpIP], withRowAnimation: .None)
        }
    }
    
    // the flag to prevent 'Cannot remove an observer' crash
    var shouldRemoveSessionObserver: Bool = false
    
    // MARK: View Life Cycle
    
    override init(sessionUIDelegate: SessionUIDelegate) {
        super.init(sessionUIDelegate: sessionUIDelegate)
        fieldsCellCount = 3
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        registerAPI?.cancel()
        registerAPI = nil
        if shouldRemoveSessionObserver {
            FASAPISession.currentSession().removeObserver(self, forKeyPath: "loggingIn", context: &kLoggingIn)
        }
        
        let notifCenter = NSNotificationCenter.defaultCenter()
        notifCenter.removeObserver(self)
    }
    
    // MARK: Actions
    
    private func doRegister() {
        printlnDebugTrace("doRegister")
        // Note: the server takes the 'username' parameter, however the email is passed
        // We check for valid email on the client (server also does this kind of checking)
        registerAPI = FASAPI.registerAccount(emailTextField?.text, password: passwordTextField?.text, passwordConfirmation: confirmPasswordTextField?.text, delegate: self)
        registerAPI?.invoke()
    }
    
    private func doLoginAfterRegistration() {
        FASAPISession.currentSession().addObserver(self, forKeyPath: "loggingIn", options: .New, context: &kLoggingIn)
        shouldRemoveSessionObserver = true
        FASAPISession.currentSession().login()
    }
    
    // MARK: KVO
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if context == &kLoggingIn {
            dispatch_async(dispatch_get_main_queue(), {
                self.busy = FASAPISession.currentSession().loggingIn
            })
        } else {
            super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)
        }
    }
}

extension RegistrationViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        if let sectionNumber = AuthenticationFormTableViewSection(rawValue: indexPath.section)  {
            switch sectionNumber {
            case .Title:
                // title
                cell = self.tableView(tableView, titleCellForIndexPath: indexPath)
            case .Fields:
                if indexPath.row != signUpButtonCellRow {
                    // fields
                    cell = self.tableView(tableView, textFieldCellForIndexPath: indexPath)
                }
                else {
                    // sign up button
                    cell = self.tableView(tableView, asyncActionCellForIndexPath: indexPath)
                }
            case .Facebook:
                // facebook button
                cell = self.tableView(tableView, facebookCellForIndexPath: indexPath)
            }
        }
        else {
            assert(false, "[ASSERTION]: cell will be nil")
            cell = nil
        }
        
        return cell!
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sectionNumber = AuthenticationFormTableViewSection(rawValue: section)  {
            switch sectionNumber {
            case .Title:
                return 1
            case .Fields:
                return fieldsCellCount + 1
            case .Facebook:
                return 1
            }
        }
        else {
            assert(false, "[ASSERTION]: cell will be nil")
            return 0
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    // TableView DataSource Helpers
    func tableView(tableView: UITableView, textFieldCellForIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(textFieldCellIdentifier) as! FASTextFieldTableViewCell
        if (cell.tag == 0) {
            cell.textField.delegate = self
            let notifCenter = NSNotificationCenter.defaultCenter()
            notifCenter.addObserver(self, selector: "textFieldDidChange:", name: UITextFieldTextDidChangeNotification, object: cell.textField)
            cell.textField.placeholderColor = UIColor.whiteColor()
            cell.textField.font = UIFont.regularBabasFontOfSize(25.0)
            cell.textField.keyboardAppearance = .Dark
            cell.tag = 1
        }
        
        switch indexPath.row {
        case 0:
            cell.leftImageView.image = UIImage(named: "IconEmail")
            cell.textField.placeholder = "EMAIL"
            emailTextField = cell.textField
            emailTextField!.secureTextEntry = false
            cell.textField.returnKeyType = .Next
            cell.textField.keyboardType = .EmailAddress
        case 1:
            cell.leftImageView.image = UIImage(named: "IconPassword")
            cell.textField.placeholder = "PASSWORD"
            passwordTextField = cell.textField
            passwordTextField!.secureTextEntry = true
            cell.textField.returnKeyType = .Next
            cell.textField.keyboardType = .Default
        case 2:
            cell.leftImageView.image = UIImage(named: "IconPassword")
            cell.textField.placeholder = "REPEAT PASSWORD"
            confirmPasswordTextField = cell.textField
            confirmPasswordTextField!.secureTextEntry = true
            cell.textField.returnKeyType = .Join
            cell.textField.keyboardType = .Default
        default:
            printlnDebugWarning("Cannot configure view because of unexpected indexPath.row: \(indexPath.row)")
        }
        
        cell.textField.setTransparencyIfNeeded()
        
        return cell
    }
    
    func tableView(tableView: UITableView, titleCellForIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier(titleCellIdentifier) as! FASTitleTableViewCell
        if cell.tag == 0 {
            cell.titleLabel.text = "CREATE NEW ACCOUNT"
            cell.titleLabel.textColor = UIColor.whiteColor()
            cell.tag = 1
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, asyncActionCellForIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(asyncActionCellIdentifier) as! FASAsynchronousActionTableViewCell
        if cell.tag == 0 {
            cell.titleLabel.text = "SIGN UP"
            cell.titleLabel.font = UIFont.boldBabasFontOfSize(25.0)
            cell.titleLabel.textColor = UIColor.yellowColor()
            cell.tag = 1
        }
        
        cell.busy = self.busy
        cell.userInteractionEnabled = !self.busy;
        
        return cell
    }
}

extension RegistrationViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.clearColor()
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int)  -> CGFloat {
        let footerHeight: CGFloat = 4.0
        return footerHeight
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if let sectionNumber = AuthenticationFormTableViewSection(rawValue: indexPath.section)  {
            if sectionNumber == .Fields && indexPath.row == signUpButtonCellRow {
                doRegister()
            }
                
            else if sectionNumber == .Facebook {
                doFacebookLogin()
            }
        }
        
    }
    
    func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if let sectionNumber = AuthenticationFormTableViewSection(rawValue: indexPath.section)  {
            if (sectionNumber == .Fields && indexPath.row == signUpButtonCellRow)
                || sectionNumber == .Facebook {
                return true
            }
        }
        return false
    }
    
    func tableView(tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        // footers are used for decoration only, so transparent
        view.alpha = 0.0
    }
}

extension RegistrationViewController: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField === confirmPasswordTextField {
            doRegister()
        }
        else {
            var nextField: UITextField?
            if textField === emailTextField {
                nextField = passwordTextField
            }
            else if textField === passwordTextField {
                nextField = confirmPasswordTextField
            }
            nextField!.becomeFirstResponder()
        }
        
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        // TODO: prevent non-english input
        
        // preventing whitespace input
        let whitespaceSet = NSCharacterSet.whitespaceCharacterSet()
        let range = string.rangeOfCharacterFromSet(whitespaceSet)
        if let _ = range {
            return false
        }
        else {
            return true
        }
    }
    
    // MARK: NSNotificationCenter callback
    
    func textFieldDidChange(notification: NSNotification) {
        if let textField = notification.object as? UITextField {
            textField.setTransparencyIfNeeded()
        }
    }
}

extension RegistrationViewController: FASAPIDelegate {
    
    func validatePreconditionsForFASAPI(theFASAPI: FASAPI!, apiError: AutoreleasingUnsafeMutablePointer<FASAPIError?>) -> Bool {
        if theFASAPI.method == .Register {
            var isValid: Bool = true
            
            // TODO: move it to the single SignUpFormValidator
            let emailValidator: EmailValidator = EmailValidator()
            emailValidator.input = emailTextField!.text
            isValid = emailValidator.validateWithError(nil) // TODO: currently returning meaningfull error is not supported
            if !isValid {
                apiError.memory = FASAPIError(code: .EmailInvalid, value: nil)
                return false
            }
            
            let passwordValidator: PasswordValidator = PasswordValidator()
            passwordValidator.input = passwordTextField!.text
            isValid = passwordValidator.validateWithError(nil) // TODO: currently returning meaningfull error is not supported
            if !isValid {
                apiError.memory = FASAPIError(code: .PasswordInvalid, value: nil)
                return false
            }
            
            isValid = (passwordTextField!.text == confirmPasswordTextField!.text) as Bool
            if !isValid {
                apiError.memory = FASAPIError(code: .PasswordConfirmationInvalid, value: "Invalid Confirm Password")
                return false
            }
        }
        
        return true
    }
    
    func FASAPIdidInvoke(theFASAPI: FASAPI!) {
        dispatch_async(dispatch_get_main_queue(), {
            self.busy = true
        })
    }
    
    func FASAPIdidFinish(theFASAPI: FASAPI!) {
        // set API object to callback, so session can get crendentials from it
        FASAPISession.currentSession().FASAPIdidFinish(theFASAPI)
        self.registerAPI = nil
        doLoginAfterRegistration()
    }
    
    func FASAPIdidFail(theFASAPI: FASAPI!) {
        dispatch_async(dispatch_get_main_queue(), {
            self.busy = false
            if theFASAPI.errors.count != 0 {
                FASAPIError.displayAlertWithErrors(theFASAPI.errors as [AnyObject])
                self.registerAPI = nil
            }
        })
    }
}