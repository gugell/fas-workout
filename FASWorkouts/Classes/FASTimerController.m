//
//  FASTimer.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/9/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "FASTimerController.h"
#import "UBILogger.h"
#import <UIKit/UIKit.h>

static NSTimeInterval const kFireIntervalHighUse = 0.02f;
static NSTimeInterval const kFireIntervalDefault = 0.1f;

@interface FASTimerController ()

@property (strong)                          NSTimer *       timer;
@property (nonatomic, strong)               NSDate *        startCountDate;
@property (nonatomic, strong)               NSDate *        pausedTime;
@property (nonatomic, strong)               NSDate *        date1970;
@property (nonatomic, strong)               NSDate *        timeToCountOff;
@property (unsafe_unretained, readwrite)    BOOL            counting;
@property (nonatomic)                       NSTimeInterval  userTime;

- (void)targetMethod:(NSTimer*)theTimer;
- (void)setStopWatchTime:(NSTimeInterval)time;
- (void)setCountDownTime:(NSTimeInterval)time;

@end

@implementation FASTimerController

- (instancetype)init
{
    return [self initWithType:FASTimerControllerTypeCountdownTimer
                     userTime:1.0
                         mode:FASTimerControllerModeDefault];
}

- (instancetype)initWithType:(FASTimerControllerType)type
                    userTime:(NSTimeInterval)userTime
                        mode:(FASTimerControllerMode)mode
{
    self = [super init];
    if (self) {
        _mode = mode;
        _type = type;
        _date1970 = [NSDate dateWithTimeIntervalSince1970:0];
        _startCountDate = nil;
        _pausedTime = nil;
        
        switch (_type) {
            case FASTimerControllerTypeCountdownTimer: {
                [self setCountDownTime:userTime];
            } break;
            case FASTimerControllerTypeStopwatch: {
                [self setStopWatchTime:userTime];
            } break;
            default:
                break;
        }
        
        _timer = nil;
        _counting = NO;
        self.resetTimerAfterFinish = NO;
        
        [self targetMethod:nil];
    }
    return self;
}

- (NSDictionary *)userInfo {
    return @{ @"StartDate" : [NSDate date] };
}

- (void)setStopWatchTime:(NSTimeInterval)time
{
    self.userTime = (time < 0) ? 0 : time;
    if (self.userTime > 0) {
        self.startCountDate = [[NSDate date] dateByAddingTimeInterval:-self.userTime];
        self.pausedTime = [NSDate date];
    }
}

- (void)setCountDownTime:(NSTimeInterval)time
{
    self.userTime = (time < 0) ? 0 : time;
    self.timeToCountOff = [self.date1970 dateByAddingTimeInterval:self.userTime];
}

- (NSTimeInterval)getTimeCounted
{
    if(!self.startCountDate) return 0;
    NSTimeInterval countedTime = [[NSDate date] timeIntervalSinceDate:self.startCountDate];
    
    if(self.pausedTime != nil){
        NSTimeInterval pauseCountedTime = [[NSDate date] timeIntervalSinceDate:self.pausedTime];
        countedTime -= pauseCountedTime;
    }
    return countedTime;
}

- (NSTimeInterval)getTimeRemaining
{
    if (self.type == FASTimerControllerTypeCountdownTimer) {
        return self.userTime - [self getTimeCounted];
    }
    return 0;
}

#if NS_BLOCKS_AVAILABLE
-(void)startWithEndingBlock:(void(^)(NSTimeInterval))end{
    self.endedBlock = end;
    [self start];
}
#endif

- (void)start
{
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
    
    NSTimeInterval timeInterval = (self.mode == FASTimerControllerModeHighUse) ? kFireIntervalHighUse : kFireIntervalDefault;
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:timeInterval
                                                  target:self
                                                selector:@selector(targetMethod:)
                                                userInfo:[self userInfo]
                                                 repeats:YES];


    if (self.timer != nil) {
        NSRunLoop *runLoop = [NSRunLoop currentRunLoop];
        [runLoop addTimer:self.timer
                  forMode:UITrackingRunLoopMode];
        
        if(self.startCountDate == nil){
            self.startCountDate = [NSDate date];
            
            if (self.type == FASTimerControllerTypeStopwatch) {
                self.startCountDate = [self.startCountDate dateByAddingTimeInterval:(self.userTime<0) ? 0 : -self.userTime];
            }
        }
        if (self.pausedTime != nil){
            NSTimeInterval countedTime = [self.pausedTime timeIntervalSinceDate:self.startCountDate];
            self.startCountDate = [[NSDate date] dateByAddingTimeInterval:-countedTime];
            self.pausedTime = [NSDate date];
        }
        
        self.counting = YES;
        [self.timer fire];
    }
}

- (void)stop {
    [self.timer invalidate];
    self.timer = nil;
}

- (void)pause {
   if(self.counting){
        [self.timer invalidate];
        self.timer = nil;
        self.counting = NO;
        self.pausedTime = [NSDate date];
       
   }

}

-(void) skip:(double)timeToSkip {
    
    if(self.counting){
        [self.timer invalidate];
        self.timer = nil;
        self.counting = NO;
        // here need logic for calculate delta
        
        
        NSTimeInterval timeInterval = (self.mode == FASTimerControllerModeHighUse) ? kFireIntervalHighUse : kFireIntervalDefault;
        
        self.timer = [NSTimer scheduledTimerWithTimeInterval:timeInterval
                                                      target:self
                                                    selector:@selector(targetMethod:)
                                                    userInfo:[self userInfo]
                                                     repeats:YES];
        
      
        self.startCountDate = [[NSDate date] dateByAddingTimeInterval: - timeToSkip];
        self.pausedTime = [NSDate date];
        
        self.counting = YES;
        [self.timer fire];
        
    }
    
    
   

    
}

- (void)reset {
    self.pausedTime = nil;
    self.userTime = (self.type == FASTimerControllerTypeStopwatch) ? 0 : self.userTime;
    self.startCountDate = (self.counting)? [NSDate date] : nil;
    [self targetMethod:nil];
}

- (void)targetMethod:(NSTimer*)theTimer
{
//    NSParameterAssert([theTimer isEqual:self.timer]);

    // get user info from timer object
//    NSDate *startDate = [[theTimer userInfo] objectForKey:@"StartDate"];
//    UBILogTrace(@"Timer started on %@", startDate);
    
    NSTimeInterval timeDiff = [[NSDate date] timeIntervalSinceDate:self.startCountDate];
    NSDate *timeToShow = [NSDate date];

    switch (self.type) {
        case FASTimerControllerTypeStopwatch: {
            
            [self.delegate timerController:self didCountToTime:timeDiff];
            
            if (self.counting) {
                timeToShow = [self.date1970 dateByAddingTimeInterval:timeDiff];
            }else{
                timeToShow = [self.date1970 dateByAddingTimeInterval:(!self.startCountDate)?0:timeDiff];
            }
        } break;
            
        case FASTimerControllerTypeCountdownTimer: {
            
            if (self.counting) {
                NSTimeInterval timeLeft = self.userTime - timeDiff;
                [self.delegate timerController:self didCountToTime:timeLeft];
                
                if(timeDiff >= self.userTime) {
                    // timeDiff has exceeded userTime => we stop here
                    [self pause];
                    timeToShow = [self.date1970 dateByAddingTimeInterval:0];
                    self.startCountDate = nil;
                    
                    [_delegate timerController:self didFinishCountdownWithTime:self.userTime];
                    
#if NS_BLOCKS_AVAILABLE
                    if(_endedBlock != nil){
                        _endedBlock(self.userTime);
                    }
#endif
                    if(self.resetTimerAfterFinish){
                        [self reset];
                        return;
                    }
                    
                }
                else {
                    timeToShow = [self.timeToCountOff dateByAddingTimeInterval:(timeDiff*-1)]; //added 0.999 to make it actually counting the whole first second
                }
            }
            else {
                // this value is returned to timerController:didCountToDate: delegate BEFORE the start
                timeToShow = self.timeToCountOff;
            }
        } break;
            
        default: {
            UBILogWarning(@"unexpected timer controller type");
            timeToShow = nil;
        } break;
    }
    
    [self.delegate timerController:self didCountToDate:timeToShow];
}

@end
