//
//  UBINullableSortDescriptor.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/10/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import Foundation

class UBINullableSortDescriptor: NSSortDescriptor {
    
    override init(key: String?, ascending: Bool) {
        super.init(key: key, ascending: ascending)
    }
    
    required override init(key: String?, ascending: Bool, selector: Selector) {
        super.init(key: key, ascending: ascending, selector: selector)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func copyWithZone(zone: NSZone) -> AnyObject {
        let thisClassType = self.dynamicType
        return thisClassType.init(key: self.key!, ascending:self.ascending, selector:self.selector)
    }
    
    override func compareObject(object1: AnyObject, toObject object2: AnyObject) -> NSComparisonResult {
        if self.isNull(object: object1.valueForKey(self.key!)/*object1[self.key!]*/) {
            if self.isNull(object: object2.valueForKey(self.key!)/*object2[self.key!]*/) {
                return .OrderedSame
            }
            return .OrderedDescending
        }
        
        if self.isNull(object: object2.valueForKey(self.key!)/*object2[self.key!]*/) {
            return .OrderedAscending
        }
        
        return super.compareObject(object1, toObject: object2)
    }
    
    override var reversedSortDescriptor: AnyObject {
        get {
            let thisClassType = self.dynamicType
            let asc: Bool = self.ascending
            let desc = !asc
            return thisClassType.init(key: self.key!, ascending: desc, selector:self.selector)
        }
    }
    
    // Note: there is no reason to this being w/ 'internal' access level,
    // but it is so we are able to test the class's internals w/ XCTest
    internal func isNull(object object: AnyObject?) -> Bool {
        // (a) == nil
        if let notNil: AnyObject = object {
            // [(a) isEqual:[NSNull null]]
            let nsNullObject = NSNull()
            if let derivesFromNSObject = notNil as? NSObject {
                if derivesFromNSObject == nsNullObject {
                    return true
                }
            }
            return false
        }
        else {
            return true
        }
    }
}
