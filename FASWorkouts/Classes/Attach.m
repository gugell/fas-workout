//
//  Attach.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/15/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "Attach.h"
#import "Exercise.h"
#import "FASWorkouts-Swift.h"
#import "UBILogger.h"

static UBIAttachFileCache *sAttachFileCache = nil;

@interface Attach (FilenameAccessors)
- (NSString *)primitiveFilename;
- (void)setPrimitiveFilename:(NSString *)filename;
@end

@interface Attach (FilepathAccessors)
- (NSURL *)primitiveFilepath;
- (void)setPrimitiveFilepath:(NSURL *)filepath;
@end

@interface Attach (ExtensionAccessors)
- (NSString *)primitiveExtension;
- (void)setPrimitiveExtension:(NSString *)extension;
@end

@interface Attach (FilenameWithExtensionAccessors)
- (NSString *)primitiveFilenameWithExtension;
- (void)setFilenameWithExtension:(NSString *)filenameWithExtension;
@end

@interface Attach ()
@property (nonatomic, retain, readwrite) NSDate * createdOn;
@property (nonatomic, retain, readwrite) NSNumber * attach_id;
@property (nonatomic, retain, readwrite) NSString * lang;
@property (nonatomic, retain, readwrite) NSString * type;
@property (nonatomic, retain, readwrite) NSString * md5;
@property (nonatomic, retain, readwrite) NSString * ct;
@property (nonatomic, retain, readwrite) NSString * origName;
@property (nonatomic, retain, readwrite) NSDate * mi_modifyDate;
@end

@implementation Attach
// attributes
@dynamic createdOn;
@dynamic attach_id;
@dynamic lang;
@dynamic type;
@dynamic md5;
@dynamic ct;
@dynamic origName;
@dynamic mi_modifyDate;

// relationships
@dynamic exercise;
// transient
@dynamic downloadProgress;

+ (void)initialize
{
    if (!sAttachFileCache) {
        sAttachFileCache = [UBIAttachFileCache new];
    }
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark CoreData overrides
#pragma mark -
///////////////////////////////////////////////

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    self.createdOn = [NSDate date];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark UBIUpdatableWithDictionary protocol
#pragma mark -
///////////////////////////////////////////////

- (void)addValuesFromDictionary:(NSDictionary *)dict
{
    for (NSString* key in [dict allKeys]) {
        
        id value = [dict valueForKey:key];
        
        if ([key isEqualToString:@"file"]) {
            [self processFileMetadataFromDictionary:(NSDictionary *)value];
        } else if ([key isEqualToString:@"mi_modifyDate"]) {
            // TODO: the format is different than other modify date
//            self.mi_modifyDate = [NSDate dateFromUnityBaseString:value];
        } else {
            [self setValue:value forKey:key];
        }
    }
}

- (void)processFileMetadataFromDictionary:(NSDictionary *)dict
{
    self.md5 = dict[@"md5"];
    self.ct = dict[@"ct"];
    self.origName = dict[@"origName"];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark NSKeyValueCoding protocol
#pragma mark -
///////////////////////////////////////////////

- (id)valueForUndefinedKey:(NSString *)key
{
    return nil;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    UBILogWarning(@"%@: UNDEFINED value = %@\tkey = %@", NSStringFromClass([self class]), value, key);
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark File
#pragma mark -
///////////////////////////////////////////////
- (NSString *)filenameWithExtension
{
    [self willAccessValueForKey:@"filenameWithExtension"];
    NSString* filenameWithExtension = [self primitiveFilenameWithExtension];
    [self didAccessValueForKey:@"filenameWithExtension"];
    if (filenameWithExtension == nil) {
        NSString* aFilename = self.filename;
        NSString* anExtension = self.extension;
        if (aFilename != nil) {
            if (anExtension != nil && ![anExtension isEqualToString:@""]) {
                filenameWithExtension = [@[aFilename, anExtension] componentsJoinedByString:@"."];
            }
            else {
                filenameWithExtension = aFilename;
            }
        }
    }
    return filenameWithExtension;
}

+ (NSSet *)keyPathsForValuesAffectingFilenameWithExtension {
    return [NSSet setWithObjects:@"filename", @"extension", nil];
}

- (NSURL *)filepath
{
    [self willAccessValueForKey:@"filepath"];
    NSURL* tmpFilepath = [self primitiveFilepath];
    [self didAccessValueForKey:@"filepath"];
    if (tmpFilepath == nil) {
        NSString* aFullFilename = [self filenameWithExtension];
        if (aFullFilename != nil) {
            tmpFilepath = [sAttachFileCache cacheURLPathForFile:[self filenameWithExtension]];
            [self setPrimitiveFilepath:tmpFilepath];
        }
    }
    
    return [tmpFilepath copy];
}

+ (NSSet *)keyPathsForValuesAffectingFilepath {
    return [NSSet setWithObject:@"filenameWithExtension"];
}

- (NSString *)filename
{
    [self willAccessValueForKey:@"filename"];
    NSString *filename = [self primitiveFilename];
    [self didAccessValueForKey:@"filename"];
    if (filename == nil) {
        if (self.type && self.attach_id) {
            filename = [@[self.type, self.attach_id] componentsJoinedByString:@"_id"];
            [self setPrimitiveFilename:filename];
        }
    }
    
    return filename;
}

+ (NSSet *)keyPathsForValuesAffectingFilename {
    return [NSSet setWithObjects:@"type", @"attach_id", nil];
}

- (NSString *)extension
{
    [self willAccessValueForKey:@"extension"];
    NSString* extension = [self primitiveExtension];
    [self didAccessValueForKey:@"extension"];
    if (extension == nil) {
        if (self.ct) {
            extension = [self.ct stringByReplacingOccurrencesOfString:@"application/" withString:@""];
            [self setPrimitiveExtension:extension];
        }
    }
    
    return extension;
}

+ (NSSet *)keyPathsForValuesAffectingExtension {
    return [NSSet setWithObject:@"ct"];
}

- (AttachContentType)contentType
{
    if ([self.type isEqualToString:@"video"]) {
        return AttachContentTypeVideo;
    } else if ([self.type isEqualToString:@"photo"]) {
        return AttachContentTypePhoto;
    } else {
        return AttachContentTypeUnknown;
    }
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Actions
#pragma mark -
///////////////////////////////////////////////

- (void)downloadFile
{
    [self downloadFile:NO];
}

- (AttachDownloadRetval)downloadFile:(BOOL)force;
{
    return [sAttachFileCache downloadDocumentWithID:self.attach_id
                                             saveAs:self.filenameWithExtension
                                              force:force];
}

- (BOOL)isFileDownloaded
{
    return [sAttachFileCache isFileExists:self.filenameWithExtension withMD5:self.md5];
}

+ (UBIAttachFileCache *)fileCache
{
    return sAttachFileCache;
}

@end

