//
//  UBISlideView.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/9/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit

protocol UBISlideViewItem {
    var text: NSString { get }
}

protocol UBISlideViewDataSource {
    func numberOfItemsForSlideView(slideView: UBISlideView) -> Int
    func slideView(slideView: UBISlideView, itemForIndex index: Int) -> UBISlideViewItem
}

class UBISlideView: UIView {
    private var pageCount: Int = 0
    private var pageViews: [UILabel?] = []
    private var timerInterval: NSTimeInterval = 0.0
    private var timer: NSTimer?
    
    var dataSource: UBISlideViewDataSource? {
        didSet {
            if let _ = dataSource {
                pageCount = dataSource!.numberOfItemsForSlideView(self)
                
                pageControl.currentPage = 0
                pageControl.numberOfPages = pageCount
                
                for _ in 0..<pageCount {
                    pageViews.append(nil)
                }
            }
        }
    }
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    // MARK: UIKit overrides
    override func awakeFromNib() {
        scrollView.delegate = self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layoutPages()
    }
    
    // MARK: controls
    
    func startAnimating(duration: NSTimeInterval) {
        timerInterval = duration
        startTimer()
        loadVisiblePages()
    }
    
    func stopAnimating() {
        stopTimer()
    }
    
    // MARK: Helpers
    
    private func createLabel(text: NSString) -> UILabel {
        let label = UILabel()
        label.numberOfLines = 2
        label.text = text as String
        label.font = UIFont.regularBabasFontOfSize( 24.0 )
        label.textColor = UIColor.whiteColor()
        label.textAlignment = .Center
        label.lineBreakMode = .ByWordWrapping
        return label
    }
    
    private func loadVisiblePages() {
        let pagesScrollViewSize = scrollView.frame.size
        scrollView.contentSize = CGSize(width: pagesScrollViewSize.width * CGFloat(pageCount), height: pagesScrollViewSize.height)
        
        // First, determine which page is currently visible
        let pageWidth = scrollView.frame.size.width
        let page = Int(floor((scrollView.contentOffset.x * 2.0 + pageWidth) / (pageWidth * 2.0)))
        
        // Update the page control
        pageControl.currentPage = page
        
        // Work out which pages you want to load
        let firstPage = page - 1
        let lastPage = page + 1
        
        // Purge anything before the first page
        for var index = 0; index < firstPage; ++index {
            purgePage(index)
        }
        
        // Load pages in our range
        for index in firstPage...lastPage {
            loadPage(index)
        }
        
        // Purge anything after the last page
        for var index = lastPage+1; index < pageCount; ++index {
            purgePage(index)
        }
    }
    
    private func loadPage(page: Int) {
        if page < 0 || page >= pageCount {
            // If it's outside the range of what you have to display, then do nothing
            return
        }
        
        if let _ = pageViews[page] {
            // Do nothing. The view is already loaded.
        } else {
            var frame = scrollView.bounds
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0.0
            
            let newPageView = createLabel(dataSource!.slideView(self, itemForIndex: page).text)
            newPageView.contentMode = .ScaleAspectFit
            newPageView.frame = frame
            scrollView.addSubview(newPageView)
            
            pageViews[page] = newPageView
        }
    }
    
    private func purgePage(page: Int) {
        if page < 0 || page >= pageCount {
            // If it's outside the range of what you have to display, then do nothing
            return
        }
        
        // Remove a page from the scroll view and reset the container array
        if let pageView = pageViews[page] {
            pageView.removeFromSuperview()
            pageViews[page] = nil
        }
    }
    
    private func layoutPages() {
        var frame = scrollView.bounds
        
        for var index = 0; index < pageCount; ++index {
            if let pageView = pageViews[index] {
                frame.origin.x = frame.size.width * CGFloat(index)
                frame.origin.y = 0.0
                pageView.frame = frame
            }
        }
    }
    
    private func scrollToNextPage() {
        var nextPageIndex = pageControl.currentPage + 1
        if nextPageIndex >= pageCount {
           nextPageIndex = 0
        }
        
        scrollView.setContentOffset(CGPoint(x: scrollView.frame.size.width * CGFloat(nextPageIndex), y: 0.0), animated: true)
    }
    
    // MARK: timer
    
    private func startTimer() {
        stopTimer()
        timer = NSTimer.scheduledTimerWithTimeInterval(timerInterval, target: self, selector: "targetMethod:", userInfo: nil, repeats: true)
    }
    
    private func stopTimer() {
        timer?.invalidate()
        timer = nil;
    }
    
    @objc func targetMethod(timer: NSTimer) {
        scrollToNextPage()
    }
}

extension UBISlideView: UIScrollViewDelegate {
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        // Load the pages that are now on screen
        loadVisiblePages()
    }
}


