//
//  Faq.m
//  FASWorkouts
//
//  Created by Vladimir Stepanchenko on 9/17/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "FAQItem.h"
#import "FASAPIObjectKeys.h"
#import "FASData.h" // <== cross-reference
#import "NSString+MyExtensions.h"
#import "UBILogger.h"

@implementation FAQItem

@dynamic faq_id;
@dynamic sortIndex;
@dynamic question;
@dynamic answer;

///////////////////////////////////////////////
#pragma mark -
#pragma mark UBIUpdatableWithDictionary protocol conformance
#pragma mark -
///////////////////////////////////////////////
- (void)addValuesFromDictionary:(NSDictionary *)dict
{
    for (NSString *key in [dict allKeys]) {
        
        id value = [dict valueForKey:key];
        
        // if value is an array
        if ([value isKindOfClass:[NSArray class]]) {
            UBILogWarning(@"Value (%@) is an array", value);
            
            continue;
        }
        
        // if value is a dictionary
        // else, add it directly
        else {
            
            if ([value isKindOfClass:[NSNull class]]) { continue; }
            if ([key isEqualToString:FASAPIIdentifier]) {
                self.faq_id = dict[FASAPIIdentifier];
                continue;
            }
            
            [self setValue:value forKey:key];
        }
    }
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark NSKeyValueCoding protocol
#pragma mark -
///////////////////////////////////////////////

- (id)valueForUndefinedKey:(NSString *)key
{
    return nil;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    UBILogWarning(@"%@: UNDEFINED value = %@\tkey = %@", NSStringFromClass([self class]), value, key);
}

@end
