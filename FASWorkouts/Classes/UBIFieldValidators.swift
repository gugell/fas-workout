//
//  UBIFieldValidators.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/22/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import Foundation

// Note: originally, these classes were designed to work w/ Swift code, however
// for now we have to move the validation to API class. In order to be able to work w/ Swift classes
// from Obj-C we have to do:
// 1. add @objc
// 2. inherit from NSObject
// Doc: https://devforums.apple.com/message/973806
// TODO: when in All-in-Swidft project, just get rid of 1. and 2.

@objc protocol Validator {
    func validateWithError(error: NSErrorPointer) -> Bool
}
///////////////////////////////////////////////
// MARK: - EmailValidator -
///////////////////////////////////////////////
@objc class EmailValidator: NSObject, Validator {
    
    var input: String?
    
    func validateWithError(error: NSErrorPointer) -> Bool {
        if let nonEmptyInput = input {
            let regexPattern = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
            let regexPredicate = NSPredicate(format:"SELF MATCHES %@", regexPattern)
            let result = regexPredicate.evaluateWithObject(nonEmptyInput)
            // evaluating w/ regexp
            // TODO: create error object for it
            return result
        }
        
        return false
    }
}
///////////////////////////////////////////////
// MARK: - PasswordValidator -
///////////////////////////////////////////////
@objc class PasswordValidator: NSObject, Validator {
    
    var input: String?
    
    func validateWithError(error: NSErrorPointer) -> Bool {
        if let nonEmptyInput = input {
            
            let regexp = try! NSRegularExpression(pattern: "[a-zA-Z0-9_]{4,}", options: [])
            let inputRange = NSRange(location:0, length: nonEmptyInput.characters.count )
            let result = regexp.rangeOfFirstMatchInString(nonEmptyInput, options: .Anchored, range: inputRange)
            if result.location == NSNotFound  {
                // regexp validation failed
                // TODO: create error object for it
                return false
            }
            else {
                return true
            }
        }
        
        // empty input
        // Note: Regexp enforces to use at least 4 characters, so not-nil check might be not needed
        // TODO: create error object for it
        return false
    }
}


//
// NOTE: the below is commented as of Xcode 6.3. brought errors I could not really understand clear how to fix
//

/////////////////////////////////////////////////
//// MARK: - SetPasswordValidator -
/////////////////////////////////////////////////
//@objc class SetPasswordValidator: NSObject, Validator {
//    
//    let firstPasswordValidator = PasswordValidator()
//    let secondPasswordValidator = PasswordValidator()
//    
//    func validateWithError(error: NSErrorPointer) -> Bool {
//        if firstPasswordValidator.validateWithError(nil) && firstPasswordValidator.input == secondPasswordValidator.input {
//            return true
//        }
//        return false
//    }
//}
/////////////////////////////////////////////////
//// MARK: - SignIn Form Validator -
/////////////////////////////////////////////////
//@objc class SignUpFormValidator: NSObject, Validator {
//    let setPasswordValidator = SetPasswordValidator()
//    let emailValidator = EmailValidator()
//    
//    func validateWithError(error: NSErrorPointer) -> Bool {
//        if (setPasswordValidator.validateWithError(nil) && emailValidator.validateWithError(nil)) {
//            return true
//        }
//        else {
//            return false
//        }
//    }
//}
/////////////////////////////////////////////////
//// MARK: - SignUp Form Validator -
/////////////////////////////////////////////////
//@objc class SignInFormValidator: NSObject, Validator {
//    let passwordValidator = PasswordValidator()
//    let emailValidator = EmailValidator()
//    
//    func validateWithError(error: NSErrorPointer) -> Bool {
//        return passwordValidator.validateWithError(nil) && emailValidator.validateWithError(nil)
//    }
//}
