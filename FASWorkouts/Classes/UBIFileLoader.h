//
//  UBIFileLoader.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/16/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kUBIFileLoaderLoadingInfoURL; // required
extern NSString * const kUBIFileLoaderLoadingInfoId; // required
extern NSString * const kUBIFileLoaderLoadingInfoFilename; // optional

@class UBIFileLoader;

@protocol UBIFileLoaderDelegate <NSObject>
@optional
// reports start of downloading process
- (void)UBIFileLoader:(UBIFileLoader *)fileLoader didStartDownloadingWithUserInfo:(NSDictionary *)userInfo;
// reports progress
- (void)UBIFileLoader:(UBIFileLoader *)fileLoader didMakeProgress:(double)progress userInfo:(NSDictionary *)userInfo;
// reports file ready to be copied at target path
- (void)UBIFileLoader:(UBIFileLoader *)fileLoader didDownloadFileWithUserInfo:(NSDictionary*)userInfo toPath:(NSURL *)fileURL;
// reports final state of downloading process
- (void)UBIFileLoader:(UBIFileLoader *)fileLoader didFinishDownloadingWithUserInfo:(NSDictionary *)userInfo error:(NSError *)error;
@end

@interface UBIFileLoader : NSObject
<NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDownloadDelegate>

@property (nonatomic, weak) id<UBIFileLoaderDelegate> delegate;

@property (nonatomic, readonly) BOOL allowsCellularAccess; // defaut is NO
@property (nonatomic, copy) NSString * authorizationToken; // default is (null)

- (instancetype)initWithAuthorizationToken:(NSString *)token allowsCellularAccess:(BOOL)allowsCellularAccess NS_DESIGNATED_INITIALIZER;

- (BOOL)downloadFileWithLoadingInfo:(NSDictionary *)loadingInfo;

@end
