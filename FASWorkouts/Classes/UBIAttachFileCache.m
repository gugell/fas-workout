//
//  UBIAttachDownloadManager.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/17/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "UBIAttachFileCache.h"
#import "UBIFileLoader.h"
#import "FASAPISession.h"
#import "FASWorkouts-Swift.h"
#import "NSString+MyExtensions.h"
#import "Attach.h"
#import "FASData.h"
#import "UBILogger.h"
// md 5 calc
#import <CommonCrypto/CommonDigest.h>

#define SESSION_SIGNATURE 0

NSString * const UBIAttachFileCacheDirectory  = @"com.fas-sport.FASWorkouts.UBIAttachFileCache";

static const NSNumberFormatter* numberFormatter = nil;

@interface UBIAttachFileCache ()
<UBIFileLoaderDelegate>
@property (nonatomic, strong) UBIFileLoader * loader;
@property (nonatomic, strong) NSManagedObjectContext * moc;
//@property (nonatomic, strong) NSMutableDictionary * pendingAttachments;
@property (nonatomic, copy) NSURL * cachePath;
@end

@implementation UBIAttachFileCache

- (instancetype) init
{
    self = [super init];
    if (self) {
        _cachePath = nil;
        
        [[FASAPISession currentSession] addObserver:self];
        NSString* t = [FASAPISession currentSession].token;
        
        _loader = [[UBIFileLoader alloc] initWithAuthorizationToken:t allowsCellularAccess:YES];
        _loader.delegate = self;

//        _pendingAttachments = [NSMutableDictionary dictionary];
        
        [NSNotificationCenter.defaultCenter addObserver:self
                                               selector:@selector(didReceiveMemoryWarning)
                                                   name:UIApplicationDidReceiveMemoryWarningNotification
                                                 object:nil];
    }
    return self;
}

- (void)dealloc
{
    self.loader.delegate = nil;
    
    [NSNotificationCenter.defaultCenter
     removeObserver:self
     name:UIApplicationDidReceiveMemoryWarningNotification
     object:nil];
}

- (void)didReceiveMemoryWarning
{
    // TODO: clear the cache
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Helpers
#pragma mark -
///////////////////////////////////////////////
- (NSString *)fromID:(id)identifier
{
    return [NSString stringWithFormat:@"%@", identifier];
}

- (NSNumber *)toID:(NSString*)strIdentifier
{
    if (!numberFormatter) {
        numberFormatter = [[NSNumberFormatter alloc] init];
    }
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSNumber * number = [numberFormatter numberFromString:strIdentifier];
    return number;
}

- (Attach *)attachForAttachID:(NSNumber*)attachID
{
    if (attachID == nil || ![attachID isKindOfClass:[NSNumber class]]) {
        return nil;
    }
    
    NSManagedObjectContext* managedObjectContext = [FASData sharedData].mainContext;
    
    __block Attach* attach = nil;
    [managedObjectContext performBlockAndWait:^{
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity =
        [NSEntityDescription entityForName:@"Attach"
                    inManagedObjectContext:managedObjectContext];
        [request setEntity:entity];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", @"attach_id" , attachID];
        [request setPredicate:predicate];
        
        NSError *error;
        NSArray *array = [managedObjectContext executeFetchRequest:request error:&error];
        
        if (array != nil) {
            attach = array.firstObject;
        }
        else {
            UBILogError(@"%@", error.localizedDescription);
        }
    }];
    
    return attach;
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Actions
#pragma mark -
///////////////////////////////////////////////
- (AttachDownloadRetval)downloadDocumentWithID:(id)documentID
                                        saveAs:(NSString *)filename
                                         force:(BOOL)force
{
    if (!self.loader) {
        UBILogError(@"cannot download file %@ for documentID %@ because loader is nil", filename, documentID);
        return NO;
    }
    
    // reset auth token at the begining of the download, b/c it might change for some reason
    self.loader.authorizationToken = [FASAPISession currentSession].token;
    
    NSString* identifier = [self fromID:documentID];
    NSURL* url = [self downloadURLForID:identifier];
    
    // if not force download and file exist - return immediately
    if (!force && [self isFileExists:filename]) {
        return AttachDownloadExistsLocal;
    }
    
    NSDictionary* loadingInfo = @{kUBIFileLoaderLoadingInfoURL: url,
                                  kUBIFileLoaderLoadingInfoId: identifier,
                                  kUBIFileLoaderLoadingInfoFilename: filename};
    BOOL success = [self.loader downloadFileWithLoadingInfo:loadingInfo];
    
    return success;
}

//- (BOOL)downloadAttach:(Attach*)attach
//{
//    if (!self.loader) {
//        NSLog(@"[ERROR] cannot download Attach %@ because loader is nil", attach);
//        return NO;
//    }
//    
//    NSString* identifier = [NSString stringWithFormat:@"%@", attach.attach_id];
//    NSURL* url = [self downloadURLForID:identifier];
//    NSString* filename = [attach filenameWithExtension];
//    
//    if ([self isFileExists:filename]) {
//        // Do nothing (however, the function above does not guarantee, the file does exist)
//        return YES;
//    }
//    
//    NSDictionary* loadingInfo = @{kUBIFileLoaderLoadingInfoURL: url,
//                                  kUBIFileLoaderLoadingInfoId: identifier,
//                                  kUBIFileLoaderLoadingInfoFilename: filename};
//
//    BOOL success = [self.loader downloadFileWithLoadingInfo:loadingInfo];
//    if (success) {
//        [self.pendingAttachments setObject:attach forKey:identifier];
//    }
//    
//    return YES;
//}

///////////////////////////////////////////////
#pragma mark -
#pragma mark FASAPISessionObserver protocol
#pragma mark -
///////////////////////////////////////////////
- (void)FASAPISessionNotification:(NSNotification *)notification
{
    if (notification.name == FASAPISessionDidLoginNotification ||
        notification.name == FASAPISessionDidRestoreSessionNotification) {
        self.loader.authorizationToken = [FASAPISession currentSession].token;
    }
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark URL constructor
#pragma mark -
///////////////////////////////////////////////
- (NSURL *)downloadURLForID:(NSString *)attachId
{
    NSString* basePath = [FASEnvironment APIBasePath];
    BOOL isSecure = [FASEnvironment APISecure];
    NSString* strPath = nil;
    strPath = [NSString stringWithFormat:@"%@://%@/getDocument?entity=fas_exercise_attach&attribute=file&ID=%@", (isSecure?@"https":@"http"), basePath, attachId];
    return [NSURL URLWithString:strPath];
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark UBIFileLoader delegate
#pragma mark -
///////////////////////////////////////////////
- (void)UBIFileLoader:(UBIFileLoader *)fileLoader didStartDownloadingWithUserInfo:(NSDictionary *)userInfo
{
    NSParameterAssert(fileLoader == self.loader);
    
    UBILogTrace(@"Downloading started for user info:\n%@", userInfo);
}

- (void)UBIFileLoader:(UBIFileLoader *)fileLoader didMakeProgress:(double)progress userInfo:(NSDictionary *)userInfo
{
    NSParameterAssert(fileLoader == self.loader);
    
    NSNumber* attachID = [self toID:(NSString *)[userInfo objectForKey:kUBIFileLoaderLoadingInfoId]];
    Attach* pendingAttach = [self attachForAttachID:attachID];
    [pendingAttach.managedObjectContext performBlock:^{
        // HACK: set download progress to 1.0 only when file is copied (see below)
        if (floor(progress) < 1.0f) {
            pendingAttach.downloadProgress = @(progress);
        }
    }];
}

- (void)UBIFileLoader:(UBIFileLoader *)fileLoader didDownloadFileWithUserInfo:(NSDictionary*)userInfo toPath:(NSURL *)fileURL
{
    NSParameterAssert(fileLoader == self.loader);
    
    BOOL result = [self copyFile:fileURL
                          toPath:[self cacheURLPathForFile:[userInfo objectForKey:kUBIFileLoaderLoadingInfoFilename]]];
    if (result) {
        UBILogTrace(@"file copied %@", [self cacheURLPathForFile:[userInfo objectForKey:kUBIFileLoaderLoadingInfoFilename]]);
    }
    else {
        // TODO: what to do here?
        UBILogError(@"error during SAVE FILE operation");
    }
    
    // HACK: set download progress to 1.0 only when file is copied
    NSNumber* attachID = [self toID:(NSString *)[userInfo objectForKey:kUBIFileLoaderLoadingInfoId]];
    Attach* pendingAttach = [self attachForAttachID:attachID];
    [pendingAttach.managedObjectContext performBlock:^{
        pendingAttach.downloadProgress = @(1.0f);
    }];
}

- (void)UBIFileLoader:(UBIFileLoader *)fileLoader didFinishDownloadingWithUserInfo:(NSDictionary *)userInfo error:(NSError *)error
{
    NSParameterAssert(fileLoader == self.loader);
    
    if (error == nil) {
        
        // This is where we know for sure that download completed, and an error it completes with
        
//        Attach* att = [self.pendingAttachments objectForKey:userInfo[kUBIFileLoaderLoadingInfoId]];
//        if (att) {
//            /*
//            NSManagedObjectContext* c = att.managedObjectContext;
//            [c performBlock:^{
//                att.filepath = [self cacheURLPathForFile:userInfo[kUBIFileLoaderLoadingInfoFilename]];
//                if (![c save:NULL]) {
//                    // handle error
//                };
//            }];
//            */
//            [self.pendingAttachments removeObjectForKey:userInfo[kUBIFileLoaderLoadingInfoId]];
//        }
    }
    else {
        UBILogError(@"Finish downloading process with error. Error was:\n%@", error);
    }
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark File Manager
#pragma mark -
///////////////////////////////////////////////
- (NSURL *)createCacheDirectory {
    NSFileManager* fm = [NSFileManager defaultManager];
    
    NSURL* applicationCacheDirectory =
    [fm URLForDirectory:NSCachesDirectory
               inDomain:NSUserDomainMask
      appropriateForURL:nil
                 create:YES
                  error:nil];
    
    NSURL *storeURL = [applicationCacheDirectory URLByAppendingPathComponent:UBIAttachFileCacheDirectory];
    
    //  create directory if does not exist
    
    NSError* createDirectoryAtURLError = nil;
    if (![fm createDirectoryAtURL:storeURL withIntermediateDirectories:YES
                       attributes:nil error:&createDirectoryAtURLError])
    {
        UBILogError(@"Cannot create directory: %@. Error was:\n%@", storeURL, createDirectoryAtURLError);
        // Handle the error.
        return nil;
    }
    
    return storeURL;
}

- (NSURL*)cachePath
{
    if (!_cachePath) {
        _cachePath = [self createCacheDirectory];
    }
    
    return _cachePath;
}

- (BOOL)copyFile:(NSURL*)fromFilepath toPath:(NSURL*)toFilepath
{
    if ([NSThread currentThread] == [NSThread mainThread]) { UBILogWarning(@"Running in Main Thread!"); }
    
    //TODO: create an NSOperation for this
    
    // checking preconditions
    if (!fromFilepath) {
        UBILogError(@"Cannot copy - no file path to copy file from");
        return NO;
    }
    
    if (!toFilepath) {
        UBILogError(@"Cannot copy - no file path to copy file to");
        return NO;
    }
    
    // remove existed file (if any)
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    // TODO: check the file exists before attempting to remove it
    NSError* removeItemAtURLError;
    [fileManager removeItemAtURL:toFilepath error:&removeItemAtURLError];
    if (!removeItemAtURLError) {
        UBILogTrace(@"Successfully removed item from filepath:\n%@", toFilepath);
    }
    
    // start copying
    NSError *errorCopy;
    BOOL success = [fileManager copyItemAtURL:fromFilepath toURL:toFilepath error:&errorCopy];
    if (success) {
        return YES;
    }
    else {
        UBILogError(@"Error during the copy: %@", [errorCopy localizedDescription]);
        /*
         In the general case, what you might do in the event of failure depends on the error and the specifics of your application.
         */
        return NO;
    }
}

- (NSURL *)cacheURLPathForFile:(NSString *)filename
{
    NSURL *path = [self.cachePath URLByAppendingPathComponent:filename];
    return path;
}

- (NSString *)cachePathForFile:(NSString *)filename
{
    NSString* strPath = [self cacheURLPathForFile:filename].path;
    return strPath;
}

- (BOOL)isFileExists:(NSString *)filename
{
    return [self isFileExists:filename withMD5:nil];
}

- (BOOL)isFileExists:(NSString *)filename withMD5:(NSString*)md5Sum
{
    // From Apple Docs
    // Note: Attempting to predicate behavior based on the current state of the file system or a particular file on the file system is not recommended. Doing so can cause odd behavior or race conditions. It's far better to attempt an operation (such as loading a file or creating a directory), check for errors, and handle those errors gracefully than it is to try to figure out ahead of time whether the operation will succeed. For more information on file system race conditions, see “Race Conditions and Secure File Operations” in Secure Coding Guide.
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    
    BOOL fileExists = [fileManager fileExistsAtPath:[self cachePathForFile:filename]];
    if (!fileExists) {
        return NO;
    }
    
    if (md5Sum == nil || [md5Sum isBlank]) {
        return YES;
    }
    
    // MD5 sum check
    
    // Mapped reading option for better performance
    NSError* error = nil;
    NSData *fileData = [NSData dataWithContentsOfFile:[self cachePathForFile:filename]
                                              options:NSDataReadingMappedIfSafe
                                                error:&error];
    if (fileData)
    {
        // Create byte array of unsigned chars
        unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
        // Create 16 byte MD5 hash value, store in buffer
        // @note added conversion of fileData.length to CC_LONG
        CC_MD5(fileData.bytes, (CC_LONG)fileData.length, md5Buffer);
        // Convert unsigned char buffer to NSString of hex values
        NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
        for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
            [output appendFormat:@"%02x",md5Buffer[i]];
        }
        
        NSString* calcMD5sum = [output copy];
        return [calcMD5sum isEqualToString:md5Sum];
    }
    else {
        UBILogError(@"error while NSData mapping. Error was:\n%@", error);
        return NO;
    }
}

@end
