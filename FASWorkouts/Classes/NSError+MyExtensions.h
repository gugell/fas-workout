//
//  NSError+MyExtensions.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/28/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (MyExtensions)
- (NSString *)detailedInfo;
@end
