//
//  AttachmentsViewController.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/16/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

class AttachmentsViewController: FASViewController {
    // cell id
    let kAttachmentCellIdentifier = "AttachmentCellIdentifier"
    
    @IBOutlet weak var tableView: UITableView!
    
    var exercise: Exercise? {
        didSet {
            fetchAttachments()
            
//            tableView?.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        tableView.estimatedRowHeight = 44.0
//        tableView.rowHeight = UITableViewAutomaticDimension
        
//        fetchAttachments()
//        
//        tableView.reloadData()
    }
    
// MARK: Core Data
    
    private lazy var fetchedAttachmentsController: NSFetchedResultsController? = {
        let fetchRequest = NSFetchRequest(entityName: "Attach")
        
        var predicate: NSPredicate?
        if let theManagedObject = self.exercise {
            predicate = NSPredicate(format: "exercise = %@", theManagedObject)
        } else {
            predicate = NSPredicate(format: "exercise = %@", 0) // result likely would be nil
        }
        fetchRequest.predicate = predicate
        
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "attach_id", ascending: true)]
            
        let controller = NSFetchedResultsController(fetchRequest: fetchRequest,
            managedObjectContext: FASData.sharedData().mainContext,
            sectionNameKeyPath:/* "sectionIdentifier"*/nil,
            cacheName: nil)
        
        controller.delegate = self
        
        return controller
    }()
    
    private func fetchAttachments() {
        
        if let frc = fetchedAttachmentsController {
            var fetchError: NSError?
            let result: Bool
            do {
                try frc.performFetch()
                result = true
            } catch let error as NSError {
                fetchError = error
                result = false
            }
            if !result {
                if let error = fetchError {
                    // TODO: Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    printlnDebugError("Unresolved error \(error.localizedDescription),\nerror info: \(error.userInfo)")
                    abort()
                }
            }
        } else {
            printlnDebugError("Fetched Result Controller was nil upon fetch()")
        }
    }
}

///////////////////////////////////////////////
// MARK: - Fetched results controller -
///////////////////////////////////////////////

extension AttachmentsViewController: NSFetchedResultsControllerDelegate {
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.reloadData()
    }
}

///////////////////////////////////////////////
// MARK: - Table View -
///////////////////////////////////////////////

extension AttachmentsViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let obj: AnyObject? = fetchedAttachmentsController?.objectAtIndexPath(indexPath)
        if let att = obj as? Attach {
            printlnDebugTrace("attachment description = \(att.description)")
        }
    }
}

extension AttachmentsViewController: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if let frc = fetchedAttachmentsController {
            if let sections = frc.sections {
                return sections.count
            }
        }
        return 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedAttachmentsController!.sections![section].numberOfObjects
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return self.tableView(tableView, attachmentCellForRowAtIndexPath: indexPath) as UITableViewCell
    }
    
    // MARK: - Helpers
    private func configureAttachmentCell(attachCell: UITableViewCell, atIndexPath indexPath: NSIndexPath) {
        let attachObj = fetchedAttachmentsController!.objectAtIndexPath(indexPath) as! Attach
        attachCell.textLabel!.text = "\(attachObj.attach_id)"
        attachCell.detailTextLabel?.text = "\(attachObj.filename)"
    }
    
    private func tableView(tableView: UITableView, attachmentCellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let aCell = tableView.dequeueReusableCellWithIdentifier(kAttachmentCellIdentifier, forIndexPath: indexPath) 
        
        configureAttachmentCell(aCell, atIndexPath: indexPath)
        return aCell
    }
}