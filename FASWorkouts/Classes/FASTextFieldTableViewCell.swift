//
//  FASTextFieldTableViewCell.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/19/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

class FASTextFieldTableViewCell: UITableViewCell {
    
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var textField: FASTextField!
}
