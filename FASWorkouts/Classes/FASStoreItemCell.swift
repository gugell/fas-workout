//
//  FASStoreItemCell.swift
//  FASWorkouts
//
//  Created by Oleksandr Nechet on 07.12.15.
//  Copyright © 2015 FAS. All rights reserved.
//

import UIKit
import StoreKit

class FASStoreItemCell: UITableViewCell {
    //MARK: - Properties
    @IBOutlet weak private var viewContainer: UIView!
    @IBOutlet weak private var labelTitle: UILabel!
    @IBOutlet weak private var labelDescription: UILabel!
    @IBOutlet weak private var labelPrice: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK: -
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.clearColor()
        
        viewContainer.layer.borderColor = UIColor.fasYellowColor().CGColor
        viewContainer.layer.borderWidth = 1
    }
    
    override func prepareForReuse() {
        self.setProduct(nil)
    }
    
    func setProduct(product: FASProduct?) {
        
        if let _ = product, _ = StoreManager.sharedInstance().skProduct(product!) {
            activityIndicator.stopAnimating()
            
            let productTitle = StoreManager.sharedInstance().productTitle(product!)
            let productDescription = StoreManager.sharedInstance().productDescrition(product!)
            let productPrice = StoreManager.sharedInstance().productPrice(product!)
            
            labelTitle.text = titlePrefixForProduct(product!) + productTitle
            labelDescription.text = productDescription
            labelPrice.text = productPrice
        } else {
            activityIndicator.startAnimating()
            // TODO: localize this
            labelTitle.text = "loading product info..."
            labelDescription.text = ""
            labelPrice.text = "--"
        }
    }
    
    func titlePrefixForProduct(product: FASProduct) -> String {
        var prefix = ""
        if product.isPaid() {
            if product.isPurchased() {
                prefix = "Purchased: "
            }
            else if product.isPurchasing() {
                prefix = "Purchasing: "
            }
        }
        else {
            prefix = "Not For Sale: "
        }
        return prefix
    }
}
