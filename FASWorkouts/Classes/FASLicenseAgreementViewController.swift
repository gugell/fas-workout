//
//  FASLicenseAgreementViewController.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/11/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit

protocol FASLicenseAgreementViewControllerDelegate {
    func licenseAgreementViewController(vc: FASLicenseAgreementViewController, didDismissWithSuccess isSuccess: Bool)
}

class FASLicenseAgreementViewController: FASTextViewController {
    
    var delegate: FASLicenseAgreementViewControllerDelegate?
    
    private typealias FASLicenseAgreementAPICompletion = ((Bool,[FASAPIError]?)->Void)
    private var APICompletion: FASLicenseAgreementAPICompletion?
    
    let licenseAgreement: FASLicenseAgreement?
    let licenseAttributedText: NSAttributedString?

    ////////////////////////////////////////////
    // MARK: - Init
    ////////////////////////////////////////////
    required init?(coder aDecoder: NSCoder) {
        licenseAgreement = FASLicenseAgreement.defaultLicenseAgreement()
        licenseAttributedText = licenseAgreement?.loadLicenseAttributedText()
        super.init(coder: aDecoder)
        self.title = "license agreement"
    }
    ////////////////////////////////////////////
    // MARK: - ViewController
    ////////////////////////////////////////////
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.textView.attributedText = licenseAttributedText
        setActionPanelEnable((licenseAttributedText != nil))
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if licenseAttributedText != nil {
            self.textView.scrollToTopAnimated(false)
        }
    }
    ////////////////////////////////////////////
    // MARK: - Actions
    ////////////////////////////////////////////
    @IBAction func primaryActionButtonDidPress(sender: AnyObject) {
        self.setActionPanelBusy(true)
        
        agreeWithLicense { (success,errors) -> Void in
            if (success) {
                FASData.sharedData().updateSessionData()
                // hides itself & notify delegate
                self.dismissWithSuccess(success)
            }
            else {
                self.setActionPanelBusy(false)
                if let allErrors = errors {
                    FASAPIError.displayAlertWithErrors(allErrors)
                }
            }
        }
    }
    ////////////////////////////////////////////
    // MARK: - Controls
    ////////////////////////////////////////////
    private func agreeWithLicense(completion: FASLicenseAgreementAPICompletion ) {
        if let licenseVersionUserAgreedUpon = licenseAgreement?.version {
            APICompletion = completion
            let v: NSNumber = licenseVersionUserAgreedUpon
            let api = FASAPI.setUserInfoKey(FASAPIAcceptedLegalAgreementVersionKey, value: v, delegate: self)
            api.invoke()
        }
        else {
            printlnDebugError("invalid license object/version")
            // callback immediatelly; use FASAPIError to maintain consistency, however that's not quite correct
            let error = FASAPIError(code: FASAPIErrorCode.LicenseInvalid, value: "Invalid license object/version")
            completion(false,[error])
        }
    }
    
    private func dismissWithSuccess(isSuccess: Bool) {
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            self.delegate?.licenseAgreementViewController(self, didDismissWithSuccess: isSuccess)
        })
    }
}
////////////////////////////////////////////
// MARK: - FASAPIDelegate
////////////////////////////////////////////
extension FASLicenseAgreementViewController: FASAPIDelegate {
    func FASAPIdidFail(theFASAPI: FASAPI!) {
        dispatch_async(dispatch_get_main_queue(), { [weak self] in
            withExtendedLifetime(self) {
                if self!.APICompletion != nil {
                    if let apiErrors = theFASAPI.errors?.copy() as? [FASAPIError] {
                        self!.APICompletion!(false,apiErrors)
                    }
                    else {
                        self!.APICompletion!(false,nil)
                    }
                    self!.APICompletion = nil
                }
            }
        })
    }
    
    func FASAPIdidFinish(theFASAPI: FASAPI!) {
        dispatch_async(dispatch_get_main_queue(),{ [weak self] in
            withExtendedLifetime(self) {
                if self!.APICompletion != nil {
                    self!.APICompletion!(true,nil)
                    self!.APICompletion = nil
                }
            }
        })
    }
}
