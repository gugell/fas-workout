//
//  UBIOperator.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/15/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import Foundation


// Append a Dictionary with values and keys from another dictionary
func += <KeyType, ValueType> (inout left: Dictionary<KeyType, ValueType>, right: Dictionary<KeyType, ValueType>) {
    for (k, v) in right {
        left.updateValue(v, forKey: k)
    }
}