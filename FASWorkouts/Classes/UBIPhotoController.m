//
//  UBIPhotoController.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 3/13/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "UBIPhotoController.h"
#import "UBILogger.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>

// error domain
NSString * const UBIPhotoControllerErrorDomain = @"com.fas-sport.photocontroller";

// photo info keys
NSString * const UBIPhotoControllerPhotoInfoImageKey    = @"kPhotoControllerPhotoInfoImageKey";
NSString * const UBIPhotoControllerPhotoInfoSourceKey   = @"kPhotoControllerPhotoInfoSourceKey";

@interface UBIPhotoController ()
<UIImagePickerControllerDelegate,
UINavigationControllerDelegate>
@property (nonatomic, strong) UIViewController *viewController;
@property (nonatomic, strong) UIImagePickerController *imagePicker;

@property (nonatomic, strong) UIAlertController *userChoiceAlertController;

@property (nonatomic, getter=isCameraAvailable) BOOL cameraAvailable;
@property (nonatomic, getter=isLibraryAvailable) BOOL libraryAvailable;

- (void)startCameraWithViewController:(UIViewController *)viewController;
- (void)startLibraryWithViewController:(UIViewController *)viewController;
- (void)presentErrorAlertWithMessage:(NSString *)message completion:(void(^)(void))compltionBlock;
@end

@implementation UBIPhotoController

///////////////////////////////////////////////
#pragma mark -
#pragma mark Init
#pragma mark -
///////////////////////////////////////////////

- (instancetype)init
{
    self = [super init];
    if (self) {
        _picturesTakenCount = 0;
        // TODO: check for availability of camera hardware w/o using UIImagePicker
        _cameraAvailable = [UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera];
        _libraryAvailable = [UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypePhotoLibrary];
        
        [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(didReceiveMemoryWarning) name:UIApplicationDidReceiveMemoryWarningNotification object:nil];
    }
    return self;
}

- (void)didReceiveMemoryWarning { }

///////////////////////////////////////////////
#pragma mark -
#pragma mark User Choice
#pragma mark -
///////////////////////////////////////////////

- (void)chooseActionWithViewController:(UIViewController *)viewController
{
    UBILogTrace(@"viewController = %@", viewController);
    
    // return if userChoiceAlertController is already presented
    if (self.userChoiceAlertController) return;
    
    if (!viewController) {
        UBILogError(@"must have a valid view controler");
        
        if ([self.delegate respondsToSelector:@selector(photoController:didFailWithError:)]) {
            NSError* error = [NSError errorWithDomain:UBIPhotoControllerErrorDomain code:UBIPhotoControllerErrorCodeInvalidViewController userInfo:@{NSLocalizedDescriptionKey:@"Must have a valid view controler"}];
            [self.delegate photoController:self didFailWithError:error];
        }
        return;
    }
    
    self.viewController = viewController;
    _picturesTakenCount = 0;
    
    // do nothing if both, Librry and Camera are not available
    if (!self.isLibraryAvailable && !self.isCameraAvailable) {
        [self presentErrorAlertWithMessage:@"The camera and photo library are not available" completion:^{
            self.viewController = nil;
        }];
        return;
    }
    
    // TODO: Make button titles localizable
    NSString* alertTitle = nil;
    NSString* alertMessage = nil;
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:alertTitle
                                                                message:alertMessage
                                                         preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* cameraAction =
    [UIAlertAction actionWithTitle:@"Take a photo"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action) {
                               [self startCameraWithViewController:self.viewController];
                               self.userChoiceAlertController = nil;
                           }];
    UIAlertAction* libraryAction =
    [UIAlertAction actionWithTitle:@"Choose from library"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * action) {
                               [self startLibraryWithViewController:self.viewController];
                               self.userChoiceAlertController = nil;
                           }];
    UIAlertAction* cancelAction =
    [UIAlertAction actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                           handler:^(UIAlertAction * action) {
                               self.userChoiceAlertController = nil;
                           }];

    if (self.isLibraryAvailable) [ac addAction:libraryAction];
    if (self.isCameraAvailable) [ac addAction:cameraAction];
    [ac addAction:cancelAction];
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        [self presentPopoverWithContentController: ac animated:YES];
    }
    else {
        [self.viewController presentViewController:ac animated:YES completion:nil];
    }
    
    self.userChoiceAlertController = ac;
}

- (void)startCameraWithViewController:(UIViewController *)viewController
{
    if (self.imagePicker) {
        UBILogWarning(@"Image Picker is already presented");
        return;
    }
    
    UBILogTrace(@"start Camera (from: %@)", viewController);
    
    _picturesTakenCount = 0;
    
    if (!viewController) {
        UBILogError(@"must have a valid controller");
        return;
    }
    
    if (self.isCameraAvailable) {
        UIImagePickerController *cameraController = [[UIImagePickerController alloc] init];
        cameraController.sourceType = UIImagePickerControllerSourceTypeCamera;
        cameraController.allowsEditing = YES;
        cameraController.delegate = self;
        cameraController.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage];
        
        // always present camera in fullscreen
        [viewController presentViewController:cameraController
                                     animated:NO
                                   completion:nil];
        
        self.imagePicker = cameraController;
    }
    
    // if camera is not available, alert with error
    else {
        [self presentErrorAlertWithMessage:@"The camera is not available" completion:^{
            self.viewController = nil;
        }];
        
        if ([self.delegate respondsToSelector:@selector(photoController:didFailWithError:)]) {
            NSError* error = [NSError errorWithDomain:UBIPhotoControllerErrorDomain code:UBIPhotoControllerErrorCodeCameraUnavailable userInfo:@{NSLocalizedDescriptionKey:@"The camera is not available"}];
            [self.delegate photoController:self didFailWithError:error];
        }
    }
}

- (void)startLibraryWithViewController:(UIViewController *)viewController
{
    if (self.imagePicker) {
        UBILogWarning(@"Image Picker is already presented");
        return;
    }
    
    UBILogTrace(@"start Library (from: %@)", viewController);
    
    _picturesTakenCount = 0;
    
    if (!viewController) {
        UBILogError(@"must have a valid controller");
        return;
    }
    
    // presenting function
    void (^finishPresentingImagePicker)(UIViewController*) = ^(UIViewController* vc){
        UIImagePickerController* imagePicker = [UIImagePickerController new];
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.allowsEditing = YES;
        imagePicker.delegate = self;
        imagePicker.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeImage];
        
        if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
            // present as a popover on iPad
            [self presentPopoverWithContentController:imagePicker animated:YES];
        }
        
        else {
            // present modally on iPhone
            [vc presentViewController:imagePicker
                             animated:YES
                           completion:nil];
        }
        
        self.imagePicker = imagePicker;
        
        UBILogTrace(@"Library presented (from: %@)", vc);
    };
    
    if (self.isLibraryAvailable) {
        // authorize access to assets library if necessary
        if ([ALAssetsLibrary authorizationStatus] == ALAuthorizationStatusNotDetermined) {
            ALAssetsLibrary *al = [ALAssetsLibrary new];
            [al enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos
                              usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                                  *stop = YES;
                                  finishPresentingImagePicker(viewController);
                              }
                            failureBlock:^(NSError *error) {
                                // TODO: test what happens in this case
                                finishPresentingImagePicker(viewController);
                            }];
        }
        else {
            finishPresentingImagePicker(viewController);
        }
    }
    
    else {
        [self presentErrorAlertWithMessage:@"The camera roll is not available" completion:^{
            self.viewController = nil;
        }];
        
        if ([self.delegate respondsToSelector:@selector(photoController:didFailWithError:)]) {
            NSError* error = [NSError errorWithDomain:UBIPhotoControllerErrorDomain code:UBIPhotoControllerErrorCodeLibraryUnavailable userInfo:@{NSLocalizedDescriptionKey:@"The camera roll is not available"}];
            [self.delegate photoController:self didFailWithError:error];
        }
    }
}

// TODO: refactor to take an error object and show error based on localized description
- (void)presentErrorAlertWithMessage:(NSString *)message completion:(void(^)(void))compltionBlock
{
    if (!self.viewController) {
        UBILogError(@"must have a valid controller");
        return;
    }
    
    UIAlertController * alert =
    [UIAlertController alertControllerWithTitle:@"Take Image Error"
                                        message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        if (compltionBlock) compltionBlock();
    }];
    [alert addAction:cancelAction];
    
    [self.viewController presentViewController:alert animated:YES completion:nil];
}

- (void)presentPopoverWithContentController:(UIViewController *)popoverContentController
                                   animated:(BOOL)animated
{
    if (!self.viewController) {
        UBILogError(@"must have a valid controller");
        return;
    }
    
    if ( UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad ) {
        UBILogError(@"popover presentation must be used on iPad only");
        return;
    }
    
    popoverContentController.modalPresentationStyle = UIModalPresentationPopover;
    UIPopoverPresentationController *popPC = popoverContentController.popoverPresentationController;
    popPC.permittedArrowDirections = UIPopoverArrowDirectionAny;
    
    if ([self.viewController respondsToSelector:@selector(popoverViewForPhotoController:)]) {
        UIView *view = [(id)self.viewController popoverViewForPhotoController:self];
        popPC.sourceView = view;
        if ([self.viewController respondsToSelector:@selector(popoverRectForPhotoController:)]) {
            popPC.sourceRect = [(id)self.viewController popoverRectForPhotoController:self];
        }
        else {
            popPC.sourceRect = view.bounds;
        }
    }
    
    else  if ([self.viewController respondsToSelector:@selector(popoverBarButtonForPhotoController:)]) {
        UIBarButtonItem *buttonItem = [(id)self.viewController popoverBarButtonForPhotoController:self];
        popPC.barButtonItem = buttonItem;
    }
    
    [self.viewController presentViewController: popoverContentController animated:animated completion:nil];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark UIImagePickerControllerDelegate
#pragma mark -
///////////////////////////////////////////////

- (void)imagePickerControllerDidCancel:(UIImagePickerController *) picker {
    [self dismissImagePickerController:picker];
    
    if ([self.delegate respondsToSelector:@selector(photoController:didFailWithError:)]) {
        NSError* error = [NSError errorWithDomain:UBIPhotoControllerErrorDomain code:UBIPhotoControllerErrorCodeCanceledByUser userInfo:@{NSLocalizedDescriptionKey:@"Cancel by user"}];
        [self.delegate photoController:self didFailWithError:error];
    }
}

- (void)imagePickerController:(UIImagePickerController *) picker
 didFinishPickingMediaWithInfo:(NSDictionary *) info
{
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    UIImage *originalImage, *editedImage, *imageToSave;

    // Handle a still image capture
    if (CFStringCompare ((CFStringRef) mediaType, kUTTypeImage, 0)
        == kCFCompareEqualTo) {

        editedImage = (UIImage *) [info objectForKey:
                                   UIImagePickerControllerEditedImage];
        originalImage = (UIImage *) [info objectForKey:
                                     UIImagePickerControllerOriginalImage];

        if (editedImage) {
            imageToSave = editedImage;
        } else {
            imageToSave = originalImage;
        }

        if (imageToSave) {
            ++_picturesTakenCount;
        }
        
        UBIPhotoControllerPhotoSource source = -1;
        switch (picker.sourceType) {
            case UIImagePickerControllerSourceTypePhotoLibrary:
                source = UBIPhotoControllerPhotoSourcePhotoLibrary;
                break;
                
            case UIImagePickerControllerSourceTypeCamera:
                source = UBIPhotoControllerPhotoSourceCamera;
                break;
                
            default: break;
        }
        
        if ( _picturesTakenCount > 0 ) {
            NSDictionary* photoInfo =
            [NSDictionary dictionaryWithObjectsAndKeys:
             imageToSave, UBIPhotoControllerPhotoInfoImageKey,
             @(source), UBIPhotoControllerPhotoInfoSourceKey,
             nil];
            
            if ([self.delegate respondsToSelector:@selector(photoController:didFinishWithPhotoInfo:)]) {
                [self.delegate photoController:self didFinishWithPhotoInfo:photoInfo];
            }
        }
        
        else {
            // NO photo
            if ([self.delegate respondsToSelector:@selector(photoController:didFailWithError:)]) {
                NSError* error = [NSError errorWithDomain:UBIPhotoControllerErrorDomain code:UBIPhotoControllerErrorCodeNoPhoto userInfo:@{NSLocalizedDescriptionKey:@"No Photo"}];
                [self.delegate photoController:self didFailWithError:error];
            }
        }
    }
    
    [self dismissImagePickerController:picker];
}

- (void)dismissImagePickerController:(UIImagePickerController *) picker
{
    if (!self.viewController) {
        UBILogWarning(@"invalid view controller for image picker");
    }
    [picker dismissViewControllerAnimated:YES completion:^{
        // update the status bar appearance for root view controller,
        // because the image picker can change it while displaying
        [self.viewController setNeedsStatusBarAppearanceUpdate];
        
        // clean up
        self.imagePicker = nil;
        self.viewController = nil;
    }];
}

@end
