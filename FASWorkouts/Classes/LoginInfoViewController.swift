//
//  LoginInfoViewController.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/11/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit
//import VideoSplashKit

/*class ViewController: VideoSplashViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = NSURL.fileURLWithPath(NSBundle.mainBundle().pathForResource("test", ofType: "mp4")!)
        self.videoFrame = view.frame
        self.fillMode = .ResizeAspectFill
        self.alwaysRepeat = true
        self.sound = true
        self.startTime = 12.0
        self.duration = 4.0
        self.alpha = 0.7
        self.backgroundColor = UIColor.blackColor()
        self.contentURL = url
        self.restartForeground = true
    }
}*/

class LoginInfoViewController: VideoSplashViewController{
    
    //FASViewController {

    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    
    var firstTimeUX: Bool = false
    var sessionUIDelegate: SessionUIDelegate?
    var hidesSkipButton: Bool = false {
        didSet {
            configureSkipButton()
        }
    }
    
    init(sessionUIDelegate sDelegate: SessionUIDelegate) {
        self.sessionUIDelegate = sDelegate
        super.init(nibName: "LoginInfoViewController", bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillLayoutSubviews() {
        configureSkipButton()
    }
    
    private func configureSkipButton() {
        assert(NSThread.currentThread() == NSThread.mainThread(), "Try to hide button not in main thread")
        self.skipButton?.hidden = hidesSkipButton
    }
    override func viewDidLoad(){
        super.viewDidLoad();
        
        let url = NSURL.fileURLWithPath(NSBundle.mainBundle().pathForResource("intro", ofType: "mp4")!)
        self.videoFrame = navigationController!.view.frame
        self.fillMode = .ResizeAspectFill
        self.alwaysRepeat = true
        self.sound = false
        self.startTime = 6.0
        self.duration = 9.0
        self.alpha = 0.7
        self.backgroundColor = UIColor.clearColor()
        self.contentURL = url
        
        let bounds = UIScreen.mainScreen().bounds;
        let screenWidth = bounds.width;
        let screenHeight = bounds.height;
        
        var sampleButton = UIButton(frame: CGRect(x: 15.0, y: screenHeight - 80, width: 140.0, height: 42.0))
        sampleButton.setImage(UIImage(named: "btnSignin"), forState: .Normal)
        sampleButton.layer.cornerRadius = 2
        sampleButton.layer.masksToBounds = true
        sampleButton.addTarget(self, action: #selector(LoginInfoViewController.signInButtonDidPress(_:)), forControlEvents: .TouchUpInside)
        view.addSubview(sampleButton)

        
        sampleButton = UIButton(frame: CGRect(x: screenWidth - 155, y: screenHeight - 80, width: 140.0, height: 42.0))
        sampleButton.setImage(UIImage(named: "btnRegister"), forState: .Normal)
        sampleButton.layer.cornerRadius = 2
        sampleButton.layer.masksToBounds = true
          sampleButton.addTarget(self, action: #selector(LoginInfoViewController.signUpButtonDidPress(_:)), forControlEvents: .TouchUpInside)
        view.addSubview(sampleButton)
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController!.setNavigationBarHidden(true , animated: false)
    }
    ///////////////////////////////////////////////
    // MARK: - Button Actions -
    ///////////////////////////////////////////////
    @IBAction func skipButtonDidPress(sender: AnyObject) {
        cancelAction()
    }
    
    @IBAction func signInButtonDidPress(sender: AnyObject) {
        loginAction()
    }
    
    @IBAction func signUpButtonDidPress(sender: AnyObject) {
        registerAction()
    }
    ///////////////////////////////////////////////
    // MARK: - Concrete Actions -
    ///////////////////////////////////////////////
    private func loginAction() {
        let loginVC = LoginViewController(sessionUIDelegate: self.sessionUIDelegate!)
        loginVC.firstTimeUX = firstTimeUX
        navigationController?.pushViewController(loginVC, animated: true)
    }
    
    private func registerAction() {
        let registrationVC = RegistrationViewController(sessionUIDelegate: self.sessionUIDelegate!)
        registrationVC.firstTimeUX = firstTimeUX
        navigationController?.pushViewController(registrationVC, animated: true)
    }
    
    private func cancelAction() {
        sessionUIDelegate!.sessionUIDidCancel(self)
    }
    
    private func loginWithFacebookAction() {
        // TODO: implement it when FB login button is on the Info screen
    }
    
    
    
}

// MARK: conform to FASAPISessionObserver

extension LoginInfoViewController {
    
    override func FASAPISessionNotification(notification: NSNotification!) {
        
        super.FASAPISessionNotification(notification)
        
        if notification.name == FASAPISessionDidLoginNotification {
            sessionUIDelegate!.sessionUIDidFinish(self)
        }
        else if notification.name == FASAPISessionDidNotLoginNotification {
            if let userInfo = notification.userInfo as [NSObject : AnyObject]! {
                if let errors = userInfo["errors"] as? [AnyObject] {
                    FASAPIError.displayAlertWithErrors(errors)
                }
            }
        }
    }
}

extension LoginInfoViewController: SessionUIController { }
