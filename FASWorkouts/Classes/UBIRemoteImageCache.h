//
//  UBIRemoteImageCache.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/1/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UBIRemoteImage.h"

extern NSString * const kUBIRemoteImageCacheInfoURL;
extern NSString * const kUBIRemoteImageCacheInfoFilename;
extern NSString * const kUBIRemoteImageCacheInfoMaxSize;
extern NSString * const kUBIRemoteImageCacheInfoDefaultFilename;

@interface UBIRemoteImageCache : NSObject

@property (nonatomic, copy) NSString *imageFilePrefix;
@property (nonatomic, copy) NSString *loadingImageName;
@property (nonatomic, copy) NSString *defaultImageName;
@property (nonatomic, strong) NSString *basePath;

- (UBIRemoteImage *)remoteImageForKey:(NSString *)key;

- (void)setRemoteImage:(UBIRemoteImage *)remoteImage
                forKey:(NSString *)key;

// returns either already cached (in memory) image or new instance
- (UBIRemoteImage *)remoteImageForKey:(NSString *)key
                         loadingInfo:(NSDictionary *)loadingInfo;

- (void)flush;
- (void)clearDefaultImages;
- (void)loadAllImages;
- (void)deleteRemoteImageForKey:(NSString *)key;
@end
