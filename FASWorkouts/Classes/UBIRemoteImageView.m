//
//  UBIRemoteImageView.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/1/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "UBIRemoteImageView.h"
#import "FASWorkouts-Swift.h"

static NSString * kRemoteImageStatus = @"remoteImageStatus";


@implementation UBIRemoteImageView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.showLoadingActivity = YES;
    self.showThumbnail = NO;
    
    self.imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    self.imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    [self addSubview:self.imageView];
    
    self.loadingIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.loadingIndicatorView.hidesWhenStopped = YES;
    self.loadingIndicatorView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleBottomMargin;
    
    CGRect indicatorFrame = self.loadingIndicatorView.frame;
    indicatorFrame.origin = CGPointMake(CGRectGetMidX(self.bounds) - CGRectGetMidX(indicatorFrame),
                                        CGRectGetMidY(self.bounds) - CGRectGetMidY(indicatorFrame));
    self.loadingIndicatorView.frame = indicatorFrame;
    [self addSubview:self.loadingIndicatorView];
    
    // layout debugging
//    self.backgroundColor = UIColor.redColor;
//    self.imageView.backgroundColor = UIColor.greenColor;
}

- (void)dealloc
{
    [_remoteImage removeObserver:self
                      forKeyPath:@"isLoading"
                         context:&kRemoteImageStatus];
}

- (void)setRemoteImage:(UBIRemoteImage *)remoteImage
{
    if (_remoteImage != remoteImage) {
        [_remoteImage removeObserver:self
                          forKeyPath:@"isLoading"
                             context:&kRemoteImageStatus];
        _remoteImage = remoteImage;
        [_remoteImage addObserver:self
                       forKeyPath:@"isLoading"
                          options:NSKeyValueObservingOptionInitial
                          context:&kRemoteImageStatus];
    }
    [self refreshImageView];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if (context == &kRemoteImageStatus) {
        [self refreshImageView];
    }
    else {
        [super observeValueForKeyPath:keyPath
                             ofObject:object
                               change:change
                              context:context];
    }
}

- (void)refreshImageView
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.remoteImage.isLoading) {
            if (self.showLoadingActivity) {
                self.imageView.hidden = YES;
                self.imageView.image = nil;
                [self.loadingIndicatorView startAnimating];
            }
            else {
                [self.loadingIndicatorView stopAnimating];
                self.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@", self.remoteImage.loadingImageName]];
                self.imageView.hidden = NO;
            }
        }
        else {
            [self.loadingIndicatorView stopAnimating];
            if (self.showThumbnail) {
                self.imageView.image = self.remoteImage.thumbnail;
            }
            else {
                self.imageView.image = self.remoteImage.image;
            }
            self.imageView.hidden = NO;
        }
    });
}

// TODO: replace w/ UBICustomRemoteImage
-(void)applyStyleForRemoteImageView {
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = self.bounds.size.width / 2.0;
    // Borders
    self.layer.borderWidth = 2.0;
    self.layer.borderColor = [UIColor fasYellowColor].CGColor;
}

@end
