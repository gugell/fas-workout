//
//  FASProductProtocol.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/21/16.
//  Copyright © 2016 FAS. All rights reserved.
//

//@import NSFoundation;

@protocol FASProduct <NSObject>
- (NSString * _Nonnull)productCategoryName;
- (NSString * _Nonnull)productIdentifier;

@property (nonatomic,assign) NSInteger productPurchaseStatus;

- (BOOL)isPaid;
- (BOOL)isPurchasing;
- (BOOL)isPurchased;

@end
