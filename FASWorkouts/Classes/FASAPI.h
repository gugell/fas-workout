//
//  FASAPI.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/19/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

@import Foundation;
@import CoreGraphics;

// FASAPI enumerator
//
// IMPORTANT: if you change an enum value, make sure you also change any localizable string
// keys in FASAPI.strings
typedef NS_ENUM(NSUInteger, FASAPIMethod) {
    FASAPIMethodLogin                                    = 0,
    FASAPIMethodLoginStart                               = 1,
    FASAPIMethodLoginFacebook                            = 2,
    FASAPIMethodRegister                                 = 3,
    FASAPIMethodUserInfo                                 = 4,
    FASAPIMethodLogout                                   = 5,
    FASAPIMethodForgotPassword                           = 6,
    FASAPIMethodTestRunList                              = 7,
    FASAPIMethodGetWorkouts                              = 8,
    FASAPIMethodGetWorkoutsModifyDate                    = 9,
    FASAPIMethodGetExercisesWithOptions                  = 10,
    FASAPIMethodGetAttachmentsWithOptions                = 11,
    FASAPIMethodGetAttachmentsForExercisesWithOptions    = 12,
    FASAPIMethodAddUserPurchase                          = 13,
    FASAPIMethodRegisterDeviceToken                      = 14,
    FASAPIMethodSetUserInfo                              = 15,
    FASAPIMethodGetWorkoutHistoryWithOptions             = 16,
    FASAPIMethodPostWorkoutHistory                       = 17,
    FASAPIMethodPostExerciseHistory                      = 18,
    FASAPIMethodGetBestResults                           = 19,
    FASAPIMethodGetFaqEntries                            = 20,
    FASAPIMethodGetStoreItems                            = 21
};

// NSURLSession completion handler type
typedef void (^CompletionHandlerType)();

// notifications
extern NSString * const FASAPIWillInvokeNotification;
extern NSString * const FASAPIDidInvokeNotification;
extern NSString * const FASAPIDidFinishNotification;
extern NSString * const FASAPIDidFailNotification;
// TODO: add remaining delegate methods and convert all delegates to use notifications
// TODO: consider using blocks instead of protocol

// forward class declaration for protocol
@class FASAPI;
@class FASAPIError;
@class FASWorkoutSession;

// the API delegate is notified about API validation and invocation flow
@protocol FASAPIDelegate <NSObject>
@optional
// sent before the API validates params
- (BOOL)validatePreconditionsForFASAPI:(FASAPI *)theFASAPI apiError:(FASAPIError **)apiError;
// sent before the API request is initiated
- (void)FASAPIwillInvoke:(FASAPI *)theFASAPI;
// sent after the API request is initiated
- (void)FASAPIdidInvoke:(FASAPI *)theFASAPI;
// sent during the request to communicate percent complete
// below line is commented because we don't currently use the class for sending and receiving huge amount of data
//- (void)FASAPI:(FASAPI *)theFASAPI didSendData:(CGFloat)percentComplete;
// sent when the API response is received w/ no errors
- (void)FASAPIdidFinish:(FASAPI *)theFASAPI;
// sent when the API fails (pre or post request)
- (void)FASAPIdidFail:(FASAPI *)theFASAPI;
@end

// the API preInvocationDelegate is notified about all preInvocationFlow
@protocol FASAPIUIDelegate <NSObject>
@optional
// send when the API requires a valid session. the delegate should respond as follows:
//  YES: delegate invokes session UI and is responsible for re-invoking or cancelling
//       the API
- (BOOL)FASAPIUIshouldWaitForSession:(FASAPI *)theFASAPI;
- (void)FASAPIUIhasInvalidPreconditions:(FASAPI *)theFASAPI;
- (void)FASAPIUIwillInvoke:(FASAPI *)theFASAPI;
- (void)FASAPIUIdidInvoke:(FASAPI *)theFASAPI;
@end

#import "FASAPISessionProtocol.h"

@class UBIRunList;

@interface FASAPI : NSObject <  FASAPISessionObserver,
                                NSURLSessionDelegate,
                                NSURLSessionTaskDelegate,
                                NSURLSessionDataDelegate/*,
                                NSURLSessionDownloadDelegate*/ >

@property (nonatomic, weak)                 id <FASAPIDelegate>             delegate;
// an object that responds to the SSAPIUIDelegate protocol
// set this so the appropriate view controller can provide error
// handling prior to the API being invoked
@property (nonatomic, strong)               id <FASAPIUIDelegate>           UIDelegate;// ???: should it be 'strong', really
@property (nonatomic, readonly)             FASAPIMethod                    method;
@property (nonatomic, strong, readonly)     NSMutableDictionary *           params;
@property (nonatomic, strong)               NSString *                      rawResponse;
@property (nonatomic, strong)               NSDictionary *                  response;
@property (nonatomic, strong)               NSMutableArray *                errors;
@property (nonatomic)                       BOOL                            showNetworkActivity;
@property (nonatomic, getter = isCancelled) BOOL                            cancelled;
@property (nonatomic,
           getter = isWaitingForSession)    BOOL                            waitingForSession;
@property (nonatomic, readonly, getter = isInvoked)             BOOL        invoked;
@property (nonatomic, readonly, copy)       NSString *                      body;

// custom object to carry on custom data (e.g. objectID) w/ API
@property (nonatomic, strong)               id                              userInfo;

// instance methods
- (void)invokeWithUIDelegate:(id <FASAPIUIDelegate>)UIDelegate;
- (void)invoke;
- (void)cancel;
- (void)simulate;
- (void)simulateFailure;

// basic API constructors
+ (FASAPI *)loginStartWithUsername:(NSString *)username
                          delegate:(id <FASAPIDelegate>)delegate;
+ (FASAPI *)loginWithClientNonce:(NSString *)clientNonce
                        username:(NSString *)username
                     andPassword:(NSString *)password
                        delegate:(id <FASAPIDelegate>)delegate;
+ (FASAPI *)loginWithFacebookToken:(NSString *)facebookToken
                          delegate:(id <FASAPIDelegate>)delegate;
+ (FASAPI *)registerAccount:(NSString *)username
                   password:(NSString *)password
       passwordConfirmation:(NSString *)passwordConfirmation
                   delegate:(id <FASAPIDelegate>)delegate;
+ (FASAPI *)logout:(id <FASAPIDelegate>)delegate;
+ (FASAPI *)forgotPassword:(NSString *)username
                  delegate:(id <FASAPIDelegate>)delegate;
+ (FASAPI *)userInfo:(id <FASAPIDelegate>)delegate;

// run lists API constructor
+ (FASAPI *)testRunList:(id <FASAPIDelegate>)delegate; // test all new APIs w/ this method

+ (FASAPI *)getWorkoutsWithDelegate:(id <FASAPIDelegate>)delegate;
+ (FASAPI *)getWorkoutsModifyDateWithRunList:(UBIRunList *)workoutRunList
                                    delegate:(id <FASAPIDelegate>)delegate;

+ (FASAPI *)getWorkoutHistoryWithRunList:(UBIRunList *)workoutHistoryRunList
                          delegate:(id <FASAPIDelegate>)delegate;

+ (FASAPI *)getFAQWithRunList:(UBIRunList *)faqRunList
                                delegate:(id <FASAPIDelegate>)delegate;

+ (FASAPI *)getExercisesWithRunList:(UBIRunList *)exercisesRunList
                           delegate:(id <FASAPIDelegate>)delegate;
+ (FASAPI *)getAttachmentsWithRunList:(UBIRunList *)exercisesRunList
                             delegate:(id <FASAPIDelegate>)delegate;
+ (FASAPI *)getAttachmentsForExercisesWithRunList:(UBIRunList *)attachmentsRunList
                                         delegate:(id <FASAPIDelegate>)delegate;

// In-App Purchase
+ (FASAPI *)addUserPurchase:(NSString *)productID
                    receipt:(NSString *)encodedReceipt
                   restored:(BOOL)isRestored
                   delegate:(id <FASAPIDelegate>)delegate;

// APNS
+ (FASAPI *)registerDeviceToken:(NSString *)deviceToken
                       delegate:(id <FASAPIDelegate>)delegate;

// UserInfo
+ (FASAPI *)setUserInfoKey:(NSString*)key
                     value:(id)value
                  delegate:(id <FASAPIDelegate>)delegate;

// Post workout info
+ (FASAPI *)postWorkoutHistory:(NSDictionary *)workoutSessionParams
                      delegate:(id <FASAPIDelegate>)delegate;
+ (FASAPI *)postExerciseHistory:(NSDictionary *)sessionDict
                exerciseHistory:(NSArray *)exerciseHistoryArray
                       delegate:(id <FASAPIDelegate>)delegate;

// Best exercise results
+ (FASAPI *)getBestExerciseResultsForWorkoutID:(NSNumber *)workoutID
                       delegate:(id <FASAPIDelegate>)delegate;

// Store
+ (FASAPI *)getStoreItemsWithDelegate:(id <FASAPIDelegate>)delegate;
@end

#import "FASAPIError.h"
#import "FASAPISession.h"