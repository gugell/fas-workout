//
//  FASEdgeInsetsLabel.m
//  FASWorkouts
//
//  Created by Oleksandr Nechet on 21.12.15.
//  Copyright © 2015 FAS. All rights reserved.
//

#import "FASEdgeInsetsLabel.h"

@implementation FASEdgeInsetsLabel

- (void)drawTextInRect:(CGRect)rect
{
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, self.textInsets)];
}

@end
