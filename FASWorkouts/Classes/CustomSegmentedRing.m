//
//  M13ProgressVewSegmentedRing.m
//  M13ProgressView
//
/*Copyright (c) 2013 Brandon McQuilkin
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#import "CustomSegmentedRing.h"

#define   DEGREES_TO_RADIANS(degrees)  ((M_PI * degrees)/ 180)

@interface CustomSegmentedRing ()
/**The start progress for the progress animation.*/
@property (nonatomic, assign) CGFloat animationFromValue;
/**The end progress for the progress animation.*/
@property (nonatomic, assign) CGFloat animationToValue;
/**The start time interval for the animaiton.*/
@property (nonatomic, assign) CFTimeInterval animationStartTime;
/**Link to the display to keep animations in sync.*/
@property (nonatomic, strong) CADisplayLink *displayLink;
/**Allow us to write to the progress.*/
@property (nonatomic, readwrite) CGFloat progress;
/**The layer that progress is shown on.*/
@property (nonatomic, retain) CAShapeLayer *progressLayer;

@property (nonatomic, retain) CAShapeLayer *imageLayer;
@property (nonatomic, retain) CAShapeLayer *customLayer;

@property(nonatomic) NSUInteger currentSegment;
@property(nonatomic) NSUInteger previousSegment;

/**The layer that the background shown on.*/
@property (nonatomic, retain) CAShapeLayer *backgroundLayer;
@property (nonatomic, retain) CAShapeLayer *backgroundLayerStatic;

@property (nonatomic) NSTimeInterval duration;
@property (nonatomic) NSTimeInterval currentTimestamp;
@property (nonatomic) BOOL durationChanged;

@end

#define kM13ProgressViewSegmentedRingHideKey @"Hide"
#define kM13ProgressViewSegmentedRingShowKey @"Show"

@implementation CustomSegmentedRing
{
    //Wether or not the corresponding values have been overriden by the user
    BOOL _progressRingWidthOverriden;
    BOOL _segmentSeparationAngleOverriden;
    //The calculated angles of the concentric rings
    CGFloat outerRingAngle;
    CGFloat innerRingAngle;
    CGFloat _segmentSeparationInnerAngle;
    
    CGFloat outerRingAngleAlt;
    CGFloat innerRingAngleAlt;
    CGFloat _segmentSeparationInnerAngleAlt;
    
    NSUInteger currentTime;
    NSUInteger currentSeconds;
    NSUInteger overallTime;
}

@dynamic progress;

#pragma mark Initalization and setup

- (id)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    // Default duration
    _duration = 0.1;
    // Set own background color
    self.backgroundColor = [UIColor clearColor];
    
    // Set defaut sizes
    _progressRingWidth = fmaxf(self.bounds.size.width * .25, 1.0);
    _progressRingWidthOverriden = NO;
    _segmentSeparationAngleOverriden = NO;
    self.animationDuration = 0.1;
    _numberOfSegments = 8;
    _numberOfSegmentsAlt = 360;
    _segmentSeparationAngle = M_PI / (8 * _numberOfSegments);
    // No separation for masked layer
    _segmentSeparationAngleAlt = 0.;
    [self updateAngles];
    
    // Set default colors
    self.primaryColor = [UIColor colorWithRed:248./255. green:228./255. blue:1.0/255. alpha:1.];
    self.secondaryColor = [UIColor colorWithRed:70./255.0 green:68./255.0 blue:69./255.0 alpha:1.];
    
    // Set up custom layer
    _customLayer = [CAShapeLayer layer];
    _customLayer.fillColor = [UIColor whiteColor].CGColor;
    _customLayer.fillRule = kCAFillRuleEvenOdd;
    
    // Set up the background layers
    _backgroundLayer = [CAShapeLayer layer];
    _backgroundLayer.fillColor = self.secondaryColor.CGColor;
    
    _backgroundLayerStatic = [CAShapeLayer layer];
    _backgroundLayerStatic.fillColor = self.primaryColor.CGColor;
    
    // Set up the progress layer
    _progressLayer = [CAShapeLayer layer];
    _progressLayer.fillColor = self.primaryColor.CGColor;
    _progressLayer.mask = _backgroundLayerStatic;
    
    [self.layer addSublayer:_backgroundLayer];
    [self.layer addSublayer:_progressLayer];
    
    return;
}

#pragma mark Appearance

- (void)setPrimaryColor:(UIColor *)primaryColor
{
    [super setPrimaryColor:primaryColor];
    _progressLayer.strokeColor = self.primaryColor.CGColor;
    [self setNeedsDisplay];
}

- (void)setSecondaryColor:(UIColor *)secondaryColor
{
    [super setSecondaryColor:secondaryColor];
    _backgroundLayer.strokeColor = self.secondaryColor.CGColor;
    [self setNeedsDisplay];
}


- (void)setProgressRingWidth:(CGFloat)progressRingWidth
{
    _progressRingWidth = progressRingWidth;
    _progressLayer.lineWidth = _progressRingWidth;
    _backgroundLayer.lineWidth = _progressRingWidth;
    _progressRingWidthOverriden = YES;
    [self updateAngles];
    [self setNeedsDisplay];
    [self invalidateIntrinsicContentSize];
}

- (void)setNumberOfSegments:(NSInteger)numberOfSegments
{
    _numberOfSegments = numberOfSegments;
    if (!_segmentSeparationAngleOverriden) {
        _segmentSeparationAngle = M_PI / (2 * _numberOfSegments);
    }
}

- (void)setSegmentSeparationAngle:(CGFloat)segmentSeparationAngle
{
    _segmentSeparationAngle = segmentSeparationAngle;
    _segmentSeparationAngleOverriden = YES;
}

#pragma mark Actions

- (void)setProgress:(CGFloat)progress animated:(BOOL)animated
{
    if (self.progress == progress) {
        return;
    }
    if (animated == NO)
    {
        if (_displayLink)
        {
            // Kill running animations
            [_displayLink removeFromRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
            _displayLink = nil;
        }
        [super setProgress:progress animated:animated];
        [self setNeedsDisplay];
    }
    else
    {
        _animationStartTime = CACurrentMediaTime();
        _animationFromValue = self.progress;
        _animationToValue = progress;
        if (!_displayLink)
        {
            //Create and setup the display link
            [self.displayLink removeFromRunLoop:NSRunLoop.mainRunLoop forMode:NSRunLoopCommonModes];
            self.displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(animateProgress:)];
            [self.displayLink addToRunLoop:NSRunLoop.mainRunLoop forMode:NSRunLoopCommonModes];
        }
    }
}

- (void)animateProgress:(CADisplayLink *)displayLink
{
    dispatch_async(dispatch_get_main_queue(), ^{
        CGFloat dt = (displayLink.timestamp - _animationStartTime) / self.animationDuration;
        if (dt >= 1.0) {
            //Order is important! Otherwise concurrency will cause errors, because setProgress: will detect an animation in progress and try to stop it by itself. Once over one, set to actual progress amount. Animation is over.
            [self.displayLink removeFromRunLoop:NSRunLoop.mainRunLoop forMode:NSRunLoopCommonModes];
            self.displayLink = nil;
            [super setProgress:_animationToValue animated:NO];
            [self setNeedsDisplay];
            return;
        }
        
        //Set progress
        [super setProgress:_animationFromValue + dt * (_animationToValue - _animationFromValue) animated:YES];
        [self setNeedsDisplay];
        
    });
}

- (CABasicAnimation *)showAnimation
{
    //Show the progress layer and percentage
    CABasicAnimation *showAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    showAnimation.fromValue = [NSNumber numberWithFloat:0.0];
    showAnimation.toValue = [NSNumber numberWithFloat:1.0];
    showAnimation.duration = self.animationDuration;
    showAnimation.repeatCount = 1.0;
    //Prevent the animation from resetting
    showAnimation.fillMode = kCAFillModeForwards;
    showAnimation.removedOnCompletion = NO;
    return showAnimation;
}

- (CABasicAnimation *)hideAnimation
{
    //Hide the progress layer and percentage
    CABasicAnimation *hideAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    hideAnimation.fromValue = [NSNumber numberWithFloat:1.0];
    hideAnimation.toValue = [NSNumber numberWithFloat:0.0];
    hideAnimation.duration = self.animationDuration;
    hideAnimation.repeatCount = 1.0;
    //Prevent the animation from resetting
    hideAnimation.fillMode = kCAFillModeForwards;
    hideAnimation.removedOnCompletion = NO;
    return hideAnimation;
}

#pragma mark Layout

- (void)layoutSubviews
{
    // Update frames of layers
    _backgroundLayer.frame = self.bounds;
    _backgroundLayerStatic.frame = self.bounds;
    _customLayer.frame = self.bounds;
    _progressLayer.frame = self.bounds;
    
    // Update line widths if not overriden
    if (!_progressRingWidthOverriden) {
        _progressRingWidth = fmaxf(self.frame.size.width * .25, 1.0);
    }
    
    [self updateAngles];
    
    // Redraw
    [self setNeedsDisplay];
}

- (CGSize)intrinsicContentSize
{
    // This might need a little more fine tuning.
    /**
     @note nope
     */
    CGFloat base = _progressRingWidth * 2;
    
    return CGSizeMake(base, base);
}

- (void)setFrame:(CGRect)frame
{
    // Keep the progress view square.
    if (frame.size.width != frame.size.height) {
        frame.size.height = frame.size.width;
    }
    
    [self updateAngles];
    
    [super setFrame:frame];
}

- (void)updateAngles
{
    // Calculate the outer ring angle for the progress segment.*/
    outerRingAngle = ((2.0 * M_PI) / (float)_numberOfSegments) - _segmentSeparationAngle;
    // Calculate the angle gap for the inner ring
    _segmentSeparationInnerAngle = 2.0 * asinf(((self.bounds.size.width / 2.0) * sinf(_segmentSeparationAngle / 2.0)) / ((self.bounds.size.width / 2.0) - _progressRingWidth));
    // Calculate the inner ring angle for the progress segment.*/
    innerRingAngle = ((2.0 * M_PI) / (float)_numberOfSegments) - _segmentSeparationInnerAngle;
    
    /**
     @note parameters' set for masked full progress layer
     */
    outerRingAngleAlt = ((2.0 * M_PI) / (float)_numberOfSegmentsAlt) - _segmentSeparationAngleAlt;
    _segmentSeparationInnerAngleAlt = 2.0 * asinf(((self.bounds.size.width / 2.0) * sinf(_segmentSeparationAngleAlt / 2.0)) / ((self.bounds.size.width / 2.0) - _progressRingWidth));
    innerRingAngleAlt = ((2.0 * M_PI) / (float)_numberOfSegmentsAlt) - _segmentSeparationInnerAngleAlt;
}

- (NSInteger)numberOfFullSegments
{
    return (NSInteger)floorf(self.progress * _numberOfSegments);
}

- (NSInteger)numberOfFullSegmentsAlt
{
    return (NSInteger)floorf(self.progress * _numberOfSegmentsAlt);
}

#pragma mark Drawing

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    //Draw the background
    [self drawBackground];
    
    //Draw Progress
    [self drawInstantProgress];
}

- (void)drawStaticBackground
{
    //The background segments are drawn counterclockwise, start with the outer ring, add an arc counterclockwise.  Then add the coresponding arc for the inner ring clockwise. Then close the path. The line connecting the two arcs is not needed. From tests it seems to be created automatically.
    CGFloat outerStartAngle = - M_PI_2;
    // Skip half of a separation angle, since the first separation will be centered upward.
    outerStartAngle -= (_segmentSeparationAngle / 2.0);
    // Calculate the inner start angle position
    CGFloat innerStartAngle = - M_PI_2;
    innerStartAngle -= (_segmentSeparationInnerAngle / 2.0) + innerRingAngle;
    // Create the path ref that all the paths will be appended
    CGMutablePathRef pathRef = CGPathCreateMutable();
    
    // Create each segment
    CGPoint center = CGPointMake(self.bounds.size.width / 2.0, self.bounds.size.width / 2.0);
    for (int i = 0; i < _numberOfSegments; i++) {
        // Create the outer ring segment
        UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:center radius:(self.bounds.size.width / 2.0) startAngle:outerStartAngle endAngle:(outerStartAngle - outerRingAngle) clockwise:NO];
        // Create the inner ring segment
        [path addArcWithCenter:center radius:(self.bounds.size.width / 2.0) - _progressRingWidth startAngle:innerStartAngle endAngle:innerStartAngle + innerRingAngle clockwise:YES];
        // Add the segment to the path
        CGPathAddPath(pathRef, NULL, path.CGPath);
        
        //Setup for the next segment
        outerStartAngle -= (outerRingAngle + _segmentSeparationAngle);
        innerStartAngle -= (innerRingAngle + _segmentSeparationInnerAngle);
    }
    
    // Set the path
    _backgroundLayerStatic.path = pathRef;
    
    CGPathRelease(pathRef);
}

-(UIBezierPath *)getCustomPath
{
    CGFloat outerStartAngle = - M_PI_2;
    // Skip half of a separation angle, since the first separation will be centered upward.
    outerStartAngle -= (_segmentSeparationAngle / 2.0);
    // Calculate the inner start angle position
    CGFloat innerStartAngle = - M_PI_2;
    innerStartAngle -= (_segmentSeparationInnerAngle / 2.0) + innerRingAngle;
    // Create the path ref that all the paths will be appended
    CGFloat xKoef = 0.;
    CGFloat radiusKoef = 0.;
    CGFloat yKoef = 0.;
    
    CGFloat screenSize = [[UIScreen mainScreen] bounds].size.height;
    
    // Added separate case for 4S
    if (screenSize < 568) {
        xKoef = 1.;
        radiusKoef = 14.;
        yKoef = 8.;
    } else {
        xKoef = 1.;
        radiusKoef = 24.;
        yKoef = 15.;
    }
    
    CGPoint altCenter = CGPointMake(self.bounds.size.width / 2.0 - xKoef, self.bounds.size.width / 2.0 - yKoef);
    
    CGPoint smallCenter = CGPointMake(self.bounds.size.width / 2.0, self.bounds.size.width / 2.0);
    
    UIBezierPath *smallPath = [UIBezierPath bezierPathWithArcCenter:smallCenter radius:self.bounds.size.width / 2 - _progressRingWidth startAngle:3 * M_PI_2 endAngle:M_PI_2 clockwise:YES];
    
    [smallPath addArcWithCenter:altCenter radius:self.bounds.size.width / 2.0 - radiusKoef startAngle:M_PI_2 endAngle:- M_PI_2 clockwise:YES];
    return smallPath;
}

- (void)drawBackground
{
    [self drawStaticBackground];
    [self drawCustom];
    //Create parameters to draw background
    //The background segments are drawn counterclockwise, start with the outer ring, add an arc counterclockwise.  Then add the coresponding arc for the inner ring clockwise. Then close the path. The line connecting the two arcs is not needed. From tests it seems to be created automatically.
    CGFloat outerStartAngle = - M_PI_2;
    // Skip half of a separation angle, since the first separation will be centered upward.
    outerStartAngle -= (_segmentSeparationAngle / 2.0);
    // Calculate the inner start angle position
    CGFloat innerStartAngle = - M_PI_2;
    innerStartAngle -= (_segmentSeparationInnerAngle / 2.0) + innerRingAngle;
    // Create the path ref that all the paths will be appended
    CGMutablePathRef pathRef = CGPathCreateMutable();
    
    // Create each segment
    CGPoint center = CGPointMake(self.bounds.size.width / 2.0, self.bounds.size.width / 2.0);
    for (int i = 0; i < _numberOfSegments - [self numberOfFullSegments]; i++) {
        //if (_numberOfSegments - [self numberOfFullSegments] > 8)
        {
            // Create the outer ring segment
            UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:center radius:(self.bounds.size.width / 2.0) startAngle:outerStartAngle endAngle:(outerStartAngle - outerRingAngle) clockwise:NO];
            //Create the inner ring segment
            [path addArcWithCenter:center radius:(self.bounds.size.width / 2.0) - _progressRingWidth  startAngle:innerStartAngle endAngle:innerStartAngle + innerRingAngle clockwise:YES];
            // Add the segment to the path
            CGPathAddPath(pathRef, NULL, path.CGPath);
        }
        //Setup for the next segment
        outerStartAngle -= (outerRingAngle + _segmentSeparationAngle);
        innerStartAngle -= (innerRingAngle + _segmentSeparationInnerAngle);
    }
    
    //Set the path
    _backgroundLayer.path = pathRef;
    
    self.layer.mask = _customLayer;
    
    CGPathRelease(pathRef);
}

-(void)drawCustom
{
    UIBezierPath *overlayPath = [UIBezierPath bezierPathWithRect:self.bounds];
    UIBezierPath *transparentPath = [self getCustomPath];
    [overlayPath appendPath:transparentPath];
    [overlayPath setUsesEvenOddFillRule:YES];
    _customLayer.path = overlayPath.CGPath;
    return;
}

- (void)drawInstantProgress
{
    // Create parameters to draw background
    // The progress segments are drawn clockwise, start with the outer ring, add an arc clockwise.  Then add the coresponding arc for the inner ring counterclockwise. Then close the path. The line connecting the two arcs is not needed. From tests it seems to be created automatically.
    CGFloat outerStartAngle = - M_PI_2;
    // Skip half of a separation angle, since the first separation will be centered upward.
    outerStartAngle += (_segmentSeparationAngle / 2.0);
    // Calculate the inner start angle position
    CGFloat innerStartAngle = - M_PI_2;
    innerStartAngle += (_segmentSeparationInnerAngle / 2.0) + innerRingAngleAlt;
    // Create the path ref that all the paths will be appended
    CGMutablePathRef pathRef = CGPathCreateMutable();
    // Create each segment
    CGPoint center = CGPointMake(self.bounds.size.width / 2.0, self.bounds.size.width / 2.0);
    
    
    BOOL returnFrom = NO;
    for (int i = 0; i < [self numberOfFullSegmentsAlt]; i++) {
        // Create the outer ring segment
        UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:center radius:(self.bounds.size.width / 2.0) startAngle:outerStartAngle endAngle:(outerStartAngle + outerRingAngleAlt) clockwise:YES];
        [path addArcWithCenter:center radius:(self.bounds.size.width / 2.0) - _progressRingWidth startAngle:innerStartAngle endAngle:(innerStartAngle - innerRingAngleAlt) clockwise:NO];
        // Add the segment to the path
        CGPathAddPath(pathRef, NULL, path.CGPath);
        
        // Setup for the next segment
        outerStartAngle += (outerRingAngleAlt + _segmentSeparationAngleAlt);
        innerStartAngle += (innerRingAngleAlt + _segmentSeparationInnerAngleAlt);
    }
    
    if (!returnFrom)
    {
        // Set the progress path
        _progressLayer.path = pathRef;
        CGPathRelease(pathRef);
    }
    else
    {
        CGPathRelease(pathRef);
    }
}

#pragma mark - Utility and time processing

- (NSString *)formattedStringForDuration:(NSTimeInterval)duration
{
    NSInteger minutes = floor(duration/60);
    NSInteger seconds = round(duration - minutes * 60);
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
}

-(void)setTotalTime:(NSTimeInterval)time
{
    self.duration = time;
    _durationChanged = YES;
    // Reset
    [self setProgress:1. animated:NO];
}

-(void)setCurrrentTime:(NSTimeInterval)time animated:(BOOL)animated
{
    CGFloat progress = (CGFloat)time / (CGFloat)_duration;
    if (progress >= 0.)
    {
        BOOL animate = animated;
        if (_durationChanged)
        {
            // Don't animate jump
            animate = NO;
            _durationChanged = NO;
        }
        [self setProgress:progress animated:animate];
    }
    else
    {
        [self setProgress:0. animated:YES];
    }
}

@end
