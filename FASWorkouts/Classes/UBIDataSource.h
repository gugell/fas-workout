//
//  UBIDataSource.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 2/20/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

extern NSString * const kUBIDataSourceTitle;
extern NSString * const kUBIDataSourceSubtitle;
extern NSString * const kUBIDataSourceCellIdentifier;
extern NSString * const kUBIDataSourceItems;
extern NSString * const kUBIDataSourceItemID;
extern NSString * const kUBIDataSourceItemInfo;

@protocol UBIDataSource <NSObject>
@optional
- (NSDictionary *)sectionInfoAtIndexPath:(NSIndexPath *)indexPath;
- (NSArray *)itemsForSection:(NSUInteger)section;
- (NSDictionary *)itemAtIndexPath:(NSIndexPath *)indexPath;
- (NSString *)cellIdentifierAtIndexPath:(NSIndexPath *)indexPath;
- (UITableViewCell *)newCellWithIdentifier:(NSString *)cellIdentifier;
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
- (NSUInteger)itemIDatIndexPath:(NSIndexPath *)indexPath;
- (void)didUpdateDataSource;
@end
