//
//  FASActivityCell.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/4/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit

class FASActivityCell: UITableViewCell {
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var statusLabel: UILabel!
    
    override func prepareForReuse() {
        activityIndicator.startAnimating()
    }
}