//
//  XJSafeMutableArray.m
//  FASWorkouts
//

#import "XJSafeMutableArray.h"

@interface XJSafeMutableArray()
@property NSMutableArray *      backingArray;
@property dispatch_queue_t      lockQueue;
- (instancetype)init NS_DESIGNATED_INITIALIZER;
@end

@implementation XJSafeMutableArray

+ (XJSafeMutableArray *)array
{
    return [XJSafeMutableArray new];
}

+ (XJSafeMutableArray *)arrayWithArray:(NSArray *)array
{
    return [[XJSafeMutableArray alloc] initWithArray:array];
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _lockQueue = dispatch_queue_create("XJSafeMutableArray", DISPATCH_QUEUE_CONCURRENT);
        _backingArray = [NSMutableArray array];
    }
    return self;
}

- (instancetype)initWithCapacity:(NSUInteger)numItems
{
    self = [self init];
    if (self) {
        _backingArray = [[NSMutableArray alloc] initWithCapacity:numItems];
    }
    return self;
}

- (instancetype)initWithArray:(NSArray *)array
{
    self = [self init];
    if (self) {
        [_backingArray addObjectsFromArray:array];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [self init];
    if (self) {
        _backingArray = [[NSMutableArray alloc] initWithCoder:aDecoder];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    dispatch_sync(self.lockQueue, ^{
        [self.backingArray encodeWithCoder:aCoder];
    });
}

- (NSUInteger)count
{
    __block NSUInteger n;
    dispatch_sync(self.lockQueue, ^{
        n = self.backingArray.count;
    });
    return n;
}

- (id)objectAtIndex:(NSUInteger)index
{
    __block id obj;
    dispatch_sync(self.lockQueue, ^{
        obj = [self.backingArray objectAtIndex:index];
    });
    return obj;
}

- (void)insertObject:(id)anObject atIndex:(NSUInteger)index
{
    dispatch_sync(self.lockQueue, ^{
        [self.backingArray insertObject:anObject atIndex:index];
    });
}

- (void)removeObjectAtIndex:(NSUInteger)index
{
    dispatch_sync(self.lockQueue, ^{
        [self.backingArray removeObjectAtIndex:index];
    });
}

- (void)addObject:(id)anObject
{
    dispatch_sync(self.lockQueue, ^{
        [self.backingArray addObject:anObject];
    });
}

- (void)removeLastObject
{
    dispatch_sync(self.lockQueue, ^{
        [self.backingArray removeLastObject];
    });
}

- (void)replaceObjectAtIndex:(NSUInteger)index withObject:(id)anObject
{
    dispatch_sync(self.lockQueue, ^{
        [self.backingArray replaceObjectAtIndex:index withObject:anObject];
    });
}

- (void)removeAllObjects
{
    dispatch_sync(self.lockQueue, ^{
        [self.backingArray removeAllObjects];
    });
}

@end
