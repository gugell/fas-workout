//
//  RadialProgressView.h
//  FAS Workouts
//
//  Created by Vladimir Stepanchenko on 12/15/14.
//  Copyright (c) 2014 Vladimir Stepanchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RadialProgressView : UIView

@property(nonatomic, retain) UILabel *titleLabel;
@property(nonatomic, retain) UILabel *captionLabel;

@property(nonatomic) CGFloat progress;

@end
