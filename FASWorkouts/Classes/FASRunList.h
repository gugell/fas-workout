//
//  FASRunList.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/8/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "UBIRunList.h"

@interface FASRunList : UBIRunList
- (instancetype)initWithFieldList:(NSArray *)fieldList entity:(NSString *)entity;
@end
