//
//  ContainerTraitOverrideController.swift
//  FASWorkouts
//
//  Created by Vladimir Stepanchenko on 11/4/15.
//  Copyright © 2015 FAS. All rights reserved.
//

import UIKit

class ContainerTraitOverrideController: UINavigationController {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    var forcedTraitCollection: UITraitCollection? {
        didSet {
            updateForcedTraitCollection()
        }
    }
    
    override func viewDidLoad() {
        setForcedTraitForSize(view.bounds.size)
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        setForcedTraitForSize(size)
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
    }
    
    func setForcedTraitForSize (size: CGSize) {
        // Use CompactW to emulate one of the Any:Any
        if FASData.sharedData().isIPAD && UIDevice.currentDevice().orientation.isPortrait == true {
            forcedTraitCollection = UITraitCollection(horizontalSizeClass: .Compact)
        } else {
            forcedTraitCollection = nil
        }
    }
    
    func updateForcedTraitCollection() {
        for vc in self.childViewControllers {
            setOverrideTraitCollection(self.forcedTraitCollection, forChildViewController: vc)
        }
    }
    
    override func shouldAutomaticallyForwardAppearanceMethods() -> Bool {
        return true
    }
    
    override func shouldAutomaticallyForwardRotationMethods() -> Bool {
        return true
    }
}
