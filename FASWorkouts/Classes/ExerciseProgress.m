//
//  ExerciseProgress.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/9/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "ExerciseProgress.h"

NSString * const FASAPIHistoryExerciseIDKey = @"kFASAPIHistoryExerciseIDKey";
NSString * const FASAPIHistoryExerciseRepeatCountKey = @"kFASAPIHistoryExerciseRepeatCountKey";
NSString * const FASAPIHistoryExerciseCycleKey = @"kFASAPIHistoryCycleKey";


@interface ExerciseProgress (PrimitiveAccessors)
@property (nonatomic, strong) NSDate *primitiveCreatedOn;
@property (nonatomic) NSNumber* primitiveStatus;
@property (nonatomic) NSNumber* primitiveCircle;
@property (nonatomic) NSNumber* primitiveRepeatCount;
@end

@implementation ExerciseProgress

@dynamic circle;
@dynamic createdOn;
@dynamic exercise_id;
@dynamic finishedAt;
@dynamic repeatCount;
@dynamic session_id;
@dynamic startedAt;
@dynamic status;
@dynamic workout_id;

///////////////////////////////////////////////
#pragma mark -
#pragma mark Accessors
#pragma mark -
///////////////////////////////////////////////

- (void)setStatus:(NSInteger)status
{
    [self willChangeValueForKey:@"status"];
    self.primitiveStatus = @(status);
    [self didChangeValueForKey:@"status"];
}

- (NSInteger)status
{
    [self willAccessValueForKey:@"status"];
    NSNumber* tmpStatus = [self primitiveStatus];
    [self didAccessValueForKey:@"status"];
    return tmpStatus.integerValue;
}

- (void)setCircle:(NSInteger)circle
{
    [self willChangeValueForKey:@"circle"];
    self.primitiveCircle = @(circle);
    [self didChangeValueForKey:@"circle"];
}

- (NSInteger)circle
{
    [self willAccessValueForKey:@"circle"];
    NSNumber* tmpCircle = [self primitiveCircle];
    [self didAccessValueForKey:@"circle"];
    return tmpCircle.integerValue;
}

- (void)setRepeatCount:(NSInteger)repeatCount
{
    [self willChangeValueForKey:@"repeatCount"];
    self.primitiveRepeatCount = @(repeatCount);
    [self didChangeValueForKey:@"repeatCount"];
}

- (NSInteger)repeatCount
{
    [self willAccessValueForKey:@"repeatCount"];
    NSNumber* tmpRepeatCount = [self primitiveRepeatCount];
    [self didAccessValueForKey:@"repeatCount"];
    return tmpRepeatCount.integerValue;
}

-(NSDictionary *)apiParams
{
    return @{FASAPIHistoryExerciseIDKey : self.exercise_id,
             FASAPIHistoryExerciseRepeatCountKey : @(self.repeatCount),
             FASAPIHistoryExerciseCycleKey : @(self.circle)};
}

@end

///////////////////////////////////////////////
#pragma mark -
#pragma mark Extension NSManagedObject
#pragma mark -
///////////////////////////////////////////////

@implementation ExerciseProgress (NSManageedObjectOverrides)

- (void)awakeFromInsert {
    [super awakeFromInsert];
    self.primitiveCreatedOn = [NSDate date];
}

/*
 If an attempt is made to set the scalar values to nil, use 0 instead of raising an exception.
 */
- (void)setNilValueForKey:(NSString *)key
{
    if ([key isEqualToString:@"circle"]) {
        self.circle = 0;
    }
    else if ([key isEqualToString:@"status"]) {
        self.status = 0;
    }
    else if ([key isEqualToString:@"repeatCount"]) {
        self.repeatCount = 0;
    }
    else {
        [super setNilValueForKey:key];
    }
}

@end

///////////////////////////////////////////////
#pragma mark -
#pragma mark Primitive Accessors
#pragma mark -
///////////////////////////////////////////////

@implementation ExerciseProgress (PrimitiveAccessors)
@dynamic primitiveCreatedOn;
@dynamic primitiveStatus;
@dynamic primitiveCircle;
@dynamic primitiveRepeatCount;
@end