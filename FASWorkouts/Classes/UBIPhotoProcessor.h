//
//  UBIPhotoProcessor.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 3/19/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIApplication.h>

typedef NS_OPTIONS(NSUInteger, UBIPhotoProcessorOptions) {
    UBIPhotoProcessorOptionsSaveToDisc       = 1 << 0,
    UBIPhotoProcessorOptionsSaveToLibrary    = 1 << 1,
    UBIPhotoProcessorOptionsMakeThumbnail    = 1 << 2,
    UBIPhotoProcessorOptionsFxBlur           = 1 << 3,
};

@interface UBIPhotoProcessor : NSObject

@property (nonatomic) UIBackgroundTaskIdentifier      bgTask;

typedef void (^photoProcessorCompletionHandler_t)(NSDictionary*);

+ (void)photoProcessorWithInfo:(NSDictionary *)info
                       options:(UBIPhotoProcessorOptions)options
                    completion:(photoProcessorCompletionHandler_t)completion;

- (void)processPostWithInfo:(NSDictionary *)info
                   options:(UBIPhotoProcessorOptions)options
                completion:(photoProcessorCompletionHandler_t)completion;

@end
