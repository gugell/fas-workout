//
//  CustomRateView.m
//  FASWorkouts
//
//  Created by Vladimir Stepanchenko on 3/15/15.
//  Copyright (c) 2015 Vladimir Stepanchenko. All rights reserved.
//

#import "CustomRateView.h"

#define LEADING_TRAILING_MARGIN 20.
#define LINE_START_Y 5.
#define LINE_ONLY_HEIGHT 5.

@interface CustomRateView ()

@property(nonatomic) NSUInteger selectedSegment;
@property(nonatomic) CGFloat regularLineWidth;
@property(nonatomic) CGFloat selectedLineWidth;
@property(nonatomic) CGFloat regularRadius;
@property(nonatomic) CGFloat selectedRadius;

@property(nonatomic, retain) CAShapeLayer *mainLayer;
@property(nonatomic, retain) CAShapeLayer *selectedLayer;
@property(nonatomic, retain) CAShapeLayer *lineLayer;

@property(nonatomic, retain) UIButton *easyButton;
@property(nonatomic, retain) UIButton *normalButton;
@property(nonatomic, retain) UIButton *hardButton;

@property(nonatomic, retain) UIImageView *imageView;

@property(nonatomic) BOOL firstDisplay;

@end

@implementation CustomRateView

- (id)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

#pragma mark - Setup

-(void)setup
{
    self.backgroundColor = [UIColor clearColor];
    self.selectedSegment = 1;
    
    // Size defaults @todo calibrate here
    self.regularLineWidth = 3.;
    self.selectedLineWidth = 1.5;
    
    self.selectedRadius = 16.;
    self.regularRadius = 5.;
    
    // Layer configuration
    self.mainLayer = [CAShapeLayer new];
    self.mainLayer.bounds = self.bounds;
    self.mainLayer.strokeColor = [UIColor whiteColor].CGColor;
    self.mainLayer.fillColor = [UIColor clearColor].CGColor;
    
    self.selectedLayer = [CAShapeLayer new];
    self.selectedLayer.bounds = self.bounds;
    self.selectedLayer.strokeColor = [UIColor whiteColor].CGColor;
    self.selectedLayer.fillColor = [UIColor clearColor].CGColor;
    
    self.lineLayer = [CAShapeLayer new];
    self.lineLayer.bounds = self.bounds;
    self.lineLayer.strokeColor = [UIColor whiteColor].CGColor;
    self.lineLayer.fillColor = [UIColor clearColor].CGColor;
    
    self.imageView = [UIImageView new];
    // @note set proper image
    self.imageView.image = [UIImage imageNamed:@"checkImage"];
    
    //...
    [self.layer addSublayer:self.mainLayer];
    [self.layer addSublayer:self.selectedLayer];
    [self.layer addSublayer:self.lineLayer];
    
    // Control buttons
    self.easyButton = [UIButton new];
    self.easyButton.frame = CGRectMake(0, 0, self.bounds.size.width / 3, self.bounds.size.height);
    [self.easyButton addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.easyButton];
    
    self.normalButton = [UIButton new];
    self.normalButton.frame = CGRectMake(self.bounds.size.width / 3, 0, self.bounds.size.width / 3, self.bounds.size.height);
    [self.normalButton addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.normalButton];
    
    self.hardButton = [UIButton new];
    self.hardButton.frame = CGRectMake(self.bounds.size.width / 3 * 2, 0, self.bounds.size.width / 3, self.bounds.size.height);
    [self.hardButton addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.hardButton];
    
    self.mainLayer.frame = self.bounds;
    self.selectedLayer.frame = self.bounds;
    self.lineLayer.frame = self.bounds;
    
    [self addSubview:self.imageView];
}

-(IBAction)buttonTapped:(id)sender
{
    if (sender == self.easyButton)
    {
        self.selectedSegment = 0;
    }
    else if (sender == self.normalButton)
    {
        self.selectedSegment = 1;
    }
    else if (sender == self.hardButton)
    {
        self.selectedSegment = 2;
    }
    // Report
    if (self.delegate && [self.delegate respondsToSelector:@selector(valueChanged:)])
        [self.delegate valueChanged:self.selectedSegment];
    [self setNeedsDisplay];
}

#pragma mark - Drawing

- (void)layoutSubviews
{
    [super layoutSubviews];
    // Re-frame
    // Buttons
    self.easyButton.frame = CGRectMake(0, 0, self.bounds.size.width / 3, self.bounds.size.height);
    self.normalButton.frame = CGRectMake(self.bounds.size.width / 3, 0, self.bounds.size.width / 3, self.bounds.size.height);
    self.hardButton.frame = CGRectMake(self.bounds.size.width / 3 * 2, 0, self.bounds.size.width / 3, self.bounds.size.height);
    [self setNeedsDisplay];
}

-(void)updateConstraints
{
    [super updateConstraints];
    //self.firstDisplay = NO;
    [self layoutSubviews];
}

- (void)drawRect:(CGRect)rect {
    // Drawing code
    self.mainLayer.lineWidth = self.regularLineWidth;
    self.selectedLayer.lineWidth = self.selectedLineWidth;
    self.lineLayer.lineWidth = 2.;
    
    UIBezierPath *selectedPath = [UIBezierPath new];
    UIBezierPath *regularPath = [UIBezierPath new];
    UIBezierPath *linePath = [UIBezierPath new];
    
    CGFloat lineYPos;
    CGFloat selectedYKoef = 6.;
    
    CGRect imageRect;
    
    switch (self.selectedSegment) {
        case 0:
        {
            lineYPos = self.selectedRadius + self.regularRadius * 2;// + 2.;
            
            CGRect selectedBounds = CGRectMake(LEADING_TRAILING_MARGIN, self.selectedRadius - selectedYKoef, self.selectedRadius * 2, self.selectedRadius * 2);
            UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:selectedBounds];
            [selectedPath appendPath:path];
            
            imageRect = selectedBounds;
            
            CGPoint center = self.center;
            CGRect centerBounds = CGRectMake(center.x - self.regularRadius, self.selectedRadius + self.regularRadius, self.regularRadius * 2, self.regularRadius * 2);
            UIBezierPath *centerPath = [UIBezierPath bezierPathWithOvalInRect:centerBounds];
            [regularPath appendPath:centerPath];
            
            
            CGRect rightBounds = CGRectMake(self.bounds.size.width - LEADING_TRAILING_MARGIN - self.regularRadius, self.selectedRadius + self.regularRadius, self.regularRadius * 2, self.regularRadius * 2);
            
            UIBezierPath *rightPath = [UIBezierPath bezierPathWithOvalInRect:rightBounds];
            [regularPath appendPath:rightPath];
            
            
            [linePath moveToPoint:CGPointMake(LEADING_TRAILING_MARGIN + self.selectedRadius * 2, lineYPos)];
            [linePath addLineToPoint:CGPointMake(center.x - self.regularRadius, self.selectedRadius + self.regularRadius * 2)];
            
            [linePath moveToPoint:CGPointMake(center.x + self.regularRadius, lineYPos)];
            [linePath addLineToPoint:CGPointMake(self.bounds.size.width - LEADING_TRAILING_MARGIN - self.regularRadius, lineYPos)];
            
            break;
        }
        case 1:
        {
            lineYPos = self.selectedRadius + self.regularRadius * 2;
            
            CGRect selectedBounds = CGRectMake(LEADING_TRAILING_MARGIN, self.selectedRadius + self.regularRadius, self.regularRadius * 2, self.regularRadius * 2);
            UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:selectedBounds];
            [regularPath appendPath:path];
            
            
            CGPoint center = self.center;
            CGRect centerBounds = CGRectMake(center.x - self.selectedRadius, self.selectedRadius - selectedYKoef, self.selectedRadius * 2, self.selectedRadius * 2);
            UIBezierPath *centerPath = [UIBezierPath bezierPathWithOvalInRect:centerBounds];
            [selectedPath appendPath:centerPath];
            
            imageRect = centerBounds;
            
            CGRect rightBounds = CGRectMake(self.bounds.size.width - LEADING_TRAILING_MARGIN - self.regularRadius, self.selectedRadius + self.regularRadius, self.regularRadius * 2, self.regularRadius * 2);
            
            UIBezierPath *rightPath = [UIBezierPath bezierPathWithOvalInRect:rightBounds];
            [regularPath appendPath:rightPath];
            
            
            [linePath moveToPoint:CGPointMake(LEADING_TRAILING_MARGIN + self.regularRadius * 2, lineYPos)];
            [linePath addLineToPoint:CGPointMake(center.x - self.selectedRadius, self.selectedRadius + self.regularRadius * 2)];
            
            [linePath moveToPoint:CGPointMake(center.x + self.selectedRadius, lineYPos)];
            [linePath addLineToPoint:CGPointMake(self.bounds.size.width - LEADING_TRAILING_MARGIN - self.regularRadius, lineYPos)];
            
            break;
        }
        case 2:
        {
            lineYPos = self.selectedRadius + self.regularRadius * 2;
            
            CGRect leftBounds = CGRectMake(LEADING_TRAILING_MARGIN, self.selectedRadius + self.regularRadius, self.regularRadius * 2, self.regularRadius * 2);
            UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:leftBounds];
            [regularPath appendPath:path];
            
            
            CGPoint center = self.center;
            CGRect centerBounds = CGRectMake(center.x - self.regularRadius, self.selectedRadius + self.regularRadius, self.regularRadius * 2, self.regularRadius * 2);
            UIBezierPath *centerPath = [UIBezierPath bezierPathWithOvalInRect:centerBounds];
            [regularPath appendPath:centerPath];
            
            
            CGRect rightBounds = CGRectMake(self.bounds.size.width - LEADING_TRAILING_MARGIN - self.selectedRadius - self.regularRadius, self.selectedRadius - selectedYKoef, self.selectedRadius * 2, self.selectedRadius * 2);
            
            imageRect = rightBounds;
            
            UIBezierPath *rightPath = [UIBezierPath bezierPathWithOvalInRect:rightBounds];
            [selectedPath appendPath:rightPath];
            
            [linePath moveToPoint:CGPointMake(LEADING_TRAILING_MARGIN + self.regularRadius * 2, lineYPos)];
            [linePath addLineToPoint:CGPointMake(center.x - self.regularRadius, self.selectedRadius + self.regularRadius * 2)];
            
            [linePath moveToPoint:CGPointMake(center.x + self.regularRadius, lineYPos)];
            [linePath addLineToPoint:CGPointMake(self.bounds.size.width - LEADING_TRAILING_MARGIN - self.selectedRadius - self.regularRadius, lineYPos)];
            
            break;
        }
        default:
            break;
    }
    // Custom image from Asset catalog (rect gets adjusted to more presentable size here)
    self.imageView.frame = CGRectMake(imageRect.origin.x + 6., imageRect.origin.y + 6., imageRect.size.width - 12, imageRect.size.height - 14);
    // Set the full path
    self.mainLayer.path = regularPath.CGPath;
    self.selectedLayer.path = selectedPath.CGPath;
    self.lineLayer.path = linePath.CGPath;
}

@end
