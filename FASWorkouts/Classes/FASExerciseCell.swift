//
//  FASExerciseCell.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/14/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

class FASExerciseCell: FASTableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var musclesLabel: UILabel!
    @IBOutlet weak var timeSpanLabel: UILabel!
    @IBOutlet weak var staticLabel: UILabel!
    @IBOutlet weak var supersetIndicatorImageView: UIImageView!
    
    @IBOutlet weak var remoteImageView: UBICustomRemoteImageView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        nameLabel.textColor = UIColor.fasYellowColor()
        staticLabel.textColor = UIColor.fasGrayColor41()
        // TODO: set "static" when we are ready to display the static indicator
        // TODO: localize this
        staticLabel.text = ""
        separatorType = .None
        supersetIndicatorImageView.hidden = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        exercise = nil
    }
    
    var exercise: Exercise? {
        didSet {
            // TODO: here is where we call updateView()
            updateView()
        }
    }
    
    private func updateView() {
        
        remoteImageView.remoteImage = exercise?.remoteIcon()
        remoteImageView.contentMode = UIViewContentMode.ScaleAspectFit;
        
        if let name = exercise?.name {
            nameLabel.text = name
        }
        
        /*if let mt = exercise?.muscleTypes {
            musclesLabel.text = mt
        }*/
        
        if let timeSpan = exercise?.timeSpan.unsignedIntegerValue {
            timeSpanLabel.text = "\(NSString.formattedTimeFromSeconds(timeSpan))"
            timeSpanLabel.textColor = UIColor.fasGrayColor206()
        }
        
        let isStatic: Bool = exercise?.isStatic?.boolValue ?? false
        staticLabel.hidden = !isStatic
        
        updateSupersetIndicator((exercise?.visualPositionInSuperset() ?? FASWorkoutPositionInSupersetType.None))
        
        layoutIfNeeded()
    }
    
    // TODO: pass separator position type
    private func updateSupersetIndicator(position: FASWorkoutPositionInSupersetType) {
        var indicatorImage: UIImage?
        
        switch position {
        case .Top:
            let topIndicatorImage = UIImage(named: "TopSupersetIndicator")?.resizableImageWithCapInsets(UIEdgeInsets(top: 13.0, left: 0.0, bottom: 0.0, right: 0.0), resizingMode: UIImageResizingMode.Stretch)
            indicatorImage = topIndicatorImage
        case .Middle:
            let middleIndicatorImage = UIImage(named: "MiddleSupersetIndicator")?.resizableImageWithCapInsets(UIEdgeInsets(top: 0.0, left: 6.0, bottom: 0.0, right: 4.0), resizingMode: UIImageResizingMode.Stretch)
            indicatorImage = middleIndicatorImage
        case .Bottom:
            let bottomIndicatorImage = UIImage(named: "BottomSupersetIndicator")?.resizableImageWithCapInsets(UIEdgeInsets(top: 0.0, left: 0.0, bottom: 13.0, right: 0.0), resizingMode: UIImageResizingMode.Stretch)
            indicatorImage = bottomIndicatorImage
        default:
            indicatorImage = nil
        }
        
        if let image = indicatorImage {
            supersetIndicatorImageView.image = image
            supersetIndicatorImageView.hidden = false
        }
        else {
            supersetIndicatorImageView.hidden = true
        }
        setNeedsLayout()
    }
}
