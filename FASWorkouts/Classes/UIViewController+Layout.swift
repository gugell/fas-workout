//
//  UIViewController+Layout.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 4/11/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit

extension UIViewController {
    
    // Add constraints to a customView that is added
    // either to self.view or to self.navigationController.view
    //
    // a. When added to self.view, call updateEdgeToEdgeConstraints(yourView) or updateEdgeToEdgeConstraints(ourView, addedToNavigationView: false)
    // b. When added to self.navigationController.view, call updateEdgeToEdgeConstraints(ourView, addedToNavigationView: true)
    
    func updateEdgeToEdgeConstraints(customView: UIView, addedToNavigationView: Bool = false) {
        var viewConstraints: [NSLayoutConstraint] = [NSLayoutConstraint]()
        let views = ["customView": customView]
        
        viewConstraints += NSLayoutConstraint.constraintsWithVisualFormat("H:|[customView]|",
            options: NSLayoutFormatOptions(rawValue: 0),
            metrics: nil,
            views: views)
        viewConstraints += NSLayoutConstraint.constraintsWithVisualFormat("V:|[customView]|",
            options: NSLayoutFormatOptions(rawValue: 0),
            metrics: nil,
            views: views)
        
        if let nanCtrl = navigationController where addedToNavigationView == true {
            nanCtrl.view.removeConstraints(viewConstraints)
            nanCtrl.view.addConstraints(viewConstraints)
        }
        else {
            self.view.removeConstraints(viewConstraints)
            self.view.addConstraints(viewConstraints)
        }
    }
}
