//
//  FASSettingsDataSource.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 2/20/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "UBIArrayDataSource.h"

typedef NS_ENUM(NSUInteger, FASSettingsDataSourceItemID) {
    FASSettingsDataSourceItemIDProfile,
    FASSettingsDataSourceItemIDExercisesBase,
    FASSettingsDataSourceItemIDAudio,
    FASSettingsDataSourceItemIDNotificationsSettings,
    FASSettingsDataSourceItemIDFAQ,
    FASSettingsDataSourceItemIDHealthKit,
    FASSettingsDataSourceItemIDRateApp,
    FASSettingsDataSourceItemIDFeedback,
    FASSettingsDataSourceItemIDShareApp,
    FASSettingsDataSourceItemIDFacebookInviteFriends,
    FASSettingsDataSourceItemIDRestorePurchases,
    FASSettingsDataSourceItemIDDebug,
};

@interface FASMenuDataSource : UBIArrayDataSource
@end
