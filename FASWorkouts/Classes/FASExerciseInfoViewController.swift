//
//  FASExerciseInfoViewController.swift
//  FASWorkouts
//
//  Created by Vladimir Stepanchenko on 3/15/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

private var avStatusObservationContext = 0
private var avCurrentItemStatusObservationContext = 0
private var avPlayerControllerReadyToDisplayObservationContext = 0

// 0 means "no limit", video playback continues till the end of tracks
private let playbackTimeLimit : Int64 = 5

enum FASExerciseInfoViewControllerSegueIdentifier: String {
    case Player = "infoPlayerSegue"
}

class FASExerciseInfoViewController: FASViewController {
    
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var progressView: UIProgressView!
    
    var exercise: Exercise?
    var delegate: FASWorkoutViewControllerDelegate?
    var workoutDownloadTracker: FASAttachDownloadProgreesTracker?
    
    var avPlayerViewController: AVPlayerViewController?
    
    private var shouldStartPlaybackWhenReady = false
    
    var playerConfigured: Bool = false
    
    private let actionController = FASActionController()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        actionController.viewController = self
    }
    
    deinit {
        workoutDownloadTracker?.stop()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        playerConfigured = false
        workoutDownloadTracker = FASAttachDownloadProgreesTracker(context: FASData.sharedData().mainContext, completionClosure: { (downloadProgress, completed) -> () in
            // Display progress
            self.progressView.hidden = completed
            self.progressView.progress = downloadProgress
            
            if completed {
                dispatch_async(dispatch_get_main_queue(),{
                    self.configureWithExercise(self.exercise)
                })
            }
        })
        
        let shareButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "shareButton"), style: UIBarButtonItemStyle.Plain, target: self, action: "shareExerciseDidPress")
        navigationItem.rightBarButtonItem = shareButton
        
        descriptionTextView.text = nil
    }
    
    override func configureView() {
        super.configureView()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar(.Dark)
        shouldStartPlaybackWhenReady = true
    }
    
    override func viewDidDisappear(animated: Bool) {
        shouldStartPlaybackWhenReady = false
        if playerConfigured == true {
            pauseVideo()
            playerConfigured = false
            printlnDebugTrace(" >> avPlayer observer removed (ExerciseInfo)")
            avPlayerViewController!.removeObserver(self, forKeyPath: "readyForDisplay")
            avPlayerViewController!.player = nil
            avPlayerViewController = nil
        }
        super.viewDidDisappear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.configureWithExercise(self.exercise)
    }
    
    ///////////////////////////////////////////////
    // MARK: Navigation bar controls
    ///////////////////////////////////////////////
    override func prefersStatusBarHidden() -> Bool {
        return navigationController?.navigationBarHidden == true
    }
    
    override func preferredStatusBarUpdateAnimation() -> UIStatusBarAnimation {
        return UIStatusBarAnimation.Fade
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.currentDevice().orientation.isLandscape.boolValue {
            navigationController?.setNavigationBarHidden(true, animated: true)
        } else {
            navigationController?.setNavigationBarHidden(false, animated: true)
        }
    }
    
    func configureWithExercise(exercise: Exercise?) {
        
        if let ex = exercise {
            
            if ex.preferredAttachment == nil {
                FASData.sharedData().updatePreferredAttachmentForExercise(ex)
            }
            
            // Track
            FASAnalyticsEventHelper.trackExerciseViewWithName(ex.name, inWorkout: ex.workout.name)
            
            self.title = ex.name
            
            descriptionTextView.text = ex.exerciseDescription
            descriptionTextView.scrollToTopAnimated(false)
            
            if (ex.preferredAttachment != nil) {
                if (ex.preferredAttachment.isFileDownloaded()) {
                    self.configurePlayerViewController(ex.preferredAttachment.filepath, showsPlaybackControls: false)
                }
                else {
                    let array = FASData.sharedData().attachIDArrayWithExercise(exercise!, download: true)
                    if let attachIDs = array as? [NSNumber] {
                        self.workoutDownloadTracker!.startTrackProgress(attachIDs)
                    }
                }
            }
            else {
                printlnDebugWarning("invalid preffered attachment")
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    ///////////////////////////////////////////////
    // MARK: Video
    ///////////////////////////////////////////////
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let segueIdentifier = segue.identifier, doExerciseViewSegueIdentifier = FASExerciseInfoViewControllerSegueIdentifier(rawValue: segueIdentifier) {
            switch doExerciseViewSegueIdentifier {
            case .Player:
                if let playerVC = segue.destinationViewController as? AVPlayerViewController {
                    avPlayerViewController = playerVC
                }
            }
        }
    }
    
    private func configurePlayerViewController(filepath: NSURL?, showsPlaybackControls: Bool) {
        if avPlayerViewController == nil {
            playerConfigured = false
            return
        }
        if playerConfigured {
            return
        }
        // observe when first frame is ready to be displayed
        if let assetURL = filepath {
            self.progressView.hidden = true
            let asset: AVAsset = AVAsset(URL: assetURL)
            
            let playerItem = AVPlayerItem(asset: asset)
            let player = AVPlayer(playerItem: playerItem)
            
            avPlayerViewController!.player = player as AVPlayer
            
            if let ex = exercise {
                if playbackTimeLimit > 0 && ex.workout.isPaid() == true && ex.workout.isPurchased() == false {
                    let cmTime = CMTimeMake(playbackTimeLimit, 1)
                    let nsValueFromTime = NSValue(CMTime: cmTime)
                    player.addBoundaryTimeObserverForTimes([nsValueFromTime], queue: nil, usingBlock: { () -> Void in
                        self.pauseAndSeekToStart()
                    })
                } else {
                    // Enable playback controls
                    avPlayerViewController!.showsPlaybackControls = true
                }
            }
            
            printlnDebugTrace(" >> observer added")
            avPlayerViewController!.addObserver(self, forKeyPath: "readyForDisplay", options:.New, context: &avPlayerControllerReadyToDisplayObservationContext)
            playerConfigured = true
        }
        else {
            printlnDebugError("attachment exists but filepath not available")
        }
    }
    
    private func pauseVideo() {
        avPlayerViewController?.player?.pause()
    }
    
    private func pauseAndSeekToStart() {
        self.pauseVideo()
        let cmTime = CMTimeMake(0, 1)
        avPlayerViewController?.player?.seekToTime(cmTime)
    }
    
    private func playVideo() {
        if shouldStartPlaybackWhenReady {
            avPlayerViewController?.player?.play()
        }
    }
    
    ///////////////////////////////////////////////
    // MARK: KVO
    ///////////////////////////////////////////////
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if context == &avPlayerControllerReadyToDisplayObservationContext {
            // Autoplay for paid (controls are hidden)
            if let ex = exercise {
                if playbackTimeLimit > 0 && ex.workout.isPaid() == true && ex.workout.isPurchased() == false {
                    self.playVideo()
                }
            }
        }
        else {
            super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)
        }
    }
    
    ///////////////////////////////////////////////
    // MARK: Actions
    ///////////////////////////////////////////////
    
    func shareExerciseDidPress() {
        actionController.fbShareContentWithItem(self.exercise, completion: nil)
    }
}

