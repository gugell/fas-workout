//
//  FASTableViewCell.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/7/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

enum FASTableViewCellSeparator {
    case Default, None
}

class FASTableViewCell: UITableViewCell {
    
    var separatorType: FASTableViewCellSeparator {
        didSet {
            if separatorSingleLineView == nil {
                separatorSingleLineView = UIView()
                separatorSingleLineView!.backgroundColor = UIColor.lightGrayColor()
                separatorSingleLineView!.alpha = 0.5
                separatorSingleLineView!.translatesAutoresizingMaskIntoConstraints = false
                self.addSubview(separatorSingleLineView!)
            }
            
            switch separatorType {
            case .None:
                separatorSingleLineView?.hidden = true
            case .Default:
                separatorSingleLineView!.hidden = false
            }
        }
    }
    private var separatorSingleLineView: UIView?
    
    required init?(coder aDecoder: NSCoder) {
        separatorType = .None
        super.init(coder: aDecoder)
    }

    override func updateConstraints() {
        
        switch (separatorType) {
        case .None:
            0
        default:
            let views = ["separatorView":separatorSingleLineView!]
            
            var separatorConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-(5)-[separatorView]|", options: [], metrics: nil, views: views)
            let bottomSpaceConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: separatorSingleLineView!, attribute: NSLayoutAttribute.Bottom, multiplier: 1.0, constant: 0.0)
            separatorConstraints.append(bottomSpaceConstraint)
            
            self.removeConstraints(separatorConstraints)
            self.addConstraints(separatorConstraints)
           
            let heightConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[separatorView(==0.5)]", options: [], metrics: nil, views: views)
            
            separatorSingleLineView!.removeConstraints(heightConstraints)
            separatorSingleLineView!.addConstraints(heightConstraints)
        }
        
        super.updateConstraints()
    }
}
