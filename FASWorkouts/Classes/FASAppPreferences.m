//
//  FASAppPreferences.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 8/15/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "FASAppPreferences.h"
// import Swift extensions for NSDate
#import "FASWorkouts-Swift.h"

@implementation FASAppPreferences

@dynamic purchasesRestoredDate;
@dynamic notificationsLocalDate;
@dynamic notificationsLocalEnabledByUser;
@dynamic notificationsRemotePromoEnabled;
@dynamic hasPromptedNotificationSettingsChange;

- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}

- (void)resetToDefaults
{
    self.purchasesRestoredDate = nil;
    self.notificationsLocalEnabledByUser = NO;
    self.notificationsLocalDate = [NSDate todayAtNineAM];
    self.notificationsRemotePromoEnabled = NO;
    self.hasPromptedNotificationSettingsChange = NO;
}

- (BOOL)isPurchasesRestored
{
    return (BOOL)(self.purchasesRestoredDate != nil);
}

@end
