//
//  FASData.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/15/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "FASAPI.h"
#import "FASWorkoutSession.h"
#import "ExerciseProgressEvent.h"
#import "UBILogger.h"
#import "FASProductProtocol.h"

NS_ASSUME_NONNULL_BEGIN
extern NSString * const FASDataSkipWelcomeScreenPrefKey;

// Workout notifications
extern NSString * const FASDataWorkoutsWillUpdateNotification;
extern NSString * const FASDataWorkoutsDidUpdateNotification;
extern NSString * const FASDataWorkoutsUpdateDidFailNotification;

// Exercises notifications
extern NSString * const FASDataExercisesWillUpdateNotification;
extern NSString * const FASDataExercisesDidUpdateNotification;
extern NSString * const FASDataExercisesUpdateDidFailNotification;

extern NSString * const FASDataBestResultsDidUpdateNotification;

// Workout history notifications
extern NSString * const FASDataWorkoutHistoryWillUpdateNotification;
extern NSString * const FASDataWorkoutHistoryDidUpdateNotification;
extern NSString * const FASDataWorkoutHistoryUpdateDidFailNotification;

// FAQ notifications
extern NSString * const FASDataFAQDidUpdateNotification;
extern NSString * const FASDataFAQDidFailNotification;

// In-App Product notifications
extern NSString * const FASDataProductsDidUpdateNotification;
extern NSString * const FASDataPurchasesRestoreDidUpdateNotification;

//Entity
extern NSString * const kFASDataCoreDataWorkoutEntityName;

// protocol to notify delegates about data events
@protocol FASDataObserver
- (void)fasDataNotification:(NSNotification *)notification;
@optional
- (void)fasDataWorkoutsWillUpdate;
- (void)fasDataWorkoutsDidUpdate:(NSNotification *)notification;
- (void)fasDataWorkoutsUpdateIsDeferred;
- (void)fasDataWorkoutsUpdateDidFail;
@end

// cache data
typedef NS_ENUM(NSUInteger, FASDataCache) {
    FASDataCachePurchases,
    FASDataCacheWorkoutProgress,
};

@class Workout;
@class Exercise;
@class Attach;

@interface FASData : NSObject
<
FASAPIDelegate
>
// singleton constructor
+ (instancetype)sharedData;

// Core Data stack
@property (readonly, strong, nonatomic) NSManagedObjectContext *mainContext;
@property (readonly, strong, nonatomic) NSManagedObjectContext *workerContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)restore;

- (void)updateSessionData;

typedef NS_ENUM(NSUInteger, FASDataDeferredTaskType) {
    FASDataDeferredNone                         = 0,
    FASDataDeferredScheduleLocalNotification,
};
@property (nonatomic, readonly) FASDataDeferredTaskType deferredTask;

// user-facing notification types same for both, local & remote notifications
@property (nonatomic, readonly) UIUserNotificationType requiredNotificationType;

// workout session
@property (nonatomic, strong, readonly, nullable) FASWorkoutSession* currentWorkoutSession;

@property (nonatomic, strong, readonly, nullable) NSArray<NSDictionary<NSString *, id>*> *bestResults;

// core data helpers

// Workouts fetch requests
- (NSFetchedResultsController *)fetchedWorkoutsControllerWithCacheName:(NSString * _Nullable)cacheName;
- (NSFetchedResultsController *)fetchedWorkoutsControllerWithKeyValueFilter:(NSDictionary <NSString *, id>*)filter
                                                                  cacheName:(NSString * _Nullable)cacheName;

// History request
- (NSFetchedResultsController *)fetchedWorkoutHistoryControllerWithCacheName:(NSString * _Nullable)cacheName;
// FAQ request
- (NSFetchedResultsController *)fetchedFAQControllerWithCacheName:(NSString * _Nullable)cacheName;

// Exercises fetch requests
- (NSFetchedResultsController *)fetchedExercisesControllerWithCacheName:(NSString * _Nullable)cacheName
                                                                context:(NSManagedObjectContext * _Nullable)context
                                                                workout:(Workout * _Nullable)workout;
- (NSFetchedResultsController *)fetchedStoreItemsControllerWithCacheName:(NSString * _Nullable)cacheName;

- (NSManagedObjectContext *)newUIContext;
- (NSManagedObjectContext *)newWorkerContext;

- (NSURL *)applicationDocumentsDirectory;
- (NSString *)applicationCacheDirectory;

// app preferences
- (NSString *)appVersion;
- (NSString *)buildVersion;
- (void)setAppVersionFromBundle;
- (BOOL)skipWelcomeScreen;
- (void)setSkipWelcomeScreen:(BOOL)shouldSkip;
- (BOOL)forceDownloadAttachFiles;
- (void)setForceDownloadAttachFiles:(BOOL)force;

// Workouts
- (void)loadWorkoutsFromRemote;
// workout observers
- (void)addWorkoutsObserver:(id <FASDataObserver>)observer;
- (void)removeWorkoutsObserver:(id <FASDataObserver>)observer;

// Workout history
- (void)loadWorkoutHistoryFromRemote;
// workout history observers
- (void)addWorkoutHistoryObserver:(id <FASDataObserver>)observer;
- (void)removeWorkoutHistoryObserver:(id <FASDataObserver>)observer;

// FAQ entries
- (void)loadFAQFromRemote;
// FAQ observers
- (void)addFAQObserver:(id <FASDataObserver>)observer;
- (void)removeFAQObserver:(id <FASDataObserver>)observer;

// Exercise
- (void)loadExercisesForWorkout:(Workout *)workout;
- (void)loadExercisesForWorkoutIfNeeded:(Workout *)workout;
- (BOOL)isEachExercisesHasAtLeastOneAttachInWorkout:(Workout *)workout;

// exercise observers
- (void)addExercisesObserver:(id <FASDataObserver>)observer;
- (void)removeExercisesObserver:(id <FASDataObserver>)observer;

// Attach
- (void)loadAttachmentsForExercise:(Exercise *)exercise;
- (void)loadAttachmentsForExerciseIfNeeded:(Exercise *)exercise;

- (NSArray*)arrayOfAttachIDStartedDownloadForWorkout:(Workout*)workout;
- (NSArray*)attachIDArrayWithExercise:(Exercise *)exercise download:(BOOL)download;
- (BOOL)hasFilesDownloadedForWorkout:(Workout*)workout;

// Best Result
- (void)getBestExercisesResultsForWorkout:(Workout *)workout;

// Local Notifications
- (NSDate * _Nullable)fireDateForLocalNotificationReminder;

// In-App Purchase
- (void)loadStoreItemsFromRemote;
- (void)addProductObserver:(id <FASDataObserver>)observer;
- (void)removeProductObserver:(id <FASDataObserver>)observer;
- (void)updateStoreProductsInformation;

- (void)restorePurchases;

- (void)purchaseProduct:(id <FASProduct>)product;

// indicates that purchase restoration is in progress
@property (nonatomic, readonly, getter=isRestoringProducts) BOOL restoringProducts;

- (void)addPurchaseObserver:(id <FASDataObserver>)observer;
- (void)removePurchaseObserver:(id <FASDataObserver>)observer;

// cache management
- (void)loadDataTypeFromCache:(FASDataCache)dataType;
- (void)saveCacheForDataType:(FASDataCache)dataType;

// images
- (NSURL *)imageURLForWorkoutWithID:(NSNumber *)identifier;
- (NSURL *)imageURLForExerciseWithID:(NSNumber *)identifier;
- (NSURL *)imageURLForExerciseID:(NSNumber *)identifier;
- (NSURL *)imageURLForWorkoutID:(NSNumber *)identifier;
- (NSURL *)imageURLForExerciseIconWithID:(NSNumber *)identifier;

- (void)saveImage:(UIImage *)image toPath:(NSString *)path;

// Language
- (NSString *)preferredLanguage;
// Returns the current device language, but not the actual language used in the app

- (NSString *)preferredLocalization;
// Returns first language in th preferred order the application is localized with

// network activity
- (void)startNetworkActivity;
- (void)stopNetworkActivity;

- (void)updatePreferredAttachmentForExercise:(Exercise *)exercise;
- (BOOL)allDownloadedPreferredAttachesCopiedForWorkout:(NSArray * _Nullable)exercises;

// system settings
- (void)openSettings;

// user
- (void)preferencesUpdatedWithDictionary:(NSDictionary * _Nullable)prefDictionary;
- (NSUInteger)currentLicenseVersion;

// device capabilities
@property (nonatomic, readonly) BOOL isIPAD;
@property (nonatomic, readonly) BOOL isIPHONE;

// history
- (BOOL)hasWorkoutSession;
- (void)beginWorkoutSessionWithWorkout:(Workout *)workout;
- (void)pauseCurrentWorkoutSession:(BOOL)isPaused;
- (void)rateCurrentWorkoutSessionWithRate:(NSUInteger)rate;
- (void)endCurrentWorkoutSession:(BOOL)isCompleted;
- (void)endCurrentWorkoutSession:(BOOL)isCompleted submit:(BOOL)shouldSubmit;
- (void)submitCurrentWorkoutSession;
- (void)clearCurrentWorkoutSession;

- (void)recordExerciseProgress:(Exercise *)exercise
                        circle:(NSInteger)circle
                          info:(ExerciseProgressEvent *)progressEvent;
// GAI
- (void)trackPageview:(NSString * _Nullable)page;
- (void)trackEvent:(NSString *)event withAction:(NSString *)action;

@end
NS_ASSUME_NONNULL_END