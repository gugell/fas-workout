//
//  FASWorkView.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/10/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit

enum FASWorkViewAction {
    case Pause
}

protocol FASWorkViewDelegate {
    func workView(workView: FASWorkView, didSelectAction action: FASWorkViewAction)
    func skipExercise()
}

class FASWorkView: UIView {
    
    @IBOutlet weak var ringProgressView: CustomSegmentedRing!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var bestResultView : UIView!
    @IBOutlet weak var bestResultLabel : UILabel!
    
    //MARK: Constraints
    // Ring
    @IBOutlet weak var ringHeightConstraint : NSLayoutConstraint!
    @IBOutlet weak var ringTrailingHorisontalConstraint : NSLayoutConstraint!
    
    // General
    @IBOutlet weak var bottomMarginConstraint : NSLayoutConstraint!
    
    // Time/'tap to pause' label
    @IBOutlet weak var timeLabelWidthConstraint : NSLayoutConstraint!
    @IBOutlet weak var timeLabelHeightConstraint : NSLayoutConstraint!
    
    // New constraints for version 1.2
    @IBOutlet weak var resultLeftMarginConstraint : NSLayoutConstraint!
    @IBOutlet weak var skipRightMarginConstraint : NSLayoutConstraint!
    @IBOutlet weak var bestResultHeightConstraint : NSLayoutConstraint!
    
    var delegate: FASWorkViewDelegate?
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.contentMode = UIViewContentMode.Redraw
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func updateConstraints() {
        // Separate value currently for 4S
        if UIScreen.mainScreen().bounds.height < 568 {
            ringHeightConstraint.constant = 82
            bottomMarginConstraint.constant = 0
            ringTrailingHorisontalConstraint.constant = 35
            timeLabelWidthConstraint.constant = 50
            timeLabelHeightConstraint.constant = 50
            bestResultHeightConstraint.constant = 70
            
            resultLeftMarginConstraint.constant = 10
            skipRightMarginConstraint.constant = 10
            
            bestResultLabel.font = UIFont.boldBabasFontOfSize(24)
        } else {
            ringHeightConstraint.constant = 150
            bottomMarginConstraint.constant = 8
            ringTrailingHorisontalConstraint.constant = 20
            timeLabelWidthConstraint.constant = 80
            timeLabelHeightConstraint.constant = 80
            bestResultHeightConstraint.constant = 100
            
            // 'More' centering on 6Plus
            if UIScreen.mainScreen().bounds.height > 667 {
                resultLeftMarginConstraint.constant = 25
                skipRightMarginConstraint.constant = 25
            } else {
                resultLeftMarginConstraint.constant = 10
                skipRightMarginConstraint.constant = 10
            }
            
            bestResultLabel.font = UIFont.boldBabasFontOfSize(50)
        }
        super.updateConstraints()
    }
    
    // MARK: - Accessors
    
    var time: String? {
        didSet {
           timeLabel.text = time
        }
    }
    
    var bestResult: UInt? {
        didSet {
            if let _ = bestResult {
                bestResultLabel.text = "\(bestResult!)"
            }
            else {
                bestResultLabel.text = ""
            }
        }
    }
    
    // MARK: - Actions
    
    @IBAction func didPressPauseButton(sender: AnyObject) {
        delegate?.workView(self, didSelectAction: .Pause)
    }
    
    @IBAction func skipExerciseButtonDidPress(sender : UIButton) {
        delegate?.skipExercise()
    }
}
