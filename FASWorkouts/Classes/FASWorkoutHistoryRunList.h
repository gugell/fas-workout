//
//  FASWorkoutHistoryRunList.h
//  FASWorkouts
//
//  Created by Vladimir Stepanchenko on 4/18/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "FASRunList.h"

extern NSString* const kFASWorkoutHistoryRunListWorkoutHistoryIdFieldKey;
extern NSString* const kFASWorkoutHistoryRunListWorkoutHistoryWorkoutIDFieldKey;
extern NSString* const kFASWorkoutHistoryRunListWorkoutHistoryUserIDFieldKey;
extern NSString* const kFASWorkoutHistoryRunListWorkoutHistoryRateFieldKey;
extern NSString* const kFASWorkoutHistoryRunListWorkoutHistoryEndDateFieldKey;
extern NSString* const kFASWorkoutHistoryRunListWorkoutHistoryPauseDurationFieldKey;

@interface FASWorkoutHistoryRunList : FASRunList

- (instancetype)initWithFieldList:(NSArray *)fieldList NS_DESIGNATED_INITIALIZER;

+ (instancetype)workoutHistoryRunListDefault;

@end