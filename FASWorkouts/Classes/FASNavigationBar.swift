//
//  FASNavigationBar.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/19/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

class FASNavigationBar : UINavigationBar {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        backItem?.title = ""
    }
}