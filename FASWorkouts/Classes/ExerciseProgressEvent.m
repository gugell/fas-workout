//
//  ExerciseProgressEvent.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/8/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "ExerciseProgressEvent.h"

@interface ExerciseProgressEvent ()
@property (nonatomic, readwrite) ExerciseProgressEventType type;
@property (nonatomic, strong, readwrite) id value;
@end

@implementation ExerciseProgressEvent

- (instancetype)init
{
    return [self initWithType:ExerciseProgressEventTypeUnknown value:nil];
}

- (nonnull instancetype)initWithType:(ExerciseProgressEventType)type value:(nullable id)value
{
    self = [super init];
    if (self) {
        self.type = type;
        self.value = value;
    }
    return self;
}

+ (nonnull instancetype)eventWithType:(ExerciseProgressEventType)type value:(nullable id)value
{
    return [[ExerciseProgressEvent alloc] initWithType:type value:value];
}

@end
