//
//  FASAPISessionProtocol.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/20/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

// Notification Center strings

// sent when a login succeeds
extern NSString * const FASAPISessionDidLoginNotification;
// sent when a logout succeeds
extern NSString * const FASAPISessionDidLogoutNotification;
// sent when a login attempt fails
extern NSString * const FASAPISessionDidNotLoginNotification;
// sent when a login attempt is cancelled
extern NSString * const FASAPISessionDidCancelLoginNotification;
// sent when the session is restored
extern NSString * const FASAPISessionDidRestoreSessionNotification;
// sent when the Facebook session status has changed
extern NSString * const FASAPISessionFacebookSessionDidChangeNotification;
// sent when licence agreement is required
extern NSString * const FASAPISessionLicenseAgreementDidChangeNotification;

@protocol FASAPISessionObserver
- (void)FASAPISessionNotification:(NSNotification *)notification;
@end

