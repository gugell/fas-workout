//
//  StoreItem+CoreDataProperties.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/24/16.
//  Copyright © 2016 FAS. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "StoreItem+CoreDataProperties.h"

@implementation StoreItem (CoreDataProperties)

@dynamic inAppProductID;
@dynamic productType;
@dynamic sortIndex;
@dynamic purchaseStatus;
@dynamic bulks;
@dynamic bulkItems;

@end
