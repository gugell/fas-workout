//
//  FASFacebookLoginTableViewCell.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 4/26/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit

class FASFacebookLoginTableViewCell: UITableViewCell {
    
    private let transparentView: UIView = UIView()
    private let separatorView: UIView = UIView()
    private let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .White)
    let titleLabel: UILabel = UILabel()
    let fbIconImageView: UIImageView = UIImageView()
    
    var transparentViewHidden = true
    var hasSeparator = true
    var busy: Bool = false {
        didSet {
            if (busy) {
                activityIndicator.startAnimating()
            }
            else {
                activityIndicator.stopAnimating()
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    private func commonInit() {

        separatorView.backgroundColor = UIColor.whiteColor()
        separatorView.alpha = 0.3
        separatorView.autoresizingMask = [UIViewAutoresizing.FlexibleTopMargin, UIViewAutoresizing.FlexibleWidth]
        separatorView.hidden = !hasSeparator
        
        fbIconImageView.image = UIImage(named: "facebook-logo")
        fbIconImageView.backgroundColor = UIColor.clearColor()
        fbIconImageView.autoresizingMask = [UIViewAutoresizing.FlexibleRightMargin, UIViewAutoresizing.FlexibleBottomMargin]
        fbIconImageView.contentMode = .ScaleAspectFit
        
        transparentView.backgroundColor = UIColor.blackColor()
        transparentView.alpha = 0.5
        transparentView.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        transparentView.hidden = transparentViewHidden
        
        titleLabel.textColor = UIColor.whiteColor()
        titleLabel.font = UIFont.regularBabasFontOfSize(25.0)
        titleLabel.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        titleLabel.textAlignment = .Center
        
        activityIndicator.hidesWhenStopped = true
        
        contentView.addSubview(transparentView)
        contentView.addSubview(separatorView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(fbIconImageView)
        contentView.addSubview(activityIndicator)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        transparentView.frame = contentView.bounds
        
        separatorView.frame = CGRectMake(0.0,transparentView.bounds.height-1.0,transparentView.bounds.width,1.0)
        
        titleLabel.frame = transparentView.bounds
        
        fbIconImageView.sizeToFit()
        let imgSize: CGSize = fbIconImageView.frame.size
        fbIconImageView.frame = CGRectMake(
            5.0,
            transparentView.center.y - imgSize.height/2.0,
            imgSize.width,
            imgSize.height
        )
        
        activityIndicator.center = fbIconImageView.center
    }
}
