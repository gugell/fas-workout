//
//  FASRibbonAccessoryView.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 8/9/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import Foundation

enum FASRibbonAccessoryViewStyle {
    case StripNone
    case StripOrange(String)
    case StripGreen(String)
    case StripYellow(String)
}

class FASRibbonAccessoryView: UIView {
    
    let ribbonView: RibbonView = RibbonView()
    let chevronView: UIImageView = UIImageView()
    var style: FASRibbonAccessoryViewStyle = .StripNone {
        didSet {
            switch style {
            case .StripNone:
                ribbonView.title = nil
                ribbonView.ribbonColor = .Clear
            case let .StripOrange(x):
                ribbonView.title = x
                ribbonView.ribbonColor = .Orange
            case let .StripGreen(x):
                ribbonView.title = x
                ribbonView.ribbonColor = .Green
            case let .StripYellow(x):
                ribbonView.title = x
                ribbonView.ribbonColor = .Yellow
            }
            
            setNeedsDisplay()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        chevronView.image = UIImage(named: "ArrowRight")
        addSubview(chevronView)
        addSubview(ribbonView)
        
        // uncomment to debug layout
//        chevronView.backgroundColor = UIColor.greenColor()
//        ribbonView.backgroundColor = UIColor.yellowColor()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // chevron - vertically centered
        let chevronInsetRight: CGFloat = 5.0
        chevronView.sizeToFit()
        var chevronViewFrame = chevronView.frame
        chevronViewFrame.origin = CGPointMake(CGRectGetWidth(bounds) - chevronViewFrame.size.width - chevronInsetRight, CGRectGetHeight(bounds)/2.0 - chevronViewFrame.size.height/2.0)
        chevronView.frame = chevronViewFrame
        
        // ribbon - align right
        ribbonView.sizeToFit()
        var ribbonViewFrame = ribbonView.frame;
        ribbonViewFrame.origin = CGPointMake(CGRectGetWidth(bounds) - ribbonViewFrame.size.width, 5.0)
        ribbonView.frame = ribbonViewFrame
        bringSubviewToFront(ribbonView) // put ribbon to top, b/c shadow layers might be drawn on top of it
    }
}

