//
//  FAQViewController.swift
//  FASWorkouts
//
//  Created by Vladimir Stepanchenko on 11/12/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import Foundation

// cell identifiers
enum FAQCellIdentifier: String {
    case FAQ = "FAQViewControllerCellIdentifier"
}

class FAQViewController: FASViewController {
    
    private var cacheName: String?
    var fetchedFaqController: NSFetchedResultsController? = nil
    
    @IBOutlet weak var tableView: UITableView!
    
    lazy var fetchedAllFaqEntriesController: NSFetchedResultsController = {
        let frc: NSFetchedResultsController = FASData.sharedData().fetchedFAQControllerWithCacheName(self.cacheName)
        return frc
    }()
    
    ///////////////////////////////////////////////
    // MARK: - UIViewController overrides -
    ///////////////////////////////////////////////
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "FAQ"
        hideBackButtonTitle = true
        
        FASData.sharedData().loadFAQFromRemote()
        
        fetchedFaqController = fetchedAllFaqEntriesController
        fetchedFaqController!.delegate = self
        
        fetch()
        
        tableView.estimatedRowHeight = 91.0
        tableView.rowHeight = 92
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let topInset = topLayoutGuide.length/* + segmentedNavigationBar.frame.height*/
        
        var tableInset: UIEdgeInsets = tableView.contentInset
        tableInset.top = topInset
        tableView.contentInset = tableInset
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar(.Dark)
    }
    
    override func viewWillDisappear(animated: Bool) {
        configureNavigationBar(.DarkWithYellowTitle)
        super.viewWillDisappear(animated)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        automaticallyAdjustsScrollViewInsets = false;
        
        FASData.sharedData().addFAQObserver(self)
    }
    
    deinit {
        FASData.sharedData().removeFAQObserver(self)
    }
    ///////////////////////////////////////////////
    // MARK: Actions
    ///////////////////////////////////////////////
    
    ///////////////////////////////////////////////
    // MARK: - Controls -
    ///////////////////////////////////////////////
    
    func fetch() {
        if let cn = cacheName {
            NSFetchedResultsController.deleteCacheWithName(cn)
        }
        
        if let frc = fetchedFaqController {
            var fetchError: NSError?
            let result: Bool
            do {
                try frc.performFetch()
                result = true
            } catch let error as NSError {
                fetchError = error
                result = false
            }
            if !result {
                if let error = fetchError {
                    printlnDebugError("Unresolved error \(error.localizedDescription),\nerror info: \(error.userInfo)")
                    abort()
                }
            }
        } else {
            printlnDebugError("Fetched Result Controller was nil upon fetch()")
        }
    }
}
///////////////////////////////////////////////
// MARK: - TableView DataSource / Delegate -
///////////////////////////////////////////////
extension FAQViewController: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return fetchedFaqController!.sections!.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedFaqController!.sections![section].numberOfObjects
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let faqCell = tableView.dequeueReusableCellWithIdentifier(FAQCellIdentifier.FAQ.rawValue, forIndexPath: indexPath) as! FAQListCell
        let faqEntry = fetchedFaqController!.objectAtIndexPath(indexPath) as! FAQItem
        faqCell.contentLabelText = faqEntry.question
        return faqCell as UITableViewCell
    }
    
    private func updateTableView() {
        self.tableView.reloadData()
    }
}

extension FAQViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.clearColor()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let faqEntry = fetchedFaqController!.objectAtIndexPath(indexPath) as! FAQItem
        let detailsVC = self.mainStoryboard.instantiateViewControllerWithIdentifier("FAQDetailsViewController") as? FAQDetailsViewController
        if detailsVC != nil {
            detailsVC?.faqEntry = faqEntry
            self.navigationController?.pushViewController(detailsVC!, animated: true)
        }
    }

    // MARK: TableView Helpers

    func selectFirstRow(tableView tv: UITableView, segueIdentifier: String?) {
        let indexPathToSelect = NSIndexPath(forRow: 0, inSection: 0)
        tv.selectRowAtIndexPath(indexPathToSelect, animated: true, scrollPosition: UITableViewScrollPosition.None)
        tv.delegate?.tableView?(tv, didSelectRowAtIndexPath: indexPathToSelect)
    }
}

///////////////////////////////////////////////
// MARK: - NSFetchedResultsControllerDelegate -
///////////////////////////////////////////////
extension FAQViewController: NSFetchedResultsControllerDelegate {
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        updateTableView()
    }
    
}

///////////////////////////////////////////////
// MARK: - FASData Observer Protocol -
///////////////////////////////////////////////
extension FAQViewController: FASDataObserver {
    func fasDataNotification(notification: NSNotification) {
        
        dispatch_async(dispatch_get_main_queue(),{
            if notification.name == FASDataFAQDidUpdateNotification {
                self.updateTableView()
            }
        })
    }
}