//
//  FASRestView.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/14/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

protocol FASRestViewDelegate {
    func restViewDidPressCloseButton(restView: FASRestView)
    func restViewDidPressFinish()
}

enum FASRestViewStyle {
    case BeforeWorkout
    case BeforeNextRound
    case AfterDinamicExercise
    case AfterStaticExercise
}

class FASRestView: FASPhaseView {

    var delegate: FASRestViewDelegate?
    var nextExercise: Exercise? {
        didSet {
            updateExercisePreview()
        }
    }
    var time: String? {
        didSet {
            repeatCounterView.timeLabel.text = time
            circleTimerView.subtitle = time
        }
    }
    
    var timeProgress: Float = 0.0 {
        didSet {
            circleTimerView.progress = timeProgress
        }
    }
    
    var lapInfo: (currentLap: Int, totalLaps: Int) = (0,0) {
        didSet {
            topLabel.text = "rounds \(lapInfo.currentLap)/\(lapInfo.totalLaps)"
        }
    }
    
    var style: FASRestViewStyle = .AfterDinamicExercise {
        didSet {
            switch style {
            case .BeforeWorkout:
                circleTimerView.hidden = false
                repeatCounterView.hidden = true
                // TODO: localize this
                circleTimerView.title = "Get Ready"
                topLabel.hidden = true // no round info
            case .BeforeNextRound:
                circleTimerView.hidden = false
                repeatCounterView.hidden = true
                topLabel.hidden = false
            case .AfterDinamicExercise:
                repeatCounterView.hidden = false
                repeatCounterView.counter.setup()
                circleTimerView.hidden = true
                topLabel.hidden = false
            case .AfterStaticExercise:
                repeatCounterView.hidden = true
                circleTimerView.hidden = false
                topLabel.hidden = false
                
                
            }
        }
    }
   
    
    @IBOutlet weak var visualEffectSubview: UIVisualEffectView!
    @IBOutlet weak var view: UIView!
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var topLabel: UILabel!
    
    @IBOutlet weak var nextExercisePreview: FASExercisePreview!
    
    //MARK: Constraints
    // Progress ring
    @IBOutlet weak var ringWidthConstraint: NSLayoutConstraint!
    // 'Next exercise' (mid) section
    @IBOutlet weak var nextExerciseHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleSubjectMarginConstraint: NSLayoutConstraint!
    @IBOutlet weak var subjectBottomMarginConstraint: NSLayoutConstraint!
    
    // views that changes the appearance of rest view
    @IBOutlet weak var repeatCounterView: FASCounterView!
    @IBOutlet weak var circleTimerView: FASCircleTimerView!
    
    // MARK: Actions
    
    @IBAction func didPressCloseButton(sender: AnyObject) {
        
    
        
        delegate?.restViewDidPressCloseButton(self)
    }
    
    private func updateExercisePreview() {
        nextExercisePreview.exercise = nextExercise
        repeatCounterView.exercise = nextExercise
        if let ex = nextExercise {
            closeButton.hidden = false
        } else {
            closeButton.hidden = true
        }
        
    }
    
    // MARK: Constraint logic
    
    @IBAction func didPressFinish(sender: AnyObject) {
        
        delegate?.restViewDidPressFinish()
        
    }
    override func updateConstraints() {
        // TODO move to superclass (with FASPauseView)
        // Separate value currently for 4S only
        if UIScreen.mainScreen().bounds.height < 568 {
            ringWidthConstraint.constant = 130
            nextExerciseHeightConstraint.constant = 90
            titleSubjectMarginConstraint.constant = 0
            subjectBottomMarginConstraint.constant = 8
        } else {
            ringWidthConstraint.constant = 150
            nextExerciseHeightConstraint.constant = 100
            titleSubjectMarginConstraint.constant = 3
            subjectBottomMarginConstraint.constant = 22
        }
        super.updateConstraints()
    }
}
