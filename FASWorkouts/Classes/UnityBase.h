//
//  UnityBase.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/31/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#ifndef FASWorkouts_UnityBase_h
#define FASWorkouts_UnityBase_h

@protocol UBIStringRepresentable <NSObject>
- (NSString *)stringValue;
@end

@protocol UBIUpdatableWithDictionary <NSObject>
- (void)addValuesFromDictionary:(NSDictionary *)dict;
@end

#define CACHES_PATH [[[NSFileManager defaultManager] URLForDirectory:NSCachesDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:nil] path]

#define DOCUMENTS_PATH [[[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:nil] path]

#endif
