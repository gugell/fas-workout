//
//  NSString+MyExtensions.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/19/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "NSString+MyExtensions.h"
#import "NSData+CRC32.h"

@implementation NSString (MyExtensions)

#pragma mark -
#pragma mark Cutting
#pragma mark -

- (NSString*)stringByTrimmingWhitespaceCharacters
{
    return  [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString *)stringByRemovingFirstOccurrenceOfString:(NSString *)string
{
    if (!string && [string isBlank]) {
        return self;
    }
    
    NSRange replaceRange = [self rangeOfString:string];
    NSString* result = self;
    if (replaceRange.location != NSNotFound){
        result = [self stringByReplacingCharactersInRange:replaceRange withString:@""];
    }
    return result;
}

- (NSString *)stringByTakingFirstPartOfEmailAddress
{
    return [self componentsSeparatedByString:@"@"].firstObject;
}

#pragma mark -
#pragma mark Validation
#pragma mark -

- (BOOL)isEmpty {
    return (self == nil) || ([@"" isEqualToString:[self stringByTrimmingWhitespaceCharacters]]);
}

- (BOOL)isBlank
{
    return [self isEqualToString:@""];
}

#pragma mark -
#pragma mark URL encoding
#pragma mark -

- (NSString *)URLEncodedString
{
    NSString *encodedString;
    encodedString =
    (NSString *)CFBridgingRelease(
                                  CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                          (CFStringRef)self,
                                                                          NULL,
                                                                          (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                          kCFStringEncodingUTF8));
    return encodedString;
}

- (NSString *)URLDecodedString
{
    NSString *decodedString;
    decodedString =
    (NSString *)CFBridgingRelease(
                                  CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL,
                                                                                          (CFStringRef)self,
                                                                                          NULL,
                                                                                          kCFStringEncodingUTF8));
    return decodedString;
}

#pragma mark -
#pragma mark Formatted Seconds
#pragma mark -

// TODO: Probably this is not good to put it here, but have no clue where it would be so
+ (NSString *)formattedTimeFromSeconds:(NSNumber *)seconds
{
    if (seconds == nil) {
        return nil;
    }
    
    NSUInteger sec = [seconds unsignedIntegerValue];
    NSUInteger h = sec / 3600;
    NSUInteger m = (sec / 60) % 60;
    NSUInteger s = sec % 60;
    
    NSString *formattedTime;
    if (h == 0) {
        formattedTime = [NSString stringWithFormat:@"%02lu:%02lu", (unsigned long)m, (unsigned long)s];
    }
    else {
        formattedTime = [NSString stringWithFormat:@"%lu:%02lu:%02lu", (unsigned long)h, (unsigned long)m, (unsigned long)s];
    }
    
    
    return formattedTime;
}
@end
