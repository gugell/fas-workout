//
//  UIImage+MyExtensions.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/15/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    public class func imageWithScreenshotFromView(view: UIView) -> UIImage! {
        if view.frame == CGRect.zero {
            // return an image object
            return UIImage()
        }
        else {
            // get the screenshot
            UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, UIScreen.mainScreen().scale)
            view.drawViewHierarchyInRect(view.bounds, afterScreenUpdates: true)
            let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return image
        }
    }
    
    public class func stretchableImage(named name: String, inset: UIEdgeInsets) -> UIImage? {
        let originalImage = UIImage(named: name)
        let patternInsets = inset
        let stretchableImage = originalImage?.resizableImageWithCapInsets(patternInsets)
        return stretchableImage
    }
}