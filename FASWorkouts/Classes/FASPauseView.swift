//
//  FASPauseView.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/1/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

enum FASPauseViewAction {
    case ExitWorkout, ExerciseLibrary, ResumeWorkout
}

protocol FASPauseViewDelegate{
    func pauseView(pauseView: FASPauseView, didSelectAction action: FASPauseViewAction)
}

class FASPauseView: FASPhaseView {
    
    // MARK: Properties
    
    var delegate: FASPauseViewDelegate?
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var exitWorkoutButton: UIButton!
    @IBOutlet weak var exerciseLibraryButton: UIButton!
    @IBOutlet weak var resumeWorkoutButton: UIButton!
    @IBOutlet weak var title: UILabel!
    
    // 'Next exercise' (mid) section
    var nextExercise: Exercise? {
        didSet {
            updateExercisePreview()
        }
    }
    
    
    
    
    @IBOutlet weak var nextExercisePreview: FASExercisePreview!
    @IBOutlet weak var nextExerciseHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleSubjectMarginConstraint: NSLayoutConstraint!
    @IBOutlet weak var subjectBottomMarginConstraint: NSLayoutConstraint!
    
    var lapsInfo: (currentLap: Int, totalLaps: Int) = (0,0) {
        didSet {
            self.topLabel.text = "rounds \(lapsInfo.currentLap)/\(lapsInfo.totalLaps)"
        }
    }
    
    // MARK: UIKit overrides
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureLabels()
        configureExitWorkoutButton(isHidden: false)
        configureExerciseLibraryButton(isHidden: false)
        configureResumeWorkoutButton(isHidden: false)
    }
    
    // MARK: Methods
    
    @IBAction func buttonDidPress(sender: AnyObject) {
        switch sender {
        case exitWorkoutButton as UIButton:
            delegate?.pauseView(self, didSelectAction: .ExitWorkout)
        case exerciseLibraryButton as UIButton:
            delegate?.pauseView(self, didSelectAction: .ExerciseLibrary)
        case resumeWorkoutButton as UIButton:
            delegate?.pauseView(self, didSelectAction: .ResumeWorkout)
        default:
            printlnDebugWarning("Unexpected sender")
        }
    }
    
    // MARK: Configuration
    
    private func updateExercisePreview() {
        nextExercisePreview.exercise = nextExercise
        
           }
    
    private func configureExitWorkoutButton(isHidden isHidden: Bool) {
        exitWorkoutButton?.hidden = isHidden
        if isHidden == false {
            FASViewHelper.applyStyle(FASViewHelperButtonStyle.Stroked, toButton: exitWorkoutButton)
        }
    }
    
    private func configureExerciseLibraryButton(isHidden isHidden: Bool) {
        exerciseLibraryButton?.hidden = isHidden
        if isHidden == false {
            FASViewHelper.applyStyle(FASViewHelperButtonStyle.Stroked, toButton: exerciseLibraryButton)
        }
    }
    
    private func configureResumeWorkoutButton(isHidden isHidden: Bool){
        resumeWorkoutButton?.hidden = isHidden
        if isHidden == false {
            FASViewHelper.applyStyle(FASViewHelperButtonStyle.Filled, toButton: resumeWorkoutButton)
        }
    }
    
    private func configureLabels() {
        title.textColor = UIColor.fasYellowColor()
        title.font = UIFont.boldBabasFontOfSize(63)
        topLabel.textColor = UIColor.whiteColor()
        topLabel.font = UIFont.boldBabasFontOfSize(22)
    }
}


