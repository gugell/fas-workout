//
//  FASAppPreferences.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 8/15/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import <PAPreferences/PAPreferences.h>

#pragma clang diagnostic push
#pragma clang diagnostic error "-Wobjc-missing-property-synthesis"

@interface FASAppPreferences : PAPreferences
@property (nonatomic, assign) NSDate *purchasesRestoredDate;
@property (nonatomic, readonly, assign, getter=isPurchasesRestored) BOOL purchasesRestored;

@property (nonatomic, assign) NSDate *notificationsLocalDate;
@property (nonatomic, assign) BOOL notificationsLocalEnabledByUser;

@property (nonatomic, assign) BOOL notificationsRemotePromoEnabled;

@property (nonatomic, assign) BOOL hasPromptedNotificationSettingsChange;

- (void)resetToDefaults;

@end

#pragma clang diagnostic pop
