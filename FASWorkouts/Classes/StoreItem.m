//
//  StoreItem.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/24/16.
//  Copyright © 2016 FAS. All rights reserved.
//

#import "StoreItem.h"
#import "UBILogger.h"

@implementation StoreItem
@end

@implementation StoreItem (UBIUpdatableWithDictionary)

- (void)addValuesFromDictionary:(NSDictionary *)dict
{
    for (NSString *key in [dict allKeys]) {
        
        id value = [dict valueForKey:key];
        
        // if value is an array, do nothing
        if ([value isKindOfClass:[NSArray class]]) {
        }
        
        // if value is a dictionary, do nothing
        else if ([value isKindOfClass:[NSDictionary class]]) {
        }
        
        // else, add it directly
        else {
            [self setValue:value forKey:key];
        }
    }
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark NSKeyValueCoding protocol
#pragma mark -
///////////////////////////////////////////////

- (id)valueForUndefinedKey:(NSString *)key
{
    return nil;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    UBILogWarning(@"%@: UNDEFINED value = %@\tkey = %@", NSStringFromClass([self class]), value, key);
}

@end