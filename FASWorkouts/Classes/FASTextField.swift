//
//  FASTextField.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/19/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

// Description:
//
// FASTextField is a text field w/ ability to set placeholder's text color. 
// It also takes into account current text alignment and position placeholder text accordingly

import UIKit

class FASTextField : UITextField {
    
    var placeholderColor: UIColor = UIColor.darkGrayColor() {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override func drawPlaceholderInRect(rect: CGRect) {
        let attr: [String : AnyObject]? = [NSForegroundColorAttributeName: placeholderColor, NSFontAttributeName: font!]
        let boundingRect: CGRect = placeholder!.boundingRectWithSize(rect.size, options: .TruncatesLastVisibleLine, attributes: attr, context: nil)
        var drawingPoint: CGPoint
        switch textAlignment {
        case .Left:
            drawingPoint = CGPointMake(0, (rect.size.height/2)-boundingRect.size.height/2);
        case .Center:
            drawingPoint = CGPointMake((rect.size.width/2)-boundingRect.size.width/2, (rect.size.height/2)-boundingRect.size.height/2)
        default:
            // TODO: specify all alignment options
            drawingPoint = CGPointMake(rect.size.width-boundingRect.size.width, (rect.size.height/2)-boundingRect.size.height/2)
        }
        if let notEmptyPlaceholder = placeholder {
            notEmptyPlaceholder.drawAtPoint(drawingPoint, withAttributes: attr)
        }
    }
}