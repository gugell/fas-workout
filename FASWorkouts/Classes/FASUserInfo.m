//
//  FASUserInfo.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 3/20/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "FASUserInfo.h"
#import "FASData.h"
#import "UBIRemoteImageCache.h"
#import "UBIRemoteImage.h"
#import "UBILogger.h"
#import "FASAPIObjectKeys.h"
#import "UnityBase.h"

// image caches
static UBIRemoteImageCache *sUserInfoImageCache = nil;
static UBIRemoteImageCache *sLocalUserInfoImageCache = nil;

@interface FASUserInfo ()
@property (nonatomic, strong, readwrite) NSNumber* userID;
@property (nonatomic, copy) NSString* gender;
@property (nonatomic, copy, readwrite) NSString *email;

@property (nonatomic, strong, readonly) NSString* baseImageFilename;
@end

@implementation FASUserInfo

- (instancetype)init
{
    self = [super init];
    if (self) {
        _local = YES;
    }
    return self;
}

+ (void)initialize
{
    NSString* defaultImageName = @"DefaultAvatar";
    
    if (!sUserInfoImageCache) {
        sUserInfoImageCache = [UBIRemoteImageCache new];
        sUserInfoImageCache.imageFilePrefix = @"userPicture_id";
        sUserInfoImageCache.defaultImageName = defaultImageName;
    }
    
    if (!sLocalUserInfoImageCache) {
        sLocalUserInfoImageCache = [UBIRemoteImageCache new];
        sLocalUserInfoImageCache.basePath = DOCUMENTS_PATH;
        sLocalUserInfoImageCache.imageFilePrefix = @"userPicture_orig";
        sLocalUserInfoImageCache.defaultImageName = defaultImageName;
    }
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark API
#pragma mark -
///////////////////////////////////////////////
- (void)addValuesFromDictionary:(NSDictionary *)dict
{
    for (NSString *key in [dict allKeys]) {
        
        id value = dict[key];
        
        [self setValue:value forKey:key];
    }
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark KVC
#pragma mark -
///////////////////////////////////////////////
- (id)valueForUndefinedKey:(NSString *)key
{
    UBILogWarning(@":%@", key);
    
    id v;
    NSString *mappedKey;
    
    // translate SSDataNotificationIDKey into notification_id;
//    if ([key isEqualToString:SSAPINotificationIDKey]) {
//        mappedKey = @"notification_id";
//    }
    
    if (mappedKey) {
        v = [super valueForKey:mappedKey];
    }
    
    return v;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    UBILogWarning(@"%@ = %@", key, value);
    
    NSString *mappedKey;
    
    // translate SSDataNotificationIDKey into notification_id;
//    if ([key isEqualToString:SSAPINotificationIDKey]) {
//        mappedKey = @"notification_id";
//    }
    
    if (mappedKey) {
        [super setValue:value forKey:mappedKey];
    }
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark NSCoding
#pragma mark -
///////////////////////////////////////////////
- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        self.userID = [decoder decodeObjectForKey:@"userID"];
        self.email = [decoder decodeObjectForKey:@"email"];
        self.gender = [decoder decodeObjectForKey:@"gender"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:self.userID forKey:@"userID"];
    [encoder encodeObject:self.email forKey:@"email"];
    [encoder encodeObject:self.gender forKey:@"gender"];
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark custom accessors
#pragma mark -
///////////////////////////////////////////////

- (NSString *)stringUserID
{
    return [self.userID stringValue];
}

- (BOOL)isValidUserID
{
    // arm64 threat the value from response as being longLongValue
    // armv7, and prior will fit in integerValue
    return (self.userID && [self.userID longLongValue]!=0);
}

- (NSString *)baseImageFilename
{
    if (self.userID) {
        return [NSString stringWithFormat:@"%@.jpg", [self stringUserID]];
    }
    
    else {
        UBILogError(@"userID is empty - baseImageFilename will be nil.");
        return nil;
    }
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark image processing methods
#pragma mark -
///////////////////////////////////////////////
- (UIImage *)userPicture
{
    return [UIImage imageWithContentsOfFile:self.originalPhotoPath];
}

- (void)setUserPicture:(UIImage *)userPicture
{
    UBIRemoteImageCache *remoteImageCahce = sLocalUserInfoImageCache;
    [remoteImageCahce deleteRemoteImageForKey:self.remoteImageKey];
    
    [FASData.sharedData saveImage:userPicture toPath:self.originalPhotoPath];
    
//    [FASData.sharedData saveImage:image toPath:self.imagePath];
//    [FASData.sharedData saveImage:[image exResizedImage:CGSizeMake(100.0, 100.0) zoomLevel:1.0 interpolationQuality:kCGInterpolationDefault] toPath:self.thumbnailPath];
}

// image being used when posting user info to the server
- (UIImage *)userPictureToPost
{
    return [self userPicture];
}

- (NSString *)originalPhotoPath
{
    return [DOCUMENTS_PATH stringByAppendingPathComponent:[self originalPhotoFilename]];
}

- (NSString *)originalPhotoFilename
{
    return [self photoFilenameWithPrefix:@"userPicture_orig"];
}

- (NSString *)photoFilenameWithPrefix:(NSString *)prefix
{
    return [NSString stringWithFormat:@"%@%@", prefix, self.baseImageFilename];
}

- (NSURL *)userPictureURL
{
//    return [NSURL URLWithString:<#construct getDocument string here#>];
    return [NSURL URLWithString:@""];
}

- (NSString *)remoteImageKey
{
    return self.baseImageFilename;
}

- (UBIRemoteImage *)remoteImage
{
    if (!self.baseImageFilename) {
        return nil;
    }
    
    UBIRemoteImageCache *remoteImageCache;
    NSMutableDictionary *loadingInfo = [NSMutableDictionary
                                        dictionaryWithObject:self.baseImageFilename
                                        forKey:kUBIRemoteImageCacheInfoFilename];
    
    if ([self isLocal]) {
        remoteImageCache = sLocalUserInfoImageCache;
    }
    else {
        remoteImageCache = sUserInfoImageCache;
        [loadingInfo setValue:self.userPictureURL
                       forKey:kUBIRemoteImageCacheInfoURL];
    }
    
    return [remoteImageCache
            remoteImageForKey:self.remoteImageKey
            loadingInfo:loadingInfo];
}

- (void)refreshImage
{
    NSString* imageKey = [self remoteImageKey];
    UBIRemoteImageCache *remoteImageCache;
    if ([self isLocal]) {
        remoteImageCache = sLocalUserInfoImageCache;
        [remoteImageCache setRemoteImage:nil
                                  forKey:imageKey];
    }
    else {
        remoteImageCache = sUserInfoImageCache;
        [remoteImageCache deleteRemoteImageForKey:imageKey];
    }
}
// TODO: return scaled down image to upload to the server

//- (void)setIsSavingPhoto:(BOOL)isSavingPhoto
//{
//    if (_isSavingPhoto != isSavingPhoto) {
//        _isSavingPhoto = isSavingPhoto;
//        [NSNotificationCenter.defaultCenter postNotificationName:SightingIsSavingPhotoDidChangeNotification object:self];
//    }
//}

//- (NSString *)thumbnailFilename
//{
//    if ([self isUserPost]) {
//        return [self photoFilenameWithPrefix:@"thumb_orig"];
//    }
//    else {
//        return [self photoFilenameWithPrefix:@"thumb_id"];
//    }
//}
//
//- (NSString *)thumbnailPath
//{
//    NSString *basePath;
//    if ([self isUserPost]) {
//        basePath = DOCUMENTS_PATH;
//    }
//    else {
//        basePath = CACHES_PATH;
//    }
//    return [basePath stringByAppendingPathComponent:[self thumbnailFilename]];
//}
//
//- (NSString *)imageFilename
//{
//    if ([self isUserPost]) {
//        return [self photoFilenameWithPrefix:@"image_orig"];
//    }
//    else {
//        return [self photoFilenameWithPrefix:@"image_id"];
//    }
//}
//
//- (NSString *)imagePath
//{
//    NSString *basePath;
//    if ([self isUserPost]) {
//        basePath = DOCUMENTS_PATH;
//    }
//    else {
//        basePath = CACHES_PATH;
//    }
//    return [basePath stringByAppendingPathComponent:[self imageFilename]];
//}

@end
