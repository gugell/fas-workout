//
//  RibbonView.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 8/2/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit

class RibbonView: UIView {
    
    private let orangeRibbon = UIImage(named: "OrangeRibbon")?.resizableImageWithCapInsets(UIEdgeInsets(top: 0.0, left: 6.0, bottom: 0.0, right: 4.0), resizingMode: UIImageResizingMode.Stretch)
    private let greenRibbon = UIImage(named: "GreenRibbon")?.resizableImageWithCapInsets(UIEdgeInsets(top: 0.0, left: 6.0, bottom: 0.0, right: 4.0), resizingMode: UIImageResizingMode.Stretch)
    private let yellowRibbon = UIImage(named: "YellowRibbon")?.resizableImageWithCapInsets(UIEdgeInsets(top: 0.0, left: 6.0, bottom: 0.0, right: 4.0), resizingMode: UIImageResizingMode.Stretch)
    
    private let kColorHexStringOnGreenRibbon = "#223508"
    private let kColorHexStringOnOrangeRibbon = "#571000"
    private let kColorHexStringOnYellowRibbon = "#2b2b2b"
    
    // an attempt to make a letterpressing fx
//    private let kTextAttribsForGreenRibbon =
//    [NSTextEffectAttributeName:NSTextEffectLetterpressStyle
//    NSForegroundColorAttributeName:UIColor(hexString: "#223508")]
//    
//    private let kTextAttribsForOrangeRibbon =
//    [NSTextEffectAttributeName:NSTextEffectLetterpressStyle,
//        NSForegroundColorAttributeName:UIColor(hexString: "#571000")]
    
    var title: String? {
        didSet {
            label.text = title
            setNeedsLayout()
        }
    }
    
    enum RibbonColor {
        case Clear, Green, Orange, Yellow
    }
    var ribbonColor: RibbonColor = .Green {
        didSet {
            switch ribbonColor {
            case .Clear:
                imageView.image = nil
                label.textColor = UIColor.blackColor()
            case .Green:
                imageView.image = greenRibbon
                label.textColor = UIColor(hexString: kColorHexStringOnGreenRibbon)
            case .Orange:
                imageView.image = orangeRibbon
                label.textColor = UIColor(hexString: kColorHexStringOnOrangeRibbon)
            case .Yellow:
                imageView.image = yellowRibbon
                label.textColor = UIColor(hexString: kColorHexStringOnYellowRibbon)
            }
            setNeedsLayout()
        }
    }
    
    private let imageView: UIImageView = UIImageView()
    private let label: UILabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        imageView.image = greenRibbon
        addSubview(imageView)
        
        label.textColor = UIColor(hexString: kColorHexStringOnGreenRibbon)
        label.font = UIFont.regularBabasFontOfSize(13.0)
        label.text = nil
        addSubview(label)
        
        // uncomment to debug layout
//        imageView.backgroundColor = UIColor.cyanColor()
//        label.backgroundColor = UIColor.magentaColor()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        label.sizeToFit()
        var labelFrame = label.frame
        
        if (!CGSizeEqualToSize(labelFrame.size, CGSizeZero) && ribbonColor != .Clear) {
            let imageViewFrameWidth = labelFrame.size.width + 12.0
            let imageViewFrameHeight = labelFrame.size.height + 4.0
            imageView.frame = CGRectMake(0, 0, imageViewFrameWidth, imageViewFrameHeight)
            
            labelFrame.origin = CGPointMake(8.0, 2.0)
            label.frame = labelFrame
        }
        else {
            imageView.frame = CGRectZero
        }
    }
    
    override func sizeThatFits(size: CGSize) -> CGSize {
        label.sizeToFit()
        let labelFrame = label.frame
        
        var height = labelFrame.size.height
        var width = labelFrame.size.width
        
        if (!CGSizeEqualToSize(labelFrame.size, CGSizeZero) && ribbonColor != .Clear) {
            height += 4.0
            // we return 8.0 instead of 12.0 b/c it creates
            // out-of-bounds effect when superview has clipToBounds == false
            width += 8.0
        }
        
        return CGSizeMake(width, height)
    }
}
