//
//  FASAPI.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/19/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "FASAPI.h"
#import "NSString+MyExtensions.h"
#import "NSError+MyExtensions.h"
#import "FASWorkouts-Swift.h"
#import "UBIRunList.h"
#import "FASData.h"
#import "UBILogger.h"

@import UIKit;

static NSString * kToken = @"token";

// server applies RegExp to URL params
// 1 when server checks URL encoded string, 0 otherwise
#define SERVER_USES_URL_ENCODING 0

#ifdef DEBUG
NSTimeInterval const kTimeoutIntervalForRequest = 30;
#else
NSTimeInterval const kTimeoutIntervalForRequest = 30;
#endif

// method param keys
NSString * const FASAPIUsernameParam                            = @"userName";
NSString * const FASAPIPasswordParam                            = @"password";
NSString * const FASAPIPasswordConfirmParam                     = @"password_confirm";
NSString * const FASAPIAuthTypeParam                            = @"AUTHTYPE";
NSString * const FASAPIClientNonceParam                         = @"clientNonce";
NSString * const FASAPIFBTokenParam                             = @"fbToken";
NSString * const FASAPIAPNSTokensKey                            = @"pushTokens";
NSString * const FASAPIAcceptedLegalAgreementVersionKey         = @"acceptedLegalAgreementVersion";
// common keys
NSString * const FASAPIResultKey                                = @"result";
NSString * const FASAPISuccessKey                               = @"success";

// user info keys
NSString * const FASAPIUserIDKey                                = @"userID";
NSString * const FASAPIEmailKey                                 = @"email";
NSString * const FASAPIGenderKey                                = @"gender";
NSString * const FASAPISettingsKey                              = @"settings";
NSString * const FASAPISettingReceiveNewsKey                    = @"setting_pushNotification_news";

// errors keys
NSString * const FASAPIErrorCodeKey                             = @"errCode";
NSString * const FASAPIErrorValueKey                            = @"errMsg";

// session keys
NSString * const FASAPITokenKey                                 = @"token";
NSString * const FASAPISessionSignatureKey                      = @"session_signature";
NSString * const FASAPILogonnameKey                             = @"logonname";
NSString * const FASAPIUserDataKey                              = @"uData";

// API version key
NSString * const FASAPIVersionKey                               = @"api";
NSString * const FASAPIVersionValue                             = @"1";

// const string values
NSString * const kFASAPIDeviceTokenEnvironmentProduction        = @"production";
NSString * const kFASAPIDeviceTokenEnvironmentDevelopment       = @"development";

// notifications
NSString * const FASAPIWillInvokeNotification                   = @"FASAPIWillInvoke";
NSString * const FASAPIDidInvokeNotification                    = @"FASAPIDidInvoke";
NSString * const FASAPIDidFinishNotification                    = @"FASAPIDidFinish";
NSString * const FASAPIDidFailNotification                      = @"FASAPIDidFail";

// data keys
NSString * const FASAPIResultDataKey                            = @"resultData";
NSString * const FASAPIExecParamsKey                            = @"execParams";

// workout
NSString * const FASAPIIdentifier                               = @"ID";
NSString * const FASAPImiModifyDate                             = @"mi_modifyDate";
NSString * const FASAPIMuscleTypes                              = @"muscleTypes.name";
NSString * const FASAPIinAppProductID                           = @"inAppProductID";
NSString * const FASAPIPrimaryMuscleType                        = @"primaryMuscleType.name";
NSString * const FASAPILinkToImageURL                           = @"linkToPhoto";

// workout history
NSString * const FASAPIStartDate                                = @"startDate";
NSString * const FASAPIEndDate                                  = @"endDate";
NSString * const FASAPIWorkoutID                                = @"workoutID";

// store item
NSString * const FASAPIStoreItemItemsKey                        = @"items";
NSString * const FASAPIStoreItemInAppProductID                  = @"inAppProductID";
NSString * const FASAPIStoreItemProductType                     = @"productType";
NSString * const FASAPIStoreItemSortIndex                       = @"sortIndex";

@interface FASAPI ()
@property (nonatomic) BOOL                          invoked;
@property (nonatomic) BOOL                          hasActiveRequest;
@property (nonatomic, strong) NSMutableArray*       requiredParams;
@property (nonatomic) BOOL                          tokenRequired;
@property (nonatomic, copy) NSString*               url;
@property (nonatomic, copy) NSString*               HTTPMethod;
@property (nonatomic) UIBackgroundTaskIdentifier    bgTask;
@property (nonatomic) BOOL                          invokeAsBackgroundTask;
@property (nonatomic) NSURLSession*                 urlSession;
@property (nonatomic) BOOL                          allowsCellularAccess;
@property (nonatomic) NSUInteger                    statusCode;
@property (nonatomic) NSMutableData*                responseData; // TODO: for multiple tasks submited along with a single session, this param might be of mutable dictionary type
@property (nonatomic, readwrite, copy)  NSString *  body;
@end

@interface FASAPI (hidden)
- (instancetype)initWithMethod:(FASAPIMethod)method delegate:(id <FASAPIDelegate>)delegate;

- (NSString *)paramString;
- (BOOL)hasValidPreconditions;
- (BOOL)hasValidToken;
- (BOOL)hasValidParams;

- (void)setURL:(NSString *)URLString secure:(BOOL)secure;
- (NSString *)stringURLWithPath:(NSString *)path secure:(BOOL)secure;
- (NSMutableURLRequest *)request;

- (BOOL)fatalError;
- (void)validateResponse;
- (void)finish;
- (void)performRequest;

@end

// HTTP methods
static NSString * const FASAPIHTTPMethodGet                 = @"GET";
static NSString * const FASAPIHTTPMethodPost                = @"POST";

// method urls
static NSString * const FASAPILoginURL                      = @"auth?";
static NSString * const FASAPIRegisterAccountURL            = @"registration?";
static NSString * const FASAPIForgotPasswordURL             = @"forgotPassword?";
static NSString * const FASAPIUserInfoURL                   = @"userInfo?";
static NSString * const FASAPISetUserInfoURL                = @"setUserInfo?";
static NSString * const FASAPIRunListURL                    = @"runList?";
static NSString * const FASAPIRunTransURL                   = @"runTrans?";
static NSString * const FASAPILogoutURL                     = @"logout?";
static NSString * const FASAPIFacebookLoginURL              = @"oauthfb?";
static NSString * const FASAPIGetBestResultsURL             = @"getUserBestResult?";
static NSString * const FASAPIGetWorkoutURL                 = @"getWorkout?";
static NSString * const FASAPIGetStoreItemsURL              = @"getStoreProduct?";


@implementation FASAPI
///////////////////////////////////////////////
#pragma mark -
#pragma mark Auth
#pragma mark -
///////////////////////////////////////////////
+ (FASAPI *)loginStartWithUsername:(NSString *)username
                          delegate:(id <FASAPIDelegate>)delegate
{
    FASAPI *theAPI = [[FASAPI alloc] initWithMethod:FASAPIMethodLoginStart
                                           delegate:delegate];
    
    [theAPI.requiredParams addObject:FASAPIUsernameParam];
    [theAPI.requiredParams addObject:FASAPIAuthTypeParam];
    
    [theAPI.params setValue:username forKey:FASAPIUsernameParam];
    [theAPI.params setValue:@"UB" forKey:FASAPIAuthTypeParam];
    
    [theAPI setURL:FASAPILoginURL secure:FASEnvironment.APISecure];
    
    return theAPI;
}

+ (FASAPI *)loginWithClientNonce:(NSString *)clientNonce
                        username:(NSString *)username
                     andPassword:(NSString *)password
                     delegate:(id <FASAPIDelegate>)delegate
{
    FASAPI *theAPI = [[FASAPI alloc] initWithMethod:FASAPIMethodLogin
                                           delegate:delegate];
    
    [theAPI.requiredParams addObject:FASAPIUsernameParam];
    [theAPI.requiredParams addObject:FASAPIPasswordParam];
    [theAPI.requiredParams addObject:FASAPIAuthTypeParam];
    [theAPI.requiredParams addObject:FASAPIClientNonceParam];
    
    [theAPI.params setValue:username forKey:FASAPIUsernameParam];
    [theAPI.params setValue:password forKey:FASAPIPasswordParam];
    [theAPI.params setValue:@"UB" forKey:FASAPIAuthTypeParam];
    [theAPI.params setValue:clientNonce forKey:FASAPIClientNonceParam];
    
    [theAPI setURL:FASAPILoginURL secure:FASEnvironment.APISecure];
    
    return theAPI;
}

+ (FASAPI *)loginWithFacebookToken:(NSString *)facebookToken
                          delegate:(id <FASAPIDelegate>)delegate
{
    FASAPI *theAPI = [[FASAPI alloc] initWithMethod:FASAPIMethodLoginFacebook
                                           delegate:delegate];

    [theAPI.requiredParams addObject:FASAPIFBTokenParam];
    
    [theAPI.params setValue:facebookToken forKey:FASAPIFBTokenParam];
    
    [theAPI setURL:FASAPIFacebookLoginURL secure:FASEnvironment.APISecure];
    
    return theAPI;
}

+ (FASAPI *)registerAccount:(NSString *)username
                   password:(NSString *)password
       passwordConfirmation:(NSString *)passwordConfirmation
                   delegate:(id <FASAPIDelegate>)delegate
{
    FASAPI *theAPI = [[FASAPI alloc] initWithMethod:FASAPIMethodRegister
                                           delegate:delegate];
    
    [theAPI.requiredParams addObject:FASAPIUsernameParam];
    [theAPI.requiredParams addObject:FASAPIPasswordParam];
    
    [theAPI.params setObject:username forKey:FASAPIUsernameParam];
    [theAPI.params setObject:password forKey:FASAPIPasswordParam];
    
    [theAPI setURL:FASAPIRegisterAccountURL secure:FASEnvironment.APISecure];
    
    return theAPI;
}

+ (FASAPI *)forgotPassword:(NSString *)username
                  delegate:(id <FASAPIDelegate>)delegate
{
    FASAPI *theAPI = [[FASAPI alloc] initWithMethod:FASAPIMethodForgotPassword delegate:delegate];
    
    [theAPI.requiredParams addObject:FASAPIUsernameParam];
    
    [theAPI setURL:FASAPIForgotPasswordURL secure:FASEnvironment.APISecure];
    
    return theAPI;
}

+ (FASAPI *)logout:(id <FASAPIDelegate>)delegate
{
    FASAPI *theAPI = [[FASAPI alloc] initWithMethod:FASAPIMethodLogout delegate:delegate];
    theAPI.tokenRequired = YES;
    [theAPI setURL:FASAPILogoutURL secure:FASEnvironment.APISecure];
    return theAPI;
}

+ (FASAPI *)userInfo:(id <FASAPIDelegate>)delegate
{
    FASAPI *theAPI = [[FASAPI alloc] initWithMethod:FASAPIMethodUserInfo
                                         delegate:delegate];
    theAPI.tokenRequired = YES;
    theAPI.HTTPMethod = FASAPIHTTPMethodPost;
    
    [theAPI setURL:FASAPIUserInfoURL secure:FASEnvironment.APISecure];
    
    return theAPI;
}
///////////////////////////////////////////////
#pragma mark - RunList
///////////////////////////////////////////////

// test run list API constructor
+ (FASAPI *)testRunList:(id <FASAPIDelegate>)delegate
{
    FASAPI *theAPI = [[FASAPI alloc] initWithMethod:FASAPIMethodTestRunList
                                           delegate:delegate];
    theAPI.tokenRequired = YES;
    theAPI.HTTPMethod = FASAPIHTTPMethodPost;
    theAPI.body = @"[{\"entity\":\"fas_exercise_attach\",\"method\":\"select\",\"fieldList\":[\"ID\",\"exerciseID\",\"lang\",\"type\",\"file\"],\"whereList\":{\"AttachmentsForWorkoutByExerciseIDs\":{\"values\":{\"exerciseID\":[\"3000000001001\",\"3000000002328\"]},\"expression\":\"[exerciseID]\",\"condition\":\"in\"}}}]";
    
    [theAPI setURL:FASAPIRunListURL secure:FASEnvironment.APISecure];
    
    return theAPI;
}

// common constructor for all Run List API requests
+ (FASAPI*)APIWithRunList:(UBIRunList*)runList
                   method:(FASAPIMethod)method
                 delegate:(id <FASAPIDelegate>)delegate
{
    FASAPI *theAPI = [[FASAPI alloc] initWithMethod:method
                                           delegate:delegate];
    theAPI.tokenRequired = YES;
    theAPI.HTTPMethod = FASAPIHTTPMethodPost;
    theAPI.body = [runList stringValue];
    [theAPI setURL:FASAPIRunListURL secure:FASEnvironment.APISecure];
    
    return theAPI;
}

+ (FASAPI *)getWorkoutsWithDelegate:(id <FASAPIDelegate>)delegate
{
    FASAPI *theAPI = [[FASAPI alloc] initWithMethod:FASAPIMethodGetWorkouts
                                           delegate:delegate];
    theAPI.tokenRequired = YES;
    
    [theAPI.requiredParams addObject:FASAPIVersionKey];
    
    [theAPI.params setValue:FASAPIVersionValue forKey:FASAPIVersionKey];
    
    [theAPI setURL:FASAPIGetWorkoutURL secure:FASEnvironment.APISecure];
    
    return theAPI;
}

+ (FASAPI *)getWorkoutsModifyDateWithRunList:(UBIRunList *)workoutRunList
                                    delegate:(id <FASAPIDelegate>)delegate
{
    return [FASAPI APIWithRunList:workoutRunList
                           method:FASAPIMethodGetWorkoutsModifyDate
                         delegate:delegate];
}

+ (FASAPI *)postWorkoutHistory:(NSDictionary *)workoutSessionParams
                   delegate:(id <FASAPIDelegate>)delegate
{
    FASAPI *theAPI = [[FASAPI alloc] initWithMethod:FASAPIMethodPostWorkoutHistory
                                           delegate:delegate];
    theAPI.tokenRequired = YES;
    theAPI.HTTPMethod = FASAPIHTTPMethodPost;
    
    theAPI.body = [NSString stringWithFormat:
                   @"[{\"entity\":\"fas_user_workout_history\","
                   "\"method\":\"insert\","
                   "\"execParams\":{"
                   "\"workoutID\":\"%@\","
                   "\"userID\":\"%@\","
                   "\"rate\":\"%@\","
                   "\"isComplete\":\"%@\","
                   "\"startDate\":\"%@\","
                   "\"endDate\":\"%@\","
                   "\"pauseDuration\":\"%@\""
                   "}}]",
                   workoutSessionParams[FASAPIHistoryWorkoutIDKey],
                   [[FASAPISession currentSession].userInfo stringUserID],
                   workoutSessionParams[FASAPIHistoryRateKey],
                   workoutSessionParams[FASAPIHistoryCompletedKey],
                   workoutSessionParams[FASAPIHistoryStartDateKey],
                   workoutSessionParams[FASAPIHistoryEndDateKey],
                   workoutSessionParams[FASAPIHistoryPauseDurationKey]];
    
    [theAPI setURL:FASAPIRunListURL secure:FASEnvironment.APISecure];
    
    return theAPI;
}

+ (FASAPI *)postExerciseHistory:(NSDictionary *)sessionDict
                exerciseHistory:(NSArray *)exerciseHistoryArray
                      delegate:(id <FASAPIDelegate>)delegate
{
    FASAPI *theAPI = [[FASAPI alloc] initWithMethod:FASAPIMethodPostExerciseHistory
                                           delegate:delegate];
    theAPI.tokenRequired = YES;
    
    theAPI.HTTPMethod = FASAPIHTTPMethodPost;

    NSMutableString *postBody = [NSMutableString new];
    for (NSDictionary* exerciseHistoryElement in exerciseHistoryArray)
    {
        [postBody appendFormat:@"{\"entity\":\"fas_user_exercise_history\","
         "\"method\":\"insert\","
         "\"execParams\":{"
         "\"workoutHistoryID\":\"%@\","
         "\"exerciseID\":\"%@\","
         "\"cycle\":\"%@\","
         "\"repeatCount\":\"%@\""
         "}}",
         sessionDict[FASAPIHistoryIDKey],
         exerciseHistoryElement[FASAPIHistoryExerciseIDKey],
         exerciseHistoryElement[FASAPIHistoryExerciseCycleKey],
         exerciseHistoryElement[FASAPIHistoryExerciseRepeatCountKey]];
        if ([exerciseHistoryArray lastObject] != exerciseHistoryElement)
        {
            [postBody appendString:@","];
        }
    }
    
    [postBody appendString:@"]"];
    theAPI.body = postBody;
    [theAPI setURL:FASAPIRunListURL secure:FASEnvironment.APISecure];
    return theAPI;
}

+ (FASAPI *)getBestExerciseResultsForWorkoutID:(NSNumber *)workoutID
                                      delegate:(id <FASAPIDelegate>)delegate
{
    FASAPI *theAPI = [[FASAPI alloc] initWithMethod:FASAPIMethodGetBestResults delegate:delegate];
    theAPI.HTTPMethod = FASAPIHTTPMethodGet;
    theAPI.tokenRequired = YES;
    [theAPI.params setObject:workoutID forKey:@"workoutID"];
    [theAPI setURL:FASAPIGetBestResultsURL secure:FASEnvironment.APISecure];
    return theAPI;
}

+ (FASAPI *)getWorkoutHistoryWithRunList:(UBIRunList *)workoutHistoryRunList
                          delegate:(id <FASAPIDelegate>)delegate
{
    return [FASAPI APIWithRunList:workoutHistoryRunList
                           method:FASAPIMethodGetWorkoutHistoryWithOptions
                         delegate:delegate];
}

+ (FASAPI *)getFAQWithRunList:(UBIRunList *)faqRunList
                     delegate:(id <FASAPIDelegate>)delegate
{
    return [FASAPI APIWithRunList:faqRunList
                           method:FASAPIMethodGetFaqEntries
                         delegate:delegate];
}

+ (FASAPI *)getExercisesWithRunList:(UBIRunList *)exercisesRunList
                          delegate:(id <FASAPIDelegate>)delegate
{
    return [FASAPI APIWithRunList:exercisesRunList
                           method:FASAPIMethodGetExercisesWithOptions
                         delegate:delegate];
}

+ (FASAPI *)getAttachmentsWithRunList:(UBIRunList *)attachmentsRunList
                             delegate:(id <FASAPIDelegate>)delegate
{
    return [FASAPI APIWithRunList:attachmentsRunList
                           method:FASAPIMethodGetAttachmentsWithOptions
                         delegate:delegate];
}

+ (FASAPI *)getAttachmentsForExercisesWithRunList:(UBIRunList *)attachmentsRunList
                                         delegate:(id <FASAPIDelegate>)delegate
{
    return [FASAPI APIWithRunList:attachmentsRunList
                           method:FASAPIMethodGetAttachmentsForExercisesWithOptions
                         delegate:delegate];
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark In-App Purchase
#pragma mark -
///////////////////////////////////////////////
+ (FASAPI *)getStoreItemsWithDelegate:(id<FASAPIDelegate>)delegate
{
    FASAPI *theAPI = [[FASAPI alloc] initWithMethod:FASAPIMethodGetStoreItems
                                           delegate:delegate];
    theAPI.tokenRequired = YES;
    theAPI.HTTPMethod = FASAPIHTTPMethodGet;
    [theAPI.requiredParams addObject:FASAPIVersionKey];
    [theAPI.params setValue:FASAPIVersionValue forKey:FASAPIVersionKey];
    [theAPI setURL:FASAPIGetStoreItemsURL secure:FASEnvironment.APISecure];
    return theAPI;
}

+ (FASAPI *)addUserPurchase:(NSString *)productID
                    receipt:(NSString *)encodedReceipt
                   restored:(BOOL)isRestored
                   delegate:(id <FASAPIDelegate>)delegate
{
    FASAPI *theAPI = [[FASAPI alloc] initWithMethod:FASAPIMethodAddUserPurchase
                                           delegate:delegate];
    theAPI.tokenRequired = YES;
    theAPI.HTTPMethod = FASAPIHTTPMethodPost;
    NSString* paymentMethod = (isRestored ? @"restore" : @"purchase");
    theAPI.body = [NSString stringWithFormat:
    @"[{\"entity\":\"fas_user_purchase\","
    "\"method\":\"addUserPurchase\","
    "\"execParams\":{"
    "\"inAppProductID\":\"%@\","
    "\"paymentMethod\":\"%@\","
    "\"receipt\":\"%@\""
    "}}]",
    productID,
    paymentMethod,
    encodedReceipt];
        
    [theAPI setURL:FASAPIRunListURL secure:FASEnvironment.APISecure];
    
    return theAPI;
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark APNS
#pragma mark -
///////////////////////////////////////////////
+ (FASAPI *)registerDeviceToken:(NSString *)deviceToken
                      delegate:(id <FASAPIDelegate>)delegate
{
    FASAPI *theAPI = [[FASAPI alloc] initWithMethod:FASAPIMethodRegisterDeviceToken
                                           delegate:delegate];
    theAPI.tokenRequired = YES;
    theAPI.HTTPMethod = FASAPIHTTPMethodPost;
    
    // for simplicity, determine environment here
    NSString* environment = nil;
#ifdef DEBUG
    environment = kFASAPIDeviceTokenEnvironmentDevelopment;
#else
    environment = kFASAPIDeviceTokenEnvironmentProduction;
#endif
    
    if ([[FASAPISession currentSession].userInfo isValidUserID]) {
        theAPI.body = [NSString stringWithFormat:
                       @"[{\"entity\":\"fas_user_push_token\","
                       "\"method\":\"insert\","
                       "\"execParams\":{"
                       "\"userID\":\"%@\","
                       "\"pushToken\":\"%@\","
                       "\"environment\":\"%@\"}}]",
                       [[FASAPISession currentSession].userInfo stringUserID],
                       deviceToken,
                       environment];
    }
    
    [theAPI setURL:FASAPIRunListURL secure:FASEnvironment.APISecure];
    
    return theAPI;
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Update User Info
#pragma mark -
///////////////////////////////////////////////
+ (FASAPI *)setUserInfoKey:(NSString*)key
                     value:(id)value
                  delegate:(id <FASAPIDelegate>)delegate
{
    FASAPI *theAPI = [[FASAPI alloc] initWithMethod:FASAPIMethodSetUserInfo
                                           delegate:delegate];
    theAPI.tokenRequired = YES;
    theAPI.HTTPMethod = FASAPIHTTPMethodGet;
    
    if ([value isKindOfClass:[NSNumber class]]) {
        [theAPI.params setObject:[(NSNumber *)value stringValue] forKey:key];
    }
    else {
        [theAPI.params setObject:value forKey:key];
    }
    
    [theAPI setURL:FASAPISetUserInfoURL secure:FASEnvironment.APISecure];
    
    return theAPI;
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Init/dealloc
#pragma mark -
///////////////////////////////////////////////
- (instancetype)init {
    self = [super init];
    if (self) {
        _params = [NSMutableDictionary dictionary];
        _requiredParams = [NSMutableArray array];
        _delegate = nil;
        _url = nil;
        _HTTPMethod = FASAPIHTTPMethodGet;
        _rawResponse = nil;
        _response = nil;
        _errors = [NSMutableArray array];
        _tokenRequired = NO;
        _waitingForSession = NO;
        _tokenRequired = NO;
        _invokeAsBackgroundTask = UIDevice.currentDevice.multitaskingSupported;
        _cancelled = NO;
        _urlSession = nil;
        _allowsCellularAccess = YES;
        _showNetworkActivity = YES;
        _statusCode = 0;
        _responseData = nil;
        _body = nil;
        _UIDelegate = nil;
        //TODO: need a way to retain self, so it is not deallocated
        [FASAPISession.currentSession addObserver:self
                                       forKeyPath:FASAPITokenKey
                                          options:NSKeyValueObservingOptionNew
                                          context:&kToken];
    }
    return self;
}

- (void)dealloc
{
    [FASAPISession.currentSession removeObserver:self];
    [FASAPISession.currentSession removeObserver:self
                                      forKeyPath:FASAPITokenKey
                                         context:&kToken];
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Control Action
#pragma mark -
///////////////////////////////////////////////
- (void)invokeWithUIDelegate:(id <FASAPIUIDelegate>)UIDelegate
{
    self.UIDelegate = UIDelegate;
    [self invoke];
}

- (void)invoke
{
    // do nothing if API has been cancelled
    if (self.cancelled) return;
    
    UBILogTrace(@"%@ - invoke", self);
    
    // clear state before validation
    self.rawResponse = nil;
    self.response = nil;
    [self.errors removeAllObjects];
    
    // validate request even before param checking
    if (![self hasValidPreconditions]) {
        return;
    }
    
    // validate required params
    if (![self hasValidParams]) {
        return;
    }
    
    // do nothing if token validation fails
    if (![self hasValidToken]) {
        return;
    }
    
    [self performRequest];
}

- (void)cancel
{
    UBILogTrace(@"%@ - cancel", self)
    self.delegate = nil;
    self.UIDelegate = nil;
    self.waitingForSession = NO;
    self.hasActiveRequest = NO;
    self.cancelled = YES;
    [self.urlSession invalidateAndCancel];
    self.responseData = nil;
}

// use to simulate an API call w/o initiating the request
- (void)simulate
{
    [self simulateWithError:nil];
}

- (void)simulateFailure
{
    FASAPIError* error = [FASAPIError errorWithCode:FASAPIErrorCodeCustom
                                              value:@"simulated error"];
    [self simulateWithError:error];
}

- (void)simulateWithError:(NSError *)error
{
    UBILogTrace(@"%@", self)
    if ([self.UIDelegate respondsToSelector:@selector(FASAPIUIwillInvoke:)]) {
        [self.UIDelegate FASAPIUIwillInvoke:self];
    }
    if ([self.delegate respondsToSelector:@selector(FASAPIwillInvoke:)]) {
        [self.delegate FASAPIwillInvoke:self];
    }
    [NSNotificationCenter.defaultCenter
     postNotificationName:FASAPIWillInvokeNotification
     object:self
     userInfo:nil];
    
    self.hasActiveRequest = YES;
    self.invoked = YES;
    self.response = nil;
    [self.errors removeAllObjects];
    
    if (error) {
        [self.errors addObject:error];
    }
    
    self.hasActiveRequest = NO;
    [self finish];
}
///////////////////////////////////////////////
#pragma mark - 
#pragma mark Custom Accessors
#pragma mark -
///////////////////////////////////////////////
- (void)setWaitingForSession:(BOOL)value
{
    _waitingForSession = value;
    if (_waitingForSession) {
        [FASAPISession.currentSession addObserver:self];
    }
    else {
        [FASAPISession.currentSession removeObserver:self];
    }
}

- (void)setShowNetworkActivity:(BOOL)value
{
    if (_showNetworkActivity == value) return;
    
    _showNetworkActivity = value;
    if (self.hasActiveRequest) {
        if (_showNetworkActivity) {
            [FASData.sharedData startNetworkActivity];
        }
        else {
            [FASData.sharedData stopNetworkActivity];
        }
    }
}

- (void)setHasActiveRequest:(BOOL)value
{
    if (_hasActiveRequest == value) return;
    _hasActiveRequest = value;
    
    if (_showNetworkActivity) {
        if (_hasActiveRequest) {
            [FASData.sharedData startNetworkActivity];
        }
        else {
            [FASData.sharedData stopNetworkActivity];
        }
    }
}

- (void)setInvoked:(BOOL)invoked
{
    if (_invoked != invoked) {
        _invoked = invoked;
        
        if (_invoked) {
            if ([self.UIDelegate respondsToSelector:@selector(FASAPIUIdidInvoke:)]){
                [self.UIDelegate FASAPIUIdidInvoke:self];
            }
            if ([self.delegate respondsToSelector:@selector(FASAPIdidInvoke:)]){
                [self.delegate FASAPIdidInvoke:self];
            }
            [NSNotificationCenter.defaultCenter
             postNotificationName:FASAPIDidInvokeNotification
             object:self
             userInfo:nil];
        }
    }
}

- (void)setStatusCode:(NSUInteger)value
{
    if (_statusCode == value) return;
    _statusCode = value;
    
    if (_statusCode >= 200 && _statusCode < 300) {
        // success code
    }
    
    else {
        FASAPIError* error = nil;
        
        if (_statusCode == 401) { // Not Authorized
            error = [FASAPIError errorWithCode:FASAPIErrorCodeTokenInvalid
                                         value:@"Token expired"];
        }
        
        else if (self.response) {
            error = [FASAPIError errorWithInfo:self.response];
        }
        
        else {
            error = [FASAPIError errorWithCode:FASAPIErrorCodeCustom
                                         value:[NSString stringWithFormat:@"HTTP status code: %li", (long)_statusCode]];
        }
        
        if (error) {
            [self.errors addObject:error];
        }
    }
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark FASAPISessionObserver protocol
#pragma mark -
///////////////////////////////////////////////
- (void)FASAPISessionNotification:(NSNotification *)notification
{
    // do nothing if not waiting for a session
    if (!self.waitingForSession) return;
    
    UBILogTrace(@"%@: FASAPISessionNotification: %@", self, notification.name);
    if (notification.name == FASAPISessionDidLoginNotification) {
        // we have a session, peform request again
        self.waitingForSession = NO;
        [self invoke];
    }
    
    else if (notification.name == FASAPISessionDidNotLoginNotification) {
        // login failed, let UIDelegate decide what to do
        // TODO: notify UIDelegate that this API needs to know whether to continue waiting for a session or to do finish (i.e. fail)
        [self.errors
         addObject:[FASAPIError
                    errorWithCode:FASAPIErrorCodeTokenRequired
                    value:nil]];
        [self finish];
    }
    
    else if (notification.name == FASAPISessionDidCancelLoginNotification) {
        // user did not login, fail with error
        [self.errors
         addObject:[FASAPIError
                    errorWithCode:FASAPIErrorCodeTokenRequired
                    value:nil]];
        [self finish];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if (context == &kToken) {
        // TODO: only clear token if request has not started
        if (!self.hasActiveRequest && self.tokenRequired) {
            
            //TODO: track this event w/ GAI
            
            UBILogWarning(@"%@: Token Did Change %@",
                    self,
                    FASAPISession.currentSession.token);
            
            // change Authorisation header upon token change notification
            [self.request setValue:FASAPISession.currentSession.token forHTTPHeaderField:@"Authorization"];
        }
    }
    
    else {
        [super observeValueForKeyPath:keyPath
                             ofObject:object
                               change:change
                              context:context];
    }
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark NSObject override
#pragma mark -
///////////////////////////////////////////////
- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ (%@)",
            [super description],
            [self localizedDescriptionStringForMethod:_method]];
}

- (NSString *)localizedDescriptionStringForMethod:(FASAPIMethod)method
{
    NSString* key = [NSString stringWithFormat:@"FASAPIMethod_%lu", (unsigned long)method];
    return NSLocalizedStringFromTable(key,
                                      @"FASAPI",
                                      nil);
}

@end

///////////////////////////////////////////////
// hiden methods implementation
///////////////////////////////////////////////

@implementation FASAPI (hidden)

- (instancetype)initWithMethod:(FASAPIMethod)method delegate:(id <FASAPIDelegate>)delegate
{
    self = [self init];
    if (self) {
        _method = method;
        self.delegate = delegate;
    }
    return self;
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Checking
#pragma mark -
///////////////////////////////////////////////
- (BOOL)hasValidPreconditions
{
    BOOL isValid = YES;
    FASAPIError *apiError = nil;
    
    // first check API level preconditions
    switch (self.method) {
        default: break;
    }
    
    // next check delegate preconditions
    if (isValid) {
        if ([self.delegate
             respondsToSelector:@selector(validatePreconditionsForFASAPI:apiError:)])
        {
            isValid = [_delegate validatePreconditionsForFASAPI:self
                                                       apiError:&apiError];
        }
    }
    
    // if preconditions are invalid, fail with error
    if (!isValid) {
        if (apiError) {
            [self.errors addObject:apiError];
        }
        // notify UIDelegate
        if (self.UIDelegate && [self.UIDelegate respondsToSelector:@selector(FASAPIUIhasInvalidPreconditions:)]) {
            [self.UIDelegate FASAPIUIhasInvalidPreconditions:self];
        }
        
        [self finish];
    }
    
    return isValid;
}

// validates presence of token and handles login if missing
- (BOOL)hasValidToken
{
    BOOL validated = YES;
    
    if (self.tokenRequired) {
        
        if ([FASAPISession.currentSession isLoggedIn]) {
            UBILogTrace(@"%@: token = %@",
                  self,
                  [FASAPISession.currentSession obfuscatedToken]);
        }
        
        else {
            UBILogError(@"%@: FAILED", self);
            validated = NO;
            
            self.waitingForSession = NO;
            
            // first ask UIDelegate if FASAPI should wait for a session
            // if UIDelegate responds YES, UIDelegate is responsible for creating a session or
            // cancelling the theAPI. UIDelegate may present login UI
            if (self.UIDelegate && [self.UIDelegate respondsToSelector:@selector(FASAPIUIshouldWaitForSession:)]) {
                UBILogTrace(@"%@:ASK UIDELEGATE IF SHOULD WAIT FOR SESSION", self);
                self.waitingForSession = [self.UIDelegate FASAPIUIshouldWaitForSession:self];
            }
            
            // if not waiting for a session, add error and fail
            if (!self.waitingForSession) {
                UBILogError(@"%@:NO SESSION, WILL FAIL", self);
                [self.errors
                 addObject:[FASAPIError
                            errorWithCode:FASAPIErrorCodeTokenRequired
                            value:nil]];
                [self finish];
            }
        }
    }
    
    return validated;
}

- (BOOL)hasValidParams
{
    NSString *paramValue;
    for (NSString *paramKey in self.requiredParams) {
        paramValue = [self.params objectForKey:paramKey];
        if (!paramValue || [paramValue isBlank]) {
            if (paramKey != FASAPITokenKey) {
                [self.errors
                 addObject:[FASAPIError
                            errorWithCode:FASAPIErrorCodeParamRequired
                            value:paramKey]];
            }
        }
    }
    
    // notify delegate if validation failed
    if (self.errors.count > 0) {
        UBILogError(@"%@:FAILED %@", self, self.errors);
        [self finish];
        return NO;
    }
    
    return YES;
}

//// Synopsys:
// We might consider some errors as being fatal. That code(s) should be here and return YES.
- (BOOL)fatalError
{
    return NO;
}

//// Synopsis:
// The response is received and parsed, means it is not empty
// Check for specifyc key/value pairs the client needs are there.
// IF some key/value does not exist, create an error object and add it to 'self.errors'
- (void)validateResponse
{
    switch (self.method) {
        case FASAPIMethodLoginStart: {
            if (self.response[FASAPIResultKey] == nil) {
                [self.errors
                 addObject:[FASAPIError
                            errorWithCode:FASAPIErrorCodeInvalidResponse
                            value:@"One or more keys missed from response"]];
            }
        } break;
            
        case FASAPIMethodLogin: {
            if (self.response[FASAPIResultKey] == nil
                || self.response[FASAPILogonnameKey] == nil
                || self.response[FASAPIUserDataKey] == nil) {
                [self.errors
                 addObject:[FASAPIError
                            errorWithCode:FASAPIErrorCodeInvalidResponse
                            value:@"One or more keys missed from response"]];
            }
        } break;
            
        default: break;
    }
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark NSURL Constructors
#pragma mark -
///////////////////////////////////////////////

// Return the params dictionary as a string intended for use in a URL
// TODO: better implementation would be to use [array componentsJoinedByString] where
// elements of the array are a custom class encapsulating a single param
// TODO: implementation assumes param string should always begin with '&'
- (NSString *)paramString
{
    NSMutableString *paramString = [NSMutableString string];
    for (id param in _params) {
        NSString *value = [[self.params objectForKey:param] description];
        if ([paramString isBlank]) {
            [paramString appendFormat:@"%@=%@",
#if SERVER_USES_URL_ENCODING
             [param URLEncodedString],
             [value URLEncodedString]];
#else
            param,
            value];
#endif
        }
        else {
            [paramString appendFormat:@"&%@=%@",
#if SERVER_USES_URL_ENCODING
             [param URLEncodedString],
             [value URLEncodedString]];
#else
            param,
            value];
#endif
        }
    }
    return paramString;
}

- (void)setURL:(NSString *)URLString secure:(BOOL)secure
{
    _url = [self stringURLWithPath:URLString secure:secure];
}

- (NSString *)stringURLWithPath:(NSString *)path secure:(BOOL)secure
{
    NSString *HTTPBase = secure ? @"https" : @"http";
    
    NSString *HTTPAuthUser = FASEnvironment.HTTPAuthUser;
    NSString *HTTPAuthPassword = FASEnvironment.HTTPAuthPassword;
    
    if ((HTTPAuthUser && ![HTTPAuthUser isBlank]) &&
        (HTTPAuthPassword && ![HTTPAuthPassword isBlank]) ) {
        return [NSString stringWithFormat:@"%@://%@:%@@%@/%@",
                HTTPBase,
                HTTPAuthUser,
                HTTPAuthPassword,
                FASEnvironment.APIBasePath,
                path];
    }
    else {
        return [NSString stringWithFormat:@"%@://%@/%@",
                HTTPBase,
                FASEnvironment.APIBasePath,
                path];
    }
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark NSURLSession Constructors
#pragma mark -
///////////////////////////////////////////////
- (NSURLSession *)ephemeralSession
{
    NSURLSessionConfiguration *ephemeralConfigObject = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    ephemeralConfigObject.allowsCellularAccess = self.allowsCellularAccess;
    ephemeralConfigObject.timeoutIntervalForRequest = kTimeoutIntervalForRequest;
    
    return [NSURLSession sessionWithConfiguration:ephemeralConfigObject
                                         delegate:self
                                    delegateQueue:[NSOperationQueue mainQueue]];
}

- (NSURLSession *)defaultSession
{
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSString *cachePath = @"/FASCacheDirectory";
    NSArray *aPathList = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *aPath    = [aPathList  objectAtIndex:0];
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    NSString *fullCachePath = [[aPath stringByAppendingPathComponent:bundleIdentifier] stringByAppendingPathComponent:cachePath];
    UBILogTrace(@"Cache path: %@\n", fullCachePath);
    
    NSURLCache *apiCache = [[NSURLCache alloc] initWithMemoryCapacity: 16384 diskCapacity: 268435456 diskPath: cachePath];
    defaultConfigObject.URLCache = apiCache;
    defaultConfigObject.requestCachePolicy = NSURLRequestUseProtocolCachePolicy;
    
    defaultConfigObject.allowsCellularAccess = self.allowsCellularAccess;
    
    return [NSURLSession sessionWithConfiguration:defaultConfigObject
                                         delegate:self
                                    delegateQueue:[NSOperationQueue mainQueue]];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Request
#pragma mark -
///////////////////////////////////////////////

- (NSMutableURLRequest *)request
{
    NSMutableURLRequest *request = [NSMutableURLRequest new];
    request.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    request.HTTPShouldUsePipelining = YES;
    
    // For NSURLSession, timeouts are configured on a per-session basis. Thus, commented.
//#ifdef DEBUG
//    request.timeoutInterval = 1200;
//#else
//    request.timeoutInterval = 30;
//#endif
    //[request setValue:[NSString stringWithFormat:@"StarSightings iPhone %@", SSData.sharedData.appVersion] forHTTPHeaderField:@"User-Agent"];
    
    if (self.tokenRequired) {
        // We have checked for valid token before in hasValidToken method
        [request setValue:FASAPISession.currentSession.token forHTTPHeaderField:@"Authorization"];
    }
    
    [request setValue:@"gzip,deflate" forHTTPHeaderField:@"Accept-Encoding"];
    [request setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%@", [[NSLocale preferredLanguages] componentsJoinedByString:@", "]] forHTTPHeaderField:@"Accept-Language"];
    
    // if post
    if (self.HTTPMethod == FASAPIHTTPMethodPost) {

        [request setHTTPMethod:FASAPIHTTPMethodPost];
        
        // set Content-Type in HTTP header
//        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", FASAPIBoundary];
//        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        // post body
        NSMutableData *body = [NSMutableData data];
        
        if (self.body) {
            [body appendData:[self.body dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
//        NSMutableString *bodyPrefix = [NSMutableString string];
//        NSMutableString *bodySuffix = [NSMutableString string];
        
        /*****************************************
        // add params
        for (NSString *param in self.params) {
            @autoreleasepool {
                // if string param
                if ([param isKindOfClass:[NSString class]]) {
                    [bodyPrefix appendString:[NSString stringWithFormat:@"%@", [_params objectForKey:param]]];
//                    [bodyPrefix appendString:[NSString stringWithFormat:@"--%@\r\n", FASAPIBoundary]];
//                    [bodyPrefix appendString:[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param]];
//                    [bodyPrefix appendString:[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]]];
                }
                
                // else if data param
                else if ([param isKindOfClass:[NSData class]]) {
//                    [bodyPrefix appendString:[NSString stringWithFormat:@"--%@\r\n", FASAPIBoundary]];
//                    [bodyPrefix appendString:[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param]];
//                    [bodyPrefix appendString:[_params objectForKey:param]];
//                    [bodyPrefix appendString:[NSString stringWithFormat:@"\r\n"]];
                }
            }
        }
        
        [body appendData:[bodyPrefix dataUsingEncoding:NSUTF8StringEncoding]];
//        [bodySuffix appendString:[NSString stringWithFormat:@"--%@--\r\n", FASAPIBoundary]];
//        [body appendData:[bodySuffix dataUsingEncoding:NSUTF8StringEncoding]];
        *****************************************/
        
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // create URL
        [request setURL:[NSURL URLWithString:self.url]];
    }
    
    // else HTTP GET, add params to URL
    else {
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", self.url, [self paramString]]]];
    }
    return request;
}

- (void)performRequest
{
    // only send FASAPIwillInvoke if the API has not yet invoked
    if (!self.invoked) {
        if ([self.UIDelegate respondsToSelector:@selector(FASAPIUIwillInvoke:)]) {
            [self.UIDelegate FASAPIUIwillInvoke:self];
        }
        if ([self.delegate respondsToSelector:@selector(FASAPIwillInvoke:)]) {
            [self.delegate FASAPIwillInvoke:self];
        }
        [NSNotificationCenter.defaultCenter
         postNotificationName:FASAPIWillInvokeNotification
         object:self
         userInfo:nil];
    }
    
    self.invoked = YES;
    self.UIDelegate = nil;
    
    UIApplication *app = UIApplication.sharedApplication;
    if (_invokeAsBackgroundTask) {
        _bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
            UBILogWarning(@"%@:EXPIRATION HANDLER", self);
            [self cancel];
            [self processError:[FASAPIError errorWithCode:FASAPIErrorCodeCustom
                                                                value:@"Background expiration handler was called"]];
            [self finish];
            [app endBackgroundTask:_bgTask];
            _bgTask = UIBackgroundTaskInvalid;
        }];
    }
    
    // Synopsys:
    // if we need some specifyc request setup (e.g. background downloading or uploading a file)
    // this set up could be done here. Of course w/ self.method switch case included.
    // Also, don't forget to set self.hasActiveRequest = YES, so the below code won't be executed.
    
    if (!self.hasActiveRequest) {
        self.hasActiveRequest = YES;
        
        NSURLRequest *request = [self request];
        UBILogTrace(@"%@ - performRequest:URL: %@", self, request.URL);
        // TODO: add check for already existed session (the case might happen when re-login is attempted, so the session is still existed)
        self.urlSession = [self ephemeralSession];
        NSURLSessionDataTask *dataTask = [self.urlSession dataTaskWithRequest:request];
        [dataTask resume];

        /*
         FASAPI *strongSelf = self;
         _asyncConnection.progress = ^(CGFloat percentComplete) {
         if ([strongSelf.delegate respondsToSelector:@selector(FASAPI:didSendData:)]) {
         [strongSelf.delegate FASAPI:strongSelf didSendData:percentComplete];
         }
         };
         */
    }
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark NSURLSessionDelegate
#pragma mark -
///////////////////////////////////////////////
- (void)URLSession:(NSURLSession *)session didBecomeInvalidWithError:(NSError *)error
{
    if (error) {
        UBILogError(@"URLSession invalidation error. Error was:\n%@", [error detailedInfo]);
    }
    
    if ([session isEqual:_urlSession]) {
        _urlSession = nil;
    }
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark NSURLSessionDataDelegate
#pragma mark -
///////////////////////////////////////////////
// SO answer: http://stackoverflow.com/a/23738200/1492173
- (void)URLSession:(NSURLSession *)session
          dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveData:(NSData *)data
{
    // TODO: uncomment when debugging this part
    
//    UBILogTrace(@"%@",
//          [NSString stringWithFormat:
//           @"\n>>>>>>>>>>>>>DID RECEIVE DATA (lenght = %lu)>>>>>>>>>>>>>"
//           @"\n\t> TASK ID:      %lu"
//           @"\n\t> URL:          %@"
//           @"\n\t> BODY:         %@"
//           @"\n\t> STATUS CODE:  %li"
//           @"\n\t> RESPONSE:     %@"
//           @"\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
//           (unsigned long)data.length,
//           (unsigned long)(unsigned long)dataTask.taskIdentifier,
//           dataTask.originalRequest.URL,
//           self.body,
//           (long)(long)[(NSHTTPURLResponse*)dataTask.response statusCode],
//           dataTask.response
//           ]);
    
    NSAssert(session == self.urlSession, @"[ASSERT]: invalid session object");
    
    if (!self.responseData) {
        _responseData = [NSMutableData dataWithData:data];
    } else {
        [self.responseData appendData:data];
    }
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark NSURLSessionTaskDelegate
#pragma mark -
///////////////////////////////////////////////
- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    UBILogTrace(@"URLSession:task:didCompleteWithError:");
    
    if (session != self.urlSession) {
        UBILogWarning(@"NSURLsession object in delegate does not match the one created with FASAPI class.\nDelegate session: %@\tObject's session: %@", session, self.urlSession);
    }
    
    self.hasActiveRequest = NO;
    
    if (error) {
        // HTTP level error
        [self processError:error];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        self.rawResponse = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
        
        NSError* serializationError = nil;
        if (![self isCancelled] && [self.responseData length] != 0) {
            self.response = [NSJSONSerialization JSONObjectWithData:self.responseData
                                                            options:kNilOptions
                                                              error:&serializationError];
        }
        
        UBILogTrace(@"%@",
              [NSString stringWithFormat:
               @"\n>>>>>>>>>>>>>>>>DID COMPLETE WITH ERROR>>>>>>>>>>>>>>>>"
               @"\n\t> TASK ID:          %lu"
               @"\n\t> URL:              %@"
               @"\n\t> HTTP AUTH HEADER: %@"
               @"\n\t> BODY:             %@"
               @"\n\t> STATUS CODE:      %li"
               @"\n\t> ERROR:            %@"
               @"\n\t> RESPONSE LENGTH:  %lu"
               @"\n\t> RESPONSE:         %@"
               @"\n\t> RAW RESPONSE:     %@"
               @"\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<",
               (unsigned long)task.taskIdentifier,
               task.originalRequest.URL,
               [task.originalRequest.allHTTPHeaderFields objectForKey:@"Authorization"],
               self.body,
               (long)[(NSHTTPURLResponse*)task.response statusCode],
               [error detailedInfo],
               (unsigned long)[self.responseData length],
               task.response,
               [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding]
               ]);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self.response) {
                [self validateResponse];
            }
            else if (serializationError) {
                UBILogError(@"Parsing response error:\n%@\nError was:\n%@", self.rawResponse, [serializationError detailedInfo]);
                [self processError:serializationError];
            }
            else {
                UBILogError(@"empty response");
            }
            
            self.responseData = nil;
            self.statusCode = [(NSHTTPURLResponse*)task.response statusCode];
            
            [self finish];
            
            if (self.bgTask != UIBackgroundTaskInvalid) {
                [UIApplication.sharedApplication endBackgroundTask:self.bgTask];
                self.bgTask = UIBackgroundTaskInvalid;
            }
        });
    });
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Process Error
#pragma mark -
///////////////////////////////////////////////
- (void)processError:(NSError *)error
{
    // special case NSURLErrorTimedOut
    if (error.code == NSURLErrorTimedOut ||
        error.code == NSURLErrorCannotFindHost ||
        error.code == NSURLErrorCannotConnectToHost)
    {
        [self.errors
         addObject:[FASAPIError
                    errorWithCode:FASAPIErrorCodeNSURLErrorTimedOut
                    value:nil]];
    }
    
    else {
        [self.errors
         addObject:[FASAPIError
                    errorWithCode:FASAPIErrorCodeCustom
                    value:[error detailedInfo]]];
    }
}

- (void)finish
{
    if (self.errors.count > 0) {
        // IF NOT already waiting for session (meaning a re-login has already been attempted)
        // AND have an invalid token
        // THEN attempt login and wait for session response
        if ( self.waitingForSession == NO &&
            [FASAPIError errors:_errors containsErrorWithCode:FASAPIErrorCodeTokenInvalid]) {
            
            // clear invalid token from session
            [FASAPISession.currentSession clearToken];
            
            // attempt autologin
            UBILogTrace(@"%@:ATTEMPT TO AUTO-LOGIN", self);
            
            self.waitingForSession = YES;
            [FASAPISession.currentSession login];
        }
        
        // else if other errors, API fails
        else if (![self fatalError]) {
            self.waitingForSession = NO;
            
            UBILogError(@"%@: API DID FAIL", self);
            
            if ([self.delegate respondsToSelector:@selector(FASAPIdidFail:)]) {
                [self.delegate FASAPIdidFail:self];
            }
            [NSNotificationCenter.defaultCenter
             postNotificationName:FASAPIDidFailNotification
             object:self
             userInfo:nil];
            
            // invalidate session to allow ARC to dealloc this instance
            [self.urlSession invalidateAndCancel];
        }
        
        // TODO: what is this case?
        else {
            NSAssert(NO, @"%@ FATAL ERROR", self);
            
            // invalidate session to allow ARC to dealloc this instance
            [self.urlSession invalidateAndCancel];
        }
    }
    
    // no errors, API succeeded
    else {
        if (!self.cancelled) {

            if ([self.delegate respondsToSelector:@selector(FASAPIdidFinish:)]) {
                [self.delegate FASAPIdidFinish:self];
            }
            
            [NSNotificationCenter.defaultCenter
             postNotificationName:FASAPIDidFinishNotification
             object:self
             userInfo:nil];
        } else {
            UBILogWarning(@"%@: API WAS CANCELED", self);
        }
        
        // invalidate session to allow ARC to dealloc this instance
        [self.urlSession invalidateAndCancel];
    }
}

@end