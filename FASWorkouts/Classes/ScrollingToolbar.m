//
//  ScrollingToolbar.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 2/21/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "ScrollingToolbar.h"
#import "FASToolbarButton.h"

#define SCROLLING_TOOLBAR_DEFAULT_BUTTON_PADDING    6
#define SCROLLING_TOOLBAR_MAX_BUTTON_WIDTH          160

@interface ScrollingToolbar ()
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIButton *leftArrow;
@property (nonatomic, strong) UIButton *rightArrow;
@end

@interface ScrollingToolbar (private)
- (UIButton *)buttonToLeftOfXPosition:(CGFloat)XPosition;
- (UIButton *)buttonToRightOfXPosition:(CGFloat)XPosition;
@end

@implementation ScrollingToolbar

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initState];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initState];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initState];
    }
    return self;
}

- (void)initState
{
    self.maxButtonSize = CGSizeMake(SCROLLING_TOOLBAR_MAX_BUTTON_WIDTH, 0);
    self.allowMultipleSelection = NO;
    self.selectedButton = nil;
    self.centerButtons = YES;
    
    // add left/right arrows displayed when buttons are out of view to left/right
    _leftArrow = [UIButton buttonWithType:UIButtonTypeCustom];
    [_leftArrow addTarget:self
                   action:@selector(scrollLeft)
         forControlEvents:UIControlEventTouchUpInside];
    [_leftArrow setImage:[UIImage imageNamed:@"toolbarArrowLeft"]
                forState:UIControlStateNormal];
    [_leftArrow sizeToFit];
    CGRect lArrowFrame = _leftArrow.frame;
    lArrowFrame.size = CGSizeMake(30.0f, 18.0f);
    lArrowFrame.origin.x = 0;
    lArrowFrame.origin.y = (CGRectGetHeight(self.frame) - CGRectGetHeight(lArrowFrame))/2;
    _leftArrow.frame = lArrowFrame;
    _leftArrow.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
    [self addSubview:_leftArrow];
    
    _rightArrow = [UIButton buttonWithType:UIButtonTypeCustom];
    [_rightArrow addTarget:self
                    action:@selector(scrollRight)
          forControlEvents:UIControlEventTouchUpInside];
    [_rightArrow setImage:[UIImage imageNamed:@"toolbarArrowRight"]
                 forState:UIControlStateNormal];
    [_rightArrow sizeToFit];
    
    CGRect rArrowFrame = _rightArrow.frame;
    rArrowFrame.size = CGSizeMake(30.0f, 18.0f);
    rArrowFrame.origin.x = CGRectGetWidth(self.frame) - CGRectGetWidth(rArrowFrame);
    rArrowFrame.origin.y = (CGRectGetHeight(self.frame) - CGRectGetHeight(rArrowFrame))/2;
    _rightArrow.frame = rArrowFrame;
    _rightArrow.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [self addSubview:_rightArrow];
    
    // add scrollView to contain buttons
    _scrollView = [[UIScrollView alloc]
                   initWithFrame:CGRectMake(CGRectGetMaxX(_leftArrow.frame),
                                            0,
                                            CGRectGetWidth(self.bounds) - CGRectGetWidth(_leftArrow.bounds) - CGRectGetWidth(_rightArrow.bounds),
                                            CGRectGetHeight(self.bounds))];
    _scrollView.backgroundColor = UIColor.clearColor;
    _scrollView.contentSize = CGSizeMake(0 ,CGRectGetHeight(self.bounds));
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.canCancelContentTouches = YES;
    _scrollView.delegate = self;
    _scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self addSubview:_scrollView];
    
    _buttons = [NSMutableArray new];
    
    _buttonPadding = SCROLLING_TOOLBAR_DEFAULT_BUTTON_PADDING;
    _showsScrollArrows = YES;
    _contentEdgeInsets = UIEdgeInsetsZero;
}

- (UIButton *)addButtonWithImage:(UIImage *)image tag:(NSUInteger)tag
{
    UIButton *button = [self addButtonWithTitle:nil andImage:image];
    button.tag = tag;
    return button;
}

- (UIButton *)addButtonWithTitle:(NSString *)title tag:(NSUInteger)buttonTag
{
    UIButton *newButton = [self addButtonWithTitle:title];
    newButton.tag = buttonTag;
    return newButton;
}

- (UIButton *)addButtonWithTitle:(NSString *)title
{
    return [self addButtonWithTitle:title andImage:nil];
}

- (UIButton *)addButtonWithTitle:(NSString *)title
                        andImage:(UIImage *)image
{
    FASToolbarButton *newButton = [FASToolbarButton fasToolbarButton];
    newButton.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    
    if (title) {
        [newButton setTitle:title forState:UIControlStateNormal];
        [newButton sizeToFit];
    }
    
    if (image || !title) {
        [newButton setImage:image forState:UIControlStateNormal];
        newButton.contentMode = UIViewContentModeScaleAspectFill;
        newButton.frame = CGRectMake(0.0f, 0.0f, 44.0f, 44.0f);
    }
    
    [self addButton:newButton];
    
    // return index of new button
    return newButton;
}

- (void)addButton:(UIButton *)newButton
{
    if (!newButton) return;
    
    CGFloat xPos = self.contentEdgeInsets.left;
    UIButton *lastButton = (UIButton *)[_buttons lastObject];
    if (lastButton) {
        xPos = CGRectGetMinX(lastButton.frame) + CGRectGetWidth(lastButton.frame) + _buttonPadding;
    }
    [newButton sizeToFit];
    CGRect buttonFrame = newButton.frame;
    
    // ensure buttons are not larger than max size
    if (CGRectGetWidth(buttonFrame) > self.maxButtonSize.width) {
        buttonFrame.size.width = self.maxButtonSize.width;
    }
    if (self.maxButtonSize.height > 0 &&
        (CGRectGetHeight(buttonFrame) > self.maxButtonSize.height)) {
        buttonFrame.size.height = self.maxButtonSize.height;
    }
    
    buttonFrame.origin.x = xPos;
    buttonFrame.origin.y = CGRectGetMidY(self.bounds) - CGRectGetHeight(buttonFrame)/2;
    newButton.frame = buttonFrame;
    
    [newButton addTarget:self
                  action:@selector(buttonAction:)
        forControlEvents:UIControlEventTouchUpInside];
    
    [_buttons addObject:newButton];
    [_scrollView addSubview:newButton];
    CGFloat contentWidth = CGRectGetMinX(newButton.frame) + CGRectGetWidth(newButton.frame) + self.contentEdgeInsets.right;
    [_scrollView setContentSize:CGSizeMake(contentWidth, 30)];
    
    [self showScrollArrowsIfNecessary];
}

- (void)removeButtonWithTag:(NSUInteger)tagValue
{
    UIButton *button = [self buttonWithTag:tagValue];
    if (button) {
        if (button == _selectedButton) {
            _selectedButton = nil;
        }
        [_buttons removeObject:button];
        [button removeFromSuperview];
        [self setNeedsLayout];
    }
}

- (void)removeAllButtons
{
    _selectedButton = nil;
    for (UIButton *button in _buttons) {
        [button removeFromSuperview];
    }
    [_buttons removeAllObjects];
    [self setNeedsLayout];
}

- (void)setButtonPadding:(NSUInteger)newButtonPadding
{
    if (_buttonPadding != newButtonPadding) {
        _buttonPadding = newButtonPadding;
        [self setNeedsLayout];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect scrollViewFrame = self.bounds;
    if (_showsScrollArrows) {
        scrollViewFrame = CGRectInset(scrollViewFrame, CGRectGetWidth(_leftArrow.bounds), 0.0f);
    }
    
    _scrollView.frame = scrollViewFrame;
    
    [self layoutButtons];
    [self showScrollArrowsIfNecessary];
}

- (void)layoutButtons
{
    // layout buttons
    CGFloat xPos = self.contentEdgeInsets.left;
    CGRect buttonFrame;
    for (UIButton *button in _buttons) {
        [button sizeToFit];
        buttonFrame = button.frame;
        // ensure buttons are not larger than max size
        if (CGRectGetWidth(buttonFrame) > self.maxButtonSize.width) {
            buttonFrame.size.width = self.maxButtonSize.width;
        }
        if (self.maxButtonSize.height > 0 &&
            (CGRectGetHeight(buttonFrame) > self.maxButtonSize.height)) {
            buttonFrame.size.height = self.maxButtonSize.height;
        }
        buttonFrame.origin.x = xPos;
        button.frame = buttonFrame;
        xPos += (CGRectGetWidth(buttonFrame) + _buttonPadding);
    }
    
    // TODO: center buttons when possible
    
    _scrollView.contentSize = CGSizeMake(xPos - _buttonPadding + self.contentEdgeInsets.right, 30);
}

- (void)setShowsScrollArrows:(BOOL)newShowsScrollArrows
{
    _showsScrollArrows = newShowsScrollArrows;
    [self setNeedsLayout];
    [self showScrollArrowsIfNecessary];
}

- (void)selectButtonAtIndex:(NSUInteger)index scroll:(BOOL)scroll animated:(BOOL)animated
{
    self.selectedButton = [self.buttons objectAtIndex:index];
    if (scroll) {
        [self scrollToSelectedButton:animated];
    }
}

- (void)selectButtonWithTag:(NSUInteger)tagValue scroll:(BOOL)scroll animated:(BOOL)animated
{
    self.selectedButton = [self buttonWithTag:tagValue];
    if (self.selectedButton && scroll) {
        [self scrollToSelectedButton:animated];
    }
}

- (NSUInteger)indexOfButton:(UIButton *)button
{
    return [self.buttons indexOfObject:button];
}

- (void)scrollLeft
{
    [self scrollToButton:[self buttonToLeftOfXPosition:_scrollView.contentOffset.x] animated:YES];
}

- (void)scrollRight
{
    [self scrollToButton:[self buttonToRightOfXPosition:_scrollView.contentOffset.x + CGRectGetWidth(_scrollView.bounds)] animated:YES];
}

- (void)scrollToSelectedButton:(BOOL)animated
{
    [self scrollToButton:_selectedButton animated:animated];
}

- (void)scrollToButton:(UIButton *)button animated:(BOOL)animated
{
    if (button) {
        CGRect visibleRect = CGRectInset(button.frame, -10.0f, 0.0f);
        [_scrollView scrollRectToVisible:visibleRect animated:animated];
    }
}

- (UIButton *)buttonWithTag:(NSUInteger)tagValue
{
    UIButton *foundButton = nil;
    
    for (UIButton *button in _buttons) {
        if (button.tag == tagValue) {
            foundButton = button;
            break;
        }
    }
    
    return foundButton;
}

- (void)setSelectedButton:(UIButton *)newSelectedButton
{
    if (_selectedButton != newSelectedButton) {
        if (!self.allowMultipleSelection) {
            _selectedButton.selected = NO;
        }
        newSelectedButton.selected = YES;
        _selectedButton = newSelectedButton;
        [self scrollToSelectedButton:YES];
    }
}

- (void)buttonAction:(id)selector
{
    self.selectedButton = (UIButton *)selector;
    [self.delegate scrollingToolbar:self
             didSelectButtonAtIndex:[self.buttons
                                     indexOfObject:self.selectedButton]];
}

- (void)showScrollArrowsIfNecessary
{
    if (_showsScrollArrows) {
        CGFloat xOffset = _scrollView.contentOffset.x;
        
        // if scrolled to right, show left arrow
        self.leftArrow.hidden = (xOffset <= 0);
        
        // if scrolled to left, show right arrow
        self.rightArrow.hidden = (xOffset >= (_scrollView.contentSize.width - CGRectGetWidth(self.scrollView.frame)));
    }
    
    else {
        self.leftArrow.hidden = YES;
        self.rightArrow.hidden = YES;
    }
}

- (void)setTitle:(NSString *)title forButtonWithTag:(NSUInteger)tagValue
{
    UIButton *button = [self buttonWithTag:tagValue];
    if (button) {
        [button setTitle:title forState:UIControlStateNormal];
        [self setNeedsLayout];
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark ---- UIScrollViewDelegate protocol methods
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self showScrollArrowsIfNecessary];
}

@end

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark ---- ScrollingToolbar (private)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

@implementation ScrollingToolbar (private)

- (UIButton *)buttonToLeftOfXPosition:(CGFloat)XPosition
{
    // find the button closest to the left edge of the scrollingToolbar
    CGFloat startXPos = _scrollView.contentOffset.x;
    CGFloat yPos = CGRectGetMidY(_scrollView.bounds);
    UIButton *nearestButton = nil;
    
    for (CGFloat xPos = startXPos; xPos < _scrollView.contentSize.width; xPos += 5) {
        nearestButton = (UIButton *)[_scrollView hitTest:CGPointMake(xPos, yPos) withEvent:nil];
        if (nearestButton) {
            break;
        }
    }
    
    // if we found a button, return the button to the left of it
    if (nearestButton) {
        if (CGRectGetMinX(nearestButton.frame) >= startXPos) {
            NSUInteger buttonIndex = [self indexOfButton:nearestButton];
            if (buttonIndex != NSNotFound && buttonIndex > 0) {
                buttonIndex--;
            }
            if (buttonIndex < self.buttons.count) {
                nearestButton = [self.buttons objectAtIndex:buttonIndex];
            }
        }
    }
    
    return nearestButton;
}

- (UIButton *)buttonToRightOfXPosition:(CGFloat)XPosition
{
    // find the button closest to the right edge of the scrollingToolbar
    CGFloat startXPos = _scrollView.contentOffset.x + CGRectGetWidth(_scrollView.bounds);
    CGFloat yPos = CGRectGetMidY(_scrollView.bounds);
    UIButton *nearestButton = nil;
    
    for (CGFloat xPos = startXPos; xPos > _scrollView.contentOffset.x; xPos -= 5) {
        nearestButton = (UIButton *)[_scrollView hitTest:CGPointMake(xPos, yPos) withEvent:nil];
        if (nearestButton) {
            break;
        }
    }
    
    // if we found a button, return the button to the right of it
    if (nearestButton) {
        if (CGRectGetMaxX(nearestButton.frame) <= startXPos) {
            NSUInteger buttonIndex = [self indexOfButton:nearestButton];
            if (buttonIndex != NSNotFound && buttonIndex > 0) {
                buttonIndex++;
            }
            
            if (buttonIndex < self.buttons.count) {
                nearestButton = [self.buttons objectAtIndex:buttonIndex];
            }
        }
    }
    
    return nearestButton;
}

@end

