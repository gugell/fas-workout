//
//  StoreItem.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/24/16.
//  Copyright © 2016 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface StoreItem : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "UnityBase.h"
@interface StoreItem (UBIUpdatableWithDictionary) <UBIUpdatableWithDictionary>
@end

#import "StoreItem+CoreDataProperties.h"
