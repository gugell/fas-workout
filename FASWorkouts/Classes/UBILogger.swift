//
//  UBILogger.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 2/11/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

/**
Prints the filename, function name, line number and textual representation of `object` and a newline character into
the standard output if the build setting for "Other Swift Flags" defines `-D DEBUG_LOGGING`.

Only the first parameter needs to be passed to this funtion.


The textual representation is obtained from the `object` using its protocol conformances, in the following
order of preference: `Streamable`, `Printable`, `DebugPrintable`. Do not overload this function for your type.
Instead, adopt one of the protocols mentioned above.

- parameter object:   The object whose textual representation will be printed. If this is an expression, it is lazily evaluated.
- parameter file:     The name of the file, defaults to the current file without the ".swift" extension.
- parameter function: The name of the function, defaults to the function within which the call is made.
- parameter line:     The line number, defaults to the line number within the file that the call is made.
*/
func printlnDebug<T>(@autoclosure object:  () -> T, _ file: String = __FILE__, _ function: String = __FUNCTION__, _ line: Int = __LINE__) {
#if DEBUG_LOGGING
    let filename = NSURL(string:file)?.lastPathComponent?.componentsSeparatedByString(".").first
    let file = (filename ?? "<unknown>")
    print("\(file).\(function)[\(line)]: \(object())")
#endif
}

func printlnDebugTrace<T>(@autoclosure object:  () -> T, _ file: String = __FILE__, _ function: String = __FUNCTION__, _ line: Int = __LINE__) {
#if DEBUG_LOGGING
    let filename = NSURL(string:file)?.lastPathComponent?.componentsSeparatedByString(".").first
    let file = (filename ?? "<unknown>")
    print("\(file).\(function)[\(line)]: [TRACE] \(object())")
#endif
}

func printlnDebugWarning<T>(@autoclosure object:  () -> T, _ file: String = __FILE__, _ function: String = __FUNCTION__, _ line: Int = __LINE__) {
#if DEBUG_LOGGING
    let filename = NSURL(string:file)?.lastPathComponent?.componentsSeparatedByString(".").first
    let file = (filename ?? "<unknown>")
    print("\(file).\(function)[\(line)]: [WARNING] \(object())")
#endif
}

func printlnDebugError<T>(@autoclosure object:  () -> T, _ file: String = __FILE__, _ function: String = __FUNCTION__, _ line: Int = __LINE__) {
#if DEBUG_LOGGING
    let filename = NSURL(string:file)?.lastPathComponent?.componentsSeparatedByString(".").first
    let file = (filename ?? "<unknown>")
    print("\(file).\(function)[\(line)]: [ERROR] \(object())")
#endif
}

///////////////////////////////////////////////
// MARK: Crashlytics logging
///////////////////////////////////////////////
import Crashlytics

// this method gives us pretty much the same functionality as the CLS_LOG macro, but written as a Swift function, the only differences are that we have to use array syntax for the argument list and that we don't get see if the method being called is a class method or an instance method. We also have to define the DEBUG compiler flag with -DDEBUG.
// usage:
// CLS_LOG_SWIFT()
// CLS_LOG_SWIFT("message!")
// CLS_LOG_SWIFT("message with parameter 1: %@ and 2: %@", ["First", "Second"])
func CLS_LOG_SWIFT( format: String = "", _ args:[CVarArgType] = [], file: String = __FILE__, function: String = __FUNCTION__, line: Int = __LINE__)
{
    let filename = NSURL(string:file)?.lastPathComponent?.componentsSeparatedByString(".").first
    #if DEBUG_LOGGING
        CLSNSLogv("\(filename).\(function) line \(line) $ \(format)", getVaList(args))
    #else
        CLSLogv("\(filename).\(function) line \(line) $ \(format)", getVaList(args))
    #endif
    
}

// CLS_LOG() output: -[ClassName methodName:] line 10 $
// CLS_LOG_SWIFT() output: ClassName.methodName line 10 $
