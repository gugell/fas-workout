//
//  ExerciseProgressEvent.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/8/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, ExerciseProgressEventType) {
    ExerciseProgressEventTypeBegin = 0,
    ExerciseProgressEventTypeRepeatCount = 1,
    ExerciseProgressEventTypeEnd = 2,
    
    ExerciseProgressEventTypeUnknown = 999,
};

@interface ExerciseProgressEvent : NSObject

@property (nonatomic, readonly) ExerciseProgressEventType type;
@property (nonatomic, strong, readonly) __nullable id value;

+ (nonnull instancetype)eventWithType:(ExerciseProgressEventType)type value:(nullable id)value;

@end
