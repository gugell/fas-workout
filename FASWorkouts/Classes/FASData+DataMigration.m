//
//  FASData+DataMigration.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/18/16.
//  Copyright © 2016 FAS. All rights reserved.
//

#import "FASData+DataMigration.h"
// thread-safe mutable map
#import "XJSafeMutableDictionary.h"

// get model for purchase status
#import "StoreObserver.h"

NSString * const DEPRECATED_FASDataAvailableProductIDsPrefKey   = @"FASPrefAvailableProductIDs";
extern NSString * const FASDataPurchasedItemsPrefKey;

@interface FASData (Protected)
@property (nonatomic, strong) XJSafeMutableDictionary *purchasedItems;
@end

@implementation FASData (DataMigration)

// Migrate:
//        @[
//            "wkr.product.id.one",
//            "wkr.product.id.one"
//        ]
// To:
//        @{
//          "workout":    @{
//                            "wkr.product.id.one": 8
//                            "wkr.product.id.two": 8
//                        },
//          "workoutBulk":  @{
//                              "wkr.bulk.product.id.one": 8
//                              "wkr.bulk.product.id.one": 8
//                          }
//          }

- (void)migrateAvailableProductIDsToPurchasedItems
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *purchasedProductIDs = (NSArray *)[userDefaults objectForKey:DEPRECATED_FASDataAvailableProductIDsPrefKey];
    if (purchasedProductIDs) {
        if (!self.purchasedItems) {
            self.purchasedItems = [XJSafeMutableDictionary dictionaryWithCapacity:1];
        }

        NSMutableDictionary <NSString*, NSNumber*> * purchasedWorkoutProducts = [NSMutableDictionary dictionaryWithCapacity:purchasedProductIDs.count];
        NSMutableDictionary <NSString*, NSNumber*> * purchasedWorkoutBulkProducts = [NSMutableDictionary dictionary];
        
        for (NSString *purchasedProductID in purchasedProductIDs) {
            // while testing new version, some Apple ID already has Bulk Workout products purchased,
            // we determine that products by Product Identifier having "workoutbulk" substring
            NSString* workoutBulkProductIdentifierSuffix = @".workoutbulk.";
            // assuming the ids here have been validated successfully, hence this status
            NSNumber* purchaseStatus = @(SOProductStatusReceiptValidationSucceeded);
            if ([purchasedProductID rangeOfString:workoutBulkProductIdentifierSuffix].location == NSNotFound) {
                [purchasedWorkoutProducts setObject:purchaseStatus forKey:purchasedProductID];
            } else {
                [purchasedWorkoutBulkProducts setObject:purchaseStatus forKey:purchasedProductID];
            }
        }
        
        [self.purchasedItems  setObject:purchasedWorkoutProducts forKey:@"workout"];
        [self.purchasedItems  setObject:purchasedWorkoutBulkProducts forKey:@"workoutBulk"];
        
        // save new data model
        [userDefaults setObject:self.purchasedItems forKey:FASDataPurchasedItemsPrefKey];
        // remove deprecated data model
        [userDefaults removeObjectForKey:DEPRECATED_FASDataAvailableProductIDsPrefKey];
        [userDefaults synchronize];
    }
}

@end
