//
//  FASWorkoutCell.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/7/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit
import QuartzCore

class FASWorkoutCell: FASTableViewCell {
    
    @IBOutlet weak var remoteImageView: UBICustomRemoteImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var musclesLabel: UILabel!
    @IBOutlet weak var cyclesLabel: UILabel!
    @IBOutlet weak var workingTimeLabel: UILabel!
    @IBOutlet weak var friendlyImageView: UIImageView!
    @IBOutlet weak var calImageView: UIImageView!
    
    private let customAccessoryView = FASRibbonAccessoryView()
    
    // kepp track of shadow layers
    private var currentShadowLayer: CAShapeLayer?
    private var currentMaskLayer: CAShapeLayer?
    
    enum WorkoutStatus {
        case None
        case OnSale
        case Paid(String)
        case New
    }
    private var status: WorkoutStatus = .None {
        didSet {
            updateStatus()
        }
    }
    
    enum FASWorkoutCellColorTheme {
        case Default
        case White
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        separatorType = .Default
        
        customAccessoryView.frame = CGRectMake(0.0, 0.0, 40.0, 88.0)
        customAccessoryView.autoresizingMask = [UIViewAutoresizing.FlexibleLeftMargin, UIViewAutoresizing.FlexibleBottomMargin]
        accessoryView = customAccessoryView

        selectionStyle = UITableViewCellSelectionStyle.None
        
        // uncomment to debug layout
//        remoteImageView.backgroundColor = UIColor.redColor()
//        nameLabel.backgroundColor = UIColor.greenColor()
//        musclesLabel.backgroundColor = UIColor.blueColor()
//        cyclesLabel.backgroundColor = UIColor.magentaColor()
//        workingTimeLabel.backgroundColor = UIColor.yellowColor()
//        customAccessoryView.backgroundColor = UIColor.blueColor()
//        calImageView.backgroundColor = UIColor.blueColor()
//        friendlyImageView.backgroundColor = UIColor.blueColor()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        workout = nil
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        backgroundColor = selected ? UIColor.blackColor().colorWithAlphaComponent(0.3) : UIColor.clearColor()
    }
    
    override func setHighlighted(highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        backgroundColor = highlighted ? UIColor.blackColor().colorWithAlphaComponent(0.3) : UIColor.clearColor()
    }
    
    var workout: Workout? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        
        remoteImageView.remoteImage = workout?.remoteImage()
        
        //cell.backgroundView = [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"cell_normal.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
        //cell.selectedBackgroundView =  [[UIImageView alloc] initWithImage:[ [UIImage imageNamed:@"cell_pressed.png"] stretchableImageWithLeftCapWidth:0.0 topCapHeight:5.0] ];
        remoteImageView.hidden = true;
        
        self.backgroundView = remoteImageView;
        
  
        
        remoteImageView.title = workout?.formattedAvailabilityStatus()
        
        // line #1
        nameLabel.text = workout?.name
        
        // line #2
        var secondLineText: String?
        var calImage: UIImage?
        if let weekDayInfo = workout?.weekDayInfo {
            secondLineText = " " + weekDayInfo // add some space between image and label
            calImage = UIImage(named: "calendar_icon")
        }
        else {
            secondLineText = workout?.formattedMuscleTypes()
            calImage = nil
        }
        musclesLabel.text = secondLineText
        calImageView.image = calImage
        
        // line #3
        // TODO: localize it
        var timeAndRoundsInfo: String = ""
        if let workingTimeNumber = workout?.timeSpan {
            let workingTime: NSString = NSString.formattedTimeFromSeconds(workingTimeNumber)
            timeAndRoundsInfo = "time: \(workingTime)"
        }
        
        if let numberOfCyclesText = workout?.lapsNumber {
            if timeAndRoundsInfo.isBlank() == false {
                timeAndRoundsInfo += " | "
            }
            timeAndRoundsInfo += "rounds: \(numberOfCyclesText)"
        }
        workingTimeLabel.text = timeAndRoundsInfo
        
        // line #4
        var genderSpecificText = workout?.formattedGenderSpecific()
        
        // assign respective image and add space for right margin
        var friendlyImage: UIImage?
        if let gs: FASWorkoutGenderSpecificType = workout?.genderSpecific() {
            switch gs {
            case .Men:
                friendlyImage = UIImage(named: "gender_frendly_male")
                genderSpecificText = genderSpecificText ?? " " + genderSpecificText!
            case .Women:
                friendlyImage = UIImage(named: "gender_frendly_female")
                genderSpecificText = genderSpecificText ?? " " + genderSpecificText!
            default: break
            }
        }
        friendlyImageView.image = friendlyImage
        cyclesLabel.text = genderSpecificText
        
        // TODO: add "is new" logic here, so the status takes priority on other statuses
        status = WorkoutStatus.None
        
        if let tt = workout?.trainingType where tt == FASWorkoutTrainingTypeTabata {
            applyCellColorTheme(.White)
        }
        else {
            applyCellColorTheme(.Default)
        }
    }
    
    private func updateStatus() {
        switch status {
        case .None:
            customAccessoryView.style = FASRibbonAccessoryViewStyle.StripNone
            removeRightShadowFromAccessoryView()
        case .Paid(let price):
            customAccessoryView.style = FASRibbonAccessoryViewStyle.StripGreen(price)
            addRightShadowToAccessoryView()
        case .OnSale:
            // TODO: localize this
            customAccessoryView.style = FASRibbonAccessoryViewStyle.StripOrange("sale")
            addRightShadowToAccessoryView()
        case .New:
            // TODO: localize this
            customAccessoryView.style = FASRibbonAccessoryViewStyle.StripYellow("new")
            addRightShadowToAccessoryView()
        }
        
        setNeedsDisplay()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    // TODO: replace w/ UBICustomRemoteImage
    private func applyRemoteImageViewStyleWithRingColor(ringColor: UIColor = UIColor.fasYellowColor()) {
        // circular image
        remoteImageView.layer.masksToBounds = true
        remoteImageView.layer.cornerRadius = remoteImageView.bounds.size.width / 2.0
        // add borders
        remoteImageView.layer.borderWidth = 3.0
        remoteImageView.layer.borderColor = ringColor.CGColor
        // TODO: make a smooth ring
    }
    
    private func applyCellColorTheme(theme: FASWorkoutCellColorTheme) {
        switch theme {
        case .Default:
            // label #1
            nameLabel.textColor = UIColor.fasYellowColor()
            // label #2
            musclesLabel.textColor = UIColor.fasYellowColor()
            musclesLabel.alpha = 0.6
            // label #3
            workingTimeLabel.textColor = UIColor.whiteColor()
            // label #4
            cyclesLabel.textColor = UIColor.whiteColor()
            // image
           // applyRemoteImageViewStyleWithRingColor()
            
        case .White:
            // label #1
            nameLabel.textColor = UIColor.whiteColor()
            // label #2
            musclesLabel.textColor = UIColor.whiteColor()
            musclesLabel.alpha = 0.6
            // label #3
            workingTimeLabel.textColor = UIColor.whiteColor()
            // label #4
            cyclesLabel.textColor = UIColor.whiteColor()
            // image
       //applyRemoteImageViewStyleWithRingColor(UIColor.whiteColor())
        }
    }
    
    private func addRightShadowToAccessoryView() {
        
        if currentShadowLayer != nil || currentMaskLayer != nil {
            // make sure we don't add the shadow twice
            return
        }

        // shadow
        let shadowPath = UIBezierPath()
        shadowPath.moveToPoint(CGPointMake(CGRectGetWidth(customAccessoryView.bounds)/2.0, CGRectGetHeight(customAccessoryView.bounds)/2.0))
        shadowPath.addLineToPoint(CGPointMake(CGRectGetWidth(customAccessoryView.bounds), 0.0))
        shadowPath.addLineToPoint(CGPointMake(CGRectGetWidth(customAccessoryView.bounds), CGRectGetHeight(customAccessoryView.bounds)))
        shadowPath.closePath()
        
        let shadowLayer = CAShapeLayer()
        shadowLayer.frame = customAccessoryView.bounds
        shadowLayer.shadowColor = UIColor.blackColor().CGColor
        shadowLayer.shadowOffset = CGSizeMake(0, 0)
        shadowLayer.shadowOpacity = 0.5
        shadowLayer.shadowRadius = 3.0
        shadowLayer.shadowPath = shadowPath.CGPath
        customAccessoryView.layer.addSublayer(shadowLayer)
        
        // masking shadow
        let maskPath = UIBezierPath()
        maskPath.moveToPoint(CGPointMake(CGRectGetWidth(customAccessoryView.bounds), 0.0))
        maskPath.addLineToPoint(CGPointMake(CGRectGetWidth(customAccessoryView.bounds) + 10.0, 0.0))
        maskPath.addLineToPoint(CGPointMake(CGRectGetWidth(customAccessoryView.bounds) + 10.0, CGRectGetHeight(customAccessoryView.bounds)))
        maskPath.addLineToPoint(CGPointMake(CGRectGetWidth(customAccessoryView.bounds), CGRectGetHeight(customAccessoryView.bounds)))
        maskPath.closePath()
        
        let maskLayer = CAShapeLayer()
        maskLayer.frame = CGRectMake(0.0, 0.0, CGRectGetWidth(customAccessoryView.bounds)+10.0, CGRectGetHeight(customAccessoryView.bounds))
        maskLayer.path = maskPath.CGPath;
        shadowLayer.mask = maskLayer; //add mask to shadow layer only
        
        // store layers
        currentShadowLayer = shadowLayer
        currentMaskLayer = maskLayer
        
        // TODO: right stroke
    }
    
    private func removeRightShadowFromAccessoryView() {
        if let shadowLayer = currentShadowLayer {
            shadowLayer.removeFromSuperlayer()
            currentShadowLayer = nil
        }
        
        if let maskLayer = currentMaskLayer {
            maskLayer.removeFromSuperlayer()
            currentMaskLayer = nil
        }
    }
}