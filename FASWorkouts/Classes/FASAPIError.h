//
//  FASAPIError.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/22/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const FASAPIErrorDomain;

// API error codes
typedef NS_ENUM(NSUInteger, FASAPIErrorCode) {
    FASAPIErrorCodeCustom                            = 0,
    FASAPIErrorCodeInvalidResponse                   = 1, // App
    FASAPIErrorCodeUnknownError                      = 3,
    FASAPIErrorCodeParserError                       = 4,
    FASAPIErrorCodeHostUnreachable                   = 5,
    FASAPIErrorCodeInvalidUsernameOrPassword         = 9, // UB
    FASAPIErrorCodeUnsanitizedValue                  = 10,
    FASAPIErrorCodeUsernameContainsInvalidCharacters = 11,
    FASAPIErrorCodeAttributeNotExist                 = 16, // UB
    FASAPIErrorCodeTokenRequired                     = 100,
    FASAPIErrorCodeTokenInvalid                      = 101,
    FASAPIErrorCodeFacebookSessionInvalid            = 102,
    FASAPIErrorCodePermissionDeniedForAccount        = 103,
    FASAPIErrorCodeAccountDisabled                   = 200,
    FASAPIErrorCodeUsernameUnavailable               = 202,
    FASAPIErrorCodePasswordInvalid                   = 203, // App
    FASAPIErrorCodePasswordConfirmationInvalid       = 204, // App
    FASAPIErrorCodeEmailInvalid                      = 205, // App
    FASAPIErrorCodeEmailUnavailable                  = 206,
    FASAPIErrorCodeAccountUnverified                 = 207,
    FASAPIErrorCodeFacebookLoginOrRegister           = 208,
    FASAPIErrorCodeDeviceAlreadyLinkedToAccount      = 209,
    FASAPIErrorCodePermissionDenied                  = 300,
    FASAPIErrorCodeParamRequired                     = 400,
    FASAPIErrorCodeParamInvalid                      = 401,
    FASAPIErrorCodePhotoInvalid                      = 402,
    FASAPIErrorCodePhotoTooLarge                     = 403,
    FASAPIErrorCodeAlertSubjectNotFound              = 501,
    FASAPIErrorCodeAlertNotFound                     = 502,
    FASAPIErrorCodeInAppReceiptNotFound              = 502, // App
    
    FASAPIErrorCodeFacebookPermissionDenied          = 603,
    FASAPIErrorCodeLicenseInvalid                    = 604, // App

    FASAPIErrorCodeUnexpectedError                   = 900,
    FASAPIErrorCodeServiceTemporarilyUnavailable     = 901,
    FASAPIErrorCodeNSURLErrorTimedOut                = 902,
    FASAPIErrorCodeUpgradeRequired                   = 9999,
    // UnityBase error codes
    FASAPIErrorCodeLoginIsAlreadyTaken               = 1001,
    FASAPIErrorCodeEmailIsAlreadyTaken               = 1002,
    FASAPIErrorCodeNoError                           = 9998,
};

@interface FASAPIError : NSError

@property (nonatomic, copy)     NSString *      value;

+ (FASAPIError *)errorWithInfo:(NSDictionary *)errorInfo;
+ (FASAPIError *)errorWithCode:(FASAPIErrorCode)code
                         value:(NSString *)value;

- (instancetype)initWithCode:(FASAPIErrorCode)code value:(NSString *)value;
- (instancetype)initWithInfo:(NSDictionary *)errorInfo;

+ (NSString *)alertStringForErrors:(NSArray *)errors;
+ (void)displayAlertWithErrors:(NSArray *)errors;

+ (NSString *)localizedDescriptionStringKeyForCode:(FASAPIErrorCode)code;

+ (BOOL)errors:(NSArray *)errors containsErrorWithCode:(FASAPIErrorCode)code;
@end
