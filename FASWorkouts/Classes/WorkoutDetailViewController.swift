//
//  WorkoutDetailViewController.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/13/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

typealias SegueIdentifier = String
enum WorkoutDetailViewControllerSegueIdentifier: SegueIdentifier {
    case ShowAttachments = "segueToShowAttachments"
    case StartWorkout = "segueToStartWorkout"
    case ShowPurchaseInfo = "segueToPurchaseInfo"
}

enum WorkoutDetailViewControllerAlertViewTag: Int {
    case DownloadFilesPrompt
}

enum WorkoutDetailTableViewSections: Int {
    case Dump
    case Exercises
}

class WorkoutDetailViewController: FASViewController {
    
    // MARK: properties
    
    private var kDetailWorkoutContext = 1
    private var hasDetailWorkoutObserver = false
    var detailWorkout: Workout? {
        didSet {
            configureView()
            
            if let unwrappedWrk = detailWorkout {
                fetchExercises()
                FASData.sharedData().loadExercisesForWorkoutIfNeeded(unwrappedWrk)
                
                // Track
                FASAnalyticsEventHelper.trackWorkoutViewWithName(unwrappedWrk.name)
            } else {
                printlnDebugWarning("nil workout passed")
            }
            
            // setup KVO
            removeDetailWorkoutObserverIfNeeded(oldValue)
            addDetailWorkoutObserver(detailWorkout)
        }
    }
    
    let kExerciseCelllIdentifier = "ExerciseCelllIdentifier"
    let kRestTimeFooterIdentifier = "RestTimeFooterIdentifier"
    let kSupersetFooterIdentifier = "SupersetFooterIdentifier"
    var workoutDownloadTracker: FASAttachDownloadProgreesTracker?
    
    // determines, whether to show library by tapping on exercise cell
    let showExerciseLibrary: Bool = true
    
    let extraWorkoutInfoHeaderView: WorkoutDetailHeaderView = WorkoutDetailHeaderView()
    
    @IBOutlet weak var workoutInfoView: FASImageAndTwoLabelsHeaderView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var primaryActionButton: UIButton!
    @IBOutlet weak var bottomPanelView: UIView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var coverView: FASCoverView! // TODO: add spinner and label to present any error message related to exercise fetching / loading
    private var shareButton: UIBarButtonItem?
    private var settingsButton: UIBarButtonItem?
    
    private lazy var fetchedExercisesController: NSFetchedResultsController? = {
        let controller = FASData.sharedData().fetchedExercisesControllerWithCacheName(nil,
            context: nil,
            workout: self.detailWorkout)
        controller.delegate = self
        return controller
        }()
    private var cacheName: String? = nil
    private let actionController = FASActionController()
    ///////////////////////////////////////////////
    // MARK: - Init -
    ///////////////////////////////////////////////
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        actionController.viewController = self
        FASData.sharedData().addExercisesObserver(self)
    }
    
    deinit {
        // remove self as a KVO observer, if needed
        removeDetailWorkoutObserverIfNeeded(self.detailWorkout)
        FASData.sharedData().removeExercisesObserver(self)
        workoutDownloadTracker?.stop()
    }
    ///////////////////////////////////////////////
    // MARK: - UIViewController overrides -
    ///////////////////////////////////////////////
    override func viewDidAppearForTheFirstTime() {
        super.viewDidAppearForTheFirstTime()
        DUBApptentiveDeferredEngagement.engage(kFASApptentiveEventAfterDidViewWorkoutDetail, fromViewController: self)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureBottomButton()
        configureProgressBar()
        
        tableView.estimatedRowHeight = 60.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        shareButton = UIBarButtonItem(image: UIImage(named: "shareButton"), style: UIBarButtonItemStyle.Plain, target: self, action: "shareWorkoutDidPress")
        settingsButton = UIBarButtonItem(image: UIImage(named: "settingsButton"), style: UIBarButtonItemStyle.Plain, target: self, action: "settingsButtonDidPress")
        if FASData.sharedData().isIPAD {
            // do not add settings button when running on iPad
            navigationItem.rightBarButtonItems = [shareButton!]
        }
        else {
            navigationItem.rightBarButtonItems = [settingsButton!,shareButton!]
        }
        
        self.tableView.backgroundColor = UIColor.clearColor()
        
        self.tableView.registerClass(FASSeparatorFooterView.self, forHeaderFooterViewReuseIdentifier: kRestTimeFooterIdentifier)
        self.tableView.registerClass(FASTableViewFooterView.self, forHeaderFooterViewReuseIdentifier: kSupersetFooterIdentifier)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        disableScreenDimming()
    }
    
    override func viewDidDisappear(animated: Bool) {
        enableScreenDimming()
        super.viewDidDisappear(animated)
        
        configureNavigationBar(.Dark)
    }
    ///////////////////////////////////////////////
    // MARK: - Actions -
    ///////////////////////////////////////////////
    @IBAction func primaryActionButtonDidPress(sender: AnyObject) {
        if sender as! NSObject == primaryActionButton {
            if let detailWorkout = detailWorkout {
                if detailWorkout.isAvailable() == false {
                    performSegueWithIdentifier(WorkoutDetailViewControllerSegueIdentifier.ShowPurchaseInfo.rawValue, sender: self)
                }
                else {
                    performSegueWithIdentifier(WorkoutDetailViewControllerSegueIdentifier.StartWorkout.rawValue, sender: self)
                }
            }
        }
    }
    
    override func configureView() {
        super.configureView()
        
        if let aWorkout = detailWorkout {
            // info view might be nil when first assigning workout object
           
            
            workoutInfoView?.item = aWorkout
            title = aWorkout.name
            workoutInfoView?.hidden = false
            shareButton?.enabled = true
            extraWorkoutInfoHeaderView.calories = aWorkout.formattedCalories()
            extraWorkoutInfoHeaderView.muscles = aWorkout.formattedPrimaryMuscleType()
            coverView?.hidden = false
        }
        else {
            printlnDebugWarning("no workout to display details")
            workoutInfoView?.item = nil
            title = nil
            workoutInfoView?.hidden = true
            shareButton?.enabled = false
            extraWorkoutInfoHeaderView.calories = nil
            extraWorkoutInfoHeaderView.muscles = nil
            coverView?.hidden = true
        }
    }
    
    private func configureBottomButton() {
        func hideBottomPanelView(@autoclosure predicate :  () -> Bool) {
            bottomPanelView?.hidden = predicate()
        }
        
        if let wrk = detailWorkout {
            hideBottomPanelView(!FASData.sharedData().isEachExercisesHasAtLeastOneAttachInWorkout(wrk))
            
            let actionBtnTitle: String
            let actionBtnBackgroundColor: UIColor
            let actionBtnTitleColorNormal: UIColor
            let actionBtnTitleColorHighlighted: UIColor
            
            if wrk.isAvailable() == false {
                actionBtnTitle = "GET YOUR PLAN TODAY!"
                actionBtnBackgroundColor = UIColor.fasYellowColor()
                actionBtnTitleColorHighlighted = UIColor.fasYellowColor()
                actionBtnTitleColorNormal = UIColor.blackColor()
            }
            else if FASData.sharedData().hasFilesDownloadedForWorkout(wrk)
                && !FASData.sharedData().forceDownloadAttachFiles() {
                actionBtnTitle = "START"
                actionBtnBackgroundColor = UIColor.fasYellowColor()
                actionBtnTitleColorHighlighted = UIColor.blackColor()
                actionBtnTitleColorNormal = UIColor.blackColor()
            }
            else if wrk.downloadStatus() == .Downloading {
                actionBtnTitle = "DOWNLOADING"
                actionBtnBackgroundColor = UIColor.fasGrayColor206()
                actionBtnTitleColorHighlighted = UIColor.blackColor()
                actionBtnTitleColorNormal = UIColor.blackColor()
            }
            else {
                actionBtnTitle = "DOWNLOAD"
                actionBtnBackgroundColor = UIColor.fasYellowColor()
                actionBtnTitleColorHighlighted = UIColor.blackColor()
                actionBtnTitleColorNormal = UIColor.blackColor()
            }
            
            primaryActionButton?.setTitle(actionBtnTitle, forState: UIControlState.Normal)
            primaryActionButton?.setTitle(actionBtnTitle, forState: UIControlState.Highlighted)
            primaryActionButton?.backgroundColor = actionBtnBackgroundColor
            primaryActionButton?.setTitleColor(actionBtnTitleColorNormal, forState: UIControlState.Normal)
            primaryActionButton?.setTitleColor(actionBtnTitleColorHighlighted, forState: UIControlState.Highlighted)
        }
        else {
            hideBottomPanelView(true)
        }
    }
    
    private func configureProgressBar() {
        // TODO: protect it from re-creating progress bar if called more than one time per the VC
        self.progressView.hidden = true // initally hidden
        workoutDownloadTracker = FASAttachDownloadProgreesTracker(context: FASData.sharedData().mainContext, completionClosure: { [weak self] (downloadProgress, completed) -> () in
            guard let strongSelf = self else { return }
            strongSelf.progressView.hidden = completed
            strongSelf.progressView.progress = downloadProgress
            
            strongSelf.primaryActionButton.enabled = completed
            strongSelf.configureBottomButton()
            
            if completed {
                if let wrk = strongSelf.detailWorkout {
                    if FASData.sharedData().allDownloadedPreferredAttachesCopiedForWorkout(strongSelf.fetchedExercisesController?.fetchedObjects) {
                        strongSelf.configureBottomButton()
                        
                        // Update Workout Download Status
                        let c = FASData.sharedData().mainContext
                        let objectID = wrk.objectID
                        c.performBlockAndWait({ () -> Void in
                            do {
                                let object = try c.existingObjectWithID(objectID)
                                if let workout = object as? Workout {
                                    workout.setDownloadStatus(.None)
                                    // TODO: save it when assigning is happening in FASData
                                }
                            } catch let someError {
                                printlnDebugError("\(someError) is occured")
                            }
                        })
                    } else {
                        // NOTE: see the comments in UBIAttachFileCache marked as HACK:
                        printlnDebugWarning("NOT all files are copied to cache path for workout:\(strongSelf.detailWorkout!)")
                    }
                }
            }
        })
    }
    
    private func presentNotificationsSettingsViewController() {
        FASSettingsViewController.presentFromViewController(self, withSettingsType: .Notifications)
    }
    
    private func presentMenuViewController() {
        FASMenuViewController.presentFromViewController(self)
    }
    
    func shareWorkoutDidPress() {
        actionController.fbShareContentWithItem(self.detailWorkout, completion: nil)
    }
    
    func settingsButtonDidPress() {
        presentMenuViewController()
    }
    ///////////////////////////////////////////////
    // MARK: - Layout -
    ///////////////////////////////////////////////
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let bottomInset = bottomPanelView.hidden ? 0.0 : bottomPanelView.frame.height
        tableView.contentInset.bottom = bottomInset
        tableView.scrollIndicatorInsets.bottom = bottomInset
    }

    ///////////////////////////////////////////////
    // MARK: - Segue -
    ///////////////////////////////////////////////
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?)
    {
        if let segueIdentifier: SegueIdentifier = segue.identifier {
            if let workoutDetailSegueIdentifier = WorkoutDetailViewControllerSegueIdentifier(rawValue: segueIdentifier) {
                switch workoutDetailSegueIdentifier {
                case .ShowAttachments:
                    if let cell = sender as? UITableViewCell {
                        let indexPath = tableView.indexPathForCell(cell)
                        let object = fetchedExercisesController?.objectAtIndexPath(indexPath!) as? Exercise
                        if let controller = segue.destinationViewController as? AttachmentsViewController {
                            controller.exercise = object
                            //                        controller.navigationItem.leftBarButtonItem = splitViewController!.displayModeButtonItem();
                            //                        controller.navigationItem.leftItemsSupplementBackButton = true;
                        }
                    }
                case .StartWorkout:
                    
                    // nested func
                    func configureWorkoutViewController(controller: FASWorkoutViewController) {
                        controller.workout = detailWorkout
                        controller.delegate = self
                    }
                    
                    // the controller might be w/ or w/o navigation controller
                    if let controller = segue.destinationViewController as? FASWorkoutViewController {
                        configureWorkoutViewController(controller)
                    } else if let navController = segue.destinationViewController as? UINavigationController {
                        if let controller = navController.topViewController as? FASWorkoutViewController {
                            configureWorkoutViewController(controller)
                        }
                    }
                default :
                    break;
                }
            }
        }
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        if let segueIdentifier: SegueIdentifier = identifier {
            if let workoutDetailSegueIdentifier = WorkoutDetailViewControllerSegueIdentifier(rawValue: segueIdentifier) {
                switch workoutDetailSegueIdentifier {
                    
                case .StartWorkout:
                    // Do not start workout if hte app is not active
                    if UIApplication.sharedApplication().applicationState != .Active {
                        return false
                    }
                    
                    if let aWorkout = detailWorkout {
                        if FASData.sharedData().hasFilesDownloadedForWorkout(aWorkout) && !FASData.sharedData().forceDownloadAttachFiles() {
                            return true
                        }
                        else {
                            let alert = FASAlertView(title: "Download Workout?", message: "Workout does not have all required video downloaded. Do you want to download them now?", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "Download")
                            alert.tag = WorkoutDetailViewControllerAlertViewTag.DownloadFilesPrompt.rawValue
                            alert.show()
                            
                            return false
                        }
                    }
                    else {
                        return false
                    }
                    
                case .ShowPurchaseInfo:
                    return true
                    
                default:
                    printlnDebugWarning("Unknown segue identifier %@", segueIdentifier)
                }
            }
        }
        
        return false
    }
    
    // override default performSegueWithIdentifier to call shouldPerformSegueWithIdentifier at first
    override func performSegueWithIdentifier(identifier: String, sender: AnyObject?) {
        if self.shouldPerformSegueWithIdentifier(identifier, sender: sender) {
            super.performSegueWithIdentifier(identifier, sender: sender)
        }
    }
    
    ///////////////////////////////////////////////
    // MARK: - Controls -
    ///////////////////////////////////////////////
    
    func fetchExercises() {
        if let cn = cacheName {
            NSFetchedResultsController.deleteCacheWithName(cn)
        }
        
        if let frc = fetchedExercisesController {
            var fetchError: NSError?
            let result: Bool
            do {
                try frc.performFetch()
                result = true
            } catch let error as NSError {
                fetchError = error
                result = false
            }
            if !result {
                if let error = fetchError {
                    // TODO: Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    printlnDebugError("Unresolved error \(error.localizedDescription),\nerror info: \(error.userInfo)")
                    abort()
                }
            }
        } else {
            printlnDebugError("Fetched Result Controller was nil upon fetch()")
        }
    }
    
    ///////////////////////////////////////////////
    // MARK: - KVO -
    ///////////////////////////////////////////////
    
    private func addDetailWorkoutObserver(workout: Workout?) {
        if (hasDetailWorkoutObserver == true) {
            printlnDebugWarning("KVO observer is already added for workout, will ignore")
        }
        
        if let _ = workout {
            workout!.addObserver(self, forKeyPath: "purchaseStatus", options: .New, context: &kDetailWorkoutContext)
            hasDetailWorkoutObserver = true
        }
    }
    
    private func removeDetailWorkoutObserverIfNeeded(workout: Workout?) {
        if hasDetailWorkoutObserver && workout != nil {
            workout!.removeObserver(self, forKeyPath: "purchaseStatus", context: &kDetailWorkoutContext)
            hasDetailWorkoutObserver = false
        }
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if context == &kDetailWorkoutContext {
            if let _ = change?[NSKeyValueChangeNewKey] where keyPath == "purchaseStatus" {
                assert(NSThread.mainThread() === NSThread.currentThread())
                self.configureBottomButton()
            }
        } else {
            super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)
        }
    }
}

///////////////////////////////////////////////
// MARK: - Fetched results controller -
///////////////////////////////////////////////

extension WorkoutDetailViewController: NSFetchedResultsControllerDelegate {
    /*
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        tableView.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        switch type {
        case .Insert:
            tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Middle)
        case .Delete:
            tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Middle)
        default:
            0
        }
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch type {
        case .Insert:
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Middle)
        case .Delete:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Middle)
        case .Update:
            let exCell: FASExerciseCell = self.tableView.cellForRowAtIndexPath(indexPath!) as! FASExerciseCell
            configureExerciseCell(exCell, atIndexPath: indexPath!)
        case .Move:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation:.Middle)
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation:.Middle)
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.endUpdates()
        
        configureBottomButton()
    }
    */
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        if self.isViewLoaded() {
            tableView.reloadData()
            configureBottomButton()
        }
    }
}

///////////////////////////////////////////////
// MARK: - TableView Delegate -
///////////////////////////////////////////////

extension WorkoutDetailViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.clearColor()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let dataSection = indexPath.section - 1
        let obj: AnyObject? = fetchedExercisesController?.objectAtIndexPath(NSIndexPath(forRow: 0, inSection: dataSection))
        if let ex = obj as? Exercise {
            // Display exercise info screen
            if self.showExerciseLibrary == true {
                let exerciseInfoVC: UIViewController? = self.mainStoryboard.instantiateViewControllerWithIdentifier("FASExerciseInfoViewController")
                
                if let nonNilVC = exerciseInfoVC as? FASExerciseInfoViewController {
                    nonNilVC.exercise = ex
                    self.navigationController?.pushViewController(nonNilVC, animated: true)
                }
            }
        }
    }
}

///////////////////////////////////////////////
// MARK: - TableView DataSource -
///////////////////////////////////////////////

extension WorkoutDetailViewController: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if let frc = fetchedExercisesController {
            if let sections = frc.sections {
                return sections.count+1
            }
        }
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == WorkoutDetailTableViewSections.Dump.rawValue {
            return 0
        }
        
        if section >= WorkoutDetailTableViewSections.Exercises.rawValue {
            let dataSection = section - 1
            return fetchedExercisesController!.sections![dataSection].numberOfObjects
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let dataIndexPath = NSIndexPath(forRow: indexPath.row, inSection: (indexPath.section - 1) )
        return self.tableView(tableView, exerciseCellForRowAtIndexPath: dataIndexPath) as UITableViewCell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == WorkoutDetailTableViewSections.Exercises.rawValue {
            return extraWorkoutInfoHeaderView.height
        }
        return CGFloat.min
    }
    
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == WorkoutDetailTableViewSections.Exercises.rawValue {
            return extraWorkoutInfoHeaderView
        }
        return nil
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if self.tableView(tableView, shouldHaveFooterForSection: section) == true {
            return 12.0
        }
        
        return CGFloat.min
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if self.tableView(tableView, shouldHaveFooterForSection: section) == true {
            
            var footerView: UIView?
            
            if let ex = self.exerciseAtTableViewSection(section) {
                var sectionFooterView: UITableViewHeaderFooterView?
                switch ex.visualPositionInSuperset() {
                case .None, .Bottom:
                    let sectionRestTimeFooterView = self.tableView.dequeueReusableHeaderFooterViewWithIdentifier(kRestTimeFooterIdentifier)
                    assert(sectionRestTimeFooterView != nil)
                    
                    sectionFooterView = sectionRestTimeFooterView
                case .Top, .Middle:
                    let sectionSupersetFooterView = self.tableView.dequeueReusableHeaderFooterViewWithIdentifier(kSupersetFooterIdentifier)
                    assert(sectionSupersetFooterView != nil)
                    
                    // custom setup
                    if sectionSupersetFooterView!.tag == 0 {
                        sectionSupersetFooterView?.textLabel?.textColor = UIColor.fasYellowColor()
                        sectionSupersetFooterView!.tag = 1
                    }
                    
                    sectionFooterView = sectionSupersetFooterView
                }
                
                // Display specific header title
                sectionFooterView!.textLabel?.text = self.tableView(self.tableView, titleForFooterInSection: section)
                
                footerView = sectionFooterView
            }
            
            return footerView
        }
        
        return nil
    }
    
    func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if self.tableView(tableView, shouldHaveFooterForSection: section) == true {
            if let ex = self.exerciseAtTableViewSection(section) {
                switch ex.visualPositionInSuperset() {
                case .None, .Bottom:
                    // TODO: localize this
                    return "rest \(NSString.formattedTimeFromSeconds(ex.restTime))"
                case .Top, .Middle:
                    // TODO: localize this
                    return "superset"
                }
            }
        }
        
        return nil
    }
    
    // MARK: - Helpers
    
    private func tableView(tableView: UITableView, shouldHaveFooterForSection section: Int) -> Bool {
        // for all sections, but the first one - add a footer
        if section > WorkoutDetailTableViewSections.Dump.rawValue {
            return true
        }
        
        return false
    }
    
    private func exerciseAtTableViewSection(section: Int) -> Exercise? {
        let dataSection = section - 1
        if let frc = fetchedExercisesController {
            if let sectionInfo: NSFetchedResultsSectionInfo = frc.sections?[dataSection] where sectionInfo.objects?.count != 0 {
                
                assert(sectionInfo.objects!.count==1) // there is presumably only one exercise object per section
                
                if let ex = sectionInfo.objects![0] as? Exercise {
                    return ex
                }
            }
        }
        
        return nil
    }
    
    private func configureExerciseCell(exerciseCell: FASExerciseCell, atIndexPath indexPath: NSIndexPath) {
        let exerciseObject = fetchedExercisesController!.objectAtIndexPath(indexPath) as! Exercise
//        exerciseCell.accessoryType = showExerciseLibrary ? .DetailButton : .None
        exerciseCell.accessoryView = showExerciseLibrary ? UIImageView(image: UIImage(named: "accessoryChevron")) : nil
        exerciseCell.exercise = exerciseObject
    }
    
    private func tableView(tableView: UITableView, exerciseCellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let anExerciseCell = tableView.dequeueReusableCellWithIdentifier(kExerciseCelllIdentifier, forIndexPath: indexPath) as! FASExerciseCell
        configureExerciseCell(anExerciseCell, atIndexPath: indexPath)
        return anExerciseCell
    }
}



extension WorkoutDetailViewController: FASDataObserver {
    func fasDataNotification(notification: NSNotification) {
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            // returns if invalid workout
            if self.detailWorkout == nil {
                return
            }
            
            if notification.name == FASDataExercisesDidUpdateNotification
                || notification.name == FASDataExercisesWillUpdateNotification
                || notification.name == FASDataExercisesUpdateDidFailNotification {
                    
                    if let userInfo = notification.userInfo, workoutObjectID = userInfo["workoutObjectID"] as? NSManagedObjectID
                        where workoutObjectID === self.detailWorkout?.objectID {
                            // now, are are confident the notification is what we've been waiting for
                            switch notification.name {
                            case FASDataExercisesWillUpdateNotification:
                                printlnDebugTrace("will update exercises")
                            case FASDataExercisesDidUpdateNotification:
                                
                                
                                if self.detailWorkout != nil {
                                    // info view might be nil when first assigning workout object
                                    
                                    let newWork = userInfo["workout"] as! Workout
                                    print(newWork.currentAllTime)
                                    self.detailWorkout?.currentAllTime = newWork.currentAllTime
                                    
                                        self.configureView()
                                }
                                
                                
                                printlnDebugTrace("did update exercises")
                            case FASDataExercisesUpdateDidFailNotification:
                                printlnDebugTrace("did fail to exercises")
                            default:
                                printlnDebugWarning("unknown notification \(notification.name)")
                            }
                    }
            }
        }
    }
}
///////////////////////////////////////////////
// MARK: - UIAlertViewDelegate -
///////////////////////////////////////////////

// TODO: replace with UIAlertController
extension WorkoutDetailViewController: UIAlertViewDelegate {
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if WorkoutDetailViewControllerAlertViewTag(rawValue: alertView.tag) == WorkoutDetailViewControllerAlertViewTag.DownloadFilesPrompt {
            switch buttonIndex {
            case 1: // Download
                let array = FASData.sharedData().arrayOfAttachIDStartedDownloadForWorkout(detailWorkout!)
                
                if let attachIDs = array as? [NSNumber] {
                    workoutDownloadTracker!.startTrackProgress(attachIDs)
                    
                    // Update Workout Download Status
                    let c = FASData.sharedData().mainContext
                    if let objectID = self.detailWorkout?.objectID {
                        c.performBlockAndWait({ () -> Void in
                            do {
                                let object = try c.existingObjectWithID(objectID)
                                if let workout = object as? Workout {
                                    workout.setDownloadStatus(.Downloading)
                                    // TODO: save it when assigning is happening in FASData
                                }
                            } catch let someError {
                                print("\(someError) is is occured")
                            }
                        })
                    }
                }
                
            default: break
            }
        }
        
    }
}

///////////////////////////////////////////////
// MARK: - FASWorkoutViewControllerDelegate -
///////////////////////////////////////////////

extension WorkoutDetailViewController: FASWorkoutViewControllerDelegate {
    func workoutViewController(controller: FASWorkoutViewController, didFinishWithStatus status: FASWorkoutViewControllerStatus)
    {
        typealias ViewControllerAnimatedCompletion = () -> Void
        var completion: ViewControllerAnimatedCompletion?
        completion = {
            () -> Void in
            if status == .WorkoutCompleted {
                // engage first, and if NO Rate Us Prompt, conditionally present Notification Prompt
                if DUBApptentiveDeferredEngagement.engage(kFASApptentiveEventAfterDidFinishWorkout, fromViewController: self) == false {
                    self.conditionallyPromptUserToTurnOnNotifications()
                }
            }
        }
        
        if let _ = controller.navigationController {
            controller.navigationController!.dismissViewControllerAnimated(true, completion: completion)
        }
        else {
            controller.dismissViewControllerAnimated(true, completion: completion)
        }
    }
    
    private func conditionallyPromptUserToTurnOnNotifications() {
        if FASAppPreferences.sharedInstance().hasPromptedNotificationSettingsChange == true {
            return; // do not show again
        }
        
        let alertController = UIAlertController(title: "Turn on notifications", message: "Be always fit. Let us notify you on new trainings.", preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Don't Allow", style: .Cancel) { (action) in
            // ...
        }
        
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            self.presentNotificationsSettingsViewController()
        }
        alertController.addAction(OKAction)
        
        self.presentViewController(alertController, animated: true) {
            FASAppPreferences.sharedInstance().hasPromptedNotificationSettingsChange = true
        }
    }
}