//
//  UBIDataSource.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 2/20/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "UBIDataSource.h"

NSString * const kUBIDataSourceTitle             =   @"title";
NSString * const kUBIDataSourceSubtitle          =   @"subtitle";
NSString * const kUBIDataSourceCellIdentifier    =   @"cellID";
NSString * const kUBIDataSourceItems             =   @"items";
NSString * const kUBIDataSourceItemID            =   @"itemID";
NSString * const kUBIDataSourceItemInfo          =   @"itemInfo";
