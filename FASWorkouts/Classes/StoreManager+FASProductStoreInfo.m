//
//  StoreManager+FASProductStoreInfo.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/24/16.
//  Copyright © 2016 FAS. All rights reserved.
//

#import "StoreManager+FASProductStoreInfo.h"

@implementation StoreManager (FASProductStoreInfo)

- (NSString *)productPrice:(id <FASProduct>)product
{
    return [[StoreManager sharedInstance] priceMatchingProductIdentifier:[product productIdentifier]];
}

- (NSString *)productTitle:(id <FASProduct>)product
{
    return [[StoreManager sharedInstance] titleMatchingProductIdentifier:[product productIdentifier]];
}

- (NSString *)productDescrition:(id <FASProduct>)product
{
    return [[StoreManager sharedInstance] descriptionMatchingProductIdentifier:[product productIdentifier]];
}

- (SKProduct *)skProduct:(id <FASProduct>)product
{
    return [[StoreManager sharedInstance] productMatchingProductIdentifier:[product productIdentifier]];
}

@end
