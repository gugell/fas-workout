//
//  FASExercisesRunList.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/12/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "FASExercisesRunList.h"

// entity
NSString* const kFASExercisesRunListEntity = @"fas_workout_exercise";

// fields
NSString* const kFASExercisesRunListIDFieldKey                  = @"ID";
NSString* const kFASExercisesRunListNameFieldKey                = @"exerciseID.name";
NSString* const kFASExercisesRunListCodeFieldKey                = @"exerciseID.code";
NSString* const kFASExercisesRunListMuscleTypesFieldKey         = @"exerciseID.muscleTypes.name";
NSString* const kFASExercisesRunListRestTimeFieldKey            = @"restTime";
NSString* const kFASExercisesRunListTimeSpanFieldKey            = @"timeSpan";
NSString* const kFASExercisesRunListLapsNumberFieldKey          = @"lapsNumber";
NSString* const kFASExercisesRunListWorkoutIDFieldKey           = @"workoutID";
NSString* const kFASExercisesRunListExerciseIDFieldKey          = @"exerciseID";
NSString* const kFASExercisesRunListIsStaticFieldKey            = @"exerciseID.isStatic";
NSString* const kFASExercisesRunListModifyDateFieldKey          = @"exerciseID.mi_modifyDate";
NSString* const kFASExercisesRunListSortIndexFieldKey           = @"sortIndex";
NSString* const kFASExercisesRunListDescriptionFieldKey         = @"exerciseID.description";
NSString* const kFASExercisesRunListPositionInSupersetFieldKey  = @"positionInSuperset";
NSString* const kFASExercisesRunListLinkToImageFieldKey         = @"exerciseID.linkToPhoto";
NSString* const kFASExercisesRunListLinkToIconFieldKey          = @"exerciseID.linkToIcon";

@implementation FASExercisesRunList

- (instancetype)initWithFieldList:(NSArray *)fieldList
{
    return [super initWithFieldList:fieldList entity:kFASExercisesRunListEntity];
}

- (instancetype)init {
    return [self initWithFieldList:@[kFASExercisesRunListIDFieldKey,
                                     kFASExercisesRunListNameFieldKey,
                                     kFASExercisesRunListCodeFieldKey,
                                     kFASExercisesRunListMuscleTypesFieldKey,
                                     kFASExercisesRunListRestTimeFieldKey,
                                     kFASExercisesRunListTimeSpanFieldKey,
                                     kFASExercisesRunListLapsNumberFieldKey,
                                     kFASExercisesRunListWorkoutIDFieldKey,
                                     kFASExercisesRunListExerciseIDFieldKey,
                                     kFASExercisesRunListIsStaticFieldKey,
                                     kFASExercisesRunListModifyDateFieldKey,
                                     kFASExercisesRunListSortIndexFieldKey,
                                     kFASExercisesRunListPositionInSupersetFieldKey,
                                     kFASExercisesRunListDescriptionFieldKey,
                                     kFASExercisesRunListLinkToImageFieldKey,
                                     kFASExercisesRunListLinkToIconFieldKey
                                     /*,@"ID"*/]];
}

- (instancetype)initWithWorkoutID:(NSNumber *)workoutID
{
    self = [self init];
    NSString* strWorkoutID = [NSString stringWithFormat:@"%@", workoutID];
    [self.whereList addItem:@"ExcercisesForWorkout" condition:UBIRunListWhereListConditionEqual value:strWorkoutID key:kFASExercisesRunListWorkoutIDFieldKey];
    return self;
}

+ (instancetype)exercisesRunListWithWorkoutID:(NSNumber *)workoutID
{
    return [[FASExercisesRunList alloc] initWithWorkoutID:workoutID];
}
@end
