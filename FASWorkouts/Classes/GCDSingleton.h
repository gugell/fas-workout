//
//  GCDSingleton.h
//
//  Macros to simplify creating of a sharedInstance for a class using GCD (ARC compliant)
//
//  Use one of the following methods to create a sharedInstance
//

//
//  METHOD #1: use this if you want the sharedInstance method to be 'sharedClassName'
//
//
//  In MyClass.h
//
//    @interface MyClass
//    + (id)sharedMyClass;
//    @end
//
//  In MyClass.m
//
//    @implementation MySharedThing
//    DEFINE_SHARED_INSTANCE_FOR_CLASS(MySharedThing)
//    // rest of implementation
//    @end

#define DEFINE_SHARED_INSTANCE_FOR_CLASS(className) \
+ (id)shared##className \
{ \
static dispatch_once_t pred = 0; \
__strong static id _sharedObject = nil; \
dispatch_once(&pred, ^{ \
_sharedObject = ^{return [self new];}(); \
}); \
return _sharedObject; \
} \


//
// METHOD #2: use this if you want to name the shared instance method yourself
//
//  In MyClass.h
//
//    @interface MyClass
//    + (id)mySharedInstance;
//    @end
//
//
//  In MyClass.m
//    @implementation MyClass
//
//    + (id)mySharedInstance
//    {
//        DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
//            return [self new];
//        });
//    }
//
//    // rest of implementation
//    @end
//

#define DEFINE_SHARED_INSTANCE_USING_BLOCK(block) \
static dispatch_once_t pred = 0; \
__strong static id _sharedObject = nil; \
dispatch_once(&pred, ^{ \
_sharedObject = block(); \
}); \
return _sharedObject; \
