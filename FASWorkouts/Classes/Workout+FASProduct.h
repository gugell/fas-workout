//
//  Workout+FASProduct.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/24/16.
//  Copyright © 2016 FAS. All rights reserved.
//

#import "Workout.h"
#import "FASProductProtocol.h"

@interface Workout (FASProduct) <FASProduct>
@end
