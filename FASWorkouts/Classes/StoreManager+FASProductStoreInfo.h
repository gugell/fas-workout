//
//  StoreManager+FASProductStoreInfo.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/24/16.
//  Copyright © 2016 FAS. All rights reserved.
//

#import "StoreManager.h"
#import "FASProductProtocol.h"

@interface StoreManager (FASProductStoreInfo)
- (NSString *)productPrice:(id <FASProduct>)product;
- (NSString *)productTitle:(id <FASProduct>)product;
- (NSString *)productDescrition:(id <FASProduct>)product;
- (SKProduct *)skProduct:(id <FASProduct>)product;
@end
