//
//  FASAttachDownloadProgreesTracker.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/4/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import Foundation
import CoreData

typealias FASAttachDownloadProgreesTrackerCompletion = (Float,Bool) -> ()

class FASAttachDownloadProgreesTracker {

    private var attachIDs: [NSNumber]?
    private let managedObjectContest: NSManagedObjectContext
    private let notificationCenter = NSNotificationCenter.defaultCenter()
    private var completion: FASAttachDownloadProgreesTrackerCompletion
    private var progressMap: [NSNumber: NSNumber] = [:]
    
    init(context: NSManagedObjectContext?, completionClosure: FASAttachDownloadProgreesTrackerCompletion) {
        // Note: this context must be the same context attach objects' downloadProgress property is changed in
        managedObjectContest = context ?? FASData.sharedData().mainContext
        completion = completionClosure
        observerChangeNotifications(managedObjectContest)
    }
    
    deinit {
        stopObservingChangeNotifications(managedObjectContest)
    }
    
    // @return true if started w/o error, false - otherwise
    func startTrackProgress(attachIDs: [NSNumber]) {
        self.attachIDs = attachIDs
        configureProgressMap()
    }
    
    func stop() {
        self.attachIDs = nil;
        clearProgressMap()
    }
    
    private func clearProgressMap() {
        progressMap.removeAll(keepCapacity: false)
    }
    
    private func configureProgressMap() {
        for attachID: NSNumber in self.attachIDs! {
            attach(attachID, madeProgress:NSNumber(floatLiteral: 0.0))
        }
    }
    
    private func attach(attachID: NSNumber, madeProgress preogress: NSNumber) {
        progressMap.updateValue(preogress, forKey: attachID)
        calculateTotalProgress()
    }
    
    private func calculateTotalProgress() {
        var sum: Float = 0.0
        for progressNumber in progressMap.values {
            sum += progressNumber.floatValue
        }
        
        let totalProgress = sum / Float(progressMap.count)
        let isCompleted = (totalProgress == 1.0 ? true : false)
        self.completion(totalProgress, isCompleted)
    }
    
    private func observerChangeNotifications(context: NSManagedObjectContext) {
        notificationCenter.addObserver(self, selector: "contextDidChange:", name: NSManagedObjectContextObjectsDidChangeNotification, object: context)
    }
    
    private func stopObservingChangeNotifications(context: NSManagedObjectContext) {
        notificationCenter.removeObserver(self, name: NSManagedObjectContextObjectsDidChangeNotification, object: context)
    }
    
    @objc func contextDidChange(notification: NSNotification) {
        
        if let unwrappedAttachIDs = attachIDs where unwrappedAttachIDs.count != 0 {
            
            if notification.name == NSManagedObjectContextObjectsDidChangeNotification {
                
                assert(NSThread.currentThread() == NSThread.mainThread()) // for DEBUG only
                
                if let nui = notification.userInfo, updatedObjects: NSSet = nui[NSUpdatedObjectsKey] as? NSSet {
                    updatedObjects.enumerateObjectsUsingBlock({ (obj, stop) -> Void in
                        if let attachObj: Attach = obj as? Attach {
                            
                            for attachID: NSNumber in unwrappedAttachIDs {
                                if attachObj.attach_id.compare(attachID) == .OrderedSame {
                                    self.attach(attachID, madeProgress: attachObj.downloadProgress)
                                    break
                                }
                            }
                            
                        }
                    })
                }
            }
            
        }
    }
}