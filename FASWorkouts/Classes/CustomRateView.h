//
//  CustomRateView.h
//  FASWorkouts
//
//  Created by Vladimir Stepanchenko on 3/15/15.
//  Copyright (c) 2015 Vladimir Stepanchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomRateViewDelegate <NSObject>
@optional
-(void)valueChanged:(NSUInteger)value;

@end

@interface CustomRateView : UIView

@property (assign, nonatomic) id<CustomRateViewDelegate> delegate;

@property(nonatomic, retain) UIColor *textColor;
@property(nonatomic, retain) UIColor *highLightedTextColor;

@end
