//
//  WorkoutsListViewController.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/7/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

// cell identifiers
enum WorkoutsListViewControllerCellIdentifier: String {
    case Workout = "WorkoutCellIdentifier"
}

// segue identifiers
enum WorkoutsListViewControllerSegueIdentifier: String {
    case ShowWorkoutDetails = "showWorkoutDetail"
}

// SegmentedControl indexes
enum DifficultySegmentedControlIndex: Int, CustomStringConvertible {
    case Beginner, Intermediate, Advanced
    
    var description : String {
        switch self {
        case .Beginner:
            return "Beginner"
        case .Intermediate:
            return "Intermediate"
        case .Advanced:
            return "Advanced"
        }
    }
}

enum TrainingTypeSegmentedControlIndex: Int, CustomStringConvertible {
    
    case Functional, Tabata,Favourite
    
    var description : String {
        switch self {
        case .Functional:
            return "Functional"
        case .Tabata:
            return "Tabata"
        case .Favourite:
        return "Favourite"
    }
    }
}

class WorkoutsListViewController: WorkoutsViewController {
    
    private var workoutDetailViewController: WorkoutDetailViewController?
    private var loginInfoViewController: LoginInfoViewController?
    
    // Read notes about nav bar hidding below to know more why this is commented
//    private var lastKnownContentOffset: CGFloat = 0.0
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedNavigationBar: FASSegmentedNavigationBar!
    @IBOutlet weak var trainingTypeSegmentedControl: HMSegmentedControl!
    @IBOutlet weak var difficultySegmentedControl: UISegmentedControl!
    
    ///////////////////////////////////////////////
    // MARK: - Init -
    ///////////////////////////////////////////////
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    ///////////////////////////////////////////////
    // MARK: - UIViewController overrides -
    ///////////////////////////////////////////////
    
    override func loadView() {
        super.loadView()
        
        
        
        
        trainingTypeSegmentedControl.type = HMSegmentedControlTypeTextImages
        trainingTypeSegmentedControl.selectionStyle = HMSegmentedControlSelectionStyleBox
        trainingTypeSegmentedControl.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleFixed
        trainingTypeSegmentedControl.addTarget(self,
            action: "filterSegmentedControlDidChangeValue:",
            forControlEvents: .ValueChanged)
        trainingTypeSegmentedControl.selectionIndicatorHeight = 2.0 // высота полоски желтой - 4 пикселя
        trainingTypeSegmentedControl.backgroundColor = UIColor.fasBlackColor() // цвет бека - 252525
        trainingTypeSegmentedControl.selectionIndicatorColor =  UIColor.yellowColor() // цвет полоски f8e401
        trainingTypeSegmentedControl.selectionBoxColor = UIColor.colorWithComponents(red: 30.0, green: 30.0, blue: 30.0, alpha: 1.0)// цвет активного - 1e1e1e
        trainingTypeSegmentedControl.selectionIndicatorBoxOpacity = 1.0
        trainingTypeSegmentedControl.titleTextAttributes = [
            NSForegroundColorAttributeName:UIColor.colorWithComponents(red: 103.0, green: 103.0, blue: 103.0, alpha: 1.0), // неактивный шрифт 676767
            NSFontAttributeName: UIFont.boldBabasFontOfSize(22.0)]
        trainingTypeSegmentedControl.selectedTitleTextAttributes = [
            NSForegroundColorAttributeName: UIColor.yellowColor(), // активный шрифт f8e401
            NSFontAttributeName: UIFont.boldBabasFontOfSize(22.0)]
        trainingTypeSegmentedControl.selectedSegmentIndex = 0
        trainingTypeSegmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown
        trainingTypeSegmentedControl.shouldAnimateUserSelection = false
        trainingTypeSegmentedControl.sectionTitles = ["\(TrainingTypeSegmentedControlIndex.Functional)", "\(TrainingTypeSegmentedControlIndex.Tabata)","\(TrainingTypeSegmentedControlIndex.Favourite)"]
        
        trainingTypeSegmentedControl.sectionImages = [UIImage(named: "timer_disable_icon")!, UIImage(named: "flash_disable_icon")!, UIImage(named: "flash_disable_icon")!]
        trainingTypeSegmentedControl.sectionSelectedImages = [UIImage(named: "timer_active_icon")!, UIImage(named: "flash_active_icon")!, UIImage(named: "flash_active_icon")!]
        
        
        self.navigationItem.title = TrainingTypeSegmentedControlIndex.Functional.description;
        
       /* __weak typeof(self) weakSelf = self;
        [self.segmentedControl4 setIndexChangeBlock:^(NSInteger index) {
            [weakSelf.scrollView scrollRectToVisible:CGRectMake(viewWidth * index, 0, viewWidth, 200) animated:YES];
            }];*/
        
        
        trainingTypeSegmentedControl.indexChangeBlock = {(index) in
 
            
              var aFilter: [String : AnyObject] = self.currentFilter
            
                if let ttsci = TrainingTypeSegmentedControlIndex(rawValue: index) {
                    switch ttsci {
                    case TrainingTypeSegmentedControlIndex.Functional:
                        aFilter["trainingType"] = FASWorkoutTrainingTypeFunctional
                    case TrainingTypeSegmentedControlIndex.Tabata:
                        aFilter["trainingType"] = FASWorkoutTrainingTypeTabata
                    case TrainingTypeSegmentedControlIndex.Favourite:
                        aFilter["trainingType"] = FASWorkoutTrainingTypeFavourite
                    }
                }

            
            self.currentFilter = aFilter
            
            // we have loaded all workouts before, so FRC' delegate method
            // won't be called until we change the model
            self.tableView.reloadData()
        
        };
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let settingsButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "settingsButton"), style: .Plain, target: self, action: "settingsButtonDidPress")
        navigationItem.rightBarButtonItem = settingsButton
//#if DEBUG_SETTINGS
//        let leftBarButton: UIBarButtonItem = UIBarButtonItem(title: "Refresh", style: .Bordered, target: self, action: "leftBarButtonDidPress")
//        navigationItem.leftBarButtonItem = leftBarButton
//#endif
        tableView.estimatedRowHeight = 91.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.backgroundColor = UIColor.fasGrayColor41();
        workoutDetailViewController = (splitViewController?.viewControllers.last as! UINavigationController).topViewController as? WorkoutDetailViewController
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if !FASAPISession.currentSession().isLoggedIn() {
            presentLoginInfoViewController(false, completion: { ()->Void in
                self.removeSplash()
            })
        }
        else {
           removeSplash()
        }
    }
    
    override func viewDidAppearForTheFirstTime() {
        super.viewDidAppearForTheFirstTime()
        // after -viewDidLayoutSubviews the tableView scroll 
        // is half-cell below the top, so we force it to scroll to top only once
        tableView.scrollToTopAnimated(false)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let topInset = topLayoutGuide.length + segmentedNavigationBar.frame.height
        
        var tableInset: UIEdgeInsets = tableView.contentInset
        tableInset.top = topInset
        tableView.contentInset = tableInset
        tableView.scrollIndicatorInsets = tableInset
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar(.Dark)
        
        // deselect selected row on iPhone
        if let tv = self.tableView where FASData.sharedData().isIPHONE == true {
            self.tableView(tv, deselectSelectedRowAnimated: false)
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        configureNavigationBar(.DarkWithYellowTitle)
        super.viewWillDisappear(animated)
    }
    ///////////////////////////////////////////////
    // MARK: Actions
    ///////////////////////////////////////////////
    private func removeSplash() {
        if let appDelegate = UIApplication.sharedApplication().delegate as? FASAppDelegate {
            appDelegate.removeSplashView()
        }
    }
    
    func settingsButtonDidPress() {
        FASMenuViewController.presentFromViewController(self)
    }
    
    func leftBarButtonDidPress() {
        FASData.sharedData().loadWorkoutsFromRemote()
    }

    @IBAction func filterSegmentedControlDidChangeValue(sender: AnyObject) {
        // start with current filter
        var aFilter: [String : AnyObject] = currentFilter
        
        if sender === difficultySegmentedControl {
            if let dsci = DifficultySegmentedControlIndex(rawValue: difficultySegmentedControl.selectedSegmentIndex) {
                switch dsci {
                case DifficultySegmentedControlIndex.Beginner:
                    aFilter["difficulty"] = FASWorkoutDifficultyBeginner
                case DifficultySegmentedControlIndex.Intermediate:
                    aFilter["difficulty"] = FASWorkoutDifficultyIntermediate
                case DifficultySegmentedControlIndex.Advanced:
                    aFilter["difficulty"] = FASWorkoutDifficultyAdvanced
                }
            }
        }
        
        if sender === trainingTypeSegmentedControl {
            if let ttsci = TrainingTypeSegmentedControlIndex(rawValue: trainingTypeSegmentedControl.selectedSegmentIndex) {
                
                var title = FASWorkoutTrainingTypeFunctional;
                
                switch ttsci {
                case TrainingTypeSegmentedControlIndex.Functional:
                    aFilter["trainingType"] = FASWorkoutTrainingTypeFunctional
                case TrainingTypeSegmentedControlIndex.Tabata:
                    aFilter["trainingType"] = FASWorkoutTrainingTypeTabata
                    title = FASWorkoutTrainingTypeTabata;
                case TrainingTypeSegmentedControlIndex.Favourite:
                    aFilter["trainingType"] = FASWorkoutTrainingTypeFavourite
                    title = FASWorkoutTrainingTypeFavourite;
                }
                
                
                
                self.navigationItem.title  = title.uppercaseString;
            }
        }

        currentFilter = aFilter
        
        // we have loaded all workouts before, so FRC' delegate method
        // won't be called until we change the model
        self.tableView.reloadData()
    }
    
    private func showWelcomeVC(allowResume: Bool) {
        let welcomeVC = WelcomeViewController()
        welcomeVC.delegate = self
        welcomeVC.allowResume = allowResume
        let navWelcome: UINavigationController? = UINavigationController(navigationBarClass: FASNavigationBar.self, toolbarClass: UIToolbar.self)
        navWelcome!.viewControllers = [welcomeVC]
        self.presentViewController(navWelcome!, animated: false, completion: nil)
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showWorkoutDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                if let object = fetchedWorkoutsController?.objectAtIndexPath(indexPath) as? Workout{
                    if let controller = (segue.destinationViewController as! UINavigationController).topViewController as? WorkoutDetailViewController {
                        controller.detailWorkout = object
                        // TODO: uncomment when you need to allow "expand detail view" controls for concrete workout
//                        controller.navigationItem.leftBarButtonItem = splitViewController!.displayModeButtonItem();
//                        controller.navigationItem.leftItemsSupplementBackButton = true;
                    }
                }
            }
        }
    }
    
    private func presentLoginInfoViewController(animated: Bool, completion: (()->Void)? = nil) {
        loginInfoViewController = LoginInfoViewController(sessionUIDelegate: self)
        if let vc = loginInfoViewController {
            vc.firstTimeUX = true
            vc.hidesSkipButton = true
            let nav: UINavigationController? = UINavigationController(navigationBarClass: FASNavigationBar.self, toolbarClass: UIToolbar.self)
            if let nav = nav {
                nav.viewControllers = [vc]
                presentViewController(nav, animated: animated, completion: completion)
            }
        }
    }
    
    private func finishNewUserFlow() {
        if !FASAPISession.currentSession().isLoggedInAsUser() {
            printlnDebugWarning("not logged is as user at New User Flow finish")
        }
    }
}
///////////////////////////////////////////////
// MARK: - TableView DataSource / Delegate -
///////////////////////////////////////////////
extension WorkoutsListViewController: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return fetchedWorkoutsController!.sections!.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedWorkoutsController!.sections![section].numberOfObjects
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return self.tableView(tableView, workoutCellForRowAtIndexPath: indexPath) as UITableViewCell
    }
    
    // MARK: - Helpers
    private func configureWorkoutCell(workoutCell: FASWorkoutCell, atIndexPath indexPath: NSIndexPath) {
        let workoutObject = fetchedWorkoutsController!.objectAtIndexPath(indexPath) as! Workout
        workoutCell.workout = workoutObject
    }
    
    private func tableView(tableView: UITableView, workoutCellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let aWorkoutCell = tableView.dequeueReusableCellWithIdentifier(WorkoutsListViewControllerCellIdentifier.Workout.rawValue, forIndexPath: indexPath) as! FASWorkoutCell
        configureWorkoutCell(aWorkoutCell, atIndexPath: indexPath)
        return aWorkoutCell
    }
}

extension WorkoutsListViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.clearColor()
    }
    
    // MARK: TableView Helpers
    
    private func tableView(tv: UITableView, deselectSelectedRowAnimated animated: Bool) {
        if let indexPath = tv.indexPathForSelectedRow {
            tv.deselectRowAtIndexPath(indexPath, animated: animated)
        }
    }
}
///////////////////////////////////////////////
// MARK: - NSFetchedResultsControllerDelegate -
///////////////////////////////////////////////
extension WorkoutsListViewController  {
    /*
    override func controllerWillChangeContent(controller: NSFetchedResultsController) {
        tableView.beginUpdates()
    }
    
    override func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        switch type {
        case .Insert:
            tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Middle)
        case .Delete:
            tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Middle)
        default:
            0
        }
    }
    
    override func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch type {
        case .Insert:
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Middle)
        case .Delete:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Middle)
        case .Update:
            let wrkCell: FASWorkoutCell = self.tableView.cellForRowAtIndexPath(indexPath!) as FASWorkoutCell
            configureWorkoutCell(wrkCell, atIndexPath: indexPath!)
        case .Move:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation:.Middle)
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation:.Middle)
        }
    }
    
    override func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.endUpdates()
    }
    */
    // Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed.
    
    override func controllerDidChangeContent(controller: NSFetchedResultsController) {
        super.controllerDidChangeContent(controller)
        
        // In the simplest, most efficient, case, reload the table view.
        tableView.reloadData()
        
        // select first row upon loading
//        if UIDevice.currentDevice().userInterfaceIdiom == .Pad {
//            selectFirstRow(tableView: tableView, segueIdentifier: WorkoutsListViewControllerSegueIdentifier.ShowWorkoutDetails.rawValue)
//        }
    }

}
///////////////////////////////////////////////
// MARK: - WelcomeViewControllerDelegate -
///////////////////////////////////////////////
extension WorkoutsListViewController: WelcomeViewControllerDelegate {
    
    func welcomeViewController(viewController: WelcomeViewController, didFinishWithOption option: WelcomeViewControllerFinishOptions)
    {
        // TODO: look at the option parameter and do appropriate actions for it
        viewController.dismissViewControllerAnimated(true, completion: nil)
    }
}
///////////////////////////////////////////////
// MARK: - ScrollView Delegate -
///////////////////////////////////////////////
// The code below is for 'manual' navigation bar hidding
/*
extension WorkoutsListViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if (view.window == nil) {
            return
        }
        
        let distanceScrolled = scrollView.contentOffset.y - lastKnownContentOffset
        let distanceFromBottom = scrollView.contentSize.height - scrollView.contentOffset.y - scrollView.bounds.size.height + scrollView.contentInset.bottom
        
//        printlnDebugTrace("distanceScrolled = \(distanceScrolled)\tdistanceFromBottom = \(distanceFromBottom)")
        // Hide the navigation bar if scrolling down via dragging
        if scrollView.dragging &&
            scrollView.contentOffset.y > 0.0 &&
//                distanceFromBottom > 108.0 &&
                    distanceScrolled > 5.0 {
            self.setBarsHidden(true, animated: true)
        }
        // Show the navigation bar if scrolling up via dragging
        else if scrollView.dragging &&
            distanceFromBottom < 0.0 &&
                distanceScrolled < -5.0 {
            self.setBarsHidden(false, animated: true)
        }
        
        lastKnownContentOffset = scrollView.contentOffset.y
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        lastKnownContentOffset = scrollView.contentOffset.y
    }
}
*/
///////////////////////////////////////////////
// MARK: - FASDataObserver -
///////////////////////////////////////////////
extension WorkoutsListViewController {
    override func fasDataNotification(notification: NSNotification) {
        
        super.fasDataNotification(notification)
        
        printlnDebug("[TRACE]: WorkoutsListViewController received FASData notification. Notification was:\n\(notification)");
    }
}
///////////////////////////////////////////////
// MARK: - SessionUIDelegate -
///////////////////////////////////////////////
extension WorkoutsListViewController: SessionUIDelegate {
    func sessionUIDidFinish(sessionUIController: SessionUIController) {
        loginInfoViewController!.dismissViewControllerAnimated(true, completion: {
            self.finishNewUserFlow()
        })
    }
    
    func sessionUIDidCancel(sessionUIController: SessionUIController) {
        loginInfoViewController!.dismissViewControllerAnimated(true, completion: {
            self.finishNewUserFlow()
        })
    }
}
