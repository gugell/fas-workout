//
//  FASWorkoutDataTypes.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/16/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#ifndef FASWorkouts_FASWorkoutDataTypes_h
#define FASWorkouts_FASWorkoutDataTypes_h

///////////////////////////////////////////////
#pragma mark -
#pragma mark Workout Gender-Specific Type
#pragma mark -
///////////////////////////////////////////////
extern NSString * const FASWorkoutGenderSpecificTypeAllRawValue;
extern NSString * const FASWorkoutGenderSpecificTypeMenRawValue;
extern NSString * const FASWorkoutGenderSpecificTypeWomenRawValue;

typedef NS_ENUM(NSUInteger, FASWorkoutGenderSpecificType) {
    FASWorkoutGenderSpecificTypeAll = 0,
    FASWorkoutGenderSpecificTypeMen,
    FASWorkoutGenderSpecificTypeWomen
};

///////////////////////////////////////////////
#pragma mark -
#pragma mark Workout Exercise Position In Superset Type
#pragma mark -
///////////////////////////////////////////////
extern NSString * const FASWorkoutPositionInSupersetTypeTopRawValue;
extern NSString * const FASWorkoutPositionInSupersetTypeMiddleRawValue;
extern NSString * const FASWorkoutPositionInSupersetTypeBottomRawValue;

typedef NS_ENUM(NSUInteger, FASWorkoutPositionInSupersetType) {
    FASWorkoutPositionInSupersetTypeNone = 0,
    FASWorkoutPositionInSupersetTypeTop,
    FASWorkoutPositionInSupersetTypeMiddle,
    FASWorkoutPositionInSupersetTypeBottom,
};

///////////////////////////////////////////////
#pragma mark -
#pragma mark Workout User Rate
#pragma mark -
///////////////////////////////////////////////
typedef NS_ENUM(NSUInteger, FASWorkoutUserRate) {
    FASWorkoutUserRatingEasy,
    FASWorkoutUserRatingNormal,
    FASWorkoutUserRatingHard,
    FASWorkoutUserRatingUndefined = 999,
};

///////////////////////////////////////////////
#pragma mark -
#pragma mark Workout Download Status
#pragma mark -
///////////////////////////////////////////////
typedef NS_ENUM(NSUInteger, WorkoutDownloadStatus) {
    WorkoutDownloadStatusNone,
    WorkoutDownloadStatusDownloading,
};

#endif
