//
//  ContainerViewController.m
//  EmbeddedSwapping
//
//  Created by Michael Luton on 11/13/12.
//  Copyright (c) 2012 Sandmoose Software. All rights reserved.
//  Heavily inspired by http://orderoo.wordpress.com/2012/02/23/container-view-controllers-in-the-storyboard/
//

#import "ContainerViewController.h"
#import "FASWorkouts-Swift.h"

static NSString * const kViewControllersDataTagPrefKey = @"kVCDTag";
static NSString * const kViewControllersDataViewControllerPrefKey = @"kVCDViewController";
static NSString * const kViewControllersDataSegueIdentifierPrefKey = @"kVCDIdentifier";

@interface ContainerViewController ()

@property (strong, nonatomic) NSArray* viewControllersData;
@property (assign, nonatomic) BOOL transitionInProgress;
@property (nonatomic) ContainerVCTag currentVCTag;

@end

@implementation ContainerViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self configure];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self configure];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self configure];
    }
    return self;
}

- (void)configure
{
    _currentVCTag = -1;
    _viewControllersData = @[
                             [NSMutableDictionary dictionaryWithObjectsAndKeys:
                              @(ContainerVCTagHistory), kViewControllersDataTagPrefKey,
                              [NSNull null], kViewControllersDataViewControllerPrefKey,
                              @"WorkoutsHistorySegueID", kViewControllersDataSegueIdentifierPrefKey,
                              nil],
                             [NSMutableDictionary dictionaryWithObjectsAndKeys:
                              @(ContainerVCTagStatistic), kViewControllersDataTagPrefKey,
                              [NSNull null], kViewControllersDataViewControllerPrefKey,
                              @"ExercisesStatisticSegueID", kViewControllersDataSegueIdentifierPrefKey,
                              nil]
    ];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.transitionInProgress = NO;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if (![self objectForSegueIdentifier:identifier]) {
        // 'nil' means we cannot proceed
        return NO;
    }
    
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if (![self hasViewControllerForSegueIdentifier:segue.identifier]) {
        
        [self addChildViewController:segue.destinationViewController];
        UIView* destView = ((UIViewController *)segue.destinationViewController).view;
        destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        destView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [self.view addSubview:destView];
        [segue.destinationViewController didMoveToParentViewController:self];
        
        [self setViewController:segue.destinationViewController forSegueIdentifier:segue.identifier];
    }
    
    else {
        [self swapFromViewController:[self viewControllerForTag:self.currentVCTag] toViewController:segue.destinationViewController];
    }
}

- (void)swapFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController
{
    toViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);

    [fromViewController willMoveToParentViewController:nil];
    [self addChildViewController:toViewController];

    self.transitionInProgress = YES;
    
    [self transitionFromViewController:fromViewController toViewController:toViewController duration:1.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:^(BOOL finished) {
        [fromViewController removeFromParentViewController];
        [toViewController didMoveToParentViewController:self];
        self.transitionInProgress = NO;
    }];
}

- (void)selectViewControllerWithTag:(ContainerVCTag)nextTag
{
    if (self.currentVCTag == nextTag || self.transitionInProgress) {
        return;
    }
    
    NSString* nextSegueIdentifier = [self segueIdentifierForTag:nextTag];
    if (!nextSegueIdentifier || nextSegueIdentifier.length == 0) {
        return;
    }
    
    if ([self isViewControllerInstantiatedForTag:nextTag]) {
        [self swapFromViewController:[self viewControllerForTag:self.currentVCTag] toViewController:[self viewControllerForTag:nextTag]];
    }
    
    else {
        [self performSegueWithIdentifier:nextSegueIdentifier sender:nil];
    }
    
    // important we set it after all transitions being made,
    // this helps us to identify the current vc during prepareForSegue step
    self.currentVCTag = nextTag;
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark View Controller Data Helpers
#pragma mark -
///////////////////////////////////////////////

- (NSString *)segueIdentifierForTag:(ContainerVCTag)tag {
    NSString* currentSegueIdentifier = nil;
    
    if (self.viewControllersData && self.viewControllersData.count > tag) {
        NSMutableDictionary* dict = self.viewControllersData[tag];
        currentSegueIdentifier = [dict objectForKey:kViewControllersDataSegueIdentifierPrefKey];
    }
    
    return currentSegueIdentifier;
}

- (BOOL)isViewControllerInstantiatedForTag:(ContainerVCTag)tag {
    if (self.viewControllersData && self.viewControllersData.count > tag) {
        NSDictionary* dict = self.viewControllersData[tag];
        id obj = [dict objectForKey:kViewControllersDataViewControllerPrefKey];
        
        if (!obj || [obj isKindOfClass:[NSNull class]]) {
            return NO;
        }
        
        else {
            return YES;
        }
    }
    
    return NO;
}

- (UIViewController *)viewControllerForTag:(ContainerVCTag)tag {
    UIViewController* vc = nil;
    
    if (self.viewControllersData && self.viewControllersData.count > tag) {
        NSDictionary* dict = self.viewControllersData[tag];
        id obj = [dict objectForKey:kViewControllersDataViewControllerPrefKey];
        if ([obj isKindOfClass:[UIViewController class]]) {
            vc = (UIViewController *)obj;
        }
    }
    
    return vc;
}

- (id)objectForSegueIdentifier:(NSString *)segueId
{
    if (!segueId || segueId.length == 0) {
        return nil;
    }
    
    id obj = nil;
    for (NSDictionary* dict in self.viewControllersData) {
        if ([dict[kViewControllersDataSegueIdentifierPrefKey] isEqualToString:segueId]) {
            obj = dict[kViewControllersDataViewControllerPrefKey];
            break;
        }
    }
    
    return obj;
}

- (BOOL)hasViewControllerForSegueIdentifier:(NSString *)segueId
{
    id obj = [self objectForSegueIdentifier:segueId];
    if (obj && [obj isKindOfClass:[UIViewController class]]) {
        return YES;
    }
    
    return NO;
}

- (void)setViewController:(UIViewController *)vc forSegueIdentifier:(NSString *)segueId
{
    if (!segueId || segueId.length == 0 || !vc || ![vc isKindOfClass:[UIViewController class]]) {
        return;
    }
    
    for (NSMutableDictionary* dict in self.viewControllersData) {
        if ([dict[kViewControllersDataSegueIdentifierPrefKey] isEqualToString:segueId]) {
            dict[kViewControllersDataViewControllerPrefKey] = vc;
            break;
        }
    }
}

@end

///////////////////////////////////////////////
// EmptySegue Class
///////////////////////////////////////////////

@implementation EmptySegue

- (void)perform
{
    // Nothing. The ContainerViewController class handles all of the view
    // controller action.
}

@end
