//
//  UBIExtensions.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/19/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import Foundation
import UIKit

extension NSRange {
    func toRange(string: String) -> Range<String.Index> {
        let startIndex = string.startIndex.advancedBy(self.location)
        let endIndex = startIndex.advancedBy(self.length)
        return startIndex..<endIndex
    }
}