//
//  FASModifyDateProtocol.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/2/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#ifndef FASWorkouts_FASModifyDateProtocol_h
#define FASWorkouts_FASModifyDateProtocol_h

@protocol FASModifyDate<NSObject>
- (NSNumber *)uniqueID;
- (NSDate *)modifyDate;
@end

#endif
