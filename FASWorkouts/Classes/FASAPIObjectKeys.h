//
//  FASAPIObjectKeys.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/28/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#ifndef FASWorkouts_FASAPIObjectKeys_h
#define FASWorkouts_FASAPIObjectKeys_h

// method param keys
extern NSString * const FASAPIUsernameParam;
extern NSString * const FASAPIPasswordParam;
extern NSString * const FASAPIPasswordConfirmParam;
extern NSString * const FASAPIAuthTypeParam;
extern NSString * const FASAPIClientNonceParam;
extern NSString * const FASAPIFBTokenParam;
extern NSString * const FASAPIAPNSTokensKey;
extern NSString * const FASAPIAcceptedLegalAgreementVersionKey;

// common keys
extern NSString * const FASAPIResultKey;
extern NSString * const FASAPISuccessKey;

// user info keys
extern NSString * const FASAPIUserIDKey;
extern NSString * const FASAPIEmailKey;
extern NSString * const FASAPIGenderKey;
extern NSString * const FASAPISettingsKey;
extern NSString * const FASAPISettingReceiveNewsKey;

// errors keys
extern NSString * const FASAPIErrorCodeKey;
extern NSString * const FASAPIErrorValueKey;

// session keys
extern NSString * const FASAPITokenKey;
extern NSString * const FASAPISessionSignatureKey;
extern NSString * const FASAPILogonnameKey;
extern NSString * const FASAPIUserDataKey;
// data
extern NSString * const FASAPIResultDataKey;
extern NSString * const FASAPIExecParamsKey;

// workout keys
extern NSString * const FASAPIIdentifier;
extern NSString * const FASAPImiModifyDate;
extern NSString * const FASAPIMuscleTypes;
extern NSString * const FASAPIinAppProductID;
extern NSString * const FASAPIPrimaryMuscleType;
extern NSString * const FASAPILinkToImageURL;

// workout history
extern NSString * const FASAPIStartDate;
extern NSString * const FASAPIEndDate;
extern NSString * const FASAPIWorkoutID;

// store item
extern NSString * const FASAPIStoreItemItemsKey;
extern NSString * const FASAPIStoreItemInAppProductID;
extern NSString * const FASAPIStoreItemProductType;
extern NSString * const FASAPIStoreItemSortIndex;

#endif
