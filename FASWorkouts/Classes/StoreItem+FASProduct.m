//
//  StoreItem+FASProduct.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/24/16.
//  Copyright © 2016 FAS. All rights reserved.
//

#import "StoreItem+FASProduct.h"
#import "StoreObserver.h"

@implementation StoreItem (FASProduct)

- (NSString * _Nonnull)productCategoryName
{
    return self.productType;
}

- (NSString * _Nonnull)productIdentifier
{
    return self.inAppProductID;
}

- (void)setProductPurchaseStatus:(NSInteger)newValue
{
    self.purchaseStatus = @(newValue);
}

- (NSInteger)productPurchaseStatus
{
    if (self.purchaseStatus) {
        return [self.purchaseStatus integerValue];
    }
    return (NSInteger)SOProductStatusPurchaseNone;
}

- (BOOL)isPaid
{
    return (BOOL)(self.inAppProductID != nil);
}

- (BOOL)isPurchasing
{
    SOProductStatus status = [self productPurchaseStatus];
    if (status == SOProductStatusPurchaseInProgress
        || status == SOProductStatusPurchaseSucceeded
        || status == SOProductStatusRestoreSucceeded
        || status == SOProductStatusReceiptValidationInProgress
        || status == SOProductStatusPurchaseDeferred) {
        return YES;
    }
    return NO;
}

- (BOOL)isPurchased
{
    return (BOOL)(self.productPurchaseStatus == SOProductStatusReceiptValidationSucceeded);
}

@end
