//
//  FASWorkoutHistoryCell.swift
//  FASWorkouts
//
//  Created by Vladimir Stepanchenko on 4/11/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit

private var sDateFormatter = NSDateFormatter()
private var dateComponentsFormatter = NSDateComponentsFormatter()

class FASWorkoutHistoryCell: FASTableViewCell {
    
    @IBOutlet weak var remoteImageView: UBIRemoteImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        separatorType = .Default
        
        nameLabel.textColor = UIColor.fasYellowColor()
        nameLabel.font = UIFont.boldBabasFontOfSize(25)
        ratingLabel.textColor = UIColor(hexString: "f8e401", alpha: 0.6)
        levelLabel.textColor = UIColor(hexString: "ffffff", alpha: 1.0)
        
        dateComponentsFormatter.zeroFormattingBehavior = NSDateComponentsFormatterZeroFormattingBehavior.Pad
        dateComponentsFormatter.allowedUnits =  [NSCalendarUnit.Hour, NSCalendarUnit.Minute, NSCalendarUnit.Second]
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        workoutHistory = nil
    }
    
    var workoutHistory: WorkoutHistory? {
        didSet {
            updateView()
        }
    }
    
    private func updateView() {
        
        if workoutHistory == nil {
            return
        }
        let workout = workoutHistory!.workout
        
        if workout == nil {
            return
        }
        
        remoteImageView.remoteImage = workoutHistory!.workout.remoteImage()
        nameLabel.text = workoutHistory!.workout.name
        
        switch workoutHistory!.rate.integerValue {
        case 1:
            ratingLabel.text = "rating: medium"
        case 2:
            ratingLabel.text = "rating: hard"
        default:
            ratingLabel.text = "rating: easy"
        }
        
        if workoutHistory!.endDate != nil {
            dateLabel.text = workoutHistory!.endDate.toString("dd.MM.yyyy")
            if workoutHistory!.startDate != nil {
                let adjustedEndDate = workoutHistory!.endDate.dateByAddingTimeInterval(-workoutHistory!.pauseDuration.timeIntervalValue)
                timeLabel.text = "duration: \((dateComponentsFormatter.stringFromDate(workoutHistory!.startDate, toDate: adjustedEndDate)!))"
            } else {
                timeLabel.text = "duration: - "
            }
        } else {
            timeLabel.text = "duration: - "
        }
        
        if let level = workoutHistory!.workout.difficulty {
            // TODO: localize it
            levelLabel.text = "level: \(level)"
        } else {
            levelLabel.text = nil
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        remoteImageView.applyStyleForRemoteImageView()
    }
}