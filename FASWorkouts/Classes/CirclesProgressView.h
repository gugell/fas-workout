//
//  CirclesProgressView.h
//  FAS Workouts
//
//  Created by Vladimir Stepanchenko on 12/15/14.
//  Copyright (c) 2014 Vladimir Stepanchenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CirclesProgressView : UIView

@property(nonatomic) CGFloat progress;

@end
