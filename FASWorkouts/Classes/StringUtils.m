//
//  StringUtils.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 6/1/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "StringUtils.h"
#import "NSData+CRC32.h"
#import <CommonCrypto/CommonDigest.h>

@implementation StringUtils

+ (NSString*)trim:(NSString*)string {
    if (string != nil) {
        return  [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    } else {
        return nil;
    }
}

+ (BOOL)isEmpty:(NSString*)string {
    return string == nil || [@"" isEqualToString:[self trim:string]];
}

+ (NSString*)escapeJSONString:(NSString*)string
{
    NSMutableString* clean_json = [NSMutableString string];
    NSScanner* scanner = [NSScanner scannerWithString: string];
    NSString* buf = nil;
    while ( ! [scanner isAtEnd] ) {
        if ( ! [scanner scanUpToString: @"\\u" intoString: &buf] )
            break;//no more characters to scan
        [clean_json appendString: buf];
        if ( [scanner isAtEnd] )
            break;//reached end with this scan
        [scanner setScanLocation: [scanner scanLocation] + 2];//skip the '\\u'
        unsigned c;
        if ( [scanner scanHexInt: &c] )
            [clean_json appendFormat: @"%u", c];
        else
            [clean_json appendString: @"\\u"];//nm
    }
    
    return [[[clean_json stringByReplacingOccurrencesOfString:@"\\" withString:@"\\\\"] stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]  stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"];
}
+ (NSString *)stringByStrippingHTML:(NSString *)inputString
{
    NSMutableString *outString = nil;
    if (inputString)
    {
        outString = [[NSMutableString alloc] initWithString:inputString];
        if ([inputString length] > 0)
        {
            NSRange r;
            while ((r = [outString rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
            {
                [outString deleteCharactersInRange:r];
            }
        }
    }
    return outString;
}
+ (NSString*)urlEncodedStrng:(NSString*)string {
    return  [(NSString *)CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)string, NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8) autorelease];
}
+ (NSString*) idToString:(id)iD {
    NSString *result = @"";
    if (iD != nil && iD != [NSNull null]) {
        if ([iD isKindOfClass:[NSString class]]) {
            result = (NSString*)iD;
        } else if ([iD isKindOfClass:[NSNumber class]]) {
            result = [iD stringValue];
        }
    }
    return result;
}

+ (NSString*)sessionKey:(NSString *)string {
    NSString* privateSalt = string;//@"8587043";
    NSString* hexa8ID = [NSString stringWithFormat: @"%08X", privateSalt.intValue];
    
    NSString* secretWord = @"secret";
    NSData* swData =[secretWord dataUsingEncoding:NSUTF8StringEncoding];
    NSInteger crc32SessionWord = [swData crc32];
    
    long timeStampI = (long)([[NSDate date] timeIntervalSince1970]);
    NSString* timeStamp = [NSString stringWithFormat: @"%08lX", timeStampI];
    
    NSString* crc32innerX = [self CRC32X: [NSString stringWithFormat:@"%ld%ld", timeStampI, (long)crc32SessionWord]];
    /*NSString* crc32inner = [NSString stringWithFormat:@"%ld%ld", timeStampI, (long)crc32SessionWord];
     NSData* crData =[crc32inner dataUsingEncoding:NSUTF8StringEncoding];
     NSInteger crc32innerC = [crData crc32];
     NSString* crc32innerX = [NSString stringWithFormat: @"%lX", (long)crc32innerC];*/
    
    NSString* signature = [NSString stringWithFormat:@"%@%@%@", hexa8ID, timeStamp, crc32innerX];
    return signature;
}

+ (UIColor *)colorFromHex:(NSString *)hex {
    unsigned int c;
    if ([hex characterAtIndex:0] == '#') {
        [[NSScanner scannerWithString:[hex substringFromIndex:1]] scanHexInt:&c];
    } else {
        [[NSScanner scannerWithString:hex] scanHexInt:&c];
    }
    return [UIColor colorWithRed:((c & 0xff0000) >> 16)/255.0
                           green:((c & 0xff00) >> 8)/255.0
                            blue:(c & 0xff)/255.0 alpha:1.0];
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Cripto
#pragma mark -
///////////////////////////////////////////////
+ (NSString*)getClientNonce {
    NSString* result = @"";
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-ddThh:mm"];
    
    result = [formatter stringFromDate:[NSDate date]];
    result = [self sha256HashFor:result];
    return result;
}

+(NSString*)sha256HashFor:(NSString*)input {
    const char* str = [input UTF8String];
    unsigned char result[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(str, (CC_LONG)strlen(str), result);
    
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH*2];
    for(int i = 0; i<CC_SHA256_DIGEST_LENGTH; i++)
    {
        [ret appendFormat:@"%02x",result[i]];
    }
    return ret;
}

+ (NSString *)MD5:(NSString *)input {
    const char *cStr = [input UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, (CC_LONG)strlen(cStr), digest );
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }
    return  output;
}

+ (NSString *)CRC32X:(NSString *)input {
    NSData* crData =[input dataUsingEncoding:NSUTF8StringEncoding];
    NSInteger crc32C = [crData crc32];
    return [NSString stringWithFormat: @"%08lX", (long)crc32C];
}

@end
