//
//  Faq.h
//  FASWorkouts
//
//  Created by Vladimir Stepanchenko on 9/17/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UnityBase.h"
#import <CoreData/CoreData.h>

@interface FAQItem : NSManagedObject <UBIUpdatableWithDictionary>

@property (nonatomic, retain) NSNumber * faq_id;
@property (nonatomic, retain) NSNumber * sortIndex;
@property (nonatomic, retain) NSString * question;
@property (nonatomic, retain) NSString * answer;

@end
