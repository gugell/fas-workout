//
//  RadialProgressView.m
//  FAS Workouts
//
//  Created by Vladimir Stepanchenko on 12/15/14.
//  Copyright (c) 2014 Vladimir Stepanchenko. All rights reserved.
//

#import "RadialProgressView.h"

@interface RadialProgressView ()

@property (nonatomic, assign) CGFloat segmentSeparationAngle;
@property (nonatomic, assign) CGFloat progressRingWidth;

@property (nonatomic, retain) UIView *labelContainerView;

@property(nonatomic, retain) CAShapeLayer *mainLayer;
@property(nonatomic, retain) CAShapeLayer *mainLayerStatic;
@property(nonatomic, retain) CAShapeLayer *progressLayer;

@property(nonatomic) BOOL constraintsSet;

@end

@implementation RadialProgressView
{
  CGFloat outerRingAngle;
  CGFloat innerRingAngle;
  CGFloat segmentSeparationInnerAngle;
  
  CGFloat outerRingAngleAlt;
  CGFloat innerRingAngleAlt;
  CGFloat segmentSeparationInnerAngleAlt;
}

#pragma mark - Init section

- (id)init
{
  self = [super init];
  if (self) {
    [self setup];
  }
  return self;
}

- (id)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {
    [self setup];
  }
  return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
  self = [super initWithCoder:aDecoder];
  if (self) {
    [self setup];
  }
  return self;
}

- (void)updateAngles
{
  /**
   @note parameters' set for masked full progress layer
   */
  outerRingAngleAlt = ((2.0 * M_PI) / 360.);
  segmentSeparationInnerAngleAlt = 2.0 * asinf(((self.bounds.size.width / 2.0) * sinf(0)) / ((self.bounds.size.width / 2.0) - _progressRingWidth));
  innerRingAngleAlt = ((2.0 * M_PI) / (float)360) - segmentSeparationInnerAngleAlt;
}

-(void)setup
{
  self.backgroundColor = [UIColor clearColor];
  // Defaults
  
  // Layers
  self.mainLayer = [CAShapeLayer layer];
  self.mainLayer.lineWidth = 3.;
  self.mainLayer.strokeColor = [UIColor whiteColor].CGColor;
  self.mainLayer.fillColor = [UIColor clearColor].CGColor;
  
  self.mainLayerStatic = [CAShapeLayer layer];
  self.mainLayerStatic.lineWidth = 2.;
  self.mainLayerStatic.strokeColor = [UIColor whiteColor].CGColor;
  self.mainLayerStatic.fillColor = [UIColor clearColor].CGColor;
  
  self.progressLayer = [CAShapeLayer layer];
  self.progressLayer.lineWidth = 2.;
  // Custom yellow color
  self.progressLayer.strokeColor = [UIColor colorWithRed:248./255. green:228./255. blue:1.0/255. alpha:1.].CGColor;
  self.progressLayer.fillColor = [UIColor colorWithRed:248./255. green:228./255. blue:1.0/255. alpha:1.].CGColor;
  
  // Progress-related properties
  self.progressRingWidth = fmaxf(self.bounds.size.width * .3, 1.0);
  self.progressLayer.mask = self.mainLayer;
  [self.layer addSublayer:self.mainLayerStatic];
  [self updateAngles];
  [self.layer addSublayer:self.progressLayer];
  
  // Label container view
  self.labelContainerView = [UIView new];
  [self.labelContainerView setTranslatesAutoresizingMaskIntoConstraints:NO];
  [self addSubview:self.labelContainerView];
  
  // Labels
  self.titleLabel = [UILabel new];
  self.titleLabel.textAlignment = NSTextAlignmentCenter;
  self.titleLabel.textColor = [UIColor colorWithRed:248./255. green:228./255. blue:1.0/255. alpha:1.];
  [self.titleLabel setAdjustsFontSizeToFitWidth:YES];
  [self.titleLabel setMinimumScaleFactor:0.1];
  
  self.captionLabel = [UILabel new];
  self.captionLabel.textAlignment = NSTextAlignmentCenter;
  self.captionLabel.textColor = [UIColor whiteColor];
  [self.captionLabel setAdjustsFontSizeToFitWidth:YES];
  [self.captionLabel setMinimumScaleFactor:0.1];
  
  [self.titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
  [self.captionLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
  
  [self.labelContainerView addSubview:self.titleLabel];
  [self.labelContainerView addSubview:self.captionLabel];
  
  self.constraintsSet = NO;
}

#pragma mark - Drawing

-(void)drawCircle
{
  CGFloat lineWidth = self.mainLayer.lineWidth;
  CGRect reducedBounds = CGRectMake(lineWidth, lineWidth, self.bounds.size.width - lineWidth, self.bounds.size.width - lineWidth);
  UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:reducedBounds];
  // Set the path
  self.mainLayer.path = path.CGPath;
  self.mainLayerStatic.path = path.CGPath;
}

- (void)drawProgress
{
  // Create parameters to draw background
  // The progress segments are drawn clockwise, start with the outer ring, add an arc clockwise.  Then add the coresponding arc for the inner ring counterclockwise. Then close the path. The line connecting the two arcs is not needed. From tests it seems to be created automatically.
  CGFloat outerStartAngle = - M_PI_2;
  // Skip half of a separation angle, since the first separation will be centered upward.
  outerStartAngle += (_segmentSeparationAngle / 2.0);
  // Calculate the inner start angle position
  CGFloat innerStartAngle = - M_PI_2;
  innerStartAngle += (segmentSeparationInnerAngle / 2.0) + innerRingAngleAlt;
  // Create the path ref that all the paths will be appended
  CGMutablePathRef pathRef = CGPathCreateMutable();
  // Create each segment
  CGPoint center = CGPointMake(self.bounds.size.width / 2.0, self.bounds.size.width / 2.0);
  
  
  for (int i = 0; i < [self intProgress]; i++) {
    // Create the outer ring segment
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:center radius:(self.bounds.size.width / 2.0) + 5 startAngle:outerStartAngle endAngle:(outerStartAngle + outerRingAngleAlt) clockwise:YES];
    [path addArcWithCenter:center radius:(self.bounds.size.width / 2.0) - _progressRingWidth startAngle:innerStartAngle endAngle:(innerStartAngle - innerRingAngleAlt) clockwise:NO];
    // Add the segment to the path
    CGPathAddPath(pathRef, NULL, path.CGPath);
    
    // Setup for the next segment
    outerStartAngle += outerRingAngleAlt;
    innerStartAngle += (innerRingAngleAlt + segmentSeparationInnerAngleAlt);
  }
  
  // Set the progress path
  _progressLayer.path = pathRef;
  CGPathRelease(pathRef);
}

- (NSInteger)intProgress
{
  return (NSInteger)floorf(self.progress * 360);
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
  // Drawing code
  [self drawCircle];
  [self drawProgress];
}

-(void)setProgress:(CGFloat)progress
{
  _progress = progress;
  [self setNeedsDisplay];
}

- (void)setFrame:(CGRect)frame
{
  // Keep the progress view square
  if (frame.size.width != frame.size.height) {
    frame.size.height = frame.size.width;
  }
  [self updateAngles];
  [super setFrame:frame];
}

- (void)layoutSubviews
{
  [super layoutSubviews];
  self.mainLayer.frame = self.bounds;
  self.mainLayerStatic.frame = self.bounds;
  self.progressLayer.frame = self.bounds;
  
  [self setNeedsDisplay];
}

#pragma mark - Constraints

- (void)updateConstraints
{
  if (!self.constraintsSet)
    [self setupConstraints];
  [super updateConstraints];
}

-(void)setupConstraints
{
  CGFloat squareSide = (self.bounds.size.width - 2 * self.mainLayer.lineWidth) / sqrtf(2.) - 15.;
  // Label container view
  NSLayoutConstraint *containerWidthConstraint = [NSLayoutConstraint
                                                  constraintWithItem:self.labelContainerView
                                                  attribute:NSLayoutAttributeWidth
                                                  relatedBy:NSLayoutRelationEqual
                                                  toItem:nil
                                                  attribute:NSLayoutAttributeNotAnAttribute
                                                  multiplier:1.0
                                                  constant:squareSide];
  [self addConstraint:containerWidthConstraint];
  
  NSLayoutConstraint *containerHeightConstraint = [NSLayoutConstraint
                                                   constraintWithItem:self.labelContainerView
                                                   attribute:NSLayoutAttributeHeight
                                                   relatedBy:NSLayoutRelationEqual
                                                   toItem:nil
                                                   attribute:NSLayoutAttributeNotAnAttribute
                                                   multiplier:1.0
                                                   constant:squareSide];
  [self addConstraint:containerHeightConstraint];
  
  
  NSLayoutConstraint *centerYConstraint = [NSLayoutConstraint
                                           constraintWithItem:self.labelContainerView
                                           attribute:NSLayoutAttributeCenterY
                                           relatedBy:NSLayoutRelationEqual
                                           toItem:self
                                           attribute:NSLayoutAttributeCenterY
                                           multiplier:1.0
                                           constant:self.mainLayer.lineWidth / 2];
  [self addConstraint:centerYConstraint];
  
  NSLayoutConstraint *centerXConstraint = [NSLayoutConstraint
                                           constraintWithItem:self.labelContainerView
                                           attribute:NSLayoutAttributeCenterX
                                           relatedBy:NSLayoutRelationEqual
                                           toItem:self
                                           attribute:NSLayoutAttributeCenterX
                                           multiplier:1.0
                                           constant:self.mainLayer.lineWidth / 2];
  [self addConstraint:centerXConstraint];
#pragma mark Title label
  // Title label constraints (align initial label to top)
  NSLayoutConstraint *titleLabelTopConstraint = [NSLayoutConstraint
                                                 constraintWithItem:self.titleLabel
                                                 attribute:NSLayoutAttributeTop
                                                 relatedBy:NSLayoutRelationEqual
                                                 toItem:self.labelContainerView
                                                 attribute:NSLayoutAttributeTop
                                                 multiplier:1.0
                                                 constant:5.];
  [self addConstraint:titleLabelTopConstraint];
  
  // Center in container
  NSLayoutConstraint *titleLabelCenterXConstraint = [NSLayoutConstraint
                                                     constraintWithItem:self.titleLabel
                                                     attribute:NSLayoutAttributeCenterX
                                                     relatedBy:NSLayoutRelationEqual
                                                     toItem:self.labelContainerView
                                                     attribute:NSLayoutAttributeCenterX
                                                     multiplier:1.0
                                                     constant:0.];
  [self addConstraint:titleLabelCenterXConstraint];
  
  NSLayoutConstraint *titleLabelWidthConstraint = [NSLayoutConstraint
                                                   constraintWithItem:self.titleLabel
                                                   attribute:NSLayoutAttributeWidth
                                                   relatedBy:NSLayoutRelationEqual
                                                   toItem:self.labelContainerView
                                                   attribute:NSLayoutAttributeWidth
                                                   multiplier:1.0
                                                   constant:0.];
  [self addConstraint:titleLabelWidthConstraint];
  
  NSLayoutConstraint *titleLabelHeightConstraint = [NSLayoutConstraint
                                                    constraintWithItem:self.titleLabel
                                                    attribute:NSLayoutAttributeHeight
                                                    relatedBy:NSLayoutRelationEqual
                                                    toItem:self.labelContainerView
                                                    attribute:NSLayoutAttributeWidth
                                                    multiplier:1./3.
                                                    constant:5.];
  [self addConstraint:titleLabelHeightConstraint];
  
  
#pragma mark Caption label
  // Caption label constraint (bottom)
  NSLayoutConstraint *captionLabelBottomConstraint = [NSLayoutConstraint
                                                      constraintWithItem:self.captionLabel
                                                      attribute:NSLayoutAttributeBottom
                                                      relatedBy:NSLayoutRelationEqual
                                                      toItem:self.labelContainerView
                                                      attribute:NSLayoutAttributeBottom
                                                      multiplier:1.0
                                                      constant:-5.];
  [self addConstraint:captionLabelBottomConstraint];
  
  
  // Center caption in container
  NSLayoutConstraint *captionLabelCenterXConstraint = [NSLayoutConstraint
                                                       constraintWithItem:self.captionLabel
                                                       attribute:NSLayoutAttributeCenterX
                                                       relatedBy:NSLayoutRelationEqual
                                                       toItem:self.labelContainerView
                                                       attribute:NSLayoutAttributeCenterX
                                                       multiplier:1.0
                                                       constant:0.];
  [self addConstraint:captionLabelCenterXConstraint];
  
  
  NSLayoutConstraint *captionLabelWidthConstraint = [NSLayoutConstraint
                                                     constraintWithItem:self.captionLabel
                                                     attribute:NSLayoutAttributeWidth
                                                     relatedBy:NSLayoutRelationEqual
                                                     toItem:self.labelContainerView
                                                     attribute:NSLayoutAttributeWidth
                                                     multiplier:1.0
                                                     constant:0.];
  [self addConstraint:captionLabelWidthConstraint];
  
  NSLayoutConstraint *captionLabelHeightConstraint = [NSLayoutConstraint
                                                      constraintWithItem:self.captionLabel
                                                      attribute:NSLayoutAttributeHeight
                                                      relatedBy:NSLayoutRelationEqual
                                                      toItem:self.labelContainerView
                                                      attribute:NSLayoutAttributeHeight
                                                      multiplier:2./3.
                                                      constant:-5.];
  [self addConstraint:captionLabelHeightConstraint];
  self.constraintsSet = YES;
}


@end
