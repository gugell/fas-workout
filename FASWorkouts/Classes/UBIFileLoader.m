//
//  UBIFileLoader.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/16/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "UBIFileLoader.h"
// TODO: loader should not know about FASAppDelegate
#import "FASAppDelegate.h"
#import "NSString+MyExtensions.h"
#import "XJSafeMutableDictionary.h"
#import "UBILogger.h"

// a wrapper around taskDescription property
@interface NSURLSessionTask (UBITaskDescription)
@property (nonatomic, strong) NSString* externalID;
@property (nonatomic, strong) NSString* filename;
@end

@interface NSURLSessionTask (UBIFileLoaderUserInfo)
- (NSDictionary *)fileLoaderUserInfo;
@end

@interface NSURLSessionTask (UBIProgress)
- (double)progress;
@end

NSString * const kUBIFileLoaderLoadingInfoURL               = @"URL";
NSString * const kUBIFileLoaderLoadingInfoFilename          = @"filename";
NSString * const kUBIFileLoaderLoadingInfoId                = @"identifier";

NSString* const kFASBackgroundDownloadSessionIdentifier = @"com.fas-sport.FASWorkouts.BackgroundSessionIdentifier";

static NSOperationQueue* sUBIFileLoaderOpQueue;

typedef void (^SessionCompletionBlock)();

@interface UBIFileLoader ()
@property (nonatomic, strong) NSURLSession * backgroundSession;
@property (nonatomic, copy) SessionCompletionBlock completionBlock;
@end

@implementation UBIFileLoader
///////////////////////////////////////////////
#pragma mark -
#pragma mark Initializers
#pragma mark -
///////////////////////////////////////////////
+ (void)initialize
{
    if (!sUBIFileLoaderOpQueue) {
        sUBIFileLoaderOpQueue = [NSOperationQueue new];
    }
}

- (instancetype)init
{
    return [self initWithAuthorizationToken:nil allowsCellularAccess:NO];
}

- (instancetype)initWithAuthorizationToken:(NSString *)token allowsCellularAccess:(BOOL)allowsCellularAccess
{
    self = [super init];
    if (self) {
        _authorizationToken = [token copy];
        _allowsCellularAccess = allowsCellularAccess;
        _backgroundSession = [self backgroundSession];
    }
    return self;
}

- (NSURLSession *)backgroundSession
{
    static NSURLSession *session = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSURLSessionConfiguration *backgroundConfigObject = nil;
        backgroundConfigObject = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:kFASBackgroundDownloadSessionIdentifier];

        // add persistant HTTP headers here
//        [backgroundConfigObject setHTTPAdditionalHeaders:@{ <#header_key#>: <#header_value#> }];
        
        backgroundConfigObject.allowsCellularAccess = self.allowsCellularAccess;
        
        session = [NSURLSession sessionWithConfiguration:backgroundConfigObject
                                                delegate:self
                                           delegateQueue:sUBIFileLoaderOpQueue];
    });
    
    return session;
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Actions
#pragma mark -
///////////////////////////////////////////////
- (BOOL)downloadFileWithLoadingInfo:(NSDictionary *)loadingInfo
{
    // get external id, which is required to get mapping between internal and external representation
    NSString* externalID = [loadingInfo objectForKey:kUBIFileLoaderLoadingInfoId];
    if (!externalID || ![externalID isKindOfClass:[NSString class]]) {
        UBILogError(@"Cannot process loading info, because the 'identifier' is invalid");
        return NO;
    }
    
    // get URL so we can create a task to download the file
    NSURL* downloadURL = [loadingInfo objectForKey:kUBIFileLoaderLoadingInfoURL];
    if (!downloadURL || ![downloadURL isKindOfClass:[NSURL class]]) {
        UBILogError(@"Cannot process loading info, because the URL is invalid");
        return NO;
    }
    
    assert(self.backgroundSession != nil);
    
    // check if we alread have info for this external id
    [self.backgroundSession getTasksWithCompletionHandler:^(NSArray<NSURLSessionDataTask *> * _Nonnull dataTasks, NSArray<NSURLSessionUploadTask *> * _Nonnull uploadTasks, NSArray<NSURLSessionDownloadTask *> * _Nonnull downloadTasks) {
        
        NSURLSessionDownloadTask* task = nil;
        for (NSURLSessionDownloadTask* downloadTask in downloadTasks) {
            if ([downloadTask.externalID isEqualToString:externalID]) {
                task = downloadTask;
                break;
            }
        }
        
        // create a new task, when not found
        if (!task) {
            NSURLRequest* request = [self requestForURL:downloadURL];
            NSURLSessionDownloadTask* downloadTask = [self.backgroundSession downloadTaskWithRequest:request];
            assert(downloadTask != nil);
            
            NSString* filename = [loadingInfo objectForKey:kUBIFileLoaderLoadingInfoFilename];
            [downloadTask setFilename:filename];
            
            [downloadTask setExternalID:externalID];
            
            task = downloadTask;
        }
        
        
        switch (task.state) {
            case NSURLSessionTaskStateRunning: {
                UBILogTrace(@"task with externalID = %@ is RUNNING", externalID);
            } break;
            
            case NSURLSessionTaskStateSuspended: {
                [task resume];
                
                if ([self.delegate respondsToSelector:@selector(UBIFileLoader:didStartDownloadingWithUserInfo:)]) {
                    NSDictionary* userInfo = [task fileLoaderUserInfo];
                    [self.delegate UBIFileLoader:self didStartDownloadingWithUserInfo:userInfo];
                }
            } break;
                
            case NSURLSessionTaskStateCanceling: {
                UBILogTrace(@"task with externalID = %@ is CANCELING", externalID);
            } break;
                
            case NSURLSessionTaskStateCompleted: {
                UBILogTrace(@"task with externalID = %@ is COMPLETED", externalID);
            } break;
        }
    }];
    
    return YES;
}

- (void)authorizationTokenDidChange
{
    UBILogTrace(@"Authorization Token Did Change");
    // TODO:
    // 1. get list of all created tasks
    // 2. Filter out all already running tasks
    // 3. Re-create these tasks and re-submit them to NSURLSession
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Custom Accessors
#pragma mark -
///////////////////////////////////////////////
- (void)setAuthorizationToken:(NSString *)authToken
{
    if (authToken != _authorizationToken && ![authToken isEqualToString:_authorizationToken]) {
        _authorizationToken = [authToken copy];
        [self authorizationTokenDidChange];
    }
}

- (NSURLRequest *)requestForURL:(NSURL *)URL
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL];
    if (self.authorizationToken != nil && ![self.authorizationToken isBlank]) {
        [request addValue:self.authorizationToken forHTTPHeaderField:@"Authorization"];
    }
    return [request copy];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark NSURLSession
#pragma mark -
///////////////////////////////////////////////

///////////////////////////////////////////////
#pragma mark NSURLSessionDownloadDelegate -
///////////////////////////////////////////////
- (void)URLSession:(NSURLSession *)session
      downloadTask:(NSURLSessionDownloadTask *)downloadTask
      didWriteData:(int64_t)bytesWritten
 totalBytesWritten:(int64_t)totalBytesWritten
totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    /*
     Sent periodically to notify the delegate of download progress.
     
     Report progress on the task.
     If you created more than one task, you might keep references to them and report on them individually.
     */

    if ([self.delegate respondsToSelector:@selector(UBIFileLoader:didMakeProgress:userInfo:)]) {
        
        double progress = downloadTask.progress;
        UBILogTrace(@"Task with ExternalID (%@) - progress: %lf", downloadTask.externalID, progress);
        
        NSDictionary* userInfo = [downloadTask fileLoaderUserInfo];
        
        [self.delegate UBIFileLoader:self
                     didMakeProgress:progress
                            userInfo:userInfo];
    }
}


- (void)URLSession:(NSURLSession *)session
      downloadTask:(NSURLSessionDownloadTask *)downloadTask
didFinishDownloadingToURL:(NSURL *)downloadURL
{
    /*
     The download completed, you need to copy the file at targetPath before the end of this block.
     As an example, copy the file to the Documents directory of your app.
     */
    
    if ([self.delegate respondsToSelector:@selector(UBIFileLoader:didDownloadFileWithUserInfo:toPath:)]) {

        NSDictionary* userInfo = [downloadTask fileLoaderUserInfo];

#ifdef DEBUG_LOGGING
        double progress = downloadTask.progress;
        if (floor(progress) != 1.0f) {
            UBILogWarning(@"FINISH ExternalID (%@), but progress (%lf) NOT EQUAL to 1.0", [userInfo objectForKey:kUBIFileLoaderLoadingInfoId], progress);
        }
        else {
            UBILogTrace(@"FINISH ExternalID (%@)", [userInfo objectForKey:kUBIFileLoaderLoadingInfoId]);
        }
#endif
        
        [self.delegate UBIFileLoader:self
          didDownloadFileWithUserInfo:userInfo
                               toPath:downloadURL];
    }
}
///////////////////////////////////////////////
#pragma mark NSURLSessionTaskDelegate -
///////////////////////////////////////////////
- (void)URLSession:(NSURLSession *)session
              task:(NSURLSessionTask *)task
didCompleteWithError:(NSError *)error
{
    if (error == nil) {
        UBILogTrace(@"Completed successfully. Task:\n%@", [task fileLoaderUserInfo]);
    }
    else {
        UBILogError(@"Completed with error: %@\nTask:\%@", [error localizedDescription], [task fileLoaderUserInfo]);
    }
    
    if ([self.delegate respondsToSelector:@selector(UBIFileLoader:didFinishDownloadingWithUserInfo:error:)]) {
        
        NSDictionary* userInfo = [task fileLoaderUserInfo];
        
        [self.delegate UBIFileLoader:self
    didFinishDownloadingWithUserInfo:userInfo
                                error:error];
    }
}
///////////////////////////////////////////////
#pragma mark NSURLSessionDelegate -
///////////////////////////////////////////////
- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session
{
    
    // TODO: create a property of block type and pass the completion handler
    
    /*
     If an application has received an -application:handleEventsForBackgroundURLSession:completionHandler: message, the session delegate will receive this message to indicate that all messages previously enqueued for this session have been delivered. At this time it is safe to invoke the previously stored completion handler, or to begin any internal updates that will result in invoking the completion handler.
     */
    FASAppDelegate *appDelegate = (FASAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.backgroundSessionCompletionHandler) {
        self.completionBlock = appDelegate.backgroundSessionCompletionHandler;
        appDelegate.backgroundSessionCompletionHandler = nil;
    }
    
    if (self.completionBlock) {
        UBILogTrace(@"session completion handler is called");
        self.completionBlock();
    }
}
///////////////////////////////////////////////
#pragma mark NSURLSessionDownloadDelegate -
///////////////////////////////////////////////
-(void)URLSession:(NSURLSession *)session
     downloadTask:(NSURLSessionDownloadTask *)downloadTask
didResumeAtOffset:(int64_t)fileOffset
expectedTotalBytes:(int64_t)expectedTotalBytes
{
    // do not expect it is ever called
    assert(NO);
}

@end


//format: <externalID>;<filename>
// TODO: filename should be an external identifier, so we would have a single data being written as description
//downloadTask.taskDescription = [NSString stringWithFormat: @"%@;%@", externalID, filename];
@implementation NSURLSessionTask (UBITaskDescription)

- (NSString*)externalID
{
    if (!self.taskDescription) {
        return nil;
    }
    
    NSArray* taskDescriptionComponents = [self.taskDescription componentsSeparatedByString:@";"];
    return taskDescriptionComponents.firstObject;
}

- (void)setExternalID:(NSString*)newExternalID
{
    NSString* filename = (self.filename == nil ? @"" : self.filename);
    self.taskDescription = [NSString stringWithFormat: @"%@;%@", (newExternalID == nil ? @"" : newExternalID), filename];
}

- (NSString*)filename
{
    if (!self.taskDescription) {
        return nil;
    }
    
    NSArray* taskDescriptionComponents = [self.taskDescription componentsSeparatedByString:@";"];
    if (taskDescriptionComponents.count > 1) {
        return [taskDescriptionComponents objectAtIndex:1];
    }
    else {
        return nil;
    }
}

- (void)setFilename:(NSString *)newFilename
{
    NSString* externalID = (self.externalID == nil ? @"" : self.externalID);
    self.taskDescription = [NSString stringWithFormat: @"%@;%@", externalID, (newFilename == nil ? @"" : newFilename)];
}

@end

@implementation NSURLSessionTask (UBIFileLoaderUserInfo)

- (NSDictionary *)fileLoaderUserInfo
{
    NSMutableDictionary* userInfo = [NSMutableDictionary dictionary];
    
    [userInfo setObject:self.externalID forKey:kUBIFileLoaderLoadingInfoId];
    
    if (self.filename) {
        [userInfo setObject:self.filename
                     forKey:kUBIFileLoaderLoadingInfoFilename];
    }
    
    return [userInfo copy];
}

@end

@implementation NSURLSessionTask (UBIProgress)

- (double)progress
{
    double p = (double)self.countOfBytesReceived / (double)self.countOfBytesExpectedToReceive;
    return p;
}

@end