//
//  UIImage+Resize.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/1/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "UIImage+Resize.h"
#import "UIImage+RoundedCorner.h"
#import "UIImage+Alpha.h"

static CGRect CGRectTranspose(CGRect rect)
{
    return CGRectMake(rect.origin.y, rect.origin.x, rect.size.height, rect.size.width);
}

@interface UIImage (Resize_)
- (UIImage *)exResizedImage:(CGSize)newSize
                  transform:(CGAffineTransform)transform
                  zoomLevel:(CGFloat)zoomLevel
             drawTransposed:(BOOL)transpose
       interpolationQuality:(CGInterpolationQuality)quality;

- (CGAffineTransform)exTransformForOrientation:(CGSize)newSize;
- (CGAffineTransform)exTransformForImageOrientation:(UIImageOrientation)imageOrientation size:(CGSize)newSize;
@end

@implementation UIImage (Resize)

- (UIImage *)imageRotatedByOrientation:(UIImageOrientation)orientation
{
    return [self imageScaledToSize:CGSizeMake(CGImageGetWidth(self.CGImage), CGImageGetHeight(self.CGImage)) andRotatedByOrientation:orientation];
}

- (UIImage *)imageScaledToSize:(CGSize)newSize andRotatedByOrientation:(UIImageOrientation)orientation
{
    CGImageRef imageRef = self.CGImage;
    CGRect imageRect = CGRectMake(0.0, 0.0, newSize.width, newSize.height);
    CGRect contextRect = imageRect;
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (orientation)
    {
        case UIImageOrientationDown: { // rotate 180 deg
            transform = CGAffineTransformTranslate(transform, imageRect.size.width, imageRect.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
        } break;
            
        case UIImageOrientationLeft: { // rotate 90 deg left
            contextRect = CGRectTranspose(contextRect);
            transform = CGAffineTransformTranslate(transform, imageRect.size.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
        } break;
            
        case UIImageOrientationRight: { // rotate 90 deg right
            contextRect = CGRectTranspose(contextRect);
            transform = CGAffineTransformTranslate(transform, 0.0, imageRect.size.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
        } break;
            
        case UIImageOrientationUp: // no rotation
        default:
            break;
    }
    
    CGBitmapInfo bitmapInfo = CGImageGetBitmapInfo(imageRef);
    CGColorSpaceRef colorSpaceRef = CGImageGetColorSpace(imageRef);
    
    // madify bitmapInfo to work with PNG if necessary
    if (bitmapInfo == kCGImageAlphaNone) {
        bitmapInfo = (CGBitmapInfo)kCGImageAlphaNoneSkipLast;
    }
    else if (bitmapInfo == kCGImageAlphaLast) {
        bitmapInfo = (CGBitmapInfo)kCGImageAlphaPremultipliedLast;
    }
    
    // Build a context that's the same dimensions as the new size
    CGContextRef context = CGBitmapContextCreate(NULL,
                                                 contextRect.size.width,
                                                 contextRect.size.height,
                                                 CGImageGetBitsPerComponent(imageRef),
                                                 0,
                                                 colorSpaceRef,
                                                 bitmapInfo);
    
    
    CGContextConcatCTM(context, transform);
    CGContextDrawImage(context, imageRect, imageRef);
    
    // Get the rotated image from the context and a UIImage
    CGImageRef rotatedImageRef = CGBitmapContextCreateImage(context);
    UIImage *rotatedImage = [UIImage imageWithCGImage:rotatedImageRef];
    
    // Clean up
    CGImageRelease(rotatedImageRef);
    CGContextRelease(context);
    
    return rotatedImage;
}

// Returns a copy of this image that is cropped to the given bounds.
// The bounds will be adjusted using CGRectIntegral.
// This method ignores the image's imageOrientation setting.
- (UIImage *)croppedImage:(CGRect)bounds {
    CGImageRef imageRef = CGImageCreateWithImageInRect([self CGImage], bounds);
    UIImage *croppedImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return croppedImage;
}

// Returns a copy of this image that is squared to the thumbnail size.
// If transparentBorder is non-zero, a transparent border of the given size will be added around the edges of the thumbnail. (Adding a transparent border of at least one pixel in size has the side-effect of antialiasing the edges of the image when rotating it using Core Animation.)
- (UIImage *)thumbnailImage:(NSInteger)thumbnailSize
          transparentBorder:(NSUInteger)borderSize
               cornerRadius:(NSUInteger)cornerRadius
       interpolationQuality:(CGInterpolationQuality)quality
{
    UIImage *resizedImage = [self resizedImageWithContentMode:UIViewContentModeScaleAspectFill
                                                       bounds:CGSizeMake(thumbnailSize, thumbnailSize)
                                                    zoomLevel:1.0
                                         interpolationQuality:quality];
    
    // Crop out any part of the image that's larger than the thumbnail size
    // The cropped rect must be centered on the resized image
    // Round the origin points so that the size isn't altered when CGRectIntegral is later invoked
    CGRect cropRect = CGRectMake(round((resizedImage.size.width - thumbnailSize) / 2),
                                 //round((resizedImage.size.height - thumbnailSize) / 2),
                                 0,
                                 thumbnailSize,
                                 thumbnailSize);
    UIImage *croppedImage = [resizedImage croppedImage:cropRect];
    
    UIImage *transparentBorderImage = borderSize ? [croppedImage transparentBorderImage:borderSize] : croppedImage;
    
    return [transparentBorderImage roundedCornerImage:cornerRadius borderSize:borderSize];
}

// Returns a rescaled copy of the image, taking into account its orientation
// The image will be scaled disproportionately if necessary to fit the bounds specified by the parameter
- (UIImage *)exResizedImage:(CGSize)newSize
                  zoomLevel:(CGFloat)zoomLevel
       interpolationQuality:(CGInterpolationQuality)quality
{
    BOOL drawTransposed;
    
    switch (self.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored: {
            drawTransposed = YES;
        } break;
            
        default: {
            drawTransposed = NO;
        } break;
    }
    
    return [self exResizedImage:newSize
                      transform:[self exTransformForOrientation:newSize]
                      zoomLevel:zoomLevel
                 drawTransposed:drawTransposed
           interpolationQuality:quality];
}

// Resizes the image according to the given content mode, taking into account the image's orientation
- (UIImage *)resizedImageWithContentMode:(UIViewContentMode)contentMode
                                  bounds:(CGSize)bounds
                               zoomLevel:(CGFloat)zoomLevel
                    interpolationQuality:(CGInterpolationQuality)quality
{
    CGFloat horizontalRatio = bounds.width / self.size.width;
    CGFloat verticalRatio = bounds.height / self.size.height;
    CGFloat ratio;
    
    switch (contentMode) {
        case UIViewContentModeScaleAspectFill: {
            ratio = MAX(horizontalRatio, verticalRatio);
        } break;
            
        case UIViewContentModeScaleAspectFit: {
            ratio = MIN(horizontalRatio, verticalRatio);
        } break;
            
        default: {
            [NSException raise:NSInvalidArgumentException
                        format:@"Unsupported content mode: %ld", (unsigned long)contentMode];
        } break;
    }
    
    CGSize newSize = CGSizeMake(self.size.width * ratio, self.size.height * ratio);
    
    return [self exResizedImage:newSize zoomLevel:zoomLevel interpolationQuality:quality];
}

- (UIImage *)imageRotatedUpForDeviceOrientation:(UIDeviceOrientation)deviceOrientation
{
    return [self imageRotatedByOrientation:[self rotationNeededForImageTakenWithDeviceOrientation:deviceOrientation]];
}

- (UIImageOrientation)rotationNeededForImageTakenWithDeviceOrientation:(UIDeviceOrientation)deviceOrientation
{
    UIImageOrientation rotationOrientation;
    
    switch (deviceOrientation) {
        case UIDeviceOrientationPortraitUpsideDown: {
            rotationOrientation = UIImageOrientationLeft;
        } break;
            
        case UIDeviceOrientationLandscapeRight: {
            rotationOrientation = UIImageOrientationDown;
        } break;
            
        case UIDeviceOrientationLandscapeLeft: {
            rotationOrientation = UIImageOrientationUp;
        } break;
            
        case UIDeviceOrientationPortrait:
        default: {
            rotationOrientation = UIImageOrientationRight;
        } break;
    }
    
    return rotationOrientation;
}

@end

@implementation UIImage (Resize_)

// Returns a copy of the image that has been transformed using the given affine transform and scaled to the new size
// The new image's orientation will be UIImageOrientationUp, regardless of the current image's orientation
// If the new size is not integral, it will be rounded up
- (UIImage *)exResizedImage:(CGSize)newSize
                  transform:(CGAffineTransform)transform
                  zoomLevel:(CGFloat)zoomLevel
             drawTransposed:(BOOL)transpose
       interpolationQuality:(CGInterpolationQuality)quality {
    
    CGRect cgRect = CGRectMake(0, 0, CGImageGetWidth(self.CGImage), CGImageGetHeight(self.CGImage));
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    CGRect transposedRect = CGRectIntegral(CGRectMake(0, 0, newSize.height, newSize.width));
    
    CGRect newContextRect = newRect;
    
    // if transposing, need to make sure the destinationRect used to create the new context
    // is the correct size
    if (transpose && !CGRectEqualToRect(cgRect, transposedRect)) {
        newContextRect = transposedRect;
    }
    
    CGImageRef zoomedImageRef = nil;
    
    if (zoomLevel > 1.0f) {
        size_t cgWidth = CGImageGetWidth(self.CGImage);
        size_t cgHeight = CGImageGetHeight(self.CGImage);
        CGRect cgRect = CGRectMake(0, 0, cgWidth, cgHeight);
        
        CGFloat zoomedWidth = cgWidth / zoomLevel;
        CGFloat zoomedHeight = cgHeight / zoomLevel;
        CGRect cropRect = CGRectInset(cgRect,
                                      (cgWidth - zoomedWidth) / 2,
                                      (cgHeight - zoomedHeight) / 2);
        
        zoomedImageRef = CGImageCreateWithImageInRect(self.CGImage, cropRect);
    }
    
    CGImageRef imageRef = zoomedImageRef ? zoomedImageRef : self.CGImage;
    
    CGBitmapInfo bitmapInfo = CGImageGetBitmapInfo(imageRef);
    CGColorSpaceRef colorSpaceRef = CGImageGetColorSpace(imageRef);
    
    // madify bitmapInfo to work with PNG if necessary
    if (bitmapInfo == kCGImageAlphaNone) {
        bitmapInfo = (CGBitmapInfo)kCGImageAlphaNoneSkipLast;
    }
    else if (bitmapInfo == kCGImageAlphaLast) {
        bitmapInfo = (CGBitmapInfo)kCGImageAlphaPremultipliedLast;
    }
    
    // Build a context that's the same dimensions as the new size
    CGContextRef bitmap = CGBitmapContextCreate(NULL,
                                                newContextRect.size.width,
                                                newContextRect.size.height,
                                                CGImageGetBitsPerComponent(imageRef),
                                                0,
                                                colorSpaceRef,
                                                bitmapInfo);
    
    // Rotate and/or flip the image if required by its orientation
    CGContextConcatCTM(bitmap, transform);
    
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(bitmap, quality);
    
    // Draw into the context; this scales the image
    CGContextDrawImage(bitmap, transpose ? transposedRect : newRect, imageRef);
    
    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(bitmap);
    
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    // Clean up
    CGImageRelease(newImageRef);
    CGContextRelease(bitmap);
    
    // release imageRef if zoomed
    CGImageRelease(zoomedImageRef);
    
    return newImage;
}

// Returns an affine transform that takes into account the image orientation when drawing a scaled image
- (CGAffineTransform)exTransformForOrientation:(CGSize)newSize
{
    return [self exTransformForImageOrientation:self.imageOrientation size:newSize];
}

- (CGAffineTransform)exTransformForImageOrientation:(UIImageOrientation)imageOrientation size:(CGSize)newSize
{
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (imageOrientation) {
        case UIImageOrientationDown:           // EXIF = 3
        case UIImageOrientationDownMirrored: { // EXIF = 4
            transform = CGAffineTransformTranslate(transform, newSize.width, newSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
        } break;
            
        case UIImageOrientationLeft:           // EXIF = 6
        case UIImageOrientationLeftMirrored: { // EXIF = 5
            transform = CGAffineTransformTranslate(transform, newSize.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
        } break;
            
        case UIImageOrientationRight:           // EXIF = 8
        case UIImageOrientationRightMirrored: { // EXIF = 7
            transform = CGAffineTransformTranslate(transform, 0, newSize.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
        } break;
            
        default: break;
    }
    
    switch (imageOrientation) {
        case UIImageOrientationUpMirrored:     // EXIF = 2
        case UIImageOrientationDownMirrored: { // EXIF = 4
            transform = CGAffineTransformTranslate(transform, newSize.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
        } break;
            
        case UIImageOrientationLeftMirrored:    // EXIF = 5
        case UIImageOrientationRightMirrored: { // EXIF = 7
            transform = CGAffineTransformTranslate(transform, newSize.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
        } break;
            
        default: break;
    }
    
    return transform;
}

@end
