//
//  FASWorkoutsHistoryViewController.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 3/1/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import Foundation

// cell identifiers
enum WorkoutHistoryViewControllerCellIdentifier: String {
    case History = "WorkoutHistoryCellIdentifier"
}

class FASWorkoutsHistoryViewController: FASViewController {
    
    private var cacheName: String?
    var fetchedWorkoutHistoryController: NSFetchedResultsController? = nil
    
    @IBOutlet weak var tableView: UITableView!
    
    lazy var fetchedAllWorkoutHistoryController: NSFetchedResultsController = {
        let frc: NSFetchedResultsController = FASData.sharedData().fetchedWorkoutHistoryControllerWithCacheName(self.cacheName)
        return frc
        }()
    
    ///////////////////////////////////////////////
    // MARK: - UIViewController overrides -
    ///////////////////////////////////////////////
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        FASData.sharedData().loadWorkoutHistoryFromRemote()
        
        //fetchedWorkoutHistoryController?.delegate = nil
        //fetchedWorkoutsController = nil
        fetchedWorkoutHistoryController = fetchedAllWorkoutHistoryController
        fetchedWorkoutHistoryController!.delegate = self
        
        fetch()
        
        tableView.estimatedRowHeight = 91.0
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let topInset = topLayoutGuide.length/* + segmentedNavigationBar.frame.height*/
        
        var tableInset: UIEdgeInsets = tableView.contentInset
        tableInset.top = topInset
        tableView.contentInset = tableInset
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar(.Dark)
    }
    
    override func viewWillDisappear(animated: Bool) {
        configureNavigationBar(.DarkWithYellowTitle)
        super.viewWillDisappear(animated)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        automaticallyAdjustsScrollViewInsets = false;
        
        FASData.sharedData().addWorkoutHistoryObserver(self)
    }
    
    deinit {
        FASData.sharedData().removeWorkoutHistoryObserver(self)
    }
    ///////////////////////////////////////////////
    // MARK: Actions
    ///////////////////////////////////////////////
    
    ///////////////////////////////////////////////
    // MARK: - Controls -
    ///////////////////////////////////////////////
    
    func fetch() {
        if let cn = cacheName {
            NSFetchedResultsController.deleteCacheWithName(cn)
        }
        
        if let frc = fetchedWorkoutHistoryController {
            var fetchError: NSError?
            let result: Bool
            do {
                try frc.performFetch()
                result = true
            } catch let error as NSError {
                fetchError = error
                result = false
            }
            if !result {
                if let error = fetchError {
                    // TODO: Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    printlnDebugError("Unresolved error \(error.localizedDescription),\nerror info: \(error.userInfo)")
                    abort()
                }
            }
        } else {
            printlnDebugError("Fetched Result Controller was nil upon fetch()")
        }
    }
}
///////////////////////////////////////////////
// MARK: - TableView DataSource / Delegate -
///////////////////////////////////////////////
extension FASWorkoutsHistoryViewController: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return fetchedWorkoutHistoryController!.sections!.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedWorkoutHistoryController!.sections![section].numberOfObjects
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return self.tableView(tableView, workoutCellForRowAtIndexPath: indexPath) as UITableViewCell
    }
    
    // MARK: - Helpers
    private func configureWorkoutHistoryCell(workoutHistoryCell: FASWorkoutHistoryCell, atIndexPath indexPath: NSIndexPath) {
        let historyObject = fetchedWorkoutHistoryController!.objectAtIndexPath(indexPath) as! WorkoutHistory
        workoutHistoryCell.workoutHistory = historyObject
    }
    
    private func tableView(tableView: UITableView, workoutCellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let aWorkoutCell = tableView.dequeueReusableCellWithIdentifier(WorkoutHistoryViewControllerCellIdentifier.History.rawValue, forIndexPath: indexPath) as! FASWorkoutHistoryCell
        
        configureWorkoutHistoryCell(aWorkoutCell, atIndexPath: indexPath)
        return aWorkoutCell
    }
    
    private func updateTableView() {
        self.tableView.reloadData()
    }
}

extension FASWorkoutsHistoryViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.clearColor()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
    }
    
    // MARK: TableView Helpers
    
    func selectFirstRow(tableView tv: UITableView, segueIdentifier: String?) {
        let indexPathToSelect = NSIndexPath(forRow: 0, inSection: 0)
        tv.selectRowAtIndexPath(indexPathToSelect, animated: true, scrollPosition: UITableViewScrollPosition.None)
        tv.delegate?.tableView?(tv, didSelectRowAtIndexPath: indexPathToSelect)
    }
}

///////////////////////////////////////////////
// MARK: - NSFetchedResultsControllerDelegate -
///////////////////////////////////////////////
extension FASWorkoutsHistoryViewController: NSFetchedResultsControllerDelegate {
    /*
    override func controllerWillChangeContent(controller: NSFetchedResultsController) {
    tableView.beginUpdates()
    }
    
    override func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
    switch type {
    case .Insert:
    tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Middle)
    case .Delete:
    tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Middle)
    default:
    0
    }
    }
*/
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        updateTableView()
    }
    
}

///////////////////////////////////////////////
// MARK: - FASData Observer Protocol -
///////////////////////////////////////////////
extension FASWorkoutsHistoryViewController: FASDataObserver {
    func fasDataNotification(notification: NSNotification) {

        dispatch_async(dispatch_get_main_queue(),{
            if notification.name == FASDataWorkoutHistoryWillUpdateNotification {
                //fasDataWorkoutsWillUpdate()
            }
            else if notification.name == FASDataWorkoutHistoryDidUpdateNotification {
                self.updateTableView()
            }
            else if notification.name == FASDataWorkoutHistoryUpdateDidFailNotification {
                //fasDataWorkoutsDidFailToUpdate(notification)
            }
        })
    }
}