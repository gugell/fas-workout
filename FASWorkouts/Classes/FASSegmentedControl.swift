//
//  FASSegmentedControl.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/5/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

class FASSegmentedControl: UISegmentedControl {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        let segmentTitleFont = UIFont.boldBabasFontOfSize(25.0)
        let segmentNormalColor = UIColor.fasGrayColor94()
        let segmentSelectedColor = UIColor.fasYellowColor()
        let segmentBackgroundColor = UIColor.fasGrayColor62()
        
        let attribsNormal = [
            NSFontAttributeName: segmentTitleFont,
            NSForegroundColorAttributeName: segmentNormalColor,
            NSBackgroundColorAttributeName: segmentBackgroundColor
        ]
        
        let attribsSelected = [
            NSFontAttributeName: segmentTitleFont,
            NSForegroundColorAttributeName: segmentSelectedColor,
            NSBackgroundColorAttributeName: segmentBackgroundColor
        ]
        
        setTitleTextAttributes(attribsNormal, forState: .Normal)
        setTitleTextAttributes(attribsSelected, forState: .Selected)
        
        let divImg = UIImage(named:"TransparentPixel")
        
        setDividerImage(divImg, forLeftSegmentState: .Normal, rightSegmentState: .Normal, barMetrics: .Default)
        setDividerImage(divImg, forLeftSegmentState: .Selected, rightSegmentState: .Normal, barMetrics: .Default)
        setDividerImage(divImg, forLeftSegmentState: .Normal, rightSegmentState: .Selected, barMetrics: .Default)
        
        setContentPositionAdjustment(UIOffset(horizontal: -5.0, vertical: 0.0), forSegmentType: .Left, barMetrics: .Default)
        setContentPositionAdjustment(UIOffset(horizontal: 5.0, vertical: 0.0), forSegmentType: .Right, barMetrics: .Default)
        setContentPositionAdjustment(UIOffset(horizontal: 0.0, vertical: 0.0), forSegmentType: .Center, barMetrics: .Default)

        // expanding the middle segmnt, so the title is not truncated
        // TODO: customize this behaviour in order to eliminate the hardcoded value
        setWidth(120, forSegmentAtIndex: 1)
    }
}
