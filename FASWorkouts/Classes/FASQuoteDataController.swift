//
//  FASQuoteDataController.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/9/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import Foundation

class FASQuoteDataController {
    var quotes: [Quote] = []
    
    init(filename: NSString) {
        var rawData: NSArray?
        if let path = NSBundle.mainBundle().pathForResource(filename as String, ofType: "plist") {
            rawData = NSArray(contentsOfFile: path)
        }
        
        if let quotesArray = rawData {
            createQuotes(quotesArray)
        }
    }
    
    private func createQuotes(dataArrray: NSArray) {
        if let quotesArray = dataArrray as? [[String:String]] {
            for quoteDict in quotesArray {
                let quoteText = quoteDict["text"]
                self.quotes.append( Quote(text: quoteText!) )
            }
        }
    }
}

extension FASQuoteDataController: UBISlideViewDataSource {
    func numberOfItemsForSlideView(slideView: UBISlideView) -> Int {
        return quotes.count
    }
    
    func slideView(slideView: UBISlideView, itemForIndex index: Int) -> UBISlideViewItem {
        return quotes[index]
    }
}

class Quote: UBISlideViewItem {
    var text: NSString
    
    init(text: NSString) {
        self.text = text
    }
}