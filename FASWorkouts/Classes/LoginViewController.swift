//
//  LoginViewController.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/11/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

private var kLoggingIn = 0 // a global context variable used for KVO

class LoginViewController: FASAuthenticationViewController {
    
    var signInButtonCellRow: Int {
        get {
            return fieldsCellCount
        }
    }
    
    // the semantic of crendentials is realy all about [String: String?]
    // however, to accomodate NSDictionary type, we use the following notation
    var credentials: [String: AnyObject] {
        get {
            // NSDictionay does not allow 'nil' to be passed as object, so we add empty string if so
//            return [FASAPIUsernameParam: (username != nil ? username : "" )!,
//                    FASAPIPasswordParam: (password != nil ? password : "" )!]
            
            return [FASAPIUsernameParam: username!,
                    FASAPIPasswordParam: password!]
        }
    }
    
    var usernameTextField: FASTextField?
    var passwordTextField: FASTextField?
    
    var username: String?
    var password: String?
    
    override init(sessionUIDelegate: SessionUIDelegate) {
        super.init(sessionUIDelegate: sessionUIDelegate)
        fieldsCellCount = 2
        
        // set initial credentials from session
        username = FASAPISession.currentSession().username
        password = FASAPISession.currentSession().password
        
        FASAPISession.currentSession().addObserver(self, forKeyPath: "loggingIn", options: .New, context: &kLoggingIn)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        FASAPISession.currentSession().removeObserver(self, forKeyPath: "loggingIn", context: &kLoggingIn)
        
        let notifCenter = NSNotificationCenter.defaultCenter()
        notifCenter.removeObserver(self)
    }
    ///////////////////////////////////////////////
    // MARK: - Concrete Actions -
    ///////////////////////////////////////////////
    private func doLogin() {
        printlnDebugTrace("doLogin")
        
        hideKeyboard()
        
        username = usernameTextField!.text
        password = passwordTextField!.text
        
        FASAPISession.currentSession().loginWithCredentials(credentials)
    }
    
    private func doForgotPassword() {
        printlnDebugTrace("doForgotPassword")
        // TODO: Implement this
    }
    
    override func doFacebookLogin() {
        hideKeyboard()
        super.doFacebookLogin()
    }
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if context == &kLoggingIn {
            handleLoggingInChange()
        } else {
            super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)
        }
    }
    
    private func handleLoggingInChange() {
        if isViewLoaded() {
            dispatch_async(dispatch_get_main_queue(),{
                let busy = FASAPISession.currentSession().loggingIn
                self.navigationItem.backBarButtonItem?.enabled = !busy
                
                let asyncIP  = NSIndexPath(forRow: self.signInButtonCellRow, inSection: AuthenticationFormTableViewSection.Fields.rawValue)
                self.tableView.reloadRowsAtIndexPaths([asyncIP], withRowAnimation: .None)
            })
        }
    }
    
    private func hideKeyboard() {
        // WORKAROUND: to fix <Issue #51: "sign up" and "sign in" form not fixed on screens>
        // we don't hide keyboard and let the iOS handle it. There are basically two circumstances when iOS hides it:
        // 1. when UIAlertView object is shown
        // 2. when VC is deallocated
        
        // uncomment to let the app to handle keyboard hiding
//        usernameTextField!.isFirstResponder() ? usernameTextField!.resignFirstResponder() : passwordTextField!.resignFirstResponder()
    }
}

extension LoginViewController: UITableViewDataSource {

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        if let sectionNumber = AuthenticationFormTableViewSection(rawValue: indexPath.section)  {
            switch sectionNumber {
            case .Title:
                // title
                cell = self.tableView(tableView, titleCellForIndexPath: indexPath)
            case .Fields:
                if indexPath.row != signInButtonCellRow {
                    // fields
                    cell = self.tableView(tableView, textFieldCellForIndexPath: indexPath)
                }
                else {
                    // sign in button
                    cell = self.tableView(tableView, asyncActionCellForIndexPath: indexPath)
                }
            case .Facebook:
                // facebook button
                cell = self.tableView(tableView, facebookCellForIndexPath: indexPath)
            }
        }
        else {
            assert(false, "[ASSERTION]: cell will be nil")
            cell = nil
        }
        
        return cell!
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sectionNumber = AuthenticationFormTableViewSection(rawValue: section)  {
            switch sectionNumber {
            case .Title:
                return 1
            case .Fields:
                return fieldsCellCount + 1 // +sign in button
            case .Facebook:
                return 1
            }
        }
        else {
            assert(false, "[ASSERTION]: cell will be nil")
            return 0
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    // TableView DataSource Helpers
    func tableView(tableView: UITableView, textFieldCellForIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(textFieldCellIdentifier) as! FASTextFieldTableViewCell
        if (cell.tag == 0) {
            let notifCenter = NSNotificationCenter.defaultCenter()
            notifCenter.addObserver(self, selector: "textFieldDidChange:", name: UITextFieldTextDidChangeNotification, object: cell.textField)
            cell.textField.delegate = self
            cell.textField.placeholderColor = UIColor.whiteColor()
            cell.textField.font = UIFont.regularBabasFontOfSize(25.0)
            cell.textField.keyboardAppearance = .Dark
            cell.tag = 1
        }
        
        switch indexPath.row {
        case 0: // username
            cell.leftImageView.image = UIImage(named: "IconUser")
            cell.textField.placeholder = "LOGIN"
            usernameTextField = cell.textField
            usernameTextField!.text = username
            usernameTextField!.secureTextEntry = false
            cell.textField.returnKeyType = .Next
            cell.textField.keyboardType = .EmailAddress
            
        case 1: // password
            cell.leftImageView.image = UIImage(named: "IconPassword")
            cell.textField.placeholder = "PASSWORD"
            passwordTextField = cell.textField
            passwordTextField!.text = password
            passwordTextField!.secureTextEntry = true
            cell.textField.returnKeyType = .Join
            cell.textField.keyboardType = .Default
            
        default:
            printlnDebugWarning("Cannot configure view because of unexpected row")
        }
        
        cell.textField.setTransparencyIfNeeded()
        
        return cell
    }
    
    func tableView(tableView: UITableView, titleCellForIndexPath indexPath: NSIndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCellWithIdentifier(titleCellIdentifier) as! FASTitleTableViewCell
        if cell.tag == 0 {
            cell.titleLabel.text = "WELCOME BACK"
            cell.titleLabel.textColor = UIColor.whiteColor()
            cell.tag = 1
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, asyncActionCellForIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(asyncActionCellIdentifier) as! FASAsynchronousActionTableViewCell
        if cell.tag == 0 {
            cell.titleLabel.text = "SIGN IN"
            cell.titleLabel.textColor = UIColor.yellowColor()
            cell.titleLabel.font = UIFont.boldBabasFontOfSize(25.0)
            cell.tag = 1
        }

        cell.busy = FASAPISession.currentSession().loggingIn
        cell.userInteractionEnabled = !FASAPISession.currentSession().loggingIn;
        
        return cell
    }
}

extension LoginViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.clearColor()
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int)  -> CGFloat {
        let footerHeight: CGFloat = 4.0
        return footerHeight
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if let sectionNumber = AuthenticationFormTableViewSection(rawValue: indexPath.section)  {
            if sectionNumber == .Fields && indexPath.row == signInButtonCellRow {
                doLogin()
            }
            
            else if sectionNumber == .Facebook {
                doFacebookLogin()
            }
        }
    }
    
    func tableView(tableView: UITableView, shouldHighlightRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if let sectionNumber = AuthenticationFormTableViewSection(rawValue: indexPath.section)  {
            // highlight SIGN IN and CONNECT WITH FACEBOOK rows
            if  (sectionNumber == .Fields && indexPath.row == signInButtonCellRow)
                || sectionNumber == .Facebook
            {
                return true
            }
        }
        return false
    }
    
    func tableView(tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        // footers are used for decoration only, so transparent
        view.alpha = 0.0
    }
}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField === passwordTextField {
            doLogin()
        }
        else {
            passwordTextField?.becomeFirstResponder()
        }
        
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        // TODO: prevent non-english input
        
        // preventing whitespace input
        let whitespaceSet = NSCharacterSet.whitespaceCharacterSet()
        let range = string.rangeOfCharacterFromSet(whitespaceSet)
        if let _ = range {
            return false
        }
        else {
            return true
        }
    }
    
    // MARK: NSNotificationCenter callback
    
    func textFieldDidChange(notification: NSNotification) {
        if let textField = notification.object as? UITextField {
            textField.setTransparencyIfNeeded()
        }
    }
}