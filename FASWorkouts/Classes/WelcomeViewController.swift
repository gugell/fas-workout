//
//  WelcomeViewController.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/12/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

enum WelcomeViewControllerFinishOptions {
    case Start
    case Resume
}

protocol WelcomeViewControllerDelegate {
    func welcomeViewController(viewController: WelcomeViewController, didFinishWithOption option: WelcomeViewControllerFinishOptions)
}

class WelcomeViewController: FASViewController {
    @IBOutlet weak var helloLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var resumeButton: UIButton!
    @IBOutlet weak var quoteSlider: UBISlideView!
    
    var delegate: WelcomeViewControllerDelegate?
    var allowResume: Bool = false {
        didSet {
            configureResumeButton(hidden: !allowResume)
        }
    }
    
    private var currentFinishOption: WelcomeViewControllerFinishOptions?
    private var loginInfoViewController: LoginInfoViewController?
    
    override func loadView() {
        self.navigationController?.navigationBar.hidden = true
        
        // load custom 'view' object
        let nibView: Array = NSBundle.mainBundle().loadNibNamed("WelcomeView", owner: self, options: nil)
        if let customView: UIView = nibView.first as? UIView {
            self.view = customView
        }
        
        configureStartButton()
        configureResumeButton()
        
        configureQuoteSlider()
        
        // make it transparent for animation
        helloLabel?.alpha = 0.0
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        configureView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !FASAPISession.currentSession().isLoggedIn() {
            presentLoginInfoViewController(false)
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        quoteSlider.startAnimating( 5.0 )
    }
    
    override func configureView() {
        super.configureView()
        resumeButton?.hidden = !allowResume
    }
    
    override func configureViewWithSession() {
        if let username = FASAPISession.currentSession().userInfo?.email {
            configureHelloLabel(name: username)
        }
    }
    
    override func configureViewWithoutSession() {
        configureHelloLabel(name: nil)
    }
    ///////////////////////////////////////////////
    // MARK: - Button Actions -
    ///////////////////////////////////////////////
    @IBAction func startButtonDidPress(sender: AnyObject) {
        currentFinishOption = .Start
        finish()
    }
    
    @IBAction func resumeButtonDidPress(sender: AnyObject) {
        currentFinishOption = .Resume
        finish()
    }
    ///////////////////////////////////////////////
    // MARK: - Helpers -
    ///////////////////////////////////////////////
    private func configureStartButton(hidden isHidden: Bool = false) {
        startButton?.hidden = isHidden
        if isHidden == false {
            FASViewHelper.applyStyle(FASViewHelperButtonStyle.Filled, toButton: startButton)
        }
    }
    
    private func configureResumeButton(hidden isHidden: Bool = false) {
        resumeButton?.hidden = isHidden
        if isHidden == false {
            FASViewHelper.applyStyle(FASViewHelperButtonStyle.Stroked, toButton: resumeButton)
        }
    }
    
    private func configureQuoteSlider() {
        quoteSlider.dataSource = FASQuoteDataController(filename: "WelcomeScreenQuotes")
        quoteSlider.hidden = false
    }
    
    private func configureHelloLabel(name name: String?) {
        // TODO: localize this
        let attributedHello = NSMutableAttributedString(string:"HELLO, ")
        let attrs = [NSFontAttributeName : UIFont.boldBabasFontOfSize( 38.0 ),
                    NSForegroundColorAttributeName: UIColor.fasYellowColor()]
        var attributedName: NSMutableAttributedString
        if let concreteName = name {
            attributedName = NSMutableAttributedString(string:concreteName, attributes:attrs)
        } else {
            attributedName = NSMutableAttributedString(string:"GUEST", attributes:attrs)
        }
        attributedHello.appendAttributedString(attributedName)
        
        helloLabel?.attributedText = attributedHello

        // animate
        UIView.animateWithDuration(NSTimeInterval(1.0), animations: { ()-> Void in
            self.helloLabel?.alpha = 1.0
            return
        })
    }
    
    private func presentLoginInfoOrFinish() {
        if !FASAPISession.currentSession().isLoggedInAsUser() {
            presentLoginInfoViewController(true)
        }
        else {
            finish()
        }
    }
    
    private func finish() {
        quoteSlider.stopAnimating()
        
        if let d = delegate {
            d.welcomeViewController(self, didFinishWithOption: currentFinishOption!)
        }
        else {
            dismissViewControllerAnimated(true, completion: nil)
        }
        
        delegate = nil
        currentFinishOption = nil
    }
    
    private func finishNewUserFlow() {
        if !FASAPISession.currentSession().isLoggedInAsUser() {
//            presentLoginInfoViewController(false)
            assert(false, "not logged in as user - why???");
        }
    }
    
    private func presentLoginInfoViewController(animated: Bool) {
        loginInfoViewController = LoginInfoViewController(sessionUIDelegate: self)
        if let vc = loginInfoViewController {
            vc.firstTimeUX = true
            vc.hidesSkipButton = true
            let navWelcome: UINavigationController? = UINavigationController(navigationBarClass: FASNavigationBar.self, toolbarClass: UIToolbar.self)
            if let nc = navWelcome {
                nc.viewControllers = [vc]
                presentViewController(nc, animated: false, completion: nil)
            }
        }
    }
}

extension WelcomeViewController: SessionUIDelegate {
    func sessionUIDidFinish(sessionUIController: SessionUIController) {
        loginInfoViewController!.dismissViewControllerAnimated(true, completion: {
            self.finishNewUserFlow()
        })
    }
    
    func sessionUIDidCancel(sessionUIController: SessionUIController) {
        loginInfoViewController!.dismissViewControllerAnimated(true, completion: {
            self.finishNewUserFlow()
        })
    }
}