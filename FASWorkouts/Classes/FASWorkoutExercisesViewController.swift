//
//  FASWorkoutExercisesViewController.swift
//  FASWorkouts
//
//  Created by Vladimir Stepanchenko on 11/12/15.
//  Copyright © 2015 FAS. All rights reserved.
//

import Foundation

class FASWorkoutExercisesViewController : UIViewController {
    let kWorkoutExerciseCelllIdentifier = "workoutExerciseCelllIdentifier"
    
    @IBOutlet var exerciseTableView: UITableView!
    
    var workoutHubViewController : FASWorkoutViewController?
    
    func updateTableView() {
        exerciseTableView.reloadData()
    }
}

///////////////////////////////////////////////
// MARK: - TableView DataSource -
///////////////////////////////////////////////

extension FASWorkoutExercisesViewController: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.workoutHubViewController != nil && self.workoutHubViewController!.workoutController != nil {
            return self.workoutHubViewController!.workoutController!.exercises.count
        } else {
            return 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return self.tableView(tableView, exerciseCellForRowAtIndexPath: indexPath) as UITableViewCell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        // TODO: should be in Extra section when FRC secion calculation is applied
        return 0.0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil;
    }
    
    // MARK: - Helpers
    
    private func configureExerciseCell(exerciseCell: FASExerciseCell, atIndexPath indexPath: NSIndexPath) {
        if self.workoutHubViewController != nil && self.workoutHubViewController!.workoutController != nil {
            let exerciseObject = self.workoutHubViewController!.workoutController!.exercises[indexPath.row]
            exerciseCell.accessoryType = .None
            exerciseCell.exercise = exerciseObject
        }
    }
    
    private func tableView(tableView: UITableView, exerciseCellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let anExerciseCell = tableView.dequeueReusableCellWithIdentifier(kWorkoutExerciseCelllIdentifier, forIndexPath: indexPath) as! FASExerciseCell
        
        configureExerciseCell(anExerciseCell, atIndexPath: indexPath)
        return anExerciseCell
    }
}

///////////////////////////////////////////////
// MARK: - TableView Delegate -
///////////////////////////////////////////////

extension FASWorkoutExercisesViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.clearColor()
    }
}