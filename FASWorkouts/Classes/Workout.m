//
//  Workout.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/30/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "Workout.h"
#import "FASAPIObjectKeys.h"
#import "FASData.h" // <== cross-reference
#import "NSString+MyExtensions.h"
#import "Exercise.h"
#import "FASWorkouts-Swift.h"
#import "UBILogger.h"
// SOProductStatus data type
#import "StoreObserver.h"

NSString * const FASWorkoutDifficultyBeginner       = @"beginner";
NSString * const FASWorkoutDifficultyIntermediate   = @"intermidiate";
NSString * const FASWorkoutDifficultyAdvanced       = @"advanced";

NSString * const FASWorkoutTrainingTypeFunctional   = @"functional";
NSString * const FASWorkoutTrainingTypeTabata       = @"tabata";
NSString * const FASWorkoutTrainingTypeFavourite    = @"favourite";

NSString * const FASWorkoutGenderSpecificTypeAllRawValue       = @"all";
NSString * const FASWorkoutGenderSpecificTypeMenRawValue       = @"men";
NSString * const FASWorkoutGenderSpecificTypeWomenRawValue     = @"women";

// cached formatter(s)
static NSDateFormatter *    sDateFormatter = nil;
static NSNumberFormatter *  sNumberFormatter = nil;

// image cache
static UBIRemoteImageCache *sWorkoutImageCache = nil;

@interface Workout ()

@property (nonatomic, strong, readwrite) NSDate*  createdOn;
@property (nonatomic, strong, readwrite) NSNumber* workout_id;
@property (nonatomic, copy, readwrite) NSString* name;
@property (nonatomic, copy, readwrite) NSString* code;
@property (nonatomic, strong, readwrite) NSNumber* restTime;
//@property (nonatomic, strong, readwrite) NSNumber* timeSpan;
@property (nonatomic, strong, readwrite) NSNumber* lapsNumber;
@property (nonatomic, strong, readwrite) NSString* difficulty;
@property (nonatomic, strong, readwrite) NSString* motivationText;
@property (nonatomic, strong, readwrite) NSDate* mi_modifyDate;
@property (nonatomic, copy, readwrite) NSString* muscleTypes;
@property (nonatomic, strong, readwrite) NSNumber* calories;
@property (nonatomic, strong, readwrite) NSString* gender;
@property (nonatomic, strong, readwrite) NSString* primaryMuscleType;
@property (nonatomic, strong, readwrite) NSNumber* sortIndex;
@property (nonatomic, strong, readwrite) NSNumber* purchaseStatus;
@property (nonatomic, strong, readwrite) NSNumber* dowload_status;
@property (nonatomic, strong, readwrite) NSString* weekDayInfo;
@property (nonatomic, strong, readwrite) NSString* trainingType;
@property (nonatomic, strong, readwrite) NSString* linkToImage;
@property (nonatomic, strong, readwrite) NSOrderedSet *exercises;




@end

@implementation Workout
// attributes
@synthesize currentAllTime;

@dynamic createdOn;
@dynamic baseImageFilename;
@dynamic exerciseCount;
@dynamic workout_id;
@dynamic linkToImage;
@dynamic name;
@dynamic code;
@dynamic restTime;
@synthesize timeSpan;
@dynamic lapsNumber;
@dynamic difficulty;
@dynamic motivationText;
@dynamic mi_modifyDate;
@dynamic muscleTypes;
@dynamic calories;
@dynamic onSale;
@dynamic inAppProductID;
@dynamic gender;
@dynamic primaryMuscleType;
@dynamic sortIndex;
@dynamic purchaseStatus;
@dynamic dowload_status;
@dynamic weekDayInfo;
@dynamic trainingType;



@dynamic exercises;

+ (void)initialize
{
    if (!sWorkoutImageCache) {
        sWorkoutImageCache = [UBIRemoteImageCache new];
        sWorkoutImageCache.imageFilePrefix = @"image_id";
        sWorkoutImageCache.defaultImageName = @"WorkoutDefaultImage";
//        sWorkoutImageCache.loadingImageName = @"workoutLoadingImage";
    }
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Image
#pragma mark -
///////////////////////////////////////////////
+ (UBIRemoteImageCache *)workoutImageCache
{
    return sWorkoutImageCache;
}

- (NSString *)remoteImageKey
{
    return self.baseImageFilename;
}

- (UBIRemoteImage *)remoteImage
{
    if (!self.remoteImageKey) {
        UBILogWarning(@"invalid remoteImageKey in workout (id = %@)", self.workout_id);
        return nil;
    }
    
    UBIRemoteImageCache *remoteImageCache = sWorkoutImageCache;
    
    NSMutableDictionary *loadingInfo = [NSMutableDictionary dictionary];
    if (self.baseImageFilename) {
        [loadingInfo setObject:self.baseImageFilename forKey:kUBIRemoteImageCacheInfoFilename];
    }
    else {
        UBILogWarning(@"invalid 'baseImageFilename' in workout with name: %@, id: %@", self.name, self.workout_id);
    }
    NSURL* iurl = self.imageURL;
    if (iurl) {
        [loadingInfo setObject:iurl forKey:kUBIRemoteImageCacheInfoURL];
    }
    [loadingInfo setObject:[NSValue valueWithCGSize:CGSizeMake(160, 160)] forKey:kUBIRemoteImageCacheInfoMaxSize];
    
    return [remoteImageCache
            remoteImageForKey:self.remoteImageKey
            loadingInfo:loadingInfo];
}

- (NSURL *)imageURL
{
    // TODO: FASData dependency should be eliminated from this class
    //NSURL *imageURL = [FASData.sharedData imageURLForWorkoutWithID:self.workout_id];
    NSString * urlString = [self valueForKey:@"linkToImage"];
    
    NSURL *imageURL;
    
    if(!urlString)  return nil;
    
    if ([urlString hasPrefix:@"http://"] || [urlString hasPrefix:@"https://"]) {
        imageURL = [NSURL URLWithString:urlString];
    } else {
        imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@", urlString]];
    }
    
    
    if ([self.trainingType isEqualToString:@"tabata"]) {
        UBILogTrace(@">>>> strPath = %@", [imageURL absoluteString]);
    }
    return imageURL;
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark UBIUpdatableWithDictionary protocol conformance
#pragma mark -
///////////////////////////////////////////////
- (void)addValuesFromDictionary:(NSDictionary *)dict
{
    for (NSString *key in [dict allKeys]) {
        
        id value = [dict valueForKey:key];
        
        // if value is an array
        if ([value isKindOfClass:[NSArray class]]) {
            UBILogWarning(@"Value (%@) is an array", value);
            continue;
        }
        
        // if value is a dictionary
        // else, add it directly
        else {
            
            if ([value isKindOfClass:[NSNull class]]) { continue; }
            
            // date / time
            // TODO: substitute w/ NSDate UBIDate extension
            if ([key isEqualToString:FASAPImiModifyDate]) {
                if (!sDateFormatter) {
                    sDateFormatter = [NSDateFormatter new];
                    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
                    [sDateFormatter setLocale:enUSPOSIXLocale];
                    [sDateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
                    [sDateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                }
                NSDate *mi_modifyDate = [sDateFormatter dateFromString:value];
                self.mi_modifyDate = mi_modifyDate;
                
                continue;
            } else if ([key isEqualToString:FASAPIIdentifier]) {
                // mapping between UnityBase's 'ID' and model's 'identifier'
                self.workout_id = dict[FASAPIIdentifier];
                
                continue;
            } else if ([key isEqualToString:FASAPIMuscleTypes]) {
                [self setValue:value forKey:@"muscleTypes"];
                
                continue;
            }
            else if ([key isEqualToString:FASAPIPrimaryMuscleType]) {
                [self setValue:value forKey:@"primaryMuscleType"];
                
                continue;
            } else if ([key isEqualToString:FASAPILinkToImageURL]) {
             
                self.linkToImage = value;
                continue;
            }
            
            [self setValue:value forKey:key];
        }
    }
}

- (void)addExercisesFromArray:(NSArray *)collection
{
    for (id objDictionary in collection) {
        @autoreleasepool {
            Exercise *newObj = (Exercise *)[NSEntityDescription insertNewObjectForEntityForName:@"Exercise"
                                                                       inManagedObjectContext:self.managedObjectContext];
            [newObj addValuesFromDictionary:objDictionary];
            // Cached image filename created locally
            newObj.baseImageFilename = [NSString stringWithFormat:@"%@", newObj.exercise_id];
            [self addExercisesObject:newObj];
        }
    }
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark NSKeyValueCoding protocol
#pragma mark -
///////////////////////////////////////////////

- (id)valueForUndefinedKey:(NSString *)key
{
    return nil;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    UBILogWarning(@"%@: UNDEFINED value = %@\tkey = %@", NSStringFromClass([self class]), value, key);
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Purchase
#pragma mark -
///////////////////////////////////////////////

- (BOOL)isOnSale
{
    return [self.onSale boolValue];
}

- (BOOL)isAvailable
{
    if (![self isPaid]) {
        return YES;
    }
    else if ([self isPurchased]) {
        return YES;
    }
    return NO;
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Gender
#pragma mark -
///////////////////////////////////////////////
- (FASWorkoutGenderSpecificType)genderSpecific
{
    if (self.gender != nil) {
        if ([self.gender isEqualToString:FASWorkoutGenderSpecificTypeMenRawValue]) {
            return FASWorkoutGenderSpecificTypeMen;
        }
        else if ([self.gender isEqualToString:FASWorkoutGenderSpecificTypeWomenRawValue]) {
            return FASWorkoutGenderSpecificTypeWomen;
        }
    }
    return FASWorkoutGenderSpecificTypeAll;
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Download Status
#pragma mark -
///////////////////////////////////////////////

- (WorkoutDownloadStatus)downloadStatus
{
    return [self.dowload_status integerValue];
}

- (void)setDownloadStatus:(WorkoutDownloadStatus)newStatus
{
    if (self.downloadStatus != newStatus) {
        self.dowload_status = @(newStatus);
    }
}

@end
///////////////////////////////////////////////
#pragma mark -
#pragma mark Extension NSManagedObject
#pragma mark -
///////////////////////////////////////////////
@implementation Workout (NSManageedObjectOverrides)

- (void)awakeFromInsert {
    [super awakeFromInsert];
    self.createdOn = [NSDate date];
    self.downloadStatus = WorkoutDownloadStatusNone;
}

@end
///////////////////////////////////////////////
#pragma mark -
#pragma mark Extension Format
#pragma mark -
///////////////////////////////////////////////
@implementation Workout (Format)

- (NSString *)formattedMuscleTypes {
    if (self.muscleTypes != nil || [self.muscleTypes isBlank]) {
        NSArray* mtArr = [self.muscleTypes componentsSeparatedByString:@","];
        NSString* formattedMuscleTypes = [mtArr componentsJoinedByString:@" | "];
        return formattedMuscleTypes.lowercaseString;
    }
    // TODO: localize this
    return @"no muscle types";
}

- (NSString *)formattedPrimaryMuscleType
{
    if (self.primaryMuscleType != nil || [self.primaryMuscleType isBlank]) {
        return self.primaryMuscleType;
    }
    // TODO: localize this
    return @"n/a";
}

- (NSString *)formattedCalories
{
    return [NSString stringWithFormat:@"%lical", (long) [self.calories integerValue]];
}

- (NSString *)formattedGenderSpecific
{
    NSString* formattedString = nil;
    // TODO: localize this
    switch ([self genderSpecific]) {
        case FASWorkoutGenderSpecificTypeMen: {
            formattedString = @"men friendly";
        } break;
        case FASWorkoutGenderSpecificTypeWomen: {
            formattedString = @"women friendly";
        } break;
        default: {
            formattedString = @"men & women friendly";
        } break;
    }
    return formattedString;
}

- (NSString *)formattedAvailabilityStatus
{
    if (self.isAvailable) {
        return @"";
    }
    else {
        return @"UNLOCK";
    }
}
@end

///////////////////////////////////////////////
#pragma mark -
#pragma mark FASModifyDate Protocol Conformance
#pragma mark -
///////////////////////////////////////////////
@implementation Workout (FASModifyDateConformance)
- (NSNumber *)uniqueID
{
    return self.workout_id;
}

- (NSDate *)modifyDate
{
    return self.mi_modifyDate;
}
@end
///////////////////////////////////////////////
#pragma mark -
#pragma mark FASImageAndTwoLabelsHeaderViewData
#pragma mark -
///////////////////////////////////////////////

#import "NSString+MyExtensions.h"

@implementation Workout (FASImageAndTwoLabelsHeaderViewData)

- (NSNumberFormatter *)headerViewNumberFormatter {
    if (!sNumberFormatter) {
        sNumberFormatter = [NSNumberFormatter new];
    }
    [sNumberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    return sNumberFormatter;
}

- (NSString *)leftTitle {
    // TODO: localize this
    return [@"rounds" copy];
}

- (NSString *)leftValue {
    return [[[self headerViewNumberFormatter] stringFromNumber: self.lapsNumber] copy];
}

- (NSString *)rightTitle {
    // TODO: localize this
    return [@"time" copy];
}

- (NSString *)rightValue {
  
    self.timeSpan = self.currentAllTime;
    return [NSString formattedTimeFromSeconds: self.currentAllTime];
    
}

- (UBIRemoteImage *)image {
    return self.remoteImage;
}

- (NSString *)imageTitle {
    return [self formattedAvailabilityStatus];
}

@end