//
//  FAQDetailsViewController.swift
//  FASWorkouts
//
//  Created by Vladimir Stepanchenko on 11/13/15.
//  Copyright © 2015 FAS. All rights reserved.
//

import Foundation

class FAQDetailsViewController : FASViewController {
    
    @IBOutlet weak var contentTextView: UITextView!
    
    var faqEntry : FAQItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "FAQ"
        hideBackButtonTitle = true
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        configureNavigationBar(.Dark)
        
        if faqEntry != nil {
            let resultString = NSMutableAttributedString()
            // Question
            let questionAttributes = [NSFontAttributeName : UIFont.boldBabasFontOfSize(32), NSForegroundColorAttributeName : UIColor.fasYellowColor()]
            let questionString = NSMutableAttributedString(string: "\(faqEntry!.question)\n\n", attributes: questionAttributes)
            resultString.appendAttributedString(questionString)
            // Answer
            let answerAttributes = [NSFontAttributeName : UIFont.regularBabasFontOfSize(21), NSForegroundColorAttributeName : UIColor.whiteColor()]
            let answerString = NSMutableAttributedString(string: "\(faqEntry!.answer)", attributes: answerAttributes)
            resultString.appendAttributedString(answerString)
            contentTextView.attributedText = resultString
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        configureNavigationBar(.DarkWithYellowTitle)
        super.viewWillDisappear(animated)
    }
}