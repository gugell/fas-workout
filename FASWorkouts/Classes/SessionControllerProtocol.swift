//
//  SessionControllerProtocol.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/11/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

// Controllers integrating a SessionController must support this protocol to receive
// messages from the SessionController
protocol SessionControllerDelegate {
    func sessionControllerDidCancel(sessionController: SessionController)
    func sessionControllerDidFinish(sessionController: SessionController)
}

protocol SessionUIController {
    var sessionUIDelegate: SessionUIDelegate? { set get }
}

// Controllers directly integrating a session UI controller outside of SessionController
// must support this protocol to receive messages from the UI controller
protocol SessionUIDelegate {
    func sessionUIDidCancel(sessionUIController: SessionUIController)
    func sessionUIDidFinish(sessionUIController: SessionUIController)
}