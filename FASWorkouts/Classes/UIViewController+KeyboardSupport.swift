//
//  UIViewController+KeyboardSupport
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/23/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

protocol KeyboardSupport {
    func registerForKeyboardNotifications()
    func unregisterForKeyboardNotifications()
    
    func keyboardWillHide(notification: NSNotification)
    func keyboardWillShow(notification: NSNotification)
    func keyboardDidHide(notification: NSNotification)
    func keyboardDidShow(notification: NSNotification)
}

extension UIViewController: KeyboardSupport {

    func registerForKeyboardNotifications() {
        addKeyboardObserver("keyboardDidHide:", UIKeyboardDidHideNotification)
        addKeyboardObserver("keyboardDidShow:", UIKeyboardDidShowNotification)
        addKeyboardObserver("keyboardWillHide:", UIKeyboardWillHideNotification)
        addKeyboardObserver("keyboardWillShow:", UIKeyboardWillShowNotification)
    }
    
    func unregisterForKeyboardNotifications() {
        removeKeyboardObserver("keyboardDidHide:")
        removeKeyboardObserver("keyboardDidShow:")
        removeKeyboardObserver("keyboardWillHide:")
        removeKeyboardObserver("keyboardWillShow:")
    }
    
    func keyboardWillHide(notification: NSNotification) {}
    func keyboardWillShow(notification: NSNotification) {}
    func keyboardDidHide(notification: NSNotification) {}
    func keyboardDidShow(notification: NSNotification) {}
    
    // Helpers
    private func addKeyboardObserver(sel: Selector, _ name: String) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: sel, name: name, object: nil)
    }
    
    private func removeKeyboardObserver(name: String) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: name, object: nil)
    }
}
