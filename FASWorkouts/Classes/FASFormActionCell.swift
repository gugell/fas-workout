//
//  FASFormActionCell.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 8/31/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit

class FASFormActionCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
}