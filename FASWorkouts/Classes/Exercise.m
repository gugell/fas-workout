//
//  Exercise.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/10/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "Exercise.h"
#import "Attach.h"
#import "FASWorkouts-Swift.h"
#import "UBILogger.h"

// image cache
static UBIRemoteImageCache *sExerciseImageCache = nil;
static UBIRemoteImageCache *sExerciseIconCache = nil;

NSString* const FASAPIExerciseWorkoutExerciseIDCodeKey = @"ID";
NSString* const FASAPIExerciseIDKey = @"exerciseID";
NSString* const FASAPIExerciseIDCodeKey = @"exerciseID.code";
NSString* const FASAPIExerciseIDMuscleTypesNameKey = @"exerciseID.muscleTypes.name";
NSString* const FASAPIExerciseIDNameKey = @"exerciseID.name";
NSString* const FASAPIExerciseLapsNumberKey = @"lapsNumber";
NSString* const FASAPIExerciseRestTimeKey = @"restTime";
NSString* const FASAPIExerciseTimeSpanKey = @"timeSpan";
NSString* const FASAPIExerciseWorkoutIDKey = @"workoutID";
NSString* const FASAPIExerciseDescription = @"exerciseID.description";
NSString* const FASAPIExerciseLinkToIconKey = @"exerciseID.linkToIcon";
NSString* const FASAPIExerciseLinkToImageKey = @"exerciseID.linkToPhoto";

//NSString* const FASAPIWorkoutExerciseIDKey = @"ID";

// string constant from server
NSString * const FASWorkoutPositionInSupersetTypeTopRawValue      = @"start";
NSString * const FASWorkoutPositionInSupersetTypeMiddleRawValue     = @"middle";
NSString * const FASWorkoutPositionInSupersetTypeBottomRawValue        = @"end";

@interface Exercise ()
@property (nonatomic, retain, readwrite) NSNumber * workoutExerciseID;
@property (nonatomic, retain, readwrite) NSDate * createdOn;
@property (nonatomic, retain, readwrite) NSNumber * exercise_id;
@property (nonatomic, retain, readwrite) NSNumber * restTime;
@property (nonatomic, strong, readwrite) NSNumber* timeSpan;
@property (nonatomic, strong, readwrite) NSNumber* lapsNumber;
@property (nonatomic, strong, readwrite) NSNumber* isStatic;
@property (nonatomic, copy, readwrite) NSString* code;
@property (nonatomic, copy, readwrite) NSString* name;
@property (nonatomic, copy, readwrite) NSString* exerciseDescription;
@property (nonatomic, copy, readwrite) NSString* muscleTypes;
@property (nonatomic, strong, readwrite) NSNumber* workout_id;
@property (nonatomic, strong, readwrite) NSDate* mi_modifyDate;
@property (nonatomic, copy, readwrite) NSString* positionInSuperset;
@property (nonatomic, strong, readwrite) NSNumber* sortIndex;
@property (nonatomic, strong, readwrite) NSNumber* isCountable;
@property (nonatomic, copy, readwrite) NSString* linkToPhoto;
@property (nonatomic, copy, readwrite) NSString* linkToIcon;
@end

@implementation Exercise
// attributes
@dynamic baseImageFilename;
@dynamic workoutExerciseID;
@dynamic createdOn;
@dynamic exercise_id;
@dynamic restTime;
@dynamic timeSpan;
@dynamic lapsNumber;
@dynamic muscleTypes;
@dynamic code;
@dynamic name;
@dynamic exerciseDescription;
@dynamic workout_id;
@dynamic attachCount;
@dynamic isStatic;
@dynamic mi_modifyDate;
@dynamic sortIndex;
@dynamic positionInSuperset;
@dynamic isCountable;

// relationships
@dynamic workout;
@dynamic preferredAttachment;
@dynamic attachments;

+ (void)initialize
{
    if (!sExerciseImageCache) {
        sExerciseImageCache = [UBIRemoteImageCache new];
        sExerciseImageCache.imageFilePrefix = @"image_id_exercise";
        //@note left WorkoutDefaultImage here
        sExerciseImageCache.defaultImageName = @"WorkoutDefaultImage";
        //        sWorkoutImageCache.loadingImageName = @"workoutLoadingImage";
    }
    
    if(!sExerciseIconCache){
        sExerciseIconCache = [UBIRemoteImageCache new];
        sExerciseIconCache.imageFilePrefix = @"icon_id_exercise";
        sExerciseIconCache.defaultImageName = @"WorkoutDefaultImage";
    }
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark CoreData overrides
#pragma mark -
///////////////////////////////////////////////
- (void)awakeFromInsert
{
    [super awakeFromInsert];
    self.createdOn = [NSDate date];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Image
#pragma mark -
///////////////////////////////////////////////
+ (UBIRemoteImageCache *)exerciseImageCache
{
    return sExerciseImageCache;
}

+ (UBIRemoteImageCache *)exerciseIconCache
{
    return sExerciseIconCache;
}

- (NSString *)remoteImageKey
{
    return self.baseImageFilename;
}

- (UBIRemoteImage *)remoteIcon
{
    if (!self.remoteImageKey) {
        UBILogWarning(@"invalid remoteImageKey in exercise (id = %@)", self.exercise_id);
        return nil;
    }
    
    UBIRemoteImageCache *remoteImageCache = sExerciseIconCache;
    
    NSMutableDictionary *loadingInfo = [NSMutableDictionary dictionary];
    if (self.baseImageFilename) {
        [loadingInfo setObject:self.baseImageFilename forKey:kUBIRemoteImageCacheInfoFilename];
    }
    else {
        UBILogWarning(@"invalid 'baseImageFilename' in exercise with name: %@, id: %@", self.name, self.exercise_id);
    }
    NSURL* iurl = self.iconURL;
    if (iurl) {
        [loadingInfo setObject:iurl forKey:kUBIRemoteImageCacheInfoURL];
    }
    [loadingInfo setObject:[NSValue valueWithCGSize:CGSizeMake(160, 160)] forKey:kUBIRemoteImageCacheInfoMaxSize];
    
    return [remoteImageCache
            remoteImageForKey:self.remoteImageKey
            loadingInfo:loadingInfo];
}

- (UBIRemoteImage *)remoteImage
{
    if (!self.remoteImageKey) {
        UBILogWarning(@"invalid remoteImageKey in exercise (id = %@)", self.exercise_id);
        return nil;
    }
    
    UBIRemoteImageCache *remoteImageCache = sExerciseImageCache;
    
    NSMutableDictionary *loadingInfo = [NSMutableDictionary dictionary];
    if (self.baseImageFilename) {
        [loadingInfo setObject:self.baseImageFilename forKey:kUBIRemoteImageCacheInfoFilename];
    }
    else {
        UBILogWarning(@"invalid 'baseImageFilename' in exercise with name: %@, id: %@", self.name, self.exercise_id);
    }
    NSURL* iurl = self.imageURL;
    if (iurl) {
        [loadingInfo setObject:iurl forKey:kUBIRemoteImageCacheInfoURL];
    }
    [loadingInfo setObject:[NSValue valueWithCGSize:CGSizeMake(160, 160)] forKey:kUBIRemoteImageCacheInfoMaxSize];
    
    return [remoteImageCache
            remoteImageForKey:self.remoteImageKey
            loadingInfo:loadingInfo];
}
/*
- (NSURL *)imageURL
{
    // TODO: FASData dependency should be eliminated from this class too?)
    NSURL *imageURL = [FASData.sharedData imageURLForExerciseID:self.exercise_id];
    return imageURL;
}
*/


- (NSURL *)imageURL
{
    // TODO: FASData dependency should be eliminated from this class
    //NSURL *imageURL = [FASData.sharedData imageURLForWorkoutWithID:self.workout_id];
    NSString * urlString = self.linkToPhoto;
    
    NSURL *imageURL;
    
    if(!urlString)  return nil;
    
    if ([urlString hasPrefix:@"http://"] || [urlString hasPrefix:@"https://"]) {
        imageURL = [NSURL URLWithString:urlString];
    } else {
        imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@", urlString]];
    }
    
    return imageURL;
}

- (NSURL *)iconURL
{
    // TODO: FASData dependency should be eliminated from this class too?)
    /*NSURL *imageURL = [FASData.sharedData imageURLForExerciseIconWithID:self.exercise_id];
    return imageURL;*/
    NSString * urlString = self.linkToIcon;
    
    NSURL *imageURL;
    
    if(!urlString)  return nil;
    
    if ([urlString hasPrefix:@"http://"] || [urlString hasPrefix:@"https://"]) {
        imageURL = [NSURL URLWithString:urlString];
    } else {
        imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@", urlString]];
    }
    
    return imageURL;
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark UBIUpdatableWithDictionary protocol
#pragma mark -
///////////////////////////////////////////////
- (void)addValuesFromDictionary:(NSDictionary *)dict
{
    for (NSString *key in [dict allKeys]) {
        
        id value = [dict valueForKey:key];
        
        // if value is an array
        if ([value isKindOfClass:[NSArray class]]) {
            UBILogWarning(@"Value (%@) is an array", value);
            
            continue;
        }
        
        // if value is a dictionary
        // else, add it directly
        else {
            
            if ([value isKindOfClass:[NSNull class]]) { continue; }
            
            if ([key isEqualToString:FASAPIExerciseIDKey]) {
                self.exercise_id = dict[key];
                continue;
            } else if ([key isEqualToString:FASAPIExerciseIDCodeKey]) {
                self.code = dict[key];
                continue;
            } else if ([key isEqualToString:FASAPIExerciseWorkoutExerciseIDCodeKey]) {
                self.workoutExerciseID = dict[key];
                continue;
            } else if ([key isEqualToString:FASAPIExerciseIDMuscleTypesNameKey]) {
                self.muscleTypes = dict[key];
                continue;
            } else if ([key isEqualToString:FASAPIExerciseIDNameKey]) {
                self.name = dict[key];
                continue;
            } else if ([key isEqualToString:FASAPIExerciseWorkoutIDKey]) {
                self.workout_id = dict[key];
                continue;
            } else if ([key isEqualToString:@"exerciseID.isStatic"]) {
                self.isStatic = dict[key];
                // TODO: currently assuming, countable when NOT static
                self.isCountable = @(!([self.isStatic boolValue]));
                continue;
            } else if ([key isEqualToString:@"exerciseID.mi_modifyDate"]) {
                self.mi_modifyDate = [NSDate dateFromUnityBaseString:dict[key]];
                continue;
            } else if ([key isEqualToString:@"exercise"]) {
              self.mi_modifyDate = [NSDate dateFromUnityBaseString:dict[key]];
              continue;
            } else if ([key isEqualToString:FASAPIExerciseDescription]) {
              self.exerciseDescription = dict[key];
              continue;
            }else if ([key isEqualToString:FASAPIExerciseLinkToImageKey]) {
                self.linkToPhoto = dict[key];
                continue;
            }else if ([key isEqualToString:FASAPIExerciseLinkToIconKey]) {
                self.linkToIcon = dict[key];
                continue;
            }
  
            
          
            [self setValue:value forKey:key];
        }
    }
}

- (void)addAttachmentsFromArray:(NSArray *)collection
{
    for (id objDictionary in collection) {
        @autoreleasepool {
            Attach *newObj = (Attach *)[NSEntityDescription insertNewObjectForEntityForName:@"Attach"
                                                                         inManagedObjectContext:self.managedObjectContext];
            [newObj addValuesFromDictionary:objDictionary];
            [self addAttachmentsObject:newObj];
        }
    }
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Position In Superset
#pragma mark -
///////////////////////////////////////////////
- (FASWorkoutPositionInSupersetType)visualPositionInSuperset
{
    if (self.positionInSuperset && ![self.positionInSuperset isBlank]) {
        if ([self.positionInSuperset isEqualToString:FASWorkoutPositionInSupersetTypeTopRawValue]) {
            return FASWorkoutPositionInSupersetTypeTop;
        }
        else if ([self.positionInSuperset isEqualToString:FASWorkoutPositionInSupersetTypeMiddleRawValue]) {
            return FASWorkoutPositionInSupersetTypeMiddle;
        }
        else if ([self.positionInSuperset isEqualToString:FASWorkoutPositionInSupersetTypeBottomRawValue]) {
            return FASWorkoutPositionInSupersetTypeBottom;
        }
    }
    
    return FASWorkoutPositionInSupersetTypeNone;
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark NSKeyValueCoding protocol
#pragma mark -
///////////////////////////////////////////////

- (id)valueForUndefinedKey:(NSString *)key
{
    return nil;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    UBILogWarning(@"%@: UNDEFINED value = %@\tkey = %@", NSStringFromClass([self class]), value, key);
}

@end

///////////////////////////////////////////////
#pragma mark -
#pragma mark Extension Format
#pragma mark -
///////////////////////////////////////////////
@implementation Exercise (Format)

- (NSString *)formattedMuscleTypes {
    if (self.muscleTypes != nil || [self.muscleTypes isBlank]) {
        NSArray* mtArr = [self.muscleTypes componentsSeparatedByString:@","];
        NSString* formattedMuscleTypes = [mtArr componentsJoinedByString:@", "];
        return formattedMuscleTypes;
    }
    // TODO: localize this
    return @"no muscle types";
}

@end
