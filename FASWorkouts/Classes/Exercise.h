//
//  Exercise.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/10/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "UnityBase.h"
#import "UBIRemoteImageCache.h"
#import "FASWorkoutDataTypes.h"

@class Workout;
@class Attach;

@interface Exercise : NSManagedObject
<UBIUpdatableWithDictionary>

+ (UBIRemoteImageCache *)exerciseImageCache;
- (UBIRemoteImage *)remoteImage;
- (UBIRemoteImage *)remoteIcon;
- (NSString *)remoteImageKey;

// persistent attributes (API)
@property (nonatomic, strong) NSString* baseImageFilename;
@property (nonatomic, retain, readonly) NSNumber * workoutExerciseID;
@property (nonatomic, retain, readonly) NSDate * createdOn;
@property (nonatomic, retain, readonly) NSNumber * exercise_id;
@property (nonatomic, retain, readonly) NSNumber * restTime;
@property (nonatomic, strong, readonly) NSNumber* timeSpan;
@property (nonatomic, strong, readonly) NSNumber* lapsNumber;
@property (nonatomic, strong, readonly) NSNumber* isStatic;
@property (nonatomic, copy, readonly) NSString* code;
@property (nonatomic, copy, readonly) NSString* name;
@property (nonatomic, copy, readonly) NSString* exerciseDescription;
@property (nonatomic, copy, readonly) NSString* muscleTypes;
@property (nonatomic, strong, readonly) NSDate* mi_modifyDate;
@property (nonatomic, strong, readonly) NSNumber* sortIndex;
@property (nonatomic, copy, readonly) NSString* positionInSuperset;
@property (nonatomic, strong, readonly) NSNumber* isCountable;

// persistent attributes (calculated)
@property (nonatomic, retain) NSNumber * attachCount;

// relationships
@property (nonatomic, strong) Workout *     workout;
@property (nonatomic, strong) Attach *      preferredAttachment;
@property (nonatomic, strong) NSSet *       attachments;

- (void)addAttachmentsFromArray:(NSArray *)collection;

// Position In Superset
- (FASWorkoutPositionInSupersetType)visualPositionInSuperset;

@end

@interface Exercise (AttachmentsAccessors)
- (void)addAttachmentsObject:(Attach *)value;
- (void)removeAttachmentsObject:(Attach *)value;
- (void)addAttachments:(NSSet *)value;
- (void)removeAttachments:(NSSet *)value;
- (NSMutableSet *)primitiveAttachments;
- (void)setPrimitiveAttachments:(NSMutableSet *)attachments;
@end

// TODO: create a protocol based on this,
// duplicated implementation
@interface Exercise (Format)
- (NSString *)formattedMuscleTypes;
@end
