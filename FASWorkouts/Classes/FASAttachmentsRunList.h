//
//  FASAttachmentsRunList.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/15/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "UBIRunList.h"
#import  "UBIResultData.h"

@interface FASAttachmentsRunList : UBIRunList

@property (nonatomic, strong, readonly) UBIResultData* resultData;

- (instancetype)initWithExerciseID:(NSNumber *)exerciseID;
+ (instancetype)attachmentsRunListWithExerciseID:(NSNumber *)exerciseID;

- (instancetype)initWithExerciseIDs:(NSArray *)exerciseIDs;
+ (instancetype)attachmentsRunListWithExerciseIDs:(NSArray *)exerciseIDs;

- (NSArray *)arrayOfDataTranslatedToModelRepresentation;

@end
