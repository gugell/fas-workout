//
//  UBIRunList.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/31/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UnityBase.h"

typedef NS_ENUM(NSUInteger, UBIRunListMethod) {
    UBIRunListMethodNone,
    UBIRunListMethodSelect,
};

typedef NS_ENUM(NSUInteger, UBIRunListWhereListCondition) {
    UBIRunListWhereListConditionEqual,
    UBIRunListWhereListConditionIn,
};

// Run List Where List

@interface UBIRunListWhereList : NSObject
<UBIStringRepresentable>
- (void)addItem:(NSString *)name condition:(UBIRunListWhereListCondition)condition value:(id)value key:(NSString *)key;
@end

//  Run List Options

@interface UBIRunListOptions : NSObject
<UBIStringRepresentable, UBIUpdatableWithDictionary>
@property (nonatomic, strong) NSNumber* limit;
@property (nonatomic, strong) NSNumber* start;
@end

// Run List

@interface UBIRunList : NSObject
<UBIStringRepresentable, UBIUpdatableWithDictionary>
@property (nonatomic, copy)     NSString* entity;
@property (nonatomic)           UBIRunListMethod method;
@property (nonatomic, strong)   NSArray* fieldList;
@property (nonatomic, strong)   UBIRunListOptions* options;
@property (nonatomic, strong)   UBIRunListWhereList* whereList;
@property (nonatomic) BOOL      nativeDatasetFormat;

@end
