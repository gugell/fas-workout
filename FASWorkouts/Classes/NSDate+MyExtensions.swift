//
//  NSDate+MyExtensions.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/13/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import Foundation

private var sDateFormatter: NSDateFormatter = NSDateFormatter()

//
// Get formated string (e.g. 00:01:32) from NSDate object
//
extension NSDate
{
    func toString(format: String) -> String {
        sDateFormatter.locale = NSLocale.currentLocale()
        sDateFormatter.timeZone = NSTimeZone.systemTimeZone()//NSTimeZone(forSecondsFromGMT: 0)
        sDateFormatter.dateFormat = format
        
        return sDateFormatter.stringFromDate(self)
    }
    
    private func toString(dateStyle dateStyle: NSDateFormatterStyle, timeStyle: NSDateFormatterStyle) -> String {
        sDateFormatter.locale = NSLocale.currentLocale()
        sDateFormatter.timeZone = NSTimeZone.systemTimeZone()
        sDateFormatter.dateStyle = dateStyle
        sDateFormatter.timeStyle = timeStyle
        
        return sDateFormatter.stringFromDate(self)
    }
    
    func toStringTime() -> String {
        return toString("HH:mm:ss")
    }
    
    func toStringTimeWithMiliseconds() -> String {
        return toString("HH:mm:ss:SS")
    }
    
    func toStringTimeMinutesAndSeconds() -> String {
        return toString("mm:ss")
    }
    
    func toStringTimeHoursAndMinutes() -> String {
        return toString("HH:mm")
    }
    
    func toStringMediumDateShortTime() -> String {
        return toString(dateStyle: NSDateFormatterStyle.MediumStyle, timeStyle: NSDateFormatterStyle.ShortStyle)
    }
    
    func toStringShortTime() -> String {
        return toString(dateStyle: NSDateFormatterStyle.NoStyle, timeStyle: NSDateFormatterStyle.ShortStyle)
    }
}

//
// period of time helpers
//

extension NSDate
{
    class func yesterday() -> NSDate {
        let secondsPerDay: NSTimeInterval = 24 * 60 * 60
        return NSDate(timeIntervalSinceNow: -secondsPerDay)
    }
    
    class func twoHoursAgo() -> NSDate {
        let secondsPerTwoHours: NSTimeInterval = 2 * 60 * 60
        return NSDate(timeIntervalSinceNow: -secondsPerTwoHours)
    }
    
    class func todayAtNineAM() -> NSDate? {        
        let dateNow = NSDate()
        let calendar = NSCalendar.currentCalendar()
        calendar.timeZone = NSTimeZone.systemTimeZone()
        let dateComponents = calendar.components(([NSCalendarUnit.Year, NSCalendarUnit.Month, NSCalendarUnit.Day]), fromDate: dateNow)
        dateComponents.hour = 9
        dateComponents.minute = 00
        dateComponents.second = 00
        return calendar.dateFromComponents(dateComponents)
    }
}