//
//  FASAttachmentsRunList.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/15/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "FASAttachmentsRunList.h"
#import "FASAPIObjectKeys.h"
#import "FASWorkouts-Swift.h"
#import "UBILogger.h"

NSString* const kFASExerciseAttachRunListEntity = @"fas_exercise_attach";

NSString* const kFASExerciseAttachRunListExerciseIDKey = @"exerciseID";
NSString* const kFASExerciseAttachRunListAttachIDKey = @"ID";
NSString* const kFASExerciseAttachRunListLangKey = @"lang";
NSString* const kFASExerciseAttachRunListMiModifyDateKey = @"mi_modifyDate";
NSString* const kFASExerciseAttachRunListFileKey = @"file";

// cached date formatter
static NSDateFormatter *    sDateFormatter = nil;

@implementation FASAttachmentsRunList

// RunList example:

/*
 
 - By Single Exercise ID:
 
     theAPI.body = @"[{\"entity\":\"fas_exercise_attach\",\"method\":\"select\",\"fieldList\":[\"ID\",\"exerciseID\",\"lang\",\"type\",\"file\"],\"whereList\":{\"ExcercisesForWorkout\":{\"values\":{\"exerciseID\":\"3000000001001\"},\"expression\":\"[exerciseID]\",\"condition\":\"equal\"}}}]";

 - By Array Of Exercise IDs:
*/

- (instancetype)init {
    self = [super init];
    if (self) {
        self.entity = kFASExerciseAttachRunListEntity;
        self.method = UBIRunListMethodSelect;
        // TODO: replace w/ constants from Attachment object
        self.fieldList = @[kFASExerciseAttachRunListAttachIDKey,kFASExerciseAttachRunListExerciseIDKey,kFASExerciseAttachRunListLangKey,@"type",@"file",kFASExerciseAttachRunListMiModifyDateKey];
        self.options.limit = @(100);
        self.options.start = @(0);
        
        _resultData = [UBIResultData new];
    }
    return self;
}

- (instancetype)initWithExerciseID:(NSNumber *)exerciseID
{
    self = [self init];
    NSString* strExerciseID = [NSString stringWithFormat:@"%@", exerciseID];
    [self.whereList addItem:@"AttachForExcercise" condition:UBIRunListWhereListConditionEqual value:strExerciseID key:@"exerciseID"];
    return self;
}

+ (instancetype)attachmentsRunListWithExerciseID:(NSNumber *)exerciseID
{
    return [[FASAttachmentsRunList alloc] initWithExerciseID:exerciseID];
}

- (instancetype)initWithExerciseIDs:(NSArray *)exerciseIDs
{
    self = [self init];
    [self.whereList addItem:@"AttachmentsForWorkoutByExerciseIDs" condition:UBIRunListWhereListConditionIn value:exerciseIDs key:@"exerciseID"];
    return self;
}

+ (instancetype)attachmentsRunListWithExerciseIDs:(NSArray *)exerciseIDs
{
    return [[FASAttachmentsRunList alloc] initWithExerciseIDs:exerciseIDs];
}

- (void)addValuesFromDictionary:(NSDictionary *)dict
{
    if (!dict) {
        return;
    }
    
    // Run List
    [super addValuesFromDictionary:dict];
    
    // Result Data
    [self.resultData addValuesFromDictionary: dict[FASAPIResultDataKey] ];
}

- (NSArray *)arrayOfDataTranslatedToModelRepresentation
{
    NSMutableArray* resultArray = [NSMutableArray arrayWithCapacity: [self.resultData.rowCount unsignedIntegerValue]];
    
    for (NSDictionary* rawDict in self.resultData.arrayOfDictionaryData) {
        @autoreleasepool {
            if (rawDict != nil && [rawDict isKindOfClass:[NSDictionary class]]) {
                
                NSMutableDictionary* item = [NSMutableDictionary dictionaryWithCapacity:[rawDict allKeys].count];
                
                for (NSString *key in [rawDict allKeys]) {
                    
                    id value = [rawDict valueForKey:key];
                    
                    // if value is an array
                    if ([value isKindOfClass:[NSArray class]]) {
                        UBILogWarning(@"Value (%@) is an array", value);
                        
                        continue;
                    }
                    
                    // if value is a dictionary
                    // ...
                    
                    // else, add it directly
                    else {
                        
                        if ([value isKindOfClass:[NSNull class]]) { continue; }
                        
                        if ([key isEqualToString:kFASExerciseAttachRunListAttachIDKey]) {
                            [item setObject:rawDict[kFASExerciseAttachRunListAttachIDKey] forKey:@"attach_id"];
                        }
                        // date / time
                        if ([key isEqualToString:kFASExerciseAttachRunListMiModifyDateKey]) {
                            NSDate *mi_modifyDate = [NSDate dateFromUnityBaseString:value];
                            [item setValue:mi_modifyDate forKey:key];
                            continue;
                        }
                        
                        if ([key isEqualToString:kFASExerciseAttachRunListFileKey]) {
                            NSDictionary *fileDict =
                            [NSJSONSerialization JSONObjectWithData: [value dataUsingEncoding:NSUTF8StringEncoding]
                                                            options: NSJSONReadingMutableContainers
                                                              error: NULL];
                            [item setValue:fileDict forKey:key];
                            continue;
                        }
                        else {
                            [item setValue:value forKey:key];
                        }
                    }
                }
                
                [resultArray addObject:item];
            }
        }
    }
    
    return [resultArray copy];
}

@end
