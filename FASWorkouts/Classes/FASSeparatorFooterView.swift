//
//  FASSeparatorFooterView.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/15/15.
//  Copyright © 2015 FAS. All rights reserved.
//

import UIKit

class FASSeparatorFooterView: FASTableViewFooterView {
    
    private var separatorLayer: CAShapeLayer?
    
    internal override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        _commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        _commonInit()
    }
    
    private func _commonInit() {
        self.textLabel?.textColor = UIColor.fasGrayColor206(alpha: 0.5)

        // debug layout
//        self.textLabel?.backgroundColor = UIColor.blueColor()
//        self.contentView.backgroundColor = UIColor.greenColor()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let _ = self.textLabel {
            
            // LABEL layout
            
            var textLabelFrame = self.textLabel!.frame
            // BUG: this result in app being stuck
//            textLabelFrame.size = CGSize(width: 50.0, height: textLabelFrame.height)
            // ... so we use origin to lay the label out
            textLabelFrame.origin = CGPoint(x: self.bounds.width / 2.0 - textLabelFrame.size.width / 2.0 , y: 0)
            self.textLabel!.frame = textLabelFrame
            
            // SEPARATOR layout
            
            if let _ = self.separatorLayer {
                self.separatorLayer!.removeFromSuperlayer()
                self.separatorLayer = nil
            }
            
            let separatorLayerHeight: CGFloat = 1.0
            let separatorLayerOffsetX: CGFloat = 8.0
            let halfHeight: CGFloat = self.bounds.height / 2.0
            let separatorLayerOffsetY: CGFloat = halfHeight - separatorLayerHeight / 2.0
            
            let path = UIBezierPath(rect: CGRect(
                x: separatorLayerOffsetX,
                y: separatorLayerOffsetY,
                width: self.bounds.width - separatorLayerOffsetX - separatorLayerOffsetX,
                height: separatorLayerHeight)
            )
            
            let holeOffset: CGFloat = 10.0
            let holePath = UIBezierPath(rect: CGRect(
                x: (self.textLabel!.frame.origin.x - holeOffset),
                y: separatorLayerOffsetY,
                width: self.textLabel!.frame.size.width + holeOffset + holeOffset,
                height: separatorLayerHeight)
            )
            path.appendPath(holePath)
            path.usesEvenOddFillRule = true
            
            self.separatorLayer = CAShapeLayer()
            self.separatorLayer!.path = path.CGPath
            self.separatorLayer!.fillRule = kCAFillRuleEvenOdd
            self.separatorLayer!.fillColor = UIColor.fasGrayColor206(alpha: 0.5).CGColor
            self.layer.addSublayer(self.separatorLayer!)
        }
    }
}
