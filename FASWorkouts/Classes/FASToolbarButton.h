//
//  FASToolbarButton.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 3/1/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FASToolbarButton : UIButton

@property (nonatomic)       CGFloat     spacing;

+ (FASToolbarButton *)fasToolbarButton;

- (void)centerButtonAndImageWithSpacing:(CGFloat)spacing;

@end
