//
//  ContainerViewController.h
//  EmbeddedSwapping
//
//  Created by Michael Luton on 11/13/12.
//  Copyright (c) 2012 Sandmoose Software. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ContainerVCTag) {
    ContainerVCTagUndefined = -1,
    ContainerVCTagHistory,
    ContainerVCTagStatistic,
};


@interface ContainerViewController : UIViewController

- (void)selectViewControllerWithTag:(ContainerVCTag)tag;

@end

///////////////////////////////////////////////
// EmptySegue Class
///////////////////////////////////////////////

#import <UIKit/UIStoryboardSegue.h>

@interface EmptySegue : UIStoryboardSegue

@end
