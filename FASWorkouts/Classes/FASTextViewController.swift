//
//  FASTextViewController.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/10/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit

class FASTextViewController: FASViewController {
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var primaryActionButton: UIButton!
    @IBOutlet weak var bottomPanel: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar(.Dark)
    }
    
    func setActionPanelBusy(isBusy: Bool) {
        activityIndicator.hidden = !isBusy
        self.primaryActionButton.hidden = isBusy
    }
    
    func setActionPanelEnable(isEnabled: Bool) {
        if (isEnabled) {
           self.bottomPanel.backgroundColor = UIColor.fasYellowColor()
        }
        else {
           self.bottomPanel.backgroundColor = UIColor.fasGrayColor62()
        }
        self.primaryActionButton.enabled = isEnabled
    }
}
