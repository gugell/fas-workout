//
//  UBIRemoteImageCache.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/1/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "UBIRemoteImageCache.h"
#import "UnityBase.h"
#import "NSString+MyExtensions.h"
#import "UBILogger.h"

static NSString * kRemoteImageStatus = @"remote image status";

NSString * const kUBIRemoteImageCacheInfoURL             = @"URL";
NSString * const kUBIRemoteImageCacheInfoFilename        = @"filename";
NSString * const kUBIRemoteImageCacheInfoMaxSize         = @"maxSize";
NSString * const kUBIRemoteImageCacheInfoDefaultFilename = @"defaultFilename";

@interface UBIRemoteImageCache ()
@property (nonatomic, strong) NSMutableDictionary *remoteImages;
@end

@implementation UBIRemoteImageCache

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.basePath = CACHES_PATH;
        self.imageFilePrefix = @"photo_";
        self.remoteImages = [NSMutableDictionary dictionary];
        
        [NSNotificationCenter.defaultCenter addObserver:self
                                               selector:@selector(didReceiveMemoryWarning)
                                                   name:UIApplicationDidReceiveMemoryWarningNotification
                                                 object:nil];
    }
    return self;
}

- (void)dealloc
{
    for (UBIRemoteImage *remoteImage in _remoteImages) {
        [remoteImage removeObserver:self
                         forKeyPath:@"status"
                            context:&kRemoteImageStatus];
    }
    [NSNotificationCenter.defaultCenter
     removeObserver:self
     name:UIApplicationDidReceiveMemoryWarningNotification
     object:nil];
}

- (void)didReceiveMemoryWarning {
    [self flush];
}

- (void)flush
{
    for (UBIRemoteImage *remoteImage in [self.remoteImages allValues]) {
        remoteImage.image = nil;
    }
}

- (void)clearDefaultImages
{
    for (UBIRemoteImage *remoteImage in [self.remoteImages allValues]) {
        [remoteImage clearDefaultImage];
    }
}

- (void)loadAllImages
{
    for (UBIRemoteImage *remoteImage in [self.remoteImages allValues]) {
        [remoteImage image];
    }
}

- (void)deleteRemoteImageForKey:(NSString *)key
{
    UBIRemoteImage *remoteImage = [self remoteImageForKey:key];
    if (remoteImage) {
        NSString *path = remoteImage.filepath;
        NSError *error;
        if (![NSFileManager.defaultManager removeItemAtPath:path error:&error]) {
            UBILogError(@"deleting %@: %@",
                    path,
                    [error localizedDescription]);
        }
        else {
            UBILogTrace(@"Deleted image %@", path);
        }
        [self setRemoteImage:nil forKey:key];
    }
}

// get image from in-memory cache
- (UBIRemoteImage *)remoteImageForKey:(NSString *)key
{
    return [self.remoteImages valueForKey:key];
}

// set image to in-memory cache
- (void)setRemoteImage:(UBIRemoteImage *)remoteImage
                forKey:(NSString *)key
{
    UBIRemoteImage *originalRemoteImage = [self remoteImageForKey:key];
    [originalRemoteImage removeObserver:self forKeyPath:@"status" context:&kRemoteImageStatus];
    [self.remoteImages setValue:remoteImage forKey:key];
    [remoteImage addObserver:self
                  forKeyPath:@"status"
                     options:NSKeyValueObservingOptionNew
                     context:&kRemoteImageStatus];
}

- (UBIRemoteImage *)remoteImageForKey:(NSString *)key
                          loadingInfo:(NSDictionary *)loadingInfo
{
    UBIRemoteImage *remoteImage = [self remoteImageForKey:key];
    
    if (!remoteImage) {
        if (!key) {
            UBILogWarning(@"UBIRemoteImageCache:remoteImageForKey:loadingInfo: NULL KEY");
        }
        
        remoteImage = [UBIRemoteImage new];
        remoteImage.loadingImageName = self.loadingImageName;
        NSString *defaultFilename = [loadingInfo valueForKey:kUBIRemoteImageCacheInfoDefaultFilename];
        remoteImage.defaultImageName = (defaultFilename && ![defaultFilename isBlank]) ? defaultFilename : self.defaultImageName;
        NSString *filename = [loadingInfo valueForKey:kUBIRemoteImageCacheInfoFilename];
        NSString *filepath = filename ? [self pathForFilename:filename] : [self pathForKey:key];
        remoteImage.filepath = filepath;
        remoteImage.URL = [loadingInfo valueForKey:kUBIRemoteImageCacheInfoURL];
        
        if ([loadingInfo valueForKey:kUBIRemoteImageCacheInfoMaxSize]) {
            remoteImage.maxSize = [[loadingInfo valueForKey:kUBIRemoteImageCacheInfoMaxSize]
                                   CGSizeValue];
        }
        
        [self setRemoteImage:remoteImage forKey:key];
    }
    
    return remoteImage;
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if (context == &kRemoteImageStatus) {
        UBIRemoteImage *remoteImage = (UBIRemoteImage *)object;
        if (remoteImage.status == UBIRemoteImageStatusLoadedFromURL) {
            
            // remoteImage might be deallocated at the time we process the image in another thread
            // as per the notification has a pointer to that memory address, the app crashes
            // so, we retain image and filepath in local variables and pass them to the block
            
            UIImage * imageToSave = remoteImage.image;
            NSString * imageFilePath = remoteImage.filepath;
            UBIRemoteImageContentType imageType = remoteImage.imageType;
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                @autoreleasepool {
                    [self saveImage:imageToSave
                             ofType:imageType
                             toPath:imageFilePath];
                }
            });
        }
    }
    
    else {
        [super observeValueForKeyPath:keyPath
                             ofObject:object
                               change:change
                              context:context];
    }
}

- (void)saveImage:(UIImage *)image ofType:(UBIRemoteImageContentType)type toPath:(NSString *)path
{
    NSData *imageData;
    
    if (type == UBIRemoteImageContentTypePNG) {
        imageData = UIImagePNGRepresentation(image);
    } else if (type == UBIRemoteImageContentTypeJPEG) {
        imageData = UIImageJPEGRepresentation(image, 1.0);
    } else {
        // fallback to JPEG representation
        UBILogWarning(@"Fallback to JPEG image data representation. Reason: Content Type is unknown.");
        imageData = UIImageJPEGRepresentation(image, 1.0);
        if (imageData == nil) { UBILogWarning(@"Fallback to JPEG image data representation FAILED"); }
    }
    
    NSError *error;
    BOOL success = [imageData writeToFile:path options:NSDataWritingAtomic error:&error];
    if (!success) {
        UBILogError(@"Image %@ was NOT saved to disc. Error was:\n%@", path, [error userInfo]);
    }
}

- (NSString *)pathForKey:(NSString *)key
{
    NSString *baseImageFilename = [NSString stringWithFormat:@"%@",
                          key];
    return [self pathForFilename:baseImageFilename];
}

- (NSString *)pathForFilename:(NSString *)filename
{
    NSString *prefixedFilename = [NSString stringWithFormat:@"%@%@",
                                  self.imageFilePrefix,
                                  filename];
    return [self.basePath stringByAppendingPathComponent:prefixedFilename];
}

@end
