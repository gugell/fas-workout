//
//  FASActionController.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 6/24/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import Foundation
import MessageUI

enum FASActionControllerError: Int {
    case NoError                                = 0
    case InvalidViewController                  = 1
    case InvalidInput                           = 2
    case FacebookCannotShowInviteFriendsDialog  = 3
    case FacebookActionCancelled                = 4
    case FacebookCustom                         = 5
    case MailCannotSend                         = 6
    case MailSendFailed                         = 7
    
    func createNSError(userInfo info: [NSObject: NSObject]? = nil) -> NSError {
        return NSError(domain: "com.fas-sport.FASWorkout.FASActionController.ErrorDomain", code: self.rawValue, userInfo: (info ?? [:]) )
    }
}

// facebook SDK
import FBSDKShareKit

///////////////////////////////////////////////
// Protocols
///////////////////////////////////////////////
protocol ContentShareable { }

protocol FacebookContentShareableStings: ContentShareable {
    func fbShareContentTitle() -> String // ???: optional
    func fbShareContentDescription() -> String // ???: optional
}

protocol FacebookContentShareableURLs: ContentShareable {
    func fbShareContentURL() -> NSURL?
    func fbShareContentImageURL() -> NSURL?
}

protocol FacebookContentShareable: FacebookContentShareableStings, FacebookContentShareableURLs { }

///////////////////////////////////////////////
// Types
///////////////////////////////////////////////
typealias CompletionBlock = (error: NSError?) -> Void

///////////////////////////////////////////////
// Controller
///////////////////////////////////////////////
class FASActionController: NSObject {
    
    weak var viewController: UIViewController? = nil
    
    var completionBlock: CompletionBlock?

    var isBusy: Bool {
        return self.completionBlock != nil
    }
    
    ///////////////////////////////////////////////
    // MARK: Init -
    ///////////////////////////////////////////////
    
    convenience override init() {
        self.init(viewController: nil)
    }
    
    init(viewController vc: UIViewController?) {
        self.viewController = vc
        super.init()
        FASAPISession.currentSession().addObserver(self);
    }
    
    deinit {
        FASAPISession.currentSession().removeObserver(self)
    }
    
    ///////////////////////////////////////////////
    // MARK: Facebook Share Open Graph Story -
    ///////////////////////////////////////////////
    
    // uncomment to share OG Story
    
//    func fbShareOpenGraphStorie(workout: Workout?) {
//        if let unwrappedWorkout = workout {
//            printlnDebugTrace("workout to share:\n\(unwrappedWorkout)")
//            
//            let properties: [NSObject : AnyObject] = [
//                "og:type": "fitness.course",
//                "og:title": "Sample Course",
//                "og:description": "This is a sample course.",
//                "fitness:duration:value": 100,
//                "fitness:duration:units": "s",
//                "fitness:distance:value": 12,
//                "fitness:distance:units": "km",
//                "fitness:speed:value": 5,
//                "fitness:speed:units": "m/s"
//            ]
//            let object = FBSDKShareOpenGraphObject(properties: properties)
//            let action = FBSDKShareOpenGraphAction()
//            action.actionType = "fitness.runs"
//            action.setObject(object, forKey: "fitness:course")
//            let content = FBSDKShareOpenGraphContent()
//            content.action = action
//            content.previewPropertyName = "fitness:course"
//            
//            var error: NSError?
//            let success = self.showShareDialog(content, error: &error)
//            if !success {
//                if let hasError = error {
//                    printlnDebug("[ERROR] cannot show share dialog. Error:\n\(hasError)")
//                }
//            }
//        }
//        
//        else {
//            printlnDebug("[WARNING]: invalid input object, cannot share")
//        }
//    }
    
    ///////////////////////////////////////////////
    // MARK: Facebook App Invites -
    ///////////////////////////////////////////////
    // TODO: replace the App Link
    func fbInviteFriends(completion: CompletionBlock? = nil) {
        
        if self.isBusy {
            // TODO: create "not available" error
            completion?(error: nil)
            return
        }
        
        completionBlock = completion
        
        if !self.fbCanInviteFriends() {
            completionBlock?(error: FASActionControllerError.FacebookCannotShowInviteFriendsDialog.createNSError() )
            completionBlock = nil
            return
        }
        
        let content = FBSDKAppInviteContent()
        content.appLinkURL = NSURL(string: FASEnvironment.FBInviteFriendsAppLinkURL())
        
        // image (optional)
        let logoImageName = "fas_invites.png"
        let imageURL = NSURL(string: "\(FASEnvironment.ImagesBasePath())/\(logoImageName)")
        content.appInvitePreviewImageURL = imageURL
        
        FBSDKAppInviteDialog.showFromViewController(self.viewController, withContent: content, delegate: self)
    }
    
    func fbCanInviteFriends() -> Bool {
        return FBSDKAppInviteDialog().canShow()
    }
    ///////////////////////////////////////////////
    // MARK: Facebook Share Content -
    ///////////////////////////////////////////////
    
    func fbShareContentWithItem(shareableItem: FacebookContentShareable?, completion: CompletionBlock? = nil) {
        
        if self.isBusy {
            // TODO: create "not available" error
            completion?(error: nil)
            return
        }
        
        completionBlock = completion
        
        if let unwrappedShareableItem = shareableItem {
            
            let content = FBSDKShareLinkContent()
            
            content.contentURL = unwrappedShareableItem.fbShareContentURL()
            content.imageURL = unwrappedShareableItem.fbShareContentImageURL() // TODO: check is image exist at URL
            content.contentTitle = unwrappedShareableItem.fbShareContentTitle()
            content.contentDescription = unwrappedShareableItem.fbShareContentDescription()
            
            fbShareContent(content, completion: { [weak self] (error) -> Void in
                withExtendedLifetime(self) {
                    self!.completionBlock?(error: error)
                    self!.completionBlock = nil
                }
            })
        }
        
        else {
            let err = FASActionControllerError.InvalidInput.createNSError(userInfo: [NSLocalizedDescriptionKey: "empty content"])
            completionBlock?(error: err)
            completionBlock = nil
        }
    }
    
    private func fbShareContent(content: FBSDKShareLinkContent, completion: CompletionBlock? = nil) {
        var error: NSError?
        let success = self.showShareDialog(content, error: &error)
        if !success {
            completion?(error: error)
        }
    }
    
    // this is an exception from other FB sharing method, 
    // b/c the content is combined from two bojects, FASWorkoutSession and FASWorkout
    func fbShareWorkoutSession(workoutSession: FASWorkoutSession?, completion: CompletionBlock? = nil) {
        
        if self.isBusy {
            // TODO: create "not available" error
            completion?(error: nil)
            return
        }
        
        completionBlock = completion
        
        if let _ = workoutSession {
            let context = FASData.sharedData().newUIContext()
            context.performBlockAndWait { () -> Void in
                var wrk: Workout? = nil
                let fetchRequest = NSFetchRequest(entityName: "Workout")
                //fetchRequest.returnsObjectsAsFaults = false
                do {
                   let workouts = try context.executeFetchRequest(fetchRequest)
                    if (workouts.count > 0) {
                        wrk = workouts.first as? Workout
                    }
                } catch let fetchError {
                    printlnDebugError("error during fetching \(fetchError)")
                }
                
                if let _ = wrk {
                    let calPart = wrk!.formattedCalories()
                    let timePart = workoutSession!.formattedDuration()
                    let ratePart = workoutSession!.formattedUserRate()
                    
                    // TODO: localize this
                    let contentTitle = "Workout \(wrk!.name)"
                    let contentDescription = "\(calPart). in \(timePart) I have just accomplished it! It was \(ratePart). You have to try it!"
                    let contentURL = wrk!.fbShareContentURL()
                    let contentContentImageURL = wrk!.fbShareContentImageURL()
                    
                    let content = FBSDKShareLinkContent()
                    
                    content.contentURL = contentURL
                    content.imageURL = contentContentImageURL
                    content.contentTitle = contentTitle
                    content.contentDescription = contentDescription
                    
                    self.fbShareContent(content, completion: { (error) -> Void in
                        self.completionBlock?(error: error)
                        self.completionBlock = nil
                    })
                }
                else {
                    let err = FASActionControllerError.InvalidInput.createNSError(userInfo: [NSLocalizedDescriptionKey: "empty content"])
                    self.completionBlock?(error: err)
                    self.completionBlock = nil
                }
            }
        }
        else {
            let err = FASActionControllerError.InvalidInput.createNSError(userInfo: [NSLocalizedDescriptionKey: "empty content"])
            completionBlock?(error: err)
            completionBlock = nil
        }
    }
    
    ///////////////////////////////////////////////
    // MARK: Facebook Share Helpers
    ///////////////////////////////////////////////
    
    private func showShareDialog(content: FBSDKSharingContent!, error: NSErrorPointer) -> Bool {
        var success = true
        if let _ = self.viewController {

            let shareDialog = FBSDKShareDialog()
            shareDialog.fromViewController = viewController
            shareDialog.shareContent = content
            shareDialog.delegate = self
            
            shareDialog.mode = .Native
            if !shareDialog.canShow() {
                printlnDebugWarning("cannot show native share dialog")
                // fallback presentation when there is no FB app
                shareDialog.mode = .FeedBrowser
            }
            
            shareDialog.show()
        }
        
        else {
            let err = FASActionControllerError.InvalidViewController.createNSError()
            if error != nil {
                error.memory = err
            }
            success = false
        }
        
        return success
    }
    
    ///////////////////////////////////////////////
    // MARK: Send Mail -
    ///////////////////////////////////////////////
    
    func mailSendFeedback(completion: CompletionBlock? = nil) {
        
        if self.isBusy {
            // TODO: create "not available" error
            completion?(error: nil)
            return
        }
        
        completionBlock = completion
        
        if MFMailComposeViewController.canSendMail() {
            let mail = FASMessageTemplate.mailTemplateForFeedback()
            
            let mailComposeViewController = MFMailComposeViewController(nibName: nil, bundle: nil)
            mailComposeViewController.mailComposeDelegate = self
            mailComposeViewController.setToRecipients(mail.emails)
            mailComposeViewController.setSubject(mail.subject)
            mailComposeViewController.setMessageBody((mail.messageBody ?? ""), isHTML: mail.isHTMLMessageBody)
            
            if let unwrappedViewController = self.viewController {
                if let nav = unwrappedViewController.navigationController {
                    nav.presentViewController(mailComposeViewController, animated: true, completion: { () -> Void in
                        printlnDebugTrace("mail composer is presented from UINavigationController")
                    })
                }
                else {
                    unwrappedViewController.presentViewController(mailComposeViewController, animated: true, completion: { () -> Void in
                        printlnDebugTrace("mail composer is presented from UIViewController")
                    })
                }
            }
                
            else {
                let err = FASActionControllerError.InvalidViewController.createNSError()
                completionBlock?(error: err)
                completionBlock = nil
            }
        }
        else {
            let err = FASActionControllerError.MailCannotSend.createNSError(userInfo: [NSLocalizedDescriptionKey: "cannot send mail"])
            completionBlock?(error: err)
            completionBlock = nil
        }
    }
}

///////////////////////////////////////////////
// MARK: FBSDKAppInviteDialogDelegate methods -
///////////////////////////////////////////////

extension FASActionController: FBSDKAppInviteDialogDelegate {
    func appInviteDialog(appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [NSObject : AnyObject]!) {
        completionBlock?(error: nil)
        completionBlock = nil
    }
    
    func appInviteDialog(appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: NSError!) {
        let err = FASActionControllerError.FacebookCustom.createNSError(userInfo: [NSUnderlyingErrorKey:error])
        completionBlock?(error: err)
        completionBlock = nil
    }
}

///////////////////////////////////////////////
// MARK: FBSDKSharingDelegate methods -
///////////////////////////////////////////////

extension FASActionController: FBSDKSharingDelegate {
    
    func sharer(sharer: FBSDKSharing!, didCompleteWithResults results: [NSObject : AnyObject]!) {
        completionBlock?(error: nil)
        completionBlock = nil;
    }
    
    func sharer(sharer: FBSDKSharing!, didFailWithError error: NSError!) {
        let err: NSError
        if let _ = error {
            err = FASActionControllerError.FacebookCustom.createNSError(userInfo: [NSUnderlyingErrorKey:error])
        }
        else {
            err = FASActionControllerError.FacebookCustom.createNSError(userInfo: [NSLocalizedDescriptionKey: "sharer did fail with unknown error"])
        }
        
        completionBlock?(error: err)
        completionBlock = nil;
    }
    
    func sharerDidCancel(sharer: FBSDKSharing!) {
        let err = FASActionControllerError.FacebookActionCancelled.createNSError(userInfo: [NSLocalizedDescriptionKey: "action cancelled for sharer: \(sharer)"])
        completionBlock?(error: err)
        completionBlock = nil;
    }
}

///////////////////////////////////////////////
// MARK: FASAPISessionNotification -
///////////////////////////////////////////////

extension FASActionController: FASAPISessionObserver {
    
   func FASAPISessionNotification(notification: NSNotification!) {
        if  notification.name == FASAPISessionDidLoginNotification ||
            notification.name == FASAPISessionDidLogoutNotification ||
            notification.name == FASAPISessionDidRestoreSessionNotification ||
            notification.name == FASAPISessionDidNotLoginNotification
        {
            dispatch_async(dispatch_get_main_queue(), {
                // do smth
            })
        }
    }
}

///////////////////////////////////////////////
// MARK: FacebookContentShareable --- Workout -
///////////////////////////////////////////////

extension Workout: FacebookContentShareableStings {
    func fbShareContentTitle() -> String {
        return "Workout \"\(self.name)\""
    }
    
    func fbShareContentDescription() -> String {
        let formattedMuscleTypes = (muscleTypes != nil ? " \(self.formattedMuscleTypes())" : "")
        
        var formattedCalories = ""
        if let cal = calories where cal.integerValue != 0 {
            formattedCalories = " \(self.formattedCalories())."
        }

        return "Effective workout!\(formattedMuscleTypes).\(formattedCalories) You have to try it!"
    }
}

extension Workout: FacebookContentShareableURLs {
    func fbShareContentURL() -> NSURL? {
        return NSURL(string: FASEnvironment.SiteBasePath())
    }
    
    func fbShareContentImageURL() -> NSURL? {
        var imageURL: NSURL?
        if !self.remoteImageKey().isEmpty {
            // custom
            let imageExtension = ".png"
            let imagePath: String = "\(FASEnvironment.ImagesBasePath())/\(self.remoteImageKey())\(imageExtension)"
            imageURL = NSURL(string: imagePath)
        }
            
        else {
            // logo
            let logoImageName = "fas_logo.png"
            imageURL = NSURL(string: "\(FASEnvironment.ImagesBasePath())/\(logoImageName)")
        }
        return imageURL
    }
}

extension Workout: FacebookContentShareable {} // explicitly comforms

///////////////////////////////////////////////
// MARK: FacebookContentShareable --- Exercise -
///////////////////////////////////////////////

extension Exercise: FacebookContentShareableStings {
    func fbShareContentTitle() -> String {
        return self.name
    }
    
    func fbShareContentDescription() -> String {
        let formattedMuscleTypes = (muscleTypes != nil ? " for \(self.formattedMuscleTypes())" : "")
        return "The best exercise\(formattedMuscleTypes). Just give it a try together with me!"
    }
}

extension Exercise: FacebookContentShareableURLs {
    func fbShareContentURL() -> NSURL? {
        return NSURL(string: FASEnvironment.SiteBasePath())
    }
    
    func fbShareContentImageURL() -> NSURL? {
        var imageURL: NSURL?
        if let exerciseId = self.exercise_id  {
            // custom
            let imageExtension = ".png"
            let imagePath: String = "\(FASEnvironment.ImagesBasePath())/\(exerciseId.stringValue)\(imageExtension)"
            imageURL = NSURL(string: imagePath)
        }
            
        else {
            // logo
            let logoImageName = "fas_logo.png"
            imageURL = NSURL(string: "\(FASEnvironment.ImagesBasePath())/\(logoImageName)")
        }
        return imageURL
    }
}

extension Exercise: FacebookContentShareable {}

///////////////////////////////////////////////
// MARK: AppInfo Model -
///////////////////////////////////////////////
struct FASAppInfo {
    let name = "FAS Workout"
    let descr = "— Nice app with 900 minutes of various workouts! Do your exercises at your ease and wherever you are!"
}

///////////////////////////////////////////////
// MARK: FacebookContentShareable --- AppInfo -
///////////////////////////////////////////////

extension FASAppInfo: FacebookContentShareableStings {
    func fbShareContentTitle() -> String {
        return self.name
    }
    
    func fbShareContentDescription() -> String {
        return self.descr
    }
}

extension FASAppInfo: FacebookContentShareableURLs {
    func fbShareContentURL() -> NSURL? {
        return NSURL(string: FASEnvironment.AppStoreURL())
    }
    
    func fbShareContentImageURL() -> NSURL? {
        return nil // let FB to load the itunesconnect icon
    }
}

extension FASAppInfo: FacebookContentShareable {} // explicitly comforms

///////////////////////////////////////////////
// MARK: MFMailComposeViewControllerDelegate -
///////////////////////////////////////////////

extension FASActionController: MFMailComposeViewControllerDelegate {
    func mailComposeController(controller:MFMailComposeViewController, didFinishWithResult result:MFMailComposeResult, error:NSError?) {
        
        var completionError: NSError? = nil
        
        switch result.rawValue {
        case MFMailComposeResultCancelled.rawValue:
            fallthrough
        case MFMailComposeResultSaved.rawValue:
            fallthrough
        case MFMailComposeResultSent.rawValue:
            // treat the statuses above as "success", no error is returned
            completionError = nil
        case MFMailComposeResultFailed.rawValue:
            var errorUserInfo: [NSObject: NSObject] = [NSLocalizedDescriptionKey: "mail send failed"]
            if let _ = error {
                errorUserInfo[NSUnderlyingErrorKey] = error!
            }
            completionError = FASActionControllerError.MailSendFailed.createNSError(userInfo: errorUserInfo)
        default:
            break
        }
        
        if let nav = viewController!.navigationController {
            nav.dismissViewControllerAnimated(false, completion: { [weak self] () -> Void in
                withExtendedLifetime(self) {
                    self!.completionBlock?(error: completionError)
                    self!.completionBlock = nil;
                }
            })
        }
        else {
            viewController!.dismissViewControllerAnimated(false, completion: { [weak self] () -> Void in
                withExtendedLifetime(self) {
                    self!.completionBlock?(error: completionError)
                    self!.completionBlock = nil;
                }
            })
        }
    }
}