//
//  FASImageAndTwoLabelsHeaderViewDataProtocol.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 3/15/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#ifndef FASWorkouts_FASImageAndTwoLabelsHeaderViewDataProtocol_h
#define FASWorkouts_FASImageAndTwoLabelsHeaderViewDataProtocol_h
@class UBIRemoteImage;

@protocol FASImageAndTwoLabelsHeaderViewData
- (NSString *)leftTitle;
- (NSString *)leftValue;
- (NSString *)rightTitle;
- (NSString *)rightValue;
- (UBIRemoteImage *)image;
- (NSString *)imageTitle;
@end

#endif
