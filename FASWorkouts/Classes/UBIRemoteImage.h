//
//  UBIRemoteImage.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/1/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

@import Foundation;
@import UIKit;

typedef NS_ENUM(NSUInteger, UBIRemoteImageStatus) {
    UBIRemoteImageStatusIdle,
    UBIRemoteImageStatusPreparingToLoad,
    UBIRemoteImageStatusLoadingFromFile,
    UBIRemoteImageStatusLoadedFromFile,
    UBIRemoteImageStatusLoadingFromURL,
    UBIRemoteImageStatusLoadedFromURL,
    UBIRemoteImageStatusFailedToLoadFromURL,
    UBIRemoteImageStatusNoImage,
    UBIRemoteImageStatusDefaultImage
};

typedef NS_ENUM(NSUInteger, UBIRemoteImageContentType) {
    UBIRemoteImageContentTypeUnknown,
    UBIRemoteImageContentTypeJPEG,
    UBIRemoteImageContentTypePNG,
    UBIRemoteImageContentTypeGIF,
    UBIRemoteImageContentTypeTIFF,
};

@interface UBIRemoteImage : NSObject

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) UIImage *thumbnail;
@property (nonatomic, readonly) UBIRemoteImageStatus status;
@property (nonatomic, readonly) BOOL isLoading;
@property (nonatomic, readonly) UBIRemoteImageContentType imageType;
@property (nonatomic, readonly) NSError *error;

@property (nonatomic, strong) NSString *loadingImageName;
@property (nonatomic, strong) NSString *defaultImageName;
@property (nonatomic, copy) NSString *filepath;
@property (nonatomic, strong) NSURL *URL;

@property (nonatomic) CGSize maxSize;

- (BOOL)shouldHaveImage;
- (void)clearDefaultImage;

@end
