//
//  FASUserProfileViewController.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 2/20/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

enum FASUserProfileSection: Int {
    case History, Statistics
    func button() -> UIButton {
        let button: UIButton = FASToolbarButton.fasToolbarButton()
        
        switch self {
        case .History:
            button.setTitle("History", forState: .Normal)
        case .Statistics:
            button.setTitle("Statistics", forState: .Normal)
        }
        
        button.tag = buttonTag
        
        button.sizeToFit()
        
        return button
    }
    
    var buttonTag: Int {
        return self.rawValue
    }
}

class FASUserProfileViewController: FASViewController {
    
    @IBOutlet weak var userInfoHeaderView: FASImageAndTwoLabelsHeaderView!
    @IBOutlet weak var scopeToolbar: UIToolbar!
    
    var containerViewController: ContainerViewController?
    let allowUserToChangePhoto = false
    
    // TODO: w/o the property, ARC deallocates the instance and it does not call delegate's methods
    // either make photoController as a singleton, or use object reatiner
    var photoController: UBIPhotoController?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.title = createTitle()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        backgroundImageView.contentMode = .ScaleAspectFill
        
        userInfoHeaderView.labelStyle = .Reverse
        userInfoHeaderView.delegate = self
        // TODO: comment when labels should display some values
        userInfoHeaderView.leftTextView.hidden = true
        userInfoHeaderView.rightTextView.hidden = true
        
        // TODO: should be 'FASAPISession.currentSession().userInfo'
        if let ui = FASAPISession.currentSession().editableUserInfo {
            
            let savedValue = NSUserDefaults.standardUserDefaults().stringForKey("myAvatar")
                
            if (savedValue != nil) {
                print("savedValue")
                print(savedValue)
                if let url = NSURL(string: savedValue!) {
                    if let data = NSData(contentsOfURL: url) {
                        
                        let image = UIImage(data: data)
                        
                        var photoProcessorInputInfo: [String : AnyObject] = [:]
                        
                       // if let image: UIImage = photoInfo[UBIPhotoControllerPhotoInfoImageKey] as? UIImage {
                            photoProcessorInputInfo.updateValue(image!, forKey: UBIPhotoControllerPhotoInfoImageKey)
                      //  }
                        
                       
                        
                        let userInfo: FASUserInfo = FASAPISession.currentSession().editableUserInfo
                        
                        // TODO: for testing only
                        photoProcessorInputInfo.updateValue(userInfo, forKey: "userInfo")
                        
                        UBIPhotoProcessor.photoProcessorWithInfo(photoProcessorInputInfo, options: ([.SaveToDisc, .FxBlur]) ) { (outputInfo) -> Void in
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                userInfo.refreshImage()
                                self.userInfoHeaderView.remoteImageView.remoteImage = userInfo.remoteImage()
                                self.backgroundImageView.image = userInfo.userPicture.applyFASImageDarkEffect()
                            })
                            printlnDebug("completed")
                            
                        }
                        
                        
                    }
                }
            
            } else {
            
                userInfoHeaderView.remoteImageView.remoteImage = ui.remoteImage()
            }
            
            backgroundImageView.image = ui.userPicture?.applyFASImageDarkEffect()
        }
        
        // uncomment to add 'Edit' button to top right
//        let editButtonItem = UIBarButtonItem(title: "Edit", style: .Plain, target: self, action: "editProfileButtonDidPress:")
//        navigationItem.rightBarButtonItem = editButtonItem
        
        let flexLeft = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        let historyButton = FASUserProfileSection.History.button()
        let historyBarButton = UIBarButtonItem(customView: historyButton)
        let flexRight = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
        self.scopeToolbar.setItems([flexLeft,historyBarButton,flexRight], animated: false)

        // configure Toolbar selection
        // HACK: set default vc by hardcoded tag (0)
        // HACK: set button selected by default
        historyButton.selected = true
        containerViewController?.selectViewControllerWithTag( ContainerVCTag(rawValue: 0 )! )
    }
    
    ///////////////////////////////////////////////
    // MARK: - Segue -
    ///////////////////////////////////////////////
    
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject?) -> Bool {
        return true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "embedContainer" {
            if let expectedVC = segue.destinationViewController as? ContainerViewController {
                containerViewController = expectedVC
            }
        }
    }
    
    ///////////////////////////////////////////////
    // MARK: - Helpers -
    ///////////////////////////////////////////////
    
    private func createTitle() -> String {
        // TODO: localize this
        let defaultTitle = "Profile"
        var aTitle: String?
        if FASAPISession.currentSession().isLoggedIn() {
             aTitle = FASAPISession.currentSession().userInfo.email?.stringByTakingFirstPartOfEmailAddress()
        }
        
        if let noNilTitle = aTitle {
            aTitle = !noNilTitle.isEmpty ? noNilTitle : defaultTitle
        }
        else {
            aTitle = defaultTitle
        }
        
        return aTitle!
    }
    
    ///////////////////////////////////////////////
    // MARK: - Actions -
    ///////////////////////////////////////////////
    
    @objc func editProfileButtonDidPress(sender: AnyObject?) {
        printlnDebugTrace("editProfileButtonDidPress")
    }
}

extension FASUserProfileViewController : FASImageAndTwoLabelsHeaderViewDelegate {
    
    func imageAndTwoLabelsHeaderView(view: FASImageAndTwoLabelsHeaderView, didAction action: FASImageAndTwoLabelsHeaderViewAction) {
        switch action {
        case .DidPressImage:
            if allowUserToChangePhoto {
                presentChangePhotoPrompt()
            }
            else {
                // no photo, show options to add one
                presentPhotoController()
            }
        }
    }
    
    private func presentPhotoController() {
        self.photoController = UBIPhotoController()
        self.photoController?.delegate = self
        self.photoController!.chooseActionWithViewController(self)
    }
    
    private func presentChangePhotoPrompt() {
        let alertCtrl = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let deleteAction = UIAlertAction(title: "Delete", style: .Destructive, handler: { (anAction) -> Void in
            // delete photo
        });
        let updateAction = UIAlertAction(title: "Update...", style: .Default, handler: { (anAction) -> Void in
            self.presentPhotoController()
        });
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { (anAction) -> Void in
            // cancel
        }
        
        alertCtrl.addAction(deleteAction)
        alertCtrl.addAction(updateAction)
        alertCtrl.addAction(cancelAction)
        
        // popover on iPad
        if FASData.sharedData().isIPAD {
            alertCtrl.modalPresentationStyle = .Popover
            
            if let popPresenter = alertCtrl.popoverPresentationController {
                let button = self.userInfoHeaderView.imageButton
                
                popPresenter.sourceView = button;
                popPresenter.sourceRect = button.bounds;
            }
            else {
                printlnDebug("[WARNING]: cannot create a popover")
            }
        }
        
        self.presentViewController(alertCtrl, animated: true, completion: { () -> Void in
            // TODO: what's here?
        })
    }
}

extension FASUserProfileViewController : UBIPhotoControllerPopover {
    func popoverViewForPhotoController(photoController: UBIPhotoController!) -> UIView! {
        return self.userInfoHeaderView.imageButton as UIView
    }
    
    func popoverBarButtonForPhotoController(photoController: UBIPhotoController!) -> UIBarButtonItem! {
        return nil
    }
    
    func popoverRectForPhotoController(photoController: UBIPhotoController!) -> CGRect {
        return self.userInfoHeaderView.imageButton.bounds
    }
}

extension FASUserProfileViewController : UBIPhotoControllerDelegate {
    func photoController(photoController: UBIPhotoController!, didFailWithError error: NSError!) {
        // TODO: display an error
    }
    
    func photoController(photoController: UBIPhotoController!, didFinishWithPhotoInfo photoInfo: [NSObject : AnyObject]!) {
        
        NSUserDefaults.standardUserDefaults().removeObjectForKey("myAvatar")
        
        
        var photoProcessorInputInfo: [String : AnyObject] = [:]
        
        if let image: UIImage = photoInfo[UBIPhotoControllerPhotoInfoImageKey] as? UIImage {
            photoProcessorInputInfo.updateValue(image, forKey: UBIPhotoControllerPhotoInfoImageKey)
        }
        
        if let imageSource: NSNumber = photoInfo[UBIPhotoControllerPhotoInfoImageKey] as? NSNumber {
            photoProcessorInputInfo.updateValue(imageSource, forKey: UBIPhotoControllerPhotoInfoImageKey)
        }
        
        let userInfo: FASUserInfo = FASAPISession.currentSession().editableUserInfo
        
        // TODO: for testing only
        photoProcessorInputInfo.updateValue(userInfo, forKey: "userInfo")
        
        UBIPhotoProcessor.photoProcessorWithInfo(photoProcessorInputInfo, options: ([.SaveToDisc, .FxBlur]) ) { (outputInfo) -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                userInfo.refreshImage()
                self.userInfoHeaderView.remoteImageView.remoteImage = userInfo.remoteImage()
                self.backgroundImageView.image = userInfo.userPicture.applyFASImageDarkEffect()
            })
            printlnDebug("completed")
            
        }
    }
}