//
//  FASPhaseView.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/9/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit

class FASPhaseView: UIVisualEffectView {
    //@IBOutlet weak var progressView: FASProgressContentView!
    @IBOutlet weak var workoutProgressView: FASWorkoutProgressView!
    
    func setProgress(exerciseProgress : Int, totalExerciseCount : Int) {
        workoutProgressView.setProgress(exerciseProgress, totalExerciseCount: totalExerciseCount)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
