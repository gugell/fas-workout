//
//  WorkoutHistory.m
//  FASWorkouts
//
//  Created by Vladimir Stepanchenko on 9/17/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "WorkoutHistory.h"
#import "FASAPIObjectKeys.h"
#import "FASData.h" // <== cross-reference
#import "NSString+MyExtensions.h"
//#import "FASWorkouts-Swift.h"
#import "UBILogger.h"

static NSDateFormatter *    sDateFormatter = nil;

@implementation WorkoutHistory

@dynamic workoutHistory_id;
@dynamic workout_id;
@dynamic rate;
@dynamic isCompleted;
@dynamic startDate;
@dynamic endDate;
@dynamic workout;
@dynamic pauseDuration;

///////////////////////////////////////////////
#pragma mark -
#pragma mark UBIUpdatableWithDictionary protocol conformance
#pragma mark -
///////////////////////////////////////////////
- (void)addValuesFromDictionary:(NSDictionary *)dict
{
    for (NSString *key in [dict allKeys]) {
        
        id value = [dict valueForKey:key];
        
        // if value is an array
        if ([value isKindOfClass:[NSArray class]]) {
            UBILogWarning(@"Value (%@) is an array", value);
            
            continue;
        }
        
        // if value is a dictionary
        // else, add it directly
        else {
            
            if ([value isKindOfClass:[NSNull class]]) { continue; }
            
            // date / time
            // TODO: substitute w/ NSDate UBIDate extension
            if ([key isEqualToString:FASAPIStartDate] || [key isEqualToString:FASAPIEndDate]) {
                if (!sDateFormatter) {
                    sDateFormatter = [NSDateFormatter new];
                    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
                    [sDateFormatter setLocale:enUSPOSIXLocale];
                    [sDateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
                    [sDateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
                }
                NSDate *date = [sDateFormatter dateFromString:value];
                
                if ([key isEqualToString:FASAPIStartDate]) {
                    self.startDate = date;
                } else {
                    self.endDate = date;
                }
                
                continue;
            } else if ([key isEqualToString:FASAPIIdentifier]) {
                self.workoutHistory_id = dict[FASAPIIdentifier];
                
                continue;
            } else if ([key isEqualToString:FASAPIWorkoutID]) {
                self.workout_id = dict[FASAPIWorkoutID];
                continue;
            }
            
            [self setValue:value forKey:key];
        }
    }
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark NSKeyValueCoding protocol
#pragma mark -
///////////////////////////////////////////////

- (id)valueForUndefinedKey:(NSString *)key
{
    return nil;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    UBILogWarning(@"%@: UNDEFINED value = %@\tkey = %@", NSStringFromClass([self class]), value, key);
}

@end
