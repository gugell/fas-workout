//
//  FASUserInfo.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 3/20/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UnityBase.h"

@class UBIRemoteImage;

@interface FASUserInfo : NSObject <NSCoding, UBIUpdatableWithDictionary>

@property (nonatomic, copy, readonly) NSString *email;
@property (nonatomic, strong, readonly) NSNumber* userID;

@property (nonatomic, getter=isLocal) BOOL local;

// representaion used for requests, printing, etc.
- (NSString *)stringUserID;
// validation the userID exist and not equal to 0
- (BOOL)isValidUserID;

// user picture
@property (nonatomic, strong) UIImage* userPicture;
- (NSString *)originalPhotoPath;
- (UBIRemoteImage *)remoteImage;
- (void)refreshImage;


// TODO: add other key-value from uwserInfo response
@end
