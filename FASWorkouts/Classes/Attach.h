//
//  Attach.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/15/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "UnityBase.h"
#import "UBIAttachFileCache.h"

typedef NS_ENUM(NSUInteger, AttachContentType) {
    AttachContentTypeUnknown = 0,
    AttachContentTypeVideo,
    AttachContentTypePhoto,
};

@class Exercise;

@interface Attach : NSManagedObject
<UBIUpdatableWithDictionary>

// persistent attributes (API)
@property (nonatomic, retain, readonly) NSDate *    createdOn;
@property (nonatomic, retain, readonly) NSNumber *  attach_id;
@property (nonatomic, retain, readonly) NSString *  lang;
@property (nonatomic, retain, readonly) NSString *  type;
@property (nonatomic, retain, readonly) NSString *  md5;
@property (nonatomic, retain, readonly) NSString *  ct;
@property (nonatomic, retain, readonly) NSString *  origName;
@property (nonatomic, retain, readonly) NSDate *    mi_modifyDate;

// persistent attributes (calculated)

// trancient attributes
@property (nonatomic, retain, readonly) NSString *  filename;
@property (nonatomic, retain, readonly) NSString *  extension;
@property (nonatomic, retain, readonly) NSString *  filenameWithExtension;
@property (nonatomic, copy, readonly) NSURL *       filepath;
@property (nonatomic, retain) NSNumber *            downloadProgress;

// relationships
@property (nonatomic, retain) Exercise *exercise;

- (AttachContentType)contentType;

// actions
- (void)downloadFile;
- (AttachDownloadRetval)downloadFile:(BOOL)force;
- (BOOL)isFileDownloaded;

+ (UBIAttachFileCache *)fileCache;

@end