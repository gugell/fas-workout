//
//  UBILabel.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 8/26/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "UBILabel.h"

@interface UBILabel()
@property (nonatomic, strong)   UIImageView *   imageView;
@end

@implementation UBILabel

- (instancetype)init
{
    self = [super init];
    if (self) {
        _autoSize = NO;
        _maxSize = CGSizeZero;
        _edgeInsets = UIEdgeInsetsZero;
        _imagePadding = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 2.0f);
        _imageAlignment = UBILabelImageAlignmentMiddle;
        self.lineBreakMode = NSLineBreakByTruncatingTail;
        self.clipsToBounds = YES;
    }
    return self;
}

- (void)setText:(NSString *)text
{
    [super setText:text];
    if (self.autoSize) {
        [self sizeToFit];
    }
}

- (UIImage *)image
{
    UIImage *image = nil;
    if (self.imageView) {
        image = self.imageView.image;
    }
    return image;
}

- (void)setImage:(UIImage *)image
{
    if (image) {
        [self.imageView removeFromSuperview];
        self.imageView = [[UIImageView alloc] initWithImage:image];
        [self addSubview:self.imageView];
    }
    else {
        [self.imageView removeFromSuperview];
        self.imageView = nil;
    }
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (CGSize)sizeThatFits:(CGSize)size
{
    CGFloat imageAdj = 0.0f;
    if (self.image) {
        imageAdj = self.image.size.width + self.imagePadding.right;
    }
    
    size.height = 0.0f;
    // start with maxSize
    if (self.maxSize.width > 0.0f) {
        size.width = self.maxSize.width;
    }
    size.width -= (self.edgeInsets.left + self.edgeInsets.right + imageAdj);
    
    CGSize newSize = [super sizeThatFits:size];
    
    newSize.width += (self.edgeInsets.left + self.edgeInsets.right + imageAdj);
    newSize.height += (self.edgeInsets.top + self.edgeInsets.bottom);
    
    if (self.maxSize.height > 0.0f && newSize.height > self.maxSize.height) {
        newSize.height = self.maxSize.height;
    }
    
    if (self.maxSize.width > 0.0f && newSize.width > self.maxSize.width) {
        newSize.width = self.maxSize.width;
    }
    
    return newSize;
}

- (void)drawTextInRect:(CGRect)rect
{
    CGRect adjustedRect = UIEdgeInsetsInsetRect(rect, self.edgeInsets);
    if (self.image) {
        CGFloat adj = self.image.size.width + self.imagePadding.right;
        adjustedRect.origin.x += adj;
        adjustedRect.size.width -= adj;
    }
    
    [super drawTextInRect:adjustedRect];
}

- (CGRect)textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines
{
    CGRect adjustedRect = UIEdgeInsetsInsetRect(bounds, self.edgeInsets);
    if (self.image) {
        CGFloat adj = self.image.size.width + self.imagePadding.right;
        adjustedRect.origin.x += adj;
        adjustedRect.size.width -= adj;
    }
    return [super textRectForBounds:adjustedRect limitedToNumberOfLines:numberOfLines];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    if (self.imageView) {
        CGRect imageFrame = self.imageView.frame;
        imageFrame.origin.x = self.edgeInsets.left;
        
        switch (self.imageAlignment) {
            case UBILabelImageAlignmentMiddle: {
                imageFrame.origin.y = CGRectGetMidY(self.bounds) - CGRectGetMidY(_imageView.bounds);
            } break;
                
            case UBILabelImageAlignmentTop: {
                imageFrame.origin.y = self.imagePadding.top;
            } break;
                
            case UBILabelImageAlignmentBottom: {
                imageFrame.origin.y = CGRectGetMaxY(self.bounds) - CGRectGetHeight(self.imageView.bounds);
            } break;
        }
        self.imageView.frame = imageFrame;
    }
}

@end
