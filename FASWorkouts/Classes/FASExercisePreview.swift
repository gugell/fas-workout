//
//  FASExercisePreview.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/9/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit

class FASExercisePreview: UIView {
    
    var exercise: Exercise? {
        didSet {
            updateView()
        }
    }
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var remoteImageView: UBIRemoteImageView!
    
    @IBOutlet weak var finishButton: UIButton!
    
    
    
    
    override func awakeFromNib() {
        
        //finish.layer.cornerRadius = 3
    
   
        title.font = UIFont.boldBabasFontOfSize( 20.0 )
        title.textColor = UIColor.whiteColor()
        title.adjustsFontSizeToFitWidth = true
        title.minimumScaleFactor = 0.5
        if UIScreen.mainScreen().bounds.height < 568 {
            subtitle.font = UIFont.boldBabasFontOfSize( 36.0 )
        } else {
            subtitle.font = UIFont.boldBabasFontOfSize( 40.0 )
        }
        subtitle.textColor = UIColor.fasYellowColor()
        subtitle.adjustsFontSizeToFitWidth = true
        subtitle.minimumScaleFactor = 0.5
    }
    
    private func updateView() {
        if let ex = exercise {
            subtitle.text = ex.name ?? ""
            title.text = "next exercise"
            remoteImageView.remoteImage = ex.remoteImage()
            remoteImageView.hidden = false
            if (finishButton != nil) {
            finishButton.hidden = true
            finishButton.titleLabel!.font = UIFont.boldBabasFontOfSize( 40.0 )
            }
        }
        else {
            subtitle.text = ""
            title.text = ""
            remoteImageView.hidden = true
            if (finishButton != nil) {
            finishButton.hidden = false
                finishButton.titleLabel!.font = UIFont.boldBabasFontOfSize( 40.0 )
            }
        }
    }
    
    // MARK: - UI customization
    override func layoutSubviews() {
        super.layoutSubviews()
        if let ex = exercise {
            remoteImageView.applyStyleForRemoteImageView()
        }
    }
}
