//
//  UBIArrayDataSource.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 2/20/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

// an array data source is an array of sections as follows:
//
//      @[ section1, section2, section3, ... ];
//
//
// each section is a dictionary as follows:
//
//      @{
//          kUBIDataSourceTitle : @"Section Title",
//          kUBIDataSourceCellIdentifier : @"Default Cell Identifier",
//          kUBIDataSourceItems : @[ item1, item2, item3, ... ]
//      };
//
//
// each item is a dictionary as follows
//
//      @{
//          kUBIDataSourceTitle : @Item Title",
//          kUBIDataSourceSubtitle : @"Item Subtitle",
//          kUBIDataSourceCellIdentifier : @"Cell Identifier",
//          kUBIDataSourceItemIdentifier : @"Item Identifier",
//          kUBIDataSourceItemInfo : @{}
//      }

#import <Foundation/Foundation.h>
#import "UBIDataSource.h"
#import <UIKit/UIKit.h>

typedef NSString *(^cellIdentifier_t)(void);
typedef UITableViewCell *(^createCell_t)(NSString *);
typedef void (^configureCell_t)(UITableViewCell *, id);

@interface UBIArrayDataSource : NSObject
<UITableViewDataSource, UBIDataSource>

@property (nonatomic, weak)     UITableView *       tableView;

@property (nonatomic, copy)     NSArray *           sections;

@property (nonatomic, copy)     cellIdentifier_t    cellIdentifierBlock;
@property (nonatomic, copy)     createCell_t        createCellBlock;
@property (nonatomic, copy)     configureCell_t     configureCellBlock;

- (instancetype)initWithSections:(NSArray *)sections;
@end
