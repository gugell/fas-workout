//
//  SettingsData.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/29/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import Foundation

enum UBISettingTag: UInt {
    case DebugTestQuery                 = 0
    case DebugSkipWelcomeScreen         = 1
    case DebugForceDownloadAttachFiles  = 2
    
    case NotificationsLocalEnable       = 3
    case NotificationsLocalDate         = 4
    case NotificationsPickDate          = 5
    
    case NotificationsRemotePromoEnable = 6
    
    case SystemNotificationsPermissions = 7
    
    case DebugAPNSToken                 = 8
    case DebugUserID                    = 9
    case DebugCrashNow                  = 10
    // TODO: add more
}

enum UBISettingCellStyle: UInt, CustomStringConvertible {
    case Default = 0
    case Checkmark = 1
    case Switch = 2
    case ValueRight = 3
    case Action = 4
    case Subtitle = 5
    case ActivityIndicator = 6
    case DatePicker = 7
    
    var description : String {
        switch self {
        case .Default: return "Default"
        case .Checkmark: return "Checkmark"
        case .Switch: return "Switch"
        case .ValueRight: return "ValueRight"
        case .Action: return "Action"
        case .Subtitle: return "Subtitle"
        case .ActivityIndicator: return "ActivityIndicator"
        case .DatePicker: return "DatePicker"
        }
    }
}

protocol UBISetting {
    var style: UBISettingCellStyle { get set }
    var tag: UBISettingTag { get set }
    var title: String? { get set }
    var value: AnyObject? { get set }
    
    init(tag: UBISettingTag)
}

final class UBIConcreteSetting: UBISetting {
    var style: UBISettingCellStyle
    var tag: UBISettingTag
    var title: String?
    var value: AnyObject? {
        didSet {
            valueDidSet()
        }
    }
    
    required init(tag: UBISettingTag) {
        self.tag = tag
        self.style = .Default
        configure()
    }
    
    private func configure() {
        switch tag {
        case .DebugTestQuery:
            self.style = .Default
            self.title = "Run Test Query"
        case .DebugForceDownloadAttachFiles:
            self.style = .Checkmark
            self.title = "Force download attach files"
            self.value = FASData.sharedData().forceDownloadAttachFiles()
        case .DebugSkipWelcomeScreen:
            self.style = .Checkmark
            self.title = "Skip 'Welcome' sceen upon start"
            self.value = FASData.sharedData().skipWelcomeScreen()
        case .NotificationsLocalEnable:
            self.style = .Switch
            self.title = "Reminder"
            self.value = FASAppPreferences.sharedInstance().notificationsLocalEnabledByUser
        case .NotificationsLocalDate:
            self.style = .ValueRight
            self.title = "Time"
            self.value = FASAppPreferences.sharedInstance().notificationsLocalDate
            // TODO: modify this setting to bear not only the date value but also a flag
            // which indicates whether we should show the date picker or not
            // this will allow an effect of showing date picker on cell tap
        case .NotificationsRemotePromoEnable:
            self.style = .Switch
            self.title = "Receive News"
            self.value = FASAppPreferences.sharedInstance().notificationsRemotePromoEnabled
        case .SystemNotificationsPermissions:
            self.style = .Action
            self.title = "Open Settings"
        case .DebugAPNSToken:
            self.style = .Subtitle
            self.title = "APNS Token"
            let appDelegate = UIApplication.sharedApplication().delegate as! FASAppDelegate
            self.value = appDelegate.APNSToken
        case .DebugUserID:
            self.style = .Subtitle
            self.title = "User ID"
            let userID = FASAPISession.currentSession().userInfo.userID
            self.value = "\(userID)"
        case .DebugCrashNow:
            self.style = .Action
            self.title = "Crash Now"
        case .NotificationsPickDate:
            self.style = .DatePicker
            self.value = FASAppPreferences.sharedInstance().notificationsLocalDate
        }
    }
    
    private func valueDidSet() {
        switch tag {
        case .DebugForceDownloadAttachFiles:
            if let newValue = value as? Bool where newValue != FASData.sharedData().forceDownloadAttachFiles() {
                FASData.sharedData().setForceDownloadAttachFiles(newValue)
            }
        case .DebugSkipWelcomeScreen:
            if let newValue = value as? Bool where newValue != FASData.sharedData().skipWelcomeScreen() {
                FASData.sharedData().setSkipWelcomeScreen(newValue)
            }
        case .NotificationsLocalEnable:
            if let newValue = value as? Bool where newValue != FASAppPreferences.sharedInstance().notificationsLocalEnabledByUser {
                FASAppPreferences.sharedInstance().notificationsLocalEnabledByUser = newValue
            }
        case .NotificationsLocalDate: break
            // TODO: uncomment when you want to set value directly to this setting
//            if let newValue = value as? NSDate where newValue != FASAppPreferences.sharedInstance().notificationsLocalDate {
//                FASAppPreferences.sharedInstance().notificationsLocalDate = newValue
//            }
        case .NotificationsRemotePromoEnable:
            if let newValue = value as? Bool where newValue != FASAppPreferences.sharedInstance().notificationsRemotePromoEnabled {
                FASAppPreferences.sharedInstance().notificationsRemotePromoEnabled = newValue
                // TODO: if the settings should be synced with the server, here is the place to submit a request
            }
        case .NotificationsPickDate:
            if let newValue = value as? NSDate where newValue != FASAppPreferences.sharedInstance().notificationsLocalDate {
                FASAppPreferences.sharedInstance().notificationsLocalDate = newValue
            }
        default:
            // .DebugTestQuery:
            // .DebugAPNSToken:
            // .DebugCrashNow
            0 // doing nothing
        }
    }
}

enum UBISettingsType: UInt, CustomStringConvertible {
    case Default = 0
    case Notifications = 1
    case Debug = 2
    
    var description : String {
        switch self {
        case .Default: return "Default"
        case .Notifications: return "Notifications"
        case .Debug: return "Debug"
        }
    }
}

final class UBISettings {
    
    class func settingsForType(type: UBISettingsType) -> [UBISettingsGroup] {
        switch type {
        case .Default:
            return defaultSettings()
        case .Notifications:
            return notificationsSettings()
        case .Debug:
            return debugSettings()
        }
    }
    
    class func defaultSettings() -> [UBISettingsGroup] {
        
        var settings: [UBISettingsGroup] = []
        var sectionInfo: UBISettingsGroup?
        var sectionSettings: [UBISetting] = []
        
    #if DEBUG_SETTINGS
        sectionSettings.append( UBIConcreteSetting(tag: .DebugTestQuery) )
        sectionSettings.append( UBIConcreteSetting(tag: .DebugForceDownloadAttachFiles) )
    #endif
        sectionSettings.append( UBIConcreteSetting(tag: .DebugSkipWelcomeScreen) )
        
        // section #1
        sectionInfo = UBISettingsGroup(headerTitle: nil, footerTitle: nil, items: sectionSettings)
        
        settings.append( sectionInfo! )
        
        return settings
    }
    
    class func notificationsSettings() -> [UBISettingsGroup] {
        
        var settings: [UBISettingsGroup] = []
        var sectionInfo: UBISettingsGroup?
        var sectionSettings: [UBISetting] = []
        
        // section #0 - permissions
        let enableLocalNotificationSetting = UBIConcreteSetting(tag: .NotificationsLocalEnable)
        let enableRemoteNotificationSetting = UBIConcreteSetting(tag: .NotificationsRemotePromoEnable)
        let isLocalNotificationEnabled = enableLocalNotificationSetting.value as! Bool
        let isRemoteNotificationEnabled = enableRemoteNotificationSetting.value as! Bool
        // IF app cannot receive remote
        // OR cannot present remote and local notifications
        // AND any notification setting is currently ON,
        // THEN show warning with "Go to settings" button
        if ((UIApplication.sharedApplication().isRegisteredForRemoteNotifications() == false
            || UIApplication.sharedApplication().isAnyUserFacingNotificationTypesAllowed() == false)
            && (isLocalNotificationEnabled || isRemoteNotificationEnabled)
            ) {
            sectionSettings.append( UBIConcreteSetting(tag: .SystemNotificationsPermissions) )
            sectionInfo = UBISettingsGroup(headerTitle: "You will not receive notifications and reminders", footerTitle: nil, items: sectionSettings)
            settings.append( sectionInfo! )
            sectionSettings.removeAll(keepCapacity: true)
        }
        
        // section #1 - remote
        sectionSettings.append( enableRemoteNotificationSetting )
        sectionInfo = UBISettingsGroup(headerTitle: nil, footerTitle: "system and application updates sent to user as remote notifications", items: sectionSettings)
        settings.append( sectionInfo! )
        
        sectionSettings.removeAll(keepCapacity: true)
        
        // section #2 - local
        sectionSettings.append( enableLocalNotificationSetting )
        sectionSettings.append( UBIConcreteSetting(tag: .NotificationsLocalDate) )
        // TODO: this setting should appear conditionaly
        // to accomodate with Apple's UI pattern for Date Picker
        sectionSettings.append( UBIConcreteSetting(tag: .NotificationsPickDate) )
        
        // create scheduled notif info
        let date = FASData.sharedData().fireDateForLocalNotificationReminder()
        let reminderDateTimeLocalizedDescription: String? = (date != nil ? "daily reminders starting from \(date!.toStringMediumDateShortTime())" : nil)
        
        sectionInfo = UBISettingsGroup(headerTitle: nil, footerTitle: reminderDateTimeLocalizedDescription, items: sectionSettings)

        settings.append( sectionInfo! )
        
        return settings
    }
    
    class func debugSettings() -> [UBISettingsGroup] {
        var settings: [UBISettingsGroup] = []
        var sectionInfo: UBISettingsGroup?
        var sectionSettings: [UBISetting] = []
        
        // section #0 - common
        sectionSettings.append( UBIConcreteSetting(tag: .DebugAPNSToken) )
        sectionSettings.append( UBIConcreteSetting(tag: .DebugUserID) )
        sectionSettings.append( UBIConcreteSetting(tag: .DebugCrashNow) )
        sectionInfo = UBISettingsGroup(headerTitle: nil, footerTitle: nil, items: sectionSettings)
        settings.append( sectionInfo! )
        
        return settings
    }
}

// TODO: create 'protocol UBITableViewDataSourcePresentable' and conform the below to it

struct UBISettingsGroup {
    var headerTitle: String?
    var footerTitle: String?
    var items: [UBISetting]?
}