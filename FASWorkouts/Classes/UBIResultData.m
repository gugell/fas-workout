//
//  UBIResultData.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/31/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "UBIResultData.h"

const NSString* UBIAPIResultDataFieldsKey      = @"fields";
const NSString* UBIAPIResultDataDataKey        = @"data";
const NSString* UBIAPIResultDataRowCountKey    = @"rowCount";

@interface UBIResultData ()
@property (nonatomic, strong) NSArray* fields;
@property (nonatomic, strong) NSArray* data;
@property (nonatomic, strong) NSNumber* rowCount;
@end

@implementation UBIResultData

- (void)addValuesFromDictionary:(NSDictionary *)dict
{
    if ([dict[UBIAPIResultDataFieldsKey] isKindOfClass:[NSArray class]]) {
        self.fields = dict[UBIAPIResultDataFieldsKey];
    }
    
    if ([dict[UBIAPIResultDataDataKey] isKindOfClass:[NSArray class]]) {
        self.data = dict[UBIAPIResultDataDataKey];
    }
    
    self.rowCount = dict[UBIAPIResultDataRowCountKey];
}

- (NSArray *)arrayOfDictionaryData
{
    NSUInteger count = self.rowCount.unsignedIntegerValue;
    if (count == 0) {
        return [NSArray array]; // ???: or should we return 'nil'?
    }
    
    __block NSMutableArray* resultData = [NSMutableArray arrayWithCapacity:count];
    for (int i=0; i<count; i++) {
        @autoreleasepool {
            NSDictionary* dict = [NSDictionary dictionaryWithObjects:self.data[i] forKeys:self.fields];
            [resultData addObject:dict];
        }
    }
    
    return [resultData copy];
}

@end
