//
//  UIFont+CustomFonts.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/5/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

extension UIFont {
    class func regularBabasFontOfSize(fontSize: CGFloat) -> UIFont {
        return UIFont(name: "BebasNeueRegular", size: fontSize)!
    }
    
    class func thinBabasFontOfSize(fontSize: CGFloat) -> UIFont {
        return UIFont(name: "BebasNeue-Thin", size: fontSize)!
    }
    
    class func bookBabasFontOfSize(fontSize: CGFloat) -> UIFont {
        return UIFont(name: "BebasNeueBook", size: fontSize)!
    }
    
    class func lightBabasFontOfSize(fontSize: CGFloat) -> UIFont {
        return UIFont(name: "BebasNeueLight", size: fontSize)!
    }
    
    class func boldBabasFontOfSize(fontSize: CGFloat) -> UIFont {
        return UIFont(name: "BebasNeueBold", size: fontSize)!
    }
    
    // TODO: inspect this method for dynamic type
    // http://www.electricpeelsoftware.com/2014/09/04/dynamic-type.html
    
    class func preferredBabasFontForTextStyle (textStyle: NSString) -> UIFont {
        let font = UIFontDescriptor.preferredFontDescriptorWithTextStyle(textStyle as String)
        let fontSize: CGFloat = font.pointSize
        
        let regularFontName: String = "BebasNeueRegular"
        
        if textStyle == UIFontTextStyleHeadline || textStyle == UIFontTextStyleSubheadline {
            let boldFontName = "BebasNeueBold"
            return UIFont(name: boldFontName, size: fontSize)!
        }
        else {
            return UIFont(name: regularFontName, size: fontSize)!
        }
        
        // all styles available:
        
        // UIFontTextStyleHeadline
        // UIFontTextStyleBody
        // UIFontTextStyleSubheadline
        // UIFontTextStyleFootnote
        // UIFontTextStyleCaption1
        // UIFontTextStyleCaption2
    }
}