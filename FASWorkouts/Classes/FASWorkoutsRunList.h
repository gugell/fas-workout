//
//  FASWorkoutsRunList.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/31/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "FASRunList.h"

NS_ASSUME_NONNULL_BEGIN
// fields
extern NSString* const kFASWorkoutsRunListNameFieldKey;
extern NSString* const kFASWorkoutsRunListCodeFieldKey;
extern NSString* const kFASWorkoutsRunListMuscleTypesNameFieldKey;
extern NSString* const kFASWorkoutsRunListRestTimeFieldKey;
extern NSString* const kFASWorkoutsRunListTimeSpanFieldKey;
extern NSString* const kFASWorkoutsRunListLapsNumberFieldKey;
extern NSString* const kFASWorkoutsRunListDifficultyFieldKey;
extern NSString* const kFASWorkoutsRunListMotivationTextFieldKey;
extern NSString* const kFASWorkoutsRunListWorkoutIDFieldKey;
extern NSString* const kFASWorkoutsRunListModifyDateFieldKey;
extern NSString* const kFASWorkoutsRunListCaloriesFieldKey;
extern NSString* const kFASWorkoutsRunListInAppProductIDFieldKey;
extern NSString* const kFASWorkoutsRunListOnSaleFieldKey;
extern NSString* const kFASWorkoutsRunListGenderFieldKey;
extern NSString* const kFASWorkoutsRunListPrimaryMuscleTypeFieldKey;
extern NSString* const kFASWorkoutsRunListSortIndexFieldKey;

@interface FASWorkoutsRunList : FASRunList

- (instancetype)initWithFieldList:(NSArray *)fieldList;
- (instancetype)initWithFieldList:(NSArray *)fieldList whereList:(UBIRunListWhereList * _Nullable)whereList NS_DESIGNATED_INITIALIZER;

+ (instancetype)workoutsRunListDefault;
+ (instancetype)workoutsRunListWithModifyDate;
+ (instancetype)workoutsRunListExcludeIncomplete;
+ (instancetype)workoutsRunListExcludeIncompleteWithModifyDate;

@end
NS_ASSUME_NONNULL_END