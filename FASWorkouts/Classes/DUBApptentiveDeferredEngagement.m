//
//  DUBApptentiveDeferredEngagement.m
//  MyCompany
//
//  Created by Evgen Dubinin on 12/21/15.
//  Copyright © 2015 MyCompany Corporation. All rights reserved.
//

#import "DUBApptentiveDeferredEngagement.h"
#import "ATConnect.h"

@interface DUBApptentiveDeferredEngagement ()
@property (nonatomic, strong) NSMutableArray* deferredEvents;
@end

@implementation DUBApptentiveDeferredEngagement

+ (DUBApptentiveDeferredEngagement*)sharedInstance
{
    static DUBApptentiveDeferredEngagement *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self class] new];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _deferredEvents = [NSMutableArray new];
    }
    return self;
}

- (void)deferEngage:(NSString *)event
{
    if (![self hasDeferredEvent:event]) {
        [self.deferredEvents addObject:event];
    }
}

- (void)cancelDeferredEngage:(NSString *)event
{
    if ([self hasDeferredEvent:event]) {
        [self.deferredEvents removeObject:event];
    }
}

- (BOOL)engageDeferred:(NSString *)event fromViewController:(UIViewController *)viewController
{
    if (![self hasDeferredEvent:event]) {
        return NO;
    }
    else {
        BOOL result = [[ATConnect sharedConnection] engage:event fromViewController:viewController];
        [self.deferredEvents removeObject:event];
        return result;
    }
}

- (BOOL)hasDeferredEvent:(NSString *)event
{
    if (!event) {
        return NO;
    }
    
    return [self.deferredEvents containsObject:event];
}

@end
