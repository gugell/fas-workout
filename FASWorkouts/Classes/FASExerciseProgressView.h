//
//  FASExerciseProgressView.h
//  FASExerciseProgressView
//
//  Created by Vladimir Stepanchenko on 3/13/16.
//  Copyright © 2016 FAS. All rights reserved.
//

@import UIKit;

@interface FASExerciseProgressView : UIView

@property (nonatomic) NSTimeInterval duration;
@property (nonatomic) NSTimeInterval currentTimestamp;

@property (nonatomic,assign) CGFloat value;

@property (nonatomic,assign) CGFloat progressLineWidth;
@property (nonatomic,strong) UIColor *progressColor;

// Convenience for progress 'border' on need
@property (nonatomic,strong)  UIColor *progressStrokeColor;

@property (nonatomic,assign) NSInteger progressCapType;
@property (nonatomic,assign) CGFloat emptyLineWidth;

@property (nonatomic,strong) UIColor *emptyBackgroundColor;

-(void)setTotalTime:(NSTimeInterval)time;
-(void)setCurrrentTime:(NSTimeInterval)time animated:(BOOL)animated;

@end
