//
//  FASAlertView.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/19/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

class FASAlertView: UIAlertView {
    override func show() {
        let notifCenter = NSNotificationCenter.defaultCenter()
        notifCenter.addObserver(self, selector: "appDidEnterBackground", name: UIApplicationDidEnterBackgroundNotification, object: nil)
        super.show()
    }
    
    override func dismissWithClickedButtonIndex(buttonIndex: Int, animated: Bool) {
        let notifCenter = NSNotificationCenter.defaultCenter()
        notifCenter.removeObserver(self, name: UIApplicationDidEnterBackgroundNotification, object: nil)
        
        super.dismissWithClickedButtonIndex(buttonIndex, animated: animated)
    }
    
    func appDidEnterBackground() {
        dismissWithClickedButtonIndex(cancelButtonIndex, animated: true)
    }
}
