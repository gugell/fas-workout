//
//  FASEdgeInsetsLabel.h
//  FASWorkouts
//
//  Created by Oleksandr Nechet on 21.12.15.
//  Copyright © 2015 FAS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FASEdgeInsetsLabel : UILabel
@property (nonatomic, assign) UIEdgeInsets textInsets;
@end
