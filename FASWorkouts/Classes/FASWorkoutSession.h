//
//  FASWorkoutSession.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/4/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FASWorkoutDataTypes.h"

@interface FASWorkoutSession : NSObject <NSCoding>

@property (nonatomic, strong, readonly) NSNumber* workoutID;
// remote id returned by the server when session is posted for the first time
// TODO: consider renaming to "remoteID"
@property (nonatomic, strong, readwrite) NSNumber* workoutHistoryID;
// a date when workout started
@property (nonatomic, strong, readonly) NSDate* beginDate;
// a date when workout aborted/completed
@property (nonatomic, strong, readonly) NSDate* endDate;
// total time use stays on pause
@property (nonatomic, strong, readonly) NSNumber* pauseDuration;
// rate
@property (nonatomic, strong, readonly) NSNumber* rate;
// with endDate != nil, this gives completion status
@property (nonatomic, getter=isCompleted, readonly) BOOL completed;
// indicates whether or not the session has been successfully submitted to the server
// TODO: does workoutHistoryID != 0 indicate the same?
@property (nonatomic, getter=isPosted, readwrite) BOOL posted;

- (instancetype)initWithWorkoutID:(NSNumber*)workoutID;

- (void)complete;
- (void)abort;
- (void)rateWorkout:(NSUInteger)rate;
- (void)setOnPause:(BOOL)isPaused;

- (FASWorkoutUserRate)userRate;

@end

extern NSString * const FASAPIHistoryWorkoutIDKey;
extern NSString * const FASAPIHistoryIDKey;
extern NSString * const FASAPIHistoryRateKey;
extern NSString * const FASAPIHistoryStartDateKey;
extern NSString * const FASAPIHistoryEndDateKey;
extern NSString * const FASAPIHistoryCompletedKey;
extern NSString * const FASAPIHistoryPauseDurationKey;

@interface FASWorkoutSession (API)
// Return API-readable dict
-(NSDictionary *)apiParams;
@end

@interface FASWorkoutSession (Format)
- (NSString *)formattedUserRate;
- (NSString *)formattedDuration;
@end
