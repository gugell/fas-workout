//
//  FASWorkoutViewController.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/6/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

private var avPlayerItemObservationContext = 0

// MARK: - protocols
enum FASWorkoutViewControllerStatus {
    case WorkoutCompleted, WorkoutAborted
}
protocol FASWorkoutViewControllerDelegate {
    func workoutViewController(controller: FASWorkoutViewController, didFinishWithStatus status: FASWorkoutViewControllerStatus)
}

enum ContainerSegueIdentifier: String {
    case Player = "player"
    case ExercisesTable = "exercisesTable"
}

struct FASWorkoutViewControllerState {
    var shouldRestoreState: Bool = false
    var shouldShowRestView: Bool = false
    var shouldShowStartView: Bool = false
    var shouldShowFinishView: Bool = false
    var shouldShowPauseView: Bool = false
}

struct WorkoutProgress {
    var exerciseIndex : Int = 0
    var exerciseCount : Int = 0
    static var currentIndex = -1;
}

final class FASWorkoutViewController: FASViewController {
    var delegate: FASWorkoutViewControllerDelegate?
    
    // UI properties
    @IBOutlet weak var workView: FASWorkView!
    @IBOutlet var restView: FASRestView!
    @IBOutlet var pauseView: FASPauseView!
    @IBOutlet var finishView: FASFinishView!
    
    @IBOutlet var workoutProgressView : FASWorkoutProgressView!
    
    // New UI elements for iPad layout
    @IBOutlet var workoutNameLabel: UILabel!
    
    private var exercisesTableViewController: FASWorkoutExercisesViewController?
    
    // HACK: restView will be re-assigned to this
    private var startView: FASRestView?
    
    private var continuousWorkoutTimer : FASTimerController!
    
    private var isRestViewShown: Bool = false
    private var isPauseViewShown: Bool = false
    private var isFinishViewShown: Bool = false
    private var isStartViewShown: Bool = false
    private var isWorkViewShown: Bool {
        // HACK: determined by delegate assigned to it
        return workView?.delegate != nil ? true : false
    }
    // fullscreen views obscure pushed vc's view, so we need
    // to appropriatelly show/hide them during vc push/pop flows
    private var viewControllerState: FASWorkoutViewControllerState
    
    // Playback related
    private var avPlayerViewController: AVPlayerViewController?
    private var avPlayer: AVPlayer?
    private dynamic var avPlayerItem: AVPlayerItem? {
        didSet {
            if hasAVPlayerItemObserver && oldValue != nil {
                assert(NSThread.mainThread() === NSThread.currentThread())
                // remove observer from "old" item
                oldValue!.removeObserver(self, forKeyPath: "status", context: &avPlayerItemObservationContext)
                hasAVPlayerItemObserver = false
            }
            
            if avPlayerItem != nil {
                assert(NSThread.mainThread() === NSThread.currentThread())
                // add observer to "new" item
                avPlayerItem!.addObserver(self, forKeyPath: "status", options: .New, context: &avPlayerItemObservationContext)
                hasAVPlayerItemObserver = true
            }
        }
    }
    private var hasAVPlayerItemObserver = false
    
    // data / controller properties
    var workout: Workout? {
        didSet {
            totalLaps = ( workout != nil ? self.workout!.lapsNumber.integerValue : 0)
            prepareExercisesWithCompletion {
                (exercises: [Exercise]) in
                self.workoutController = FASWorkoutController(exercises: exercises)
            }
        }
    }
    
    /*private*/ var workoutController: FASWorkoutController? {
        willSet {
            self.workoutController?.delegate = nil
            self.workoutController?.UIDelegate = nil
        }
        didSet {
            workoutController?.delegate = self
            self.workoutController?.UIDelegate = self
        }
    }
    
    private var lapsInfo: [FASWorkoutLap] = []
    private var totalLaps: Int = 0
    var currentLap: Int = 0
    
    //    var workoutProgress : WorkoutProgress {
    //        didSet {
    //
    //        }
    //    }
    
    // sound
    var soundNextExercise: Sound? = Sound.fasSound(.NextExercise)
    var soundBeginWorkout: Sound? = Sound.fasSound(.BeginWorkout)
    // general sound settings, set to false when the sound should not be played
    let playSoundBeforeWorkout: Bool = true
    let playSoundBeforeExercise: Bool = true
    
    let actionController = FASActionController()
    
    // MARK: - Init
    
    required init?(coder aDecoder: NSCoder) {
        viewControllerState = FASWorkoutViewControllerState()
        
        super.init(coder: aDecoder)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "applicationWillResignActive:", name: UIApplicationWillResignActiveNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "didUpdateBestResult:", name: FASDataBestResultsDidUpdateNotification, object: nil)
        
        self.hideBackButtonTitle = true
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIApplicationWillResignActiveNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: FASDataBestResultsDidUpdateNotification, object: nil)
    }
    
    // MARK: - KVO
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if context == &avPlayerItemObservationContext {
            if let _ = change?[NSKeyValueChangeNewKey], player = avPlayer {
                if player.currentItem === object && keyPath == "status" {
                    switch player.status {
                    case .Failed:
                        fallthrough
                    case .Unknown:
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            let message: String
                            if player.status == .Failed {
                                message = (player.currentItem!.error?.localizedDescription ?? "")
                            }
                            else {
                                message = "Unknown"
                            }
                            
                            self.presentAlertWithTitle("Player Error", message:message)
                        })
                    case .ReadyToPlay:
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            //@note possibly a workaround only
                            if UIApplication.sharedApplication().applicationState == .Active {
                                self.playVideo()
                            }
                        })
                    }
                }
            }
        } else {
            super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)
        }
    }
    
    // MARK: - ViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        beginWorkoutController()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar(.DarkWithYellowTitle)
        restoreViewControllerState()
        
        
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        disableScreenDimming()
    }
    
    override func viewDidAppearForTheFirstTime() {
        super.viewDidAppearForTheFirstTime()
        // Jump to progress configuration immediately
        if workoutController != nil {
            self.workoutProgressView.setProgress(0, totalExerciseCount: workoutController!.exerciseCount)
        }
    }
    
    override func viewDidDisappear(animated: Bool) {
        enableScreenDimming()
        // Pause playback
        pauseVideo()
        super.viewDidDisappear(animated)
    }
    
    // MARK: - Views
    
    // MARK: - Start View
    private func showStartView() {
        isStartViewShown = true
        // HACK: use RestView
        showRestView()
        startView = restView
    }
    
    private func hideStartView() {
        hideRestView()
        startView = nil // clear reference
        restView = nil // remove from memory
        isStartViewShown = false
    }
    
    private func configureStartViewWithExercise(exercise: Exercise) {
        restView.style = .BeforeWorkout
        restView.closeButton.hidden = false
        restView.nextExercise = exercise
    }
    
    // MARK: - Work View
    private func showWorkView() {
        workView.delegate = self
    }
    
    private func hideWorkView() {
        workView.delegate = nil
    }
    
    private func configureWorkViewWithExercise(exercise: Exercise?, duration: NSTimeInterval) {
        if let ex = exercise {
            workView.ringProgressView.setTotalTime(duration)
            
            // Added for iPad UI support
            if FASData.sharedData().isIPAD {
                self.exercisesTableViewController?.updateTableView()
            }
            
            let isCountableWorkout = !shouldHideBestResultControlsForWorkout()
            if let isCountableExercise = exercise?.isCountable.boolValue
                where (isCountableExercise && isCountableWorkout) {
                workView.bestResultView.hidden = false
                workView.bestResult = bestRepeatCountForExercise(ex)
            }
            else {
                workView.bestResultView.hidden = true
                workView.bestResult = nil
            }
            
            self.configurePlayerViewController(ex.preferredAttachment.filepath)
        }
    }
    
    var showLastRestView = false;
    // MARK: - Rest View
    private func showRestView() {
        
        if (!isRestViewShown) {
            if let _ = restView { }
            else {
                NSBundle.mainBundle().loadNibNamed("FASRestView", owner: self, options: nil)
                restView!.translatesAutoresizingMaskIntoConstraints = false
            }
            
            restView!.delegate = self
            
            if let nanCtrl = navigationController {
                nanCtrl.view.addSubview(restView!)
            }
            else {
                view.addSubview(restView!)
            }
            isRestViewShown = true
            view.setNeedsUpdateConstraints()
            // Pass workout progress
            if let wrkCtrl = self.workoutController {
                if WorkoutProgress.currentIndex < 0 {
                    WorkoutProgress.currentIndex = 0;
                }else{
                    WorkoutProgress.currentIndex  = wrkCtrl.exerciseIndex+1;
                }
                
                restView.setProgress(WorkoutProgress.currentIndex, totalExerciseCount: wrkCtrl.exerciseCount)
                
                
                if WorkoutProgress.currentIndex == wrkCtrl.exerciseCount {
                    
                    if (self.currentLap == self.totalLaps){
                        showLastRestView = true
                        if let sound = self.soundNextExercise{
                            sound.stop()
                            continuousWorkoutTimer.stop()
                        }
                        
                        
                    }
                    
                }
            }
        }
        
        
        
        
    }
    
    private func hideRestView() {
        if (isRestViewShown) {
            restView.removeFromSuperview()
            isRestViewShown = false
            view.setNeedsUpdateConstraints()
            
            restView!.delegate = nil
        }
    }
    
    private func configureRestViewWithExercise(exercise: Exercise, nextExercise: Exercise?, lapInfo: (Int,Int) ) {
        restView.repeatCounterView.counter.value = 0
        restView.lapInfo = lapInfo
        
        if self.shouldHideBestResultControlsForWorkout() {
            // Tabata training type should not have controls at all
            // in this way restView style is similar to "static" exercise's style
            restView.style = .AfterStaticExercise
        }
        else if let isStatic = exercise.isStatic where isStatic.boolValue == true {
            restView.style = .AfterStaticExercise
        }
        else {
            restView.style = .AfterDinamicExercise
        }
        
        // next exercise preview
        restView.nextExercise = nextExercise
    }
    
    // MARK: - Pause View
    private func showPauseView() {
        if (!isPauseViewShown) {
            if let _ = pauseView { }
            else {
                NSBundle.mainBundle().loadNibNamed("FASPauseView", owner: self, options: nil)
                pauseView.translatesAutoresizingMaskIntoConstraints = false
            }
            
            pauseView.delegate = self
            
            if let nanCtrl = navigationController {
                nanCtrl.view.addSubview(pauseView)
            }
            else {
                view.addSubview(pauseView)
            }
            isPauseViewShown = true
            view.setNeedsUpdateConstraints()
            
            self.configurePauseView()
        }
    }
    
    private func hidePauseView() {
        if (isPauseViewShown) {
            pauseView.removeFromSuperview()
            isPauseViewShown = false
            view.setNeedsUpdateConstraints()
            
            pauseView.delegate = nil
        }
    }
    
    private func configurePauseView() {
        pauseView.lapsInfo = (self.currentLap, self.totalLaps)
        if let wrkCtrl = self.workoutController {
            pauseView.setProgress(wrkCtrl.exerciseIndex, totalExerciseCount: wrkCtrl.exerciseCount)
            // New - pass next exercise to 'pause' screen
            pauseView.nextExercise = wrkCtrl.nextExercise
            
        }
    }
    
    // MARK: - Finish View
    private func showFinishView() {
        if (!isFinishViewShown) {
            
           print("show finish")
            
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                 self.workoutController?.endWorkout();
                 self.endWorkout();
                self.pauseVideo()
                 self.continuousWorkoutTimer.stop()
            }
            
          

            
            
            if let _ = finishView { }
            else {
                NSBundle.mainBundle().loadNibNamed("FASFinishView", owner: self, options: nil)
                finishView!.translatesAutoresizingMaskIntoConstraints = false
            }
            
            finishView!.delegate = self
            
            if let nanCtrl = navigationController {
                nanCtrl.view.addSubview(finishView!)
            }
            else {
                view.addSubview(finishView!)
            }
            isFinishViewShown = true
            
            WorkoutProgress.currentIndex = -1;
            view.setNeedsUpdateConstraints()
            
            
            
            
        }
    }
    
    private func hideFinishView() {
        if (isFinishViewShown) {
            finishView.removeFromSuperview()
            isFinishViewShown = false
            view.setNeedsUpdateConstraints()
            
            finishView.delegate = nil
        }
    }
    
    // MARK: - Layout
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        if (isPauseViewShown) {
            updateEdgeToEdgeConstraints(self.pauseView, addedToNavigationView: true)
        }
        
        if (isFinishViewShown) {
            updateEdgeToEdgeConstraints(self.finishView, addedToNavigationView: true)
        }
        
        if (isRestViewShown) {
            updateEdgeToEdgeConstraints(self.restView, addedToNavigationView: true)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // bring Pause view on top
        if isPauseViewShown {
            view.bringSubviewToFront(pauseView)
        }
    }
    
    // MARK: - Workout Data Processing
    
    private func prepareExercisesWithCompletion(completionBlock: (exercises: [Exercise]) -> Void) {
        
        if let workout = self.workout {
            let exercises = workout.exercises
            // sort
            let sortDescriptor = UBINullableSortDescriptor(key: "sortIndex", ascending: true)
            let sortDescriptors = [sortDescriptor]
            let sortedExercises = exercises.sortedArrayUsingDescriptors(sortDescriptors)
            
            var workoutExercises: [Exercise] = []
            
            // get plain exercises list AND collect laps info
            var totalIndex: Int = 0
            if (sortedExercises.count > 0) {
                for lap in 1...totalLaps {
                    let lapStartIndex = totalIndex
                    
                    for var index = 0; index < sortedExercises.count; index++ {
                        let ex: Exercise = sortedExercises[index] as! Exercise
                        // do not include if laps
                        if (ex.lapsNumber.integerValue >= lap) {
                            workoutExercises.append(ex)
                            ++totalIndex
                        }
                    }
                    
                    let lapEndIndex = totalIndex
                    let lapInfo = FASWorkoutLap(lapNumber: lap, lapStartIndex: lapStartIndex, lapEndIndex: lapEndIndex)
                    self.lapsInfo.append(lapInfo)
                }
            }
            completionBlock(exercises: workoutExercises)
        }
    }
    
    private func lapForIndex(index: Int) -> FASWorkoutLap? {
        var theLap: FASWorkoutLap?
        
        for aLap in self.lapsInfo {
            if aLap.isLapIndex(index) {
                theLap = aLap
                break
            }
        }
        
        return theLap
    }
    
    private func fractionOfTime(time: NSTimeInterval, withTotalTime totalTime: NSTimeInterval) -> Float
    {
        var fraction: Float = 0.0
        if totalTime > 0.0 {
            fraction = 1.0 - Float(time) / Float(totalTime)
        }
        
        if fraction > 1.0 {
            fraction = 1.0
        }
        
        return fraction
    }
    
    private func shouldHideBestResultControlsForWorkout() -> Bool {
        if let tt = workout?.trainingType where tt == FASWorkoutTrainingTypeTabata {
            return true
        }
        else {
            return false
        }
    }
    
    // MARK: - Views Restoration
    
    private func restoreViewControllerState() {
        if viewControllerState.shouldRestoreState {
            if viewControllerState.shouldShowFinishView { showFinishView() }
            if viewControllerState.shouldShowRestView { showRestView() }
            if viewControllerState.shouldShowStartView { showStartView() }
            // pause is always on top of any other view
            if viewControllerState.shouldShowPauseView { showPauseView() }
            // prevent restore after restore
            viewControllerState.shouldRestoreState = false
        }
    }
    
    private func saveViewControllerState() {
        viewControllerState.shouldRestoreState = true
        viewControllerState.shouldShowFinishView = isFinishViewShown
        viewControllerState.shouldShowPauseView = isPauseViewShown
        viewControllerState.shouldShowRestView = isRestViewShown
        viewControllerState.shouldShowStartView = isStartViewShown
    }
    
    // MARK: - Best Result
    
    private func loadBestExerciseResultsForWorkout(workout: Workout) {
        FASData.sharedData().getBestExercisesResultsForWorkout(workout)
    }
    
    func didUpdateBestResult(notification: NSNotification) {
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            // here, we could refresh the current work view, if it is currently shown
            // Example: at the .Work phase there was an attempt to get best result,
            // but it failed due to delay in receiving it, (or something else)
            // now we can get it the last chance to update data
            if self.isWorkViewShown == true, let ex = self.workoutController?.exercise {
                self.workView.bestResult = self.bestRepeatCountForExercise(ex)
            }
        }
    }
    
    // TODO: consider to move it to FASData
    // Note: when this function returns nil, that means there was no value found
    private func bestRepeatCountForExercise(exercise: Exercise) -> UInt? {
        if let bestResults = FASData.sharedData().bestResults {
            for resultResult in bestResults {
                if let exerciseID = resultResult["exerciseID"] as? NSNumber where exerciseID == exercise.exercise_id {
                    return resultResult["maxRepeat"] as? UInt
                }
            }
        }
        return nil
    }
    
    // MARK: - Controls
    var timer: FASTimerController?
    private func prepareUserForWorkout(workout: Workout, completionBlock: () -> Void) {
        showStartView()
        
        if let wrkCtrl = self.workoutController, ex = wrkCtrl.exercise {
            configureStartViewWithExercise(ex)
        }
        
        // to sync with sound file take its duration as a "get ready" time
        // IF no sound should be played, or no Sound is created,
        // THEN set default to 3 sec
        let getReadyForWorkoutTime: NSTimeInterval
        if let soundDuration = soundBeginWorkout?.duration where playSoundBeforeWorkout {
            getReadyForWorkoutTime = soundDuration
        }
        else {
            getReadyForWorkoutTime = 3.0
        }
        
        timer = FASTimerController(type: FASTimerControllerType.CountdownTimer, userTime: getReadyForWorkoutTime, mode: FASTimerControllerMode.Default)
        timer!.delegate = self
        timer!.startWithEndingBlock { (time) -> Void in
            self.hideStartView()
            completionBlock()
        }
    }
    
    private func beginWorkoutController() {
        if self.workout != nil {
            continuousWorkoutTimer = FASTimerController(type: FASTimerControllerType.CountdownTimer, userTime: self.workout!.timeSpan.doubleValue, mode: FASTimerControllerMode.Default)
            continuousWorkoutTimer.delegate = self
            continuousWorkoutTimer.start()
        }
        
        // Layout for iPad
        if FASData.sharedData().isIPAD {
            // Configure name label
            self.workoutNameLabel.font = UIFont.boldBabasFontOfSize(32.0)
            self.workoutNameLabel.text = self.workout?.name
            // Configure table view
            //            self.exerciseTableView.dataSource = self
            //            self.exerciseTableView.delegate = self
            //            self.exerciseTableView.reloadData()
        }
        
        if FASData.sharedData().hasWorkoutSession() {
            printlnDebugWarning("unfinished workout session detected - will be deleted")
            FASData.sharedData().clearCurrentWorkoutSession()
        }
        
        prepareUserForWorkout(self.workout!, completionBlock: { () -> Void in
            self.workoutController?.beginWokrout()
        })
        
        loadBestExerciseResultsForWorkout(self.workout!)
    }
    var firstTime:Bool = true
    private func pauseWorkoutController() {
        self.workoutController?.pauseWorkout()
        pauseVideo()
        
        //pause timer when stop
        
        if firstTime {
            continuousWorkoutTimer.start()
        }
        firstTime = false
        continuousWorkoutTimer.pause()
    }
    
    private func resumeWorkoutController() {
        if self.workoutController?.phase == .Rest && self.workoutController?.status == .Paused {
            self.workoutController?.resumeWorkout()
            return
        }
        self.workoutController?.resumeWorkout()
        playVideo()
        
        
        //continue
        
        
        continuousWorkoutTimer.start()
    }
    
    private func abortWorkoutController() {
        self.workoutController?.abortWorkout()
        pauseVideo()
    }
    
    private func openExerciseLibrary() {
        // save view controller state BEFORE we hide views
        saveViewControllerState()
        
        // hide view that could obscure exercise library view
        self.hidePauseView()
        self.hideRestView()
        
        if let exerciseInfoVC = self.mainStoryboard.instantiateViewControllerWithIdentifier("FASExerciseInfoViewController") as? FASExerciseInfoViewController {
            exerciseInfoVC.exercise = self.workoutController?.exercise
            self.navigationController?.pushViewController(exerciseInfoVC, animated: true)
        }
    }
    
    private func dismiss(status: FASWorkoutViewControllerStatus) {
        if let d = delegate {
            d.workoutViewController(self, didFinishWithStatus: status)
        } else {
            self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    // pause upon receiving calls, etc..
    func applicationWillResignActive(notification: NSNotification) {
        pauseWorkoutController()
    }
    
    // MARK: - Playback controls
    
    private func pauseVideo() {
        avPlayer?.pause()
    }
    
    private func playVideo() {
        if let player = avPlayer, let item = player.currentItem where item.status == .ReadyToPlay {
            player.play()
        }
        else {
            printlnDebugWarning("cannot play() with current player")
        }
    }
    
    ///////////////////////////////////////////////
    // MARK: Video
    ///////////////////////////////////////////////
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let segueIdentifier = segue.identifier {
            if let doExerciseViewSegueIdentifier = ContainerSegueIdentifier(rawValue: segueIdentifier) {
                switch doExerciseViewSegueIdentifier {
                case .Player:
                    if let playerVC = segue.destinationViewController as? AVPlayerViewController {
                        avPlayerViewController = playerVC
                        avPlayerViewController!.showsPlaybackControls = false
                        if #available(iOS 9.0, *) {
                            avPlayerViewController!.allowsPictureInPicturePlayback = false
                        } else {
                            // Fallback on earlier versions
                        }
                    }
                case .ExercisesTable:
                    if let exercisesVC = segue.destinationViewController as? FASWorkoutExercisesViewController {
                        exercisesTableViewController = exercisesVC
                        exercisesTableViewController!.workoutHubViewController = self
                    }
                }
            }
        }
    }
    
    private func configurePlayerViewController(filepath: NSURL?) {
        
        if avPlayerViewController == nil {
            printlnDebugError("player VC is not set")
            return
        }
        
        if let assetURL = filepath {
            let urlAsset = AVURLAsset(URL: assetURL, options: nil)
            let tracksKey = "tracks"
            urlAsset.loadValuesAsynchronouslyForKeys([tracksKey], completionHandler: { () -> Void in
                // TODO: add weak self
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    var error: NSError?
                    let status = urlAsset.statusOfValueForKey(tracksKey, error: &error)
                    if status == .Loaded {
                        self.avPlayerItem = AVPlayerItem(asset: urlAsset)
                        
                        if let _ = self.avPlayer {
                            self.avPlayer!.replaceCurrentItemWithPlayerItem(self.avPlayerItem)
                        }
                        else {
                            let player = AVPlayer(playerItem: self.avPlayerItem!)
                            self.avPlayerViewController!.player = player
                            self.avPlayer = player
                        }
                    }
                    else {
                        printlnDebugError("error while loading \"\(tracksKey)\" key. Error was: \(error?.localizedDescription)")
                        
                        #if DEBUG_ERRORS
                            self.presentAlertWithTitle("Asset Error", message: "error while loading \"\(tracksKey)\" key. Error was: \(error?.localizedDescription)")
                        #endif
                    }
                })
            })
        }
    }
    
    ///////////////////////////////////////////////
    // MARK: Session Share
    ///////////////////////////////////////////////
    
    private func shareCurrentWorkoutSessionAndDismiss(shouldDismiss: Bool = true) {
        actionController.viewController = self;
        actionController.fbShareWorkoutSession(FASData.sharedData().currentWorkoutSession, completion: { [weak self] (error) -> Void in
            if shouldDismiss {
                self?.dismiss(FASWorkoutViewControllerStatus.WorkoutCompleted)
            }
            })
    }
}

// MARK: - FASWorkoutControllerDelegate

extension FASWorkoutViewController: FASWorkoutControllerDelegate {
    
    func workoutController(_: FASWorkoutController, shouldBeginPhase phase: FASExercisePhase, forExercise exercise: Exercise, withIndex idx: Int) -> Bool
    {
        // a place where we can pause workout after specifyc exercise is finished
        // func shouldPauseAfterExercise(exercise: Exercise, atIndex idx: Int) -> Bool
        return true
    }
    
    func workoutController(_: FASWorkoutController, willBeginPhase phase: FASExercisePhase, forExercise exercise: Exercise, withIndex idx: Int) {
        
        
    }
    
    func workoutController(controller: FASWorkoutController, didBeginPhase phase: FASExercisePhase, forExercise exercise: Exercise, withIndex idx: Int) {
        printlnDebugTrace("\nBEGIN <\(phase)>;\tname=\(exercise.name);\tlap=\(lapForIndex(idx)!.lapNumber)\ttime=\(controller.phaseDuration)")
        
        let lapNumber: Int = (self.lapForIndex(idx) != nil) ? self.lapForIndex(idx)!.lapNumber : 0
        self.currentLap = lapNumber
        
        switch phase {
        case .Work:
            // Configure UI
            self.title = exercise.name
            self.showWorkView()
            self.configureWorkViewWithExercise(exercise, duration: controller.phaseDuration)
            
            // Record History - Exercise Begins
            let event = ExerciseProgressEvent(type: ExerciseProgressEventType.Begin, value: nil)
            FASData.sharedData().recordExerciseProgress(exercise, circle: self.currentLap, info: event)
        case .Rest:
            // Configure UI
            if (workout?.trainingType == FASWorkoutTrainingTypeTabata || controller.exercise?.isStatic.boolValue == true) && controller.nextExercise == nil  {
                
                print("show final view tabata")
                workoutController?.endWorkout();
                endWorkout();
                
            } else {
                self.showRestView()
                self.configureRestViewWithExercise(exercise, nextExercise: controller.nextExercise, lapInfo: (self.currentLap,self.totalLaps) )
            }
        }
    }


func endWorkout(){
 
    timer!.stop()
    
    soundBeginWorkout?.stop()
    SoundManager.sharedManager().stopMusic()
    SoundManager.sharedManager().stopAllSounds()
    soundNextExercise?.stop()
    
    soundBeginWorkout = nil
    soundNextExercise = nil
}
func workoutController(_: FASWorkoutController, willEndPhase phase: FASExercisePhase, forExercise exercise: Exercise, withIndex idx: Int) {
}

func workoutController(controller: FASWorkoutController, didEndPhase phase: FASExercisePhase, forExercise exercise: Exercise, withIndex idx: Int) {
    printlnDebugTrace("\nEND <\(phase)>")
    
    if phase == .Work {
        // Pause video playback
        pauseVideo()
        
        self.hideWorkView()
        
        // Record History - Exercise Ends
        let event = ExerciseProgressEvent(type: ExerciseProgressEventType.End, value: nil)
        let circleNumber: Int = (self.lapForIndex(idx) != nil) ? self.lapForIndex(idx)!.lapNumber : 0
        FASData.sharedData().recordExerciseProgress(exercise, circle: circleNumber, info: event)
    }
    
    if phase == .Rest {
        var userInputRepeatCount : UInt = 0
        if restView != nil {
            userInputRepeatCount = restView.repeatCounterView.counter.value
        }
        // direct get!
        self.hideRestView()
        // Record History - Exercise Repeat Count
        if exercise.isCountable.boolValue {
            let event = ExerciseProgressEvent(type: ExerciseProgressEventType.RepeatCount, value: NSNumber(unsignedLong: userInputRepeatCount))
            let circleNumber: Int = (self.lapForIndex(idx) != nil) ? self.lapForIndex(idx)!.lapNumber : 0
            FASData.sharedData().recordExerciseProgress(exercise, circle: circleNumber, info: event)
        }
    }
    
}

func workoutControllerDidChangeStatus(controller: FASWorkoutController) {
    if controller === self.workoutController {
        switch controller.status {
        case .PreFinished: break
        case .Idle:
            printlnDebugTrace("WORKOUT IDLE")
            
        case .InProgress:
            printlnDebugTrace("WORKOUT IN PROGRESS")
            
            if FASData.sharedData().hasWorkoutSession()  {
                // Record History - Workout Resumed After Being Paused
                FASData.sharedData().pauseCurrentWorkoutSession(false)
                self.hidePauseView() // hide, in case it is shown
            }
            else {
                // Record History - Workout Begins
                FASData.sharedData().beginWorkoutSessionWithWorkout(self.workout!)
                
                // Track
                FASAnalyticsEventHelper.trackWorkoutDidStartWithName(self.workout!.name)
            }
            
        case .Paused:
            printlnDebugTrace("WORKOUT PAUSED")
            FASData.sharedData().pauseCurrentWorkoutSession(true)
            self.showPauseView()
            
        case .Aborted:
            printlnDebugTrace("WORKOUT ABORTED")
            // Record History - Workout Aborts
            // Note: submit immediately as this does not have to be rated
            FASData.sharedData().endCurrentWorkoutSession(false, submit: true)
            
            self.dismiss(FASWorkoutViewControllerStatus.WorkoutAborted)
            
            // Track
            FASAnalyticsEventHelper.trackWorkoutDidAbortWithName(self.workout!.name)
            
        case .Finished:
            
            printlnDebugTrace("WORKOUT FINISHED")
            //self.showRestView();
            self.showFinishView()
            // Record History - Workout Ends
            FASData.sharedData().endCurrentWorkoutSession(true)
            // Track
            FASAnalyticsEventHelper.trackWorkoutDidFinishWithName(self.workout!.name)
            // Apptentive
            DUBApptentiveDeferredEngagement.engage(kFASApptentiveEventDidFinishWorkout, fromViewController: self)
        }
    }
}
}

// MARK: - FASWorkoutControllerUIDelegate

extension FASWorkoutViewController: FASWorkoutControllerUIDelegate {
    
    func workoutController(controller: FASWorkoutController, timerDidCountToDate date: NSDate) {
        
        switch (controller.phase) {
        case .Work:
            workView?.time = date.toStringTimeMinutesAndSeconds()
        case .Rest:
            restView?.time = date.toStringTimeMinutesAndSeconds()
        }
    }
    
    func workoutController(controller: FASWorkoutController, timerDidCountToTime time: NSTimeInterval, totalTime: NSTimeInterval) {
        
        switch (controller.phase) {
            
        case .Work:
            workView.ringProgressView.setCurrrentTime( (totalTime - time), animated: false)
        case .Rest:
            
            
            if !showLastRestView {
                
                if let sound = self.soundNextExercise where playSoundBeforeExercise && sound.duration >= time {
                 
                        sound.play()
                    
                }
                restView?.timeProgress = fractionOfTime(time, withTotalTime: totalTime)
            } else {
                continuousWorkoutTimer.stop()
            }
        }
        
        
    }
    
    func workoutController(controller: FASWorkoutController, didUpdateProgress progress: Int, totalExerciseCount count: Int) {
        // Increment - already doing exercise
        workoutProgressView.setProgress(progress, totalExerciseCount: count)
    }
}

// MARK: - Views' Delegates

extension FASWorkoutViewController: FASFinishViewDelegate {
    func finishViewDidClose(finishView: FASFinishView, withOption option: FASFinishViewCloseOption) {
        let userWorkoutRate = finishView.difficultyRate
        FASData.sharedData().rateCurrentWorkoutSessionWithRate(userWorkoutRate)
        
        FASData.sharedData().submitCurrentWorkoutSession()
        
        if (option == .Share) {
            shareCurrentWorkoutSessionAndDismiss()
        }
        else {
            self.dismiss(FASWorkoutViewControllerStatus.WorkoutCompleted)
        }
    }
}

extension FASWorkoutViewController: FASWorkViewDelegate {
    func workView(workView: FASWorkView, didSelectAction action: FASWorkViewAction) {
        self.pauseWorkoutController()
    }
    
    
    
    func skipExercise() {
        
        workoutController?.skipExercise()
        
        // =10 because 10 second at start
        var timeElapsed = 10
        
        let total = workoutController!.exercises.count - 1;
        
        for ex in 0...total {
            
            if ex <= workoutController!.exerciseIndex-1 {
                
                
                let overall = Int(workoutController!.exercises[ex].timeSpan.intValue);
                
                var rest = Int(workoutController!.exercises[ex].restTime.intValue)
                
                
                timeElapsed  += overall;
                
                
                if ex == total - 1 {
                    let type = workout?.trainingType;
                    if type == FASWorkoutTrainingTypeTabata {
                        rest += 10;
                    }
                    
                }
                
                timeElapsed +=  rest;
            }
            
            
            
            
        }
        
        continuousWorkoutTimer.skip(Double(timeElapsed))
    }
}

extension FASWorkoutViewController: FASRestViewDelegate {
    func restViewDidPressFinish() {
        
        self.workoutController?.endWorkout()
        
    }
    
    func restViewDidPressCloseButton(restView: FASRestView)
    {
        self.abortWorkoutController()
        self.dismissViewControllerAnimated(true, completion: nil)
        
        WorkoutProgress.currentIndex = -1
        
        self.workoutProgressView.setProgress(0, totalExerciseCount: workoutController!.exerciseCount)
        
        enableScreenDimming()
        // Pause playback
        pauseVideo()
        
        timer!.stop()
        
        soundBeginWorkout?.stop()
        SoundManager.sharedManager().stopMusic()
        SoundManager.sharedManager().stopAllSounds()
        
        soundBeginWorkout?.stop()
        
        
        //        if workoutController!.exerciseIndex == 0 {
        //            self.dismissViewControllerAnimated(true, completion: nil)
        //
        //            WorkoutProgress.currentIndex = -1
        //
        //            self.workoutProgressView.setProgress(0, totalExerciseCount: workoutController!.exerciseCount)
        //
        //            enableScreenDimming()
        //            // Pause playback
        //            pauseVideo()
        //
        //            timer!.stop()
        //
        //            soundBeginWorkout?.stop()
        //            SoundManager.sharedManager().stopMusic()
        //            SoundManager.sharedManager().stopAllSounds()
        //
        //            soundBeginWorkout?.stop()
        //
        //        }
        //        else {
        //            print("pause")
        //            if self.restView === restView {
        //                self.pauseWorkoutController()
        //            }
        //        }
    }
}

extension FASWorkoutViewController: FASPauseViewDelegate {
    func pauseView(pauseView: FASPauseView, didSelectAction action: FASPauseViewAction)
    {
        switch action {
        case .ExitWorkout:
            self.abortWorkoutController()
        case .ExerciseLibrary:
            openExerciseLibrary()
        case .ResumeWorkout:
            self.resumeWorkoutController()
        }
    }
}

// MARK: - FASTimerControllerDelegate

extension FASWorkoutViewController: FASTimerControllerDelegate {
    func timerController(timerController: FASTimerController!, didCountToDate date: NSDate!) {
        if timerController == continuousWorkoutTimer {
            let time = date.toStringTimeMinutesAndSeconds()
            self.workoutProgressView.updateWithReadableTime(time)
            if self.restView != nil {
                
                self.restView.workoutProgressView.updateWithReadableTime(time)
            }
            if self.pauseView != nil {
                self.pauseView.workoutProgressView.updateWithReadableTime(time)
            }
        } else {
            startView?.time = date.toStringTimeMinutesAndSeconds()
        }
    }
    
    func timerController(timerController: FASTimerController!, didCountToTime time: NSTimeInterval) {
        if timerController == continuousWorkoutTimer {
        } else {
            if isStartViewShown && playSoundBeforeWorkout {
                // start playing after the first call
                
                soundBeginWorkout?.play()
            }
            
            startView?.timeProgress = fractionOfTime(time, withTotalTime: timerController.userTime)
        }
    }
    
    func timerController(timerController: FASTimerController!, didFinishCountdownWithTime time: NSTimeInterval) {
        // for startView we use block to callback when the time elapsed, so no actions here
        if timerController == continuousWorkoutTimer {
            
            self.workoutController?.endWorkout()
        }
    }
}

// MARK: - Lap Data

struct FASWorkoutLap: CustomStringConvertible, CustomDebugStringConvertible {
    var lapNumber = 0
    // TODO: replace with custom range struct
    // TODO: add lapStartIndex <= lapEndIndex check upon init
    var lapStartIndex = 0
    var lapEndIndex = 0
    
    var description: String {
        return "\(lapNumber)"
    }
    
    var debugDescription: String {
        return  "\nlapNumber = \(lapNumber)\napStartIndex = \(lapStartIndex)\nlapEndIndex = \(lapEndIndex)"
    }
    
    func isLapIndex(index: Int) -> Bool {
        // assumed, bounds are left-inclusive [lapStartIndex, lapEndIndex)
        return (index >= self.lapStartIndex && index < self.lapEndIndex)
    }
}

// MARK: - Alerts

extension FASWorkoutViewController {
    private func presentAlertWithTitle(title: String, message: String)
    {
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction( UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
}