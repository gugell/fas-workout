//
//  FASWorkoutController.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/6/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import Foundation

// TASKS:
// 1. Go throught exercises from ordered collection (array) of exercises, taking in account the exercise work and rest durations (and other possible timings). The controller does not know about cicles, but rather take exercises array as ready-to-use sequence.

// TODO: For Swift 2.0, replaced Printable with CustomStringConvertible
enum FASWorkoutControllerExercisePhase: CustomStringConvertible {
    // TODO: .Idle state should be added to indicate "No State"
    case Work, Rest
    var description : String {
        switch self {
        case .Work:
            return "WORK"
        case .Rest:
            return "REST"
        }
    }
}
typealias FASExercisePhase = FASWorkoutControllerExercisePhase

enum FASWorkoutControllerStatus {
    case Idle, InProgress, Paused, PreFinished,Finished, Aborted
}
    
protocol FASWorkoutControllerDelegate {
    // ctrl asks delegate to begin particular phase
    func workoutController(controller: FASWorkoutController, shouldBeginPhase phase: FASExercisePhase, forExercise exercise: Exercise, withIndex idx: Int) -> Bool
    
    // notify delegate about exercise phase change
    // TODO: add phase duration to did... proto methods
    // TODO: remove will.. phase proto method, as they introduces confusion
    func workoutController(controller: FASWorkoutController, willBeginPhase phase: FASExercisePhase, forExercise exercise: Exercise, withIndex idx: Int)
    func workoutController(controller: FASWorkoutController, didBeginPhase phase: FASExercisePhase, forExercise exercise: Exercise, withIndex idx: Int)
    func workoutController(controller: FASWorkoutController, willEndPhase phase: FASExercisePhase, forExercise exercise: Exercise, withIndex idx: Int)
    func workoutController(controller: FASWorkoutController, didEndPhase phase: FASExercisePhase, forExercise exercise: Exercise, withIndex idx: Int)
    
    // notify delegate about workout state change
    func workoutControllerDidChangeStatus(controller: FASWorkoutController)
}

protocol FASWorkoutControllerUIDelegate {
    // Note: 'date' represented as NSDate instance, but in fact only time components get changed
    func workoutController(controller: FASWorkoutController, timerDidCountToDate date: NSDate)
    
    func workoutController(controller: FASWorkoutController, timerDidCountToTime time: NSTimeInterval, totalTime: NSTimeInterval)
    
    // update UI delegate with total workout progress, from 0.0...1.0
    // @note changed to pass both total count and current index
    func workoutController(controller: FASWorkoutController, didUpdateProgress progress: Int, totalExerciseCount count: Int)
}

final class FASWorkoutController {
    
    // MARK: - Data
    
    var delegate: FASWorkoutControllerDelegate?
    var UIDelegate: FASWorkoutControllerUIDelegate?
    
    //@note changed access type to not hold a copy of these for iPad table
    private(set) var exercises: [Exercise]
    
    var exercise: Exercise? {
        if exerciseCount == 0 || exerciseIndex >= exerciseCount {
            return nil
        }
        return exercises[exerciseIndex]
    }
    var nextExercise: Exercise? {
        let nextIndex = exerciseIndex + 1
        if (exerciseCount == 0 || exerciseCount <= nextIndex) {
            return nil
        }
        else {
            return exercises[nextIndex]
        }
    }
    
    var exerciseCount: Int {
        return exercises.count
    }
    
    //@note updated access level for iPad UI (accessed from FASWorkoutsViewController)
     var exerciseIndex: Int = 0 {
        didSet {
            //(self, didUpdateProgress: Float(exerciseIndex) / Float(exerciseCount) )
            UIDelegate?.workoutController(self, didUpdateProgress: exerciseIndex, totalExerciseCount: exerciseCount)
        }
    }
    
    var timer: FASTimerController?
    
    // the phase, last exercise ends up with (change, only when absolutely required)
    let lastPhase: FASWorkoutControllerExercisePhase = .Work
    
    init(exercises: [Exercise]) {
        self.exercises = exercises
    }
    
     var status: FASWorkoutControllerStatus = .Idle
    private(set) var phaseDuration: NSTimeInterval = 0.0
    private(set) var phase: FASExercisePhase = .Work
    
    // MARK: - Controls
    
    func beginWokrout() {
        if exerciseCount == 0 {
            // TODO: return an error
            printlnDebugError("no exercises")
            return
        }
        
        if status != .InProgress {
            status = .InProgress
            delegate?.workoutControllerDidChangeStatus(self)
            
            // always begin w/ current phase
            beginPhase(self.phase)
        }
    }
    
    func pauseWorkout() {
        if status == .InProgress {
            status = .Paused
            delegate?.workoutControllerDidChangeStatus(self)
            
            pauseTimer()
        }
    }
    
    func resumeWorkout() {
        if status == .Paused {
            status = .InProgress
            delegate?.workoutControllerDidChangeStatus(self)
            
            resumeTimer()
        }
    }
    
    func abortWorkout() {
        if status != .Aborted {
            status = .Aborted
            delegate?.workoutControllerDidChangeStatus(self)
            
            reset()
        }
    }
    
    // @note changes access level for continuous workout time
    func endWorkout() {
        
        if status != .Finished {
            status = .Finished
            delegate?.workoutControllerDidChangeStatus(self)
        //stopTimer();
        }
    }
    
    func skipExercise() {
        // End current work phase + skip next rest phase
        endPhase(self.phase, skip: true)
    }
    
    // MARK: - Helpers
    
    func reset() {
        stopTimer()
        self.exerciseIndex = 0
        self.phaseDuration = 0.0
        self.phase = .Work
    }
    
    // MARK: - Timer Controls
    
    private func startTimer(time: NSTimeInterval)
    {
        stopTimer()
        
        timer = FASTimerController(type: .CountdownTimer, userTime: time, mode: .HighUse)
        timer!.delegate = self
        timer!.start()
    }
    
    private func pauseTimer() {
        if let tc = timer {
            tc.pause()
        }
    }
    
    private func resumeTimer() {
        if let tc = timer {
            tc.start()
        }
    }
    
     func stopTimer() {
        if let tc = timer {
            tc.stop()
            tc.delegate = nil
        }
        
        timer = nil
    }
    
    // MARK: - Exercise phase control
    
    private func beginPhase(phase: FASExercisePhase) {
        if (self.exercise == nil) {
            
            if exerciseIndex == exerciseCount{
                endWorkout()
            }else{
                
                printlnDebugError("invalid exercise")
            }
                  return ;
        }
        
        if let shouldBegin: Bool = delegate?.workoutController(self, shouldBeginPhase: phase, forExercise: self.exercise!, withIndex: self.exerciseIndex) where shouldBegin == true {
            delegate?.workoutController(self, willBeginPhase: phase, forExercise: self.exercise!, withIndex: self.exerciseIndex)

            // get phase duration
            switch phase {
            case .Work:
#if DEBUG
                phaseDuration = 5.0
#else
                phaseDuration = self.exercise!.timeSpan.timeIntervalValue
#endif
                  phaseDuration = self.exercise!.timeSpan.timeIntervalValue
            case .Rest:
#if DEBUG
                phaseDuration = 5.0
#else
                phaseDuration = self.exercise!.restTime.timeIntervalValue
#endif
                
                  phaseDuration = self.exercise!.restTime.timeIntervalValue
            }

            // fire timer
            self.startTimer(phaseDuration)
            
            delegate?.workoutController(self, didBeginPhase: phase, forExercise: self.exercise!, withIndex: self.exerciseIndex)
        }
        else {
            pauseWorkout()
        }
    }
    
    //@note added 'skip' essentially specifies skip of 'rest' phase following the 'work' phase
    private func endPhase(phase: FASExercisePhase, skip : Bool = false) {
        if (self.exercise != nil) {
            delegate?.workoutController(self, willEndPhase: phase, forExercise: self.exercise!, withIndex: self.exerciseIndex)
        }
        var canContinue: Bool = true
        
        // cache exercise & index before the index is incremented, and current exercise's changed
        let cachedExercise = self.exercise
        let cachedIndex = self.exerciseIndex
        
        // move to next phase
        switch self.phase {
        case .Work:
            if lastPhase == .Work && self.nextExercise == nil {
                // rest phase for the current exercise
                let nextIndex = exerciseIndex + 1
                if  exerciseCount == nextIndex {
                    canContinue = true;
                     self.phase = .Rest
                }else{
                    
                    canContinue = false
                }
            }
            else {
                self.phase = .Rest
            }
        case .Rest:
            if lastPhase == .Rest && self.nextExercise == nil {
                canContinue = false
            }
            else {
                // work phase for the next exercise
                self.phase = .Work
                ++self.exerciseIndex
            }
        }
        if (cachedExercise != nil) {
             delegate?.workoutController(self, didEndPhase: phase, forExercise: cachedExercise!, withIndex: cachedIndex)
        }
       
        
        if canContinue {
            if !skip {
                self.beginPhase(self.phase)
            } else {
                // Report 'exercise ended'
                delegate?.workoutController(self, didEndPhase: .Rest, forExercise: cachedExercise!, withIndex: cachedIndex)
                // Advance to next 'work' phase for next exercise
                self.phase = .Work
                ++self.exerciseIndex
                self.beginPhase(self.phase)
            }
        }
        else {
            endWorkout()
        }
    }
}

// MARK: - FASTimerControllerDelegate

extension FASWorkoutController: FASTimerControllerDelegate {
    
    @objc func timerController(timerController: FASTimerController!, didFinishCountdownWithTime time: NSTimeInterval) {
        assert(self.timer === timerController)
        endPhase(self.phase)
    }
    
    @objc func timerController(timerController: FASTimerController!, didCountToDate date: NSDate!) {
        assert(self.timer === timerController)
        self.UIDelegate?.workoutController(self, timerDidCountToDate: date)
    }
    
    // not used
    @objc func timerController(timerController: FASTimerController!, didCountToTime time: NSTimeInterval) {
        assert(self.timer === timerController)
        self.UIDelegate?.workoutController(self, timerDidCountToTime: time, totalTime: timerController.userTime)
    }
}
