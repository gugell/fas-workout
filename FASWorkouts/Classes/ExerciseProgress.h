//
//  ExerciseProgress.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/9/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

typedef NS_ENUM(NSInteger, ExerciseProgressStatus) {
    ExerciseProgressStatusIdle,
    ExerciseProgressStatusInProgress,
    ExerciseProgressStatusFinished,
};

extern NSString * const FASAPIHistoryExerciseIDKey;
extern NSString * const FASAPIHistoryExerciseRepeatCountKey;
extern NSString * const FASAPIHistoryExerciseCycleKey;

@interface ExerciseProgress : NSManagedObject

@property (nonatomic) NSInteger circle;
@property (nonatomic, retain) NSDate * createdOn;
@property (nonatomic, retain) NSNumber * exercise_id;
@property (nonatomic, retain) NSDate * finishedAt;
@property (nonatomic) NSInteger repeatCount;
@property (nonatomic, retain) NSNumber * session_id;
@property (nonatomic, retain) NSDate * startedAt;
@property (nonatomic) NSInteger status;
@property (nonatomic, retain) NSNumber * workout_id;

-(NSDictionary *)apiParams;

@end
