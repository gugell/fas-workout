//
//  UBICustomRemoteImageView.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/12/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "UBICustomRemoteImageView.h"
#import "FASEdgeInsetsLabel.h"
#import "FASWorkouts-Swift.h"

@import QuartzCore;

@interface UBICustomRemoteImageView ()
@property (nonatomic, strong) FASEdgeInsetsLabel *titleLabel;
@property (nonatomic, strong) UIImageView *purchaseStatus;
@end

@implementation UBICustomRemoteImageView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self criv_commonInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self criv_commonInit];
    }
    return self;
}

- (void)criv_commonInit {
    self.circular = NO;
    self.borderColor = [UIColor whiteColor];
    self.borderWidth = 0.0;
    
    _title = nil;
    
    /*_titleLabel = [[FASEdgeInsetsLabel alloc] initWithFrame:self.bounds];
    _titleLabel.font = [UIFont fontWithName:@"BebasNeueBold" size:15.0];
    // localize this
    _titleLabel.text = self.title;
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.textColor = [UIColor fasYellowColor];
    _titleLabel.backgroundColor =[UIColor colorWithWhite:0 alpha:0.6];
    _titleLabel.textInsets = UIEdgeInsetsMake(2, 0, -2, 0);
    _titleLabel.hidden = YES;
    [self addSubview:_titleLabel];*/
    
    // Apply the insets…
    CGRect adjustedRect = UIEdgeInsetsInsetRect(self.bounds, UIEdgeInsetsMake(0, 0, 0, 0));
    
    _purchaseStatus = [[UIImageView alloc] initWithFrame:adjustedRect];
    _purchaseStatus.backgroundColor = [UIColor colorWithWhite:0 alpha:0.6];
    _purchaseStatus.image = [UIImage imageNamed:@"lock"];
    //_purchaseStatus.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    _purchaseStatus.contentMode = UIViewContentModeCenter;
    _purchaseStatus.hidden = YES;
    
    [self addSubview:_purchaseStatus];
    
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if ([self isCircular]) {
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = self.bounds.size.width / 2.0;
    }
    
    if (fabs(self.borderWidth) != 0) {
        self.layer.borderWidth = self.borderWidth;
        self.layer.borderColor = self.borderColor.CGColor;
    }
}

#pragma mark -
#pragma mark Custom Accessors
#pragma mark -

- (void)setBorderColor:(UIColor *)borderColor
{
    if (borderColor != _borderColor) {
        _borderColor = borderColor;
        
        [self setNeedsLayout];
    }
}

- (void)setBorderWidth:(CGFloat)borderWidth
{
    if (borderWidth != _borderWidth) {
        _borderWidth = borderWidth;
        
        [self setNeedsLayout];
    }
}

- (void)setCircular:(BOOL)circular
{
    if (circular != _circular) {
        _circular = circular;
        
        [self setNeedsLayout];
    }
}

- (void)setTitle:(NSString *)newValue {
    
    if (_title != newValue) {
        _title = newValue;

    }
    
    
    if(!_title || [_title isBlank]){
        self.purchaseStatus.hidden = true;
        self.borderColor = [UIColor fasYellowColor];
        
    }else{
        self.purchaseStatus.hidden = false;
        self.imageView.backgroundColor =[UIColor colorWithWhite:0 alpha:0.6];
        self.borderColor = [UIColor whiteColor];
    }
}

@end
