//
//  FASAPIError.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/22/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "FASAPIError.h"
#import "NSString+MyExtensions.h"
#import "FASWorkouts-Swift.h"

NSString * const FASAPIErrorDomain          = @"com.fas-sport.FASWorkouts.FASAPIError";
NSString * const FASAPIErrorErrMsgPrefix    = @"Cmd #0: ";

extern NSString* const FASAPIErrorCodeKey;
extern NSString* const FASAPIErrorValueKey;

@implementation FASAPIError

//factory methods
+ (FASAPIError *)errorWithInfo:(NSDictionary *)errorInfo
{
    return [[FASAPIError alloc] initWithInfo:errorInfo];
}

+ (FASAPIError *)errorWithCode:(FASAPIErrorCode)code
                        value:(NSString *)value
{
    return [[FASAPIError alloc] initWithCode:code
                                       value:value];
}

// initializers

- (instancetype)init
{
    return [self initWithCode:FASAPIErrorCodeUnknownError
                        value:nil];
}

- (instancetype)initWithCode:(FASAPIErrorCode)code value:(NSString *)value
{
    self = [self initWithDomain:FASAPIErrorDomain
                           code:code
                       userInfo:nil];
    if (self) {
        _value = value;
    }
    return self;
}

- (instancetype)initWithInfo:(NSDictionary *)errorInfo
{
    BOOL success = [[errorInfo objectForKey:FASAPISuccessKey] boolValue];
    FASAPIErrorCode code = FASAPIErrorCodeUnknownError;
    if (success) {
        // this case is highly unlikeable
        code = FASAPIErrorCodeNoError;
    }
    
    NSNumber *codeNumber = [errorInfo objectForKey:FASAPIErrorCodeKey];
    if (codeNumber) {
        code = [codeNumber integerValue];
    }
    
    NSString* rawValue = [errorInfo objectForKey:FASAPIErrorValueKey];
    NSString *value = [rawValue stringByRemovingFirstOccurrenceOfString:FASAPIErrorErrMsgPrefix];
    
    if (value == nil && codeNumber == nil) {
        return nil;
    } else {
        return [self initWithCode:code value:value];
    }
}

// instance methods

- (NSString *)localizedDescriptionStringKey
{
    return [FASAPIError localizedDescriptionStringKeyForCode:self.code];
}

// NSObject override
- (NSString *)description
{
    return [self localizedDescription];
}

// NSError override
- (NSString *)localizedDescription
{
    NSString *desc = NSLocalizedStringFromTable([FASAPIError localizedDescriptionStringKeyForCode:self.code],
                                                @"FASAPIError",
                                                nil);
    
    if (!desc) {
        desc = NSLocalizedStringFromTable([FASAPIError localizedDescriptionStringKeyForCode:FASAPIErrorCodeUnknownError],
                                          @"FASAPIError",
                                          nil);
    }
    
    // replace value placeholder in desc if necessary
    NSRange range = [desc rangeOfString:@"%@"];
    if (range.location != NSNotFound) {
        NSString *val = self.value;
        if (!val || [val isBlank]) {
            val = @"value";
        }
        desc = [NSString stringWithFormat:desc, val];
    }
    
#ifdef DEBUG_ERRORS
    return [NSString stringWithFormat:@"%@\n[DEBUG: (%ld) %@]", desc, (long)self.code, self.value];
#else
    return desc;
#endif
}

// class methods

// display error with AlertView
+ (void)displayAlertWithErrors:(NSArray *)errors
{
    if (errors && errors.count > 0) {
        FASAlertView *alertView = [[FASAlertView alloc] initWithTitle:nil
                                                              message:[FASAPIError alertStringForErrors:errors]
                                                             delegate:nil
                                                    cancelButtonTitle:@"Close"
                                                    otherButtonTitles:nil];
        [alertView show];
    }
}

+ (NSString *)alertStringForErrors:(NSArray *)errors
{
    return [errors componentsJoinedByString:@"\n"];
}

+ (NSString *)localizedDescriptionStringKeyForCode:(FASAPIErrorCode)code
{
    return [NSString stringWithFormat:@"FASAPIErrorCode_%lu",
            (unsigned long)code];
}

+ (BOOL)errors:(NSArray *)errors containsErrorWithCode:(FASAPIErrorCode)code
{
    return [FASAPIError indexOfErrorWithCode:code inErrors:errors] != NSNotFound;
}

+ (NSUInteger)indexOfErrorWithCode:(FASAPIErrorCode)code inErrors:(NSArray *)errors
{
    NSUInteger index = 0;
    NSUInteger indexOfError = NSNotFound;
    for (FASAPIError *error in errors) {
        if (error.code == code) {
            indexOfError = index;
            break;
        }
        index++;
    }
    return indexOfError;
}

@end
