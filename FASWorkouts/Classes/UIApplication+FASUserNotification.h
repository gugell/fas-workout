//
//  UIApplication+FASUserNotification.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/2/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIApplication (FASUserNotification)

// user-facing notification types

// @return YES, if notification settings have all types specified, No otherwise
- (BOOL)isAllowedUserFacingNotificationTypes:(UIUserNotificationType)type;
// @return YES, if notification settings have UIUserNotificationTypeNone type
- (BOOL)isAnyUserFacingNotificationTypesAllowed;
// ask user or re-register types
- (void)registerUserFacingNotificationTypes:(UIUserNotificationType)type;
@end
