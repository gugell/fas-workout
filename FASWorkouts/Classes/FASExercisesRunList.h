//
//  FASExercisesRunList.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/12/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "FASRunList.h"

// fields
extern NSString* const kFASExercisesRunListIDFieldKey;
extern NSString* const kFASExercisesRunListNameFieldKey;
extern NSString* const kFASExercisesRunListCodeFieldKey;
extern NSString* const kFASExercisesRunListMuscleTypesFieldKey;
extern NSString* const kFASExercisesRunListRestTimeFieldKey;
extern NSString* const kFASExercisesRunListTimeSpanFieldKey;
extern NSString* const kFASExercisesRunListLapsNumberFieldKey;
extern NSString* const kFASExercisesRunListWorkoutIDFieldKey;
extern NSString* const kFASExercisesRunListExerciseIDFieldKey;
extern NSString* const kFASExercisesRunListIsStaticFieldKey;
extern NSString* const kFASExercisesRunListModifyDateFieldKey;
extern NSString* const kFASExercisesRunListSortIndexFieldKey;
extern NSString* const kFASExercisesRunListPositionInSupersetFieldKey;
extern NSString* const kFASExercisesRunListDescriptionFieldKey;

@interface FASExercisesRunList : FASRunList

- (instancetype)initWithFieldList:(NSArray *)fieldList NS_DESIGNATED_INITIALIZER;

- (instancetype)initWithWorkoutID:(NSNumber *)workoutID;
+ (instancetype)exercisesRunListWithWorkoutID:(NSNumber *)workoutID;
@end
