//
//  FAQListCell.swift
//  FASWorkouts
//
//  Created by Vladimir Stepanchenko on 11/13/15.
//  Copyright © 2015 FAS. All rights reserved.
//

import UIKit

class FAQListCell: FASTableViewCell {
    
    @IBOutlet weak var contentLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        separatorType = .Default
        contentLabel.font = UIFont.boldBabasFontOfSize(25)
        contentLabel.textColor = UIColor.whiteColor()
        //@note consider custom image
        accessoryType = .DisclosureIndicator
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        contentLabelText = nil
    }
    
    var contentLabelText: String? {
        didSet {
            contentLabel.text = contentLabelText
        }
    }
}
