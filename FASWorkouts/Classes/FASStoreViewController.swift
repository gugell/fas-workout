//
//  FASStoreViewController.swift
//  FASWorkouts
//
//  Created by Oleksandr Nechet on 07.12.15.
//  Copyright © 2015 FAS. All rights reserved.
//

import UIKit
import StoreKit

class FASStoreViewController: FASViewController
{
    @IBOutlet weak var tableView: UITableView!
    
    var cacheName: String?
    lazy var fetchedStoreItemsController: NSFetchedResultsController = {
        let frc: NSFetchedResultsController = FASData.sharedData().fetchedStoreItemsControllerWithCacheName(self.cacheName)
        frc.delegate = self
        return frc
    }()
    
    ///////////////////////////////////////////////
    // MARK: - Init -
    ///////////////////////////////////////////////
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        FASData.sharedData().addProductObserver(self)
    }
    
    deinit {
        fetchedStoreItemsController.delegate = nil
        FASData.sharedData().removeProductObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // reload product identifiers, as they would be removed upon mem warn
        FASData.sharedData().updateStoreProductsInformation()
    }
    
    ///////////////////////////////////////////////
    // MARK: - Lifecycle
    ///////////////////////////////////////////////
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetch()
            configureNavigationBar(.Dark)
        
        
        let imgView = UIImageView(frame: self.view.frame);
        imgView.image = UIImage(named: "shutterstock_201512071");
   
        self.view.addSubview(imgView);
    self.view.sendSubviewToBack(imgView);
        
        
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        FASData.sharedData().updateStoreProductsInformation()
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);

        
    }
    ///////////////////////////////////////////////
    // MARK: - Controls
    ///////////////////////////////////////////////
    func fetch() {
        if let cn = cacheName {
            NSFetchedResultsController.deleteCacheWithName(cn)
        }
        
        var fetchError: NSError?
        let result: Bool
        do {
            try fetchedStoreItemsController.performFetch()
            result = true
        } catch let error as NSError {
            fetchError = error
            result = false
        }
        if !result {
            if let error = fetchError {
                // TODO: Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                printlnDebugError("Unresolved error \(error.localizedDescription),\nerror info: \(error.userInfo)")
                abort()
            }
        }
    }
}

///////////////////////////////////////////////
// MARK: - Table View
///////////////////////////////////////////////

extension FASStoreViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let product = fetchedStoreItemsController.objectAtIndexPath(indexPath) as! StoreItem
        
        if !product.isPurchased() && !product.isPurchasing() {
            FASData.sharedData().purchaseProduct(product)
        }
    }
}

extension FASStoreViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("FASStoreItemCell", forIndexPath: indexPath) as! FASStoreItemCell
        let product = fetchedStoreItemsController.objectAtIndexPath(indexPath) as! StoreItem
        cell.setProduct(product)
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedStoreItemsController.sections![section].numberOfObjects
    }
}

///////////////////////////////////////////////
// MARK: - FRC
///////////////////////////////////////////////

extension FASStoreViewController: NSFetchedResultsControllerDelegate {
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
       // self.tableView.reloadData()
    }
}

///////////////////////////////////////////////
// MARK: - FASDataObserver -
///////////////////////////////////////////////
extension FASStoreViewController:  FASDataObserver {
    
    func fasDataNotification(notification: NSNotification) {
        printlnDebug("[TRACE]: FASStoreViewController received FASData notification. Notification was:\n\(notification)");
        
        if notification.name == FASDataProductsDidUpdateNotification {
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.fasDataProductDidUpdate(notification)
            })
        }
    }
    
    func fasDataProductDidUpdate(notification: NSNotification!) {
        //self.tableView.reloadData()
    }
}