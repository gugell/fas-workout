//
//  FASCircleTimerView.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/11/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit

class FASCircleTimerView: UIView {

    @IBOutlet weak var progressView: RadialProgressView!
    
    var progress: Float = 0.0 {
        didSet {
            progressView.progress = CGFloat(progress)
        }
    }
    
    var title: String? = "" {
        didSet {
            progressView.titleLabel.text = title
        }
    }
    
    var subtitle: String? = "" {
        didSet {
            progressView.captionLabel.text = subtitle
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        progressView.titleLabel.font = UIFont.boldBabasFontOfSize(32.0)
        progressView.captionLabel.font = UIFont.boldBabasFontOfSize(67.0)
    }
}
