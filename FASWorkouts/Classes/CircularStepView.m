//
//  CircularStepView.m
//  FAS Workouts
//
//  Created by Vladimir Stepanchenko on 12/15/14.
//  Copyright (c) 2014 Vladimir Stepanchenko. All rights reserved.
//

#import "CircularStepView.h"

#define STEPPER_BUTTON_TIMER_INTERVAL 0.17

@interface CircularStepView ()

@property(nonatomic, retain) UIButton *topButton;
@property(nonatomic, retain) UIButton *bottomButton;

@property(nonatomic, assign) UIButton *activeButton;

@property(nonatomic, retain) CAShapeLayer *mainLayer;
@property(nonatomic, retain) CAShapeLayer *arrowLayerUp;
@property(nonatomic, retain) CAShapeLayer *arrowLayerDown;

@property(nonatomic, retain) UILabel *valueLabel;

@property(nonatomic, retain) NSTimer *stepTimer;

@property(nonatomic) BOOL timerFired;

@end

@implementation CircularStepView

- (id)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

-(void)setup
{
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    self.backgroundColor = [UIColor clearColor];
    // Defaults
    _value = 0;
    _min = 0;
    _step = 1;
    _max = NSUIntegerMax;
    // Buttons
    _topButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _bottomButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    _topButton.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height / 2);
    _bottomButton.frame = CGRectMake(0, self.frame.size.height / 2, self.frame.size.width, self.frame.size.height / 2);
    
    [_topButton addTarget:self action:@selector(touchTimerDisable) forControlEvents:(UIControlEventTouchUpInside | UIControlEventTouchDragOutside | UIControlEventTouchDragExit | UIControlEventTouchUpOutside)];
    [_topButton addTarget:self action:@selector(buttonDown:) forControlEvents:UIControlEventTouchDown];
    
    
    [_bottomButton addTarget:self action:@selector(touchTimerDisable) forControlEvents:(UIControlEventTouchUpInside | UIControlEventTouchDragOutside | UIControlEventTouchDragExit | UIControlEventTouchUpOutside)];
    [_bottomButton addTarget:self action:@selector(buttonDown:) forControlEvents:UIControlEventTouchDown];
    
    [self addSubview:_topButton];
    [self addSubview:_bottomButton];
    // Layers
    _mainLayer = [CAShapeLayer layer];
    _mainLayer.lineWidth = 2.;
    _mainLayer.strokeColor = [UIColor whiteColor].CGColor;
    _mainLayer.fillColor = [UIColor clearColor].CGColor;
    
    _arrowLayerUp = [CAShapeLayer layer];
    _arrowLayerUp.lineWidth = 2.5;
    _arrowLayerUp.strokeColor = [UIColor whiteColor].CGColor;
    _arrowLayerUp.fillColor = [UIColor clearColor].CGColor;
    
    _arrowLayerDown = [CAShapeLayer layer];
    _arrowLayerDown.lineWidth = 2.5;
    _arrowLayerDown.strokeColor = [UIColor whiteColor].CGColor;
    _arrowLayerDown.fillColor = [UIColor clearColor].CGColor;
    
    [self.layer addSublayer:_mainLayer];
    [self.layer addSublayer:_arrowLayerUp];
    [self.layer addSublayer:_arrowLayerDown];
    
    // Label
    _valueLabel = [UILabel new];
    _valueLabel.textAlignment = NSTextAlignmentCenter;
    if (_textColor)
        _valueLabel.textColor = _textColor;
    else
        _valueLabel.textColor = [UIColor colorWithRed:248./255. green:228./255. blue:1.0/255. alpha:1.];
    
    CGFloat fontSize = 90.;
    if ([UIScreen mainScreen].bounds.size.height < 568) {
        fontSize = 70.;
    }
    _valueLabel.font = [UIFont fontWithName:@"BebasNeueBold" size:fontSize];
    _valueLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)_value];
    
    [_valueLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [self addSubview:_valueLabel];
}

-(void)setTextColor:(UIColor *)textColor
{
    _textColor = textColor;
    _valueLabel.textColor = _textColor;
}

-(void)touchTimerFired
{
    _timerFired = YES;
    if (_activeButton == _topButton)
        [self changeValue:_topButton];
    else if (_activeButton == self.bottomButton)
        [self changeValue:_bottomButton];
    // Continue counting
    if (_activeButton)
    {
        _stepTimer = [NSTimer scheduledTimerWithTimeInterval:STEPPER_BUTTON_TIMER_INTERVAL target:self selector:@selector(touchTimerFired) userInfo:nil repeats:NO];
    }
}

-(void)touchTimerDisable
{
    [_stepTimer invalidate];
    if (!_timerFired)
        [self changeValue:_activeButton];
    _activeButton = nil;
    _arrowLayerUp.strokeColor = [UIColor whiteColor].CGColor;
    _arrowLayerDown.strokeColor = [UIColor whiteColor].CGColor;
}

-(void)changeValue:(id)sender
{
    BOOL report = YES;
    if (sender == _topButton) {
        if (_value == _max)
            report = NO;
        if (_value + _step < _max) {
            _value += _step;
        }
        else
            _value = _max;
        // Report
        if (report && _delegate && [_delegate respondsToSelector:@selector(valueChanged:)])
            [_delegate valueChanged:_value];
        // Revert visual state
        //_arrowLayerUp.strokeColor = [UIColor whiteColor].CGColor;
    } else if (sender == _bottomButton) {
        if (_value == _min)
            report = NO;
        if (_value < _step) {
            _value = _min;
        }
        else
            _value -= _step;
        // Report to delegate
        if (report && _delegate && [_delegate respondsToSelector:@selector(valueChanged:)])
            [_delegate valueChanged:_value];
        
        //_arrowLayerDown.strokeColor = [UIColor whiteColor].CGColor;
    }
    [self reReadValueLabel];
}

-(void)reReadValueLabel
{
    _valueLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)_value];
    [self setNeedsDisplay];
}

-(void)buttonDown:(id)sender
{
    _timerFired = NO;
    _activeButton = sender;
    _stepTimer = [NSTimer scheduledTimerWithTimeInterval:STEPPER_BUTTON_TIMER_INTERVAL target:self selector:@selector(touchTimerFired) userInfo:nil repeats:NO];
    if (sender == _topButton)
        _arrowLayerUp.strokeColor = [UIColor grayColor].CGColor;
    else
        _arrowLayerDown.strokeColor = [UIColor grayColor].CGColor;
    // Update display
    [self setNeedsDisplay];
}

-(void)drawCircle
{
    CGFloat lineWidth = _mainLayer.lineWidth;
    CGRect reducedBounds = CGRectMake(lineWidth, lineWidth, self.bounds.size.width - lineWidth, self.bounds.size.width - lineWidth);
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:reducedBounds];
    // Set the path
    _mainLayer.path = path.CGPath;
}

-(void)drawArrows
{
    UIBezierPath *pathUp = [UIBezierPath new];
    
    CGFloat yLineOffset = 15.;
    
    CGFloat lineLength = 10.;
    if ([UIScreen mainScreen].bounds.size.height < 568) {
        yLineOffset = 10.;
        lineLength = 7.;
    }
    CGFloat currentY = yLineOffset;
    
    [pathUp moveToPoint:CGPointMake(self.bounds.size.width / 2., currentY)];
    [pathUp addLineToPoint:CGPointMake(self.bounds.size.width / 2. - lineLength, currentY + lineLength)];
    [pathUp moveToPoint:CGPointMake(self.bounds.size.width / 2. - 1, currentY)];
    [pathUp addLineToPoint:CGPointMake(self.bounds.size.width / 2. + lineLength, currentY + lineLength)];
    
    UIBezierPath *pathDown = [UIBezierPath new];
    currentY = self.bounds.size.height - yLineOffset;
    [pathDown moveToPoint:CGPointMake(self.bounds.size.width / 2., currentY)];
    [pathDown addLineToPoint:CGPointMake(self.bounds.size.width / 2. - lineLength, currentY - lineLength)];
    [pathDown moveToPoint:CGPointMake(self.bounds.size.width / 2. - 1, currentY)];
    [pathDown addLineToPoint:CGPointMake(self.bounds.size.width / 2. + lineLength, currentY - lineLength)];
    
    // Set the path
    _arrowLayerUp.path = pathUp.CGPath;
    _arrowLayerDown.path = pathDown.CGPath;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    [self drawCircle];
    [self drawArrows];
}

-(void)setValue:(NSUInteger)value
{
    _value = value;
    [self reReadValueLabel];
}

-(void)setStep:(NSUInteger)step
{
    _step = step;
}
-(void)setMinimum:(NSUInteger)min
{
    _min = min;
}

-(void)setMaximum:(NSUInteger)max
{
    _max = max;
}

- (void)updateConstraints
{
    NSLayoutConstraint *centerYConstraint = [NSLayoutConstraint
                                             constraintWithItem:self.valueLabel
                                             attribute:NSLayoutAttributeCenterY
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self
                                             attribute:NSLayoutAttributeCenterY
                                             multiplier:1.0
                                             constant:8.];
    [self removeConstraint:centerYConstraint];
    [self addConstraint:centerYConstraint];
    
    NSLayoutConstraint *centerXConstraint = [NSLayoutConstraint
                                             constraintWithItem:self.valueLabel
                                             attribute:NSLayoutAttributeCenterX
                                             relatedBy:NSLayoutRelationEqual
                                             toItem:self
                                             attribute:NSLayoutAttributeCenterX
                                             multiplier:1.0
                                             constant:0.];
    [self removeConstraint:centerXConstraint];
    [self addConstraint:centerXConstraint];
    
    [super updateConstraints];
}
@end
