//
//  UBICustomRemoteImageView.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/12/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "UBIRemoteImageView.h"

@interface UBICustomRemoteImageView : UBIRemoteImageView

// draw circular imageView
@property (nonatomic, getter=isCircular) IBInspectable BOOL   circular;

// if not equal to 0, draw borders with borderWidth
@property (nonatomic) IBInspectable CGFloat borderWidth;

// default is white color
@property (nonatomic, strong) IBInspectable UIColor* borderColor;

@property (nonatomic, copy) NSString* title;

@end
