//
//  WorkoutsViewController.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/14/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

class WorkoutsViewController: FASViewController {
    private var cacheName: String?
    
    // TODO: this should be set from latest chosen segmented control tab
    var currentFilter: [String : AnyObject] = ["difficulty": FASWorkoutDifficultyBeginner, "trainingType": FASWorkoutTrainingTypeFunctional] {
        didSet {
            fetchWithFilter()
        }
    }
    
    var fetchedWorkoutsController: NSFetchedResultsController? = nil
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        automaticallyAdjustsScrollViewInsets = false;
        
        FASData.sharedData().addWorkoutsObserver(self)
    }
    
    deinit {
        fetchedWorkoutsController?.delegate = nil
        FASData.sharedData().removeWorkoutsObserver(self)
    }
    ///////////////////////////////////////////////
    // MARK: - View controller overrides -
    ///////////////////////////////////////////////
    override func viewDidLoad() {
        super.viewDidLoad()

        fetchWithFilter()
    }
    
    ///////////////////////////////////////////////
    // MARK: - Controls -
    ///////////////////////////////////////////////
    
    func fetch() {
        if let cn = cacheName {
            NSFetchedResultsController.deleteCacheWithName(cn)
        }
        
        if let frc = fetchedWorkoutsController {
            var fetchError: NSError?
            let result: Bool
            do {
                try frc.performFetch()
                result = true
            } catch let error as NSError {
                fetchError = error
                result = false
            }
            if !result {
                if let error = fetchError {
                    // TODO: Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    printlnDebugError("Unresolved error \(error.localizedDescription),\nerror info: \(error.userInfo)")
                    abort()
                }
            }
        } else {
            printlnDebugError("Fetched Result Controller was nil upon fetch()")
        }
    }
    
    func fetchWithFilter() {
        fetchedWorkoutsController?.delegate = nil
        fetchedWorkoutsController = nil
        
        let frc: NSFetchedResultsController = FASData.sharedData().fetchedWorkoutsControllerWithKeyValueFilter(currentFilter, cacheName: self.cacheName)
        fetchedWorkoutsController = frc
        
        fetchedWorkoutsController!.delegate = self

        fetch()
    }
    
    ///////////////////////////////////////////////
    // MARK: - Workouts Notification -
    ///////////////////////////////////////////////
    func fasDataWorkoutsWillUpdate() {
        
    }
    
    func fasDataWorkoutsDidUpdate(notification: NSNotification) {
        
    }
    
    func fasDataWorkoutsDidFailToUpdate(notification: NSNotification) {
        if let userInfo = notification.userInfo, errors = userInfo["errors"] as? [FASAPIError] {
            FASAPIError.displayAlertWithErrors(errors)
        }
    }
}
///////////////////////////////////////////////
// MARK: - Fetched results controller -
///////////////////////////////////////////////
extension WorkoutsViewController: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(controller: NSFetchedResultsController) { }
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) { }
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) { }
    func controllerDidChangeContent(controller: NSFetchedResultsController) { }
}
///////////////////////////////////////////////
// MARK: - FASData Observer Protocol -
///////////////////////////////////////////////
extension WorkoutsViewController: FASDataObserver {
    func fasDataNotification(notification: NSNotification) {
        
        printlnDebugTrace("WorkoutsViewController received FASData notification, name = \(notification.name)");
        
        if notification.name == FASDataWorkoutsWillUpdateNotification {
            fasDataWorkoutsWillUpdate()
        }
        else if notification.name == FASDataWorkoutsDidUpdateNotification {
            fasDataWorkoutsDidUpdate(notification)
        }
        else if notification.name == FASDataWorkoutsUpdateDidFailNotification {
            fasDataWorkoutsDidFailToUpdate(notification)
        }
    }
}