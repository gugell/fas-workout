//
//  FASSession.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/19/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "FASAPISession.h"
#import "GCDSingleton.h"
#import "FASAPI.h"
#import "XJSafeMutableDictionary.h"
#import "NSString+MyExtensions.h"
#import "FASWorkouts-Swift.h"
#import "FASAPIObjectKeys.h"
#import "FASAPIError.h"
#import "FASUserInfo.h"
#import "UBILogger.h"
#import "StringUtils.h"
#import "FASAppDelegate.h"
#import <Crashlytics/Crashlytics.h>


#import "AFNetworking.h"


// Facebook integration
@import FBSDKCoreKit;
@import FBSDKLoginKit;

// tags
typedef NS_ENUM(NSUInteger, FASAPISessionTag) {
    FASAPISessionTagLogoutAlertView                     = 100,
    FASAPISessionTagFacebookEmailPermissionsAlertView   = 101,
};

// sessionInfo keys
NSString * const FASAPISessionInfoKey                               = @"FASAPISession";
NSString * const FASAPISessionAccountTypeKey                        = @"account_type";
NSString * const FASAPISessionSessionWordKey                        = @"sessionWord";
NSString * const FASAPISessionServerNonceKey                        = @"serverNonce";
NSString * const FASAPISessionUsernameKey                           = @"userName";
NSString * const FASAPISessionPasswordKey                           = @"password";
NSString * const FASAPISessionUserInfoKey                           = @"userInfo";
NSString * const FASAPISessionErrorCodeFromLastLoginKey             = @"errorCodeFromLastLogin";
NSString * const FASAPISessionAcceptedLicenseVersionKey             = @"acceptedLicenseVersion";

// notifications
NSString * const FASAPISessionDidLoginNotification                  = @"FASAPISessionDidLogin";
NSString * const FASAPISessionDidLogoutNotification                 = @"FASAPISessionDidLogout";
NSString * const FASAPISessionDidNotLoginNotification               = @"FASAPISessionDidNotLogin";
NSString * const FASAPISessionDidCancelLoginNotification            = @"FASAPISessionDidCancelLogin";
NSString * const FASAPISessionDidRestoreSessionNotification         = @"FASAPISessionDidRestore";
NSString * const FASAPISessionFacebookSessionDidChangeNotification  = @"FASAPISessionFacebookSessionDidChange";
NSString * const FASAPISessionLicenseAgreementDidChangeNotification = @"FASAPISessionLicenseAgreementDidChange";

@interface FASAPISession ()
@property (nonatomic, strong)   XJSafeMutableDictionary *           sessionInfo;
@property (nonatomic, strong)   FASAPI*                             loginAPI;

@property (nonatomic, strong) NSString *serverNonce;
@property (nonatomic, strong) NSString *sessionWord;
@property (nonatomic, strong, readonly) NSString *secretWord;
@end

@interface FASAPISession (UnityBaseToken)
- (NSString *)UBToken;
- (NSString *)getSessionPart;
- (NSString *)sessionId;
@end

@implementation FASAPISession
///////////////////////////////////////////////
#pragma mark -
#pragma mark Init
#pragma mark -
///////////////////////////////////////////////
+ (FASAPISession *)currentSession
{
    DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
        return [self new];
    });
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.sessionInfo = [XJSafeMutableDictionary dictionary];
        _loggingIn = NO;
        self.accountType = FASAPISessionAccountTypeUnknown;
    }
    return self;
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark NSObject Overrides
#pragma mark -
///////////////////////////////////////////////
- (NSString *)description
{
    return [NSString stringWithFormat:
            @"session ="
            @"\n\taccount_type = %lu"
            @"\n\terrorCodeFromLastLogin = %lu"
            @"\n\tsessionWord = %@"
            @"\n\tserverNonce = %@"
            @"\n\ttoken = %@"
#if DEBUG
            @"\n\tpassword = %@"
#endif
            @"\n\tusername = %@"
            @"\nacceptedLicenseVersion = %lu",
            (unsigned long)self.accountType,
            (unsigned long)self.errorCodeFromLastLogin,
            self.sessionWord,
            self.serverNonce,
            [self obfuscatedToken],
#if DEBUG
            self.password,
#endif
            self.username,
            (unsigned long)self.acceptedLicenseVersion
            ];
}



-(void) getMeAvatar {
    if ([FBSDKAccessToken currentAccessToken]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{ @"fields" : @"id,name,picture.width(500).height(500)"}]startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
            if (!error) {
                NSString *nameOfLoginUser = [result valueForKey:@"name"];
                NSString *imageStringOfLoginUser = [[[result valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"];
              
                NSLog(@"avatra vatar %@", imageStringOfLoginUser);
                
                [[NSUserDefaults standardUserDefaults] setObject:imageStringOfLoginUser forKey:@"myAvatar"];
            }
        }];
    }
}







///////////////////////////////////////////////
#pragma mark -
#pragma mark Notification Observers
#pragma mark -
///////////////////////////////////////////////
- (void)addObserver:(id)observer
{
    [NSNotificationCenter.defaultCenter
     addObserver:observer
     selector:@selector(FASAPISessionNotification:)
     name:FASAPISessionDidLoginNotification
     object:nil];
    
    [NSNotificationCenter.defaultCenter
     addObserver:observer
     selector:@selector(FASAPISessionNotification:)
     name:FASAPISessionDidLogoutNotification
     object:nil];
    
    [NSNotificationCenter.defaultCenter
     addObserver:observer
     selector:@selector(FASAPISessionNotification:)
     name:FASAPISessionDidNotLoginNotification
     object:nil];
    
    [NSNotificationCenter.defaultCenter
     addObserver:observer
     selector:@selector(FASAPISessionNotification:)
     name:FASAPISessionDidCancelLoginNotification
     object:nil];
    
    [NSNotificationCenter.defaultCenter
     addObserver:observer
     selector:@selector(FASAPISessionNotification:)
     name:FASAPISessionDidRestoreSessionNotification
     object:nil];
    
    [NSNotificationCenter.defaultCenter
     addObserver:observer
     selector:@selector(FASAPISessionNotification:)
     name:FASAPISessionFacebookSessionDidChangeNotification
     object:nil];
    
    [NSNotificationCenter.defaultCenter
     addObserver:observer
     selector:@selector(FASAPISessionNotification:)
     name:FASAPISessionLicenseAgreementDidChangeNotification
     object:nil];
}

- (void)removeObserver:(id)observer
{
    [NSNotificationCenter.defaultCenter
     removeObserver:observer
     name:FASAPISessionDidLoginNotification
     object:nil];
    
    [NSNotificationCenter.defaultCenter
     removeObserver:observer
     name:FASAPISessionDidLogoutNotification
     object:nil];
    
    [NSNotificationCenter.defaultCenter
     removeObserver:observer
     name:FASAPISessionDidNotLoginNotification
     object:nil];
    
    [NSNotificationCenter.defaultCenter
     removeObserver:observer
     name:FASAPISessionDidCancelLoginNotification
     object:nil];
    
    [NSNotificationCenter.defaultCenter
     removeObserver:observer
     name:FASAPISessionDidRestoreSessionNotification
     object:nil];
    
    [NSNotificationCenter.defaultCenter
     removeObserver:observer
     name:FASAPISessionFacebookSessionDidChangeNotification
     object:nil];
    
    [NSNotificationCenter.defaultCenter
     removeObserver:observer
     name:FASAPISessionLicenseAgreementDidChangeNotification
     object:nil];
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Account Type
#pragma mark -
///////////////////////////////////////////////
- (FASAPISessionAccountType)accountType
{
    NSNumber *accountTypeNumber = [self.sessionInfo valueForKey:FASAPISessionAccountTypeKey];
    if (accountTypeNumber) {
        return [accountTypeNumber integerValue];
    }
    else {
        return FASAPISessionAccountTypeUnknown;
    }
}

- (void)setAccountType:(FASAPISessionAccountType)accountType
{
    [self.sessionInfo setValue:@(accountType) forKey:FASAPISessionAccountTypeKey];
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Token
#pragma mark -
///////////////////////////////////////////////
- (NSString *)token
{
    if (self.sessionWord && ![self.sessionWord isBlank]) {
        return [self UBToken];
    }
    return nil;
}

- (void)setToken:(NSString *)token
{
    assert(true);
    //[self.sessionInfo setValue:token forKey:FASAPISessionTokenKey];
}

// returns an obfuscated token for use in logging
- (NSString *)obfuscatedToken
{
    NSString *token = self.token;
#ifndef DEBUG
    NSUInteger substringIndex = 0;
    if (token && token.length > 0) {
        substringIndex = token.length > 10 ? 10 : token.length - 1;
    }
    token = [token substringToIndex:substringIndex];
    token = [NSString stringWithFormat:@"%@...", token];
#endif
    return token;
}

- (void)clearToken
{
    [self.sessionInfo removeObjectForKey:FASAPISessionSessionWordKey];
    [self.sessionInfo removeObjectForKey:FASAPISessionServerNonceKey];
}

+ (NSSet *)keyPathsForValuesAffectingToken
{
    return [NSSet setWithObject:@"sessionWord"];
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark User Data
#pragma mark -
///////////////////////////////////////////////

- (NSString *)username
{
    return [self.sessionInfo valueForKey:FASAPISessionUsernameKey];
}

- (void)setUsername:(NSString *)newUsername
{
    [self.sessionInfo setValue:newUsername forKey:FASAPISessionUsernameKey];
}

- (NSString *)password
{
    return [self.sessionInfo valueForKey:FASAPISessionPasswordKey];
}

- (void)setPassword:(NSString *)newPassword
{
    [self.sessionInfo setValue:newPassword forKey:FASAPISessionPasswordKey];
}

- (NSUInteger)errorCodeFromLastLogin
{
    return [[self.sessionInfo valueForKey:FASAPISessionErrorCodeFromLastLoginKey] integerValue];
}

- (void)setErrorCodeFromLastLogin:(NSUInteger)code
{
    [self.sessionInfo setValue:@(code) forKey:FASAPISessionErrorCodeFromLastLoginKey];
}

- (FASUserInfo *)editableUserInfo
{
    if (self.userInfo) {
        FASUserInfo* userInfo = self.userInfo;
        userInfo.local = YES;
        return userInfo;
    }
    
    return nil;
}

- (NSString *)sessionWord
{
    return [self.sessionInfo valueForKey:FASAPISessionSessionWordKey];
}

- (void)setSessionWord:(NSString *)newSessionWord
{
    [self.sessionInfo setValue:newSessionWord forKey:FASAPISessionSessionWordKey];
}

- (void)setServerNonce:(NSString *)newServerNonce
{
    [self.sessionInfo setValue:newServerNonce forKey:FASAPISessionServerNonceKey];
}

- (NSString *)serverNonce
{
    return [self.sessionInfo valueForKey:FASAPISessionServerNonceKey];
}

- (NSString *)secretWord
{
    if (self.password) {
        return [StringUtils sha256HashFor:[NSString stringWithFormat:@"salt%@", self.password]];
    }
    else {
        return nil;
    }
}

// No setSecretWord: b/c it is readonly

+ (NSSet *)keyPathsForValuesAffectingSecretWord
{
    return [NSSet setWithObject:@"password"];
}

- (void)setAcceptedLicenseVersion:(NSUInteger)newAcceptedLicenseVersion
{
    NSUInteger currentLicenseVersion = [FASData.sharedData currentLicenseVersion];
    if (newAcceptedLicenseVersion < currentLicenseVersion) {
        [[NSNotificationCenter defaultCenter] postNotificationName:FASAPISessionLicenseAgreementDidChangeNotification
                                                            object:self];
    }
    
    [self.sessionInfo setValue:@(newAcceptedLicenseVersion) forKey:FASAPISessionAcceptedLicenseVersionKey];
}

- (NSUInteger)acceptedLicenseVersion
{
    return [[self.sessionInfo valueForKey:FASAPISessionAcceptedLicenseVersionKey] unsignedIntegerValue];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Login
#pragma mark -
///////////////////////////////////////////////
- (NSDictionary *)credentials
{
    NSMutableDictionary *currentCredentials = [NSMutableDictionary dictionary];
    [currentCredentials setValue:self.username forKey:FASAPISessionUsernameKey];
    [currentCredentials setValue:self.password forKey:FASAPISessionPasswordKey];
    return currentCredentials;
}

- (void)setCredentials:(NSDictionary *)credentials
{
    [self.sessionInfo setValuesForKeysWithDictionary:credentials];
}

//  - YES if username & password are stored in session
- (BOOL)hasCredentials
{
    return (self.username && ![self.username isBlank] &&
            self.password && ![self.password isBlank]);
}

- (void)clearCredentials
{
    [self.sessionInfo removeObjectForKey:FASAPISessionUsernameKey];
    [self.sessionInfo removeObjectForKey:FASAPISessionPasswordKey];
}
// returns YES if session contains necessary info to login and obtain a token w/o
// requiring user interaction
- (BOOL)canLoginWithoutUserInteraction
{
    return [self hasCredentials];
}

- (void)login
{
    // don't allow multiple simultaneous logins
    if ([self isLoggingIn]) {
        UBILogWarning(@"Already logging-in");
        return;
    }
    
    // if have FB session
    if ([FBSDKAccessToken currentAccessToken]) {
        [self loginWithFacebook];
    }
    
    // else if have username/password
    else if ([self hasCredentials]) {
        [self loginWithCredentials:nil];
    }
    
    else {
        UBILogError(@"[WARNING] No credentials is found to do autologin. Logout to clear the session info.");
        
        [self logout];
        
        [NSNotificationCenter.defaultCenter
         postNotificationName:FASAPISessionDidNotLoginNotification
         object:self
         userInfo:nil];
    }
}

- (void)loginWithCredentials:(NSDictionary *)credentials
{
    if (credentials) {
        self.credentials = credentials;
    }
    
    UBILogTrace(@"Login with username:\t%@", self.username);
    self.accountType = FASAPISessionAccountTypeUser;
    
    self.loginAPI = [FASAPI loginStartWithUsername:self.username delegate:self];

    [self.loginAPI invoke];
}

- (void)finishLogin
{
    if (self.serverNonce && ![self.serverNonce isBlank]) {
        NSString *clientNonce = [StringUtils getClientNonce];
        NSString* password = [self getPasswordHash:clientNonce];
        
        self.loginAPI = [FASAPI loginWithClientNonce:clientNonce
                                            username:self.username
                                         andPassword:password
                                            delegate:self];
        [self.loginAPI invoke];
    }
    
    else {
        UBILogError(@"invalid server nonce");
        // TODO: what to do here?
    }
}

- (NSString*) getPasswordHash:(NSString*)clientNonce {
    NSString* result = @"";
    NSString* server = FASEnvironment.APIBasePath;
    NSString* appName  = [server lastPathComponent];
    NSString* shaData = [NSString stringWithFormat:@"%@%@%@%@%@", appName, self.serverNonce, clientNonce, self.username, [self secretWord]];
    result = [StringUtils sha256HashFor:shaData];
    
    return result;
}

- (void)loginWithFacebook
{
    UBILogTrace(@"FB login");
    self.accountType = FASAPISessionAccountTypeUser;
    self.loginAPI = [FASAPI loginWithFacebookToken:[FBSDKAccessToken currentAccessToken].tokenString
                                         delegate:self];
    [self.loginAPI invoke];
}

- (void)userActionCancelledLogin
{
    [NSNotificationCenter.defaultCenter
     postNotificationName:FASAPISessionDidCancelLoginNotification
     object:nil];
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Logout
#pragma mark -
///////////////////////////////////////////////
- (void)logoutWithConfirmation:(BOOL)shouldConfirm
{
    if (shouldConfirm) {
        // TODO: localize this
        FASAlertView *alert = [[FASAlertView alloc] initWithTitle:@"Are you sure you want to logout?"
                                                         message:nil
                                                        delegate:self
                                                cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:@"Logout",nil];
        alert.tag = FASAPISessionTagLogoutAlertView;
        [alert show];
    }
    else {
        [self logout];
    }
}

- (void)logout
{
    if ([self isLoggedIn]) {
        UBILogTrace(@"logout - have token");
        FASAPI *api = [FASAPI logout:self];
        [api invoke];
    }
    else {
        UBILogTrace(@"logout - no token");
    }
    
    [self logoutFacebook];
    
    [self clearToken];
    
    [self.sessionInfo removeObjectForKey:FASAPISessionUsernameKey];
    [self.sessionInfo removeObjectForKey:FASAPISessionPasswordKey];
    [self.sessionInfo removeObjectForKey:FASAPISessionErrorCodeFromLastLoginKey];
    
    self.userInfo = nil;
    
    self.accountType = FASAPISessionAccountTypeUnknown;
    
    [self save];
    [self notifyObserversAboutSessionChangeWithUserInfo:nil];
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Status
#pragma mark -
///////////////////////////////////////////////
- (BOOL)isLoggedIn
{
    BOOL ret = NO;
    if (!self.isLoggingIn) {
        ret = self.token && ![self.token isBlank];
    }
    return ret;
}

- (void)setLoginAPI:(FASAPI *)loginAPI
{
    // only cancel existing loginAPI if replacing with a new one
    if (loginAPI) {
        [_loginAPI cancel];
    }
    _loginAPI = loginAPI;
}

- (BOOL)isLoggedInAsUser
{
    return
    [self isLoggedIn] &&
    self.accountType == FASAPISessionAccountTypeUser;
}

- (BOOL)isLoggedInWithUserID:(NSNumber *)userID
{
    return
    [self isLoggedIn] &&
    [self.userInfo.userID compare:userID] == NSOrderedSame;
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Session
#pragma mark -
///////////////////////////////////////////////
- (void)restore
{
    // restore session dictionary
    NSUserDefaults *defaults = NSUserDefaults.standardUserDefaults;
    NSDictionary *cachedSessionInfo = [defaults dictionaryForKey:FASAPISessionInfoKey];
    if (cachedSessionInfo) {
        self.sessionInfo = [XJSafeMutableDictionary dictionaryWithDictionary:cachedSessionInfo];
        
        // TODO: should it be related to (cachedSessionInfo not nil) condition
        NSData *userInfoData = [[NSUserDefaults standardUserDefaults] objectForKey:FASAPISessionUserInfoKey];
        self.userInfo = [NSKeyedUnarchiver unarchiveObjectWithData:userInfoData];
        
        if (![self.userInfo isValidUserID]) {
            UBILogWarning(@"userInfo.userId==0 after restore session");
        }
        else {
            [[Crashlytics sharedInstance] setUserIdentifier:self.userInfo.stringUserID];
            [[Crashlytics sharedInstance] setUserEmail:self.userInfo.email];
        }
        
        UBILogTrace(@"Did Restore Session:\n%@", [self description]);
    }
    
    else {
        UBILogWarning(@"Did Restore Session:\n<no stored session data found>");
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(fbAccessTokenDidChange:)
                                                 name:FBSDKAccessTokenDidChangeNotification
                                               object:nil];
    
    [NSNotificationCenter.defaultCenter
     postNotificationName:FASAPISessionDidRestoreSessionNotification
     object:nil];
}

// save session dictionaty
- (void)save
{
    NSUserDefaults *defaults = NSUserDefaults.standardUserDefaults;
    [defaults setObject:self.sessionInfo forKey:FASAPISessionInfoKey];
    
    // TODO: make user info a part of sessionInfo dictionary?
    if (self.userInfo) {
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.userInfo];
        [defaults setObject:data forKey:FASAPISessionUserInfoKey];
    }
    else {
        [defaults removeObjectForKey:FASAPISessionUserInfoKey];
        UBILogWarning(@"userInfo was nil upon saving session data");
    }
    
    [defaults synchronize];
}

// sends Did Login or Did Logout notifications only
- (void)notifyObserversAboutSessionChangeWithUserInfo:(NSDictionary *)userInfo
{
    if (self.token) {
        [NSNotificationCenter.defaultCenter
         postNotificationName:FASAPISessionDidLoginNotification
         object:nil
         userInfo:userInfo];
    }
    else {
        [NSNotificationCenter.defaultCenter
         postNotificationName:FASAPISessionDidLogoutNotification
         object:nil
         userInfo:nil];
    }
}

- (void)updateSessionData
{
    if ([self isLoggedIn]) {
        self.loginAPI = [FASAPI userInfo:self];
        [self.loginAPI invoke];
    }
    else {
        UBILogWarning(@"Cannot update uer info, because is NOT logged in");
    }
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark UIAlertViewDelegate
#pragma mark -
///////////////////////////////////////////////
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == FASAPISessionTagLogoutAlertView) {
        if (buttonIndex != alertView.cancelButtonIndex) {
            UBILogTrace(@"user action - logout");
            [self logout];
        }
    }
    
    else if (alertView.tag == FASAPISessionTagFacebookEmailPermissionsAlertView) {
        if (buttonIndex == [alertView cancelButtonIndex]) {
            [self logoutFacebook];
        }
        else {
            [self requestFacebookReadPermissionEmail];
        }
    }
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark FASAPIDelegate
#pragma mark -
///////////////////////////////////////////////

- (void)FASAPIwillInvoke:(FASAPI *)theFASAPI { }

- (void)FASAPIdidInvoke:(FASAPI *)theFASAPI
{
    switch (theFASAPI.method) {
        case FASAPIMethodLoginFacebook:
            // fallthrough
        case FASAPIMethodLoginStart: {
            self.loggingIn = YES;
        } break;
            
        default: break;
    }
}

- (void)FASAPIdidFinish:(FASAPI *)theFASAPI
{
    UBILogTrace(@"%@", theFASAPI);
    
    switch (theFASAPI.method) {
            
        case FASAPIMethodLoginStart: {
            
            self.serverNonce = [theFASAPI.response valueForKey:FASAPIResultKey];
            [self finishLogin];
            
        } break;
        
        case FASAPIMethodLogin:
        case FASAPIMethodLoginFacebook: {
            
            FASAPIError* error = nil;
            NSDictionary* json = theFASAPI.response;
            NSString *result = [json valueForKey:FASAPIResultKey];
            if (result && ![result isBlank]) {
                self.sessionWord = result;
                [self setErrorCodeFromLastLogin:FASAPIErrorCodeNoError];
            }
            else {
                [self clearToken];
                error = [FASAPIError errorWithInfo:json];
                [self setErrorCodeFromLastLogin:error.code];
            }
            
            [self save];
            
            UBILogTrace(@"username \"%@\" logged in with token %@",
                        self.username, // obtained from crendentials being set BEFORE the login
                        self.obfuscatedToken);
            
            self.loggingIn = NO;
            self.loginAPI = nil;
            
            [self notifyObserversAboutSessionChangeWithUserInfo:nil];
            
        } break;
            
        case FASAPIMethodRegister: {
            
            // if register succeeded, set the account type to a user account
            self.accountType = FASAPISessionAccountTypeUser;
            
            self.username = [theFASAPI.params valueForKey:FASAPIUsernameParam];
            self.password = [theFASAPI.params valueForKey:FASAPIPasswordParam];
            
        } break;
            
        case FASAPIMethodUserInfo: {
            
            // save updated user data
            NSDictionary* result = theFASAPI.response[FASAPIResultKey];
            BOOL success = [theFASAPI.response[FASAPISuccessKey] boolValue];
            if (success) {
                if (!self.userInfo) {
                    self.userInfo = [FASUserInfo new];
                }
                [self.userInfo addValuesFromDictionary:result];
                
                [self save];
            }
            
            FASAppDelegate *appDelegate = (id)UIApplication.sharedApplication.delegate;
            [appDelegate serverReportsAPNSTokens:[result valueForKey:FASAPIAPNSTokensKey]];
            
            // report latest license agreement accepted by user
            id alavObj = [result valueForKey:FASAPIAcceptedLegalAgreementVersionKey];
            NSUInteger latestAcceptedVersion = ((alavObj != nil && [alavObj isKindOfClass:[NSNumber class]]) ? [(NSNumber*)alavObj unsignedIntegerValue] : 0);
            [self setAcceptedLicenseVersion:latestAcceptedVersion]; // 0 - means used has never accepted license yet
            
            // update use preferences
            [FASData.sharedData preferencesUpdatedWithDictionary:result[FASAPISettingsKey]];
        } break;
        
        case FASAPIMethodLogout: {
            
            UBILogTrace(@"did logout");
            
        } break;
            
        default: break;
    }
}

- (void)FASAPIdidFail:(FASAPI *)theFASAPI
{
    switch (theFASAPI.method) {
            
        case FASAPIMethodLogout: {
            
            UBILogError(@"logout did fail.");
            
        } break;
            
        case FASAPIMethodLoginFacebook: {
            // logout from FB in case of failure
            [self logoutFacebook];
        } // fallthrough
            
        case FASAPIMethodLoginStart:
        case FASAPIMethodLogin: {
            
            UBILogError(@"login did fail.");
            self.loggingIn = NO;
            self.loginAPI = nil;
            
            // if login failed b/c of connectivity don't reset current credentials
            if (![FASAPIError errors:theFASAPI.errors containsErrorWithCode:FASAPIErrorCodeNSURLErrorTimedOut]) {
                self.accountType = FASAPISessionAccountTypeUnknown;
                [self clearToken];
            }
            
            // if login failed b/c of invalid credentials reset current credentials
            if ([FASAPIError errors:theFASAPI.errors containsErrorWithCode:FASAPIErrorCodeInvalidUsernameOrPassword]
                || [FASAPIError errors:theFASAPI.errors containsErrorWithCode:FASAPIErrorCodeEmailInvalid]) {
                [self clearCredentials];
            }
            
            FASAPIError *error = [theFASAPI.errors firstObject];
            self.errorCodeFromLastLogin = error.code;
            
            [self save];
            
            NSMutableArray *errors = [NSMutableArray arrayWithArray:theFASAPI.errors];
            
            [NSNotificationCenter.defaultCenter
             postNotificationName:FASAPISessionDidNotLoginNotification
             object:self
             userInfo:@{@"errors" : errors}];
            
        } break;
            
        case FASAPIMethodUserInfo: {
            
            UBILogError(@"UserInfo did fail with API: %@", self.loginAPI);
            
        } break;
            
        default: break;
    }
}

-(BOOL)validatePreconditionsForFASAPI:(FASAPI *)theFASAPI apiError:(FASAPIError *__autoreleasing *)apiError
{
    if (theFASAPI.method == FASAPIMethodLoginStart) {
        EmailValidator* emailValidator = [EmailValidator new];
        emailValidator.input = theFASAPI.params[FASAPIUsernameParam];
        if (![emailValidator validateWithError:nil]) {
            *apiError = [FASAPIError errorWithCode:FASAPIErrorCodeEmailInvalid value:nil];
            return NO;
        }
    }
    
    return YES;
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Facebook
#pragma mark -
///////////////////////////////////////////////

- (void)fbAccessTokenDidChange:(NSNotification*)notification
{
    if ([notification.name isEqualToString:FBSDKAccessTokenDidChangeNotification]) {
        BOOL didChangeUserID = NO;
        if ([notification.userInfo valueForKey:FBSDKAccessTokenDidChangeUserID]) {
            didChangeUserID = [[notification.userInfo valueForKey:FBSDKAccessTokenDidChangeUserID] boolValue];
        }
        UBILogTrace(@"FB access token did change. %@", didChangeUserID ? @"NEW USER ID" : @"UPDATE FOR CURRENT USER ID");
        
        [NSNotificationCenter.defaultCenter
         postNotificationName:FASAPISessionFacebookSessionDidChangeNotification
         object:nil];
    }
}

- (void)connectFacebookAccount
{
    [self connectFacebookAccountWithCompletionHandler:nil];
}

- (void)connectFacebookAccountWithCompletionHandler:(void (^)(NSError *))completionHandler
{
    NSArray *readPermissions = @[@"public_profile", @"email"];
    
    if (![FBSDKAccessToken currentAccessToken]) {
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        self.loggingIn = YES;
        [login logInWithReadPermissions:readPermissions fromViewController:nil handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            [self fbLoginManagerLoginResult:result didChangeWithError:error];
            
            [self getMeAvatar];
            
            if (completionHandler) {
                completionHandler(error);
            }
        }];
    }
    
    else {
        if (completionHandler) {
            [self getMeAvatar];
            completionHandler(nil);
        }
    }
}

- (void)requestFacebookReadPermissionEmail
{
    [self requestFacebookReadPermission:@"email" withCompletionHandler:nil];
}

- (void)requestFacebookReadPermission:(NSString *)permission withCompletionHandler:(void (^)(NSError *))completionHandler
{
    // is session opened?
    if ([FBSDKAccessToken currentAccessToken]) {
        // is permission declined?
        if ([[FBSDKAccessToken currentAccessToken].declinedPermissions containsObject:permission] ) {
            NSArray* readPermissions = @[permission];
            FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
            self.loggingIn = YES;
            [login logInWithReadPermissions:readPermissions fromViewController:nil handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                [self fbLoginManagerLoginResult:result didChangeWithError:error];
                
                if (completionHandler) {
                    completionHandler(error);
                }
            }];
        }
    }
    
    else {
        if (completionHandler) {
            completionHandler(nil);
        }
    }
}

- (void)connectFacebookAccountAndRequestPublishPermissionsWithCompletionHandler:(void (^)(NSError *))completionHandler
{
    // if already has token, request publish permissions
    if ([FBSDKAccessToken currentAccessToken]) {
        [self requestFacebookPublishPermissionsWithCompletionHandler:completionHandler];
    }
    
    else {
        // TODO: accoring to FB documentation:
        //
        // Quote:
        // "For this case, make sure that the only read permissions you request are public_profile. This provides the best user experience because the user wants to publish from your app and is often not interested in providing additional read permissions. It will also improve your conversion."
        [self connectFacebookAccountWithCompletionHandler:^(NSError *error) {
            [self requestFacebookPublishPermissionsWithCompletionHandler:completionHandler];
        }];
    }
}

- (void)requestFacebookPublishPermissionsWithCompletionHandler:(void (^)(NSError *))completionHandler
{
    static bool haveRequestedWritePermissisons = NO;
    
    // IF we already have publish permissions, call the completion handler
    if ([self hasFacebookPublishPermissions]) {
        if (completionHandler) {
            completionHandler(nil);
        }
    }
    
    // ELSE request permissions
    else if (!haveRequestedWritePermissisons) {
        haveRequestedWritePermissisons = YES;
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logInWithPublishPermissions:@[@"publish_actions"]
                        fromViewController:nil handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                            if (error) {
                                UBILogError(@"fb publish permissions failed: %@", error);
                            }
                            else if (result.isCancelled) {
                                UBILogWarning(@"fb publish permissions cancelled");
                            }
                            else if ([result.grantedPermissions containsObject:@"publish_actions"]) {
                                UBILogTrace(@"fb publish permissions granted");
                            }
                            
                            if (completionHandler) {
                                completionHandler(error);
                            }
                            
                            haveRequestedWritePermissisons = NO;
                        }];
    }
}

- (BOOL)hasFacebookPublishPermissions
{
    return [[FBSDKAccessToken currentAccessToken].permissions containsObject:@"publish_actions"];
}

- (BOOL)hasFacebookReadPermissions
{
    return [[FBSDKAccessToken currentAccessToken].permissions containsObject:@"email"];
}

- (void)logoutFacebook
{
    if ([FBSDKAccessToken currentAccessToken]) {
        [[FBSDKLoginManager new] logOut];
    }
}

- (void)fbLoginManagerLoginResult:(FBSDKLoginManagerLoginResult *)result didChangeWithError:(NSError *)error
{
    if (error) {
        UBILogError(@"fb connect failed: %@", error);
        [self handleFacebookError:error];
        self.loggingIn = NO;
    }
    else if (result.isCancelled) {
        UBILogWarning(@"fb connect cancelled");
        [self logoutFacebook];
        self.loggingIn = NO;
    }
    else {
        // check if specific permissions missing
        if ([result.grantedPermissions containsObject:@"email"]) {
            [self loginWithFacebook];
        }
        
        else {
            // we cannot proceeed permissions
#if 0
            // display an error & logout
            FASAPIError* unsatisfiedPermissionsError =
            [FASAPIError errorWithCode:FASAPIErrorCodeFacebookPermissionDenied
                                 value:@"email permission required"];
            [FASAPIError displayAlertWithErrors:@[unsatisfiedPermissionsError]];
#else
            // prompt user to allow permissions
            [self promptForEmailPermissions];
#endif
            self.loggingIn = NO;
        }
    }
}

- (void)handleFacebookError:(NSError *)error
{
    // Handle errors
    if (error) {
        [FASAPIError displayAlertWithErrors:@[error]];
        // Clear this token
        [self logoutFacebook];
    }
}

- (void)promptForEmailPermissions {
    FASAlertView* alertView = [[FASAlertView alloc] initWithTitle:@"Facebook permissions"
                                                          message:@"Email is necessary for creation of your unique account and synchronization all devices. We do not send spam or ads."
                                                         delegate:self
                                                cancelButtonTitle:@"Cancel"
                                                otherButtonTitles:nil];
    [alertView addButtonWithTitle:@"Confirm"];
    alertView.delegate = self;
    alertView.tag = FASAPISessionTagFacebookEmailPermissionsAlertView;
    [alertView show];
}

@end
///////////////////////////////////////////////
#pragma mark -
#pragma mark UB Token Ext
#pragma mark -
///////////////////////////////////////////////
@implementation FASAPISession (UnityBaseToken)

typedef NS_ENUM(NSUInteger, UnityBaseTokenSessionWordComponentIndex) {
    UnityBaseTokenSessionWordComponentIndexClientSessionId      = 0,
    UnityBaseTokenSessionWordComponentIndexClientSessionHash    = 1,
    UnityBaseTokenSessionWordComponentIndexClientPasswordHash   = 2,
};

- (NSString *)getSessionPart {
    NSArray *sessionArr = [self.sessionWord componentsSeparatedByString:@"+"];
    return  [sessionArr objectAtIndex:UnityBaseTokenSessionWordComponentIndexClientSessionId];
}

- (NSString *)getPasswordHashPart {
    NSArray *sessionArr = [self.sessionWord componentsSeparatedByString:@"+"];
    if (sessionArr.count > UnityBaseTokenSessionWordComponentIndexClientPasswordHash) {
        return [sessionArr objectAtIndex:UnityBaseTokenSessionWordComponentIndexClientPasswordHash];
    }
    return nil;
}

- (NSString *)getSessionIdAndHashPart
{
    NSArray *sessionArr = [self.sessionWord componentsSeparatedByString:@"+"];
    if (sessionArr.count > UnityBaseTokenSessionWordComponentIndexClientPasswordHash) {
        // session word has password hash component, so trim that part
        return [NSString stringWithFormat:@"%@+%@", [self getSessionPart], [sessionArr objectAtIndex:UnityBaseTokenSessionWordComponentIndexClientSessionHash]];
    }
    else {
        return self.sessionWord;
    }
}

- (NSString *)sessionId {
    
    NSString* sessionId = [self getSessionPart];
    NSString* hexa8ID = [NSString stringWithFormat: @"%08X", sessionId.intValue]; //hex value of sessionId
    
    long timeStamp = (long)([[NSDate date] timeIntervalSince1970]);
    NSString* hexaTime = [NSString stringWithFormat: @"%08lX", timeStamp]; //hex value of timestamp
    
    NSString* pwdHash = self.secretWord;
    if (!pwdHash) { // client does not have a password stored, get from session word instead
        pwdHash = [self getPasswordHashPart];
    }
    
    NSString* crc32X = [StringUtils CRC32X:[NSString stringWithFormat:@"%@%@%@", [self getSessionIdAndHashPart], pwdHash, hexaTime]];
    
    //UBILogTrace(@"SessionId Params: TimeStamp:%ld; TimeStampHex:%@; hexa8ID:%@; crc32X(sessionWord+pwdHash+hexaTime):%@",timeStamp, hexaTime, hexa8ID, crc32X);
    NSString *result = [NSString stringWithFormat:@"%@%@%@",hexa8ID,hexaTime,crc32X];
    //UBILogTrace(@"SessionId:%@", result);
    return result;
}

- (NSString *)UBToken {
    
    NSString* aToken = nil;
    NSString* sessionId = [self sessionId];
    if (sessionId != nil && ![sessionId isBlank]) {
        aToken = [NSString stringWithFormat:@"UB %@", sessionId];
    }
    return aToken;
}

@end