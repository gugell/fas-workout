//
//  NSError+MyExtensions.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/28/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "NSError+MyExtensions.h"
@import CoreData.CoreDataErrors;

@implementation NSError (MyExtensions)

- (NSString *)detailedInfo
{
    NSMutableString *errorInfo = [NSMutableString string];
    
    [errorInfo appendFormat:@"ERROR: %@", [self localizedDescription]];
    NSArray *detailedErrors = [[self userInfo] objectForKey:NSDetailedErrorsKey];
    if (detailedErrors && [detailedErrors count] > 0) {
        for(NSError* detailedError in detailedErrors) {
            [errorInfo appendFormat:@"\nDetailedError: %@", [detailedError userInfo]];
        }
    }
    else {
        [errorInfo appendFormat:@"userInfo = %@\n", [self userInfo]];
        [errorInfo appendFormat:@"domain = %@\n", [self domain]];
        [errorInfo appendFormat:@"code = %ld", (long)[self code]];
    }
    
    return errorInfo;
}

@end
