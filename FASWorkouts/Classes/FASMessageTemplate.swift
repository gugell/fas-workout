//
//  FASMessageTemplate.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/15/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import Foundation

struct FASDataMail {
    let emails: [String]
    let subject: String
    let messageBody: String?
    let isHTMLMessageBody = false
}

class FASMessageTemplate {
    
    class func mailTemplateForFeedback() -> FASDataMail {
        let primaryContactEmail = "info@fas-sport.com"
        let subject = "FAS Workout App. Feedback"
        
        // additional info included in mail body
        let appVersion = FASData.sharedData().appVersion()
        let appBuildVersion = FASData.sharedData().buildVersion()
        let deviceName = UIDevice.currentDevice().model
        let osVersion = UIDevice.currentDevice().systemVersion
        
        let composedMessageBody =
        "app version \(appVersion)(\(appBuildVersion))"
        "\ndevice \(deviceName)"
        "\niOS version \(osVersion)"
        
        return FASDataMail(emails: [primaryContactEmail], subject: subject, messageBody: composedMessageBody)
    }
}