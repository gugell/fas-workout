//
//  StoreItem+CoreDataProperties.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/24/16.
//  Copyright © 2016 FAS. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "StoreItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface StoreItem (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *inAppProductID;
@property (nullable, nonatomic, retain) NSString *productType;
@property (nullable, nonatomic, retain) NSNumber *sortIndex;
@property (nullable, nonatomic, retain) NSNumber *purchaseStatus;
@property (nullable, nonatomic, retain) NSSet<StoreItem *> *bulks;
@property (nullable, nonatomic, retain) NSSet<StoreItem *> *bulkItems;

@end

@interface StoreItem (CoreDataGeneratedAccessors)

- (void)addBulksObject:(StoreItem *)value;
- (void)removeBulksObject:(StoreItem *)value;
- (void)addBulks:(NSSet<StoreItem *> *)values;
- (void)removeBulks:(NSSet<StoreItem *> *)values;

- (void)addBulkItemsObject:(StoreItem *)value;
- (void)removeBulkItemsObject:(StoreItem *)value;
- (void)addBulkItems:(NSSet<StoreItem *> *)values;
- (void)removeBulkItems:(NSSet<StoreItem *> *)values;

@end

NS_ASSUME_NONNULL_END
