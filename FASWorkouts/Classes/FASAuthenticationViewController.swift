//
//  FASAuthenticationViewController.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/21/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit
// Facebook SDK
import FBSDKCoreKit
import FBSDKLoginKit

enum AuthenticationFormTableViewSection: Int {
    case Title = 0
    case Fields = 1
    case Facebook = 2
}

class FASAuthenticationViewController: FASViewController {
    
    @IBOutlet weak var tableView: UITableView!

    var fieldsCellCount: Int = 0
    
    var firstTimeUX: Bool = false
    var sessionUIDelegate: SessionUIDelegate?
    
    let textFieldCellIdentifier = "TextFieldCellIdentifier"
    let titleCellIdentifier = "TitleCellIdentifier"
    let asyncActionCellIdentifier = "AsyncActionCellIdentifier"
    let facebookCellIdentifier = "FacebookCellIdentifier"
    
    init(sessionUIDelegate: SessionUIDelegate) {
        self.sessionUIDelegate = sessionUIDelegate
        super.init(nibName: "FASAuthenticationViewController", bundle: nil)
        title = ""
        automaticallyAdjustsScrollViewInsets = false;
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "fbAccessTokenDidChange:", name: FBSDKAccessTokenDidChangeNotification, object: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: FBSDKAccessTokenDidChangeNotification, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let textFieldCellNib = UINib(nibName: "FASTextFieldTableViewCell", bundle: nil)
        tableView.registerNib(textFieldCellNib, forCellReuseIdentifier: textFieldCellIdentifier)
        
        let titleCellNib = UINib(nibName: "FASTitleTableViewCell", bundle: nil)
        tableView.registerNib(titleCellNib, forCellReuseIdentifier: titleCellIdentifier)
        
        let asyncCellNib = UINib(nibName: "FASAsynchronousActionTableViewCell", bundle: nil)
        tableView.registerNib(asyncCellNib, forCellReuseIdentifier: asyncActionCellIdentifier)
        
        tableView.registerClass(FASFacebookLoginTableViewCell.self, forCellReuseIdentifier: facebookCellIdentifier)
        
        tableView.rowHeight = 42
        tableView.estimatedRowHeight = 42
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar(.Transparent)
        registerForKeyboardNotifications()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        unregisterForKeyboardNotifications()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let topInset = topLayoutGuide.length + 110.0
        
        var tableInset: UIEdgeInsets = tableView.contentInset
        tableInset.top = topInset
        tableView.contentInset = tableInset
    }
    
    @objc func fbAccessTokenDidChange(notification: NSNotification) {
        if isViewLoaded() && notification.name == FBSDKAccessTokenDidChangeNotification {
            dispatch_async(dispatch_get_main_queue(),{
                let fbIP  = NSIndexPath(forRow: 0, inSection: AuthenticationFormTableViewSection.Facebook.rawValue)
                self.tableView.reloadRowsAtIndexPaths([fbIP], withRowAnimation: .None)
            })
        }
    }
    
    func doFacebookLogin() {
        printlnDebugTrace("doFacebookLogin")
        
        if let _ = FBSDKAccessToken.currentAccessToken() {
            FASAPISession.currentSession().logoutFacebook()
        }
        else {
           
            FASAPISession.currentSession().connectFacebookAccount()
        }
    }
    ////////////////////////////////////////////////
    // MARK: Common Cell Constructor
    ////////////////////////////////////////////////
    func tableView(tableView: UITableView, facebookCellForIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(facebookCellIdentifier) as! FASFacebookLoginTableViewCell
        
        if let _ = FBSDKAccessToken.currentAccessToken() {
            cell.titleLabel.text = "LOGOUT FACEBOOK"
        }
        else {
            cell.titleLabel.text = "CONNECT WITH FACEBOOK"
        }
        
        cell.busy = FASAPISession.currentSession().loggingIn
        cell.userInteractionEnabled = !FASAPISession.currentSession().loggingIn;
        
        return cell
    }
}

extension FASAuthenticationViewController: SessionUIController { }

// MARK: KeyboardSupport conformance

extension FASAuthenticationViewController {
    
    // MARK: Keyboard Event Notifications
    
    override func keyboardWillShow(notification: NSNotification) {
        super.keyboardWillShow(notification)
        keyboardWillChangeFrameWithNotification(notification, showsKeyboard: true)
    }
    
    override func keyboardWillHide(notification: NSNotification) {
        super.keyboardWillHide(notification)
        keyboardWillChangeFrameWithNotification(notification, showsKeyboard: false)
    }
    
    // MARK: Convenience
    
    func keyboardWillChangeFrameWithNotification(notification: NSNotification, showsKeyboard: Bool) {
        
        let userInfo = notification.userInfo!
        
        let animationDuration: NSTimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
        
        // Convert the keyboard frame from screen to view coordinates.
        let keyboardScreenBeginFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        
        let keyboardViewBeginFrame = view.convertRect(keyboardScreenBeginFrame, fromView: view.window)
        let keyboardViewEndFrame = view.convertRect(keyboardScreenEndFrame, fromView: view.window)
        let originDelta = keyboardViewEndFrame.origin.y - keyboardViewBeginFrame.origin.y
        
        // The tableView and scrollIndicator insets should be abjusted
        var contentInset: UIEdgeInsets = tableView.contentInset
        contentInset.bottom -= originDelta
        
        UIView.animateWithDuration(animationDuration, delay: 0, options: .BeginFromCurrentState, animations: {
            self.tableView.contentInset = contentInset
            self.tableView.scrollIndicatorInsets = contentInset
            }, completion: nil)
    }
}
