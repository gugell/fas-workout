//
//  WorkoutHistory.h
//  FASWorkouts
//
//  Created by Vladimir Stepanchenko on 9/17/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UnityBase.h"
#import <CoreData/CoreData.h>

@class Workout;

@interface WorkoutHistory : NSManagedObject <UBIUpdatableWithDictionary>

@property (nonatomic, retain) NSNumber * workoutHistory_id;
@property (nonatomic, retain) NSNumber * workout_id;
@property (nonatomic, retain) NSNumber * rate;
@property (nonatomic, retain) NSNumber * isCompleted;
@property (nonatomic, retain) NSDate * startDate;
@property (nonatomic, retain) NSDate * endDate;
@property (nonatomic, retain) Workout *workout;
@property (nonatomic, retain) NSNumber * pauseDuration;

@end
