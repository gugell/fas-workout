//
//  FASFAQRunList.h
//  FASWorkouts
//
//  Created by Vladimir Stepanchenko on 4/18/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "FASRunList.h"

extern NSString* const kFASFaqIdFieldKey;
extern NSString* const kFASFaqSortIndexFieldKey;
extern NSString* const kFASFaqQuestionFieldKey;
extern NSString* const kFASFaqAnswerFieldKey;

@interface FASFAQRunList : FASRunList

- (instancetype)initWithFieldList:(NSArray *)fieldList NS_DESIGNATED_INITIALIZER;

+ (instancetype)faqRunListDefault;

@end