//
//  UBILabel.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 8/26/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, UBILabelImageAlignment) {
    UBILabelImageAlignmentTop,
    UBILabelImageAlignmentBottom,
    UBILabelImageAlignmentMiddle
};


@interface UBILabel : UILabel

@property (nonatomic, strong)   UIImage *                   image;
@property (nonatomic)           CGSize                      maxSize;
@property (nonatomic)           UIEdgeInsets                edgeInsets;
@property (nonatomic)           UIEdgeInsets                imagePadding;
@property (nonatomic)           UBILabelImageAlignment      imageAlignment;

@property (nonatomic)           BOOL                        autoSize;

@end
