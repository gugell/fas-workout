//
//  FASViewController.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/7/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

class FASViewController: UIViewController {
    
    private(set) var viewHasNeverLoaded: Bool = true
    private(set) var viewHasNeverAppeared: Bool = true
    
    var hideBackButtonTitle: Bool = false {
        didSet {
            setBackButtonTitleHidden(hideBackButtonTitle)
        }
    }
    
    var mainStoryboard: UIStoryboard {
        var storyboardToReturn: UIStoryboard
        
        if self.storyboard == nil {
            storyboardToReturn = UIStoryboard(name: "Main", bundle: nil)
        }
        else {
            storyboardToReturn = self.storyboard!
        }
        
        return storyboardToReturn
    }
    
    enum FASNavigationBarStyle {
        case Default, Dark, Transparent, DarkWithYellowTitle
    }
    
    // specify the way viewcontroller, is presented  by another viewcontroller
    // the presented view controller should manually assign the value to it
    enum FASViewControllerPresentationMethod {
        case Unspecified, Push, Modal
    }
    var presentationMethod: FASViewControllerPresentationMethod = .Unspecified
    
    deinit {
        FASAPISession.currentSession().removeObserver(self);
    }
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    
    ///////////////////////////////////////////////
    // MARK: - UIViewController overrides -
    ///////////////////////////////////////////////
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        if viewHasNeverLoaded {
            viewDidLoadForTheFirstTime()
        }
        
        FASAPISession.currentSession().addObserver(self)
        
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        configureBackgroundImageView()
        setBackButtonTitleHidden(hideBackButtonTitle)
    }
    
    override func viewDidAppear(animated: Bool) {
        if viewHasNeverAppeared {
            viewDidAppearForTheFirstTime()
        }
        super.viewDidAppear(animated)
        // any view controller that wants to be on top of any other at the moment
        // must do this when current on going presentations are finished
        // 'interraptive' logic prepares view controler at the time event comes in,
        // but waits till an appropriate time to present the flow
        presentInterruptiveFlowIfNeeded()
    }
    
    // subclasses must call super's implementation when overrided
    func viewDidLoadForTheFirstTime() {
        viewHasNeverLoaded = false
    }
    
    // subclasses must call super's implementation when overrided
    func viewDidAppearForTheFirstTime() {
        viewHasNeverAppeared = false
    }
    ///////////////////////////////////////////////
    // MARK: - View Configurations -
    ///////////////////////////////////////////////
    func configureView() {
        if FASAPISession.currentSession().isLoggedIn() {
            configureViewWithSession()
        }
        else {
            configureViewWithoutSession()
        }
    }
    
    private func configureBackgroundImageView() {
        backgroundImageView?.image = FASViewHelper.backgroundImageForViewController( self )
    }
    // subclasses should override these
    func configureViewWithSession() { }
    func configureViewWithoutSession() { }
    ///////////////////////////////////////////////
    // MARK: - Status Bar -
    ///////////////////////////////////////////////
    var hideStatusBar: Bool = false {
        didSet {
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    func setBarsHidden(hidden: Bool, animated: Bool) {
        
        if hidden == hideStatusBar { return } // preventing needless hide/show cycles
        
        if hidden {
            self.navigationController?.setNavigationBarHidden(hidden, animated:animated)
            hideStatusBar = true
        } else {
            hideStatusBar = false
            self.navigationController?.setNavigationBarHidden(hidden, animated:animated)
        }
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return hideStatusBar
    }

    override func preferredStatusBarUpdateAnimation() -> UIStatusBarAnimation {
        return .Slide
    }

    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    // descenders might call this function in 'viewDidLoad'
    // to get its navigation bar styled w/ different styles
    func configureNavigationBar(style: FASNavigationBarStyle = .Default) {
        switch style {
        case .Dark:
            navigationController?.navigationBar.barStyle = .Black
            navigationController?.navigationBar.translucent = false
            navigationController?.navigationBar.tintColor = UIColor.whiteColor()
            navigationController?.navigationBar.shadowImage = UIImage(named:"TransparentPixel")
            navigationController?.navigationBar.setBackgroundImage(UIImage(named:"NavigationBarBackgroundColorPixel"), forBarMetrics: .Default)
            navigationController?.navigationBar.titleTextAttributes =
                [NSFontAttributeName: UIFont.boldBabasFontOfSize(30.0),
                    NSForegroundColorAttributeName: UIColor.whiteColor(),
                    NSBackgroundColorAttributeName: UIColor.whiteColor()]
        case .Transparent:
            navigationController!.setNavigationBarHidden(false, animated: false)
            navigationController!.navigationBar.tintColor = UIColor.whiteColor()
            let navBarBkgImage = UIImage.imageWithScreenshotFromView(view)
            let navBar = self.navigationController?.navigationBar
            navBar?.setBackgroundImage(navBarBkgImage, forBarMetrics: .Default)
            navBar?.shadowImage = UIImage()
        case .DarkWithYellowTitle:
            navigationController!.navigationBar.barStyle = .Black
            navigationController!.navigationBar.translucent = false
            navigationController!.navigationBar.tintColor = UIColor.whiteColor()
            navigationController!.navigationBar.shadowImage = UIImage(named:"TransparentPixel")
            navigationController!.navigationBar.setBackgroundImage(UIImage(named:"NavigationBarBackgroundColorPixel"), forBarMetrics: .Default)
            navigationController!.navigationBar.titleTextAttributes =
                [NSFontAttributeName: UIFont.boldBabasFontOfSize(30.0),
                    NSForegroundColorAttributeName: UIColor.fasYellowColor(),
                    NSBackgroundColorAttributeName: UIColor.fasYellowColor()]
        default: break;
        }
        
        // b/c we are using all-capital font, the adj should be applied
        navigationController?.navigationBar.setTitleVerticalPositionAdjustment(3.0, forBarMetrics: UIBarMetrics.Default)
    }
    
    // hides "Back" button title (leaving only a shevron) for all VCs pushed by this VC
    private func setBackButtonTitleHidden(isHidden: Bool) {
        if (isHidden && isViewLoaded()) {
            let backItem = UIBarButtonItem(title: " ", style: .Plain, target: nil, action: nil)
            navigationItem.backBarButtonItem = backItem
        }
    }
    
    // MARK: automatic device screen dimming control
    
    func disableScreenDimming() {
        UIApplication.sharedApplication().idleTimerDisabled = true
    }
    
    func enableScreenDimming() {
        UIApplication.sharedApplication().idleTimerDisabled = false
    }
}
///////////////////////////////////////////////
// MARK: - Extensions -
///////////////////////////////////////////////
extension FASViewController: FASAPISessionObserver {
    
    func FASAPISessionNotification(notification: NSNotification!) {
        
        if  notification.name == FASAPISessionDidLoginNotification ||
            notification.name == FASAPISessionDidLogoutNotification ||
            notification.name == FASAPISessionDidRestoreSessionNotification ||
            notification.name == FASAPISessionDidNotLoginNotification
        {
            dispatch_async(dispatch_get_main_queue(), {
                self.configureView()
            })
        }
        
        if notification.name == FASAPISessionLicenseAgreementDidChangeNotification {
            dispatch_async(dispatch_get_main_queue(), {
                // action to accesp new license is required from user right now
                self.prepareInterruptiveFlowWithType(.License)
            })
        }
    }
}

extension FASViewController: FASAPIUIDelegate {
    
    func FASAPIUIshouldWaitForSession(theFASAPI: FASAPI!) -> Bool {
        printlnDebugTrace("FASVC:FASAPIUIshouldWaitForSession:")
        let sessionCtrl: SessionController = SessionController(style: .LoginOrRegister)
        sessionCtrl.sessionControllerDelegate = self
        presentViewController(sessionCtrl.navCtrl!, animated: true, completion: nil)
        return true
    }
}

extension FASViewController: SessionControllerDelegate {
    
    func sessionControllerDidCancel(sessionController: SessionController) {
        doneWithSessionController(sessionController)
    }
    
    func sessionControllerDidFinish(sessionController: SessionController) {
        doneWithSessionController(sessionController)
    }
    
    // common actions for both callbacks
    private func doneWithSessionController(sessionController: SessionController) {
        dispatch_async(dispatch_get_main_queue(), {
            sessionController.dismissViewController()
            self.configureView()
        })
    }
}

extension FASViewController: FASLicenseAgreementViewControllerDelegate {
    func licenseAgreementViewController(vc: FASLicenseAgreementViewController, didDismissWithSuccess isSuccess: Bool) {
        vc.delegate = nil
        FASViewControllerInterruptiveFlowVariables.reset()
    }
}

// MARK: - Interraptive Flow Controls

extension FASViewController {
    
    // MARK: - Interruptive Flow Data
    
    enum FASViewControllerInterruptiveFlowType: CustomStringConvertible {
        case None, License
        var description : String {
            switch self {
            case .License:
                return "License"
            default:
                return "_NONE"
            }
        }
    }
    struct FASViewControllerInterruptiveFlowVariables {
        static var interraptiveFlowType: FASViewControllerInterruptiveFlowType = .None
        static var interruptiveNavigationController: UINavigationController?
        static var interruptiveFlowIsPresented: Bool = false
        
        static func reset() {
            FASViewControllerInterruptiveFlowVariables.interruptiveNavigationController = nil
            FASViewControllerInterruptiveFlowVariables.interruptiveFlowIsPresented = false
            FASViewControllerInterruptiveFlowVariables.interraptiveFlowType = .None
        }
    }
    
    private func presentInterruptiveFlowIfNeeded() {
        // IF some view controller is ready to be presented
        // AND not yet presented
        // THEN present it and change the respective flag
        if let vc = FASViewControllerInterruptiveFlowVariables.interruptiveNavigationController
            where FASViewControllerInterruptiveFlowVariables.interruptiveFlowIsPresented == false
        {
            FASViewControllerInterruptiveFlowVariables.interruptiveFlowIsPresented = true
            self.presentViewController(vc, animated: false, completion: { () -> Void in
                printlnDebugTrace("Interruptive flow \"\(FASViewControllerInterruptiveFlowVariables.interraptiveFlowType)\" presented")
            })
        }
    }
    
    private func prepareInterruptiveFlowWithType(type: FASViewControllerInterruptiveFlowType) {
        if (FASViewControllerInterruptiveFlowVariables.interraptiveFlowType != .None) {
            // some interraptive flow is already active, ignore this one
            return
        }
        FASViewControllerInterruptiveFlowVariables.interraptiveFlowType = type
        
        switch type {
        case .License:
            if FASViewControllerInterruptiveFlowVariables.interruptiveNavigationController == nil {
                if let vc = self.mainStoryboard.instantiateViewControllerWithIdentifier("LicenseAgreementStoryboardID") as? FASLicenseAgreementViewController {
                    vc.modalPresentationStyle = .FormSheet
                    vc.presentationMethod = .Modal
                    vc.delegate = self
                    let nav = UINavigationController(rootViewController: vc)
                    
                    FASViewControllerInterruptiveFlowVariables.interruptiveNavigationController = nav
                    FASViewControllerInterruptiveFlowVariables.interruptiveFlowIsPresented = false
                
                    printlnDebugTrace("Interruptive flow \"\(FASViewControllerInterruptiveFlowVariables.interraptiveFlowType)\" is ready")
                }
        }
            
        default:
            0 // do nothing
        }
    }
}
