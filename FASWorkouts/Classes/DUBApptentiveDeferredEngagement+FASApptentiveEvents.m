//
//  DUBApptentiveDeferredEngagement+FASApptentiveEvents.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 2/4/16.
//  Copyright © 2016 FAS. All rights reserved.
//

#import "DUBApptentiveDeferredEngagement+FASApptentiveEvents.h"

// Who? Events
NSString * const kFASApptentiveEventDidFinishWorkout = @"did_finish_workout";
NSString * const kFASApptentiveEventDidMakeInAppPurchase = @"did_make_in_app_purchase";
// Where? events
NSString * const kFASApptentiveEventAfterDidFinishWorkout = @"after_did_finish_workout";
NSString * const kFASApptentiveEventAfterDidMakeInAppPurchase = @"after_did_make_in_app_purchase";
NSString * const kFASApptentiveEventAfterDidViewWorkoutDetail = @"after_did_view_workout_detail";

@implementation DUBApptentiveDeferredEngagement (FASApptentiveEvents)

+ (BOOL)engage:(NSString *)event fromViewController:(UIViewController *)viewController
{
    [[DUBApptentiveDeferredEngagement sharedInstance] deferEngage:event];
    return [[DUBApptentiveDeferredEngagement sharedInstance] engageDeferred:event fromViewController:viewController];
}

@end
