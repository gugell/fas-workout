//
//  FASWorkoutProgressView.swift
//  FASWorkouts
//
//  Created by Vladimir Stepanchenko on 3/27/16.
//  Copyright © 2016 FAS. All rights reserved.
//

import Foundation

class FASWorkoutProgressView : UIView {
    @IBInspectable var xibName: String?
    
    @IBOutlet var view: UIView!
    @IBOutlet var exCountLabel : UILabel!
    @IBOutlet var timeLabel : UILabel!
    @IBOutlet var timeProgressView : UIProgressView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        NSBundle.mainBundle().loadNibNamed("FASWorkoutProgressView", owner: self, options: nil)[0] as! UIView
        self.addSubview(view)
        view.frame = self.bounds
    }
    
    // MARK: - Actions
    func setProgress(exerciseProgress : Int, totalExerciseCount : Int) {
        exCountLabel.text = "\(exerciseProgress)/\(totalExerciseCount)"
        timeProgressView.progress = Float(exerciseProgress) / Float(totalExerciseCount)
    }
    
    func updateWithReadableTime(time : String) {
        timeLabel.text = time
    }
}