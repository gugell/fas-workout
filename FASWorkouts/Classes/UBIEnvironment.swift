//
//  UBAEnvironment.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/14/14.
//  Copyright (c) 2014 Dubinin Evgeniy. All rights reserved.
//

import Foundation

class UBIEnvironment: NSObject {
    private let kUBIEnvironmentKeyAllowOverrides = "AllowOverrides"
    private let kUBIInfoDictionaryKeyEnvironmentConfig = "UBIEnvironmentConfig"
    private let environmentConfig: String
    private var environmentInfo: [String: AnyObject]
    private var allowOverrides: Bool {
        get {
            return valueForKey(kUBIEnvironmentKeyAllowOverrides) as! Bool
        }
        set {
            setValue(newValue, forKey: kUBIEnvironmentKeyAllowOverrides)
        }
    }
    
    // singleton method
    class var sharedInstance : UBIEnvironment {
        struct StaticStruct {
            static let instance: UBIEnvironment = UBIEnvironment()
        }
        return StaticStruct.instance
    }
    
    override init() {
        let mainBundle = NSBundle.mainBundle()
        environmentConfig = mainBundle.objectForInfoDictionaryKey(kUBIInfoDictionaryKeyEnvironmentConfig) as! String
        environmentInfo = [:]
        super.init()
        loadEnvironment()
    }
    
    class func info() -> Dictionary <String, AnyObject> {
        return sharedInstance.environmentInfo
    }
    
    class func config() -> String {
        return sharedInstance.environmentConfig
    }
    
    // config overrides
    class func shouldAllowOverrides () -> Bool {
        return sharedInstance.allowOverrides
    }
    
    class func setShouldAllowOverrides (allowOverrides: Bool) {
        sharedInstance.allowOverrides = allowOverrides
    }
    
    class func deleteOverrides () {
        deleteOverridesForEnviromentNamed( config() )
        loadEnvironment()
    }
    
    class func deleteOverridesForEnviromentNamed (name: String) {
        let overridesKey = "UBIEnvironmentOverrides_" + name
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.removeObjectForKey(overridesKey)
    }
    
    // get/set values
    
    override func setValue(value: AnyObject!, forKey key: String) {
        environmentInfo[key] = value
    }
    
    override func valueForKey(key: String) -> AnyObject? {
        return environmentInfo[key]
    }
    
    override class func valueForKey(key: String) -> AnyObject? {
        return self.sharedInstance.valueForKey(key)
    }
    
    override class func setValue(value: AnyObject!, forKey key: String) {
        self.sharedInstance.setValue(value, forKey: key)
    }
    
    class func setOverrideValue(value:AnyObject!, forKey key: String!) {
        self.sharedInstance.setOverrideValue(value, forKey: key)
    }
    
    func setOverrideValue(value:AnyObject!, forKey key:String!) {
        if allowOverrides {
            let userDefaults = NSUserDefaults.standardUserDefaults()
            let defaultKey = "UBIEnvironmentOverrides_" + environmentConfig
            var overrides: Dictionary? = userDefaults.dictionaryForKey(defaultKey)
            if (overrides == nil) {
                overrides = [String: AnyObject]()
            }
            overrides![key] = value
            userDefaults.setObject(overrides!, forKey: defaultKey)
            setValue(value, forKey: key)
            if (value == nil) {
                // TODO: in Swift this is never executed, but origin logic has this check
                loadEnvironment()
            }
        }
    }
    
    func loadEnvironment() {
        environmentInfo.removeAll(keepCapacity: true)
        loadEnvironmentNamed("Default")
        loadEnvironmentNamed(environmentConfig)
    }
    
    class func loadEnvironment() {
        sharedInstance.loadEnvironment()
    }
    
    func loadEnvironmentNamed(name: String?) {
        if name == nil {
            return
        }
        
        let filename = "Environment-" + name!
        if let plistPath = NSBundle.mainBundle().pathForResource(filename, ofType: "plist") {
            if let loadedDictionary: [String: AnyObject] = readDictionaryFromFile(plistPath) {
                environmentInfo += loadedDictionary
            }
        }
        else {
            printlnDebugError("Cannot find file \(filename).plist")
        }
        
        loadOverridesForEnviromentNamed(name!)
    }
    
    func loadOverridesForEnviromentNamed(name: String) {
        if allowOverrides {
            let overridesKey = "UBIEnvironmentOverrides_" + name
            let userDefaults = NSUserDefaults.standardUserDefaults()
            if let overrides: [String: AnyObject] = userDefaults.valueForKey(overridesKey) as? [String: AnyObject] {
                for (aKey, aValue) in overrides {
                    setValue(aValue, forKey: aKey)
                }
            }
            
        }
    }
    
    // TODO: move to some extentions
    
    func readDictionaryFromFile(filePath: String) -> Dictionary<String,AnyObject!>? {
        var anError : NSError?
        let data : NSData?
        do {
            data = try NSData(contentsOfFile: filePath, options: NSDataReadingOptions.DataReadingUncached)
        } catch let error as NSError {
            anError = error
            data = nil
        }
        
        if let _ = anError{
            return nil
        }
        
        let dict : AnyObject?
        do {
            dict = try NSPropertyListSerialization.propertyListWithData(data!, options: [],format: nil)
        } catch let error as NSError {
            anError = error
            dict = nil
        }
        
        if let _ = anError {
            printlnDebugError("Couldn't read the file at filepath \(filePath). Error was: \(anError!.localizedDescription)")
        }
        else if let ocDictionary = dict as? NSDictionary {
            var swiftDict : [String:AnyObject] = [:]
            for key : AnyObject in ocDictionary.allKeys{
                let stringKey : String = key as! String
                
                if let keyValue : AnyObject = ocDictionary.valueForKey(stringKey){
                    swiftDict[stringKey] = keyValue
                }
            }
            return swiftDict
        } else {
            return nil
        }
        
        return nil
    }
}