//
//  SessionController.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/28/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit;

class SessionController {
    
    var sessionControllerDelegate: SessionControllerDelegate?
    var navCtrl: UINavigationController?
    var firstTimeUX: Bool = false {
        didSet {
            if let rootVC: UIViewController = self.navCtrl?.viewControllers[0] {
                if let loginVC = rootVC as? LoginInfoViewController {
                    loginVC.firstTimeUX = firstTimeUX
                }
            }
        }
    }
    
    enum SessionControllerStyle : Int {
        case LoginOrRegister
        case Login
        case Register
    }
    let style: SessionControllerStyle
    
    class func presentSessionController<T: UIViewController where T: SessionControllerDelegate>(style: SessionControllerStyle, fromViewController vc: T ) -> SessionController {
        let sc: SessionController = SessionController(style: style)
        sc.configure()
        sc.sessionControllerDelegate = vc
        vc.presentViewController(sc.navCtrl!, animated: true, completion: nil)
        return sc
    }
    
    init(style: SessionControllerStyle) {
        self.style = style
    }
    
    // this is a workaround of restriction of not being able to reference self inside init() method
    // see usage above
    private func configure() {
        let firstVC: UIViewController = firstViewControllerForSessionControllerStyle(self.style)
        let cancelButton: UIBarButtonItem =  UIBarButtonItem(barButtonSystemItem: .Cancel, target: self, action: "sessionUIDidCancel")
        firstVC.navigationItem.leftBarButtonItem = cancelButton
        navCtrl = UINavigationController(rootViewController: firstVC)
        firstVC.modalPresentationStyle = .PageSheet
    }
    
    private func firstViewControllerForSessionControllerStyle(sessionControllerStyle: SessionControllerStyle) -> UIViewController
    {
        let firstVC: UIViewController
        
        switch sessionControllerStyle {
        case .Login:
            firstVC = LoginViewController(sessionUIDelegate: self)
        case .Register:
            firstVC = RegistrationViewController(sessionUIDelegate: self)
        default:
            firstVC = LoginInfoViewController(sessionUIDelegate: self)
        }
        
        return firstVC
    }
    
    func dismissViewController() {
        self.navCtrl!.dismissViewControllerAnimated(true, completion: nil)
    }
}
///////////////////////////////////////////////
// MARK: - Extensions -
///////////////////////////////////////////////
extension SessionController: SessionUIDelegate {
    func sessionUIDidCancel(sessionUIController: SessionUIController) {
        sessionControllerDelegate?.sessionControllerDidFinish(self)
    }
    
    func sessionUIDidFinish(sessionUIController: SessionUIController) {
        FASAPISession.currentSession().userActionCancelledLogin()
        sessionControllerDelegate?.sessionControllerDidCancel(self)
    }
    // optional (available for @objc protocols only)
//    func titleForSessionUI(sessionUIController: SessionUIController) {
//        
//    }
//    
//    func subtitleForSessionUI(sessionUIController: SessionUIController) {
//        
//    }
}