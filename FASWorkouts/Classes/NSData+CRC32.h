//
//  NSData+CRC32.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/25/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

@import Foundation;
@interface NSData (CRC32)


- (NSUInteger)crc32;

@end