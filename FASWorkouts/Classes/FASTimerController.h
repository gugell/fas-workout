//
//  FASTimer.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/9/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, FASTimerControllerMode) {
    FASTimerControllerModeDefault, // normal frequency
    FASTimerControllerModeHighUse, // high frequency
};

typedef NS_ENUM(NSUInteger, FASTimerControllerType) {
    FASTimerControllerTypeCountdownTimer, // counting from some specifyc time to 0 (e.g. 10, 9, ..., 0)
    FASTimerControllerTypeStopwatch, // counting from 0 until stop (e.g. 0, 1, 2, ...)
};

@class FASTimerController;
@protocol FASTimerControllerDelegate
- (void)timerController:(FASTimerController *)timerController didCountToTime:(NSTimeInterval)time;
- (void)timerController:(FASTimerController *)timerController didFinishCountdownWithTime:(NSTimeInterval)time;
- (void)timerController:(FASTimerController *)timerController didCountToDate:(NSDate *)date;
@end

@interface FASTimerController : NSObject

@property (nonatomic, readonly) NSTimeInterval userTime;

@property (nonatomic, readonly) FASTimerControllerMode mode;
@property (nonatomic, readonly) FASTimerControllerType type;

// is the timer running
@property (unsafe_unretained,readonly) BOOL counting;

// reset the Timer after countdown (e.g. set back to 00:00:36)
// default is NO
@property (assign) BOOL resetTimerAfterFinish;

@property (nonatomic, weak) id <FASTimerControllerDelegate> delegate;

#if NS_BLOCKS_AVAILABLE
@property (copy) void (^endedBlock)(NSTimeInterval);
#endif

- (instancetype)initWithType:(FASTimerControllerType)type
                    userTime:(NSTimeInterval)userTime
                        mode:(FASTimerControllerMode)mode;

- (void)start;
#if NS_BLOCKS_AVAILABLE
-(void)startWithEndingBlock:(void(^)(NSTimeInterval countTime))end; //use it if you are not going to use delegate
#endif
- (void)stop;
- (void)pause;
- (void)reset;
-(void) skip:(double)timeToSkip;

- (void)setStopWatchTime:(NSTimeInterval)time;

- (NSTimeInterval)getTimeRemaining;
- (NSTimeInterval)getTimeCounted;

@end
