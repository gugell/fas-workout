//
//  FASExerciseProgressView.m
//  FASExerciseProgressView
//
//  Created by Vladimir Stepanchenko on 3/13/16.
//  Copyright © 2016 FAS. All rights reserved.
//

#import "FASExerciseProgressView.h"
#import "FASExerciseProgressLayer.h"
#import "FASWorkouts-Swift.h"

@implementation FASExerciseProgressView

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self configureWithFrame:[self frame]];
    }
    return self;
}

-(instancetype)init{
    self = [super init];
    if (self) {
        [self configureWithFrame:CGRectNull];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self configureWithFrame:frame];
    }
    return self;
}

-(void)configureWithFrame:(CGRect)frame{
    [self setContentScaleFactor:[[UIScreen mainScreen] scale]];
    // Defaults
    [self setValue:1.f];
    [self setProgressStrokeColor:[UIColor fasYellowColor]];
    [self setProgressColor:[UIColor fasYellowColor]];
    [self setProgressCapType:kCGLineCapButt];
    [self setEmptyBackgroundColor:[UIColor fasGrayColor62]];
    [self setEmptyLineWidth:7.f];
    [self setProgressLineWidth:7.f];
}

#pragma mark - Getters and Setters for layer properties

-(void)setValue:(CGFloat)value{
    [self progressLayer].animated = NO;
    self.progressLayer.value = value;
    
    if(value == 0){
        [self.layer setNeedsDisplay];
    }
}

-(CGFloat)value{
    return self.progressLayer.value;
}

-(void)setProgressLineWidth:(CGFloat)width{
    self.progressLayer.progressLineWidth = width;
}

-(CGFloat)progressLineWidth{
    return self.progressLayer.progressLineWidth;
}

-(void)setEmptyLineWidth:(CGFloat)width{
    self.progressLayer.emptyLineWidth = width;
}

-(CGFloat)emptyLineWidth{
    return self.progressLayer.emptyLineWidth;
}

-(void)setProgressColor:(UIColor*)color{
    self.progressLayer.progressColor = color;
}

-(UIColor*)progressColor{
    return self.progressLayer.progressColor;
}

-(void)setProgressStrokeColor:(UIColor*)color{
    self.progressLayer.progressStrokeColor = color;
}

-(UIColor*)progressStrokeColor{
    return self.progressLayer.progressStrokeColor;
}

-(void)setEmptyBackgroundColor:(UIColor *)emptyLineColor{
    self.progressLayer.emptyLineColor = emptyLineColor;
}

-(UIColor*)emptyBackgroundColor{
    return self.progressLayer.emptyLineColor;
}

#pragma mark - CALayer

-(FASExerciseProgressLayer*)progressLayer{
    FASExerciseProgressLayer* layer = (FASExerciseProgressLayer*) self.layer;
    return layer;
}

+ (Class)layerClass
{
    return [FASExerciseProgressLayer class];
}

#pragma mark - Compatible time controls
-(void)setTotalTime:(NSTimeInterval)time
{
    self.duration = time;
    // Reset to full
    [self progressLayer].animated = NO;
    [self progressLayer].value = 1.;
}

-(void)setCurrrentTime:(NSTimeInterval)time animated:(BOOL)animated
{
    CGFloat progress = (CGFloat)time / (CGFloat)_duration;
    if (progress >= 0.)
    {
        BOOL animate = animated;
        [self progressLayer].animated = animate;
        [self progressLayer].value = 1 - progress;
    }
    else
    {
        [self progressLayer].animated = NO;
        [self progressLayer].value = 0;
    }
}

@end
