//
//  UBILogger.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/3/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#ifndef FASWorkouts_UBILogger_h
#define FASWorkouts_UBILogger_h

// --- TRACE

#ifdef DEBUG_LOGGING
#define UBILogTrace(formatString, ...) NSLog((@"[TRACE]: %s [Line %d] " formatString), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define UBILogTrace(...)
#endif

// --- WARNING

#ifdef DEBUG_LOGGING
#define UBILogWarning(formatString, ...) NSLog((@"[WARNING]: %s [Line %d] " formatString), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define UBILogWarning(...)
#endif

// --- ERROR

#ifdef DEBUG_LOGGING
#define UBILogError(formatString, ...) NSLog((@"[ERROR]: %s [Line %d] " formatString), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define UBILogError(...)
#endif

#endif
