//
//  FASImageAndTwoLabelsHeaderView.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 2/22/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import Foundation
import UIKit

enum FASImageAndTwoLabelsHeaderViewLabelStyle {
    case Default, Reverse
}

enum FASImageAndTwoLabelsHeaderViewAction {
    case DidPressImage
}
protocol FASImageAndTwoLabelsHeaderViewDelegate {
    func imageAndTwoLabelsHeaderView(view: FASImageAndTwoLabelsHeaderView, didAction action: FASImageAndTwoLabelsHeaderViewAction)
}


class FASImageAndTwoLabelsHeaderView: UIView {
    // MARK: Public Properties
    var shouldHaveSeparator: Bool = true {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var labelStyle: FASImageAndTwoLabelsHeaderViewLabelStyle? {
        didSet  {
            if let newStyle = labelStyle {
                switch (newStyle) {
                case .Reverse:
                    leftTextView.style = .Reverse
                    rightTextView.style = .Reverse
                    
                case .Default:
                    leftTextView.style = .Default
                    rightTextView.style = .Default
                }
            }
        }
    }
    
    var item: FASImageAndTwoLabelsHeaderViewData? {
        didSet {
            updateView()
        }
    }
    
    var delegate: FASImageAndTwoLabelsHeaderViewDelegate? = nil
    
    @IBOutlet weak var leftTextView: FASTwoLabelsView!
    @IBOutlet weak var rightTextView: FASTwoLabelsView!
    @IBOutlet weak var remoteImageView: UBICustomRemoteImageView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var onSaleView: UIImageView!
    
    override func drawRect(rect: CGRect) {
        // draw line separator
        if shouldHaveSeparator == true, let context: CGContextRef = UIGraphicsGetCurrentContext() {
            CGContextSetStrokeColorWithColor(context, UIColor.fasSeparatorColor().CGColor)
            CGContextSetLineWidth(context, 2.0)
            CGContextMoveToPoint(context, 0.0, rect.height)
            CGContextAddLineToPoint(context, rect.width, rect.height)
            CGContextStrokePath(context)
        }
    }
    
    ///////////////////////////////////////////////
    // MARK: - Init -
    ///////////////////////////////////////////////
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        labelStyle = .Default
        remoteImageView.borderColor = UIColor.fasYellowColor()
    }
    
     func updateView() {
        
        rightTextView.titleLabel.text = item?.rightTitle() ?? ""
        rightTextView.subtitleLabel.text = item?.rightValue() ?? ""
        
        leftTextView.titleLabel.text = item?.leftTitle() ?? ""
        leftTextView.subtitleLabel.text = item?.leftValue() ?? ""
        
        if let remoteImage = item?.image() {
            remoteImageView.remoteImage = remoteImage
//            remoteImageView.hidden = false
//            imageView.hidden = true
        }
        else {
            remoteImageView.remoteImage = nil
//            remoteImageView.hidden = true
//            imageView.hidden = false
        }
        
        if let _ = item?.imageTitle() where item?.imageTitle().isBlank() == false {
            remoteImageView.title = item!.imageTitle()
        }
        else {
            remoteImageView.title = nil
        }
    }

    override func tintColorDidChange() {
        if self.tintAdjustmentMode == .Dimmed {
            // dimm
            remoteImageView.borderColor = UIColor.darkGrayColor()
        }
        else {
            // restore
            remoteImageView.borderColor = UIColor.fasYellowColor()
        }
    }
    
    ///////////////////////////////////////////////
    // MARK: - Actions -
    ///////////////////////////////////////////////
    
    @IBAction func imageButtonDidPress(sender: AnyObject?) {
        self.delegate?.imageAndTwoLabelsHeaderView(self, didAction: .DidPressImage)
    }
}
