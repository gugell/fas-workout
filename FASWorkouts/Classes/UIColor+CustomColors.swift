//
//  UIColor+CustomColors.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/5/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

extension UIColor {
    
    class func fasBlackColor(alpha: Float = 1.0) -> UIColor {
        return UIColor.colorWithComponents(red: 37.0, green: 37.0, blue: 37.0, alpha: alpha)
    }
    
    // MARK: - Gray Colors -
    
    class func fasGrayColor62() -> UIColor {
        return UIColor.colorWithComponents(red: 62.0, green: 62.0, blue: 62.0, alpha: 1.0)
    }
    
    class func fasGrayColor41() -> UIColor {
        return UIColor.colorWithComponents(red: 41.0, green: 41.0, blue: 41.0, alpha: 1.0)
    }
    
    class func fasGrayColor94() -> UIColor {
        return UIColor.colorWithComponents(red: 94.0, green: 94.0, blue: 94.0, alpha: 1.0)
    }
    
    class func fasGrayColor206(alpha alpha: Float = 1.0) -> UIColor {
        return UIColor.colorWithComponents(red: 206.0, green: 206.0, blue: 206.0, alpha: alpha)
    }
    
    class func fasGrayColor53() -> UIColor {
        return UIColor.colorWithComponents(red: 53.0, green: 53.0, blue: 53.0, alpha: 1.0)
    }
    
    // MARK: - Yellow Colors -
    
    class func fasYellowColor() -> UIColor {
        return UIColor(hexString: "#f8e401")!
    }
    
    class func fasSeparatorColor() -> UIColor {
        return UIColor.colorWithComponents(red: 78.0, green: 78.0, blue: 78.0, alpha: 1.0)
    }
    
    // MARK: - Green Colors -
    
    class func fasGreenColor() -> UIColor {
        return UIColor.colorWithComponents(red: 152.0, green: 252.0, blue: 12.0, alpha: 1.0)
    }
    
    // MARK: - Shorthand -
    
    class func colorWithComponents(red redComponent: Float, green: Float, blue: Float, alpha: Float) -> UIColor {
        let r: CGFloat = CGFloat(redComponent/255.0)
        let g: CGFloat = CGFloat(green/255.0)
        let b: CGFloat = CGFloat(blue/255.0)
        let a: CGFloat = CGFloat(alpha)
        return UIColor(red: r, green: g, blue: b, alpha: a)
    }
}
