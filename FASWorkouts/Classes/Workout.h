//
//  Workout.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/30/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "UnityBase.h"
#import "UBIRemoteImageCache.h"
#import "FASWorkoutDataTypes.h"

// possible 'difficulty' level const
extern NSString * const FASWorkoutDifficultyBeginner;
extern NSString * const FASWorkoutDifficultyIntermediate;
extern NSString * const FASWorkoutDifficultyAdvanced;

// possible 'trainingType' level const
extern NSString * const FASWorkoutTrainingTypeFunctional;
extern NSString * const FASWorkoutTrainingTypeTabata;
extern NSString * const FASWorkoutTrainingTypeFavourite;

@class Exercise;

@interface Workout : NSManagedObject
<UBIUpdatableWithDictionary>

+ (UBIRemoteImageCache *)workoutImageCache;
- (UBIRemoteImage *)remoteImage;
- (NSString *)remoteImageKey;

// persistent attributes
@property (nonatomic, strong, readonly) NSDate*  createdOn;
@property (nonatomic, strong) NSString* baseImageFilename;
@property (nonatomic, strong) NSNumber* exerciseCount;

@property (nonatomic, strong) NSNumber* currentAllTime;

@property (nonatomic, strong, readonly) NSNumber* workout_id;
@property (nonatomic, copy, readonly) NSString* name;
@property (nonatomic, copy, readonly) NSString* code;
@property (nonatomic, strong, readonly) NSNumber* restTime;
@property (nonatomic, strong) NSNumber* timeSpan;
@property (nonatomic, strong, readonly) NSNumber* lapsNumber;
@property (nonatomic, strong, readonly) NSString* difficulty;
@property (nonatomic, strong, readonly) NSString* motivationText;
@property (nonatomic, strong, readonly) NSDate* mi_modifyDate;
@property (nonatomic, copy, readonly) NSString* muscleTypes;
@property (nonatomic, strong, readonly) NSNumber* calories;
@property (nonatomic, strong, readonly) NSNumber* onSale;
@property (nonatomic, strong, readonly) NSString* inAppProductID;
@property (nonatomic, strong, readonly) NSString* gender;
@property (nonatomic, strong, readonly) NSString* primaryMuscleType;
@property (nonatomic, strong, readonly) NSNumber* sortIndex;
@property (nonatomic, strong, readonly) NSNumber* purchaseStatus;
@property (nonatomic, strong, readonly) NSNumber* dowload_status;
@property (nonatomic, strong, readonly) NSString* weekDayInfo;
@property (nonatomic, strong, readonly) NSString* trainingType;
@property (nonatomic, strong, readonly) NSString* linkToImage;
@property (nonatomic, strong, readonly) NSOrderedSet *exercises;




// TODO: transient property to track operation status
// @property (nonatomic, strong) NSNumber * operation_status;
// @property (nonatomic, strong) NSNumber * operation_progress;
// @property (nonatomic) BOOL isLoadingPhoto;

- (void)addExercisesFromArray:(NSArray *)collection;

// Comerce
- (BOOL)isOnSale;
- (BOOL)isAvailable;

// Gender
- (FASWorkoutGenderSpecificType)genderSpecific;

// Download Status
- (WorkoutDownloadStatus)downloadStatus;
- (void)setDownloadStatus:(WorkoutDownloadStatus)newStatus;

@end

@interface Workout (Format)
- (NSString *)formattedMuscleTypes;
- (NSString *)formattedPrimaryMuscleType;
- (NSString *)formattedCalories;
- (NSString *)formattedGenderSpecific;
- (NSString *)formattedAvailabilityStatus;
@end

@interface Workout (ExercisesAccessors)
- (void)addExercisesObject:(Exercise *)value;
- (void)removeExercisesObject:(Exercise *)value;
- (void)addExercises:(NSSet *)value;
- (void)removeExercises:(NSSet *)value;
- (NSMutableSet *)primitiveExercises;
- (void)setPrimitiveExercises:(NSMutableSet *)exercises;
@end

#import "FASModifyDateProtocol.h"
@interface Workout (FASModifyDateConformance) <FASModifyDate>
@end

#import "FASImageAndTwoLabelsHeaderViewDataProtocol.h"
@interface Workout (FASImageAndTwoLabelsHeaderViewData) <FASImageAndTwoLabelsHeaderViewData>
@end
