//
//  ScrollingToolbar.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 2/21/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ScrollingToolbar;

@protocol ScrollingToolbarDelegate
- (void)scrollingToolbar:(ScrollingToolbar *)toolbar didSelectButtonAtIndex:(NSUInteger)index;
@end

@interface ScrollingToolbar : UIView <UIScrollViewDelegate>

@property (nonatomic, weak) IBOutlet id <ScrollingToolbarDelegate> delegate;
@property (nonatomic, strong) NSMutableArray *buttons;

@property (nonatomic, strong, readonly) UIScrollView *scrollView;
@property (nonatomic, strong) UIButton *selectedButton;
@property (nonatomic) NSUInteger buttonPadding;
@property (nonatomic) BOOL showsScrollArrows;
@property (nonatomic) UIEdgeInsets contentEdgeInsets;
@property (nonatomic) CGSize maxButtonSize;
@property (nonatomic) BOOL allowMultipleSelection;
@property (nonatomic) BOOL centerButtons;

// button actions
- (void)initState;

- (void)buttonAction:(id)selector;

- (UIButton *)addButtonWithTitle:(NSString *)title;
- (UIButton *)addButtonWithTitle:(NSString *)title tag:(NSUInteger)tag;
- (UIButton *)addButtonWithImage:(UIImage *)image tag:(NSUInteger)tag;
- (UIButton *)addButtonWithTitle:(NSString *)title andImage:(UIImage *)image;

- (void)addButton:(UIButton *)button;

- (void)removeButtonWithTag:(NSUInteger)tagValue;
- (void)removeAllButtons;

- (void)showScrollArrowsIfNecessary;

- (void)selectButtonAtIndex:(NSUInteger)index scroll:(BOOL)scroll animated:(BOOL)animated;
- (void)selectButtonWithTag:(NSUInteger)tagValue scroll:(BOOL)scroll animated:(BOOL)animated;
- (UIButton *)buttonWithTag:(NSUInteger)tagValue;
- (void)layoutButtons;
- (void)scrollToSelectedButton:(BOOL)animated;
- (void)scrollToButton:(UIButton *)button animated:(BOOL)animated;
- (void)setTitle:(NSString *)title forButtonWithTag:(NSUInteger)tagValue;

@end