//
//  NSString+MyExtensions.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/19/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MyExtensions)

// cutting out
NS_ASSUME_NONNULL_BEGIN
- (NSString *)stringByTrimmingWhitespaceCharacters;
- (NSString *)stringByRemovingFirstOccurrenceOfString:(NSString *)string;
- (NSString *)stringByTakingFirstPartOfEmailAddress;

// validation
- (BOOL)isEmpty;
- (BOOL)isBlank;

// encoding/decoding
- (null_unspecified NSString *)URLEncodedString;
- (null_unspecified NSString *)URLDecodedString;

// formatted sting
+ (NSString *)formattedTimeFromSeconds:(NSNumber *)seconds;
NS_ASSUME_NONNULL_END
@end
