//
//  FASToolbarButton.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 3/1/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "FASToolbarButton.h"
#import "FASWorkouts-Swift.h"

@implementation FASToolbarButton

+ (FASToolbarButton *)fasToolbarButton
{
    FASToolbarButton *button = [FASToolbarButton buttonWithType:UIButtonTypeCustom];
    
    [button setTitleColor:[UIColor colorWithWhite:1.0f alpha:0.2f]
                 forState:UIControlStateNormal];
    [button setTitleColor:UIColor.whiteColor
                 forState:UIControlStateHighlighted];
    [button setTitleColor:UIColor.whiteColor
                 forState:UIControlStateSelected];
    
    button.spacing = 0;
    button.contentEdgeInsets = UIEdgeInsetsMake(0, 6, 0, 6);
    [button.titleLabel setFont:[UIFont boldBabasFontOfSize:31.0f]];
    button.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    
    return button;
}

- (CGSize)sizeThatFits:(CGSize)size
{
    CGSize computedSize = [super sizeThatFits:size];
    CGSize stringSize = [self.titleLabel.text sizeWithAttributes:@{NSFontAttributeName:self.titleLabel.font}];
    computedSize.width = MAX(computedSize.width, stringSize.width) + self.spacing;
    return computedSize;
}

- (void)centerButtonAndImageWithSpacing:(CGFloat)spacing
{
    self.spacing = spacing;
    self.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, spacing);
    self.titleEdgeInsets = UIEdgeInsetsMake(0, spacing, 0, 0);
    [self setNeedsLayout];
}

@end
