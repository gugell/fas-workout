//
//  UBIRunList.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/31/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "UBIRunList.h"
#import "UBILogger.h"

// keys
const NSString * UBIAPIRunListEntityKey         = @"entity";
const NSString * UBIAPIRunListMethodKey         = @"method";
const NSString * UBIAPIRunListFieldListKey      = @"fieldList";
const NSString * UBIAPIRunListOptionsKey        = @"options";
const NSString * UBIAPIRunListWhereListKey      = @"whereList";

const NSString * UBIAPIRunListOptionsLimitKey  = @"limit";
const NSString * UBIAPIRunListOptionsStartKey  = @"start";

///////////////////////////////////////////////
#pragma mark -
#pragma mark UBIRunListWhereListItem
#pragma mark -
///////////////////////////////////////////////

@interface UBIRunListWhereListItem : NSObject
@property (nonatomic, copy) NSString* name;
@property (nonatomic) UBIRunListWhereListCondition condition;
// TODO: value and key migh be of type array for more complex conditions
@property (nonatomic, copy) id value;
@property (nonatomic, copy) NSString* key;
@end

@implementation UBIRunListWhereListItem
@end

///////////////////////////////////////////////
#pragma mark -
#pragma mark UBIRunListWhereList
#pragma mark -
///////////////////////////////////////////////

@interface UBIRunListWhereList ()
@property (nonatomic, strong) NSMutableArray* whereListItems;
@end

@implementation UBIRunListWhereList

- (instancetype)init {
    self = [super init];
    if (self) {
        _whereListItems = [NSMutableArray arrayWithCapacity:1];
    }
    return self;
}

- (void)addItem:(NSString *)name
      condition:(UBIRunListWhereListCondition)condition
          value:(id)value
            key:(NSString *)key
{
    UBIRunListWhereListItem* aCondition = [UBIRunListWhereListItem new];
    aCondition.name = name;
    aCondition.condition = condition;
    aCondition.value = value;
    aCondition.key = key;
    
    [self.whereListItems addObject:aCondition];
}

// conforming to UBIStringRepresentable protocol
- (NSString *)stringValue
{
    if (self.whereListItems.count == 1) {
        UBIRunListWhereListItem* cond = (UBIRunListWhereListItem *)self.whereListItems.firstObject;
        NSMutableString* resultString = [[NSMutableString alloc] init];
        switch (cond.condition) {
            case UBIRunListWhereListConditionEqual: {
                // Example:
                /*
                 "whereList":{
                 "difficultyFilter":{
                 "values":{"difficulty":"intermidiate"},
                 "expression":"[difficulty]",
                 "condition":"equal"}
                 }}
                 */
                [resultString appendFormat:@",\"%@\":{",UBIAPIRunListWhereListKey];
                
                [resultString appendFormat:@"\"%@\":{", cond.name];
                [resultString appendFormat:@"\"values\":{\"%@\":\"%@\"},\"expression\":\"[%@]\",", cond.key, cond.value, cond.key];
                [resultString appendFormat:@"\"condition\":\"equal\"}}}"];
                
                return resultString;
            } break;
                
            case UBIRunListWhereListConditionIn: {
                // Example:
                /*
                 "whereList":{
                     "AttachmentsForWorkoutByExerciseIDs":{
                        "values":{"exerciseID":["3000000001001","3000000002328"]},
                        "expression":"[exerciseID]",
                        "condition":"in"
                    }
                 }
                 */
                [resultString appendFormat:@",\"%@\":{",UBIAPIRunListWhereListKey];
                [resultString appendFormat:@"\"%@\":{", cond.name];
                
                NSMutableString* valuesString = [NSMutableString string];
                if ([cond.value isKindOfClass:[NSArray class]] && [(NSArray*)cond.value count] > 0) {
                    NSArray* arrayValue = (NSArray *)cond.value;
                    [valuesString appendString:@"["];
                    for (id item in arrayValue) {
                        if ([item isKindOfClass:[NSNumber class]]) {
                            // add as a number
                            [valuesString appendFormat:@"%@,", [(NSNumber*)item stringValue]];
                        }
                        
                        else {
                            // add as a string
                            [valuesString appendFormat:@"\"%@\",", item];
                        }
                    }
                    [valuesString deleteCharactersInRange:NSMakeRange([valuesString length]-1, 1)]; // remove last comma
                    [valuesString appendString:@"]"];
                }
                [resultString appendFormat:@"\"values\":{\"%@\":%@},\"expression\":\"[%@]\",", cond.key, valuesString, cond.key];
                [resultString appendFormat:@"\"condition\":\"in\"}}"];
                
                return resultString;
            } break;
                
            default:
                UBILogWarning(@"Unexpected condition type send to UBIRunList");
                break;
        }
    }
    
    return @"";
}

// Note: hystorically, where list is not returned upon response,
// so, there is no need to add UBIUpdatableWithDictionary conformance to it

@end

///////////////////////////////////////////////
#pragma mark -
#pragma mark UBIRunListOptions
#pragma mark -
///////////////////////////////////////////////

@implementation UBIRunListOptions

// conforming to UBIStringRepresentable protocol
- (NSString *)stringValue
{
    NSMutableString* resultString = [[NSMutableString alloc] init];
    
    if (self.limit && self.start) {
        [resultString appendFormat:@"\"%@\":{\"%@\":%lu,\"%@\":%lu}", UBIAPIRunListOptionsKey, UBIAPIRunListOptionsLimitKey, (unsigned long)self.limit.unsignedIntegerValue, UBIAPIRunListOptionsStartKey, (unsigned long)self.start.unsignedIntegerValue];
    } else {
        UBILogWarning(@"'limit' and/or 'start' are invalid - empty options string would be returned");
    }
    
    return [resultString copy];
}

// conforming to UBIUpdatableWithDictionary protocol
- (void)addValuesFromDictionary:(NSDictionary *)dict
{
    if ([dict[UBIAPIRunListOptionsLimitKey] isKindOfClass:[NSNumber class]]) {
        self.limit = dict[UBIAPIRunListOptionsLimitKey];
    }
 
    if ([dict[UBIAPIRunListOptionsStartKey] isKindOfClass:[NSNumber class]]) {
        self.start = dict[UBIAPIRunListOptionsStartKey];
    }
}

@end

///////////////////////////////////////////////
#pragma mark -
#pragma mark UBIRunList
#pragma mark -
///////////////////////////////////////////////

@implementation UBIRunList

- (instancetype)init {
    self = [super init];
    if (self) {
        _method = UBIRunListMethodNone;
        _options = [UBIRunListOptions new];
        _whereList = [UBIRunListWhereList new];
        _nativeDatasetFormat = NO;
    }
    return self;
}

// conforming to UBIStringRepresentable protocol
- (NSString *)stringValue
{
    // check preconditions
    if (self.entity == nil || self.method == UBIRunListMethodNone || self.fieldList.count == 0) {
        return nil;
    }
 
    NSMutableString* resultString= [[NSMutableString alloc] init];
    [resultString appendFormat:@"[{\"%@\":\"%@\",", UBIAPIRunListEntityKey, self.entity];
    
    NSString* strMethod = nil;
    switch (self.method) {
        case UBIRunListMethodSelect:
            strMethod = @"select";
            break;
        default:
            break;
    }
    if (strMethod) { [resultString appendFormat:@"\"%@\":\"%@\",", UBIAPIRunListMethodKey, strMethod]; }
    
    if (self.fieldList.count != 0) {
        [resultString appendFormat:@"\"%@\":[", UBIAPIRunListFieldListKey];
        for (NSString* aField in self.fieldList) {
            [resultString appendFormat:@"\"%@\",", aField];
        }
        [resultString deleteCharactersInRange:NSMakeRange([resultString length]-1, 1)];
        [resultString appendString:@"],"];
    }
    
    [resultString appendString:[self.options stringValue]];
    
    [resultString appendString:[self.whereList stringValue]];
    
    if (self.nativeDatasetFormat) {
        [resultString appendString:@",\"__nativeDatasetFormat\":true"];
    }
    
    [resultString appendString:@"}]"];
    
    return [resultString copy];
}

// conforming to UBIUpdatableWithDictionary protocol
- (void)addValuesFromDictionary:(NSDictionary *)dict
{
    self.entity = dict[UBIAPIRunListEntityKey];
    
    if ([dict[UBIAPIRunListMethodKey] isEqualToString:@"select"]) {
        self.method = UBIRunListMethodSelect;
    }
    
    if ([dict[UBIAPIRunListFieldListKey] isKindOfClass:[NSArray class]]) {
        self.fieldList = dict[UBIAPIRunListFieldListKey];
    }
    
    [self.options addValuesFromDictionary:dict[UBIAPIRunListOptionsKey]];
}

@end