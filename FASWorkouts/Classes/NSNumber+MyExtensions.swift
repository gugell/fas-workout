//
//  NSNumber+MyExtensions.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/11/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import Foundation

extension NSNumber
{
    var timeIntervalValue: NSTimeInterval {
        get {
            return (self.doubleValue as NSTimeInterval)
        }
        // TODO: write a setter
    }
}
