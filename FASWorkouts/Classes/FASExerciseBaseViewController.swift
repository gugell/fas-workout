//
//  FASExerciseBaseViewController.swift
//  FASWorkouts
//
//  Created by Vladimir Stepanchenko on 7/17/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit

class FASExerciseBaseViewController: FASViewController {
    
    // MARK: properties
    
    let kExerciseCelllIdentifier = "ExerciseCelllIdentifier"
    var workoutDownloadTracker: FASAttachDownloadProgreesTracker?
    
    var fetchedWorkoutsController: NSFetchedResultsController? = nil
    
    // determines, whether to show library by tapping on exercise cell
    let showExerciseLibrary: Bool = true
    
    @IBOutlet weak var tableView: UITableView!
    
    lazy var fetchedAllWorkoutsController: NSFetchedResultsController = {
        let frc: NSFetchedResultsController = FASData.sharedData().fetchedWorkoutsControllerWithCacheName(self.cacheName)
        frc.delegate = self
        return frc
        }()
    
    private lazy var fetchedExercisesController: NSFetchedResultsController? = {
        // TODO: this FRC will put one exercise per section
        let controller = FASData.sharedData().fetchedExercisesControllerWithCacheName(nil,
            context: nil,
            workout: nil)
        
        controller.delegate = self
        
        return controller
        }()
    private var cacheName: String? = nil
    private let actionController = FASActionController()
    ///////////////////////////////////////////////
    // MARK: - Init -
    ///////////////////////////////////////////////
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        automaticallyAdjustsScrollViewInsets = false
        actionController.viewController = self
    }
    ///////////////////////////////////////////////
    // MARK: - Actions -
    ///////////////////////////////////////////////
    
    func fetchWorkouts() {
        
        fetchedWorkoutsController?.delegate = nil
        fetchedWorkoutsController = nil
        
        fetchedWorkoutsController = fetchedAllWorkoutsController
        fetchedWorkoutsController!.delegate = self
        
        if let cn = cacheName {
            NSFetchedResultsController.deleteCacheWithName(cn)
        }
        
        if let frc = fetchedWorkoutsController {
            var fetchError: NSError?
            let result: Bool
            do {
                try frc.performFetch()
                result = true
            } catch let error as NSError {
                fetchError = error
                result = false
            }
            if !result {
                if let error = fetchError {
                    // TODO: Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    printlnDebugError("Unresolved error \(error.localizedDescription),\nerror info: \(error.userInfo)")
                    abort()
                }
            } else {
                if let fetchedObjects = frc.fetchedObjects {
                    for workout in fetchedObjects {
                        if let wrk = workout as? Workout {
                            FASData.sharedData().loadExercisesForWorkoutIfNeeded(wrk)
                        }
                    }
                }
            }
        } else {
            printlnDebugError("Fetched Result Controller was nil upon fetch()")
        }
    }
    
    @IBAction func startButtonDidPress(sender: AnyObject) { }
    
    override func configureView() {
        super.configureView()
        title = "EXERCISES BASE"
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar(.DarkWithYellowTitle)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        
        // Get both workouts and exercises (in case exercises have not been loaded)
        fetchWorkouts()
        fetchExercises()
        
        tableView.estimatedRowHeight = 61.0
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    ///////////////////////////////////////////////
    // MARK: - Layout -
    ///////////////////////////////////////////////
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    ///////////////////////////////////////////////
    // MARK: - Controls -
    ///////////////////////////////////////////////
    
    func fetchExercises() {
        if let cn = cacheName {
            NSFetchedResultsController.deleteCacheWithName(cn)
        }
        
        if let frc = fetchedExercisesController {
            var fetchError: NSError?
            let result: Bool
            do {
                try frc.performFetch()
                result = true
            } catch let error as NSError {
                fetchError = error
                result = false
            }
            if !result {
                if let error = fetchError {
                    // TODO: Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    printlnDebugError("Unresolved error \(error.localizedDescription),\nerror info: \(error.userInfo)")
                    abort()
                }
            }
        } else {
            printlnDebugError("Fetched Result Controller was nil upon fetch()")
        }
    }
}

///////////////////////////////////////////////
// MARK: - Fetched results controller -
///////////////////////////////////////////////

extension FASExerciseBaseViewController: NSFetchedResultsControllerDelegate {
    /*
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
    tableView.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
    switch type {
    case .Insert:
    tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Middle)
    case .Delete:
    tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Middle)
    default:
    printlnDebugTrace("doing nothing")
    }
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
    switch type {
    case .Insert:
    tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Middle)
    case .Delete:
    tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Middle)
    case .Update:
    let exCell: FASExerciseCell = self.tableView.cellForRowAtIndexPath(indexPath!) as! FASExerciseCell
    configureExerciseCell(exCell, atIndexPath: indexPath!)
    case .Move:
    tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation:.Middle)
    tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation:.Middle)
    }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
    tableView.endUpdates()
    
    configureBottomButton()
    }
    */
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        if controller == fetchedWorkoutsController {
            fetchExercises()
        } else if controller == fetchedExercisesController {
            tableView.reloadData()
        }
    }
}

///////////////////////////////////////////////
// MARK: - TableView Delegate -
///////////////////////////////////////////////

extension FASExerciseBaseViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.clearColor()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let obj: AnyObject? = fetchedExercisesController?.objectAtIndexPath(indexPath)
        if let ex = obj as? Exercise {
            printlnDebugTrace("exercise = \(ex.preferredAttachment)")
            // Display exercise info screen
            if self.showExerciseLibrary == true {
                let exerciseInfoVC = self.mainStoryboard.instantiateViewControllerWithIdentifier("FASExerciseInfoViewController")
                
                if let nonNilVC = exerciseInfoVC as? FASExerciseInfoViewController {
                    nonNilVC.exercise = ex
                    self.navigationController?.pushViewController(nonNilVC, animated: true)
                }
            }
        }
    }
}

///////////////////////////////////////////////
// MARK: - TableView DataSource -
///////////////////////////////////////////////

extension FASExerciseBaseViewController: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if let frc = fetchedExercisesController {
            if let sections = frc.sections {
                return sections.count
            }
        }
        return 0
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedExercisesController!.sections![section].numberOfObjects
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return self.tableView(tableView, exerciseCellForRowAtIndexPath: indexPath) as UITableViewCell
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        // TODO: should be in Extra section when FRC secion calculation is applied
        return 0.0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil;
    }
    
    // MARK: - Helpers
    
    private func configureExerciseCell(exerciseCell: FASExerciseCell, atIndexPath indexPath: NSIndexPath) {
        let exerciseObject = fetchedExercisesController!.objectAtIndexPath(indexPath) as! Exercise
        exerciseCell.accessoryType = showExerciseLibrary ? .DisclosureIndicator : .None
        exerciseCell.exercise = exerciseObject
    }
    
    private func tableView(tableView: UITableView, exerciseCellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let anExerciseCell = tableView.dequeueReusableCellWithIdentifier(kExerciseCelllIdentifier, forIndexPath: indexPath) as! FASExerciseCell
        
        configureExerciseCell(anExerciseCell, atIndexPath: indexPath)
        return anExerciseCell
    }
}
