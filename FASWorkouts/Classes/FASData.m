//
//  FASData.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/15/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "FASData.h"
#import "GCDSingleton.h"
#import "FASAPISession.h"
#import "FASRunLists.h"
#import "UBIResultData.h"
#import "FASAPIObjectKeys.h"
#import "UnityBase.h"
#import "FASWorkouts-Swift.h"
#import "NSString+MyExtensions.h"
#import "FASModifyDateProtocol.h"
#import "NSError+MyExtensions.h"
// extension for Remote and Local notifications
#import "UIApplication+FASUserNotification.h"
// license data
#import "FASLicenseAgreement.h"

#import "Workout.h"
#import "Attach.h"
#import "Exercise.h"
#import "ExerciseProgress.h"
#import "WorkoutHistory.h"
#import "FAQItem.h"
#import "StoreItem.h"

// thread-safe mutable map
#import "XJSafeMutableDictionary.h"

// to get and process global notifications
#import "FASAppDelegate.h"

// md5
#import "StringUtils.h"

// In-App
@import StoreKit;
#import "StoreObserver.h"
#import "StoreManager.h"
#import "StoreManager+FASProductStoreInfo.h"

// Application prefferences (NSUserDefaults wrapper)
#import "FASAppPreferences.h"

// GAI to implement tracking functions
#import <Google/Analytics.h>

// NSUserDefaults preferences keys
NSString * const FASDataVersionPrefKey                          = @"FASPrefVersion";
NSString * const FASDataBuildVersionPrefKey                     = @"FASPrefBuildVersion";
NSString * const FASDataSkipWelcomeScreenPrefKey                = @"FASPrefSkipWelcomeScreen";
NSString * const FASDataForceDownloadAttachFilesPrefKey         = @"FASPrefForceDownloadAttachFiles";
NSString * const FASDataPurchasedItemsPrefKey                   = @"FASDataPurchasedItemsPrefKey";

NSString * const FASDataCurrentWorkoutSessionPrefKey            = @"FASPrefCurrentWorkoutSession";

// Workout notification keys
NSString * const FASDataWorkoutsWillUpdateNotification          = @"FASDataWorkoutsWillUpdate";
NSString * const FASDataWorkoutsDidUpdateNotification           = @"FASDataWorkoutsDidUpdate";
NSString * const FASDataWorkoutsUpdateDidFailNotification       = @"FASDataWorkoutsUpdateDidFail";

// Exercises notification keys
NSString * const FASDataExercisesWillUpdateNotification         = @"FASDataExercisesWillUpdate";
NSString * const FASDataExercisesDidUpdateNotification          = @"FASDataExercisesDidUpdate";
NSString * const FASDataExercisesUpdateDidFailNotification      = @"FASDataExercisesUpdateDidFailUpdate";

NSString * const FASDataBestResultsDidUpdateNotification        = @"FASDataBestResultsDidUpdate";

// Workout history notification keys
NSString * const FASDataWorkoutHistoryWillUpdateNotification    = @"FASDataWorkoutHistoryWillUpdate";
NSString * const FASDataWorkoutHistoryDidUpdateNotification     = @"FASDataWorkoutHistoryDidUpdate";
NSString * const FASDataWorkoutHistoryUpdateDidFailNotification = @"FASDataWorkoutHistoryUpdateDidFail";

// In-App Product notification keys
NSString * const FASDataProductsDidUpdateNotification           = @"FASDataProductsDidUpdate";
NSString * const FASDataPurchasesRestoreDidUpdateNotification   = @"FASDataPurchasesDidUpdate";

// Store
NSString * const FASDataStoreItemsDidUpdateNotification         = @"FASDataStoreItemsDidUpdateNotification";
NSString * const FASDataStoreItemsUpdateDidFailNotification     = @"FASDataStoreItemsUpdateDidFailNotification";

// FAQ notifications
NSString * const FASDataFAQDidUpdateNotification                = @"FASDataFAQDidUpdate";
NSString * const FASDataFAQDidFailNotification                  = @"FASDataFAQDidFail";

// core data entities
NSString * const kFASDataCoreDataWorkoutEntityName              = @"Workout";
NSString * const kFASDataCoreDataWorkoutHistoryEntityName       = @"WorkoutHistory";
NSString * const kFASDataCoreDataExerciseEntityName             = @"Exercise";
NSString * const kFASDataCoreDataAttachEntityName               = @"Attach";
NSString * const kFASDataCoreDataExerciseProgressEntityName     = @"ExerciseProgress";
NSString * const kFASDataCoreDataFAQEntityName                  = @"FAQItem";
NSString * const kFASDataCoreDataStoreItemEntityName            = @"StoreItem";

// local notification identifiers
NSString * const kFASDataWorkoutReminderLocalNotificationID     = @"workoutReminder";

static NSOperationQueue* sFASExerciseOpQueue;

//typedef NS_ENUM(NSUInteger, FASDataUITag) {
//    FASDataUI... = 2000,
//};
@interface  FASData (UIAlertViewDelegate) <UIAlertViewDelegate>
@end

@interface FASData()
<StoreObserverNotificationsObserver>
@property (strong, nonatomic) NSManagedObjectContext *mainContext;
@property (strong, nonatomic) NSManagedObjectContext *workerContext;
@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

// network activity counter (used to show/hide network activity indicator)
@property (nonatomic) NSUInteger networkActivityCount;
// GAI label with version, userID info etc.
@property (nonatomic, copy) NSString *trackerLabel;

// TODO: annotate the array

@property (nonatomic, strong) XJSafeMutableDictionary *purchasedItems;

@property (nonatomic, strong, readwrite, nullable) FASWorkoutSession* currentWorkoutSession;

@property (nonatomic, strong, readwrite, nullable) NSArray<NSDictionary<NSString *, id>*> *bestResults;

@property (nonatomic, readwrite, getter=isRestoringProducts) BOOL restoringProducts;

@end

@implementation FASData
///////////////////////////////////////////////
#pragma mark -
#pragma mark Initialization
#pragma mark -
///////////////////////////////////////////////
+ (void)initialize
{
    if (!sFASExerciseOpQueue) {
        sFASExerciseOpQueue = [NSOperationQueue new];
    }
}

+ (instancetype)sharedData
{
    DEFINE_SHARED_INSTANCE_USING_BLOCK(^{
        return [self new];
    });
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self configureDeviceCapabilities];
        
        _networkActivityCount = 0;
        
        _purchasedItems = [XJSafeMutableDictionary dictionary];
        _deferredTask = FASDataDeferredNone;
        _requiredNotificationType = (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert);
        
        _restoringProducts = NO;
        
        (void)[self mainContext];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handlePreferencesDidChangeNotification:)
                                                     name:PAPreferencesDidChangeNotification
                                                   object:[FASAppPreferences sharedInstance]];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(appDelegateNotification:)
                                                     name:FASAppDelegateDidRegisterUserNotificationSettingsNotification
                                                   object:nil];
    }
    return self;
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Session
#pragma mark -
///////////////////////////////////////////////

// load data saved from the last session
- (void)restore
{
    // restore device cache
    // purchased items, unfinished workout sessions are restored here
    [self loadDataTypeFromCache:FASDataCachePurchases];
    [self loadDataTypeFromCache:FASDataCacheWorkoutProgress];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleStoreManagerDidChangeStatusNotification:)
                                                 name:StoreManagerDidChangeStatusNotification
                                               object:[StoreManager sharedInstance]];
    

    [StoreObserver.sharedInstance addStoreObserverNotificationsObserver:self];
    
    // observe session
    [FASAPISession.currentSession addObserver:self];
}

// update session data based on loggedIn status
- (void)updateSessionData
{
    if ([FASAPISession.currentSession isLoggedIn]) {
        // load user info
        [FASAPISession.currentSession updateSessionData];
        
        // load workouts from remote
        [self loadWorkoutsFromRemote];
    }
    
    else {
        // remove all data & files
        UBILogTrace(@"Clear session data because user is NOT logged in");
        [self clearSessionData];
    }
}

- (void)clearSessionData
{
    [self clearPreferences];
    
    [self.purchasedItems removeAllObjects];
    [self saveCacheForDataType:FASDataCachePurchases];
    
    [self clearCurrentWorkoutSession];
    
    NSManagedObjectContext *c = self.workerContext;
    BOOL didImport = [self importWorkoutsFromResponse:nil
                                 shouldReplaceExisting:YES
                                             inContext:c];
    [NSNotificationCenter.defaultCenter
     postNotificationName:FASDataWorkoutsDidUpdateNotification
     object:self
     userInfo:@{@"didImport":@(didImport)}];
    
    // TODO: remove file cache folder
}

- (void)clearPreferences
{
    self.skipWelcomeScreen = NO;
    self.forceDownloadAttachFiles = NO;
    
    [[FASAppPreferences sharedInstance] resetToDefaults];
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Core Data Stack
#pragma mark -
///////////////////////////////////////////////
- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.fas-sport.FASWorkouts" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSString *)applicationCacheDirectory {
    return [[[NSFileManager defaultManager] URLForDirectory:NSCachesDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:nil] path];
}

- (NSManagedObjectContext *)mainContext
{
    if (!_mainContext) {
        _mainContext = [[NSManagedObjectContext alloc]
                        initWithConcurrencyType:NSMainQueueConcurrencyType];
        NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
        if (!coordinator) {
            return nil;
        }
        _mainContext.persistentStoreCoordinator = coordinator;
        [self observeSaveNotificationsForContext:_mainContext];
    }
    return _mainContext;
}

- (NSManagedObjectContext *)workerContext
{
    if (!_workerContext) {
        _workerContext = [[NSManagedObjectContext alloc]
                          initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
        if (!coordinator) {
            return nil;
        }
        _workerContext.persistentStoreCoordinator = coordinator;
        [self observeSaveNotificationsForContext:_workerContext];
    }
    return _workerContext;
}

- (NSManagedObjectContext *)newUIContext
{
    NSManagedObjectContext *c = [[NSManagedObjectContext alloc]
                                 initWithConcurrencyType:NSMainQueueConcurrencyType];
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    c.persistentStoreCoordinator = coordinator;
    return c;
}

- (NSManagedObjectContext *)newWorkerContext
{
    NSManagedObjectContext *c = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSConfinementConcurrencyType];
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    c.persistentStoreCoordinator = coordinator;
    return c;
}

- (void)observeSaveNotificationsForContext:(NSManagedObjectContext *)context
{
    [NSNotificationCenter.defaultCenter addObserver:self
                                           selector:@selector(contextDidSave:)
                                               name:NSManagedObjectContextDidSaveNotification
                                             object:context];
}

- (void)stopObservingSaveNotificationsForContext:(NSManagedObjectContext *)context
{
    [NSNotificationCenter.defaultCenter removeObserver:self
                                                  name:NSManagedObjectContextDidSaveNotification
                                                object:context];
}

- (void)contextDidSave:(NSNotification *)notification
{
    NSParameterAssert(notification.name == NSManagedObjectContextDidSaveNotification);
    
    NSManagedObjectContext *context = notification.object;
    
    BOOL mergeChangesToMainContext = context != self.mainContext;
    BOOL mergeChangesToWorkerContext = context != self.workerContext;
    
    if (mergeChangesToMainContext) {
        NSManagedObjectContext *mainContext = self.mainContext;
        [mainContext performBlockAndWait:^{
            [mainContext
             mergeChangesFromContextDidSaveNotification:notification];
        }];
    }
    
    if (mergeChangesToWorkerContext) {
        NSManagedObjectContext *workerContext = self.workerContext;
        [workerContext performBlockAndWait:^{
            [workerContext
             mergeChangesFromContextDidSaveNotification:notification];
        }];
    }
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (!_managedObjectModel) {
        NSURL *modelURL = [NSBundle.mainBundle URLForResource:@"FASWorkouts" withExtension:@"momd"];
        _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    }
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (!_persistentStoreCoordinator) {
        
        _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];
        
        NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"FASWorkouts.sqlite"];
        
        // add SQLite store to coordinator
        NSError *error;
        
        // set options to use Core Data automatic lightweight migration
        NSDictionary *options = @{
                                  NSMigratePersistentStoresAutomaticallyOption : @YES,
                                  NSInferMappingModelAutomaticallyOption : @YES
                                  };
        
        NSPersistentStore *sqlStore = [_persistentStoreCoordinator
                                       addPersistentStoreWithType:NSSQLiteStoreType
                                       configuration:nil
                                       URL:storeURL
                                       options:options
                                       error:&error];
        if (!sqlStore) {
//            NSAssert2(!sqlStore, @"[ASSERTION]: Cannot create Persistent Store. Error\n%@\nError Description: %@", error, [error userInfo]);
            
            // TODO: handle error, the app cannot function without a store (e.g. FASAPIErrorCodeUpgradeRequired)
            
            // Report any error we got.
            NSString *failureReason = @"There was an error creating or loading the application's saved data.";
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
            dict[NSLocalizedFailureReasonErrorKey] = failureReason;
            dict[NSUnderlyingErrorKey] = error;
            error = [NSError errorWithDomain:FASAPIErrorDomain code:FASAPIErrorCodeUpgradeRequired userInfo:dict];
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            UBILogError(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
    return _persistentStoreCoordinator;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Device capabilities
#pragma mark -
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)configureDeviceCapabilities
{
    _isIPAD = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
    _isIPHONE = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone;
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Fetched Result Controller(s)
#pragma mark -
///////////////////////////////////////////////

// Workouts FRCs

- (NSFetchedResultsController *)fetchedWorkoutsControllerWithCacheName:(NSString * _Nullable)cacheName
                                                             predicate:(NSPredicate * _Nullable)predicate
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:kFASDataCoreDataWorkoutEntityName inManagedObjectContext:self.mainContext];
    [fetchRequest setEntity:entity];
    
    if (predicate) {
        fetchRequest.predicate = predicate;
    }

    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    UBINullableSortDescriptor *sdSortIndex = [[UBINullableSortDescriptor alloc] initWithKey:@"sortIndex" ascending:YES];
    NSSortDescriptor *sdModifyDate = [[NSSortDescriptor alloc] initWithKey:@"mi_modifyDate" ascending:NO];
    NSArray *sortDescriptors = @[sdSortIndex, sdModifyDate];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                               managedObjectContext:self.mainContext
                                                 sectionNameKeyPath:nil
                                                          cacheName:cacheName];
}

- (NSFetchedResultsController *)fetchedWorkoutsControllerWithKeyValueFilter:(NSDictionary <NSString *, id>*)filter
                                                                  cacheName:(NSString * _Nullable)cacheName
{
    __block NSPredicate *compoundPredicate = nil;
    @autoreleasepool {
        NSMutableArray* predicates = [NSMutableArray array];
        NSArray* allKeys = filter.allKeys;
        for (NSString* key in allKeys) {
            @autoreleasepool {
                id someObj = filter[key];
                if ([someObj isKindOfClass:[NSString class]]) {
                    NSString* stringFilterPattern = @"%K == %@";
                    NSPredicate* aPredicate = [NSPredicate predicateWithFormat:stringFilterPattern, key, (NSString *)someObj];
                    [predicates addObject:aPredicate];
                }
                else {
                    UBILogTrace(@"unsupported value type: %@", NSStringFromClass([someObj class])) ;
                }
            }
        }
        
        compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    }
    
    return [self fetchedWorkoutsControllerWithCacheName:cacheName
                                              predicate:compoundPredicate];
}

// all workouts FRC

- (NSFetchedResultsController *)fetchedWorkoutsControllerWithCacheName:(NSString * _Nullable)cacheName
{
    return [self fetchedWorkoutsControllerWithCacheName:cacheName predicate:nil];
}

// FAQ FRCs

- (NSFetchedResultsController *)fetchedFAQControllerWithCacheName:(NSString * _Nullable)cacheName
{
    return [self fetchedFAQControllerWithCacheName:cacheName predicate:nil];
}

- (NSFetchedResultsController *)fetchedFAQControllerWithCacheName:(NSString * _Nullable)cacheName
                                                        predicate:(NSPredicate * _Nullable)predicate
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:kFASDataCoreDataFAQEntityName inManagedObjectContext:self.mainContext];
    [fetchRequest setEntity:entity];
    
    if (predicate) {
        fetchRequest.predicate = predicate;
    }
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    //@note change priority here if has to be reversed
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortIndex" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                               managedObjectContext:self.mainContext
                                                 sectionNameKeyPath:nil
                                                          cacheName:cacheName];
}

// Workout history FRCs

- (NSFetchedResultsController *)fetchedWorkoutHistoryControllerWithCacheName:(NSString * _Nullable)cacheName
{
    return [self fetchedWorkoutHistoryControllerWithCacheName:cacheName predicate:nil];
}

- (NSFetchedResultsController *)fetchedWorkoutHistoryControllerWithCacheName:(NSString * _Nullable)cacheName
                                                             predicate:(NSPredicate * _Nullable)predicate
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:kFASDataCoreDataWorkoutHistoryEntityName inManagedObjectContext:self.mainContext];
    [fetchRequest setEntity:entity];
    
    if (predicate) {
        fetchRequest.predicate = predicate;
    }
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"startDate" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                               managedObjectContext:self.mainContext
                                                 sectionNameKeyPath:nil
                                                          cacheName:cacheName];
}

// Exercises FRCs

- (NSFetchedResultsController *)fetchedExercisesControllerWithCacheName:(NSString * _Nullable)cacheName
                                                                context:(NSManagedObjectContext * _Nullable)context
                                                              predicate:(NSPredicate * _Nullable)predicate
                                                            useSections:(BOOL)useSections
                                     relationshipKeyPathsForPrefetching:(NSArray * _Nullable)prefetchingKeys
{
    NSManagedObjectContext* frcContext = (context != nil ? context : self.mainContext);
    NSString *sectionNameKeyPath = nil;
    NSMutableArray *sortDescriptors = [NSMutableArray array];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription * entity = [NSEntityDescription entityForName:kFASDataCoreDataExerciseEntityName
                                               inManagedObjectContext:frcContext];
    fetchRequest.entity = entity;
    
    if (useSections) {
        sectionNameKeyPath = @"workoutExerciseID";
    }
    
    if (prefetchingKeys != nil && prefetchingKeys.count != 0) {
        fetchRequest.relationshipKeyPathsForPrefetching = prefetchingKeys;
    }
    
    if (predicate) {
        fetchRequest.predicate = predicate;
    }
    
    UBINullableSortDescriptor *nullableSortDescriptor = [[UBINullableSortDescriptor alloc] initWithKey:@"sortIndex" ascending:YES];
    [sortDescriptors addObject:nullableSortDescriptor];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                               managedObjectContext:frcContext
                                                 sectionNameKeyPath:sectionNameKeyPath
                                                          cacheName:cacheName];
}

- (NSFetchedResultsController *)fetchedExercisesControllerWithCacheName:(NSString * _Nullable)cacheName
                                                                context:(NSManagedObjectContext * _Nullable)context
                                                                workout:(Workout * _Nullable)workout
{
    NSPredicate* predicate = nil;
    if (workout != nil) {
        predicate = [NSPredicate predicateWithFormat:@"workout = %@", workout];
    }
    
    NSArray *prefetchingKeys = @[@"preferredAttachment"];
    
    return [self fetchedExercisesControllerWithCacheName:cacheName
                                                 context:context
                                               predicate:predicate
                                             useSections:YES
                      relationshipKeyPathsForPrefetching:prefetchingKeys];
}

- (NSFetchedResultsController *)fetchedStoreItemsControllerWithCacheName:(NSString * _Nullable)cacheName
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"productType = %@", @"workoutBulk"];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:kFASDataCoreDataStoreItemEntityName inManagedObjectContext:self.mainContext];
    [fetchRequest setEntity:entity];
    
    UBINullableSortDescriptor *sdSortIndex = [[UBINullableSortDescriptor alloc] initWithKey:@"sortIndex" ascending:YES];

    NSArray *sortDescriptors = @[sdSortIndex];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    return [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                               managedObjectContext:self.mainContext
                                                 sectionNameKeyPath:nil
                                                          cacheName:cacheName];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Application Preferences
#pragma mark -
///////////////////////////////////////////////
- (NSString *)appVersion
{
    return [NSUserDefaults.standardUserDefaults
            objectForKey:FASDataVersionPrefKey];
}

- (NSString *)buildVersion
{
    return [NSUserDefaults.standardUserDefaults
            objectForKey:FASDataBuildVersionPrefKey];
}

- (void)setAppVersionFromBundle
{
    // set current versions from bundle
    [NSUserDefaults.standardUserDefaults
     setObject:[[NSBundle.mainBundle infoDictionary]
                objectForKey:@"CFBundleShortVersionString"]
     forKey:FASDataVersionPrefKey];
    [NSUserDefaults.standardUserDefaults
     setObject:[[NSBundle.mainBundle infoDictionary]
                objectForKey:@"CFBundleVersion"]
     forKey:FASDataBuildVersionPrefKey];
}

- (BOOL)skipWelcomeScreen
{
    BOOL shouldSkip = NO;
    
    NSUserDefaults *defaults = NSUserDefaults.standardUserDefaults;
    if ([defaults objectForKey:FASDataSkipWelcomeScreenPrefKey]) {
        shouldSkip = [[defaults objectForKey:FASDataSkipWelcomeScreenPrefKey] boolValue];
    }
    
    return shouldSkip;
}

- (void)setSkipWelcomeScreen:(BOOL)shouldSkip
{
    NSUserDefaults *defaults = NSUserDefaults.standardUserDefaults;
    [defaults setObject:@(shouldSkip) forKey:FASDataSkipWelcomeScreenPrefKey];
    [defaults synchronize];
}

- (BOOL)forceDownloadAttachFiles
{
    BOOL force = NO;
    
    NSUserDefaults *defaults = NSUserDefaults.standardUserDefaults;
    if ([defaults objectForKey:FASDataForceDownloadAttachFilesPrefKey]) {
        force = [[defaults objectForKey:FASDataForceDownloadAttachFilesPrefKey] boolValue];
    }
    
    return force;
}

- (void)setForceDownloadAttachFiles:(BOOL)force
{
    NSUserDefaults *defaults = NSUserDefaults.standardUserDefaults;
    [defaults setObject:@(force) forKey:FASDataForceDownloadAttachFilesPrefKey];
    [defaults synchronize];
}

- (void)handlePreferencesDidChangeNotification:(NSNotification *)notification
{
    UBILogTrace(@"property name: \"%@\"", notification.userInfo[PAPreferencesChangedPropertyKey]);
    
    NSDictionary* userInfo = notification.userInfo;
    NSString* propertyName = userInfo[PAPreferencesChangedPropertyKey];
    if (!propertyName || [propertyName isBlank]) {
        return;
    }
    
    if ([propertyName isEqualToString:@"notificationsLocalEnabledByUser"]
        || [propertyName isEqualToString:@"notificationsLocalDate"]) {
        
        // local notifications settings pair
        if ([FASAppPreferences sharedInstance].notificationsLocalEnabledByUser) {
            NSDate *dateToFire = [FASAppPreferences sharedInstance].notificationsLocalDate;
            [self scheduleLocalRepeatedNotificationWithID:kFASDataWorkoutReminderLocalNotificationID remindDate:dateToFire];
        }
        else {
            [self cancelPreviouslyScheduledRepeatedNotificationWithID:kFASDataWorkoutReminderLocalNotificationID];
        }
    }
    
    else if ([propertyName isEqualToString:@"notificationsRemotePromoEnabled"]) {
        BOOL currentValue = [FASAppPreferences sharedInstance].notificationsRemotePromoEnabled;
        
        if (currentValue && ![self hasPermissionToReceiveRemoteNotifications]) {
            // TODO: notify UI to show a pop-up to prepare user for the permission to be asked.
            // For now, just ask immediatelly, and wait afterward
            [self registerPermissionToReceiveRemoteNotifications];
        }
        
        if ([[FASAPISession currentSession] isLoggedIn]) {
            BOOL receiveNewsValue = [FASAppPreferences sharedInstance].notificationsRemotePromoEnabled;
            FASAPI* api = [FASAPI setUserInfoKey:FASAPISettingReceiveNewsKey
                                           value:@(receiveNewsValue)
                                        delegate:self];
            [api invoke];
        }
    }
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark FASAPISessionObserver protocol
#pragma mark -
///////////////////////////////////////////////
- (void)FASAPISessionNotification:(NSNotification *)notification
{
    if (notification.name == FASAPISessionDidRestoreSessionNotification ||
        notification.name == FASAPISessionDidLogoutNotification ||
        notification.name == FASAPISessionDidNotLoginNotification ||
        notification.name == FASAPISessionDidLoginNotification /*||
        notification.name == FASAPISessionDidCancelLoginNotification*/)
    {
        [self updateSessionData];
    }
    
    // special case for log out
    if (notification.name == FASAPISessionDidLogoutNotification) {
        // reset VC hierarchy upon log out
        FASAppDelegate* appDelegate = (FASAppDelegate *)[UIApplication sharedApplication].delegate;
        [appDelegate setupViewControllers];
    }
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Workouts Data
#pragma mark -
///////////////////////////////////////////////

// IF ANY workout object is loaded 'long time ago'
// THEN return YES
// 'long time ago' == yesterday
- (BOOL)isWorkoutDataLoadedLongTimeAgo
{
    __block BOOL result = NO;
    NSManagedObjectContext* c = self.workerContext;
    [c performBlockAndWait:^{
        NSEntityDescription *entityDescription = [NSEntityDescription
                                                  entityForName:kFASDataCoreDataWorkoutEntityName
                                                  inManagedObjectContext:c];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        
        NSDate* dateToCompare = [NSDate yesterday];
        
        NSPredicate *olderThanDatePredicate = [NSPredicate predicateWithFormat:@"createdOn > %@", dateToCompare];
        [request setPredicate:olderThanDatePredicate];
        
        NSError *error;
        NSArray *array = [c executeFetchRequest:request error:&error];
        if (array == nil)
        {
            UBILogError(@"Error during fetching workout to find out how old dataset is. Error was:\n%@", error.localizedDescription);
        } else {
            if (array.count == 0) {
                result = YES;
            }
        }
    }];
    
    return result;
}

- (void)loadWorkoutsFromRemoteIfNeeded
{
    // uncomment to refresh workouts each day
//    BOOL shouldLoad = [self isWorkoutDataLoadedLongTimeAgo];
//    if (shouldLoad) {
//        [self loadWorkoutsFromRemote];
//    }
    
    // before loading workouts, find out
    // is anything gets changed on the server
    [self loadModifyDateForWorkoutsExcludeIncomplete];
}

- (void)loadModifyDateForWorkouts {
    FASWorkoutsRunList* workoutsModifyDateRunList = [FASWorkoutsRunList workoutsRunListWithModifyDate];
    FASAPI* api = [FASAPI getWorkoutsModifyDateWithRunList:workoutsModifyDateRunList delegate:self];
    [api invoke];
}

- (void)loadWorkoutsFromRemote
{
    FASAPI* api = [FASAPI getWorkoutsWithDelegate:self];
    [api invoke];
}

- (void)loadModifyDateForWorkoutsExcludeIncomplete {
    FASWorkoutsRunList* workoutsModifyDateRunList = [FASWorkoutsRunList workoutsRunListExcludeIncompleteWithModifyDate];
    FASAPI* api = [FASAPI getWorkoutsModifyDateWithRunList:workoutsModifyDateRunList delegate:self];
    [api invoke];
}

- (void)loadWorkoutHistoryFromRemote
{
    FASWorkoutHistoryRunList* runList = [FASWorkoutHistoryRunList workoutHistoryRunListDefault];
    FASAPI* api = [FASAPI getWorkoutHistoryWithRunList:runList delegate:self];
    [api invoke];
}

- (void)loadFAQFromRemote
{
    FASFAQRunList* runList = [FASFAQRunList faqRunListDefault];
    FASAPI* api = [FASAPI getFAQWithRunList:runList delegate:self];
    [api invoke];
}

- (BOOL)importWorkoutsFromResponse:(NSDictionary *)response
             shouldReplaceExisting:(BOOL)shouldReplaceExisting
                         inContext:(NSManagedObjectContext *)context
{
    NSParameterAssert(shouldReplaceExisting == YES); // unexpected behaviour in other case
    NSParameterAssert(context);
    
    __block NSMutableArray *remoteImagesToRemove = [NSMutableArray array];
    
    __block BOOL didImport = NO;
    
    [context performBlockAndWait:^{
        
        // if shouldReplaceExisting, delete existing Workouts from context
        if (shouldReplaceExisting) {
            NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:kFASDataCoreDataWorkoutEntityName];
            NSError *error;
            NSArray *workouts = [context executeFetchRequest:request error:&error];
            for (Workout *workout in workouts) {
                if (workout.remoteImageKey) {
                    [remoteImagesToRemove addObject:workout.remoteImageKey];
                }
                [context deleteObject:workout];
            }
        }

        NSArray* workoutDictionaries = response[FASAPIResultKey];
        
        NSMutableArray *addedProductIDs = [NSMutableArray array];
        
        NSArray *addedWorkouts = [self arrayOfWorkoutsAddedFromDictionaryRepresentations:workoutDictionaries
                                                                               inContext:context
                                                                         addedProductIDs:addedProductIDs];
        
        // post-process workout
        
        // do not delete images for workouts, existed before
        // BUG: when someone updates image for workout the app would not receive the update. Possible fix is when we have modifyDate feature working, we could remove images for particular workouts that got updated since last load.
        for (Workout* wrk in addedWorkouts) {
            [remoteImagesToRemove removeObject:wrk.remoteImageKey];
            [wrk.remoteImage image];
            
            [self updateProductPurchaseStatus:wrk];
        }
        
        // get permanent object IDs in the store
//        [context obtainPermanentIDsForObjects:addedWorkouts
//                                        error:nil];
        
        // uncomment to obtain SKProduct info, when required
//        if (addedProductIDs.count != 0) {
//            [self fetchProductInformationForIdentifiers:[addedProductIDs copy]];
//        }
        
        NSError *error;
        if ([context hasChanges]) {
            didImport = [context save:&error];
        }
    }];

    // delete leftover image files for deleted workouts
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (NSString *imageKey in remoteImagesToRemove) {
            [Workout.workoutImageCache deleteRemoteImageForKey:imageKey];
        }
    });
    
    return didImport;
}

- (BOOL)importWorkoutHistoryFromResponse:(NSDictionary *)response inContext:(NSManagedObjectContext *)context
{
    NSParameterAssert(context);
    
    __block BOOL didImport = NO;
    
    [context performBlockAndWait:^{
        
        // if shouldReplaceExisting, delete existing Workouts from context
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:kFASDataCoreDataWorkoutHistoryEntityName];
        NSError *error;
        NSArray *workoutHistoryArr = [context executeFetchRequest:request error:&error];
        for (WorkoutHistory *history in workoutHistoryArr) {
            [context deleteObject:history];
        }
        
        UBIResultData* resultData = [UBIResultData new];
        [resultData addValuesFromDictionary:response[FASAPIResultDataKey]];
        NSArray* workoutHistoryDicts = [resultData arrayOfDictionaryData];
        
        // Core data write operations
        [self arrayOfWorkoutHistoryAddedFromDictionaryRepresentations:workoutHistoryDicts inContext:context];
        
        if ([context hasChanges]) {
            didImport = [context save:&error];
        }
    }];
    
    // Here should be some conditions for this
    return didImport;
}

- (BOOL)importFAQEntriesFromResponse:(NSDictionary *)response inContext:(NSManagedObjectContext *)context
{
    NSParameterAssert(context);
    
    __block BOOL didImport = NO;
    
    [context performBlockAndWait:^{
        
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:kFASDataCoreDataFAQEntityName];
        NSError *error;
        NSArray *faqArray = [context executeFetchRequest:request error:&error];
        for (FAQItem *faqEntry in faqArray) {
            [context deleteObject:faqEntry];
        }
        
        UBIResultData* resultData = [UBIResultData new];
        [resultData addValuesFromDictionary:response[FASAPIResultDataKey]];
        NSArray *faqDicts = [resultData arrayOfDictionaryData];
        
        // Core data write operations
        [self arrayOfFaqEntriesFromDictionaryRepresentations:faqDicts inContext:context];
        
        if ([context hasChanges]) {
            didImport = [context save:&error];
        }
    }];
    
    return didImport;
}

- (NSArray *)arrayOfWorkoutsAddedFromDictionaryRepresentations:(NSArray *)workouts
                                                     inContext:(NSManagedObjectContext* )context
                                               addedProductIDs:(NSMutableArray *)addedProductIDs
{
    UBILogTrace(@"workouts count = %lu", (unsigned long)workouts.count);
    
    NSMutableArray *addedWorkouts = [NSMutableArray array];
    
    for (NSDictionary *workoutDictionary in workouts) {
        @autoreleasepool {
            Workout *workout = (Workout *)[NSEntityDescription insertNewObjectForEntityForName:kFASDataCoreDataWorkoutEntityName
                                                                        inManagedObjectContext:context];
            [workout addValuesFromDictionary:workoutDictionary];
            
            // add additional values that cannot be obtained from server response
            workout.baseImageFilename = [NSString stringWithFormat:@"%@", workout.workout_id];
            
            if (workout.inAppProductID != nil) {
                [addedProductIDs addObject:workout.inAppProductID];
            }
            
            [addedWorkouts addObject:workout];
        }
    }
    
    return addedWorkouts;
}

- (NSArray *)arrayOfWorkoutHistoryAddedFromDictionaryRepresentations:(NSArray *)workoutHistoryArr
                                                     inContext:(NSManagedObjectContext* )context
{
    UBILogTrace(@"history elements' count = %lu", (unsigned long)workoutHistoryArr.count);
    
    NSMutableArray *addedWorkoutHistory = [NSMutableArray array];
    
    for (NSDictionary *workoutHistoryDict in workoutHistoryArr) {
        @autoreleasepool {
            WorkoutHistory *cdWorkoutHistory = (WorkoutHistory *)[NSEntityDescription insertNewObjectForEntityForName:kFASDataCoreDataWorkoutHistoryEntityName
                                                                        inManagedObjectContext:context];
            [cdWorkoutHistory addValuesFromDictionary:workoutHistoryDict];
            
            // Tie to workout
            NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:kFASDataCoreDataWorkoutEntityName];
            request.predicate = [NSPredicate predicateWithFormat:@"workout_id = %@", cdWorkoutHistory.workout_id];
            NSError *error;
            NSArray *objects = [context executeFetchRequest:request error:&error];
            if (objects.count > 0) {
                Workout *foundWorkout = (Workout *)objects.firstObject;
                cdWorkoutHistory.workout = foundWorkout;
                if (![context save:&error]) {
                    UBILogError(@"workout history fail:\n%@\nerror\n%@", cdWorkoutHistory, error.localizedDescription);
                }
            }
            
            [addedWorkoutHistory addObject:cdWorkoutHistory];
        }
    }
    
    return addedWorkoutHistory;
}

- (NSArray *)arrayOfFaqEntriesFromDictionaryRepresentations:(NSArray *)faqEntriesArray
                                                           inContext:(NSManagedObjectContext* )context
{
    NSMutableArray *addedFaqEntries = [NSMutableArray array];
    
    for (NSDictionary *faqDict in faqEntriesArray) {
        @autoreleasepool {
            FAQItem *newFaq = (FAQItem *)[NSEntityDescription insertNewObjectForEntityForName:kFASDataCoreDataFAQEntityName
                                                                                               inManagedObjectContext:context];
            [newFaq addValuesFromDictionary:faqDict];
        }
    }
    
    return addedFaqEntries;
}

- (void)addWorkoutsObserver:(id <FASDataObserver>)observer
{
    [NSNotificationCenter.defaultCenter addObserver:observer selector:@selector(fasDataNotification:) name:FASDataWorkoutsWillUpdateNotification object:nil];
    [NSNotificationCenter.defaultCenter addObserver:observer selector:@selector(fasDataNotification:) name:FASDataWorkoutsDidUpdateNotification object:nil];
    [NSNotificationCenter.defaultCenter addObserver:observer selector:@selector(fasDataNotification:) name:FASDataWorkoutsUpdateDidFailNotification object:nil];
}

- (void)removeWorkoutsObserver:(id <FASDataObserver>)observer
{
    [NSNotificationCenter.defaultCenter removeObserver:observer name:FASDataWorkoutsWillUpdateNotification object:nil];
    [NSNotificationCenter.defaultCenter removeObserver:observer name:FASDataWorkoutsDidUpdateNotification object:nil];
    [NSNotificationCenter.defaultCenter removeObserver:observer name:FASDataWorkoutsUpdateDidFailNotification object:nil];
}

- (void)addWorkoutHistoryObserver:(id <FASDataObserver>)observer
{
    [NSNotificationCenter.defaultCenter addObserver:observer selector:@selector(fasDataNotification:) name:FASDataWorkoutHistoryWillUpdateNotification object:nil];
    [NSNotificationCenter.defaultCenter addObserver:observer selector:@selector(fasDataNotification:) name:FASDataWorkoutHistoryDidUpdateNotification object:nil];
    [NSNotificationCenter.defaultCenter addObserver:observer selector:@selector(fasDataNotification:) name:FASDataWorkoutHistoryUpdateDidFailNotification object:nil];
}

- (void)removeWorkoutHistoryObserver:(id <FASDataObserver>)observer
{
    [NSNotificationCenter.defaultCenter removeObserver:observer name:FASDataWorkoutHistoryWillUpdateNotification object:nil];
    [NSNotificationCenter.defaultCenter removeObserver:observer name:FASDataWorkoutHistoryDidUpdateNotification object:nil];
    [NSNotificationCenter.defaultCenter removeObserver:observer name:FASDataWorkoutHistoryUpdateDidFailNotification object:nil];
}

- (void)addFAQObserver:(id <FASDataObserver>)observer
{
    [NSNotificationCenter.defaultCenter addObserver:observer selector:@selector(fasDataNotification:) name:FASDataFAQDidUpdateNotification object:nil];
    [NSNotificationCenter.defaultCenter addObserver:observer selector:@selector(fasDataNotification:) name:FASDataFAQDidFailNotification object:nil];
}

- (void)removeFAQObserver:(id <FASDataObserver>)observer
{
    [NSNotificationCenter.defaultCenter removeObserver:observer name:FASDataFAQDidUpdateNotification object:nil];
    [NSNotificationCenter.defaultCenter removeObserver:observer name:FASDataFAQDidFailNotification object:nil];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Exercise Data
#pragma mark -
///////////////////////////////////////////////
- (BOOL)hasExercisesForWorkout:(Workout *)workout
{
    NSManagedObjectContext* c = self.workerContext;
    
    __block BOOL result = NO;
    
    [c performBlockAndWait:^{
        
        NSError* existingObjectWithIDError = nil;
        NSManagedObject* wrk = [c existingObjectWithID:workout.objectID
                                                 error:&existingObjectWithIDError];
        if (wrk) {
            NSEntityDescription *entityDescription = [NSEntityDescription
                                                      entityForName:kFASDataCoreDataExerciseEntityName
                                                      inManagedObjectContext:c];
            NSFetchRequest *request = [[NSFetchRequest alloc] init];
            [request setEntity:entityDescription];
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"workout = %@", wrk]; // TODO: beter to search for workout_id in exercise
            [request setPredicate:predicate];
            
            NSError *error;
            NSArray *array = [c executeFetchRequest:request error:&error];
            if (array == nil)
            {
                UBILogError(@"Error during fetching exercises for workout. Error was:\n%@", error.localizedDescription);
            } else if (array.count != 0) {
                result = YES;
            }
        } else {
            UBILogError(@"Cannot get workout in context. Error was: %@", existingObjectWithIDError.localizedDescription);
        }
    }];
    
    return result;
}
- (void)loadExercisesForWorkoutIfNeeded:(Workout *)workout
{
    if (!workout) {
        UBILogError("workout object is nil, function will return");
        return;
    }
    
    // assuming that there is no need to update exercises if they are already loaded
    // TODO: modifyDate logic has to be added here
    if (![self hasExercisesForWorkout:workout]) {
        [self loadExercisesForWorkout:workout];
    }
}

- (void)loadExercisesForWorkout:(Workout *)workout
{
    if (workout == nil) {
        UBILogError("workout object is nil, function will return");
        return;
    }
    
    FASExercisesRunList* runList = [FASExercisesRunList exercisesRunListWithWorkoutID:workout.workout_id];
    FASAPI* api = [FASAPI getExercisesWithRunList:runList delegate:self];
    api.userInfo = @{@"workoutObjectID" : workout.objectID };
    [api invoke];
}

- (void)importExercisesFromResponse:(NSDictionary *)response
                    workoutObjectID:(NSManagedObjectID *)workoutObjectID
{
    
    
    [sFASExerciseOpQueue addOperationWithBlock:^{
        
        NSManagedObjectContext *c = [self newWorkerContext];
        
        [self observeSaveNotificationsForContext:c];
        
        NSError *error;
        Workout *workout = (Workout *)[c existingObjectWithID:workoutObjectID
                                                        error:&error];
        BOOL didImport = NO;
        
        if (workout) {
            UBIResultData* resultData = [UBIResultData new];
            [resultData addValuesFromDictionary: response[FASAPIResultDataKey] ];
            workout.exerciseCount = resultData.rowCount;
            
            // will be used for subsequent loading of attachments
            NSArray *exerciseIDs = nil;
            NSArray *workoutExerciseIDs = nil;
            
            if (resultData.rowCount.unsignedIntegerValue != 0) {
                
                // remove already existed
                NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:kFASDataCoreDataExerciseEntityName];
                request.predicate = [NSPredicate predicateWithFormat:@"workout = %@", workout];
                NSError *error;
                NSArray *exercises = [c executeFetchRequest:request error:&error];
                for (Exercise *ex in exercises) {
                    [c deleteObject:ex];
                }
                
                // add new
                NSArray* exerciseDictionaries = [resultData arrayOfDictionaryData];
                [workout addExercisesFromArray:exerciseDictionaries];
                
                int currentTimeSpan = 10; // start 10 second for every workout
                
                if ([workout.trainingType  isEqual: @"tabata"]) {
                    currentTimeSpan += 10;
                    NSLog(@"dsfdsafsdafdsaf");
                }
                
                
                
                int count = 0; // for calculate last loop in all exercise
                
                for (Exercise* ex in workout.exercises) {
                    count += 1;
                    
                    currentTimeSpan += [ex.lapsNumber integerValue]*([ex.timeSpan integerValue] + [ex.restTime integerValue]);
                    
                    if ([ex.sortIndex integerValue] == workout.exercises.count - 1) {
                      
                        currentTimeSpan -= [ex.restTime integerValue];
                        
                    }
                    
                    //check if last exercise in loop
                    if (count == workout.exercises.count) {
                        
                        workout.currentAllTime = [NSNumber numberWithInt:currentTimeSpan];
                        workout.timeSpan = [NSNumber numberWithInt:currentTimeSpan];
                        
                                               
                        
                    }
                    
                    
                    
                }
                
                
                
                // save IDs for further use
                exerciseIDs = [exerciseDictionaries valueForKey:@"exerciseID"];
                workoutExerciseIDs = [exerciseDictionaries valueForKey:@"ID"];
            } else {
                UBILogWarning(@"exercise count == 0, nothing to process");
            }
            
            // TODO: set isExercisesLoaded flag to workout
            
            didImport = [c save:&error];
            if (!didImport) {
                UBILogError(@"workout:\n%@\nerror\n%@", workout, error.localizedDescription);
            }
            
            [self loadAttachmentsForExerciseIDs:exerciseIDs workoutExerciseIDs:workoutExerciseIDs];
        }
        else {
            UBILogError(@"Cannot obtain Workout for objectID = %@. Error was:\n%@", workoutObjectID, error.localizedDescription);
        }
        
        [self stopObservingSaveNotificationsForContext:c];
        
        [NSNotificationCenter.defaultCenter postNotificationName:FASDataExercisesDidUpdateNotification object:self userInfo:@{@"didImport": @(didImport), @"workoutObjectID": workoutObjectID,@"workout":workout}];
    }];
}

- (void)addExercisesObserver:(id <FASDataObserver>)observer
{
    [NSNotificationCenter.defaultCenter addObserver:observer selector:@selector(fasDataNotification:) name:FASDataExercisesWillUpdateNotification object:nil];
    [NSNotificationCenter.defaultCenter addObserver:observer selector:@selector(fasDataNotification:) name:FASDataExercisesDidUpdateNotification object:nil];
    [NSNotificationCenter.defaultCenter addObserver:observer selector:@selector(fasDataNotification:) name:FASDataExercisesUpdateDidFailNotification object:nil];
}

- (void)removeExercisesObserver:(id <FASDataObserver>)observer
{
    [NSNotificationCenter.defaultCenter removeObserver:observer name:FASDataExercisesWillUpdateNotification object:nil];
    [NSNotificationCenter.defaultCenter removeObserver:observer name:FASDataExercisesDidUpdateNotification object:nil];
    [NSNotificationCenter.defaultCenter removeObserver:observer name:FASDataExercisesUpdateDidFailNotification object:nil];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Attachment Data
#pragma mark -
///////////////////////////////////////////////
- (void)loadAttachmentsForExerciseIfNeeded:(Exercise *)exercise
{
    if (!exercise) {
        return;
    }
    
    __block BOOL shouldUpdate = NO;
    
    NSManagedObjectContext* c = self.workerContext;
    
    [c performBlockAndWait:^{
        NSError* existingObjectWithID = nil;
        NSManagedObject* ex = [c existingObjectWithID:exercise.objectID error:&existingObjectWithID];
        
        NSManagedObjectModel *model = [self managedObjectModel];
        
        NSDictionary *substitutionDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                                ex, @"EXERCISE", nil];
        NSFetchRequest *fetchRequest =
        [model fetchRequestFromTemplateWithName:@"FetchAllAttachWhereExercise"
                          substitutionVariables:substitutionDictionary];
    
        NSError *executeFetchRequestError = nil;
        NSArray *array = [c executeFetchRequest:fetchRequest error:&executeFetchRequestError];
        
        if (array == nil)
        {
            UBILogError(@"Error during fetching workout to find out how old dataset is. Error was:\n%@", executeFetchRequestError.localizedDescription);
        } else {
            if (array.count == 0) {
                shouldUpdate = YES;
            }
        }
    }];
    
    if (shouldUpdate) {
        [self loadAttachmentsForExercise:exercise];
    }
}

- (void)loadAttachmentsForExercise:(Exercise *)exercise
{
    if (exercise == nil) {
        UBILogWarning(@"Load attachments for exercise requested, but nil exercise object was passed.");
        return;
    }
    
    FASAttachmentsRunList* runList = [FASAttachmentsRunList attachmentsRunListWithExerciseID:exercise.exercise_id];
    FASAPI* api = [FASAPI getAttachmentsWithRunList:runList delegate:self];
    api.userInfo = @{@"exerciseObjectID" : exercise.objectID };
    //    [self setIsWaitingForAPI:YES
    //                 forObjectID:comment.objectID];
    [api invoke];
}

- (void)importAttachmentsFromResponse:(NSDictionary *)response
                    exerciseObjectID:(NSManagedObjectID *)exerciseObjectID
{
    NSManagedObjectContext* c = self.workerContext;
    
    [c performBlockAndWait:^{
        NSError *error;
        Exercise *ex = (Exercise *)[c existingObjectWithID:exerciseObjectID
                                                        error:&error];
        if (ex) {
            
            FASAttachmentsRunList* attachmentsRunList = [FASAttachmentsRunList new];
            [attachmentsRunList addValuesFromDictionary:response];
            
            if (attachmentsRunList.resultData.rowCount != 0) {
                
                // remove already existed
                NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Attach"];
                request.predicate = [NSPredicate predicateWithFormat:@"exercise = %@", ex];
                NSError *error;
                NSArray *attachments = [c executeFetchRequest:request error:&error];
                for (Attach *att in attachments) {
                    [c deleteObject:att];
                }
                
                // add new
                NSArray* attachmentsArray = [attachmentsRunList arrayOfDataTranslatedToModelRepresentation];
                
                [ex addAttachmentsFromArray:attachmentsArray];
                
                if ([c save:&error]) {
                    // TODO: remove from Waiting on API
                } else {
                    UBILogError(@"Cannot save MOC upon adding new attachments.\n%@\n Error was:\n%@", ex, error.localizedDescription);
                }
            } else {
                UBILogWarning(@"rowCount == 0 when processing the response for attachments list");
            }
        }
        else {
            UBILogError(@"Cannot obtain Exercise for objectID = %@. Error was:\n%@", exerciseObjectID, error.localizedDescription);
        }
    }];
}

- (void)loadAttachmentsForExerciseIDs:(NSArray *)exerciseIDs
                   workoutExerciseIDs:(NSArray *)workoutExerciseIDs
{
    if (exerciseIDs == nil || exerciseIDs.count == 0) {
        return;
    }
    
    if (workoutExerciseIDs == nil || workoutExerciseIDs.count == 0) {
        return;
    }
    
    UBILogTrace(@"will load attach for exerciseID list:\n%@", exerciseIDs);
    
    FASAttachmentsRunList* runList = [FASAttachmentsRunList attachmentsRunListWithExerciseIDs:exerciseIDs];
    FASAPI* api = [FASAPI getAttachmentsForExercisesWithRunList:runList delegate:self];
    api.userInfo = @{@"exerciseIdentifiers" : exerciseIDs, @"workoutExerciseIdentifiers" : workoutExerciseIDs };
    [api invoke];
}

- (void)loadAttachmentsForExerciseIDs:(NSArray *)exerciseIDs
{
    if (exerciseIDs == nil || exerciseIDs.count == 0) {
        return;
    }
    
    UBILogTrace(@"will load attach for exerciseID list:\n%@", exerciseIDs);
    
    FASAttachmentsRunList* runList = [FASAttachmentsRunList attachmentsRunListWithExerciseIDs:exerciseIDs];
    FASAPI* api = [FASAPI getAttachmentsForExercisesWithRunList:runList delegate:self];
    api.userInfo = @{@"exerciseIdentifiers" : exerciseIDs };
    [api invoke];
}

- (void)importAttachmentsFromResponse:(NSDictionary *)response
                          exerciseIDs:(NSArray *)exerciseIDs
                   workoutExerciseIDs:(NSArray *)workoutExerciseIDs
{
    NSManagedObjectContext* c = self.workerContext;
    
    [c performBlockAndWait:^{

        FASAttachmentsRunList* attachmentsRunList = [FASAttachmentsRunList new];
        [attachmentsRunList addValuesFromDictionary:response];
        
        // get all affected exercises by exercise IDs
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:kFASDataCoreDataExerciseEntityName];
        request.predicate = [NSPredicate predicateWithFormat:@"workoutExerciseID IN %@", workoutExerciseIDs];
        NSArray *arrayOfExercises = [c executeFetchRequest:request error:nil];
        
        // add attachments
        if (attachmentsRunList.resultData.rowCount != 0) {
            
            // find already existed
            NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:kFASDataCoreDataAttachEntityName];
            request.predicate = [NSPredicate predicateWithFormat:@"exercise IN %@", arrayOfExercises];
            NSError *error;
            NSArray *attachments = [c executeFetchRequest:request error:&error];
            for (Attach *att in attachments) {
                [c deleteObject:att];
            }
            
            // get array of attachments dictionaries
            NSArray* attachmentsArray = [attachmentsRunList arrayOfDataTranslatedToModelRepresentation];
            
            // add new objects for corresponding exercises
            for (Exercise* ex in arrayOfExercises) {
                for (NSDictionary* attachItemDict in attachmentsArray) {
                    if ([(NSNumber *)attachItemDict[@"exerciseID"] isEqualToNumber:ex.exercise_id]) {
                        ex.attachCount = @(ex.attachCount.unsignedIntegerValue + 1);
                        [ex addAttachmentsFromArray:@[attachItemDict]];
                    }
                }
            }
            
            if (![c save:&error]) {
                UBILogError(@"Cannot save MOC upon adding new attachments.\nError was:\n%@", error.localizedDescription);
            }
            
            [self updatePreferredAttachmentForExercises:arrayOfExercises];
            
        } else {
            UBILogWarning(@"rowCount == 0 when processing the response for attachments list");
        }
    }];
}

- (NSArray*)arrayOfAttachIDStartedDownloadForWorkout:(Workout*)workout
{
    NSManagedObjectContext* c = self.workerContext;
    __block NSMutableArray* attachIDs = [NSMutableArray array];
    
    [c performBlockAndWait:^{
        NSError *errorExistingObjectWithID;
        Workout *wrk = (Workout *)[c existingObjectWithID:workout.objectID
                                                     error:&errorExistingObjectWithID];
        if (wrk) {
            if (wrk.exerciseCount != 0) {
                // get all exercises for the workout w/ pre-fetched attachments
                NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:kFASDataCoreDataExerciseEntityName];
                [request setRelationshipKeyPathsForPrefetching:@[@"prefferedAttachment"]];
                request.predicate = [NSPredicate predicateWithFormat:@"workout = %@", wrk];
                NSError *error;
                NSArray *exercises = [c executeFetchRequest:request error:&error];
                for (Exercise* ex in exercises) {
                    if (ex.preferredAttachment != nil) {
                        BOOL forceDownload = self.forceDownloadAttachFiles;
                        
                        if ([ex.preferredAttachment downloadFile:forceDownload] == AttachDownloadTrue)
                        {
                          NSNumber* attachID = ex.preferredAttachment.attach_id;
                          [attachIDs addObject:attachID];
                        }
                    }
                    else {
                        UBILogError(@"preferredAttachment == nil for exercise:\n%@", ex);
                    }
                }
            }
            else {
                UBILogWarning(@"The workout has no exercises. Workout:\n%@", wrk);
            }
        }
        else {
            UBILogError(@"Failed to get workout objet in context (%@). Error was:\n%@", c, errorExistingObjectWithID);
        }
    }];
    
    return [attachIDs copy];
}

- (NSArray*)attachIDArrayWithExercise:(Exercise *)exercise download:(BOOL)download
{
    NSManagedObjectContext* c = self.workerContext;
    __block NSMutableArray* attachIDs = [NSMutableArray array];
    
    [c performBlockAndWait:^{
        Exercise* ex = [c existingObjectWithID:exercise.objectID error:nil];
        
        if (ex.preferredAttachment != nil) {
            if ([ex.preferredAttachment downloadFile:download] == AttachDownloadTrue)
            {
                UBILogTrace(@"download requested");
            }
            NSNumber* attachID = ex.preferredAttachment.attach_id;
            [attachIDs addObject:attachID];
        }
        else {
            UBILogError(@"exercise does not have preffered attach:\n%@", ex);
        }
    }];
    
    return [attachIDs copy];
}

// @return NO when minimum attachCount value for exercises in workout == 0
- (BOOL)isEachExercisesHasAtLeastOneAttachInWorkout:(Workout *)workout
{
    NSManagedObjectContext* c = self.mainContext;
  
    __block BOOL result = NO;
    [c performBlockAndWait:^{
        NSError *errorExistingObjectWithID;
        Workout *wrk = (Workout *)[c existingObjectWithID:workout.objectID
                                                    error:&errorExistingObjectWithID];
        
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kFASDataCoreDataExerciseEntityName];
        fetchRequest.predicate = [NSPredicate predicateWithFormat:@"workout = %@", wrk];
        
        [fetchRequest setResultType:NSDictionaryResultType];
        
        NSExpression *keyPathExpression = [NSExpression expressionForKeyPath:@"attachCount"];
        NSExpression *minAttachCount = [NSExpression expressionForFunction:@"min:" arguments:[NSArray arrayWithObject:keyPathExpression]];
        NSExpressionDescription *expressionDescription = [[NSExpressionDescription alloc] init];
        [expressionDescription setName:@"minAttachCount"];
        [expressionDescription setExpression:minAttachCount];
        [expressionDescription setExpressionResultType:NSDecimalAttributeType];
        [fetchRequest setPropertiesToFetch:[NSArray arrayWithObject:expressionDescription]];
        
        NSError *error = nil;
        NSArray *objects = [c executeFetchRequest:fetchRequest error:&error];
        if (objects == nil) {
            UBILogError(@"error:\n%@", error.localizedDescription);
        }
        else {
            if ([objects count] > 0) {
                UBILogTrace(@"exercises in workout with name \"%@\" have min attach count: %@", wrk.name, [[objects objectAtIndex:0] valueForKey:@"minAttachCount"]);
                
                id value = [[objects objectAtIndex:0] valueForKey:@"minAttachCount"];
                if ([value isKindOfClass:[NSNumber class]]) {
                    NSNumber* attachCount = (NSNumber *)value;
                    result = ([attachCount compare:@(0)] == NSOrderedDescending);
                }
            }
            else {
                UBILogWarning(@"some exercise(s) in workout \"%@\" does not have an attach", wrk.name);
            }
        }
    }];
    
    return result;
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Best Result
#pragma mark -
///////////////////////////////////////////////
// TODO: change method name to - (void)loadBestExercisesResultsForWorkout:(Workout *)workout
- (void)getBestExercisesResultsForWorkout:(Workout *)workout
{
    FASAPI *api = [FASAPI getBestExerciseResultsForWorkoutID:workout.workout_id delegate:self];
    [api invoke];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark File Operations
#pragma mark -
///////////////////////////////////////////////

- (BOOL)hasFilesDownloadedForWorkout:(Workout*)workout
{
    NSManagedObjectContext* c = self.mainContext;
    
    __block BOOL success = YES;
    
    [c performBlockAndWait:^{
        NSError *errorExistingObjectWithID;
        Workout *wrk = (Workout *)[c existingObjectWithID:workout.objectID
                                                    error:&errorExistingObjectWithID];
        
        NSFetchRequest *preferredAttachmentRequest = [NSFetchRequest fetchRequestWithEntityName:kFASDataCoreDataExerciseEntityName];
        [preferredAttachmentRequest setRelationshipKeyPathsForPrefetching:@[@"prefferedAttachment"]];
        preferredAttachmentRequest.predicate = [NSPredicate predicateWithFormat:@"workout = %@", wrk];
        NSError *preferredAttachmentRequestError;
        NSArray *exercises = [c executeFetchRequest:preferredAttachmentRequest error:&preferredAttachmentRequestError];
        for (Exercise* ex in exercises) {
            if (ex.preferredAttachment == nil || ex.preferredAttachment.isFileDownloaded == NO) {
                success = NO;
                // there is no point to check everything else, can just stop here
                break;
            }
        }
    }];
    return success;
}

- (void)updatePreferredAttachmentForExercise:(Exercise *)exercise
{
    [self updatePreferredAttachmentForExercises:@[exercise]];
}

- (void)updatePreferredAttachmentForExercises:(NSArray <Exercise *> *)exercises
{
    [self updatePreferredAttachmentForExercises:exercises force:NO context:nil];
}

- (void)updatePreferredAttachmentForExercises:(NSArray <Exercise *> *)exercises
                                        force:(BOOL)force
                                      context:(NSManagedObjectContext *)context
{
    if (exercises == nil || exercises.count == 0) {
        return;
    }
    
    if (!context) {
        context = self.mainContext;
    }
    
    [context performBlockAndWait:^{
        // conditions
        NSString* prefLang = [self preferredLocalization];
        AttachContentType prefContentType = AttachContentTypeVideo;
        NSString* fallbackPrefLang = @"en";
        
        [exercises enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            Exercise* e = [context existingObjectWithID:[(Exercise *)obj objectID] error:nil];
            if (e) {
                if (force == YES || e.preferredAttachment == nil) {
                    [e.attachments enumerateObjectsUsingBlock:^(id obj, BOOL *stopInner) {
                        Attach* a = (Attach *)obj;
                        
                        if ([a.lang isEqualToString:prefLang] && a.contentType == prefContentType) {
                            e.preferredAttachment = a;
                            *stopInner = YES;
                        }
                        else if ([a.lang isEqualToString:fallbackPrefLang]) {
                            e.preferredAttachment = a;
                        }
                    }];
                    
                    // no preferred attachment found
                    if (e.preferredAttachment == nil) {
                        if (e.attachments.count > 0) {
                            UBILogWarning(@"Exercise with ID (%@) DOES NOT have a preferred attachment. Set first attach from attachments", e.exercise_id);
                            e.preferredAttachment = e.attachments.allObjects.firstObject;
                        }
                        else {
                            UBILogError(@"Exercise with ID (%@) DOES NOT have ANY attachment", e.exercise_id);
                        }
                    }
                }
                else {
                    UBILogTrace(@"exercise with ID (%@) ALREADY HAS preferred attachment. Skipping.", e.exercise_id);
                }
            }
        }];
        
        NSError *saveError = nil;
        if (![context save:&saveError]) {
            UBILogError("%@", [saveError localizedDescription]);
        }
    }];
}

- (BOOL)allDownloadedPreferredAttachesCopiedForWorkout:(NSArray * _Nullable)exercises {
    if (exercises == nil || exercises.count == 0) {
        return false;
    }
    
    __block BOOL cacheFileSaved = YES;
    [exercises enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        Exercise* e = (Exercise *)obj;
        
        if (![e.preferredAttachment isFileDownloaded]) {
            UBILogWarning(@"cache file not saved: %@", e.preferredAttachment);
            cacheFileSaved = NO;
            *stop = YES;
        }
        
        // no preferred attachment found
        if (e.preferredAttachment == nil) {
            UBILogWarning(@"No preferred attachment");
        }
    }];
    return cacheFileSaved;
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Modify Date
#pragma mark -
///////////////////////////////////////////////

- (NSArray *)workoutIDsToUpdateFromModifyDateResponse:(NSDictionary *)response
{
    __block NSArray* existedObjects = nil;
    NSManagedObjectContext* c = self.workerContext;
    NSArray* modifiedIDs = nil;
    
    [c performBlockAndWait:^{
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kFASDataCoreDataWorkoutEntityName];
        [fetchRequest setPropertiesToFetch:[NSArray arrayWithObjects:@"workout_id", @"mi_modifyDate", nil]];
        NSError *fetchRequestError = nil;
        existedObjects = [c executeFetchRequest:fetchRequest error:&fetchRequestError];
        if (fetchRequestError) {
            UBILogError(@"cannot fetch objects. Error was:\n%@", fetchRequestError.localizedDescription);
        }
    }];
    
    modifiedIDs = [self modifiedIDsForExistedObjects:existedObjects
                                            response:response
                                             context:c];
    
    return modifiedIDs;
}

- (NSArray *)exerciseIDsToUpdateFromModifyDateResponse:(NSDictionary *)response
{
    __block NSArray* existedObjects = nil;
    NSManagedObjectContext* c = self.workerContext;
    NSArray* modifiedIDs = nil;
    
    [c performBlockAndWait:^{
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kFASDataCoreDataExerciseEntityName];
        [fetchRequest setPropertiesToFetch:[NSArray arrayWithObjects:@"exercise_id", @"mi_modifyDate", nil]];
        NSError *fetchRequestError = nil;
        existedObjects = [c executeFetchRequest:fetchRequest error:&fetchRequestError];
        if (fetchRequestError) {
            UBILogError(@"cannot fetch objects. Error was:\n%@", fetchRequestError.localizedDescription);
        }
    }];
    modifiedIDs = [self modifiedIDsForExistedObjects:existedObjects
                                            response:response
                                             context:c];
    
    return modifiedIDs;
}

- (NSArray *)attachIDsToUpdateFromModifyDateResponse:(NSDictionary *)response
{
    __block NSArray* existedObjects = nil;
    NSManagedObjectContext* c = self.workerContext;
    NSArray* modifiedIDs = nil;
    
    [c performBlockAndWait:^{
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kFASDataCoreDataAttachEntityName];
        [fetchRequest setPropertiesToFetch:[NSArray arrayWithObjects:@"attach_id", @"mi_modifyDate", nil]];
        NSError *fetchRequestError = nil;
        existedObjects = [c executeFetchRequest:fetchRequest error:&fetchRequestError];
        if (fetchRequestError) {
            UBILogError(@"cannot fetch objects. Error was:\n%@", fetchRequestError.localizedDescription);
        }
    }];
    
    modifiedIDs = [self modifiedIDsForExistedObjects:existedObjects
                                            response:response
                                             context:c];
    
    return modifiedIDs;
}

- (NSArray *)modifiedIDsForExistedObjects:(NSArray*)existedObjects
                                 response:(NSDictionary *)response
                                  context:(NSManagedObjectContext *)context
{
    NSManagedObjectContext* c = (context != nil ? context : self.workerContext);
    __block NSMutableArray *objectIDsToUpdate = [NSMutableArray array];

    // TODO: response should be prepared by RunList and run list should return, array of object conforming to <FASModifyDate> protocol
    // process response
    UBIResultData* resultData = [UBIResultData new];
    [resultData addValuesFromDictionary:response[FASAPIResultDataKey]];
    NSArray* nativeDataSetDictionaries = [resultData arrayOfDictionaryData];
    
    [c performBlockAndWait:^{
        for (NSDictionary* respDict in nativeDataSetDictionaries) {
            assert([[respDict objectForKey:@"ID"] isKindOfClass:[NSNumber class]]);
            assert([[respDict objectForKey:@"mi_modifyDate"] isKindOfClass:[NSString class]]);
            
            NSDate* respModifyDate = [NSDate dateFromUnityBaseString:[respDict objectForKey:@"mi_modifyDate"]];
            
            BOOL isFound = NO;
            
            for (id<FASModifyDate> obj in existedObjects) {
                if ([obj conformsToProtocol:@protocol(FASModifyDate)]) {
                    id <FASModifyDate> modifyDateComparable = obj;
                    
                    if ([[modifyDateComparable uniqueID] compare:(NSNumber* )[respDict objectForKey:@"ID"]] == NSOrderedSame) {
                        isFound = YES; // the object is found
                        if ([[modifyDateComparable modifyDate] compare:respModifyDate] != NSOrderedSame) {
                            [objectIDsToUpdate addObject:[modifyDateComparable uniqueID]];
                        }
                    }
                }
                else {
                    UBILogWarning(@"object %@ does not conform to protocol %@", obj, @protocol(FASModifyDate));
                }
            }
            
            // there is no object w/ respective ID loaded before
            if ( ! isFound) {
                [objectIDsToUpdate addObject:[respDict objectForKey:@"ID"]];
            }
        }
    }];
    
    UBILogTrace(@"object IDs to update:\n%@", objectIDsToUpdate);
    
    return [objectIDsToUpdate copy];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Network Activity
#pragma mark -
///////////////////////////////////////////////

- (void)startNetworkActivity
{
    self.networkActivityCount++;
}

- (void)stopNetworkActivity
{
    if (self.networkActivityCount > 0) {
        self.networkActivityCount--;
    }
}

- (void)setNetworkActivityCount:(NSUInteger)networkActivityCount
{
    if (_networkActivityCount != networkActivityCount) {
        _networkActivityCount = networkActivityCount;
        dispatch_async(dispatch_get_main_queue(), ^{
                UIApplication.sharedApplication.networkActivityIndicatorVisible = _networkActivityCount > 0;
        });
    }
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Image & photo methods
#pragma mark -
///////////////////////////////////////////////

- (void)saveImage:(UIImage *)image toPath:(NSString *)path
{
    NSData *jpegData = UIImageJPEGRepresentation(image, 1.0);
    NSError *error;
    BOOL success = [jpegData writeToFile:path options:NSDataWritingAtomic error:&error];
    if (!success) {
        UBILogError(@"path:%@, err:%@", path, [error detailedInfo])
    }
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Global Data Helpers
#pragma mark -
///////////////////////////////////////////////

// Image URL

- (NSURL *)imageURLForEntity:(NSString*)entity ID:(NSNumber *)identifier
{
    NSString* strPath = [NSString stringWithFormat:@"%@://%@/%@entity=%@&attribute=photo&ID=%@",(FASEnvironment.APISecure ? @"https" : @"http"), FASEnvironment.APIBasePath,@"getDocument?",entity,identifier];
    return [NSURL URLWithString:strPath];
}

- (NSURL *)imageURLForWorkoutWithID:(NSNumber *)identifier
{
    return [self imageURLForEntity:@"fas_workout" ID:identifier];
}

- (NSURL *)imageURLForExerciseWithID:(NSNumber *)identifier
{
    return [self imageURLForEntity:@"fas_workout_exercise" ID:identifier];
}


- (NSURL *)imageURLForWorkoutID:(NSNumber *)identifier
{
    return [self imageURLForEntity:@"fas_workout" ID:identifier];
}

/*
 > www.fas-sport.com/img/mobile/exercises/3000000037212.png
 > www.fas-sport.com/img/mobile/icons/3000000037212.png

 
 */

- (NSURL *)imageURLForExerciseID:(NSNumber *)identifier
{
    return [self imageURLForEntity:@"fas_workout_exercise" withID:identifier];
}

- (NSURL *)imageURLForExerciseIconWithID:(NSNumber *)identifier
{
    NSString* strPath = [NSString stringWithFormat:@"%@://%@/img/mobile/icons/%@.png",(FASEnvironment.APISecure ? @"https" : @"http"), @"www.fas-sport.com",identifier];
    
    return [NSURL URLWithString:strPath];
    
}


- (NSURL *)imageURLForEntity:(NSString*)entity withID:(NSNumber *)identifier
{
    NSString *entityString = [entity isEqualToString:@"fas_workout_exercise"] ? @"exercises":@"";
    
    NSString* strPath = [NSString stringWithFormat:@"%@://%@/img/mobile/%@/%@.png",(FASEnvironment.APISecure ? @"https" : @"http"), FASEnvironment.APIBasePath,entityString,identifier];
    return [NSURL URLWithString:strPath];
}

// Language

- (NSString *)preferredLanguage
{
    return [[self preferredLanguages] firstObject];
}

- (NSArray *)preferredLanguages
{
    return [NSLocale preferredLanguages];
}

- (NSString *)preferredLocalization
{
    return [[[NSBundle mainBundle] preferredLocalizations] firstObject];
}

// Response

// RunList response is returned as an array w/ one element,
// so verify the expected response is returned
- (BOOL)isValidRunListResponse:(id)response
{
    if (response != nil
        && [response isKindOfClass:[NSArray class]]
        && [[(NSArray *)response firstObject] isKindOfClass:[NSDictionary class]])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

// @return nil if response object is of unexpected structure
- (NSDictionary *)dictionaryFromRunListResponse:(id)responseObject
{
    NSDictionary* returnObject = nil;
    if ([self isValidRunListResponse:responseObject]) {
        returnObject = [(NSArray *)responseObject firstObject];
    }
    return returnObject;
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Notification Settings
#pragma mark -
///////////////////////////////////////////////
- (BOOL)hasPermissionToScheduleLocalNotifications
{
    return [[UIApplication sharedApplication] isAnyUserFacingNotificationTypesAllowed];
}

- (BOOL)hasPermissionToReceiveRemoteNotifications
{
    return [[UIApplication sharedApplication] isAnyUserFacingNotificationTypesAllowed] && [[UIApplication sharedApplication] isRegisteredForRemoteNotifications];
}

- (void)registerPermissionToScheduleLocalNotifications
{
    [[UIApplication sharedApplication] registerUserFacingNotificationTypes:self.requiredNotificationType];
}

- (void)registerPermissionToReceiveRemoteNotifications
{
    [[UIApplication sharedApplication] registerUserFacingNotificationTypes:self.requiredNotificationType];
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Local Notification
#pragma mark -
///////////////////////////////////////////////

- (void)scheduleLocalRepeatedNotificationWithID:(NSString *)identifier
                                     remindDate:(NSDate *)remindDate
{
    if (!identifier || [identifier isBlank]) {
        return;
    }
    
    // do not schedule notifications with duplicated uid
    [self cancelPreviouslyScheduledRepeatedNotificationWithID:identifier];
    
    if (![self hasPermissionToScheduleLocalNotifications]) {
        // TODO: notify UI to show a pop-up to prepare user for the permission to be asked.
        // For now, just ask immediatelly, and wait afterward
        [self registerPermissionToScheduleLocalNotifications];
    }
    
    if (remindDate) {
        NSString* alertBody = nil;
        NSString* alertTitle = nil;
        NSString* soundName = nil;
        NSCalendarUnit repeatInterval;
        
        
        if ([identifier isEqualToString:kFASDataWorkoutReminderLocalNotificationID]) {
            alertBody = @"Time to start training! Don't waste your chance to become better already today!"; // TODO: localize this
            soundName = nil;
#ifdef DEBUG
            repeatInterval = NSCalendarUnitHour;
#else
            repeatInterval = NSCalendarUnitDay;
#endif
            alertTitle = @"Apple Watch Title haha!";
        }
        
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        // TODO: customize the notification depending on currentUserNotificationSettings value
        localNotification.alertBody = alertBody;
        localNotification.alertTitle = alertTitle; // Apple Watch title
        localNotification.soundName = soundName;
        localNotification.repeatInterval = repeatInterval;
        localNotification.fireDate = remindDate;
        localNotification.timeZone = [NSTimeZone systemTimeZone];
        localNotification.userInfo = @{@"uid":identifier};
        
        UIApplication *app = [UIApplication sharedApplication];
        [app scheduleLocalNotification:localNotification];
    }
}

- (void)cancelPreviouslyScheduledRepeatedNotificationWithID:(NSString *)identifier
{
    if (!identifier || [identifier isBlank]) {
        return;
    }
    
    UIApplication *app = [UIApplication sharedApplication];
    
    for (UILocalNotification* notif in [app scheduledLocalNotifications]) {
        NSString *uid = [notif.userInfo valueForKey:@"uid"];
        if ([uid isEqualToString:identifier])
        {
            [app cancelLocalNotification:notif];
            break;
        }
        
        // remove all unrecognized notifications (with NO uid)
#if DEBUG
        if (uid == nil) {
            UBILogWarning(@"unrecognized local notification is scheduled %@", notif);
            [app cancelLocalNotification:notif];
        }
#endif
    }
}

- (NSDate *)fireDateForLocalNotificationWithID:(NSString *)identifier
{
    if (!identifier || [identifier isBlank]) {
        UBILogError(@"invalid identifier");
        return nil;
    }
    
    UIApplication *app = [UIApplication sharedApplication];
    NSDate* date = nil;
    
    for (UILocalNotification* notif in [app scheduledLocalNotifications]) {
        NSString *uid = [notif.userInfo valueForKey:@"uid"];
        if ([uid isEqualToString:identifier])
        {
            date = notif.fireDate;
            break;
        }
    }
    
    return date;
}

- (NSDate * _Nullable)fireDateForLocalNotificationReminder
{
    return [self fireDateForLocalNotificationWithID: kFASDataWorkoutReminderLocalNotificationID];
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark In-App Purchase
#pragma mark -
///////////////////////////////////////////////
- (void)loadStoreItemsFromRemote
{
    FASAPI* api = [FASAPI getStoreItemsWithDelegate:self];
    [api invoke];
}

- (BOOL)importStoreItemsFromResponse:(NSDictionary *)response
                           inContext:(NSManagedObjectContext *)context
{
    NSParameterAssert(context);
    
    __block BOOL didImport = NO;
    
    [context performBlockAndWait:^{
        // delete existing StoreItems from context
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:kFASDataCoreDataStoreItemEntityName];
        NSError *fetchError;
        NSArray *storeItems = [context executeFetchRequest:request error:&fetchError];
        for (StoreItem *item in storeItems) {
            [context deleteObject:item];
        }
        
        NSMutableArray* importedStoreItems = [NSMutableArray array];
        NSMutableArray* addedInAppProductIDs = [NSMutableArray array];
        
        for (NSDictionary *storeItemDictionary in response[FASAPIResultKey]) {
            @autoreleasepool {
                // checking against duplicated
                NSString* uniqueID = storeItemDictionary[FASAPIStoreItemInAppProductID];
                if ([addedInAppProductIDs indexOfObject: uniqueID] != NSNotFound) {
                    continue;
                }
                [addedInAppProductIDs addObject:uniqueID];
                
                StoreItem *storeItem = (StoreItem *)[NSEntityDescription insertNewObjectForEntityForName:kFASDataCoreDataStoreItemEntityName
                                                                                  inManagedObjectContext:context];
                [storeItem addValuesFromDictionary:storeItemDictionary];
                
                // add to local collection for post-processing
                [importedStoreItems addObject:storeItem];
                
                for (NSDictionary *includedStoreItemDictionary in storeItemDictionary[FASAPIStoreItemItemsKey]) {
                    @autoreleasepool {
                        
                        // checking agains duplicated
                        uniqueID = includedStoreItemDictionary[FASAPIStoreItemInAppProductID];
                        if ([addedInAppProductIDs indexOfObject: uniqueID] != NSNotFound) {
                            // find existed object
                            StoreItem *existedStoreItem = nil;
                            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K == %@", FASAPIStoreItemInAppProductID, uniqueID];
                            NSArray *filteredArray = [importedStoreItems filteredArrayUsingPredicate:predicate];
                            existedStoreItem = filteredArray.firstObject;
                            
                            [storeItem addBulkItemsObject:existedStoreItem];
                        }
                        else {
                            [addedInAppProductIDs addObject:uniqueID];
                            
                            StoreItem *includedStoreItem = (StoreItem *)[NSEntityDescription insertNewObjectForEntityForName:kFASDataCoreDataStoreItemEntityName
                                                                                                      inManagedObjectContext:context];
                            [includedStoreItem addValuesFromDictionary:includedStoreItemDictionary];
                            [storeItem addBulkItemsObject:includedStoreItem];
                            
                            // add to local collection for post-processing
                            [importedStoreItems addObject:includedStoreItem];
                        }
                    }
                }
                
                // update purchase status for top storeItem ONLY
                [self updateProductPurchaseStatus:storeItem];
            }
        }
        
        [self fetchProductInformationForIdentifiers:addedInAppProductIDs];
        
        NSError* saveError = nil;
        if ([context hasChanges]) {
            didImport = [context save:&saveError];
        }
    }];
    
    return didImport;
}

- (BOOL)isInAppEnabled
{
    // TODO: test the canMakePayments status change
    return [SKPaymentQueue canMakePayments];
}


-(void)checkReceipt:(NSString*)base64EncodedReceipt{
    
    NSError *error;
    
    NSDictionary *requestContents = @{ @"receipt-data": base64EncodedReceipt};
    
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:requestContents
                                                          options:0
                                                            error:&error];
    
    if (!requestData) {
    
        }
    
    // Create a POST request with the receipt data.
    NSURL *storeURL = [NSURL URLWithString:@"https://buy.itunes.apple.com/verifyReceipt"];
    
    NSMutableURLRequest *storeRequest = [NSMutableURLRequest requestWithURL:storeURL];
    
    [storeRequest setHTTPMethod:@"POST"];
    [storeRequest setHTTPBody:requestData];
    
    // Make a connection to the iTunes Store on a background queue.
    NSOperationQueue *queue = [NSOperationQueue new];
    
    [NSURLConnection sendAsynchronousRequest:storeRequest queue:queue
                    completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                    if (connectionError) {
                                   /* ... Handle error ... */
                    } else {
                        NSError *error;
                        NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                        
                        if (jsonResponse) {
                        
                        }else{
                            
                        }
                        
                }
            }];

    
}
- (void)fetchProductInformationForIdentifiers:(NSArray*)productIds
{
    [[StoreManager sharedInstance] fetchProductInformationForIds:productIds];
}

- (void)updateStoreProductsInformation
{
    __block NSArray * productIdentifiers = nil;
    NSManagedObjectContext* c = self.workerContext;
    [c performBlockAndWait:^{
        NSEntityDescription *entityDescription = [NSEntityDescription entityForName:kFASDataCoreDataStoreItemEntityName
                                                             inManagedObjectContext:c];
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        [request setEntity:entityDescription];
        [request setResultType: NSDictionaryResultType];
        [request setPropertiesToFetch:@[FASAPIinAppProductID]];
        [request setPredicate:[NSPredicate predicateWithFormat:@"(%K!=nil)", FASAPIinAppProductID]];
        
        NSError *error;
        NSArray* objects = [c executeFetchRequest:request error:&error];
        if (objects == nil) {
            UBILogError(@"no objects with inAppProductID found");
        } else if (objects.count != 0) {
            productIdentifiers = [objects valueForKey: FASAPIinAppProductID];
        }
    }];
    
    if (productIdentifiers.count != 0) {
        [self fetchProductInformationForIdentifiers:productIdentifiers];
    }
}

- (void)restorePurchases
{
    if (self.isRestoringProducts) {
        return;
    }
    
    if ([self isInAppEnabled]) {
        [[StoreObserver sharedInstance] restorePurchasesWithSession];
    }
}

- (void)processPurchaseStatus:(SOProductStatus)status
                 forProductID:(NSString *)productID
{
    if (![[FASAPISession currentSession] isLoggedIn]) {
        return;
    }
    
    dispatch_async(dispatch_queue_create("com.fas-sport.FASWorkouts.Queues.FASData.PurchaseQueue", NULL), ^(void) {
        if (productID && ![productID isBlank]) {
            
            NSManagedObjectContext* context = [self newWorkerContext];
            [self observeSaveNotificationsForContext:context];
            
            NSFetchRequest *requestStoreItem = [NSFetchRequest fetchRequestWithEntityName:kFASDataCoreDataStoreItemEntityName];
            requestStoreItem.predicate = [NSPredicate predicateWithFormat:@"%K = %@", @"inAppProductID", productID];
            NSError *error;
            NSArray *objects = [context executeFetchRequest:requestStoreItem error:&error];
            if (objects.count > 0) {
                StoreItem *item = (StoreItem *)objects.firstObject;
                
                [item setProductPurchaseStatus:status];
                
                [self saveProductStatus:status
                 forItemWithProductType:[item productType]
                      productIdentifier:[item productIdentifier]];
                
                if (status ==  SOProductStatusPurchaseSucceeded) {
                    [DUBApptentiveDeferredEngagement.sharedInstance deferEngage:kFASApptentiveEventDidMakeInAppPurchase];
                }
                
                if (status == SOProductStatusReceiptValidationFailed) {
                    // do not engage, if validation is failed
                    [DUBApptentiveDeferredEngagement.sharedInstance cancelDeferredEngage:kFASApptentiveEventDidMakeInAppPurchase];
                }
                
                if (status == SOProductStatusReceiptValidationSucceeded) {
                    [DUBApptentiveDeferredEngagement.sharedInstance engageDeferred:kFASApptentiveEventDidMakeInAppPurchase
                                                                fromViewController:nil];
                    
                    // consider this status, to mark related products as being "available"
                    [self makeProductsAvailableForPurchasedStoreItem:item];
                }
                
                NSError *saveError = nil;
                if (![context save:&saveError]) {
                    UBILogError("%@", [saveError localizedDescription]);
                }
            } else {
                // not found among storeItems, ignore it
            }
            
            [self stopObservingSaveNotificationsForContext:context];
        }
    });
}

- (void)saveProductStatus:(SOProductStatus)status
   forItemWithProductType:(NSString *)productType
        productIdentifier:(NSString *)productIdentifier
{
    NSMutableDictionary* productsOfType = [NSMutableDictionary dictionaryWithDictionary:self.purchasedItems[productType]];
    [productsOfType setObject:@(status) forKey:productIdentifier];
    [self.purchasedItems setObject:[productsOfType copy] forKey:productType];
    [self saveCacheForDataType:FASDataCachePurchases];
}

- (void)purchaseProduct:(id <FASProduct>)product
{
    if (![self isInAppEnabled]) {
        UBILogError(@"In-App Purchase is restricted");
    }
    
    UBILogTrace(@"will purchase product with identifier: %@", [product productIdentifier]);
    
    if (!product || !product.isPaid) {
        return; // error - not for purchase
    }
    
    SKProduct *skProduct = [[StoreManager sharedInstance] skProduct:product];
    if (skProduct) {
        // Attempt to purchase the tapped product
        [[StoreObserver sharedInstance] buyProductWithSession:skProduct];
    }
}

// TODO: we assume that only Workout could be purchased via StoreItem
// this might be a subject of change in further versions
- (void)makeProductsAvailableForPurchasedStoreItem:(StoreItem *)storeItem
{
    NSManagedObjectContext* context = storeItem.managedObjectContext;
    NSPredicate* predicate = nil;
    
    if ([[storeItem productCategoryName] isEqualToString:@"workoutBulk"]) {
        // make products in the bulk available
        NSMutableArray* availableProductIDs = [NSMutableArray arrayWithCapacity:[storeItem bulkItems].count];
        for (StoreItem* itemInBulk in [storeItem bulkItems]) {
            [availableProductIDs addObject:[itemInBulk productIdentifier]];
        }
        
        if (availableProductIDs.count > 0) {
            predicate = [NSPredicate predicateWithFormat:@"%K IN %@", @"inAppProductID", availableProductIDs];
        }
    }
    else if ([[storeItem productCategoryName] isEqualToString:@"workout"]) {
        // make individual workout available
        predicate = [NSPredicate predicateWithFormat:@"%K = %@", @"inAppProductID", storeItem.productIdentifier];
    }
    else {
        UBILogWarning(@"unexpected productCategoryName == %@", [storeItem productCategoryName]);
    }
    
    // make available respective objects
    if (predicate) {
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:kFASDataCoreDataWorkoutEntityName];
        //request.predicate = predicate;
    
        NSError *error = nil;
        NSArray *objects = [context executeFetchRequest:request error:&error];
        for (Workout* workout in objects) {
            // HACK: the workout is NOT purchased, but rather "available", b/c related store item had been purchased
            [workout setProductPurchaseStatus: SOProductStatusReceiptValidationSucceeded];
        }
    }
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Available (Purchased & Restored) Product IDs
#pragma mark -
///////////////////////////////////////////////

- (void)updateProductPurchaseStatus:(id <FASProduct>)fasProduct {
    if (![fasProduct isPaid]) {
        return; // do nothing for "free" products
    }
    
    NSString *productCategoryName = [fasProduct productCategoryName];
    NSDictionary* purchasedProductsForCategoryName = [self.purchasedItems objectForKey:productCategoryName];
    if (purchasedProductsForCategoryName) {
        NSString *productIdentifier = [fasProduct productIdentifier];
        id productPurchaseStatusObject = [purchasedProductsForCategoryName objectForKey:productIdentifier];
        if (productPurchaseStatusObject) {
            // product found among purchased items, so update the status with the model
            NSInteger productPurchaseStatus = [(NSNumber*)productPurchaseStatusObject integerValue];
            [fasProduct setProductPurchaseStatus:productPurchaseStatus];
            
            // update items included in store item
            if ([fasProduct isKindOfClass:[StoreItem class]]
                && productPurchaseStatus == SOProductStatusReceiptValidationSucceeded) {
                [self makeProductsAvailableForPurchasedStoreItem:(StoreItem *)fasProduct];
            }
        }
    }
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Product Observers
#pragma mark -
///////////////////////////////////////////////
- (void)addProductObserver:(id <FASDataObserver>)observer
{
    [NSNotificationCenter.defaultCenter
     addObserver:observer
     selector:@selector(fasDataNotification:)
     name:FASDataProductsDidUpdateNotification
     object:nil];
}

- (void)removeProductObserver:(id <FASDataObserver>)observer
{
    [NSNotificationCenter.defaultCenter
     removeObserver:observer
     name:FASDataProductsDidUpdateNotification
     object:nil];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Purchase Observers
#pragma mark -
///////////////////////////////////////////////
- (void)addPurchaseObserver:(id <FASDataObserver>)observer
{
    [NSNotificationCenter.defaultCenter
     addObserver:observer
     selector:@selector(fasDataNotification:)
     name:FASDataPurchasesRestoreDidUpdateNotification
     object:nil];
}

- (void)removePurchaseObserver:(id <FASDataObserver>)observer
{
    [NSNotificationCenter.defaultCenter
     removeObserver:observer
     name:FASDataPurchasesRestoreDidUpdateNotification
     object:nil];
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Cache Management
#pragma mark -
///////////////////////////////////////////////

- (void)loadDataTypeFromCache:(FASDataCache)dataType
{
    NSUserDefaults *defaults = NSUserDefaults.standardUserDefaults;
    
    switch (dataType) {
            
        case FASDataCachePurchases: {
            NSDictionary* storedPurchasedItems = (NSDictionary *)[defaults objectForKey:FASDataPurchasedItemsPrefKey];
            if (storedPurchasedItems) {
                _purchasedItems = [XJSafeMutableDictionary dictionaryWithDictionary:storedPurchasedItems];
            }
        } break;
            
        case FASDataCacheWorkoutProgress: {
            NSData *encodedCurrentWorkoutSession = [defaults objectForKey:FASDataCurrentWorkoutSessionPrefKey];
            if (encodedCurrentWorkoutSession) {
                _currentWorkoutSession = [NSKeyedUnarchiver unarchiveObjectWithData:encodedCurrentWorkoutSession];
            }
        } break;
            
        default: break;
    }
    
    [defaults synchronize];
}

- (void)saveCacheForDataType:(FASDataCache)dataType
{
    FASDataCache cacheType = dataType;
    
    NSUserDefaults *defaults = NSUserDefaults.standardUserDefaults;
    
    switch(cacheType) {
        case FASDataCachePurchases: {
            [defaults setObject:self.purchasedItems forKey:FASDataPurchasedItemsPrefKey];
        } break;
            
        case FASDataCacheWorkoutProgress: {
            if (self.currentWorkoutSession) {
                NSData *encodedCurrentWorkoutSession = [NSKeyedArchiver archivedDataWithRootObject:self.currentWorkoutSession];
                [defaults setObject:encodedCurrentWorkoutSession forKey:FASDataCurrentWorkoutSessionPrefKey];
            }
            else {
                [defaults removeObjectForKey:FASDataCurrentWorkoutSessionPrefKey];
            }
        } break;
            
        default: break;
    }
    
    [defaults synchronize];
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Handle product request notification
#pragma mark -
///////////////////////////////////////////////
-(void)handleStoreManagerDidChangeStatusNotification:(NSNotification *)notification
{
    StoreManager *storeManager = (StoreManager*)[notification object];
    IAPProductRequestStatus result = (IAPProductRequestStatus)storeManager.status;
    
    switch (result)
    {
        case IAPRequestSuceeded: {
            // notify GAI about invalid products
            NSArray* invalidProducts = storeManager.invalidProductIds;
            if (invalidProducts.count > 0) {
                assert(invalidProducts.count != 0);
                UBILogWarning(@"INVALID product IDs\n%@", invalidProducts);
                // Track
                for (NSString* aProduct in invalidProducts) {
                    [FASAnalyticsEventHelper trackInvalidProductID:aProduct];
                }
            }
            
            [NSNotificationCenter.defaultCenter
             postNotificationName:FASDataProductsDidUpdateNotification
             object:self
             userInfo:nil];
        } break;
            
        case IAPRequestFailed: {
            UBILogError(@"%@", storeManager.errorFromLastRequest.localizedDescription);
        } break;
            
        default: break;
    }
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Store Observer
#pragma mark -
///////////////////////////////////////////////
- (void)storeObserverNotification:(NSNotification *)notification
{
    if (notification.name == StoreObserverDidChangeProductStatusNotification) {
        [self storeObserverProductStatus:notification];
    }
    else if (notification.name == StoreObserverWillRestoreNotification) {
        [self storeObserverWillRestore];
    }
    else if (notification.name == StoreObserverDidFinishRestoreNotification) {
        [self storeObserverDidFinishRestore];
    }
    else if (notification.name == StoreObserverRestoreDidFailNotification) {
        [self storeObserverRestoreDidFail:notification];
    }
}

- (void)storeObserverProductStatus:(NSNotification *)notification
{
    SOProductStatus status = (SOProductStatus)[(NSNumber*)[notification.userInfo objectForKey:SOProductStatusNotificationStatusKey] integerValue];
    NSString* productIdentifier = [notification.userInfo objectForKey:SOProductStatusNotificationProductIdentifierKey];
    NSError* error = [notification.userInfo objectForKey:SOProductStatusNotificationErrorKey];
    
    switch (status)
    {
        case SOProductStatusPurchaseInProgress: {
            UBILogTrace(@"Purchase STARTED: %@", productIdentifier);
        } break;

        case SOProductStatusRestoreSucceeded:
        case SOProductStatusPurchaseSucceeded:{
            UBILogTrace(@"Purchase COMPLETED: %@", productIdentifier);
        } break;
            
        case SOProductStatusRestoreFailed:
        case SOProductStatusPurchaseFailed: {
            UBILogTrace(@"Purchase FAILED: %@", productIdentifier);
            
            if (error != nil && error.code != SKErrorPaymentCancelled) {
                [FASAPIError alertStringForErrors:@[error]];
            }
        } break;

        case SOProductStatusPurchaseDeferred: {
            UBILogTrace(@"Purchase DEFERRED: %@", productIdentifier);
        } break;
            
            // VALIDATION
        case SOProductStatusReceiptValidationInProgress: {
            UBILogTrace(@"Purchase VALIDATING: %@", productIdentifier);
        } break;
            
        case SOProductStatusReceiptValidationSucceeded: {
            UBILogTrace(@"Purchase VALIDATED: %@", productIdentifier);
        } break;
            
        case SOProductStatusReceiptValidationFailed: {
            UBILogTrace(@"Purchase DID FAIL VALIDATION: %@", productIdentifier);
            if (error != nil) {
                [FASAPIError alertStringForErrors:@[error]];
            }
        } break;
            
        default: break;
    }
    
    [self processPurchaseStatus:status forProductID:productIdentifier];
}

- (void)storeObserverWillRestore
{
    self.restoringProducts = YES;
    [NSNotificationCenter.defaultCenter
     postNotificationName:FASDataPurchasesRestoreDidUpdateNotification
     object:self
     userInfo:nil];
}

- (void)storeObserverDidFinishRestore
{
    self.restoringProducts = NO;
    
    // update the restore date when finished
    if ([[FASAPISession currentSession] isLoggedIn]) {
        [FASAppPreferences sharedInstance].purchasesRestoredDate = [NSDate date];
    }
    
    [NSNotificationCenter.defaultCenter
     postNotificationName:FASDataPurchasesRestoreDidUpdateNotification
     object:self
     userInfo:nil];
}

- (void)storeObserverRestoreDidFail:(NSNotification *)notification
{
    NSError* error = notification.userInfo[SOProductStatusNotificationErrorKey];
    UBILogTrace(@"Failed to restore products. Error was:\n%@", error);
    [FASAPIError displayAlertWithErrors:@[error]];
    
    self.restoringProducts = NO;
    [NSNotificationCenter.defaultCenter
     postNotificationName:FASDataPurchasesRestoreDidUpdateNotification
     object:self
     userInfo:nil];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Handle AppDelegate notification
#pragma mark -
///////////////////////////////////////////////
- (void)appDelegateNotification:(NSNotification *)notif
{
    if (notif.name == FASAppDelegateDidRegisterUserNotificationSettingsNotification) {
        if (_deferredTask == FASDataDeferredScheduleLocalNotification) {
            // doing nothing...
            _deferredTask = FASDataDeferredNone;
        }
    }
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark System  Settings
#pragma mark -
///////////////////////////////////////////////

- (void)openSettings {
    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    [[UIApplication sharedApplication] openURL:url];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark User Info
#pragma mark -
///////////////////////////////////////////////

- (void)preferencesUpdatedWithDictionary:(nullable NSDictionary *)prefDictionary
{
    UBILogTrace(@"%@", prefDictionary);
    
//    if (prefDictionary[FASAPISettingReceiveNewsKey]
//        && [prefDictionary[FASAPISettingReceiveNewsKey] isKindOfClass:[NSNumber class]]) {
//        [FASAppPreferences sharedInstance].notificationsRemotePromoEnabled = [(NSNumber*)prefDictionary[FASAPISettingReceiveNewsKey] boolValue];
//    }
}

- (NSUInteger)currentLicenseVersion
{
    NSUInteger currentLicenseVersion = [FASLicenseAgreement defaultLicenseAgreement].version;
    return currentLicenseVersion;
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark History
#pragma mark -
///////////////////////////////////////////////

- (BOOL)hasWorkoutSession
{
    return (BOOL)(self.currentWorkoutSession != nil);
}

- (void)clearCurrentWorkoutSession
{
    if ([self clearExerciseProgressRecords]) {
        self.currentWorkoutSession = nil;
        [self saveCacheForDataType:FASDataCacheWorkoutProgress];
    }
}

- (void)beginWorkoutSessionWithWorkout:(Workout *)workout
{
    [self beginWorkoutSessionWithWorkout:workout force:NO];
}

- (void)beginWorkoutSessionWithWorkout:(Workout *)workout
                                 force:(BOOL)isForced
{
    if (isForced) {
        [self clearCurrentWorkoutSession];
    }
    
    if (![self hasWorkoutSession]) {
        if (workout) {
            FASWorkoutSession* workoutSession = [[FASWorkoutSession alloc] initWithWorkoutID:workout.workout_id];
            self.currentWorkoutSession = workoutSession;
        }
        else {
            UBILogError("invalid workout");
        }
    }
    else {
        UBILogError("cannot start new session before current session is ended and submitted");
    }
    
    [self saveCacheForDataType:FASDataCacheWorkoutProgress];
}

- (void)endCurrentWorkoutSession:(BOOL)isCompleted
{
    [self endCurrentWorkoutSession:isCompleted submit:NO];
}

- (void)endCurrentWorkoutSession:(BOOL)isCompleted
                   submit:(BOOL)shouldSubmit
{
    if ([self hasWorkoutSession]) {
        if (isCompleted) {
            [self.currentWorkoutSession complete];
        }
        else {
            [self.currentWorkoutSession abort];
        }
        
        [self saveCacheForDataType:FASDataCacheWorkoutProgress];
        
        // send to history
        if (shouldSubmit) {
            [self submitCurrentWorkoutSession];
        }
    }
    else {
        UBILogWarning(@"current session not found");
    }
}

- (void)submitCurrentWorkoutSession {
    FASAPI* api = [FASAPI postWorkoutHistory:[self.currentWorkoutSession apiParams] delegate:self];
    [api invoke];
}

- (void)submitExerciseHistory:(NSArray *)exerciseHistory {
    NSMutableArray *exerciseHistoryDicts = [@[] mutableCopy];
    for (ExerciseProgress *epObject in exerciseHistory) {
        [exerciseHistoryDicts addObject:[epObject apiParams]];
    }
    FASAPI* api = [FASAPI postExerciseHistory:[self.currentWorkoutSession apiParams] exerciseHistory:exerciseHistoryDicts delegate:self];
    [api invoke];
}

- (void)rateCurrentWorkoutSessionWithRate:(NSUInteger)rate
{
    [self.currentWorkoutSession rateWorkout:rate];
    [self saveCacheForDataType:FASDataCacheWorkoutProgress];
}

- (void)pauseCurrentWorkoutSession:(BOOL)isPaused
{
    if ([self hasWorkoutSession]) {
        [self.currentWorkoutSession setOnPause:isPaused];
    }
    else {
        UBILogWarning(@"an attempt to pause current workout session when it does not exist");
    }
}

- (BOOL)clearExerciseProgressRecords
{
    NSManagedObjectContext* moc = self.workerContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:kFASDataCoreDataExerciseProgressEntityName];
    __block BOOL success = YES;
    [moc performBlockAndWait:^{
        NSError *fetchError = nil;
        NSArray *exerciseProgressObjects = [moc executeFetchRequest:request error:&fetchError];
        
        for (NSManagedObject* obj in exerciseProgressObjects) {
            [moc deleteObject:obj];
        }
        
        NSError *saveError = nil;
        if (![moc save:&saveError]) {
            success = NO;
            UBILogError("%@", [saveError localizedDescription]);
        }
    }];
    
    return success;
}

- (void)recordExerciseProgress:(Exercise *)exercise
                        circle:(NSInteger)circle
                          info:(ExerciseProgressEvent *)progressEvent
{
    if (!exercise) {
        UBILogError("invalid exercise object")
        return;
    }
    
    if (!self.currentWorkoutSession) {
        UBILogError("invalid session")
        return;
    }
    
    NSManagedObjectContext* moc = self.workerContext;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:kFASDataCoreDataExerciseProgressEntityName];
    // TODO: add to predicate: lap number
    // supposing all progress are in a single session
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"exercise_id = %@ && circle = %i", exercise.exercise_id, circle];
    request.predicate = predicate;
    
    __block NSManagedObjectID *exerciseObjectID = exercise.objectID;
    [moc performBlock:^{
        ExerciseProgress* exerciseProgress = nil;
        
        // search for existing
        NSError *fetchError = nil;
        NSArray *exerciseProgressObjects = [moc
                                            executeFetchRequest:request
                                            error:&fetchError];
        if (exerciseProgressObjects && exerciseProgressObjects.count!=0) {
            exerciseProgress = exerciseProgressObjects.firstObject;
        }
        
        // create new if not found
        if (!exerciseProgress) {
            exerciseProgress = [NSEntityDescription insertNewObjectForEntityForName:kFASDataCoreDataExerciseProgressEntityName
                                                             inManagedObjectContext:moc];
            
            // add basic attrib values
            NSError* exerciseError = nil;
            Exercise* exercise = (Exercise*)[moc existingObjectWithID:exerciseObjectID error:&exerciseError];
            if (exercise) {
                exerciseProgress.exercise_id = exercise.exercise_id;
                exerciseProgress.circle = circle;
                exerciseProgress.workout_id = self.currentWorkoutSession.workoutID;
            }
            else if (exerciseError) {
                UBILogError(@"%@", [exerciseError localizedDescription]);
            }
        }
        
        switch (progressEvent.type) {
            case ExerciseProgressEventTypeBegin: {
                exerciseProgress.status = ExerciseProgressStatusInProgress;
                exerciseProgress.startedAt = [NSDate date];
            } break;
            case ExerciseProgressEventTypeRepeatCount: {
                if (progressEvent.value && [progressEvent.value isKindOfClass:[NSNumber class]]) {
                    exerciseProgress.repeatCount = [(NSNumber *)progressEvent.value integerValue];
                }
            } break;
            case ExerciseProgressEventTypeEnd: {
                exerciseProgress.status = ExerciseProgressStatusFinished;
                exerciseProgress.finishedAt = [NSDate date];
            } break;
                
            default: break;
        }
        
        NSError *saveError = nil;
        if (![moc save:&saveError]) {
            UBILogError("%@", [saveError localizedDescription]);
        }
    }];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark GAI
#pragma mark -
///////////////////////////////////////////////

- (void)trackPageview:(NSString * _Nullable)page
{
    if (page) {
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:page];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    }
}

- (void)trackEvent:(NSString *)event withAction:(NSString *)action
{
    if (event && action) {
        id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:event              // Event category (required)
                                                              action:action             // Event action (required)
                                                               label:self.trackerLabel  // Event label
                                                               value:nil] build]];      // Event value
    }
}

- (NSString *)trackerLabel
{
    if (!_trackerLabel) {
        _trackerLabel = [NSString stringWithFormat:@"%@(%@)",
                         self.appVersion, self.buildVersion];
    }
    NSString *trackerLabelWithUserID = _trackerLabel;
    if (FASAPISession.currentSession.userInfo.isValidUserID) {
        trackerLabelWithUserID = [NSString stringWithFormat:@"%@/%@",
                                  _trackerLabel,
                                  FASAPISession.currentSession.userInfo.stringUserID];
    }
    return trackerLabelWithUserID;
}

@end
///////////////////////////////////////////////
#pragma mark -
#pragma mark FASAPI delegate implementation
#pragma mark -
///////////////////////////////////////////////
@implementation FASData (FASAPIDelegate)

- (void)FASAPIwillInvoke:(FASAPI *)theFASAPI
{
    switch (theFASAPI.method) {
            
        case FASAPIMethodGetWorkouts: {
            [NSNotificationCenter.defaultCenter postNotificationName:FASDataWorkoutsWillUpdateNotification
                                                              object:nil];
        } break;
            
        case FASAPIMethodGetExercisesWithOptions: {
            [NSNotificationCenter.defaultCenter postNotificationName:FASDataExercisesWillUpdateNotification
                                                              object:self
                                                            userInfo:theFASAPI.userInfo];
        } break;
         
        case FASAPIMethodGetWorkoutHistoryWithOptions: {
            [NSNotificationCenter.defaultCenter postNotificationName:FASDataWorkoutHistoryWillUpdateNotification
                                                              object:self
                                                            userInfo:nil];
        } break;
            
        default: break;
    }
}

- (void)FASAPIdidInvoke:(FASAPI *)theFASAPI
{
    switch (theFASAPI.method) {
            
        case FASAPIMethodGetWorkouts:
        case FASAPIMethodGetWorkoutsModifyDate:
        case FASAPIMethodGetExercisesWithOptions:
        case FASAPIMethodGetAttachmentsWithOptions:
        case FASAPIMethodGetAttachmentsForExercisesWithOptions:
        case FASAPIMethodGetStoreItems:{
            // Doing nothing...
            // ???: setWaitingForAPI here???
            UBILogTrace();
        } break;
            
        default: {
            UBILogWarning(@"unexpected method: %lu", (unsigned long)theFASAPI.method);
        }
    }
}

- (void)FASAPIdidFinish:(FASAPI *)theFASAPI
{
    switch (theFASAPI.method) {
        
        case FASAPIMethodGetWorkouts: {
            NSDictionary* dictResponse = theFASAPI.response;
            if (!dictResponse) {
                FASAPIError* error = [FASAPIError errorWithCode:FASAPIErrorCodeInvalidResponse
                                                          value:nil];
                [NSNotificationCenter.defaultCenter
                 postNotificationName:FASDataWorkoutsUpdateDidFailNotification
                 object:self
                 userInfo:@{@"errors": @[error]}];
            } else {
                NSManagedObjectContext* c = self.workerContext;
                BOOL didImport = [self importWorkoutsFromResponse:dictResponse
                                            shouldReplaceExisting:YES // always refresh the data
                                                        inContext:c];
                
                [NSNotificationCenter.defaultCenter
                 postNotificationName:FASDataWorkoutsDidUpdateNotification
                 object:self
                 userInfo:@{@"didImport":@(didImport)}];
            }
            
            // loading store here to make sure the workout statuses will be updated for existed objects
            [self loadStoreItemsFromRemote];
        } break;
            
        case FASAPIMethodGetWorkoutsModifyDate: {
            NSDictionary* dictResponse = [self dictionaryFromRunListResponse:theFASAPI.response];
            if (dictResponse) {
                if ([self workoutIDsToUpdateFromModifyDateResponse:dictResponse].count != 0) {
                    // TODO: update only IDs that actually need to be updated, but not ALL ids
                    [self loadWorkoutsFromRemote];
                }
            }
            else {
                // ???: failure - what to do here?
            }
        } break;
            
        case FASAPIMethodGetWorkoutHistoryWithOptions: {
            NSDictionary* dictResponse = [self dictionaryFromRunListResponse:theFASAPI.response];
            if (!dictResponse) {
                [NSNotificationCenter.defaultCenter
                 postNotificationName:FASDataWorkoutHistoryUpdateDidFailNotification
                 object:self
                 userInfo:nil];
            } else {
                NSManagedObjectContext* c = self.workerContext;
                BOOL didImport = [self importWorkoutHistoryFromResponse:dictResponse inContext:c];
                [NSNotificationCenter.defaultCenter
                 postNotificationName:FASDataWorkoutHistoryDidUpdateNotification
                 object:self
                 userInfo:@{@"didImport":@(didImport)}];
            }
        } break;
            
        case FASAPIMethodGetFaqEntries: {
            NSDictionary* dictResponse = [self dictionaryFromRunListResponse:theFASAPI.response];
            if (!dictResponse) {
                [NSNotificationCenter.defaultCenter
                 postNotificationName:FASDataFAQDidFailNotification
                 object:self
                 userInfo:nil];
            } else {
                NSManagedObjectContext* c = self.workerContext;
                BOOL didImport = [self importFAQEntriesFromResponse:dictResponse inContext:c];
                [NSNotificationCenter.defaultCenter
                 postNotificationName:FASDataFAQDidUpdateNotification
                 object:self
                 userInfo:@{@"didImport":@(didImport)}];
            }
        } break;
            
        case FASAPIMethodGetExercisesWithOptions: {
            NSDictionary* dictResponse = [self dictionaryFromRunListResponse:theFASAPI.response];
            if (dictResponse) {
                NSManagedObjectID* workoutObjectID = [theFASAPI.userInfo objectForKey:@"workoutObjectID"];
                
                if (workoutObjectID != nil && [workoutObjectID isKindOfClass:[NSManagedObjectID class]]) {
                    [self importExercisesFromResponse:dictResponse
                                      workoutObjectID:workoutObjectID];
                }
            } else {
                UBILogError(@"reponse would not be parsed because userInfo does not have correct workout object ID");
            }
        } break;
            
        case FASAPIMethodGetAttachmentsWithOptions: {
            NSDictionary* dictResponse = [self dictionaryFromRunListResponse:theFASAPI.response];
            if (dictResponse) {
                
                NSManagedObjectID* exerciseObjectID = [theFASAPI.userInfo objectForKey:@"exerciseObjectID"];
                
                if (exerciseObjectID != nil && [exerciseObjectID isKindOfClass:[NSManagedObjectID class]]) {
                    
                    [self importAttachmentsFromResponse:dictResponse
                                       exerciseObjectID:exerciseObjectID];
                }
            }
        } break;
            
        case FASAPIMethodGetAttachmentsForExercisesWithOptions: {
            NSDictionary* dictResponse = [self dictionaryFromRunListResponse:theFASAPI.response];
            if (dictResponse) {
                NSArray* exerciseIDs = [theFASAPI.userInfo objectForKey:@"exerciseIdentifiers"];
                NSArray* workoutExerciseIDs = [theFASAPI.userInfo objectForKey:@"workoutExerciseIdentifiers"];
                
                if (exerciseIDs != nil &&
                    [exerciseIDs isKindOfClass:[NSArray class]] &&
                    exerciseIDs.count != 0)
                {
                    [self importAttachmentsFromResponse:dictResponse
                                            exerciseIDs:exerciseIDs
                                     workoutExerciseIDs:workoutExerciseIDs];
                }
            }
        } break;
            
        case FASAPIMethodPostWorkoutHistory: {
            if (theFASAPI.response && [theFASAPI.response isKindOfClass:[NSArray class]]) {
                NSDictionary *element = ((NSArray *)(theFASAPI.response))[0];
                 if (element[FASAPIExecParamsKey] && element[FASAPIExecParamsKey][FASAPIIdentifier]) {
                     // ID for created workout
                     self.currentWorkoutSession.workoutHistoryID = element[FASAPIExecParamsKey][FASAPIIdentifier];
                     [self saveCacheForDataType:FASDataCacheWorkoutProgress];
                     
                     NSManagedObjectContext* moc = self.workerContext;
                     NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:kFASDataCoreDataExerciseProgressEntityName];
                     [moc performBlockAndWait:^{
                         NSError *fetchError = nil;
                         NSArray *exerciseHistory = [moc executeFetchRequest:request error:&fetchError];
                         [self submitExerciseHistory:exerciseHistory];
                     }];
                 } else {
                     UBILogError(@"didn't get ID for workout history")
                 }
            }
        } break;
            
        case FASAPIMethodPostExerciseHistory:{
            //@note additional actions?
            [self clearCurrentWorkoutSession];
        } break;
            
        case FASAPIMethodGetBestResults:{
            self.bestResults = [theFASAPI.response[@"result"] copy];
            [NSNotificationCenter.defaultCenter postNotificationName:FASDataBestResultsDidUpdateNotification object:self userInfo:nil];
        } break;
            
        case FASAPIMethodGetStoreItems: {
            NSDictionary* dictResponse = theFASAPI.response;
            if (!dictResponse) {
                FASAPIError* error = [FASAPIError errorWithCode:FASAPIErrorCodeInvalidResponse
                                                          value:nil];
                [NSNotificationCenter.defaultCenter
                 postNotificationName:FASDataStoreItemsUpdateDidFailNotification
                 object:self
                 userInfo:@{@"errors": @[error]}];
            } else {
                NSManagedObjectContext* c = self.workerContext;
                BOOL didImport = [self importStoreItemsFromResponse:dictResponse
                                                          inContext:c];
                
                [NSNotificationCenter.defaultCenter
                 postNotificationName:FASDataStoreItemsDidUpdateNotification
                 object:self
                 userInfo:@{@"didImport":@(didImport)}];
            }
        } break;
            
        default: {
            UBILogWarning(@"unexpected method: %lu", (unsigned long)theFASAPI.method);
        }
    }
}

- (void)FASAPIdidFail:(FASAPI *)theFASAPI
{
    dispatch_async(dispatch_get_main_queue(), ^{
        switch (theFASAPI.method) {
            case FASAPIMethodGetWorkouts: {
                [NSNotificationCenter.defaultCenter
                 postNotificationName:FASDataWorkoutsUpdateDidFailNotification
                 object:self
                 userInfo:@{@"errors": theFASAPI.errors}];
            } break;
                
            case FASAPIMethodGetWorkoutsModifyDate: {
                UBILogError(@"FASAPIMethodGetWorkoutsModifyDate DID FAIL");
            } break;
                
            case FASAPIMethodGetExercisesWithOptions: {
                [NSNotificationCenter.defaultCenter postNotificationName:FASDataExercisesUpdateDidFailNotification
                                                                  object:self
                                                                userInfo:theFASAPI.userInfo];
                [FASAPIError displayAlertWithErrors:theFASAPI.errors];
            } break;
                
            case FASAPIMethodGetAttachmentsWithOptions: {
                UBILogError(@"FASAPIMethodGetAttachmentsWithOptions DID FAIL");
                [FASAPIError displayAlertWithErrors:theFASAPI.errors];
            } break;
                
            case FASAPIMethodGetAttachmentsForExercisesWithOptions: {
                UBILogError(@"FASAPIMethodGetAttachmentsForExercisesWithOptions DID FAIL");
                [FASAPIError displayAlertWithErrors:theFASAPI.errors];
            } break;
            
            case FASAPIMethodGetStoreItems: {
                UBILogError(@"%@", theFASAPI.errors);
            } break;
                
            default: {
                UBILogError(@"FASData did get DidFail from unexpected method %lu", (unsigned long)theFASAPI.method);
            }
        }
    });
}

@end

///////////////////////////////////////////////
#pragma mark -
#pragma mark UIAlertViewDelegate
#pragma mark -
///////////////////////////////////////////////
@implementation FASData (UIAlertViewDelegate)

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    if (alertView.tag == FASDataUIR...) {
//    }
}

@end