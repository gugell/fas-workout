//
//  FASMenuViewController.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 2/21/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit
import Foundation

private var kLoggingIn = 0 // KVO context var

class FASMenuViewController: FASViewController {
    private let actionController = FASActionController()
    
    ///////////////////////////////////////////////
    // MARK: - Class Presentation -
    ///////////////////////////////////////////////
    
    class func presentFromViewController(viewController: UIViewController) {
        let vc: FASMenuViewController = FASMenuViewController()
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .PageSheet;
        viewController.presentViewController(nav, animated: true, completion: nil)
    }
    
    ///////////////////////////////////////////////
    // MARK: - Initializers -
    ///////////////////////////////////////////////
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        self.title = "Menu"
        self.hideBackButtonTitle = true
        FASAPISession.currentSession().addObserver(self, forKeyPath: "loggingIn", options: .New, context: &kLoggingIn)
        actionController.viewController = self
    }
    
    deinit {
        FASAPISession.currentSession().removeObserver(self, forKeyPath: "loggingIn", context: &kLoggingIn)
    }
    ///////////////////////////////////////////////
    // MARK: - View Controller -
    ///////////////////////////////////////////////
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if presentingViewController != nil {
            let closeButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "cross"), style: .Plain, target: self, action: Selector("dismissAction"))
            navigationItem.leftBarButtonItem = closeButton
        }
        
        let logoutButton: UIBarButtonItem = UIBarButtonItem(title: "Logout", style: .Plain, target: self, action: Selector("doLogout"))
        navigationItem.rightBarButtonItem = logoutButton
        
        // create background image view in code
        let imgView = UIImageView(frame: CGRectZero)
        self.backgroundImageView = imgView
        self.view.addSubview(self.backgroundImageView)
        self.backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        self.backgroundImageView.contentMode = .ScaleAspectFill
        view.setNeedsUpdateConstraints()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar(.Dark)
        addMenuTableView()
    }
    
    override func viewDidDisappear(animated: Bool) {
        removeMenuTableView()
        
        super.viewDidDisappear(animated)
    }
    
    ///////////////////////////////////////////////
    // MARK: - Layout -
    ///////////////////////////////////////////////
    override func updateViewConstraints() {
        updateEdgeToEdgeConstraints(self.backgroundImageView)
        
        super.updateViewConstraints()
    }
    
    ///////////////////////////////////////////////
    // MARK: - Table View Controller -
    ///////////////////////////////////////////////
    private func addMenuTableView() {
        let menuTableVC = FASMenuTableViewController(style: .Grouped)
        menuTableVC.delegate = self
        addChildViewController(menuTableVC)
        let frame = self.view.bounds
        menuTableVC.view.frame = frame
        view.addSubview(menuTableVC.view)
        menuTableVC.didMoveToParentViewController(self)
    }
    
    private func removeMenuTableView() {
        if let tableVC: FASMenuTableViewController? = childViewControllers.first as? FASMenuTableViewController {
            tableVC!.delegate = nil
            
            tableVC!.willMoveToParentViewController(nil)
            tableVC!.view.removeFromSuperview()
            tableVC!.removeFromParentViewController()
        }
    }

    ///////////////////////////////////////////////
    // MARK: - Button Actions -
    ///////////////////////////////////////////////

    @objc func dismissAction() {
        doCloseMenu()
    }
    
    ///////////////////////////////////////////////
    // MARK: - Concrete Actions -
    ///////////////////////////////////////////////
    
    func doCloseMenu() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func doLogout() {
        FASAPISession.currentSession().logoutWithConfirmation(true)
        doCloseMenu()
    }
    
    ///////////////////////////////////////////////
    // MARK: - KVO -
    ///////////////////////////////////////////////
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if context == &kLoggingIn {
            // doing nothing....
        } else {
            super.observeValueForKeyPath(keyPath, ofObject: object, change: change, context: context)
        }
    }
}

///////////////////////////////////////////////
// MARK: - FASMenuTableViewControllerDelegate -
///////////////////////////////////////////////

extension FASMenuViewController : FASMenuTableViewControllerDelegate {
    func menuTableViewController(menuTableVC: FASMenuTableViewController, didSelectItemID itemID: FASSettingsDataSourceItemID) {
        var nextVC: UIViewController?
        
        switch itemID {
        case .IDProfile:
            // access mainStoryboard, because current VC is not instantiated from Main storyboard
            nextVC = self.mainStoryboard.instantiateViewControllerWithIdentifier("UserProfileViewControllerStoryboardID")
        case .IDExercisesBase:
            nextVC = self.mainStoryboard.instantiateViewControllerWithIdentifier("FASExerciseBaseViewController")
        case .IDFacebookInviteFriends:
            actionController.fbInviteFriends({ (error) -> Void in
                if error != nil {
                    printlnDebugError("invite friends FAILED: \(error)")
                }
                else {
                    printlnDebugTrace("invite friends SUCCEEDED")
                }
            })
        case .IDShareApp:
            actionController.fbShareContentWithItem(FASAppInfo(), completion: { (error) -> Void in
                if error != nil {
                    printlnDebugError("share app FAILED: \(error)")
                }
                else {
                    printlnDebugTrace("share app SUCCEEDED")
                }
            })
        case .IDRestorePurchases:
            FASData.sharedData().restorePurchases()
        case .IDNotificationsSettings:
            if let settingsVC = self.mainStoryboard.instantiateViewControllerWithIdentifier("SettingsStoryboardID") as? FASSettingsViewController {
                let settingsType: UBISettingsType = .Notifications
                settingsVC.settingsType = settingsType
                settingsVC.title = settingsType.description
                nextVC = settingsVC
            }
        case .IDDebug:
            if let settingsVC = self.mainStoryboard.instantiateViewControllerWithIdentifier("SettingsStoryboardID") as? FASSettingsViewController {
                let settingsType: UBISettingsType = .Debug
                settingsVC.settingsType = settingsType
                settingsVC.title = settingsType.description
                nextVC = settingsVC
            }
        case .IDFeedback:
            actionController.mailSendFeedback({ (error) -> Void in
                if error != nil {
                    printlnDebugError("send mail FAILED: \(error)")
                }
                else {
                    printlnDebugTrace("send mail SUCCEEDED")
                }
            })
        case .IDFAQ:
            nextVC = mainStoryboard.instantiateViewControllerWithIdentifier("FAQViewController")
        case .IDRateApp:
            #if TARGET_IPHONE_SIMULATOR
                printlnDebugWarning("The iTunes App Store is not supported in the iOS simulator.")
            #else
                if let appReviewPageURL = NSURL(string: FASEnvironment.AppStoreReviewURL()) {
                    printlnDebugTrace("Open iTunes App Store")
                    UIApplication.sharedApplication().openURL(appReviewPageURL)
                }
            #endif
        default:
            0
        }
        
        if let notNilNextVC = nextVC {
            self.navigationController?.pushViewController(notNilNextVC, animated: true)
        }
    }
}
