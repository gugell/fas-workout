//
//  UBIObjectRetainer.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 3/19/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UBIObjectRetainer : NSObject

@property (nonatomic, strong)       id      object;

@end
