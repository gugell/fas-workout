//
//  UIImage+Alpha.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/1/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <UIKit/UIKit.h>

// Helper methods for adding an alpha layer to an image
@interface UIImage (Alpha)
- (BOOL)hasAlpha;
- (UIImage *)imageWithAlpha;
- (UIImage *)transparentBorderImage:(NSUInteger)borderSize;
@end
