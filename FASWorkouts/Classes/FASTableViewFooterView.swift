//
//  FASTableViewFooterView.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/15/15.
//  Copyright © 2015 FAS. All rights reserved.
//

import UIKit

class FASTableViewFooterView: UITableViewHeaderFooterView {
    
    internal override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        self.textLabel?.font = UIFont.systemFontOfSize(10.0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.textLabel?.font = UIFont.systemFontOfSize(10.0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let _ = self.textLabel {
            var textLabelFrame = self.textLabel!.frame
            textLabelFrame.origin = CGPoint(x: 10.0, y: 0)
            self.textLabel!.frame = textLabelFrame
        }
    }
}
