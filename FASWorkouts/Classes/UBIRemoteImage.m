//
//  UBIRemoteImage.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/1/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "UBIRemoteImage.h"
#import "UIImage+Resize.h"
#import "FASAPISession.h"
#import "UBILogger.h"
#import "SDWebImageDownloader.h"

#define CHECK_FOR_NATIVE_SCALE 1

static NSOperationQueue *sUBIRemoteImageOpQueue;
static CGFloat scaleFactor;

@interface UBIRemoteImage ()
@property (nonatomic, readwrite) UBIRemoteImageStatus status;
@property (nonatomic, readwrite) BOOL isLoading;
@property (nonatomic, readwrite) UBIRemoteImageContentType imageType;
@property (nonatomic, readwrite) NSError* error;
@end

@implementation UBIRemoteImage
@synthesize image = _image;

+ (void)initialize
{
    if (!sUBIRemoteImageOpQueue) {
        sUBIRemoteImageOpQueue = [NSOperationQueue new];
    }
    UIScreen* mainScreen = [UIScreen mainScreen];
#if CHECK_FOR_NATIVE_SCALE
    if ([mainScreen respondsToSelector:@selector(nativeScale)]) {
        scaleFactor = mainScreen.nativeScale;
    } else {
        scaleFactor = mainScreen.scale;
    }
#else
    scaleFactor = mainScreen.scale;
#endif
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _status = UBIRemoteImageStatusIdle;
        _isLoading = NO;
        _maxSize = CGSizeZero;
        _imageType = UBIRemoteImageContentTypeUnknown;
        _error = nil;
    }
    return self;
}

- (UIImage *)image
{
    if (!_image) {
        if (self.status == UBIRemoteImageStatusDefaultImage) {
            _image = [UIImage imageNamed:self.defaultImageName];
        }
        
        else if (self.status == UBIRemoteImageStatusIdle) {
            self.status = UBIRemoteImageStatusPreparingToLoad;
            self.isLoading = YES;
            [sUBIRemoteImageOpQueue addOperationWithBlock:^{
                [self loadImageFromFile];
                
                if (self.image) {
                    self.status = UBIRemoteImageStatusIdle;
                    self.isLoading = NO;
                }
                
                else if (self.URL != nil) {
                    self.status = UBIRemoteImageStatusLoadingFromURL;
                    self.error = nil;
                    self.imageType = UBIRemoteImageContentTypeUnknown;
                    
                   // [self loadProtectedImageFromURL:self.URL completion:^(UIImage* image, UBIRemoteImageContentType contentType, NSError* loadingFromURLError){
                    [self loadImageFromURL:self.URL completion:^(UIImage* image, UBIRemoteImageContentType contentType, NSError* loadingFromURLError){
                        if (!image) {
                            self.error = loadingFromURLError;
                            self.imageType = contentType;
                            self.status = UBIRemoteImageStatusFailedToLoadFromURL;
                            self.image = [UIImage imageNamed:self.defaultImageName];
                        }
                        
                        else {
                            self.image = image;
                            self.imageType = contentType;
                            self.status = UBIRemoteImageStatusLoadedFromURL;
                        }
                        
                       // self.status = UBIRemoteImageStatusIdle;
                        self.isLoading = NO;
                    }];
                }
                
                else {
                    // кіна не буде
                    self.image = [UIImage imageNamed:self.defaultImageName];
                    self.status = self.image ? UBIRemoteImageStatusDefaultImage : UBIRemoteImageStatusNoImage;
                    
                    self.isLoading = NO;
                }
            }];
        }
    }
    
    return _image;
}

- (void)setImage:(UIImage *)image
{
    self.thumbnail = nil;
    _image = image;
}

- (UIImage *)thumbnail
{
    if (!_thumbnail) {
        _thumbnail = [self.image
                      resizedImageWithContentMode:UIViewContentModeScaleAspectFill
                      bounds:self.maxSize
                      zoomLevel:1.0
                      interpolationQuality:kCGInterpolationDefault];
    }
    return _thumbnail;
}

- (void)loadImageFromFile
{
    UIImage *imageFromFile = nil;
    if (self.filepath) {
        self.status = UBIRemoteImageStatusLoadingFromFile;
        imageFromFile = [UIImage imageWithContentsOfFile:self.filepath];
        if (imageFromFile) {
            // TODO: set contentType for image loaded from file
            self.image = imageFromFile;
            self.status = UBIRemoteImageStatusLoadedFromFile;
        }
    }
}

- (void)loadProtectedImageFromURL:(NSURL *)imageURL completion:(void (^)(UIImage*, UBIRemoteImageContentType, NSError*))completionBlock
{
    if (imageURL && FASAPISession.currentSession.token) {
        // TODO: consider using single NSURLSession for all image loading
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        // TODO: the remoteImage should not know about the URLSession
        [sessionConfig setHTTPAdditionalHeaders:@{@"Authorization": FASAPISession.currentSession.token}];
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig
                                                              delegate:nil//self
                                                         delegateQueue:sUBIRemoteImageOpQueue];
        NSURLSessionDownloadTask *getImageTask =
        [session downloadTaskWithURL:imageURL completionHandler:^(NSURL *location, NSURLResponse* response, NSError *error) {
            @autoreleasepool {
                if (error) {
                    UBILogError(@"%@ Failed to load image from URL: %@\nResponse:\n%@\nError was:\n%@", self, imageURL, response, error.localizedDescription);
                    if (completionBlock) {
                        completionBlock(nil, UBIRemoteImageContentTypeUnknown, error);
                    }
                } else {
                    NSData *imageData = [NSData dataWithContentsOfURL:location];
                    if (imageData) {
                        UIImage* imageFromURL = nil;
                        imageFromURL = [[UIImage alloc] initWithData:imageData scale:scaleFactor];
                        if (imageFromURL) {
                            if (completionBlock) {
                                UBIRemoteImageContentType ct = [self imageContentTypeFromResponse:response];
                                if (ct == UBIRemoteImageContentTypeUnknown) {
                                    UBILogWarning(@"Image content type is unknown after an attempt to read response header.");
                                }
                                completionBlock(imageFromURL, ct, nil);
                            }
                        } else {
                            UBILogError(@"%@ Failed to initialize imagefrom data. Response:%@", self, response);
                            if (completionBlock) {
                                completionBlock(nil, UBIRemoteImageContentTypeUnknown, nil);
                            }
                        }
                    } else {
                        UBILogError(@"Failed to create image data from location (%@).\nResponse:\n%@", location, response);
                        if (completionBlock) {
                            completionBlock(nil, UBIRemoteImageContentTypeUnknown, nil);
                        }
                    }
                }
            }
        }];
        [getImageTask resume];
    }
}


- (void)loadImageFromURL:(NSURL *)imageURL completion:(void (^)(UIImage*, UBIRemoteImageContentType, NSError*))completionBlock
{
    
 
    if (!imageURL) {
        
       }
        [SDWebImageDownloader.sharedDownloader downloadImageWithURL:imageURL
                                                            options:0
                                                           progress:^(NSInteger receivedSize, NSInteger expectedSize)
         {
             // progression tracking code
         }
                                                          completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished)
         {
             if (image )
             {
                 if (completionBlock) {
                     
                     UBIRemoteImageContentType ct = [self contentTypeForImageData:data];
                     
                     if (ct == UBIRemoteImageContentTypeUnknown) {
                         UBILogWarning(@"Image content type is unknown after an attempt to read response header.");
                     }
                     
                     completionBlock(image, ct, nil);
                 }
             }else {
                
                 if(error) {
                     
                     UBILogError(@"Failed to initialize image from %@", imageURL);
                     
                     if (completionBlock) {
                         completionBlock(nil, UBIRemoteImageContentTypeUnknown, nil);
                     }

                     
                 }
             }
         }];
     }

- (BOOL)shouldHaveImage
{
    return self.URL != nil;
}

- (void)clearDefaultImage
{
    if (self.status == UBIRemoteImageStatusDefaultImage) {
        self.image = nil;
        self.status = UBIRemoteImageStatusIdle;
    }
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark HTTP helpers
#pragma mark -
///////////////////////////////////////////////
- (UBIRemoteImageContentType)imageContentTypeFromResponse:(NSURLResponse *)resp
{
    NSHTTPURLResponse* httpURLResponse = (NSHTTPURLResponse *)resp;
    NSString *contentType = httpURLResponse.allHeaderFields[@"Content-Type"];
    if ([contentType isEqualToString:@"image/jpeg"]) {
        return UBIRemoteImageContentTypeJPEG;
    }
    else if ([contentType isEqualToString:@"image/png"]) {
        return UBIRemoteImageContentTypePNG;
    }
    else if ([contentType isEqualToString:@"image/gif"]) {
        return UBIRemoteImageContentTypeGIF;
    }
    else if ([contentType isEqualToString:@"image/tiff"]) {
        return UBIRemoteImageContentTypeTIFF;
    }
    
    return UBIRemoteImageContentTypeUnknown;
}

- (UBIRemoteImageContentType)contentTypeForImageData:(NSData *)imageData
{
    uint8_t c;
    [imageData getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return UBIRemoteImageContentTypeJPEG;
        case 0x89:
            return UBIRemoteImageContentTypePNG;
        case 0x47:
            return UBIRemoteImageContentTypeGIF;
        case 0x49:
        case 0x4D:
            return UBIRemoteImageContentTypeTIFF;
    }
    return UBIRemoteImageContentTypeUnknown;
}
/*
 + (NSString *)contentTypeForImageData:(NSData *)data {
 uint8_t c;
 [data getBytes:&c length:1];
 
 switch (c) {
 case 0xFF:
 return @"image/jpeg";
 case 0x89:
 return @"image/png";
 case 0x47:
 return @"image/gif";
 case 0x49:
 case 0x4D:
 return @"image/tiff";
 }
 return nil;
 }
 
 */
///////////////////////////////////////////////
#pragma mark -
#pragma mark NSObject overrides
#pragma mark -
///////////////////////////////////////////////
- (NSString *)debugDescription
{
    return [NSString stringWithFormat:@"%@\nimage = %@\nURL = %@\nfilepath = %@\nis loading = %@\nstatus = %lu\nerror = %@", [super debugDescription], self.image, self.URL, self.filepath, (self.isLoading ? @"YES" : @"NO"), (unsigned long)self.status, self.error.localizedDescription];
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Xcode debug
#pragma mark -
///////////////////////////////////////////////
- (id)debugQuickLookObject
{
    UIView* contentView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 400.0f, 200.0f)];
    UIImageView* imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 200.0f, 200.0f)];
    UIImageView* thumbView = [[UIImageView alloc] initWithFrame:CGRectMake(200.0f, 0.0f, 200.0f, 200.0f)];
    [contentView addSubview:imgView];
    [contentView addSubview:thumbView];
    imgView.image = self.image;
    thumbView.image = self.thumbnail;
    UILabel* imgLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 180, 50)];
    UILabel* thumbLabel = [[UILabel alloc] initWithFrame:CGRectMake(210, 10, 180, 50)];
    imgLabel.text = @"image";
    thumbLabel.text = @"thumbnail";
    [contentView addSubview:imgLabel];
    [contentView addSubview:thumbLabel];
    return contentView;
}
@end
