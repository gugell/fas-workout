//
//  NSDate+UnityBase.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/23/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import Foundation

private var sDateFormatter: NSDateFormatter = NSDateFormatter()

//
// Convert the date string received from UnityBase to NSDate object
//
extension NSDate
{
    convenience
    init?(fromUnityBaseString dateString: String) {
        sDateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"
        sDateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
        sDateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        if let date: NSDate = sDateFormatter.dateFromString(dateString) {
            self.init(timeInterval: 0.0, sinceDate: date)
        } else {
            // Note: The line blow is needed because of "All stored properties of a class 
            // instance must be initialized before returning nil from an initializer" compile error.
            // The swift 1.1 compiler is currently unable to destroy partially initialized classes in all cases, 
            // so it disallows formation of a situation where it would have to.  We consider this a bug to be fixed 
            // in future releases, not a feature.
            self.init()
            return nil
        }
    }
    
    class func dateFromUnityBaseString(dateString: String) -> NSDate? {
        return NSDate(fromUnityBaseString: dateString)
    }
}