//
//  UINavigationController+StatusBarStyle.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/4/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

extension UINavigationController {
    override public func childViewControllerForStatusBarStyle() -> UIViewController? {
        return visibleViewController
    }

    override public func childViewControllerForStatusBarHidden() -> UIViewController? {
        return visibleViewController
    }
}
