//
//  UIScrollView+ScrollableToTop.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/12/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit

@objc protocol ScrollableToTop {
    func scrollToTopAnimated(animated: Bool)
}

extension UIScrollView: ScrollableToTop {
    // not tested
    func scrollToTopAnimated(animated: Bool) {
        self.setContentOffset(CGPointMake(0.0, -self.contentInset.top), animated: animated)
    }
}

extension UITableView {
    override func scrollToTopAnimated(animated: Bool) {
        self.scrollRectToVisible(CGRectMake(0.0, 0.0, 1.0, 1.0), animated: animated)
    }
}

extension UITextView {
    override func scrollToTopAnimated(animated: Bool) {
       self.scrollRangeToVisible(NSRange(location:0, length:0))
    }
}