//
//  FASRunList.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/8/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "FASRunList.h"

@implementation FASRunList

- (instancetype)initWithFieldList:(NSArray *)fieldList entity:(NSString *)entity
{
    self = [super init];
    if (self) {
        self.entity = entity;
        self.method = UBIRunListMethodSelect;
        self.fieldList = fieldList;
        self.options.limit = @(100);
        self.options.start = @(0);
    }
    return self;
}

@end
