//
//  FASTwoLabelsView.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/15/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

enum FASTwoLabelsViewStyle {
    case Default, Reverse
}

class FASTwoLabelsView: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        style = .Default
        
        // debug layout
//        backgroundColor = UIColor.cyanColor()
//        titleLabel.backgroundColor = UIColor.redColor()
//        subtitleLabel.backgroundColor = UIColor.blueColor()
    }
    
    var style: FASTwoLabelsViewStyle? {
        didSet  {
            if let newStyle = style {
                let smallFont = UIFont.regularBabasFontOfSize(15.0)
                let bigFont = UIFont.boldBabasFontOfSize(36.0)
                
                switch (newStyle) {
                case .Reverse:
                    titleLabel.font = bigFont
                    subtitleLabel.font = smallFont
                    
                case .Default:
                    titleLabel.font = smallFont
                    subtitleLabel.font = bigFont
                }
                
                updateColorScheme(newStyle)
            }
        }
    }
    
    override func tintColorDidChange() {
        if self.tintAdjustmentMode == .Dimmed {
            // dimm
            if let newStyle = style {
                switch (newStyle) {
                case .Reverse:
                    titleLabel.textColor = UIColor.grayColor()
                    subtitleLabel.textColor = UIColor.darkGrayColor()
                case .Default:
                    titleLabel.textColor = UIColor.darkGrayColor()
                    subtitleLabel.textColor = UIColor.grayColor()
                }
            }
        }
        else {
            // restore
            updateColorScheme(self.style)
        }
    }
    
    private func updateColorScheme(currentStyle: FASTwoLabelsViewStyle?) {
        if let newStyle = currentStyle {
            switch (newStyle) {
            case .Reverse:
                titleLabel.textColor = UIColor.whiteColor()
                subtitleLabel.textColor = UIColor.fasYellowColor()
            case .Default:
                titleLabel.textColor = UIColor.fasYellowColor()
                subtitleLabel.textColor = UIColor.whiteColor()
            }
        }
    }
}
