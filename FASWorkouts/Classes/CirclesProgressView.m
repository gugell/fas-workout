//
//  CirclesProgressView.m
//  FAS Workouts
//
//  Created by Vladimir Stepanchenko on 12/15/14.
//  Copyright (c) 2014 Vladimir Stepanchenko. All rights reserved.
//

#import "CirclesProgressView.h"

@interface CirclesProgressView ()

@property(nonatomic) CGFloat delimiterWidth;
@property(nonatomic) NSUInteger segments;
@property(nonatomic) CGFloat radius;

@property(nonatomic) CGFloat current;
@property(nonatomic) CGFloat rawProgress;

@property(nonatomic, retain) CAShapeLayer *backgroundLayer;
@property(nonatomic, retain) CAShapeLayer *progressLayer;

@property(nonatomic) CGFloat total;

@end

@implementation CirclesProgressView

- (id)init
{
  self = [super init];
  if (self) {
    [self setup];
  }
  return self;
}

- (id)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {
    [self setup];
  }
  return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
  self = [super initWithCoder:aDecoder];
  if (self) {
    [self setup];
  }
  return self;
}

-(void)setup
{
  self.backgroundColor = [UIColor clearColor];
  
  _backgroundLayer = [CAShapeLayer new];
  
  _backgroundLayer.fillColor = [UIColor colorWithRed:70./255.0 green:68./255.0 blue:69./255.0 alpha:1.].CGColor;
  _backgroundLayer.frame = self.bounds;
  
  _progressLayer = [CAShapeLayer new];
  _progressLayer.fillColor = [UIColor colorWithRed:248./255. green:228./255. blue:1.0/255. alpha:1.].CGColor;
  _progressLayer.frame = self.bounds;
  _total = 1.0;
  // Count radii
  _radius = self.bounds.size.height / 2;
  _delimiterWidth = 10.;
  _segments = (self.bounds.size.width) / (_radius * 2 + _delimiterWidth);
  
  [self.layer addSublayer:_backgroundLayer];
  [self.layer addSublayer:_progressLayer];
}

-(void)drawBackground
{
  CGMutablePathRef pathRef = CGPathCreateMutable();
  CGFloat currentX = (self.bounds.size.width - _segments * (_radius * 2 + _delimiterWidth) + _delimiterWidth) / 2.;
  for (int i = 0; i < _segments; i++) {
    // Create the outer ring segment
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(currentX, self.bounds.size.height, _radius * 2, _radius * 2)];
    currentX += _radius * 2 + _delimiterWidth;
    // Add the segment to the path
    CGPathAddPath(pathRef, NULL, path.CGPath);
  }
  
  // Set the path
  _backgroundLayer.path = pathRef;
  
  CGPathRelease(pathRef);
}

-(void)drawProgress
{
  CGMutablePathRef pathRef = CGPathCreateMutable();
  CGFloat currentX = (self.bounds.size.width - _segments * (_radius * 2 + _delimiterWidth) + _delimiterWidth) / 2.;
  for (int i = 0; i < _current; i++) {
    // Create the outer ring segment
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(currentX, self.bounds.size.height, _radius * 2, _radius * 2)];
    currentX += _radius * 2 + _delimiterWidth;
    // Add the segment to the path
    CGPathAddPath(pathRef, NULL, path.CGPath);
  }
  
  // Set the path
  _progressLayer.path = pathRef;
  
  CGPathRelease(pathRef);
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
  // Drawing code
  [self drawBackground];
  [self drawProgress];
}

-(void)setTotal:(CGFloat)total {
  _total = total;
  // Reset progress
  _current = 0.;
  [self setNeedsDisplay];
}

-(void)setProgress:(CGFloat)progress {
  _rawProgress = progress;
  if (_total > 0. && _segments > 1)
    _current = ((CGFloat)progress / (CGFloat)_total) * _segments;
  // Check for possible overflow
  if (_current > _segments)
    _current = _segments;
  [self setNeedsDisplay];
}

- (void)layoutSubviews {
  // Adapt to rotation
  _segments = (self.bounds.size.width) / (_radius * 2 + _delimiterWidth);
  [self setProgress:_rawProgress];
  [self setNeedsDisplay];
}

@end
