//
//  FASFormSwitchCell.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 8/16/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit

@objc protocol FASFormSwitchCellDelegate {
    func fasFormSwitchCellValueChanged(switchCell: FASFormSwitchCell)
}

class FASFormSwitchCell: UITableViewCell {
    @IBOutlet weak var switchControl: UISwitch! {
        didSet {
            switchControl?.addTarget(self, action: "valueDidChange:", forControlEvents: UIControlEvents.ValueChanged)
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    
    var delegate: FASFormSwitchCellDelegate?
    
    var setting: UBISetting? {
        didSet {
            if let aSetting = setting {
                titleLabel?.text = aSetting.title
                switchControl.setOn( (aSetting.value as? Bool ?? false) , animated: false)
            }
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        setting = nil
    }
    
    func valueDidChange(sender: AnyObject!) {
        if let switchCtrl = sender as? UISwitch where switchCtrl == switchControl {
            delegate?.fasFormSwitchCellValueChanged(self)
        }
    }
}
