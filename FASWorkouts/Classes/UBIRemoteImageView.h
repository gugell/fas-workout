//
//  UBIRemoteImageView.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/1/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UBIRemoteImage.h"

@interface UBIRemoteImageView : UIView

@property (nonatomic, strong) UBIRemoteImage *remoteImage;

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIActivityIndicatorView *loadingIndicatorView;

@property (nonatomic) BOOL showThumbnail;
@property (nonatomic) BOOL showLoadingActivity;

// Style customization
-(void)applyStyleForRemoteImageView;

@end
