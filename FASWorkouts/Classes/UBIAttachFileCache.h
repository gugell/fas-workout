//
//  UBIAttachDownloadManager.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/17/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, AttachDownloadRetval) {
    AttachDownloadFalse = 0,
    AttachDownloadTrue,
    AttachDownloadExistsLocal,
};

@class UBIAttachFileCache;
@protocol UBIAttachFileCacheDelegate <NSObject>
@optional
- (void)attachFileCache:(UBIAttachFileCache *)fileCache didChangeDownloadProgress:(NSNumber *)progress forDocumentID:(NSNumber*)documentID;
@optional
- (void)attachFileCache:(UBIAttachFileCache *)fileCache didFinishDownloadingDocumentWithID:(NSNumber*)documentID;
@end

@interface UBIAttachFileCache : NSObject

@property (nonatomic, weak) id <UBIAttachFileCacheDelegate> delegate;

- (AttachDownloadRetval)downloadDocumentWithID:(NSNumber*)documentID
                                        saveAs:(NSString *)filename
                                         force:(BOOL)force;

//- (BOOL)downloadAttach:(Attach*)attach;

// filepath to resources w/ specified filename
- (NSURL *)cacheURLPathForFile:(NSString *)filename;
- (NSString *)cachePathForFile:(NSString *)filename;

- (BOOL)isFileExists:(NSString *)filename;
- (BOOL)isFileExists:(NSString *)filename withMD5:(NSString*)md5Sum;

@end
