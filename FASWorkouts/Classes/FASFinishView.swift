//
//  FASFinishView.swift
//  FASWorkouts
//
//  Created by Vladimir Stepanchenko on 3/16/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import Foundation
import UIKit

enum FASFinishViewCloseOption {
    case None, Share
}

protocol FASFinishViewDelegate {
    func finishViewDidClose(finishView: FASFinishView, withOption option: FASFinishViewCloseOption)
}

class FASFinishView: UIVisualEffectView, CustomRateViewDelegate {

  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
  }

  // MARK: Properties
  
  var delegate: FASFinishViewDelegate?
  
  @IBOutlet weak var okButton: UIButton!
  @IBOutlet weak var shareButton: UIButton!
  @IBOutlet weak var title: UILabel!
  
  @IBOutlet weak var howHeavyTitle: UILabel!
  
  @IBOutlet weak var motivationLabel: UILabel!
  @IBOutlet weak var easyLabel: UILabel!
  @IBOutlet weak var mediumLabel: UILabel!
  @IBOutlet weak var hardLabel: UILabel!
  
  @IBOutlet weak var customRateView: CustomRateView!
  
  var difficultyRate : UInt = 1
  
  // MARK: UIKit overrides
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.customRateView.delegate = self
    configureControls()
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "rotated", name: UIDeviceOrientationDidChangeNotification, object: nil)
    self.rotated()
  }
  
  deinit {
    let notifCenter = NSNotificationCenter.defaultCenter()
    notifCenter.removeObserver(self)
  }
  
  // MARK: Methods
  
  @IBAction func buttonDidPress(sender: AnyObject) {
    switch sender {
    case okButton as UIButton:
      delegate?.finishViewDidClose(self, withOption: .None)
    case shareButton as UIButton:
      delegate?.finishViewDidClose(self, withOption: .Share)
    default:
      printlnDebugWarning("Unexpected sender: \(sender)")
    }
  }
  
  func valueChanged(value: UInt) {
    self.difficultyRate = value
    switch value {
    case 0:
      easyLabel.textColor = UIColor.fasYellowColor()
      mediumLabel.textColor = UIColor.whiteColor()
      hardLabel.textColor = UIColor.whiteColor()
    case 1:
      easyLabel.textColor = UIColor.whiteColor()
      mediumLabel.textColor = UIColor.fasYellowColor()
      hardLabel.textColor = UIColor.whiteColor()
    case 2:
      easyLabel.textColor = UIColor.whiteColor()
      mediumLabel.textColor = UIColor.whiteColor()
      hardLabel.textColor = UIColor.fasYellowColor()
    default:
      printlnDebugWarning("Unexpected value: \(value)")
    }
  }
  
  func rotated()
  {
    if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation)) {
      if (FASData.sharedData().isIPHONE) {
        self.motivationLabel.hidden = true
      }
      else {
        self.motivationLabel.hidden = false
      }
    }
    else if(UIDeviceOrientationIsPortrait(UIDevice.currentDevice().orientation)) {
      self.motivationLabel.hidden = false
    }
    
  }

  private func configureControls() {
    title.textColor = UIColor.fasYellowColor()
    title.font = UIFont.boldBabasFontOfSize(50)
    title.text = String(format: "YOU HAVE FINISHED \nYOUR WORKOUT");
 
    motivationLabel.textColor = UIColor.whiteColor()
    motivationLabel.font = UIFont.boldBabasFontOfSize(32)

    motivationLabel.adjustsFontSizeToFitWidth = true;
    
    howHeavyTitle.textColor = UIColor.whiteColor()
    howHeavyTitle.font = UIFont.boldBabasFontOfSize(25)
    
    
    easyLabel.textColor = UIColor.whiteColor()
    easyLabel.font = UIFont.boldBabasFontOfSize(20)
    
    mediumLabel.textColor = UIColor.fasYellowColor()
    mediumLabel.font = UIFont.boldBabasFontOfSize(20)
    
    hardLabel.textColor = UIColor.whiteColor()
    hardLabel.font = UIFont.boldBabasFontOfSize(20)
    
    FASViewHelper.applyStyle(FASViewHelperButtonStyle.Filled, toButton: self.okButton)
    FASViewHelper.applyStyle(FASViewHelperButtonStyle.Stroked, toButton: self.shareButton)
    
    if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation)) {
      if (FASData.sharedData().isIPHONE) {
        self.motivationLabel.hidden = true
      }
      else {
        self.motivationLabel.hidden = false
      }
    }
  }
}