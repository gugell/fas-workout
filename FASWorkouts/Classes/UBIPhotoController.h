//
//  UBIPhotoController.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 3/13/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// TODO: make the protocol to be more generic
@class UBIPhotoController;
@protocol UBIPhotoControllerPopover
@required
- (UIView *)popoverViewForPhotoController:(UBIPhotoController *)photoController;
- (UIBarButtonItem *)popoverBarButtonForPhotoController:(UBIPhotoController *)photoController;
@optional
- (CGRect)popoverRectForPhotoController:(UBIPhotoController *)photoController;
@end

// photo source
typedef NS_ENUM(NSInteger, UBIPhotoControllerPhotoSource) {
    UBIPhotoControllerPhotoSourcePhotoLibrary,
    UBIPhotoControllerPhotoSourceCamera,
};

// photoInfo keys
extern NSString * const UBIPhotoControllerPhotoInfoImageKey;
extern NSString * const UBIPhotoControllerPhotoInfoSourceKey;

@protocol UBIPhotoControllerDelegate <NSObject>
@optional
- (void)photoController:(UBIPhotoController *)photoController didFinishWithPhotoInfo:(NSDictionary *)photoInfo;
- (void)photoController:(UBIPhotoController *)photoController didFailWithError:(NSError*)error;
@end

@interface UBIPhotoController : NSObject

@property (nonatomic, weak) id <UBIPhotoControllerDelegate> delegate;

@property (nonatomic, readonly) NSUInteger picturesTakenCount;

- (void)chooseActionWithViewController:(UIViewController*)viewController;

@end

typedef NS_ENUM(NSUInteger, UBIPhotoControllerErrorCode) {
    UBIPhotoControllerErrorCodeInvalidViewController = 0,
    UBIPhotoControllerErrorCodeCameraUnavailable,
    UBIPhotoControllerErrorCodeLibraryUnavailable,
    UBIPhotoControllerErrorCodeCanceledByUser,
    UBIPhotoControllerErrorCodeNoPhoto,
};
