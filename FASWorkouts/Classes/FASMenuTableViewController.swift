//
//  FASMenuTableViewController.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 2/21/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit
import Foundation

protocol FASMenuTableViewControllerDelegate {
    func menuTableViewController(menuTableVC: FASMenuTableViewController, didSelectItemID itemID: FASSettingsDataSourceItemID)
}

final class FASMenuTableViewController: UITableViewController {
    let dataSource = FASMenuDataSource()
    var delegate: FASMenuTableViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.dataSource.tableView = self.tableView;
        self.tableView.dataSource = self.dataSource;
        self.tableView.delegate = self
        
        // transparent so the background image is visible
        view.backgroundColor = UIColor.clearColor()
    }
}

///////////////////////////////////////////////
// MARK: - UITableViewDelegate -
///////////////////////////////////////////////

extension FASMenuTableViewController {
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let itemID: FASSettingsDataSourceItemID = FASSettingsDataSourceItemID(rawValue: dataSource.itemIDatIndexPath(indexPath))!
        delegate?.menuTableViewController(self, didSelectItemID: itemID)
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 61.0;
    }
    
    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        
        if let header = view as? UITableViewHeaderFooterView {
            header.textLabel?.textColor = UIColor.whiteColor()
            header.textLabel?.font = UIFont.boldBabasFontOfSize(17.0)
            header.textLabel?.textColor = UIColor.fasYellowColor()
        }
    }
}
