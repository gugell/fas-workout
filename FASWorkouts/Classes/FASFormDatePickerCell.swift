//
//  FASFormDatePickerCell.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/27/15.
//  Copyright © 2015 FAS. All rights reserved.
//

import UIKit

@objc protocol FASFormDatePickerCellDelegate {
    func fasFormDatePickerCellValueChanged(datePickerCell: FASFormDatePickerCell)
}

class FASFormDatePickerCell: UITableViewCell {
    
    var delegate: FASFormDatePickerCellDelegate?
    
    @IBOutlet weak var datePicker: UIDatePicker! {
        didSet {
            datePicker?.addTarget(self, action: "valueDidChange:", forControlEvents: UIControlEvents.ValueChanged)
        }
    }
    
    func valueDidChange(sender: AnyObject!) {
        if let datePicker = sender as? UIDatePicker where datePicker == self.datePicker {
            delegate?.fasFormDatePickerCellValueChanged(self)
        }
    }
}
