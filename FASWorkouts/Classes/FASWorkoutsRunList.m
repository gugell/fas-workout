//
//  FASWorkoutsRunList.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/31/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "FASWorkoutsRunList.h"

// entity
NSString* const kFASWorkoutsRunListEntity = @"fas_workout";

// fields
NSString* const kFASWorkoutsRunListNameFieldKey                 = @"name";
NSString* const kFASWorkoutsRunListCodeFieldKey                 = @"code";
NSString* const kFASWorkoutsRunListMuscleTypesNameFieldKey      = @"muscleTypes.name";
NSString* const kFASWorkoutsRunListRestTimeFieldKey             = @"restTime";
NSString* const kFASWorkoutsRunListTimeSpanFieldKey             = @"timeSpan";
NSString* const kFASWorkoutsRunListLapsNumberFieldKey           = @"lapsNumber";
NSString* const kFASWorkoutsRunListDifficultyFieldKey           = @"difficulty";
NSString* const kFASWorkoutsRunListMotivationTextFieldKey       = @"motivationText";
NSString* const kFASWorkoutsRunListWorkoutIDFieldKey            = @"ID";
NSString* const kFASWorkoutsRunListModifyDateFieldKey           = @"mi_modifyDate";
NSString* const kFASWorkoutsRunListCaloriesFieldKey             = @"calories";
NSString* const kFASWorkoutsRunListInAppProductIDFieldKey       = @"inAppProductID";
NSString* const kFASWorkoutsRunListOnSaleFieldKey               = @"onSale";
NSString* const kFASWorkoutsRunListGenderFieldKey               = @"gender";
NSString* const kFASWorkoutsRunListPrimaryMuscleTypeFieldKey    = @"primaryMuscleType.name";
NSString* const kFASWorkoutsRunListSortIndexFieldKey            = @"sortIndex";
NSString* const kFASWorkoutsRunListIsIncompleteFieldKey         = @"isIncomplete";

@implementation FASWorkoutsRunList
///////////////////////////////////////////////
#pragma mark -
#pragma mark Initialization
#pragma mark -
///////////////////////////////////////////////
- (instancetype)init
{
    return [self initWithDefaultFieldListAndWhereList:nil];
}

- (instancetype)initWithFieldList:(NSArray *)fieldList
{
    return [self initWithFieldList:fieldList whereList:nil];
}

- (instancetype)initWithFieldList:(NSArray *)fieldList whereList:(UBIRunListWhereList *)whereList
{
    self = [super initWithFieldList:fieldList entity:kFASWorkoutsRunListEntity];
    if (whereList) {
        self.whereList = whereList;
    }
    return self;
}

- (instancetype)initWithDefaultFieldListAndWhereList:(UBIRunListWhereList *)whereList
{
    return [self initWithFieldList:@[kFASWorkoutsRunListNameFieldKey,
                                     kFASWorkoutsRunListCodeFieldKey,
                                     kFASWorkoutsRunListMuscleTypesNameFieldKey,
                                     kFASWorkoutsRunListRestTimeFieldKey,
                                     kFASWorkoutsRunListTimeSpanFieldKey,
                                     kFASWorkoutsRunListLapsNumberFieldKey,
                                     kFASWorkoutsRunListDifficultyFieldKey,
                                     kFASWorkoutsRunListMotivationTextFieldKey,
                                     kFASWorkoutsRunListWorkoutIDFieldKey,
                                     kFASWorkoutsRunListModifyDateFieldKey,
                                     kFASWorkoutsRunListCaloriesFieldKey,
                                     kFASWorkoutsRunListInAppProductIDFieldKey,
                                     kFASWorkoutsRunListOnSaleFieldKey,
                                     kFASWorkoutsRunListGenderFieldKey,
                                     kFASWorkoutsRunListPrimaryMuscleTypeFieldKey,
                                     kFASWorkoutsRunListSortIndexFieldKey]
                         whereList:whereList];
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Workout Run List Constructors
#pragma mark -
///////////////////////////////////////////////
+ (instancetype)workoutsRunListDefault
{
    return [[FASWorkoutsRunList alloc] init];
}

+ (instancetype)workoutsRunListWithModifyDate
{
    return [[FASWorkoutsRunList alloc] initWithFieldList:@[kFASWorkoutsRunListWorkoutIDFieldKey,
                                                           kFASWorkoutsRunListModifyDateFieldKey]];
}

+ (instancetype)workoutsRunListExcludeIncompleteWithModifyDate
{
    return [[FASWorkoutsRunList alloc] initWithFieldList:@[kFASWorkoutsRunListWorkoutIDFieldKey,
                                                           kFASWorkoutsRunListModifyDateFieldKey]
                                               whereList:[[self class] whereListExcludeIncomplete]];
}

+ (instancetype)workoutsRunListExcludeIncomplete
{
    return [[FASWorkoutsRunList alloc] initWithDefaultFieldListAndWhereList:[[self class] whereListExcludeIncomplete]];
}

+ (UBIRunListWhereList *)whereListExcludeIncomplete {
    UBIRunListWhereList* wlExcludeIncomplete = [[UBIRunListWhereList alloc] init];
    [wlExcludeIncomplete addItem:@"ExcludeIncomplete" condition:UBIRunListWhereListConditionEqual value:@0 key:kFASWorkoutsRunListIsIncompleteFieldKey];
    return wlExcludeIncomplete;
}

@end
