//
//  CustomLayer.h
//  CustomLayer
//
//  Created by Vladimir Stepanchenko on 3/13/16.
//  Copyright © 2016 FAS. All rights reserved.
//

@import QuartzCore;

@interface FASExerciseProgressLayer : CALayer

@property (nonatomic,assign) CGFloat value;
@property (nonatomic,assign) NSTimeInterval animationDuration;
@property (nonatomic,assign) BOOL animated;
@property (nonatomic,assign) CGFloat progressLineWidth;
@property (nonatomic,strong) UIColor *progressColor;
@property (nonatomic,strong) UIColor *progressStrokeColor;
@property (nonatomic,assign) CGFloat emptyLineWidth;
@property (nonatomic,strong) UIColor *emptyLineColor;

@end
