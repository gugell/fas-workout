//
//  FASLicenseAgreement.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/10/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "FASLicenseAgreement.h"
#import "UBILogger.h"
@import UIKit;

NSString * const kFASLicenseAgreementDataDefaultFilename = @"LicenseAgreementData";

NSString * const FASLicenseAgreementFilenameKey             = @"LicenseFilename";
NSString * const FASLicenseAgreementVersionKey              = @"LicenseVersion";

@implementation FASLicenseAgreement

+ (nullable instancetype)licenseAgreementFromFile:(nonnull NSString *)filename
{
    if (!filename) {
        UBILogError(@"invalid filename");
        return nil;
    }
    
    NSString* filenameWithoutExtension = filename.stringByDeletingPathExtension;
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:filenameWithoutExtension ofType:@"plist"];
    NSMutableDictionary* plist = [NSMutableDictionary dictionaryWithContentsOfFile:plistPath];
    
    id licenseFilenameObj = plist[FASLicenseAgreementFilenameKey];
    id licenseVersionObj = plist[FASLicenseAgreementVersionKey];
    
    NSString* licenseFilename = nil;
    if (licenseFilenameObj && [licenseFilenameObj isKindOfClass:[NSString class]]) {
        licenseFilename = (NSString*)licenseFilenameObj;
    }
    else {
        UBILogError("invalid license filename from datasource")
    }
    
    NSUInteger licenseVersion = 0;
    if (licenseVersionObj && [licenseVersionObj isKindOfClass:[NSNumber class]]) {
        licenseVersion = [(NSNumber *)licenseVersionObj unsignedIntegerValue];
    }
    
    if (licenseFilename) {
        return [[[self class] alloc] initWithVersion:licenseVersion
                                            filename:licenseFilename];
    }

    return nil;
}

+ (nullable instancetype)defaultLicenseAgreement
{
    return [[self class] licenseAgreementFromFile:kFASLicenseAgreementDataDefaultFilename];
}

- (instancetype)initWithVersion:(NSUInteger)version
                       filename:(NSString*)filename
{
    self = [super init];
    if (self) {
        _version = version;
        _filename = [filename copy];
    }
    return self;
}


- (nullable NSString *)loadLicenseTextFromFile:(NSString *)filename
{
    NSString* text = nil;
    if (filename) {
        if (![filename.pathExtension isEqualToString:@"rtf"]) {
            UBILogError(@"imvalid filename extension");
        }
        else {
            NSString *licenseFilePath = [[NSBundle mainBundle] pathForResource:filename.stringByDeletingPathExtension ofType:filename.pathExtension];
            
            NSError *err = nil;
            NSString *fileContents = [NSString stringWithContentsOfFile:licenseFilePath
                                                               encoding:NSUTF8StringEncoding
                                                                  error:&err];
            
            if (err == nil) {
                text = fileContents;
            }
            else {
                UBILogError("Error reading %@: %@", licenseFilePath, err);
            }
        }
    }
    
    return text;
}

- (nullable NSString *)loadLicenseText
{
    return [self loadLicenseTextFromFile:self.filename];
}

- (nullable NSAttributedString *)loadLicenseAttributedTextFromFile:(NSString *)filename
{
    NSAttributedString* attrtText = nil;
    if (filename) {
        if (![filename.pathExtension isEqualToString:@"rtf"]) {
            UBILogError(@"imvalid filename extension");
        }
        else {
            NSURL *licensePath = [[NSBundle mainBundle] URLForResource: filename.stringByDeletingPathExtension withExtension:filename.pathExtension];
            NSError* rtfError = nil;
            NSDictionary* customDocumentAttribs = nil; // for .rtf seems like these attributes don't apply, thus nil
            NSAttributedString *attributedStringWithRtf = [[NSAttributedString alloc] initWithFileURL:licensePath
                                                                                              options:@{NSDocumentTypeDocumentAttribute:NSRTFTextDocumentType}
                                                                                   documentAttributes:&customDocumentAttribs
                                                                                                error:&rtfError];
            if (rtfError == nil) {
                attrtText = attributedStringWithRtf;
            }
            else {
                UBILogError(@"rtf conversion error: %@",[rtfError localizedDescription]);
            }
        }
    }
    return attrtText;
}

- (nullable NSAttributedString *)loadLicenseAttributedText
{
    return [self loadLicenseAttributedTextFromFile:self.filename];
}

@end
