//
//  StringUtils.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 6/1/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIColor.h>

@interface StringUtils : NSObject

+ (NSString*)trim:(NSString*)string;
+ (BOOL)isEmpty:(NSString*)string;
+ (NSString*)escapeJSONString:(NSString*)string;
+ (NSString*)urlEncodedStrng:(NSString*)string;
+ (NSString*)idToString:(id)iD;
+ (NSString *)stringByStrippingHTML:(NSString *)inputString;
+ (NSString*)sessionKey:(NSString*)string;
+ (UIColor*)colorFromHex:(NSString*)hex;
+ (NSString*)getClientNonce;
+ (NSString*)sha256HashFor:(NSString*)input;
+ (NSString *)MD5:(NSString *)input;
+ (NSString *)CRC32X:(NSString *)input;

@end
