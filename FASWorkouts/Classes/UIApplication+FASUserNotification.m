//
//  UIApplication+FASUserNotification.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/2/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "UIApplication+FASUserNotification.h"

@implementation UIApplication (FASUserNotification)
///////////////////////////////////////////////
#pragma mark -
#pragma mark Permissions
#pragma mark -
///////////////////////////////////////////////
- (BOOL)isAllowedUserFacingNotificationTypes:(UIUserNotificationType)type
{
    UIUserNotificationSettings *settings;
    settings = [self currentUserNotificationSettings];
    return ((settings.types & type) == type);
}

- (BOOL)isAnyUserFacingNotificationTypesAllowed
{
    UIUserNotificationType types = [self currentUserNotificationSettings].types;
    return !(types == UIUserNotificationTypeNone);
}

- (void)registerUserFacingNotificationTypes:(UIUserNotificationType)type
{
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:type
                                                                             categories:nil];
    [self registerUserNotificationSettings:settings];
}


@end
