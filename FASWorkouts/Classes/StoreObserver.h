//
//  StoreObserver.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/25/15.
//  Copyright (c) 2014 FAS. All rights reserved.
//

@import StoreKit;

#import "FASAPI.h"

// Product Status Change Notifications
extern NSString * const StoreObserverDidChangeProductStatusNotification;

// Product Status Cahnge Notificaion UserInfo keys
extern NSString * const SOProductStatusNotificationProductIdentifierKey;
extern NSString * const SOProductStatusNotificationStatusKey;
extern NSString * const SOProductStatusNotificationErrorKey;

// Restore Notifications
extern NSString * const StoreObserverWillRestoreNotification;
extern NSString * const StoreObserverDidFinishRestoreNotification;
extern NSString * const StoreObserverRestoreDidFailNotification;

typedef NS_ENUM(NSInteger, SOProductStatus)
{
    SOProductStatusPurchaseNone = 0,
    
    SOProductStatusPurchaseInProgress,
    
    SOProductStatusPurchaseSucceeded,
    SOProductStatusPurchaseFailed,
    
    SOProductStatusRestoreSucceeded,
    SOProductStatusRestoreFailed,
    
    SOProductStatusPurchaseDeferred,
    
    SOProductStatusReceiptValidationInProgress,
    SOProductStatusReceiptValidationSucceeded,
    SOProductStatusReceiptValidationFailed,
};

@protocol StoreObserverNotificationsObserver
- (void)storeObserverNotification:(NSNotification *)notification;
@optional
- (void)storeObserverProductStatus:(NSNotification *)notification;
- (void)storeObserverWillRestore;
- (void)storeObserverDidFinishRestore;
- (void)storeObserverRestoreDidFail:(NSNotification *)notification;
@end

@interface StoreObserver : NSObject
<
SKPaymentTransactionObserver,
SKRequestDelegate,
FASAPIDelegate
>

+ (StoreObserver *)sharedInstance;

- (void)addStoreObserverNotificationsObserver:(id <StoreObserverNotificationsObserver>)observer;
- (void)removeStoreObserverNotificationsObserver:(id <StoreObserverNotificationsObserver>)observer;

// Implement the purchase of a product
- (void)buy:(SKProduct *)product;
- (void)buy:(SKProduct *)product withUserHash:(NSString *)userhash;

// Implement the restoration of previously completed purchases
- (void)restorePurchases;
- (void)restorePurchasesWithUserHash:(NSString *)userhash;

@end

// Extesion with helpers that have protected purchases in mind
@interface StoreObserver (PurchaseWithSession)
- (void)buyProductWithSession:(SKProduct *)product;
- (void)restorePurchasesWithSession;
@end
