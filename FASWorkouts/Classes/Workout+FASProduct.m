//
//  Workout+FASProduct.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/24/16.
//  Copyright © 2016 FAS. All rights reserved.
//

#import "Workout+FASProduct.h"
#import "StoreObserver.h"

@interface Workout (FASProduct_Protected)
@property (nonatomic, strong, readwrite) NSNumber* purchaseStatus;
@end

@implementation Workout (FASProduct)

- (NSString *)productCategoryName
{
    return @"workout";
}

- (NSString *)productIdentifier
{
    return self.inAppProductID;
}

- (BOOL)isPaid
{
    return (BOOL)(self.inAppProductID != nil);
}

- (void)setProductPurchaseStatus:(NSInteger)newValue
{
    self.purchaseStatus = @(newValue);
}

- (NSInteger)productPurchaseStatus
{
    if (self.purchaseStatus) {
        return [self.purchaseStatus integerValue];
    }
    return (NSInteger)SOProductStatusPurchaseNone;
    
}

- (BOOL)isPurchasing
{
    SOProductStatus status = [self productPurchaseStatus];
    if (status == SOProductStatusPurchaseInProgress
        || status == SOProductStatusPurchaseSucceeded
        || status == SOProductStatusRestoreSucceeded
        || status == SOProductStatusReceiptValidationInProgress
        || status == SOProductStatusPurchaseDeferred) {
        return YES;
    }
    return NO;
}

- (BOOL)isPurchased
{
    return (BOOL)(self.productPurchaseStatus == SOProductStatusReceiptValidationSucceeded);
}

@end
