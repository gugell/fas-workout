//
//  FASEnvironment.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/14/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

class FASEnvironment: UBIEnvironment {
    private struct EnvironmentKeys {
        static let APIBasePathKey = "APIBasePath"
        static let GoogleAnalyticsPropertyIdKey = "GAPropertyId"
        static let HTTPAuthUserKey = "HTTPAuthUser"
        static let HTTPAuthPassword = "HTTPAuthPassword"
        static let APISecure = "APISecure"
        static let SiteBasePath = "SiteBasePath"
        static let ImagesBasePath = "ImagesBasePath"
        static let AppStoreURL = "AppStoreURL"
        static let AppStoreID = "AppStoreID"
        static let FBInviteFriendsAppLinkURL = "FBInviteFriendsAppLinkURL"
        static let AppStoreReviewURL = "AppStoreReviewURL"
        static let GoogleAnalyticsPropertyID = "GoogleAnalyticsPropertyID"
        static let ApptentiveAPIKey = "ApptentiveAPIKey"
    }
    
    private class func valueForEnvironmentKey(key: String!) -> String {
        let aValue: AnyObject? = valueForKey(key)
        if let stringValue = aValue as? String {
            return stringValue
        } else {
            return ""
        }
    }
    
    class func APIBasePath() -> String {
        return valueForEnvironmentKey(EnvironmentKeys.APIBasePathKey)
    }
    
    class func GAIPropertyId() -> String {
        return valueForEnvironmentKey(EnvironmentKeys.GoogleAnalyticsPropertyIdKey)
    }
    
    class func HTTPAuthUser() -> String {
        return valueForEnvironmentKey(EnvironmentKeys.HTTPAuthUserKey)
    }
    
    class func HTTPAuthPassword() -> String {
        return valueForEnvironmentKey(EnvironmentKeys.HTTPAuthPassword)
    }
    
    class func APISecure() -> Bool {
        let aValue: AnyObject? = valueForKey(EnvironmentKeys.APISecure)
        if let boolValue = aValue as? Bool {
            return boolValue
        }
        else {
            return false; // NOT secure if value does not exist
        }
    }
    
    class func SiteBasePath() -> String {
        return valueForEnvironmentKey(EnvironmentKeys.SiteBasePath)
    }
    
    class func ImagesBasePath() -> String {
        return valueForEnvironmentKey(EnvironmentKeys.ImagesBasePath)
    }
    
    class func AppStoreURL() -> String {
        return valueForEnvironmentKey(EnvironmentKeys.AppStoreURL)
    }
    
    class func AppStoreID() -> String {
        return valueForEnvironmentKey(EnvironmentKeys.AppStoreID)
    }
    
    class func FBInviteFriendsAppLinkURL() -> String {
        return valueForEnvironmentKey(EnvironmentKeys.FBInviteFriendsAppLinkURL)
    }
    
    class func AppStoreReviewURL() -> String {
        return valueForEnvironmentKey(EnvironmentKeys.AppStoreReviewURL)
    }
    
    class func GoogleAnalyticsPropertyID() -> String {
        return valueForEnvironmentKey(EnvironmentKeys.GoogleAnalyticsPropertyID)
    }
    
    class func ApptentiveAPIKey() -> String {
        return valueForEnvironmentKey(EnvironmentKeys.ApptentiveAPIKey)
    }
}
