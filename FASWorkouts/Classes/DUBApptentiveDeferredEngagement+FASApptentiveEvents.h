//
//  DUBApptentiveDeferredEngagement+FASApptentiveEvents.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 2/4/16.
//  Copyright © 2016 FAS. All rights reserved.
//

#import "DUBApptentiveDeferredEngagement.h"

extern NSString * const kFASApptentiveEventDidFinishWorkout;
extern NSString * const kFASApptentiveEventAfterDidFinishWorkout;
extern NSString * const kFASApptentiveEventDidMakeInAppPurchase;
extern NSString * const kFASApptentiveEventAfterDidMakeInAppPurchase; // not yet used
extern NSString * const kFASApptentiveEventAfterDidViewWorkoutDetail;

@interface DUBApptentiveDeferredEngagement (FASApptentiveEvents)
+ (BOOL)engage:(NSString *)event fromViewController:(UIViewController *)viewController;
@end
