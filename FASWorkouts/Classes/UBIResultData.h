//
//  UBIResultData.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 10/31/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UnityBase.h"

@interface UBIResultData : NSObject
<UBIUpdatableWithDictionary>

@property (nonatomic, strong, readonly) NSNumber* rowCount;
@property (nonatomic, strong, readonly) NSArray* data;
@property (nonatomic, strong, readonly) NSArray* fields;

- (NSArray *)arrayOfDictionaryData;

@end
