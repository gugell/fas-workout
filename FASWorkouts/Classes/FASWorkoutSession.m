//
//  FASWorkoutSession.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/4/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "FASWorkoutSession.h"

@interface FASWorkoutSession ()
@property (nonatomic, strong, readwrite) NSNumber* workoutID;
@property (nonatomic, strong, readwrite) NSDate* beginDate;
@property (nonatomic, strong, readwrite) NSDate* endDate;
@property (nonatomic, strong, readwrite) NSNumber* pauseDuration;
@property (nonatomic, strong, readwrite) NSNumber* rate;
@property (nonatomic, readwrite) BOOL completed;
@property (nonatomic, strong) NSDate* onPauseDate;
@end

@implementation FASWorkoutSession

///////////////////////////////////////////////
#pragma mark -
#pragma mark Init
#pragma mark -
///////////////////////////////////////////////

- (instancetype)initWithWorkoutID:(NSNumber*)workoutID
{
    self = [super init];
    if (self) {
        _workoutID = workoutID;
        _workoutHistoryID = @0;
        _completed = NO;
        _beginDate = [NSDate date];
        _endDate = nil;
        _pauseDuration = @0;
        _rate = @0;
        _posted = NO;
        _onPauseDate = nil;
    }
    return self;
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark NSCoding
#pragma mark -
///////////////////////////////////////////////

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [self init];
    if (self) {
        self.workoutID = [aDecoder decodeObjectForKey:@"workoutID"];
        self.workoutHistoryID = [aDecoder decodeObjectForKey:@"workoutHistoryID"];
        self.beginDate = [aDecoder decodeObjectForKey:@"beginDate"];
        self.endDate = [aDecoder decodeObjectForKey:@"endDate"];
        self.pauseDuration = [aDecoder decodeObjectForKey:@"pauseDuration"];
        self.rate = [aDecoder decodeObjectForKey:@"rate"];
        self.completed = [[aDecoder decodeObjectForKey:@"completed"] boolValue];
        self.posted = [[aDecoder decodeObjectForKey:@"posted"] boolValue];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.workoutID forKey:@"workoutID"];
    [aCoder encodeObject:self.workoutHistoryID forKey:@"workoutHistoryID"];
    [aCoder encodeObject:self.beginDate forKey:@"beginDate"];
    [aCoder encodeObject:self.endDate forKey:@"endDate"];
    [aCoder encodeObject:self.pauseDuration forKey:@"pauseDuration"];
    [aCoder encodeObject:self.rate forKey:@"rate"];
    [aCoder encodeObject:@(self.completed) forKey:@"completed"];
    [aCoder encodeObject:@(self.posted) forKey:@"posted"];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Control
#pragma mark -
///////////////////////////////////////////////

- (void)complete
{
    self.endDate = [NSDate date];
    self.completed = YES;
}

- (void)abort
{
    // IF "on pause", THEN force pauseDuration calculation
    [self setOnPause:NO];
    self.endDate = [NSDate date];
}

- (void)rateWorkout:(NSUInteger)rate
{
    self.rate = @(rate);
}

// TODO: should we archieve onPauseDate ?
- (void)setOnPause:(BOOL)isPaused
{
    if (isPaused && self.onPauseDate == nil) {
        // onPause BEGIN
        self.onPauseDate = [NSDate date];
    }
    
    else if (!isPaused && self.onPauseDate != nil) {
        // onPause END
        NSDate* now = [NSDate date];
        NSTimeInterval secondsOnPause = [now timeIntervalSinceDate:self.onPauseDate];
        assert( (secondsOnPause >= 0.0) ); // assume always greater than 0
        self.pauseDuration = [NSNumber numberWithDouble:[self.pauseDuration doubleValue] + secondsOnPause];
        self.onPauseDate = nil;
    }
    
    // ignore everything else
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Accessors
#pragma mark -
///////////////////////////////////////////////

- (FASWorkoutUserRate)userRate
{
    if (self.rate) {
        return (FASWorkoutUserRate)[self.rate unsignedIntegerValue];
    }
    
    return FASWorkoutUserRatingUndefined;
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark NSObject
#pragma mark -
///////////////////////////////////////////////

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@"
            @"\nworkoutID = %@"
            @"\nbeginDate = %@"
            @"\nendDate = %@"
            @"\npauseDuration = %@"
            @"\nisCompleted = %@"
            @"\nisPosted = %@",
            super.description,
            self.workoutID,
            self.beginDate,
            self.endDate,
            self.pauseDuration,
            self.isCompleted?@"YES":@"NO",
            self.isPosted?@"YES":@"NO"];
}
@end

///////////////////////////////////////////////
#pragma mark -
#pragma mark API
#pragma mark -
///////////////////////////////////////////////

NSString * const FASAPIHistoryWorkoutIDKey = @"kFASAPIHistoryWorkoutIDKey";
NSString * const FASAPIHistoryIDKey = @"kFASAPIHistoryIDKey";
NSString * const FASAPIHistoryRateKey = @"kFASAPIHistoryRateKey";
NSString * const FASAPIHistoryStartDateKey = @"kFASAPIHistoryStartDateKey";
NSString * const FASAPIHistoryEndDateKey = @"kFASAPIHistoryEndDateKey";
NSString * const FASAPIHistoryCompletedKey = @"kFASAPIHistoryCompletedKey";
NSString * const FASAPIHistoryPauseDurationKey = @"kFASAPIHistoryPauseDurationKey";

@implementation FASWorkoutSession (API)

- (NSDictionary *)apiParams
{
    static NSDateFormatter *historyDateFormatter = nil;
    
    if (!historyDateFormatter)
    {
        historyDateFormatter = [NSDateFormatter new];
        [historyDateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        [historyDateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
        [historyDateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    }
    
    long long integerPauseDuration = [self.pauseDuration longLongValue];
    NSNumber* roundedPauseDuration = [NSNumber numberWithLongLong:integerPauseDuration];
    
    return @{FASAPIHistoryWorkoutIDKey : self.workoutID,
             FASAPIHistoryIDKey : self.workoutHistoryID,
             FASAPIHistoryRateKey : self.rate,
             FASAPIHistoryStartDateKey : [historyDateFormatter stringFromDate:self.beginDate],
             FASAPIHistoryEndDateKey : [historyDateFormatter stringFromDate:self.endDate],
             FASAPIHistoryCompletedKey : @(self.isCompleted),
             FASAPIHistoryPauseDurationKey : roundedPauseDuration};
}

@end

///////////////////////////////////////////////
#pragma mark -
#pragma mark Format
#pragma mark -
///////////////////////////////////////////////

#import "FASWorkouts-Swift.h" // timeIntervalValue

@implementation FASWorkoutSession (Format)

- (NSString *)formattedUserRate
{
    NSString* result = nil;
    switch ([self userRate]) {
        case FASWorkoutUserRatingEasy: {
            result = @"easy";
        } break;
            
        case FASWorkoutUserRatingNormal: {
            result = @"normal";
        } break;
            
        case FASWorkoutUserRatingHard: {
            result = @"hard";
        } break;
            
        default: {
            result = @"unknown";
        } break;
    }
    return result;
}

- (NSString *)formattedDuration
{
    static NSDateComponentsFormatter* dateComponentsFormatter = nil;
    
    if (!dateComponentsFormatter) {
        dateComponentsFormatter = [NSDateComponentsFormatter new];
        dateComponentsFormatter.zeroFormattingBehavior = NSDateComponentsFormatterZeroFormattingBehaviorPad;
        dateComponentsFormatter.allowedUnits = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    }
    
    NSString* result = nil;
    if (self.endDate != nil && self.beginDate != nil) {
        NSDate* adjustedEndDate = [self.endDate dateByAddingTimeInterval:-[self.pauseDuration timeIntervalValue]];
        result = [dateComponentsFormatter stringFromDate:self.beginDate toDate:adjustedEndDate];
    }
    
    return result;
}

@end
