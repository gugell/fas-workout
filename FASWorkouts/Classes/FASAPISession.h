//
//  FASSession.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 9/19/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

@import Foundation;
@import UIKit;

typedef NS_ENUM(NSUInteger, FASAPISessionAccountType) {
    FASAPISessionAccountTypeUnknown,
    FASAPISessionAccountTypeUser,
};

#import "FASAPISessionProtocol.h"
#import "FASAPI.h"

@class FASUserInfo;

@interface FASAPISession : NSObject < UIAlertViewDelegate, FASAPIDelegate >

// user info
@property (nonatomic, strong) FASUserInfo* userInfo;
@property (nonatomic, strong, readonly) FASUserInfo* editableUserInfo;

// crendentials components
@property (nonatomic, copy) NSString *username;
@property (nonatomic, copy) NSString *password;

// login info
@property (nonatomic, copy, readonly) NSString *token;
@property (nonatomic, copy, readonly) NSString *obfuscatedToken;
@property (nonatomic) NSUInteger errorCodeFromLastLogin;

@property (nonatomic, getter = isLoggingIn) BOOL loggingIn;

@property (nonatomic, readonly) NSUInteger acceptedLicenseVersion;

+ (FASAPISession *)currentSession;

// auto login user w/ stored credentials
- (void)login;
- (void)loginWithCredentials:(NSDictionary *)credentials;

- (void)logout;
- (void)logoutWithConfirmation:(BOOL)shouldConfirm;

- (void)updateSessionData;

- (void)userActionCancelledLogin;

- (BOOL)isLoggedIn;
- (BOOL)isLoggedInAsUser;
- (BOOL)isLoggedInWithUserID:(NSNumber *)userID;

- (FASAPISessionAccountType)accountType;
- (void)setAccountType:(FASAPISessionAccountType)accountType;

// observing APISession's notification
- (void)addObserver:(id)observer;
- (void)removeObserver:(id)observer;

- (NSDictionary *)credentials;
- (void)setCredentials:(NSDictionary *)credentials;
- (BOOL)hasCredentials;
- (BOOL)canLoginWithoutUserInteraction;

- (void)restore;
- (void)save;

- (void)clearToken;

-(void) getIdFromMail;

// Facebook
- (void)connectFacebookAccount;
- (void)connectFacebookAccountWithCompletionHandler:(void (^)(NSError *))completionHandler;
- (void)connectFacebookAccountAndRequestPublishPermissionsWithCompletionHandler:(void (^)(NSError *))completionHandler;
- (void)requestFacebookPublishPermissionsWithCompletionHandler:(void (^)(NSError *))completionHandler;
- (BOOL)hasFacebookPublishPermissions;
- (void)logoutFacebook;
@end