//
//  FASFAQRunList.m
//  FASWorkouts
//
//  Created by Vladimir Stepanchenko on 4/18/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

#import "FASFAQRunList.h"
//#import "FASAPISession.h"

// entity
NSString* const kFASFaqRunListEntity = @"fas_faq";

// fields

NSString* const kFASFaqIdFieldKey            = @"ID";
NSString* const kFASFaqSortIndexFieldKey     = @"sortIndex";
NSString* const kFASFaqQuestionFieldKey      = @"question";
NSString* const kFASFaqAnswerFieldKey        = @"answer";

@implementation FASFAQRunList
///////////////////////////////////////////////
#pragma mark -
#pragma mark Initialization
#pragma mark -
///////////////////////////////////////////////
- (instancetype)init
{
    
    self = [self initWithFieldList:@[kFASFaqIdFieldKey,
                                     kFASFaqSortIndexFieldKey,
                                     kFASFaqQuestionFieldKey,
                                     kFASFaqAnswerFieldKey]];
//    [self.whereList addItem:@"userID" condition:UBIRunListWhereListConditionEqual value:[[FASAPISession currentSession].userInfo stringUserID] key:kFASWorkoutHistoryRunListWorkoutHistoryUserIdFieldKey];
    return self;
}

- (instancetype)initWithFieldList:(NSArray *)fieldList
{
    return [super initWithFieldList:fieldList entity:kFASFaqRunListEntity];
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark FAQ Run List Constructors
#pragma mark -
///////////////////////////////////////////////
+ (instancetype)faqRunListDefault
{
    return [[FASFAQRunList alloc] init];
}

@end