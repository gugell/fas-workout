//
//  FASProgressContentView.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/9/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit

class FASProgressContentView: UIView {
    
    lazy var numberFormatter: NSNumberFormatter = {
        var _numberFormatter = NSNumberFormatter()
        _numberFormatter.numberStyle = .PercentStyle
        _numberFormatter.maximumFractionDigits = 0
        return _numberFormatter
    }()
    
    var progress: Float = 0.0 {
        didSet {
//            progressPercentsLabel.text = numberFormatter.stringFromNumber( NSNumber(float: progress) )
//            progressView.progress = CGFloat(progress)
        }
    }
    
    override func awakeFromNib() {
        // uncomment for view debugging
        //self.backgroundColor = UIColor.greenColor()
    }
}
