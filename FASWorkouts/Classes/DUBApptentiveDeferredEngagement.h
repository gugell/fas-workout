//
//  DUBApptentiveDeferredEngagement.m
//  MyCompany
//
//  Created by Evgen Dubinin on 12/21/15.
//  Copyright © 2015 MyCompany Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

// Utility for Apptentive to work with deferred engagement
@class UIViewController;

@interface DUBApptentiveDeferredEngagement : NSObject

+ (DUBApptentiveDeferredEngagement*)sharedInstance;

- (void)deferEngage:(NSString *)event;
- (void)cancelDeferredEngage:(NSString *)event;

- (BOOL)engageDeferred:(NSString *)event fromViewController:(UIViewController *)viewController;

@end
