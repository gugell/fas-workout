//
//  FASData+DataMigration.h
//  FASWorkouts
//
//  Created by Evgen Dubinin on 1/18/16.
//  Copyright © 2016 FAS. All rights reserved.
//

#import "FASData.h"

@interface FASData (DataMigration)

// availableProductIDs array was used to store In-App Product Identifiers of workouts
// user had bought. This allowed us to update purchased status for Workout objects
// when they were updated from the server and the purchase statuses were lost.
//
// now we have extended that model to support purchasing of different entities in the app.
// those items have to be restored after an update, thus now we have purchasedItems
// in place. The data from purchased Workout objsects, though, should be migrated to
// the new model. This is what the migration does.
- (void)migrateAvailableProductIDsToPurchasedItems;

@end
