//
//  UISplitViewController+StatusBarStyle.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/4/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

extension UISplitViewController {
    
    override public func childViewControllerForStatusBarStyle() -> UIViewController {
        return viewControllers[0]
    }
    
    override public func childViewControllerForStatusBarHidden() -> UIViewController {
        return viewControllers[0]
    }
}