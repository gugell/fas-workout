//
//  StoreObserver.m
//  FASWorkouts
//
//  Created by Evgen Dubinin on 11/25/15.
//  Copyright (c) 2014 FAS. All rights reserved.
//

#import "StoreObserver.h"
#import "UBILogger.h"
#import "NSString+MyExtensions.h"
#import "FASData.h"

NSString * const StoreObserverDidChangeProductStatusNotification = @"StoreObserverDidChangeProductStatus";

NSString * const SOProductStatusNotificationProductIdentifierKey = @"SOProductIdentifier";
NSString * const SOProductStatusNotificationStatusKey = @"SOStatus";
NSString * const SOProductStatusNotificationErrorKey = @"SOError";

NSString * const StoreObserverWillRestoreNotification = @"StoreObserverWillRestore";
NSString * const StoreObserverDidFinishRestoreNotification = @"StoreObserverDidFinishRestore";
NSString * const StoreObserverRestoreDidFailNotification = @"StoreObserverDidFailRestore";

@interface StoreObserver ()
@property (nonatomic, strong) dispatch_queue_t storeQueue;
@end

@implementation StoreObserver

+ (StoreObserver *)sharedInstance
{
    static dispatch_once_t onceToken;
    static StoreObserver * storeObserverSharedInstance;
    
    dispatch_once(&onceToken, ^{
        storeObserverSharedInstance = [[StoreObserver alloc] init];
    });
    return storeObserverSharedInstance;
}

- (instancetype)init
{
	self = [super init];
	if (self != nil)
    {   
        _storeQueue = dispatch_queue_create("com.fas-sport.FASWorkouts.Queues.StoreQueue", NULL);
    }
	return self;
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Notifications
///////////////////////////////////////////////

- (void)addStoreObserverNotificationsObserver:(id <StoreObserverNotificationsObserver>)observer
{
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:observer selector:@selector(storeObserverNotification:) name:StoreObserverDidChangeProductStatusNotification object:self];
    [nc addObserver:observer selector:@selector(storeObserverNotification:) name:StoreObserverWillRestoreNotification object:self];
    [nc addObserver:observer selector:@selector(storeObserverNotification:) name:StoreObserverDidFinishRestoreNotification object:self];
    [nc addObserver:observer selector:@selector(storeObserverNotification:) name:StoreObserverRestoreDidFailNotification object:self];
}

- (void)removeStoreObserverNotificationsObserver:(id <StoreObserverNotificationsObserver>)observer
{
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    [nc removeObserver:StoreObserverDidChangeProductStatusNotification];
    [nc removeObserver:StoreObserverWillRestoreNotification];
    [nc removeObserver:StoreObserverDidFinishRestoreNotification];
    [nc removeObserver:StoreObserverRestoreDidFailNotification];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Keep Track
///////////////////////////////////////////////

- (void)changeStatus:(SOProductStatus)status forProductIdentifier:(NSString *)productId error:(NSError *)error
{
    if (productId == nil || [productId isBlank]) {
        return;
    }
    
    NSMutableDictionary* ui = [NSMutableDictionary dictionaryWithObject:productId
                                                                 forKey:SOProductStatusNotificationProductIdentifierKey];
    [ui setObject:@(status)
           forKey:SOProductStatusNotificationStatusKey];
    if (error) {
        [ui setObject:error
               forKey:SOProductStatusNotificationErrorKey];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:StoreObserverDidChangeProductStatusNotification
                                                        object:self
                                                      userInfo:[ui copy]];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Validating Receipts
///////////////////////////////////////////////

- (void)validateReceiptForTransaction:(SKPaymentTransaction *)transaction
{
    __typeof(self) __weak weakSelf = self;
    dispatch_async(self.storeQueue, ^{
        @autoreleasepool {
            NSURL* receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
            NSData* receipt = [NSData dataWithContentsOfURL:receiptURL];
            NSString* productId = transaction.payment.productIdentifier;
            if (receipt) {
                NSString *base64StringRceipt = [receipt base64EncodedStringWithOptions:kNilOptions];
                BOOL isRestored = (transaction.originalTransaction != nil);
                FASAPI *api = [FASAPI addUserPurchase:productId
                                              receipt:base64StringRceipt
                                             restored:isRestored
                                             delegate:self];
                api.userInfo = @{@"transaction": transaction};
                [api invoke];
            }
            else {
                FASAPIError* noReceiptError = [FASAPIError errorWithCode:FASAPIErrorCodeInAppReceiptNotFound value:@"No receipt found"];
                [weakSelf changeStatus:SOProductStatusReceiptValidationFailed
                  forProductIdentifier:productId
                                 error:noReceiptError];
            }
        }
    });
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Make a purchase
///////////////////////////////////////////////

- (void)buy:(SKProduct *)product
{
    [self buy:product withUserHash:nil];
}

- (void)buy:(SKProduct *)product withUserHash:(NSString *)userhash
{
    if ([self isPurchasingProductWithProductIdentifier:product.productIdentifier]
        && [SKPaymentQueue canMakePayments]) {
        return;
    }
    
    SKMutablePayment *payment = [SKMutablePayment paymentWithProduct:product];
    if (userhash != nil && ![userhash isBlank]) {
        payment.applicationUsername = userhash;
    }
//    payment.simulatesAskToBuyInSandbox = YES;
    [[SKPaymentQueue defaultQueue] addPayment:payment];
}

- (BOOL)isPurchasingProductWithProductIdentifier:(NSString *)productIdentifier
{
    if (productIdentifier == nil || [productIdentifier isBlank]) { return NO; }
    BOOL result = NO;
    for (SKPaymentTransaction* transaction in [SKPaymentQueue defaultQueue].transactions) {
        if ([transaction.payment.productIdentifier isEqualToString:productIdentifier]) {
            result = YES;
            break;
        }
    }
    return result;
}
///////////////////////////////////////////////
#pragma mark -
#pragma mark Restore purchases
///////////////////////////////////////////////

- (void)restorePurchases
{
    [self restorePurchasesWithUserHash:nil];
}

- (void)restorePurchasesWithUserHash:(NSString *)userhash
{
    if (userhash) {
        [[SKPaymentQueue defaultQueue] restoreCompletedTransactionsWithApplicationUsername:userhash];
    }
    else {
        [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:StoreObserverWillRestoreNotification object:self];
}

// Implement the refreshing the app receipt
// TODO: when receip validation is failed we have to refresh the receipt, as Apple suggests
- (void)refreshReceipts
{
    NSDictionary *sandboxProperties = nil;
    SKReceiptRefreshRequest* request = [[SKReceiptRefreshRequest alloc] initWithReceiptProperties:sandboxProperties];
    request.delegate = self;
    [request start];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark SKRequestDelegate methods
///////////////////////////////////////////////

- (void)requestDidFinish:(SKRequest *)request
{
    UBILogTrace(@"%@", request);
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
    if (error) {
        UBILogError(@"[ERROR]: %@", [error localizedDescription]);
    }
    
    UBILogError(@"%@", request);
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark SKPaymentTransactionObserver methods
///////////////////////////////////////////////

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
	for(SKPaymentTransaction * transaction in transactions)
	{
		switch (transaction.transactionState)
		{
            case SKPaymentTransactionStatePurchasing: {
                [self changeStatus:SOProductStatusPurchaseInProgress
              forProductIdentifier:transaction.payment.productIdentifier
                             error:nil];
            } break;
            
			case SKPaymentTransactionStatePurchased:
            case SKPaymentTransactionStateRestored: {
            {
                // we do not rely on SKPaymentQueue state, as the restore not always happens
                // via SKPaymentTransactionStateRestored state, so we find out the state from
                // original transaction
                SOProductStatus productStatus = SOProductStatusPurchaseSucceeded;
                if (transaction.originalTransaction) {
                    productStatus = SOProductStatusRestoreSucceeded;
                }
                
                // Check whether the purchased product has content hosted with Apple.
                if(transaction.downloads && transaction.downloads.count > 0) {
                    // Do not expect any downloads for a transaction
                    // for now, just finish transaction when in production
                    assert(NO);
                    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
                }
                else {
                    [self processTransaction:transaction forStatus:productStatus];
                }
            } break;

            } break;
                
			case SKPaymentTransactionStateFailed: {
                SOProductStatus productStatus = SOProductStatusPurchaseFailed;
                if (transaction.originalTransaction) {
                    productStatus = SOProductStatusRestoreFailed;
                }
                [self processTransaction:transaction forStatus:productStatus];
            } break;
            
            // Transaction is waiting for apptove (Ask to Buy)
            case SKPaymentTransactionStateDeferred: {
                UBILogTrace(@"Deferred product for %@",transaction.payment.productIdentifier);
                [self changeStatus:SOProductStatusPurchaseDeferred
              forProductIdentifier:transaction.payment.productIdentifier
                             error:nil];
            } break;
                
			default: break;
		}
	}
}


// Called when the payment queue has downloaded content
- (void)paymentQueue:(SKPaymentQueue *)queue updatedDownloads:(NSArray *)downloads
{
    // We do not expect it to be ever called so far
    assert(NO);
}

// Logs all transactions that have been removed from the payment queue
- (void)paymentQueue:(SKPaymentQueue *)queue removedTransactions:(NSArray *)transactions
{
	for(SKPaymentTransaction * transaction in transactions) {
		UBILogTrace(@"%@ was removed from the payment queue.", transaction.payment.productIdentifier);
	}
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Restoration callbacks
///////////////////////////////////////////////

// Called when an error occur while restoring purchases. Notify the user about the error.
- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
    NSDictionary* userInfo = nil;
    if (error.code != SKErrorPaymentCancelled) {
        userInfo = @{SOProductStatusNotificationErrorKey: error};
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:StoreObserverRestoreDidFailNotification
                                                        object:self
                                                      userInfo:userInfo];
}


// Called when all restorable transactions have been processed by the payment queue
- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    [[NSNotificationCenter defaultCenter] postNotificationName:StoreObserverDidFinishRestoreNotification object:self];
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark Complete transaction
///////////////////////////////////////////////

-(void)processTransaction:(SKPaymentTransaction *)transaction forStatus:(SOProductStatus)status
{
    NSString* productIdentifier = transaction.payment.productIdentifier;
    NSError* transactionError = transaction.error;
    
    [self changeStatus:status forProductIdentifier:productIdentifier error:transaction.error];
    
    // Validate the receipt OR finish if error
    if (!transactionError) {
        [self validateReceiptForTransaction:transaction];
    }
    else {
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    }
}

///////////////////////////////////////////////
#pragma mark -
#pragma mark - FASAPIDelegate
///////////////////////////////////////////////

- (void)FASAPIdidInvoke:(FASAPI *)theFASAPI
{
    if (theFASAPI.method == FASAPIMethodAddUserPurchase) {
        
        SKPaymentTransaction* transaction = theFASAPI.userInfo[@"transaction"];
        assert(transaction);
        
        NSString* purchasedID = transaction.payment.productIdentifier;
        [self changeStatus:SOProductStatusReceiptValidationInProgress
   forProductIdentifier:purchasedID
                  error:nil];
    }
}

- (void)FASAPIdidFinish:(FASAPI *)theFASAPI
{
    if (theFASAPI.method == FASAPIMethodAddUserPurchase) {
        
        SKPaymentTransaction* transaction = theFASAPI.userInfo[@"transaction"];
        assert(transaction);
        
        NSString* purchasedID = transaction.payment.productIdentifier;
        [self changeStatus:SOProductStatusReceiptValidationSucceeded
   forProductIdentifier:purchasedID
                  error:nil];
        [[FASData sharedData] trackEvent:@"WorkoutPackPurchase" withAction:purchasedID];
        
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    }
}

- (void)FASAPIdidFail:(FASAPI *)theFASAPI
{
    if (theFASAPI.method == FASAPIMethodAddUserPurchase) {
        SKPaymentTransaction* transaction = theFASAPI.userInfo[@"transaction"];
        assert(transaction);
        
        NSString* productID = transaction.payment.productIdentifier;
        NSError* apiError = [[theFASAPI errors] firstObject];
        
        UBILogError(@"Product validation DID FAIL for ID: %@\n\n%@", productID, theFASAPI.errors);
        
        [self changeStatus:SOProductStatusReceiptValidationFailed
   forProductIdentifier:productID
                  error:apiError];
        
        // TODO: check the error code and request -refreshReceipts call if needed
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    }
}

@end

///////////////////////////////////////////////
#pragma mark -
#pragma mark - Protected Purchase
///////////////////////////////////////////////

#import "FASAPISession.h"
#import "StringUtils.h"
#import "FASUserInfo.h"

@implementation StoreObserver (PurchaseWithSession)

- (void)buyProductWithSession:(SKProduct *)product
{
    if ([[FASAPISession currentSession] isLoggedIn]) {
        NSString* userID = [[[FASAPISession currentSession] userInfo] stringUserID];
        if (userID && ![userID isBlank]) {
            NSString* hashedUserID = [StringUtils MD5:userID];
            [self buy:product withUserHash:hashedUserID];
        }
    }
}

- (void)restorePurchasesWithSession
{
    if ([[FASAPISession currentSession] isLoggedIn]) {
        NSString* userID = [[[FASAPISession currentSession] userInfo] stringUserID];
        if (userID && ![userID isBlank]) {
            NSString* hashedUserID = [StringUtils MD5:userID];
            [self restorePurchasesWithUserHash:hashedUserID];
        }
    }
}

@end
