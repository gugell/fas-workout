//
//  FASSettingsViewController.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 8/16/15.
//  Copyright (c) 2015 FAS. All rights reserved.
//

import UIKit
import Crashlytics
    
final class FASSettingsViewController: FASViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    // data source
    var settingsType: UBISettingsType = .Default {
        didSet {
            refreshCurrentSettings()
        }
    }
    
    private var currentSettings: [UBISettingsGroup]?
    
    // MARK: - Presentation -
    
    // TODO: viewController is required to be of FASViewController type b/c of mainStoryboard property, is it possible to pass UIViewController with protocol extension?
    class func presentFromViewController(viewController: FASViewController, withSettingsType settingsType: UBISettingsType = .Default) {
        if let vc = viewController.mainStoryboard.instantiateViewControllerWithIdentifier("SettingsStoryboardID") as? FASSettingsViewController {
            vc.settingsType = settingsType
            vc.title = settingsType.description
            vc.presentationMethod = .Modal
            let nav = UINavigationController(rootViewController: vc)
            nav.modalPresentationStyle = .PageSheet
            viewController.presentViewController(nav, animated: true, completion: nil)
        }
    }
    
    // MARK: - Init -
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "applicationDidBecomeActive:", name: UIApplicationDidBecomeActiveNotification, object: nil)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIApplicationDidBecomeActiveNotification, object: nil)
    }

    // MARK: - UIViewController methods -
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if presentationMethod == .Modal {
            let closeButton: UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "cross"), style: .Plain, target: self, action: Selector("doCloseSettingsVC"))
            navigationItem.leftBarButtonItem = closeButton
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar(.Dark)
    }
    
    // MARK: - Update Settings Handler -
    
    private func refreshCurrentSettings() {
        currentSettings = UBISettings.settingsForType(settingsType)
    }
    
    private func handleSettingsChange() {
        refreshCurrentSettings()
        self.tableView.reloadData()
    }
    
    // MARK: - Actions -
    
    private func showDateTimePickerForSetting(atIndexpath indexPath: NSIndexPath, animated: Bool) {
        // TODO: refresh settings with the flag that allows date picker setting to show up
    }
    
    func applicationDidBecomeActive(notification: NSNotification) {
        handleSettingsChange()
    }

    func doCloseSettingsVC() {
        dismissViewControllerAnimated(true, completion: nil)
    }
}

extension FASSettingsViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if let aSetting = settingAtIndexPath(indexPath) {
            switch aSetting.tag {
            case .DebugTestQuery:
                let runListAPI: FASAPI = FASAPI.testRunList(nil)
                runListAPI.invoke()
            case .DebugSkipWelcomeScreen:
                0 // do nothing
//                let currentSettingValue: Bool = aSetting.value as! Bool
//                aSetting.value = !currentSettingValue
                //            settingSkipWelcomeScreen(aSetting.value as! Bool)
            case .DebugForceDownloadAttachFiles:
                0 // do nothing
//                let currentSettingValue: Bool = aSetting.value as! Bool
//                aSetting.value = !currentSettingValue
                //            settingForceDownloadAttachFiles(aSetting.value as! Bool)
            case .NotificationsLocalDate:
                showDateTimePickerForSetting(atIndexpath: indexPath, animated: false)
            case .SystemNotificationsPermissions:
                FASData.sharedData().openSettings()
            case .DebugCrashNow:
                Crashlytics.sharedInstance().crash()
            default:
                0 // do ntohing
            }
        }
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = UIColor.clearColor()
    }
}

extension FASSettingsViewController: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return (self.currentSettings?.count ?? 0)
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rowCount = 0
        if let settings = self.currentSettings {
            let sectionInfo =  settings[section]
            rowCount = (sectionInfo.items?.count ?? 0)
        }
        return rowCount
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return self.tableView(tableView, cellForSetting: settingAtIndexPath(indexPath)! )
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return self.tableView(tableView, heightForSetting: settingAtIndexPath(indexPath)! )
    }
    
    // Header
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let settings = currentSettings {
            let sectionInfo = settings[section]
            return sectionInfo.headerTitle
        }
        return nil
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if let title = self.tableView(tableView, titleForHeaderInSection: section) {
            let heightForHeader = FASViewHelper.tableView(tableView, heightForHeaderWithTitle: title)
            return heightForHeader + 10.0
        }
        return 0.1
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let title: String? = self.tableView(tableView, titleForHeaderInSection: section)
        return FASViewHelper.tableSectionHeaderView(title)
    }
    
    // Footer
    
    func tableView(tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        if let settings = currentSettings {
            let sectionInfo = settings[section]
            return sectionInfo.footerTitle
        }
        return nil
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if let title = self.tableView(tableView, titleForFooterInSection: section) {
            let heightForFooter = FASViewHelper.tableView(tableView, heightForFooterWithTitle: title)
            return heightForFooter
        }
        return 10.0
    }
    
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let title: String? = self.tableView(tableView, titleForFooterInSection: section)
        return FASViewHelper.tableSectionFooterView(title)
    }
    
    // Helpers
    
    private func tableView(sTableView: UITableView, heightForSetting aSetting: UBISetting) -> CGFloat {
        switch aSetting.style {
        case .DatePicker:
            return 217
        default:
            return 61
        }
    }
    
    private func tableView(sTableView: UITableView, cellForSetting aSetting: UBISetting) -> UITableViewCell {
        let cellStyle: UBISettingCellStyle = aSetting.style
        let cellTag: UBISettingTag = aSetting.tag
        let cellIdentifier: String = "cell\(cellStyle.description)"
        var cell: UITableViewCell? = sTableView.dequeueReusableCellWithIdentifier(cellIdentifier)
        
        switch cellStyle {
        case .Default:
            0 // do ntohing
        case .Checkmark:
            0 // do ntohing
        case .Switch:
            if let switchCell = cell as? FASFormSwitchCell {
                if switchCell.tag == 0 {
                    switchCell.delegate = self
                    switchCell.tag = 1
                }
                switchCell.setting = aSetting
            }
        case .Action:
            if let asyncCell = cell as? FASFormActionCell {
                if asyncCell.tag == 0 {
                    asyncCell.tag = 1
                }
                
                asyncCell.titleLabel?.text = aSetting.title
            }
        case .ValueRight:
            0 // do nothing
        case .Subtitle:
            if cell == nil {
                cell = UITableViewCell(style: .Subtitle, reuseIdentifier: cellIdentifier)
                cell!.selectionStyle = UITableViewCellSelectionStyle.None
                cell!.detailTextLabel!.adjustsFontSizeToFitWidth = true;
            }
            
            cell!.textLabel!.text = aSetting.title
            var value: String?
            if let strValue = aSetting.value as? String {
                value = strValue
            }
            else if value == nil || value!.isBlank() {
                value = "(no value)"
            }
            cell!.detailTextLabel!.text = value
            cell!.textLabel!.font = UIFont.systemFontOfSize(16.0)
            cell!.detailTextLabel!.font = UIFont.systemFontOfSize(16.0)
            cell!.textLabel!.textColor = UIColor.whiteColor()
            cell!.detailTextLabel!.textColor = UIColor.whiteColor()
        case .ActivityIndicator:
            if let activityCell = cell as? FASActivityCell {
                activityCell.statusLabel!.text = aSetting.title
            }
        case .DatePicker:
            if let datePickerCell = cell as? FASFormDatePickerCell {
                if datePickerCell.tag == 0 {
                    datePickerCell.tag = 1
                    // the hack to make picker interface white
                    datePickerCell.datePicker.setValue(UIColor.whiteColor(), forKeyPath: "textColor")
                    datePickerCell.delegate = self
                }
                if let settingDate = aSetting.value as? NSDate {
                    datePickerCell.datePicker.date = settingDate
                }
                else {
                    datePickerCell.datePicker.date = NSDate()
                }
            }
        }
        
        switch cellTag {
        case .DebugTestQuery:
            cell!.textLabel!.text = aSetting.title
        case .DebugSkipWelcomeScreen, .DebugForceDownloadAttachFiles:
            cell!.textLabel!.text = aSetting.title
            if let shouldSkip = aSetting.value as? Bool {
                cell!.accessoryType = (shouldSkip ? .Checkmark : .None)
            }
        case .NotificationsLocalDate:
            if let valueRightCell = cell as? FASValueRightCell {
                valueRightCell.titleLabel?.text = aSetting.title
                valueRightCell.subtitleLabel?.text = ((aSetting.value as? NSDate)?.toStringShortTime() ?? "?")
                valueRightCell.selectionStyle = UITableViewCellSelectionStyle.None
            }
        case .SystemNotificationsPermissions:
            if let actionCell = cell as? FASFormActionCell {
                actionCell.titleLabel.text = aSetting.title
            }
        default:
            0 // do ntohing
        }
        
        return cell!
    }
    
    func settingAtIndexPath(indexPath: NSIndexPath) -> UBISetting? {
        if let settings = currentSettings {
            let sectionInfo = settings[indexPath.section]
            return sectionInfo.items?[indexPath.row]
        }
        
        return nil
    }
}

extension FASSettingsViewController: FASFormSwitchCellDelegate {
    func fasFormSwitchCellValueChanged(switchCell: FASFormSwitchCell) {
        if let indexPath = self.tableView.indexPathForCell(switchCell) {
            var setting = self.settingAtIndexPath(indexPath)
            
            if setting != nil {
                switch setting!.tag {
                case .NotificationsLocalEnable:
                    if let boolValue = switchCell.switchControl?.on {
                        setting!.value = boolValue
                        handleSettingsChange()
                    }
                case .NotificationsRemotePromoEnable:
                    if let boolValue = switchCell.switchControl?.on {
                        setting!.value = boolValue
                        handleSettingsChange()
                    }
                default:
                    0 // do ntohing
                }
            }
            
        }
    }
}

extension FASSettingsViewController: FASFormDatePickerCellDelegate {
    func fasFormDatePickerCellValueChanged(datePickerCell: FASFormDatePickerCell) {
        if let indexPath = self.tableView.indexPathForCell(datePickerCell) {
            var setting = self.settingAtIndexPath(indexPath)
            
            if setting != nil {
                switch setting!.tag {
                case .NotificationsPickDate:
                    setting!.value = datePickerCell.datePicker.date
                    handleSettingsChange()
                default: break
                }
            }
        }
    }
}
