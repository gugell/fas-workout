//
//  FASViewHelper.swift
//  FASWorkouts
//
//  Created by Evgen Dubinin on 12/15/14.
//  Copyright (c) 2014 FAS. All rights reserved.
//

import UIKit

enum FASViewHelperButtonStyle {
    case Stroked, Filled
}

class FASViewHelper {
    
    // Apply a FAS style to a button
    
    class func applyStyle(style: FASViewHelperButtonStyle, toButton button: UIButton?) {
        if let btn = button {
            switch style {
            case .Stroked:
                
                let patternInsets = UIEdgeInsets(top: 6.0, left: 4.0, bottom: 6.0, right: 4.0)
                
                let normalImage = UIImage.stretchableImage(named: "YellowButtonBackggroundNormalStroke", inset: patternInsets)
                let highlightedImage = UIImage.stretchableImage(named: "YellowButtonBackggroundHighlightedStroke", inset: patternInsets)
                
                btn.setBackgroundImage(normalImage, forState: .Normal)
                btn.setBackgroundImage(highlightedImage, forState: .Highlighted)
                
            case .Filled:
                let patternInsets = UIEdgeInsets(top: 6.0, left: 5.0, bottom: 6.0, right: 5.0)
                
                let normalImage = UIImage.stretchableImage(named: "YellowButtonBackggroundNormalFill", inset: patternInsets)
                let highlightedImage = UIImage.stretchableImage(named: "YellowButtonBackggroundHighlightedFill", inset: patternInsets)
                btn.setBackgroundImage(normalImage, forState: .Normal)
                btn.setBackgroundImage(highlightedImage, forState: .Highlighted)
            }
        }
    }
    
    class func backgroundImageForViewController(viewControler: AnyObject?) -> UIImage? {
        var image: UIImage?
        
        if let vc: AnyObject = viewControler {
            switch (vc) {
            case is LoginInfoViewController:
                image = UIImage(named: "shutterstock_133766795")
            case is LoginViewController:
                image = UIImage(named: "shutterstock_212753242")
            case is RegistrationViewController:
                image = UIImage(named: "shutterstock_187665596")
            case is WelcomeViewController:
                image = UIImage(named: "shutterstock_164812154")
            case is WorkoutsListViewController:
                fallthrough
            case is WorkoutDetailViewController:
                fallthrough
            case is FASMenuViewController:
                fallthrough
            case is FASSettingsViewController:
                fallthrough
            case is FASTextViewController:
                fallthrough
            case is FASWorkoutViewController:
                image = UIImage(named: "shutterstock_215295838")
            case is FASStoreViewController:
                image = UIImage(named: "shutterstock_201512071")
            default:
                image = nil
                break;
            }
        }
        
        return image
    }
    
    ///////////////////////////////////////////////
    // MARK: - TableView Helpers -
    ///////////////////////////////////////////////
    
    class func tableSectionHeaderView(title: String?) -> UBILabel? {
        var headerLabel: UBILabel?
        if let title = title {
            headerLabel = UBILabel()
            headerLabel!.numberOfLines = 0
            headerLabel!.lineBreakMode = NSLineBreakMode.ByWordWrapping
            headerLabel!.edgeInsets = UIEdgeInsetsMake(5, 15, 5, 15)
            headerLabel!.font = UIFont.boldBabasFontOfSize(14.0)
            headerLabel!.textColor = UIColor.whiteColor()
            headerLabel!.textAlignment = NSTextAlignment.Center
            headerLabel!.text = title;
        }
        return headerLabel
    }
    
    class func tableSectionFooterView(title: String?) -> UBILabel? {
        var footerLabel: UBILabel?
        if let title = title {
            footerLabel = UBILabel()
            footerLabel!.numberOfLines = 0
            footerLabel!.lineBreakMode = NSLineBreakMode.ByWordWrapping
            footerLabel!.edgeInsets = UIEdgeInsetsMake(5, 15, 5, 15)
            footerLabel!.font = UIFont.boldBabasFontOfSize(14.0)
            footerLabel!.textColor = UIColor.whiteColor()
            footerLabel!.text = title;
        }
        return footerLabel
    }
    
    class func tableView(tableView: UITableView, heightForHeaderWithTitle title: String?) -> CGFloat {
        var height: CGFloat = 0.0
        if let title = title {
            // TODO: try to use sizeToFit constraining to tableView's width
            let r = title.boundingRectWithSize(CGSizeMake(tableView.bounds.size.width, 0),
                options: NSStringDrawingOptions.UsesLineFragmentOrigin,
                attributes: [NSFontAttributeName:UIFont.boldBabasFontOfSize(14.0)],
                context: nil)
            height = r.size.height + 12.0
        }
        return height
    }
    
    class func tableView(tableView: UITableView, heightForFooterWithTitle title: String?) -> CGFloat {
        var height: CGFloat = 0.0
        if let title = title {
            // TODO: try to use sizeToFit constraining to tableView's width
            let r = title.boundingRectWithSize(CGSizeMake(tableView.bounds.size.width, 0),
                options: NSStringDrawingOptions.UsesLineFragmentOrigin,
                attributes: [NSFontAttributeName:UIFont.boldBabasFontOfSize(14.0)],
                context: nil)
            height = r.size.height + 12.0
        }
        return height
    }
}
